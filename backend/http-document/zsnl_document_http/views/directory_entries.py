# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty_pyramid.views.pydantic_entity import JSONAPIEntityView


class DirectoryEntriesViews(JSONAPIEntityView):
    view_mapper = {
        "GET": {
            "get_directory_entries_for_intake_count": {
                "cq": "query",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.document",
                "run": "get_directory_entries_for_intake_count",
                "from": {
                    "request_params": {
                        "filter[fulltext]": "search_term",
                        "filter[attributes.assignment.is_set]": "assigned",
                        "filter[attributes.assignment.is_self]": "assigned_to_self",
                    }
                },
            },
        },
    }

# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty_infra_sqlalchemy.json_encoder import _json_encoder
from uuid import uuid4


class TestJsonEncoder:
    def test_encoding_standard_json(self):
        structure = {"human": {"name": "frits", "gender": "male"}}

        assert (
            _json_encoder(structure)
            == """{"human": {"name": "frits", "gender": "male"}}"""
        )

    def test_encoding_uuid(self):
        test_uuid = uuid4()
        structure = {"uuid": test_uuid}

        assert _json_encoder(structure) == '{"uuid": "' + str(test_uuid) + '"}'

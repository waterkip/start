# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .event_logger_base import BaseLogger
from .repository import get_user
from minty.exceptions import NotFound
from typing import Final
from uuid import UUID
from zsnl_domains.database.schema import Logging

JOB_TYPE_MAPPING: Final = {"delete_case": "Zaken vernietigen"}
JOB_EMPTY_CONTEXT_NAME: Final = "(naamloos)"


class JobsBase(BaseLogger):
    def __init__(self):
        pass

    def __call__(self, session, event) -> Logging | None:
        """Collect and necessary information before creating log record."""

        try:
            user_info = get_user(session=session, uuid=event["user_uuid"])
        except NotFound:
            # User not found.
            return

        formatted_entity_data = self.format_entity_data(event=event)

        event_parameters = self.generate_event_parameters(
            entity_id=event["entity_id"],
            entity_data=formatted_entity_data,
        )

        event_parameters["correlation_id"] = event["correlation_id"]
        return self._create_logging_record(
            user_info=user_info,
            event_type=self.event_type,
            subject=self._generate_subject(event_parameters),
            component=self.component,
            component_id=None,
            case_id=None,
            created_date=event["created_date"],
            event_data=event_parameters,
            restricted=False,
        )

    def _generate_subject(self, event_parameters: dict) -> str:
        """Generate subject line formatted with parameters from decoded message.

        :param event_parameters: to use in formatting
        :type event_parameters: dict
        :return: subject line to log
        :rtype: str
        """
        vars = [event_parameters[v] for v in self.variables]
        log_subject = self.subject.format(*vars)
        return log_subject

    def generate_event_parameters(
        self,
        entity_data: dict,
        entity_id: UUID,
    ):
        raise NotImplementedError  # pragma: no cover


class JobCreated(JobsBase):
    def __init__(self):
        super().__init__()
        self.component = "jobs"
        self.subject = "Job '{}' ({}) van type '{}' aangemaakt."
        self.variables = ["friendly_name", "id", "job_type"]
        self.event_type = "job/created"

    def generate_event_parameters(
        self,
        entity_data: dict,
        entity_id: UUID,
    ):
        job_type = JOB_TYPE_MAPPING.get(
            entity_data["job_description"]["job_type"],
            entity_data["job_description"]["job_type"],
        )

        event_parameters = {
            "id": entity_id,
            "job_type": job_type,
            "friendly_name": entity_data["friendly_name"]
            or JOB_EMPTY_CONTEXT_NAME,
            "job_description": entity_data["job_description"],
        }

        return event_parameters


class JobCancelled(JobsBase):
    def __init__(self):
        super().__init__()
        self.component = "jobs"
        self.subject = "Job '{}' ({}) geannuleerd."
        self.variables = ["friendly_name", "id"]
        self.event_type = "job/cancelled"

    def generate_event_parameters(
        self,
        entity_data: dict,
        entity_id: UUID,
    ):
        event_parameters = {
            "id": entity_id,
            "friendly_name": entity_data["friendly_name"]
            or JOB_EMPTY_CONTEXT_NAME,
        }

        return event_parameters


class JobDeleted(JobsBase):
    def __init__(self):
        super().__init__()
        self.component = "jobs"
        self.subject = "Job '{}' ({}) verwijderd."
        self.variables = ["friendly_name", "id"]
        self.event_type = "job/deleted"

    def generate_event_parameters(
        self,
        entity_data: dict,
        entity_id: UUID,
    ):
        event_parameters = {
            "id": entity_id,
            "friendly_name": entity_data["friendly_name"]
            or JOB_EMPTY_CONTEXT_NAME,
        }

        return event_parameters


class JobResultsDownloaded(JobsBase):
    def __init__(self):
        super().__init__()
        self.component = "jobs"
        self.subject = "Resultaat van Job '{}' ({}) gedownload."
        self.variables = ["friendly_name", "id"]
        self.event_type = "job/download_result"

    def generate_event_parameters(
        self,
        entity_data: dict,
        entity_id: UUID,
    ):
        event_parameters = {
            "id": entity_id,
            "friendly_name": entity_data["friendly_name"]
            or JOB_EMPTY_CONTEXT_NAME,
        }
        return event_parameters


class JobErrorsDownloaded(JobsBase):
    def __init__(self):
        super().__init__()
        self.component = "jobs"
        self.subject = "Foutrapportage van Job '{}' ({}) gedownload."
        self.variables = ["friendly_name", "id"]
        self.event_type = "job/download_error"

    def generate_event_parameters(
        self,
        entity_data: dict,
        entity_id: UUID,
    ):
        event_parameters = {
            "id": entity_id,
            "friendly_name": entity_data["friendly_name"]
            or JOB_EMPTY_CONTEXT_NAME,
        }
        return event_parameters

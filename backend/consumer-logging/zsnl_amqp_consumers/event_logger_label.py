# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .event_logger_base import BaseLogger
from .repository import DocumentInformation, get_case, get_document, get_user
from minty.exceptions import NotFound
from zsnl_domains.database.schema import Logging


class LabelBase(BaseLogger):
    def __init__(self):
        pass

    def __call__(self, session, event) -> Logging:
        """Collect and necessary information before creating log record."""

        try:
            user_info = get_user(session=session, uuid=event["user_uuid"])
        except NotFound:
            # User not found.
            return

        try:
            document_info = get_document(
                session=session, uuid=event["entity_id"]
            )
        except NotFound:
            # Document not found.
            return

        formatted_entity_data = self.format_entity_data(event=event)
        case_uuid = formatted_entity_data.get("case_uuid")
        case_id = None
        case_info = None

        if case_uuid:
            try:
                case_info = get_case(session, case_uuid)
                case_id = case_info.id
            except NotFound:
                return

        event_parameters = self.generate_event_parameters(
            entity_data=formatted_entity_data,
            document_info=document_info,
        )

        event_parameters["correlation_id"] = event["correlation_id"]
        return self._create_logging_record(
            user_info=user_info,
            event_type=self.event_type,
            subject=self._generate_subject(event_parameters),
            component=self.component,
            component_id=None,
            case_id=case_id,
            created_date=event["created_date"],
            event_data=event_parameters,
            restricted=False,
        )

    def _generate_subject(self, event_parameters: dict) -> str:
        """Generate subject line formatted with parameters from decoded message.

        :param event_parameters: to use in formatting
        :type event_parameters: dict
        :return: subject line to log
        :rtype: str
        """
        vars = [event_parameters[v] for v in self.variables]
        log_subject = self.subject.format(*vars)
        return log_subject

    def generate_event_parameters(
        self,
        entity_data: dict,
        document_info: DocumentInformation,
    ):
        raise NotImplementedError

    def get_label_string(
        self, entity_data: dict, all_labels: str, filter_labels: str
    ):
        """Returns a string containing all magic strings of labels that are in
        all_labels but not in filter_labels
        """
        labels = []
        for label in entity_data[all_labels]:
            if not next(
                (
                    item
                    for item in entity_data[filter_labels]
                    if item["uuid"] == label["uuid"]
                ),
                None,
            ):
                labels.append(label["magic_string"])

        labels_string: str = ""
        if len(labels) > 1:
            labels_string += "s '"
        else:
            labels_string += " '"

        first_label = True
        for label in labels:
            if first_label:
                labels_string += label
                first_label = False
            else:
                labels_string += ", " + label
        return labels_string


class LabelApplied(LabelBase):
    def __init__(self):
        super().__init__()
        self.component = "document"
        self.subject = "Documentlabel{}' toegevoegd aan document {}."
        self.variables = ["documentlabel", "document"]
        self.event_type = "document/label/apply"

    def generate_event_parameters(
        self,
        entity_data: dict,
        document_info: DocumentInformation,
    ):
        event_parameters = {
            "documentlabel": self.get_label_string(
                entity_data, "labels", "labels_old"
            ),
            "document": document_info.name,
        }

        return event_parameters


class LabelDeleted(LabelBase):
    def __init__(self):
        super().__init__()
        self.component = "document"
        self.subject = "Documentlabel{}' verwijderd van document {}."
        self.variables = ["documentlabel", "document"]
        self.event_type = "document/label/delete"

    def generate_event_parameters(
        self,
        entity_data: dict,
        document_info: DocumentInformation,
    ):
        event_parameters = {
            "documentlabel": self.get_label_string(
                entity_data, "labels_old", "labels"
            ),
            "document": document_info.name,
        }

        return event_parameters

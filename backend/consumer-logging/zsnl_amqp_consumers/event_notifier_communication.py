# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from . import repository
from .event_notifier_base import BaseNotifier
from minty.exceptions import NotFound
from zsnl_domains.database.schema import Message


class CommunicationBase(BaseNotifier):
    def __call__(self, session, event, logging_id) -> Message | None:
        """Collect and necessary information before creating log record.

        :param BaseMessage: base class for message
        :type BaseMessage: BaseMessage
        :param session: database session
        :type session: Session
        :param event: event
        :type event: Event
        :param logging_id: id of entry in logging table
        :type event: int
        :raises error: no document found or no case_assignee found
        :return: logging record
        :rtype: Logging
        """
        formatted_entity_data = self.format_entity_data(event=event)
        creator_type = formatted_entity_data.get("creator_type")

        thread_uuid = formatted_entity_data.get(
            "thread_uuid", None
        ) or formatted_entity_data.get("uuid", None)

        if not thread_uuid:
            self.logger.debug("Not creating message: no thread UUID?")
            return

        try:
            self.logger.debug("Retrieving case by thread")
            case, thread = repository.get_case_from_thread(
                session=session,
                thread_uuid=thread_uuid,
            )

        # If the message is not a case message, no message notification
        except NotFound:
            self.logger.debug("Not creating message: no case_id")
            return

        if thread.is_notification:
            self.logger.debug(
                "Not creating message: message is in a notification thread"
            )
            return

        # If the case has no assignee, no message notification
        try:
            assignee_info = repository.get_case_assignee(
                session=session, case_uuid=case.uuid
            )
        except NotFound:
            self.logger.debug("Not creating message: no case_assignee")
            return

        # If user is pip user, always create notification.
        # If the logged in user is the assignee of case, no notification.
        if (
            str(assignee_info.uuid) != event["user_uuid"]
            or creator_type == "pip"
        ):
            return self._create_message_notifier_record(
                assignee_info=assignee_info,
                subject=self._generate_subject(formatted_entity_data),
                logging_id=logging_id,
                is_read=False,
                is_archived=False,
            )
        else:
            self.logger.debug("Not creating message")
            return

    def _generate_subject(self, entity_data: dict) -> str:
        """Generate subject line formatted with parameters from decoded message.

        :param entity_data: formatted entity data
        :type entity_data: dict
        :return: message line to log
        :rtype: str
        """
        raise NotImplementedError


class ExternalMessageCreated(CommunicationBase):
    def _generate_subject(self, entity_data: dict):
        external_message_type = entity_data.get("external_message_type", None)

        translated_type = {"pip": "PIP-bericht", "email": "E-mail"}
        translated_message_type = translated_type.get(external_message_type)
        return f"{translated_message_type} toegevoegd"


class NoteCreated(CommunicationBase):
    def _generate_subject(self, entity_data: dict):
        return "Notitie toegevoegd"


class ContactMomentCreated(CommunicationBase):
    def _generate_subject(self, entity_data: dict):
        return "Contactmoment toegevoegd"

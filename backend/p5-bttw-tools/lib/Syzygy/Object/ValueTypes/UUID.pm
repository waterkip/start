package Syzygy::Object::ValueTypes::UUID;
our $VERSION = '0.006';
use Moose;
use namespace::autoclean;

with qw[
    Syzygy::Interface::ValueType
];

=head1 NAME

Syzygy::Object::ValueTypes::UUID - Syzygy value type for UUID values

=head1 DESCRIPTION

Many parts of Syzygy use UUIDs for indentification of objects and related
entities. This value type provides constraints, checks, and comparison
behavior for such values.

=cut

use Syzygy::Types qw[UUID];

=head1 METHODS

=head2 name

Implements L<Syzygy::Interface::ValueType/name>.

=cut

sub name {
    return 'uuid';
}

=head2 perl_type_constraint

Implements L<Syzygy::Interface::ValueType/perl_type_constraint>.

=cut

sub perl_type_constraint {
    return UUID
}

=head2 coerce

Implements L<Syzygy::Interface::ValueType/coerce>.

=cut

sub coerce {
    my $self = shift;
    my $value = shift;

    return unless $value->has_value;

    return $self->new_value($value->value) if $value->type_name eq 'string';

    return;
}

=head2 deflate_value

Implements L<Syzygy::Interface::ValueType/deflate_value>.

=cut

sub deflate_value {
    my $self = shift;
    my $value = shift;

    return $value->value;
}

=head2 equal

Performs an equality comparison between two values.

=cut

sub equal {
    my $self = shift;
    my $a = $self->coerce(shift);
    my $b = $self->coerce(shift);

    return unless defined $a && $b;

    return $a->value eq $b->value;
}

=head2 not_equal

Performs an inequality comparison between two values.

=cut

sub not_equal {
    return not shift->equal(@_);
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.

# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

stages:
    - Tag
    - QA
    - Build
    - Verify
    - Release

.http_admin_job_variables:
  variables:
    BACKEND_PATH: "backend/http-admin"
    ADD_JOB_ON_CHANGES_OF_1: "backend/domains/**/*"
    ADD_JOB_ON_CHANGES_OF_2: "backend/migration_libraries/**/*"
    ADD_JOB_ON_CHANGES_OF_3: "backend/minty/**/*"
    ADD_JOB_ON_CHANGES_OF_4: "backend/minty-infra-*/**/*"
    ADD_JOB_ON_CHANGES_OF_5: "backend/minty-pyramid/**/*"

http-admin:Tag latest container when there are no changes:
  extends: 
    - .http_admin_job_variables
    - .tag_latest_backend_container_template
  # only is used to add this job only when the app or ci template is NOT changed
  # This is not supported yet by rules
  only:
    variables:
      - $CI_COMMIT_TAG =~ /^release\//
      - $CI_COMMIT_BRANCH =~ /^(master|production|preprod|development)$/
  except:
    refs:
      - schedule
      - web
    changes:
      - "backend/http-admin/**/*"
      - "template.gitlab-ci.yml"
      - "backend/domains/**/*"
      - "backend/migration_libraries/**/*"
      - "backend/minty/**/*"
      - "backend/minty-infra-*/**/*"
      - "backend/minty-pyramid/**/*"

http-admin:REUSE Compliance:
  extends: 
    - .http_admin_job_variables
    - .reuse_compliance_template

http-admin:OpenAPI Lint:
  extends: 
    - .http_admin_job_variables
    - .openapi_lint_template

http-admin:Run Python tests:
  extends: 
    - .http_admin_job_variables
    - .python_tests_template

.build_container_image_http_admin:
    extends:
      - .http_admin_job_variables
      - .build_and_push_container_image_template
    needs:
      - "set-version-number-and-tag-commit"
      - "http-admin:REUSE Compliance"
      - "http-admin:OpenAPI Lint"
      - "http-admin:Run Python tests"
      - job: "minty-pyramid:REUSE Compliance"
        optional: true
      - job: "minty-pyramid:Run Python tests"
        optional: true
      - job: "domains:REUSE Compliance"
        optional: true
      - job: "domains:Run Python tests"
        optional: true
      - job: "migration_libraries:REUSE Compliance"
        optional: true
      - job: "migration_libraries:Run Python tests"
        optional: true

http-admin:Build container image (x86_64):
  extends: .build_container_image_http_admin
  tags: [ "xxllnc-shared", "arch:x86_64" ]
  variables:
    BUILD_ARCH: "amd64"

http-admin:Build container image (arm64):
  extends: .build_container_image_http_admin
  tags: [ "xxllnc-shared", "arch:arm64" ]
  variables:
    BUILD_ARCH: "arm64"

http-admin:Make multiarch manifest:
  extends:
    - .http_admin_job_variables
    - .make_multiarch_manifest_template
  variables:
    BUILD_ARCHS: "amd64 arm64"
  needs:
    - "set-version-number-and-tag-commit"
    - "http-admin:Build container image (x86_64)"
    - "http-admin:Build container image (arm64)"

http-admin:Create SBOM:
    extends:
      - .http_admin_job_variables
      - .create_sbom_template
    needs: ["http-admin:Make multiarch manifest"]

#
# Release targets
#
http-admin:Tag xcp release container:
  extends:
    - .http_admin_job_variables    
    - .release_on_xcp

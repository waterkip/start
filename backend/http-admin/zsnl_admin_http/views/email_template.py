# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from . import serializers
from minty_pyramid.session_manager import protected_route
from pyramid.httpexceptions import HTTPBadRequest
from pyramid.request import Request


@protected_route("beheer_zaaktype_admin")
def get_email_template_detail(request: Request, user_info):
    """Get detailed information about an email_template.

    :param request: request
    :type request: Request
    :param user_info: user information
    :type user_info: UserInfo
    :raises HTTPForbidden: forbidden
    :raises HTTPBadRequest: bad request
    :return: response
    :rtype: dict
    """
    try:
        uuid = request.params["email_template_id"]
    except KeyError as e:
        raise HTTPBadRequest(
            json={
                "errors": [
                    {"title": f"Required parameter not specified: '{e}'"}
                ]
            }
        ) from e

    query_instance = request.get_query_instance(
        "zsnl_domains.admin.catalog", user_info.user_uuid
    )

    email_template = query_instance.get_email_template_details_for_edit(
        uuid=uuid
    )

    return {
        "data": serializers.email_template_details_for_edit(email_template),
        "links": {"self": f"{request.current_route_path()}"},
    }


@protected_route("beheer_zaaktype_admin")
def edit_an_email_template(request: Request, user_info):
    try:
        email_template_uuid = request.json_body["email_template_uuid"]
        fields = request.json_body["fields"]
    except KeyError as error:
        raise HTTPBadRequest(
            json={"errors": [{"title": f"Missing parameter '{error}'"}]}
        ) from error

    command_params = {"uuid": email_template_uuid, "fields": fields}

    cmd = request.get_command_instance(
        "zsnl_domains.admin.catalog", user_info.user_uuid
    )
    cmd.edit_email_template(**command_params)
    return {"data": {"success": True}}


@protected_route("beheer_zaaktype_admin")
def create_an_email_template(request: Request, user_info):
    try:
        email_template_uuid = request.json_body["email_template_uuid"]
        fields = request.json_body["fields"]
    except KeyError as error:
        raise HTTPBadRequest(
            json={"errors": [{"title": f"Missing parameter '{error}'"}]}
        ) from error

    command_params = {"uuid": email_template_uuid, "fields": fields}

    cmd = request.get_command_instance(
        "zsnl_domains.admin.catalog", user_info.user_uuid
    )
    cmd.create_email_template(**command_params)

    return {"data": {"success": True}}


@protected_route("beheer_zaaktype_admin")
def delete_email_template(request: Request, user_info):
    try:
        email_template_uuid = request.json_body["email_template_uuid"]
        reason = request.json_body["reason"]
    except KeyError as error:
        raise HTTPBadRequest(
            json={"errors": [{"title": f"Missing parameter '{error}'"}]}
        ) from error

    command_params = {"uuid": email_template_uuid, "reason": reason}

    cmd = request.get_command_instance(
        "zsnl_domains.admin.catalog", user_info.user_uuid
    )
    cmd.delete_email_template(**command_params)

    return {"data": {"success": True}}

# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytest
from .test_zsnl_admin_http import MockRequest
from pyramid.httpexceptions import HTTPBadRequest, HTTPForbidden
from unittest import mock
from uuid import uuid4
from zsnl_admin_http.views import attribute_views


class TestAttributeViews:
    def test_attribute_no_admin(self):
        mock_query_instance = mock.MagicMock()
        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin":false} }
                    """
                }
            },
            params={},
            query_instances={
                "zsnl_domains.admin.catalog": mock_query_instance
            },
            command_instances={"none": None},
        )
        views = [attribute_views.delete_attribute]
        for view in views:
            with pytest.raises(HTTPForbidden):
                view(request)

    def test_attribute_search(self):
        mock_query_instance = mock.MagicMock()
        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin": true} }
                    """
                }
            },
            params={"type": "file", "search_string": "document"},
            command_instances={"none": None},
            query_instances={
                "zsnl_domains.admin.catalog": mock_query_instance
            },
        )
        mock_query_instance.search_atttribute_by_name.return_value = []
        rv = attribute_views.search_attribute_by_name(request)

        assert rv == {"data": [], "links": {"self": "mock_current_route"}}

    def test_attribute_search_no_search_string(self):
        mock_query_instance = mock.MagicMock()
        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin": true} }
                    """
                }
            },
            params={"type": ""},
            command_instances={"none": None},
            query_instances={
                "zsnl_domains.admin.catalog": mock_query_instance
            },
        )
        mock_query_instance.search_atttribute_by_name.return_value = []
        with pytest.raises(HTTPBadRequest):
            attribute_views.search_attribute_by_name(request)

    def test_delete_attribute(self):
        mock_command = mock.MagicMock()

        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin": true} }
                    """
                }
            },
            params={},
            query_instances={},
            command_instances={"zsnl_domains.admin.catalog": mock_command},
        )
        # validation of fields will happen in the domain
        request.body = {
            "attribute_uuid": str(uuid4()),
            "reason": "reason for delete",
        }
        res = attribute_views.delete_attribute(request)
        assert res == {"data": {"success": True}}

        with pytest.raises(HTTPBadRequest):
            request.body = {}
            res = attribute_views.delete_attribute(request)

# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from . import handlers
from minty_amqp.consumer import BaseConsumer


class DocumentProcessingConsumer(BaseConsumer):
    def _register_routing(self):
        handler_classes = (
            handlers.DocumentProcessingBaseHandler.__subclasses__()
        )
        self._known_handlers = [
            handler_class(self.cqrs) for handler_class in handler_classes
        ]

        self.routing_keys = []
        for handler in self._known_handlers:
            self.routing_keys.extend(handler.routing_keys)

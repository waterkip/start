# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

stages:
    - Tag
    - QA
    - Build
    - Verify
    - Release

.consumer_document_processing_job_variables:
  variables:
    BACKEND_PATH: "backend/consumer-document_processing"
    ADD_JOB_ON_CHANGES_OF_1: "backend/domains/**/*"
    ADD_JOB_ON_CHANGES_OF_2: "backend/minty-amqp/**/*"
    ADD_JOB_ON_CHANGES_OF_3: "backend/minty/**/*"
    ADD_JOB_ON_CHANGES_OF_4: "backend/minty-infra-*/**/*"

consumer-document_processing:Tag latest container when there are no changes:
  extends: 
    - .consumer_document_processing_job_variables
    - .tag_latest_backend_container_template
  # only is used to add this job only when the app or ci template is NOT changed
  # This is not supported yet by rules
  only:
    variables:
      - $CI_COMMIT_TAG =~ /^release\//
      - $CI_COMMIT_BRANCH =~ /^(master|production|preprod|development)$/
  except:
    refs:
      - schedule
      - web
    changes:
      - "backend/consumer-document_processing/**/*"
      - "template.gitlab-ci.yml"
      - "backend/domains/**/*"
      - "backend/minty/**/*"
      - "backend/minty-amqp/**/*"
      - "backend/minty-infra-*/**/*"

consumer-document_processing:REUSE Compliance:
  extends: 
    - .consumer_document_processing_job_variables
    - .reuse_compliance_template

consumer-document_processing:Run Python tests:
  extends: 
    - .consumer_document_processing_job_variables
    - .python_tests_template

.build_container_image_consumer_document_processing:
  extends:
    - .consumer_document_processing_job_variables
    - .build_and_push_container_image_template
  needs:
    - "set-version-number-and-tag-commit"
    - "consumer-document_processing:REUSE Compliance"
    - "consumer-document_processing:Run Python tests"
    - job: "minty-amqp:REUSE Compliance"
      optional: true
    - job: "minty-amqp:Run Python tests"
      optional: true
    - job: "domains:REUSE Compliance"
      optional: true
    - job: "domains:Run Python tests"
      optional: true

consumer-document_processing:Build container image (x86_64):
  extends: .build_container_image_consumer_document_processing
  tags: [ "xxllnc-shared", "arch:x86_64" ]
  variables:
    BUILD_ARCH: "amd64"

consumer-document_processing:Build container image (arm64):
  extends: .build_container_image_consumer_document_processing
  tags: [ "xxllnc-shared", "arch:arm64" ]
  variables:
    BUILD_ARCH: "arm64"

consumer-document_processing:Make multiarch manifest:
  extends:
    - .consumer_document_processing_job_variables
    - .make_multiarch_manifest_template
  variables:
    BUILD_ARCHS: "amd64 arm64"
  needs:
    - "set-version-number-and-tag-commit"
    - "consumer-document_processing:Build container image (x86_64)"
    - "consumer-document_processing:Build container image (arm64)"

consumer-document_processing:Create SBOM:
    extends:
      - .consumer_document_processing_job_variables
      - .create_sbom_template
    needs: ["consumer-document_processing:Make multiarch manifest"]

#
# Release targets
#
consumer-document_processing:Tag xcp release container:
  extends:
    - .consumer_document_processing_job_variables    
    - .release_on_xcp

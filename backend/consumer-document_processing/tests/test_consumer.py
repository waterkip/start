# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from unittest import TestCase, mock
from zsnl_document_processing_consumer import __main__
from zsnl_document_processing_consumer.consumers import (
    DocumentProcessingConsumer,
)


class TestConsumer(TestCase):
    def test_consumer(self):
        document_consumer = DocumentProcessingConsumer(
            queue="fake_queue_name",
            exchange="fake_exchange",
            cqrs=mock.Mock(),
            qos_prefetch_count=2,
            dead_letter_config={},
        )

        rk = document_consumer.routing_keys

        assert set(rk) == {
            "zsnl.v2.zsnl_domains_document.Document.SearchIndexSetDelayed",
        }

    def test_consumer_routing(self):
        consumer = DocumentProcessingConsumer.__new__(
            DocumentProcessingConsumer
        )
        consumer.cqrs = mock.Mock()

        consumer._register_routing()

        self.assertEqual(len(consumer._known_handlers), 1)

    @mock.patch("zsnl_document_processing_consumer.__main__.CQRS")
    @mock.patch(
        "zsnl_document_processing_consumer.__main__.InfrastructureFactory"
    )
    @mock.patch("zsnl_document_processing_consumer.__main__.AMQPClient")
    def test_main(self, mock_client, mock_infra, mock_cqrs):
        with mock.patch.object(__main__, "__name__", "__main__"):
            __main__.init()
            mock_client.assert_called()
            mock_client().register_consumers.assert_called_with(
                [DocumentProcessingConsumer]
            )
            mock_client().start.assert_called()

    @mock.patch("zsnl_document_processing_consumer.__main__.old_factory")
    def test_log_record_factory(self, old_factory):
        record = __main__.log_record_factory("a", test="b")

        old_factory.assert_called_once_with("a", test="b")
        assert record.zs_component == "zsnl_document_processing_consumer"  # type: ignore
        assert record == old_factory()

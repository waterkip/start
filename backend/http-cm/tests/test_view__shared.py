# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import unittest
from unittest import mock
from uuid import UUID, uuid4
from zsnl_case_management_http.views import case, custom_object, search_result


def _create_mock_entity(entity_type: str, entity_id: UUID):
    entity = mock.MagicMock()

    entity.result_type = None
    entity.type = None

    type(entity).entity_type = mock.PropertyMock(return_value=entity_type)
    type(entity).entity_id = mock.PropertyMock(return_value=entity_id)

    return entity


class TestViewShared(unittest.TestCase):
    def test_nonexistent_entity_type(self):
        request = mock.MagicMock()
        entity_id = uuid4()

        entity = _create_mock_entity("__no_object_type__", entity_id)

        ov = custom_object.CustomObjectViews(context="test", request=request)

        self.assertEqual(ov.create_link_from_entity(entity), None)

    def test_create_link_from_entity_object_type(self):
        request = mock.MagicMock()
        entity_id = uuid4()

        entity = _create_mock_entity("custom_object_type", entity_id)

        ov = custom_object.CustomObjectViews(context="test", request=request)

        ov.create_link_from_entity(entity)

        request.route_path.assert_called_once_with(
            "get_custom_object_type", _query={"uuid": str(entity_id)}
        )

    def test_create_link_from_entity_object(self):
        request = mock.MagicMock()
        entity_id = uuid4()

        entity = _create_mock_entity("custom_object", entity_id)

        ov = custom_object.CustomObjectViews(context="test", request=request)

        ov.create_link_from_entity(entity)

        request.route_path.assert_called_once_with(
            "get_custom_object", _query={"uuid": str(entity_id)}
        )

    def test_create_link_from_entity_employee(self):
        request = mock.MagicMock()
        entity_id = uuid4()

        entity = _create_mock_entity("employee", entity_id)

        ov = custom_object.CustomObjectViews(context="test", request=request)

        ov.create_link_from_entity(entity)

        request.route_path.assert_called_once_with(
            "get_subject", _query={"employee_uuid": str(entity_id)}
        )

    def test_create_link_from_entity_related_case(self):
        request = mock.MagicMock()
        entity_id = uuid4()

        entity = _create_mock_entity("related_case", entity_id)

        ov = custom_object.CustomObjectViews(context="test", request=request)

        ov.create_link_from_entity(entity)

        request.route_path.assert_called_once_with(
            "get_case", _query={"case_uuid": str(entity_id)}
        )

    def test_create_link_from_entity_person(self):
        request = mock.MagicMock()
        entity_id = uuid4()

        entity = _create_mock_entity("person", entity_id)

        ov = custom_object.CustomObjectViews(context="test", request=request)

        ov.create_link_from_entity(entity)

        request.route_path.assert_called_once_with(
            "get_subject", _query={"person_uuid": str(entity_id)}
        )

    def test_create_link_from_entity_organization(self):
        request = mock.MagicMock()
        entity_id = uuid4()

        entity = _create_mock_entity("organization", entity_id)

        ov = custom_object.CustomObjectViews(context="test", request=request)

        ov.create_link_from_entity(entity)

        request.route_path.assert_called_once_with(
            "get_subject", _query={"organization_uuid": str(entity_id)}
        )

    def test_create_link_from_entity_persistent_object_type(self):
        request = mock.MagicMock()
        entity_id = uuid4()

        entity = _create_mock_entity("persistent_object_type", entity_id)

        ov = custom_object.CustomObjectViews(context="test", request=request)

        ov.create_link_from_entity(entity)

        request.route_path.assert_called_once_with(
            "get_persistent_custom_object_type",
            _query={"uuid": str(entity_id)},
        )

    def test_create_link_from_entity_case_type(self):
        request = mock.MagicMock()
        entity_id = uuid4()

        entity = _create_mock_entity("case_type", entity_id)

        sv = search_result.SearchResultViews(context="test", request=request)

        sv.create_link_from_entity(entity)

        request.route_path.assert_called_once_with(
            "get_case_type_active_version",
            _query={"case_type_uuid": str(entity_id)},
        )

    def test_create_link_from_entity_case_type_version(self):
        request = mock.MagicMock()
        entity_id = uuid4()

        entity = _create_mock_entity("case_type_version", entity_id)

        sv = case.CaseViews(context="test", request=request)

        sv.create_link_from_entity(entity)

        request.route_path.assert_called_once_with(
            "get_case_type_version",
            _query={"version_uuid": str(entity_id)},
        )

# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from datetime import datetime
from unittest import mock
from uuid import uuid4
from zsnl_case_management_http.views.handlers import (
    CaseRelationHandler,
    CaseSummaryHandler,
    CasetypeHandler,
    DateTimeHandler,
    EmployeeHandler,
    OrganizationHandler,
    PersonHandler,
    SubjectRelationHandler,
    UUIDHandler,
)
from zsnl_domains.case_management.entities import (
    CaseTypeVersionEntity,
    Employee,
    Organization,
    Person,
    SubjectRelation,
)
from zsnl_domains.case_management.entities.case_type_version import (
    CaseTypeMetaData,
    CaseTypeSettings,
)
from zsnl_domains.case_management.entities.employee import (
    RelatedDepartment,
    RelatedRole,
)
from zsnl_domains.case_management.entities.subject_relation import (
    SubjectRelationRelatedCase,
    SubjectRelationRelatedSubject,
)


class TestDateTimeHandler:
    def test_flatten(self):
        handler = DateTimeHandler(None)
        date = datetime(year=2019, month=7, day=14)
        result = handler.flatten(date, None)
        assert result == "2019-07-14T00:00:00"


class TestUuidHandler:
    def test_flatten(self):
        handler = UUIDHandler(None)
        uuid_obj = uuid4()
        result = handler.flatten(uuid_obj, None)
        assert result == str(uuid_obj)


class TestCasetypeHandler:
    def test_format_meta_data(self):
        metadata = CaseTypeMetaData(
            may_postpone="Ja",
            may_extend="Nee",
            possibility_for_objection_and_appeal="Ja",
            publication="Nee",
            penalty_law="Nee",
            bag="Ja",
            lex_silencio_positivo="Ja",
            wkpb_applies="Ja",
            legal_basis="test text",
            process_description="process_description",
        )

        handler = CasetypeHandler(None)

        res = handler.format_metadata(metadata)

        assert (
            res.items()
            >= {
                "may_postpone": True,
                "may_extend": False,
                "possibility_for_objection_and_appeal": True,
                "publication": False,
                "penalty_law": False,
                "bag": True,
                "lex_silencio_positivo": True,
                "wkpb_applies": True,
                "legal_basis": "test text",
            }.items()
        )

    def test_format_settings(self):
        settings = CaseTypeSettings(
            allow_reuse_casedata=1,
            check_acl_on_allocation=None,
            enable_webform="1",
            text_public_confirmation_message="test text",
            payment={},
        )

        handler = CasetypeHandler(None)
        res = handler.format_settings(settings)

        assert (
            res.items()
            > {
                "allow_reuse_casedata": True,
                "check_acl_on_allocation": False,
                "enable_webform": True,
                "text_public_confirmation_message": "test text",
            }.items()
        )

    def test_format_catalog_folder(self):
        folder_uuid = uuid4()
        folder = mock.MagicMock()
        folder.configure_mock(name="test", uuid=folder_uuid)
        handler = CasetypeHandler(None)
        res = handler.format_catalog_folder(folder)

        assert res["data"] == {
            "id": str(folder_uuid),
            "type": "folder",
            "meta": {"name": "test"},
        }

    def test_format_preset_assignee(self):
        assignee_uuid = uuid4()
        preset_assignee = mock.MagicMock()
        preset_assignee.configure_mock(
            entity_type="employee",
            entity_meta_summary="beheerder",
            uuid=assignee_uuid,
        )

        handler = CasetypeHandler(None)
        res = handler.format_related_contact(preset_assignee)
        assert res == {
            "data": {
                "id": str(assignee_uuid),
                "type": "employee",
                "meta": {"name": "beheerder"},
            }
        }

    def test_flatten(self):
        handler = CasetypeHandler(None)
        case_type_uuid = uuid4()
        department_id = uuid4()
        role_id = uuid4()
        preset_assignee_uuid = uuid4()
        preset_requestor_uuid = uuid4()
        case_type = CaseTypeVersionEntity.parse_obj(
            {
                "id": 10,
                "uuid": case_type_uuid,
                "case_type_uuid": uuid4(),
                "version": 3,
                "name": "Parking permit",
                "description": "Parking permit 2019",
                "identification": "prk-2019",
                "tags": "parking",
                "case_summary": "Information",
                "case_public_summary": "Information external",
                "active": True,
                "requestor": {
                    "type_of_requestors": ["natuurlijk_persoon", "bedrijf"],
                    "use_for_correspondence": True,
                },
                "catalog_folder": {
                    "uuid": uuid4(),
                    "type": "folder",
                    "name": "folder xyz",
                },
                "metadata": {
                    "may_postpone": "Ja",
                    "may_extend": "Nee",
                    "possibility_for_objection_and_appeal": "Ja",
                    "publication": "Nee",
                    "penalty_law": "Nee",
                    "bag": "Ja",
                    "lex_silencio_positivo": "Ja",
                    "wkpb_applies": "Ja",
                    "legal_basis": "test text",
                    "process_description": "Ja",
                },
                "phases": [
                    {
                        "milestone": 1,
                        "name": "Registration",
                        "phase": "Registration",
                        "allocation": {
                            "department": {
                                "uuid": department_id,
                                "name": "Development",
                            },
                            "role": {"uuid": role_id, "name": "Development"},
                        },
                        "cases": [],
                        "custom_fields": [],
                        "documents": [],
                    }
                ],
                "settings": {
                    "require_phonenumber_on_webform": 1,
                    "require_mobilenumber_on_webform": 1,
                    "disable_captcha_for_predefined_requestor": 1,
                    "show_confidentiality": 1,
                    "enable_subject_relations_on_form": 1,
                    "open_case_on_create": 1,
                    "enable_allocation_on_form": 1,
                    "disable_pip_for_requestor": 1,
                    "payment": {"webform": {"amount": 1}},
                },
                "terms": {
                    "lead_time_legal": {"type": "kalenderdagen", "value": 12},
                    "lead_time_service": {
                        "type": "kalenderdagen",
                        "value": 10,
                    },
                },
                "initiator_source": "extern",
                "initiator_type": "aangaan",
                "is_eligible_for_case_creation": True,
                "preset_assignee": {
                    "uuid": preset_assignee_uuid,
                    "entity_meta_summary": "Admin",
                },
                "preset_requestor": {
                    "entity_type": "person",
                    "entity_id": preset_requestor_uuid,
                    "uuid": preset_requestor_uuid,
                    "entity_meta_summary": "Test Person",
                },
                "case_type_results": [],
                "created": "2019-07-14T00:00:00",
                "last_modified": "2019-07-15T00:00:00",
                "deleted": None,
            }
        )
        result = handler.flatten(case_type, None)
        assert result["type"] == "case_type"
        assert result["id"] == str(case_type_uuid)
        assert result["meta"]["created"] == datetime(2019, 7, 14, 0, 0)
        assert result["meta"]["last_modified"] == datetime(2019, 7, 15, 0, 0)
        assert result["meta"]["summary"] == "Parking permit"

        assert result["relationships"]["catalog_folder"] == {
            "data": {
                "id": str(case_type.catalog_folder.uuid),
                "type": "folder",
                "meta": {"name": "folder xyz"},
            }
        }
        assert result["relationships"]["preset_assignee"] == {
            "data": {
                "id": str(preset_assignee_uuid),
                "type": "employee",
                "meta": {"name": "Admin"},
            }
        }
        assert result["relationships"]["preset_requestor"] == {
            "data": {
                "id": str(preset_requestor_uuid),
                "type": "person",
                "meta": {"name": "Test Person"},
            }
        }

        assert result["attributes"]["name"] == "Parking permit"

        assert result["attributes"]["terms"]["lead_time_legal"] == {
            "type": "kalenderdagen",
            "value": "12",
        }
        assert result["attributes"]["terms"]["lead_time_service"] == {
            "type": "kalenderdagen",
            "value": "10",
        }

        assert result["attributes"]["phases"][0]["milestone"] == 1
        assert result["attributes"]["phases"][0]["name"] == "Registration"

        assert (
            result["attributes"]["phases"][0]["allocation"][
                "department"
            ].items()
            > {
                "description": "",
                "uuid": str(department_id),
                "name": "Development",
            }.items()
        )


class TestOrganizationHandler:
    def test_flatten(self):
        handler = OrganizationHandler(None)
        organization_uuid = uuid4()
        organization = Organization(
            uuid=organization_uuid,
            type="organization",
            source="Zaaksysteem",
            name="Test",
            coc_number="12341241",
            coc_location_number=1,
            date_founded=datetime(2020, 5, 5),
            date_registered=datetime(2020, 5, 5),
            date_ceased=None,
            rsin="1234",
            oin="123",
            main_activity={"code": "1234", "description": "activity"},
            secondary_activities=[],
            organization_type="Coöperatie",
            location_address={
                "street": "cirres strrat",
                "zipcode": "1234RT",
                "street_number": 25,
                "street_number_letter": "A",
                "street_number_suffix": None,
                "city": "corre woonplaats",
                "country": "Nederland",
            },
            correspondence_address={
                "street": "cirres strrat",
                "zipcode": "1234RT",
                "street_number": 25,
                "street_number_letter": "B",
                "street_number_suffix": None,
                "city": "corre woonplaats",
                "country": "Nederland",
            },
            contact_information={
                "email": "mail@mail.com",
                "phone_number": "0666666",
                "mobile_number": "994499449944",
                "internal_note": "note",
            },
            related_custom_object=None,
            has_valid_address=True,
            authenticated=True,
        )
        result = handler.flatten(organization, None)
        assert result["type"] == "organization"
        assert result["id"] == str(organization_uuid)

        assert result["attributes"]["name"] == "Test"
        assert result["attributes"]["coc_number"] == "12341241"
        assert result["attributes"]["coc_location_number"] == 1
        assert result["attributes"]["organization_type"] == "Coöperatie"
        assert result["attributes"]["has_valid_address"] is True

        assert result["attributes"]["location_address"] == {
            "street": "cirres strrat",
            "zipcode": "1234RT",
            "street_number": 25,
            "street_number_letter": "A",
            "street_number_suffix": None,
            "city": "corre woonplaats",
            "country": "Nederland",
            "country_code": 6030,
            "is_foreign": False,
            "geo_lat_long": None,
        }

        assert result["attributes"]["has_correspondence_address"] is True
        assert result["attributes"]["correspondence_address"] == {
            "street": "cirres strrat",
            "zipcode": "1234RT",
            "street_number": 25,
            "street_number_letter": "B",
            "street_number_suffix": None,
            "city": "corre woonplaats",
            "country": "Nederland",
            "is_foreign": False,
            "country_code": 6030,
            "geo_lat_long": None,
        }

        assert result["attributes"]["contact_information"] == {
            "email": "mail@mail.com",
            "phone_number": "0666666",
            "mobile_number": "994499449944",
            "internal_note": "note",
            "preferred_contact_channel": "pip",
            "is_anonymous_contact": False,
        }


class TestPersonHandler:
    def test_flatten(self):
        handler = PersonHandler(None)
        person_uuid = uuid4()
        person = Person(
            uuid=person_uuid,
            type="person",
            authenticated=True,
            first_names="Anna",
            insertions="M",
            family_name="Moen",
            noble_title="Miss",
            surname="Anna Moen",
            name="A. Moen",
            date_of_birth=datetime(year=2019, month=5, day=5),
            date_of_death=None,
            gender="F",
            inside_municipality=True,
            residence_address={
                "street": "cirres strrat",
                "zipcode": "1234RT",
                "street_number": 25,
                "street_number_letter": "X",
                "city": "corre woonplaats",
                "country": "Nederland",
                "is_foreign": False,
            },
            correspondence_address={
                "street": "cirres strrat",
                "zipcode": "1234RT",
                "street_number": 25,
                "street_number_letter": "Y",
                "city": "corre woonplaats",
                "country": "Nederland",
                "is_foreign": False,
            },
            contact_information={
                "email": "mail@mail.com",
                "phone_number": "0666666",
                "mobile_number": "994499449944",
                "internal_note": "note",
            },
            related_custom_object_uuid=str(uuid4()),
            has_valid_address=False,
        )
        result = handler.flatten(person, None)
        assert result["type"] == "person"
        assert result["id"] == str(person_uuid)

        assert result["attributes"]["authenticated"] is True
        assert result["attributes"]["first_names"] == "Anna"
        assert result["attributes"]["insertions"] == "M"
        assert result["attributes"]["family_name"] == "Moen"
        assert result["attributes"]["noble_title"] == "Miss"
        assert result["attributes"]["surname"] == "Anna Moen"
        assert result["attributes"]["date_of_birth"] == "2019-05-05T00:00:00"
        assert result["attributes"]["date_of_death"] is None
        assert result["attributes"]["gender"] == "F"
        assert result["attributes"]["inside_municipality"] is True
        assert result["attributes"]["has_valid_address"] is False

        assert result["attributes"]["residence_address"] == {
            "street": "cirres strrat",
            "zipcode": "1234RT",
            "street_number": 25,
            "street_number_letter": "X",
            "street_number_suffix": None,
            "city": "corre woonplaats",
            "country": "Nederland",
            "country_code": 6030,
            "is_foreign": False,
            "geo_lat_long": None,
        }

        assert result["attributes"]["has_correspondence_address"] is True
        assert result["attributes"]["correspondence_address"] == {
            "street": "cirres strrat",
            "zipcode": "1234RT",
            "street_number": 25,
            "street_number_letter": "Y",
            "street_number_suffix": None,
            "city": "corre woonplaats",
            "country": "Nederland",
            "country_code": 6030,
            "is_foreign": False,
            "geo_lat_long": None,
        }

        assert result["attributes"]["contact_information"] == {
            "email": "mail@mail.com",
            "phone_number": "0666666",
            "mobile_number": "994499449944",
            "internal_note": "note",
            "preferred_contact_channel": "pip",
            "is_anonymous_contact": False,
        }


class TestEmployeeHandler:
    def test_flatten(self):
        handler = EmployeeHandler(None)
        employee_uuid = uuid4()
        role_uuid = uuid4()
        department_uuid = uuid4()
        employee = Employee(
            id=123,
            uuid=employee_uuid,
            type="employee",
            source="Zaaksysteem",
            status="active",
            name="Admin",
            first_name="ad",
            surname="min",
            roles=[
                RelatedRole(
                    uuid=role_uuid,
                    entity_id=role_uuid,
                    entity_meta_summary="Admin",
                    parent=RelatedDepartment(
                        uuid=department_uuid,
                        entity_id=department_uuid,
                        entity_meta_summary="dep",
                    ),
                )
            ],
            department=RelatedDepartment(
                uuid=department_uuid,
                entity_id=department_uuid,
                entity_meta_summary="Development",
            ),
            contact_information={
                "email": "mail@mail.com",
                "phone_number": "0666666",
                "is_anonymous_contact": True,
            },
            related_custom_object_uuid=str(uuid4()),
        )
        result = handler.flatten(employee, None)
        assert result["type"] == "employee"
        assert result["id"] == str(employee_uuid)

        assert result["attributes"]["source"] == "Zaaksysteem"
        assert result["attributes"]["status"] == "active"
        assert result["attributes"]["first_name"] == "ad"
        assert result["attributes"]["surname"] == "min"
        assert result["attributes"]["has_valid_address"] is False

        assert result["attributes"]["contact_information"] == {
            "email": "mail@mail.com",
            "internal_note": None,
            "mobile_number": None,
            "phone_number": "0666666",
            "preferred_contact_channel": "pip",
            "is_anonymous_contact": True,
        }

        assert result["relationships"]["department"]["data"] == {
            "type": "department",
            "id": str(department_uuid),
            "meta": {"name": "Development"},
        }

        assert result["relationships"]["roles"]["data"] == [
            {"type": "role", "id": str(role_uuid), "meta": {"name": "Admin"}}
        ]


class TestSubjectRelationHandler:
    def test_format_case(self):
        case_uuid = uuid4()
        case = SubjectRelationRelatedCase(type="case", id=str(case_uuid))
        handler = SubjectRelationHandler(None)
        res = handler.format_case(case)

        assert res == {
            "data": {"id": str(case_uuid), "type": "case"},
            "links": {
                "related": f"/api/v2/cm/case/get_case?case_uuid={case_uuid}"
            },
        }

    def test_format_subject(self):
        subject_uuid = uuid4()
        subject_type = "organization"
        subject = SubjectRelationRelatedSubject(
            type=subject_type,
            id=subject_uuid,
            name="test_org",
        )
        handler = SubjectRelationHandler(None)
        res = handler.format_subject(subject)

        assert res == {
            "data": {
                "id": str(subject_uuid),
                "type": subject_type,
                "meta": {"display_name": "test_org"},
            },
            "links": {
                "related": f"/api/v2/cm/subject/get_subject?{subject_type}_uuid={subject_uuid}"
            },
        }

    def test_flatten(self):
        handler = SubjectRelationHandler(None)
        subject_relation_uuid = uuid4()
        subject_uuid = uuid4()
        subject_type = "organization"
        case_uuid = uuid4()
        subject_relation = SubjectRelation(
            uuid=subject_relation_uuid,
            role="Behanldlaar",
            magic_string_prefix="behandlaar1",
            authorized=True,
            case=SubjectRelationRelatedCase(type="case", id=str(case_uuid)),
            subject=SubjectRelationRelatedSubject(
                type=subject_type,
                id=subject_uuid,
                name="test_org",
            ),
            is_preset_client=True,
            type="subject_relation",
        )

        result = handler.flatten(subject_relation, None)
        assert result["type"] == "subject_relation"
        assert result["id"] == str(subject_relation_uuid)

        assert result["relationships"]["case"] == {
            "data": {"id": str(subject_relation.case.id), "type": "case"},
            "links": {
                "related": f"/api/v2/cm/case/get_case?case_uuid={case_uuid}"
            },
        }
        assert result["relationships"]["subject"] == {
            "data": {
                "id": str(subject_relation.subject.id),
                "type": subject_relation.subject.type,
                "meta": {"display_name": subject_relation.subject.name},
            },
            "links": {
                "related": f"/api/v2/cm/subject/get_subject?{subject_type}_uuid={subject_uuid}"
            },
        }

        assert result["attributes"]["authorized"] is True
        assert result["attributes"]["magic_string_prefix"] == "behandlaar1"
        assert result["attributes"]["role"] == "Behanldlaar"
        assert result["attributes"]["is_preset_client"] is True

        subject_type = "employee"
        subject_relation = SubjectRelation(
            uuid=subject_relation_uuid,
            role="Aanvrager",
            magic_string_prefix="aanvrager",
            authorized=True,
            case={"id": case_uuid, "type": "case"},
            subject={
                "type": subject_type,
                "id": subject_uuid,
                "name": "beheerder",
            },
            permission="write",
        )
        result = handler.flatten(subject_relation, None)

        assert result["attributes"]["permission"] == "write"
        assert result["attributes"]["magic_string_prefix"] == "aanvrager"
        assert result["attributes"]["role"] == "Aanvrager"


class TestCaseRelationHandler:
    def test_flatten(self):
        case_relation = mock.MagicMock()
        case_relation.relation_type = "related_case"
        case_relation.uuid = "fake_uuid"
        case_relation.blocks_deletion = "fake_block_deletion"
        case_relation.owner_uuid = "fake_owner_uuid"
        case_relation.object1_uuid = "fake_object1_uuid"
        case_relation.object2_uuid = "fake_object2_uuid"
        case_relation.summary1 = "fake_summary1"
        case_relation.summary2 = "fake_summary2"
        case_relation.order_seq_a = 1
        case_relation.order_seq_b = 2
        case_relation.relationship_type1 = "plain"
        case_relation.relationship_type2 = "continuation"
        case_relation.current_case_uuid = "fake_object2_uuid"

        handler = CaseRelationHandler(None)

        assert handler.flatten(case_relation, None) == {
            "type": "case_relation",
            "id": case_relation.uuid,
            "attributes": {
                "blocks_deletion": case_relation.blocks_deletion,
                "owner_uuid": case_relation.owner_uuid,
                "sequence_number": 1,
                "relation_type": "related_case",
            },
            "relationships": {
                "this_case": {
                    "data": {
                        "type": "case_summary",
                        "id": case_relation.object2_uuid,
                    },
                    "meta": {
                        "relation_type": case_relation.relationship_type2
                    },
                },
                "other_case": {
                    "data": {
                        "type": "case_summary",
                        "id": case_relation.object1_uuid,
                    },
                    "meta": {
                        "relation_type": case_relation.relationship_type1
                    },
                },
            },
        }

        case_relation.current_case_uuid = "fake_object1_uuid"
        assert handler.flatten(case_relation, None) == {
            "type": "case_relation",
            "id": case_relation.uuid,
            "attributes": {
                "blocks_deletion": case_relation.blocks_deletion,
                "owner_uuid": case_relation.owner_uuid,
                "sequence_number": 2,
                "relation_type": "related_case",
            },
            "relationships": {
                "this_case": {
                    "data": {
                        "type": "case_summary",
                        "id": case_relation.object1_uuid,
                    },
                    "meta": {
                        "relation_type": case_relation.relationship_type1
                    },
                },
                "other_case": {
                    "data": {
                        "type": "case_summary",
                        "id": case_relation.object2_uuid,
                    },
                    "meta": {
                        "relation_type": case_relation.relationship_type2
                    },
                },
            },
        }


class TestCaseSummaryHandler:
    def test_flatten(self):
        case_summary = mock.Mock()
        case_summary.uuid = uuid4()
        case_summary.type = "case_summary"
        case_summary.summary = "summary"
        case_summary.progress_status = 0.5
        case_summary.result = "resultaat"
        case_summary.casetype_title = "title"
        case_summary.number = 1
        case_summary.assignee = mock.Mock()
        case_summary.assignee.configure_mock(uuid=uuid4(), name="beheerder")

        handler = CaseSummaryHandler(None)

        assignee_uuid = str(case_summary.assignee.uuid)
        case_uuid = str(case_summary.uuid)

        assert handler.flatten(case_summary, None) == {
            "type": "case_summary",
            "id": str(case_summary.uuid),
            "attributes": {
                "number": case_summary.number,
                "progress_status": case_summary.progress_status,
                "result": case_summary.result,
                "summary": case_summary.summary,
                "casetype_title": case_summary.casetype_title,
            },
            "relationships": {
                "assignee": {
                    "data": {"type": "subject", "id": assignee_uuid},
                    "meta": {"display_name": "beheerder"},
                    "links": {
                        "self": f"/api/v2/cm/subject/get_subject?employee_uuid={assignee_uuid}"
                    },
                }
            },
            "links": {
                "self": f"/api/v2/cm/case/get_case?case_uuid={case_uuid}"
            },
        }

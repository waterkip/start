# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import copy
import importlib
import pytest
from .test_zsnl_case_management_http import mock_protected_route
from pyramid.httpexceptions import HTTPBadRequest
from unittest import mock
from uuid import uuid4
from zsnl_case_management_http.views import tasks
from zsnl_domains.case_management.entities import task as entities


class TestTaskHandlers:
    def setup_method(self):
        with mock.patch(
            "minty_pyramid.session_manager.protected_route",
            mock_protected_route,
        ):
            from zsnl_case_management_http.views import tasks

            importlib.reload(tasks)

            self.tasks = tasks
            self.task = entities.Task(
                uuid=uuid4(),
                title="some_title",
                description="some descript",
                due_date="2019-10-21",
                completed=False,
                user_defined=True,
                assignee=entities.TaskAssigneeData(
                    type="employee",
                    id=uuid4(),
                    display_name="Ad Min",
                ),
                case={
                    "uuid": uuid4(),
                    "status": "open",
                    "milestone": 2,
                    "id": 2002,
                },
                case_type={"uuid": uuid4(), "title": "Case Type"},
                phase=2,
                department={"uuid": uuid4(), "display_name": "Backoffice"},
                product_code=None,
                dso_action_request=False,
            )

    def test_get_task_list_exceptions(self):
        mock_request = mock.MagicMock()
        query_instance_mock = mock.MagicMock()

        mock_request.get_query_instance.return_value = query_instance_mock
        query_instance_mock.get_task_list.return_value = []
        page = "1"
        page_size = "10"

        mock_request.params = {
            "page": page,
            "page_size": page_size,
        }

        mock_request.current_route_path.return_value = (
            "/api/v2/cm/task/get_task_list?page=1&page_size=10"
        )

        with pytest.raises(HTTPBadRequest):
            phase = "one"
            mock_request.params = {
                "filter[relationships.case.id]": str(uuid4()),
                "filter[attributes.phase]": phase,
            }
            self.tasks.get_task_list(request=mock_request)

        with pytest.raises(HTTPBadRequest):
            mock_request.params = {
                "filter[relationships.case.number]": "1,2,a"
            }
            self.tasks.get_task_list(request=mock_request)

        with pytest.raises(HTTPBadRequest):
            mock_request.params = {"filter[attributes.completed]": "not true"}
            self.tasks.get_task_list(request=mock_request)

        with pytest.raises(HTTPBadRequest):
            mock_request.params = {"filter[attributes.title.contains]": None}
            self.tasks.get_task_list(request=mock_request)

    def test_get_task_list_no_keywords(self):
        mock_request = mock.MagicMock()
        query_instance_mock = mock.MagicMock()

        mock_request.get_query_instance.return_value = query_instance_mock
        query_instance_mock.get_task_list.return_value = []
        page = "1"
        page_size = "10"

        mock_request.params = {
            "page": page,
            "page_size": page_size,
        }

        mock_request.current_route_path.return_value = (
            "/api/v2/cm/task/get_task_list?page=1&page_size=10"
        )

        ret = self.tasks.get_task_list(request=mock_request)

        assert ret == {
            "meta": {"api_version": 2},
            "data": [],
            "links": {
                "self": {
                    "href": "/api/v2/cm/task/get_task_list?page=1&page_size=10"
                }
            },
        }

    def test_get_task_list(self):
        mock_request = mock.MagicMock()
        query_instance_mock = mock.MagicMock()

        case_uuid = str(self.task.case["uuid"])
        assignee_uuid = str(self.task.assignee.id)
        phase = "2"
        completed = "true"
        department_uuid = str(self.task.department["uuid"])
        case_type_uuid = str(self.task.case_type["uuid"])
        mock_request.get_query_instance.return_value = query_instance_mock
        query_instance_mock.get_task_list.return_value = [self.task]
        page = "1"
        page_size = "10"

        mock_request.params = {
            "filter[relationships.case.id]": case_uuid,
            "filter[relationships.case.number]": "1,2,3",
            "filter[attributes.phase]": phase,
            "filter[attributes.completed]": completed,
            "filter[relationships.assignee.id]": assignee_uuid,
            "filter[relationships.case_type.id]": case_type_uuid,
            "filter[relationships.department.id]": department_uuid,
            "filter[attributes.title.contains]": "foo,bar",
            "keyword": "some_keyword",
            "page": page,
            "page_size": page_size,
        }

        mock_request.current_route_path.return_value = (
            "/api/v2/cm/task/get_task_list"
            f"?filter[relationships.case.id]={case_uuid}&filter[attributes.phase]=2&filter[attributes.completed]=true&filter[relationships.assignee.id]={assignee_uuid}&filter[relationships.department.id]={department_uuid}&page=1&page_size=10"
        )

        ret = self.tasks.get_task_list(request=mock_request)

        assert ret == {
            "data": [
                {
                    "attributes": {
                        "completed": False,
                        "description": "some descript",
                        "due_date": "2019-10-21",
                        "title": "some_title",
                        "phase": 2,
                    },
                    "id": f"{self.task.uuid}",
                    "product_code": "",
                    "dso_action_request": False,
                    "meta": {"can_set_completion": True, "is_editable": True},
                    "relationships": {
                        "assignee": {
                            "data": {
                                "id": str(self.task.assignee.id),
                                "type": "employee",
                                "meta": {"display_name": "Ad Min"},
                            }
                        },
                        "case": {
                            "data": {"id": f"{case_uuid}", "type": "case"},
                            "meta": {"phase": 2, "display_number": 2002},
                        },
                        "case_type": {
                            "data": {
                                "id": f"{case_type_uuid}",
                                "type": "case_type",
                            },
                            "meta": {"display_name": "Case Type"},
                        },
                        "department": {
                            "data": {
                                "id": f"{self.task.department['uuid']}",
                                "type": "department",
                            },
                            "meta": {"display_name": "Backoffice"},
                        },
                    },
                    "type": "task",
                }
            ],
            "links": {
                "self": {
                    "href": f"/api/v2/cm/task/get_task_list?filter[relationships.case.id]={case_uuid}&filter[attributes.phase]=2&filter[attributes.completed]=true&filter[relationships.assignee.id]={assignee_uuid}&filter[relationships.department.id]={department_uuid}&page=1&page_size=10"
                }
            },
            "meta": {"api_version": 2},
        }

        query_instance_mock.get_task_list.assert_called_once_with(
            case_uuids=[case_uuid],
            case_ids=[1, 2, 3],
            case_type_uuids=[case_type_uuid],
            phase=int(phase),
            completed=True,
            assignee_uuids=[assignee_uuid],
            department_uuids=[department_uuid],
            title_words=["foo", "bar"],
            keyword="some_keyword",
            page=int(page),
            page_size=int(page_size),
        )

    def test_create_task_exceptions(self):
        mock_request = mock.MagicMock()
        command_instance_mock = mock.MagicMock()

        case_uuid = str(uuid4())
        task_uuid = str(uuid4())
        title = "some_title"
        phase = 2

        mock_request.get_command_instance.return_value = command_instance_mock
        mock_request.json_body = {
            "case_uuid": case_uuid,
            "phase": phase,
            "title": title,
            # "task_uuid": task_uuid,
        }
        with pytest.raises(HTTPBadRequest):
            self.tasks.create_task(request=mock_request)
        with pytest.raises(HTTPBadRequest):
            phase = "one"
            mock_request.json_body = {
                "case_uuid": case_uuid,
                "phase": phase,
                "title": title,
                "task_uuid": task_uuid,
            }
            self.tasks.create_task(request=mock_request)

    def test_create_task(self):
        mock_request = mock.MagicMock()
        command_instance_mock = mock.MagicMock()

        case_uuid = str(uuid4())
        task_uuid = str(uuid4())
        title = "some_title"
        phase = 2

        mock_request.get_command_instance.return_value = command_instance_mock
        mock_request.json_body = {
            "case_uuid": case_uuid,
            "phase": phase,
            "title": title,
            "task_uuid": task_uuid,
        }
        self.tasks.create_task(request=mock_request)

        command_instance_mock.create_task.assert_called_once_with(
            case_uuid=case_uuid, task_uuid=task_uuid, phase=phase, title=title
        )

        ret = self.tasks.create_task(request=mock_request)
        assert ret == {"data": {"success": True}}

    def test_delete_task_exception(self):
        mock_request = mock.MagicMock()
        command_instance_mock = mock.MagicMock()

        # task_uuid = str(uuid4())

        mock_request.get_command_instance.return_value = command_instance_mock
        mock_request.json_body = {
            # "task_uuid": task_uuid,
        }
        with pytest.raises(HTTPBadRequest):
            self.tasks.delete_task(request=mock_request)

    def test_delete_task(self):
        mock_request = mock.MagicMock()
        command_instance_mock = mock.MagicMock()

        task_uuid = str(uuid4())

        mock_request.get_command_instance.return_value = command_instance_mock
        mock_request.json_body = {"task_uuid": task_uuid}
        res = self.tasks.delete_task(request=mock_request)

        command_instance_mock.delete_task.assert_called_once_with(
            task_uuid=task_uuid
        )
        assert res == {"data": {"success": True}}

    def test_update_task_uuid_required(self):
        mock_request = mock.MagicMock()
        command_instance_mock = mock.MagicMock()

        title = "some_title"
        mock_request.get_command_instance.return_value = command_instance_mock
        mock_request.json_body = {
            "title": title,
        }
        with pytest.raises(HTTPBadRequest):
            self.tasks.update_task(request=mock_request)

    def test_update_task_exception_on_off_required_fields(self):
        mock_request = mock.MagicMock()
        command_instance_mock = mock.MagicMock()

        task_uuid = str(uuid4())

        mock_request.get_command_instance.return_value = command_instance_mock
        mock_request.json_body = {
            "task_uuid": task_uuid,
        }
        with pytest.raises(HTTPBadRequest):
            self.tasks.update_task(request=mock_request)

    def test_update_task(self):
        mock_request = mock.MagicMock()
        command_instance_mock = mock.MagicMock()

        task_uuid = str(uuid4())
        description = "some descr"
        title = "some_title"
        due_date = "2019-10-19"
        assignee = str(uuid4())
        product_code = "code1"
        dso_action_request = False

        mock_request.get_command_instance.return_value = command_instance_mock
        mock_request.json_body = {
            "title": title,
            "task_uuid": task_uuid,
            "description": description,
            "due_date": due_date,
            "assignee": assignee,
            "product_code": product_code,
            "dso_action_request": dso_action_request,
        }
        res = self.tasks.update_task(request=mock_request)

        command_instance_mock.update_task.assert_called_once_with(
            task_uuid=task_uuid,
            title=title,
            description=description,
            assignee=assignee,
            due_date=due_date,
            product_code=product_code,
            dso_action_request=dso_action_request,
        )

        assert res == {"data": {"success": True}}

    def test_set_completion_on_task_exception(self):
        mock_request = mock.MagicMock()
        command_instance_mock = mock.MagicMock()

        task_uuid = str(uuid4())
        # completed = True

        mock_request.get_command_instance.return_value = command_instance_mock
        mock_request.json_body = {
            # "completed": completed,
            "task_uuid": task_uuid
        }
        with pytest.raises(HTTPBadRequest):
            self.tasks.set_task_completion(request=mock_request)

    def test_set_completion_on_task(self):
        mock_request = mock.MagicMock()
        command_instance_mock = mock.MagicMock()

        task_uuid = str(uuid4())
        completed = True

        mock_request.get_command_instance.return_value = command_instance_mock
        mock_request.json_body = {
            "completed": completed,
            "task_uuid": task_uuid,
        }
        res = self.tasks.set_task_completion(request=mock_request)

        command_instance_mock.set_completion_on_task.assert_called_once_with(
            task_uuid=task_uuid, completed=completed
        )

        assert res == {"data": {"success": True}}


class TestTaskSerializers:
    def setup_method(self):
        self.task = entities.Task(
            uuid=uuid4(),
            title="some_title",
            description="some descript",
            due_date="2019-10-21",
            completed=False,
            user_defined=True,
            assignee=entities.TaskAssigneeData(
                type="employee",
                id=uuid4(),
                display_name="Ad Min",
            ),
            case={
                "uuid": uuid4(),
                "status": "open",
                "milestone": 2,
                "id": 2002,
            },
            case_type={"uuid": uuid4(), "title": "Case Type"},
            department={"uuid": uuid4(), "display_name": "Backoffice"},
            phase=2,
            product_code=None,
            dso_action_request=False,
        )

    def test_task_serializer_list(self):
        task = copy.deepcopy(self.task)
        ser = tasks.task_list_serializer(task=task)
        assert ser == {
            "attributes": {
                "completed": False,
                "description": "some descript",
                "due_date": "2019-10-21",
                "title": "some_title",
                "phase": 2,
            },
            "id": str(self.task.uuid),
            "product_code": "",
            "dso_action_request": False,
            "meta": {"can_set_completion": True, "is_editable": True},
            "relationships": {
                "assignee": {
                    "data": {
                        "meta": {"display_name": "Ad Min"},
                        "id": str(self.task.assignee.id),
                        "type": "employee",
                    }
                },
                "case": {
                    "data": {
                        "id": str(self.task.case["uuid"]),
                        "type": "case",
                    },
                    "meta": {
                        "phase": 2,
                        "display_number": self.task.case["id"],
                    },
                },
                "case_type": {
                    "data": {
                        "id": str(self.task.case_type["uuid"]),
                        "type": "case_type",
                    },
                    "meta": {"display_name": "Case Type"},
                },
                "department": {
                    "data": {
                        "id": str(self.task.department["uuid"]),
                        "type": "department",
                    },
                    "meta": {"display_name": "Backoffice"},
                },
            },
            "type": "task",
        }

    def test_serialize_assignee(self):
        assignee = entities.TaskAssigneeData(
            id=uuid4(),
            display_name="firstname lastname",
            type="employee",
        )
        assignee_ser = tasks.serialize_assignee(assignee)
        assert assignee_ser == {
            "data": {
                "id": str(assignee.id),
                "type": assignee.type,
                "meta": {"display_name": assignee.display_name},
            }
        }
        assignee2 = None
        assignee_ser2 = tasks.serialize_assignee(assignee2)
        assert assignee_ser2 is None

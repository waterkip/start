# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

package ConvertService::Plugin::Poppler;
use Moo;
use namespace::autoclean;

with 'ConvertService::Plugin';

=head1 NAME

ConvertService::Plugin::Poppler - Convert service plugin using the Poppler
PDF library

=head1 DESCRIPTION

This plugin allows ConvertService to convert PDF files to PNG, either with a
preset size or based on the size declared by the PDF.

If the PDF file has more than one page, the plugin will only convert/thumbnail
the first page.

=cut

use Cairo;
use Cairo::GObject;
use File::Slurp qw(read_file);
use File::Temp;
use Function::Parameters qw(:strict);
use List::Util qw(any);
use Poppler;
use Scalar::Util qw(looks_like_number);

my @SUPPORTED_SOURCE_TYPES = qw(
    application/pdf
);

my @SUPPORTED_TARGET_TYPES = qw(
    image/png
);

method _create_cairo_context($width, $height, $pdf_width, $pdf_height) {
    my $scale_factor = $height / $pdf_height;

    if ($pdf_width * $scale_factor > $width) {
        $scale_factor = $width / $pdf_width;
        $height = $pdf_height * $scale_factor;
    }
    else {
        $width = $pdf_width * $scale_factor;
    }

    my $surface = Cairo::ImageSurface->create('argb32', $width, $height);
    my $cairo = Cairo::Context->create($surface);
    $cairo->scale($scale_factor, $scale_factor);

    return $cairo;
}

=head2 name

Return the short name of this plugin: C<Poppler>.

=cut

method name() { return 'Poppler' }

=head2 can_convert

Returns a true value if this plugin can convert files with a MIME type of
C<$from> to C<$to>.

=cut

method can_convert(:$from, :$to) {
    return 1 if     any { $_ eq $from } @SUPPORTED_SOURCE_TYPES
                and any { $_ eq $to } @SUPPORTED_TARGET_TYPES;
    return 0;
}

=head2 convert(:$source_file, :$from_type, :$to_type, :$options)

Convert C<$source_file> to C<$to_type>, using the specified C<$options> for the conversion.

C<$from_type> is not used by this plugin.

C<$options> should be a reference to a hash. This plugin understands the
following options:

=over

=item * width

The width, in pixels, of the target image.

=item * height

The height, in pixels, of the target image.

=back

The image will be scaled in a way that keeps the aspect ratio the same.

=cut

method convert(:$source_file, :$from_type, :$to_type, :$options) {
    my $pdf = Poppler::Document->new_from_file($source_file);

    # Making a thumbnail - only use the first page
    my $page = $pdf->get_page(0);

    my ($pdf_width, $pdf_height) = $page->get_size;

    my ($width, $height);
    if ($options->{width} && $options->{height}) {
        $height = looks_like_number($options->{height}) ? int($options->{height}) : undef;
        $width  = looks_like_number($options->{width})  ? int($options->{width})  : undef;
    }
    else {
        # Default to the PDF dimensions, except in pixels
        $height = int($pdf_height);
        $width  = int($pdf_width);
    }

    my $cairo = $self->_create_cairo_context($width, $height, $pdf_width, $pdf_height);

    $page->render($cairo);

    # Make the transparent bits ("the page") white
    $cairo->set_operator('dest-over');
    $cairo->set_source_rgb(1, 1, 1);
    $cairo->paint();

    my $destination_file = File::Temp->new();
    my $surface = $cairo->get_target();
    $surface->write_to_png("$destination_file");

    my $result = read_file($destination_file, binmode => ':raw');
    return $result;
}

1;

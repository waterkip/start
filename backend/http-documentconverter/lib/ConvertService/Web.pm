# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

package ConvertService::Web;
use Log::Report ();
use Dancer2;
use Dancer2::Plugin::LogReport 'ConverterService', import => [];
use Net::Statsd;

=head1 NAME

ConvertService::Web - Dancer2 web app wrapper around ConvertService

=head1 DESCRIPTION

This is a L<Dancer2> application that's a thin wrapper around the
L<ConvertService> class.

=head1 ENDPOINTS

=cut

use File::Temp;
use MIME::Base64 qw(encode_base64 decode_base64);
use Try::Tiny;

use ConvertService;

set serializer => 'JSON';

my $startup = time;

my $SILO_ID = $ENV{ZS_SILO_ID} // 'unknown';
my $GAUGE_METRIC = "document_converter.silo.$SILO_ID.all.active_threads";
my $COUNTER_METRIC = "document_converter.silo.$SILO_ID.all.total_requests";

$Net::Statsd::HOST = $ENV{STATSD_HOST} if defined $ENV{STATSD_HOST};
$Net::Statsd::PORT = $ENV{STATSD_PORT} if defined $ENV{STATSD_PORT};

hook 'before' => sub {
    #debug("Increasing $GAUGE_METRIC on $Net::Statsd::HOST / $Net::Statsd::PORT");
    Net::Statsd::gauge($GAUGE_METRIC, "+1");
    Net::Statsd::increment($COUNTER_METRIC);
};

hook 'after' => sub {
    #debug("Decreasing $GAUGE_METRIC on $Net::Statsd::HOST / $Net::Statsd::PORT");
    Net::Statsd::gauge($GAUGE_METRIC, "-1");
};

=head2 GET /v1/alive_check

Returns an object with the server's uptime, which can be used to check if the
server is alive/online.

=cut

get '/v1/alive_check' => sub {
    return {
        uptime_seconds => time - $startup,
    };
};

=head2 GET /v1/can_convert

Determine whether a can be converted from one format to another.

Expects two query parameters:

=over

=item * from_type

MIME type to convert from

=item * to_type

MIME type to convert to

=back

The call will return a simple data structure:


    {
        can_convert: true
    }

=cut

get '/v1/can_convert' => sub {
    my $from_type = query_parameters->get('from_type') // '';
    my $to_type   = query_parameters->get('to_type') // '';

    my $service = ConvertService->new();
    my $result = $service->can_convert(from_type => $from_type, to_type => $to_type);

    return {
        "can_convert" => $result ? \1 : \0,
    };
};

=head2 POST /v1/convert

Convert a file from one format to another.

Expects the request body to be a JSON message, containing an object
with at least 2 keys:

    {
        "to_type": "image/png",
        "content": "YmFzZTY0LWVuY29kZWQgYmluYXJ5IGltYWdlIGRhdGE=",
        "options": { "width": 800, "height": 600 }
    }

The value of C<to_type> should be the MIME type the C<content> should be
converted to.

C<content> is the base64-encoded content of the source file to convert.

The C<options> key is optional, and contains plugin-specific conversion
options.

Returns a data structure that looks like:

    {
        content: "base64_encoded_content"
    }

If there's an error,

    {
        error: "Error message goes here"
    }

=cut

post '/v1/convert' => sub {
    my $to_type = body_parameters->get('to_type');

    my $temp = File::Temp->new();
    $temp->print( decode_base64(body_parameters->get('content') // '') );
    $temp->close();

    my $options = body_parameters->get('options');
    $options = {} if (ref $options ne 'HASH');

    my $rv = try {
        my $service = ConvertService->new();
        my $result = $service->convert(
            source_file => "$temp",
            to_type     => $to_type,
            options     => $options,
        );

        return { content => encode_base64($result, '') };
    } catch {
        status 500;

        my $msg = "Error during conversion: $_";
        error($msg);
        return { error => $msg };
    };

    return $rv;
};

1;

# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from typing import Final

CASE_PREFIX: Final = "zsnl.v2.zsnl_domains_case_management.Case."
CASE_LEGACY_PREFIX: Final = "zsnl.v2.legacy.Case."
EXPORT_FILE_PREFIX: Final = "zsnl.v2.zsnl_domains_case_management.ExportFile."
JOBS_PREFIX: Final = "zsnl.v2.zsnl_domains_jobs.Job."
SUBJECT_RELATION_PREFIX: Final = (
    "zsnl.v2.zsnl_domains_case_management.SubjectRelation."
)
TIMELINE_EXPORT_PREFIX: Final = (
    "zsnl.v2.zsnl_domains_case_management.TimelineExport."
)
ARCHIVE_EXPORT_PREFIX: Final = (
    "zsnl.v2.zsnl_domains_case_management.ArchiveExport."
)

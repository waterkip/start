setuptools>=69.2.0

../minty
../minty-pyramid

../minty-infra-amqp
../minty-infra-sqlalchemy

python-json-logger

##  Project specific requirements
../domains
../zsnl-pyramid

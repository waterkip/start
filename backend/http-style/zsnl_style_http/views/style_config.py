# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

"""
File contains custom `view` for Entity type `StyleConfiguration`
"""

from minty_pyramid.views.pydantic_entity import JSONAPIEntityView


class StyleConfigViews(JSONAPIEntityView):
    """
    Class represents a custom view for Entity `StyleConfiguration`
    """

    view_mapper = {
        "GET": {
            "get_tenants": {
                "cq": "query",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.configuration",
                "run": "get_tenants",
                "from": {},
            }
        },
    }

# SPDX-FileCopyrightText: 2020 Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

# AUTOGENERATED FILE - No need to edit
# GENERATED AT: 2024-05-31T13:04:23"
# Re-create by running 'generate-routes' command
# Run 'generate-routes --help' for more options


def add_routes(config):
    """Add routes to pyramid application.

    :param config: pyramid config file
    :type config: Configurator
    """
    handlers = [
        {
            "route": "/api/v2/auth/openapi/{filename}",
            "handler": "apidocs_fileserver",
            "method": "GET",
            "view": "zsnl_auth_http.views.apidocs.apidocs_fileserver",
        },
        {
            "route": "/api/v2/auth/auth0/authorization_code_flow/callback",
            "handler": "callback",
            "method": "GET",
            "view": "zsnl_auth_http.views.auth0.callback",
        },
    ]

    for h in handlers:
        config.add_route(
            name=h["handler"], pattern=h["route"], request_method=h["method"]
        )
        config.add_view(
            view=h["view"],
            route_name=h["handler"],
            request_method=h["method"],
        )

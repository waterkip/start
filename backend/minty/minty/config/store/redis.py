# SPDX-FileCopyrightText: 2020 Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import redis
from ..parser import ConfigParserBase
from .base import ConfigStoreBase, ConfigurationNotFound


class RedisStore(ConfigStoreBase):
    def __init__(self, parser: ConfigParserBase, arguments: redis.Redis):
        """Initialize the Redis configuration store

        :param parser: Configuration file/data parser
        :type parser: minty.config.parser.Base
        :param redis: dictionary containing keys/values to configure a Redis
            client instance.
        :type arguments: dict
        """

        self.parser: ConfigParserBase = parser
        self.redis: redis.Redis = arguments

    def retrieve(self, name: str) -> dict:
        """Retrieve configuration from Redis

        :param name: Name of the configuration to retrieve
        :param name: str
        :raises ConfigurationNotFound: if the named configuration can't be found
        :return: The configuration "file" contents
        :rtype: str
        """

        super().retrieve(name)

        timer = self.statsd.get_timer("config_database_read_duration")
        with timer.time():
            config = self.redis.get("saas:instance:" + name)
        self.statsd.get_counter("config_database_read_number").increment()

        if config is None:
            raise ConfigurationNotFound

        parsed = self.parser.parse(config.decode("utf-8"))

        return parsed["instance"]

    def get_instance_hostnames(self) -> list[str]:
        """
        Return a list of all "main" hostnames of instances

        This can be used by tools
        """
        instances = list(self.redis.smembers("saas:instances"))

        # Redis returns bytes
        return [i.decode() for i in instances]

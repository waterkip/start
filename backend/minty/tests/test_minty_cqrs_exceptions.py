# SPDX-FileCopyrightText: 2020 Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.exceptions as cqrs_exc
import pytest


class TestCQRSExceptions:
    def test_cqrs_not_found(self):
        with pytest.raises(cqrs_exc.CQRSException):
            raise cqrs_exc.NotFound
        with pytest.raises(cqrs_exc.NotFound):
            raise cqrs_exc.NotFound

    def test_cqrs_forbidden(self):
        with pytest.raises(cqrs_exc.CQRSException):
            raise cqrs_exc.Forbidden
        with pytest.raises(cqrs_exc.Forbidden):
            raise cqrs_exc.Forbidden

    def test_cqrs_conflict(self):
        with pytest.raises(cqrs_exc.CQRSException):
            raise cqrs_exc.Conflict
        with pytest.raises(cqrs_exc.Conflict):
            raise cqrs_exc.Conflict

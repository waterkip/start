# SPDX-FileCopyrightText: 2020 Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import os
import pytest
from minty.config.parser import JSONConfigParser


def load_first_config():
    filename = os.path.dirname(__file__) + "/data/first.json"
    with open(filename, encoding="utf-8") as file:
        return file.read()


class TestApacheLoading:
    def test_load_config_content(self):
        parser = JSONConfigParser()
        assert parser

    def test_load_config_file_without_config(self):
        with pytest.raises(ValueError):
            JSONConfigParser().parse(content="")


class TestApacheParsing:
    def test_parse_config_file(self):
        parser = JSONConfigParser()
        config = parser.parse(content=load_first_config())
        assert config["name"] == "Example"
        assert config["simple_version"] == 3
        assert config["HashExample"]["hash_value"] == 1
        assert config["HashExample"]["hash_value2"] == 2
        assert config["MultiHash"]["multientity1"]["multivalue"] == 1
        assert config["MultiHash"]["multientity2"]["multivalue"] == 1

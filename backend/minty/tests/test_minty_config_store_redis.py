# SPDX-FileCopyrightText: 2020 Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytest
from minty.config.parser import ApacheConfigParser
from minty.config.store import RedisStore
from minty.config.store.base import ConfigurationNotFound
from unittest import mock


class TestRedisRetrieving:
    def test_get_configuration(self):
        store = RedisStore(
            parser=ApacheConfigParser(),
            arguments={  # type: ignore
                "saas:instance:test.example.com": b"""
                    <instance>
                        Example = true
                    </instance>"""
            },
        )
        config = store.retrieve("test.example.com")
        assert config, "Redis did not return configuration"
        assert "Example" in config.keys(), "Config did not match 'Example'"

    def test_get_configuration_miss(self):
        store = RedisStore(
            parser=ApacheConfigParser(),
            arguments={  # type: ignore
                "saas:instance:test.example.com": b"""
                    <instance>
                        Example = true
                    </instance>"""
            },
        )

        with pytest.raises(ConfigurationNotFound):
            store.retrieve("does-not-exist.example.com")

    def test_get_instance_hostnames(self):
        mock_redis = mock.Mock()
        mock_redis.smembers.return_value = [b"host1", b"host2"]
        store = RedisStore(
            parser=ApacheConfigParser(),
            arguments=mock_redis,
        )

        assert store.get_instance_hostnames() == ["host1", "host2"]
        mock_redis.smembers.assert_called_once_with("saas:instances")

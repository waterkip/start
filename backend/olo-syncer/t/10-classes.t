use Test::Class::Moose::Load 't/lib';
use Test::Class::Moose::Runner;

use Log::Log4perl ':easy';

Log::Log4perl->easy_init($TRACE);

Test::Class::Moose::Runner->new(
    test_classes => \@ARGV
)->runtests;

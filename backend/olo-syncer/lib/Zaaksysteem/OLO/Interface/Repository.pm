package Zaaksysteem::OLO::Interface::Repository;

use Moose::Role;
use namespace::autoclean;

use BTTW::Tools qw[sig];
use DateTime::Format::Strptime;

=head1 NAME

Zaaksysteem::OLO::Interface::Repository - Interface definition for OLO repositories

=head1 REQUIRED METHODS

=head2 get_messages

This method takes an optional L<DateTime>, and should produce a list of
L<Zaaksysteem::OLO::Message> instances found in the repository, starting at
that date. If no date is provided, all messages must be returned.

=head2 get_file

Given a file path, returns a L<Zaaksysteem::OLO::Attachment> instance,
representing the file at the other end.

=cut

requires qw[
    get_messages
    get_file
];

sig get_messages => '?DateTime => @Zaaksysteem::OLO::Message';
sig get_file => '=> ?Zaaksysteem::OLO::Attachment';

=head1 ATTRIBUTES

=head2 timestamp_parser

Timestamp parser configured for OLO-specific timestamp format

=cut

has timestamp_parser => (
    is => 'rw',
    isa => 'DateTime::Format::Strptime',
    builder => '_build_timestamp_parser',
);

=head1 METHODS

=head2 parse_timestamp

This method helps with parsing timestamps found in the filenames of messages
in OLO repositories.

=cut

sub parse_timestamp {
    my $self = shift;

    my $dt = $self->timestamp_parser->parse_datetime(shift);

    return unless defined $dt;

    $dt->set_time_zone('UTC');

    return $dt;
}

=head1 PRIVATE METHODS

=head2 _build_timestamp_parser

Builds an OLO-specific L<DateTime::Format::Strptime> timestamp parser.

=cut

sub _build_timestamp_parser {
    my $self = shift;

    return DateTime::Format::Strptime->new(
        time_zone => 'Europe/Amsterdam',
        on_error => 'undef',
        pattern => '%Y%m%d%H%M%S'
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE> file.

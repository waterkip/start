package Zaaksysteem::OLO::Attachment;

use Moose;
use namespace::autoclean;

=head1 NAME

Zaaksysteem::OLO::Attachment - Attachment data holder/handler

=head1 DESCRIPTION

OLO messages come with attached documents. This class provides a bit of
formalized glue for repositories to return when a file is requested.

Essentially, objects of this class contain a document name and a reference
to the content of the document for use in the OLO model(s).

=cut

use BTTW::Tools;

=head1 ATTRIBUTES

=head2 title

Title of the document.

=cut

has title => (
    is => 'rw',
    isa => 'Str',
    required => 1
);

=head2 handle

Reference to an open L<IO::Handle> object from which the file contents can
be read.

=cut

has content => (
    is => 'rw',
    isa => 'IO::Handle',
    required => 1
);

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

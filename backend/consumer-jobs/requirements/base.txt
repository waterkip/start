setuptools>=69.2.0

../minty
../minty-amqp

../minty-infra-amqp
../minty-infra-sqlalchemy

python-json-logger

##  Project specific requirements
../domains

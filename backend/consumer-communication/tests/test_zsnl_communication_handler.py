# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
from datetime import datetime, timezone
from minty import exceptions
from unittest import mock
from uuid import uuid4
from zsnl_communication_consumer import handlers


class TestHandler:
    """Tests for the consumer classes"""

    def test_email_import_handler(self):
        mock_cqrs = mock.MagicMock()
        handler = handlers.EmailImportHandler(mock_cqrs)

        assert handler.routing_keys == [
            "zsnl.v2.communication.IncomingEmail.EmailReceived"
        ]

        file_uuid = str(uuid4())

        mock_event = mock.MagicMock()
        mock_event.changes = [{"key": "file", "new_value": file_uuid}]
        handler.handle(mock_event)

        mock_cqrs.get_command_instance.assert_called_once_with(
            mock_event.correlation_id,
            "zsnl_domains.communication",
            mock_event.context,
            mock_event.user_uuid,
            mock_event.user_info,
        )

        mock_cqrs.get_command_instance().import_email.assert_called_once_with(
            file_uuid=file_uuid
        )

    def test_email_send_handler(self):
        mock_cqrs = mock.MagicMock()
        handler = handlers.EmailSendHandler(mock_cqrs)

        assert handler.routing_keys == [
            "zsnl.v2.zsnl_domains_communication.ExternalMessage.ExternalMessageCreated",
            "zsnl.v2.zsnl_communication_http_domains_communication.ExternalMessage.ExternalMessageCreated",
        ]

        message_uuid = str(uuid4())

        mock_event = mock.MagicMock()
        mock_event.changes = [
            {"key": "external_message_type", "new_value": "email"},
            {"key": "direction", "new_value": "outgoing"},
        ]
        mock_event.entity_id = message_uuid

        handler.handle(mock_event)

        mock_cqrs.get_command_instance.assert_called_once_with(
            mock_event.correlation_id,
            "zsnl_domains.communication",
            mock_event.context,
            mock_event.user_uuid,
            mock_event.user_info,
        )

        mock_cqrs.get_command_instance().send_email.assert_called_once_with(
            message_uuid=message_uuid
        )

    def test_email_send_handler_not_email(self):
        mock_cqrs = mock.MagicMock()
        handler = handlers.EmailSendHandler(mock_cqrs)

        assert handler.routing_keys == [
            "zsnl.v2.zsnl_domains_communication.ExternalMessage.ExternalMessageCreated",
            "zsnl.v2.zsnl_communication_http_domains_communication.ExternalMessage.ExternalMessageCreated",
        ]

        message_uuid = str(uuid4())

        mock_event = mock.MagicMock()
        mock_event.changes = [
            {"key": "external_message_type", "new_value": "pip"},
            {"key": "direction", "new_value": "outgoing"},
        ]
        mock_event.entity_id = message_uuid

        handler.handle(mock_event)

        mock_cqrs.get_command_instance.assert_not_called()
        mock_cqrs.get_command_instance().send_email.assert_not_called()

    def test_email_send_handler_not_outgoing(self):
        mock_cqrs = mock.MagicMock()
        handler = handlers.EmailSendHandler(mock_cqrs)

        assert handler.routing_keys == [
            "zsnl.v2.zsnl_domains_communication.ExternalMessage.ExternalMessageCreated",
            "zsnl.v2.zsnl_communication_http_domains_communication.ExternalMessage.ExternalMessageCreated",
        ]

        message_uuid = str(uuid4())

        mock_event = mock.MagicMock()
        mock_event.changes = [
            {"key": "external_message_type", "new_value": "email"},
            {"key": "direction", "new_value": "incoming"},
        ]
        mock_event.entity_id = message_uuid

        handler.handle(mock_event)

        mock_cqrs.get_command_instance.assert_not_called()
        mock_cqrs.get_command_instance().send_email.assert_not_called()

    def test_case_assignee_notification_handler(self):
        mock_cqrs = mock.MagicMock()
        handler = handlers.CaseAssigneeNotificationHandler(mock_cqrs)

        assert handler.routing_keys == [
            "zsnl.v2.zsnl_domains_case_management.Case.AssigneeNotified",
        ]

        case_uuid = str(uuid4())
        now = datetime.now(timezone.utc)

        mock_event = minty.cqrs.events.Event(
            uuid=uuid4(),
            correlation_id=uuid4(),
            user_uuid=uuid4(),
            created_date=now,
            domain="case_management",
            context="test_context",
            event_name="AssigneeNotified",
            entity_id=case_uuid,
            entity_type="Case",
            changes=[
                {
                    "key": "notification",
                    "new_value": {
                        "sender_name": "SenderName",
                        "sender_address": "sender@example.com",
                        "subject": "Message subject",
                        "body": "Message body",
                        "recipient_address": "recipient@example.com",
                    },
                    "old_value": None,
                }
            ],
            entity_data={},
        )

        handler.handle(mock_event)

        mock_cqrs.get_command_instance().send_notification.assert_called_once_with(
            case_uuid=mock_event.entity_id,
            sender_name="SenderName",
            sender_address="sender@example.com",
            subject="Message subject",
            body="Message body",
            recipient_address="recipient@example.com",
        )


class Test_as_user_I_want_to_generate_pdf_derivative_for_attachment:
    """Test pdf derivative generation for an attachment.
    This is for the preview for an attachment.
    """

    def test_attached_to_message_handler(self):
        mock_cqrs = mock.MagicMock()
        handler = handlers.AttachedToMessageHandler(mock_cqrs)
        assert handler.routing_keys == [
            "zsnl.v2.zsnl_domains_communication.MessageAttachment.AttachedToMessage",
            "zsnl.v2.zsnl_communication_http_domains_communication.MessageAttachment.AttachedToMessage",
        ]

        attachment_uuid = str(uuid4())

        mock_event = mock.MagicMock()
        mock_event.changes = [
            {
                "key": "attachment_uuid",
                "new_value": attachment_uuid,
                "old_value": None,
            }
        ]
        mock_event.entity_id = uuid4()

        handler.handle(mock_event)

        mock_cqrs.get_command_instance.assert_called_once_with(
            mock_event.correlation_id,
            "zsnl_domains.communication",
            mock_event.context,
            mock_event.user_uuid,
            mock_event.user_info,
        )
        mock_cqrs.get_command_instance().generate_pdf_derivative.assert_called_once_with(
            attachment_uuid=attachment_uuid
        )

    def test_attached_to_message_handler_errors(self):
        mock_cqrs = mock.MagicMock()
        handler = handlers.AttachedToMessageHandler(mock_cqrs)
        assert handler.routing_keys == [
            "zsnl.v2.zsnl_domains_communication.MessageAttachment.AttachedToMessage",
            "zsnl.v2.zsnl_communication_http_domains_communication.MessageAttachment.AttachedToMessage",
        ]

        attachment_uuid = str(uuid4())

        mock_event = mock.MagicMock()
        mock_event.changes = [
            {
                "key": "attachment_uuid",
                "new_value": attachment_uuid,
                "old_value": None,
            }
        ]
        mock_event.entity_id = uuid4()

        # First error:
        mock_cqrs.get_command_instance().generate_pdf_derivative.side_effect = KeyError
        mock_cqrs.reset_mock()

        handler.handle(mock_event)

        mock_cqrs.get_command_instance.assert_called_once_with(
            mock_event.correlation_id,
            "zsnl_domains.communication",
            mock_event.context,
            mock_event.user_uuid,
            mock_event.user_info,
        )
        mock_cqrs.get_command_instance().generate_pdf_derivative.assert_called_once_with(
            attachment_uuid=attachment_uuid
        )

        mock_cqrs.reset_mock()

        # Second error:
        mock_cqrs.get_command_instance().generate_pdf_derivative.side_effect = exceptions.NotFound
        handler.handle(mock_event)

        mock_cqrs.get_command_instance().generate_pdf_derivative.assert_called_once_with(
            attachment_uuid=attachment_uuid
        )

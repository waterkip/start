# SPDX-FileCopyrightText: 2020 Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import amqpstorm
import json
import threading
from minty_amqp.consumer import BaseConsumer, BaseHandler
from unittest import mock


class MockHandler(BaseHandler):
    @property
    def domain(self):
        return "test_domain"

    @property
    def routing_keys(self):
        return ["zsnl.v2.zsnl_domains_case_management.Case.CaseStatusSet"]

    def __init__(self):
        self.called_count = 0

    def handle(self, message):
        self.called_count += 1


class MockDyingHandler(BaseHandler):
    @property
    def domain(self):
        return "test_domain"

    @property
    def routing_keys(self):
        return ["zsnl.v2.zsnl_domains_case_management.Case.CaseStatusSet"]

    def __init__(self):
        self.called_count = 0

    def handle(self, message):
        raise IndexError  # just some random exception


class MockConsumer(BaseConsumer):
    def _register_routing(self):
        pass


class TestAMQPConsumer:
    def setup_method(self, mock_base):
        self.base_consumer = MockConsumer(
            queue="queue",
            exchange="simple_exchange",
            cqrs={"cqrs": "object"},
            qos_prefetch_count=99,
            dead_letter_config=None,
        )
        self.base_consumer.routing_keys = [
            "zsnl.v2.zsnl_domains_case_management.Case.CaseStatusSet"
        ]

    def test_consumer_start(self):
        event = threading.Event()
        with mock.MagicMock() as mock_connection:
            self.base_consumer.start(connection=mock_connection, ready=event)

            mock_connection.channel.assert_called()

            self.base_consumer.channel.basic.qos.assert_called_with(99)
            self.base_consumer.channel.queue.bind.assert_called_with(
                queue="queue",
                exchange="simple_exchange",
                routing_key="zsnl.v2.zsnl_domains_case_management.Case.CaseStatusSet",
            )
            self.base_consumer.channel.basic.consume.assert_called_with(
                self.base_consumer, "queue", no_ack=False
            )
            assert event.is_set()
            self.base_consumer.channel.start_consuming.assert_called()

        # check if close channel is called after start_consuming()
        with mock.MagicMock() as mock_connection:
            with mock.MagicMock() as mock_channel:
                mock_channel.consumer_tags = False
                mock_connection.channel.return_value = mock_channel
                self.base_consumer.start(
                    connection=mock_connection, ready=event
                )
                mock_channel.start_consuming.assert_called()

                mock_channel.close.assert_called()

        # Check if consumer is set to inactive on exception
        with mock.MagicMock() as mock_connection:
            self.base_consumer.active = False
            mock_connection.channel.side_effect = amqpstorm.AMQPError()
            self.base_consumer.start(connection=mock_connection, ready=event)
            assert self.base_consumer.active is False

    def test_consumer_stop(self):
        with mock.MagicMock() as mock_channel:
            self.base_consumer.channel = mock_channel
            self.base_consumer.stop()

            mock_channel.close.assert_called()

    def test_consumer_stop_no_channel(self):
        self.base_consumer.stop()

        assert not hasattr(self.base_consumer, "channel")

    def test_consumer_call(self):
        message = mock.Mock()
        message.body = json.dumps(
            {
                "changes": [
                    {
                        "key": "requestor",
                        "new_value": {
                            "id": "1e2cbe4d-96a0-4578-8b29-5429f6ba9702",
                            "type": "employee",
                        },
                        "old_value": None,
                    },
                    {
                        "key": "contact_channel",
                        "new_value": "email",
                        "old_value": None,
                    },
                    {"key": "status", "new_value": "new", "old_value": None},
                    {
                        "key": "created_date",
                        "new_value": "2019-09-20 10:08:49.245124",
                        "old_value": None,
                    },
                    {
                        "key": "registration_date",
                        "new_value": "2019-09-20",
                        "old_value": None,
                    },
                    {
                        "key": "last_modified_date",
                        "new_value": "2019-09-20 10:08:49.245124",
                        "old_value": None,
                    },
                    {"key": "milestone", "new_value": 1, "old_value": None},
                    {
                        "key": "case_type_uuid",
                        "new_value": "5dc74364-5ff9-4bfa-a0de-cf8996d51590",
                        "old_value": None,
                    },
                    {
                        "key": "case_type_version_uuid",
                        "new_value": "080a66c0-1428-4725-b068-749966a1de3d",
                        "old_value": None,
                    },
                    {"key": "case_type_id", "new_value": 1, "old_value": None},
                    {
                        "key": "case_type_node_id",
                        "new_value": 6,
                        "old_value": None,
                    },
                    {
                        "key": "request_trigger",
                        "new_value": "extern",
                        "old_value": None,
                    },
                    {
                        "key": "group_id",
                        "new_value": "18685486-b834-4921-9df2-9f4ae92e4ea9",
                        "old_value": None,
                    },
                    {
                        "key": "route_role",
                        "new_value": "aa60f010-a4e9-4228-a8b5-2d73e2e3125b",
                        "old_value": None,
                    },
                    {
                        "key": "department",
                        "new_value": "Development",
                        "old_value": None,
                    },
                    {"key": "subject", "new_value": "", "old_value": None},
                    {
                        "key": "subject_extern",
                        "new_value": "",
                        "old_value": None,
                    },
                    {
                        "key": "target_completion_date",
                        "new_value": "2019-09-27",
                        "old_value": None,
                    },
                    {
                        "key": "confidentiality",
                        "new_value": "public",
                        "old_value": None,
                    },
                ],
                "context": "dev.zaaksysteem.nl",
                "correlation_id": "ede50e21-86fe-4af6-86ad-6c00b8286042",
                "created_date": "2019-09-20T10:08:49.245271",
                "domain": "zsnl_domains.case_management",
                "entity_data": {},
                "entity_id": "7c38eb3e-8e53-40ca-8516-05eedbcf8931",
                "entity_type": "Case",
                "event_name": "CaseCreated",
                "id": "a1fa93e5-a6ec-4c50-a701-fbbf1890e3ee",
                "user_uuid": "1e2cbe4d-96a0-4578-8b29-5429f6ba9702",
                "user_info": {
                    "type": "UserInfo",
                    "user_uuid": "1e2cbe4d-96a0-4578-8b29-5429f6ba9702",
                    "permissions": {"admin": True},
                },
            }
        )
        message.method = {
            "routing_key": "zsnl.v2.zsnl_domains_case_management.Case.CaseStatusSet"
        }

        self.base_consumer._known_handlers = [MockHandler()]
        self.base_consumer(message=message)
        message.ack.assert_called_once_with()
        message.reject.assert_not_called()

    def test_consumer_call_no_handler_found(self):
        # Strange situation (got a message with a routing key that's not for us)
        handler = MockHandler()
        self.base_consumer._known_handlers = [handler]

        message = mock.Mock()
        message.body = json.dumps(
            {
                "changes": [
                    {
                        "key": "requestor",
                        "new_value": {
                            "id": "1e2cbe4d-96a0-4578-8b29-5429f6ba9702",
                            "type": "employee",
                        },
                        "old_value": None,
                    },
                    {
                        "key": "contact_channel",
                        "new_value": "email",
                        "old_value": None,
                    },
                    {"key": "status", "new_value": "new", "old_value": None},
                    {
                        "key": "created_date",
                        "new_value": "2019-09-20 10:08:49.245124",
                        "old_value": None,
                    },
                    {
                        "key": "registration_date",
                        "new_value": "2019-09-20",
                        "old_value": None,
                    },
                    {
                        "key": "last_modified_date",
                        "new_value": "2019-09-20 10:08:49.245124",
                        "old_value": None,
                    },
                    {"key": "milestone", "new_value": 1, "old_value": None},
                    {
                        "key": "case_type_uuid",
                        "new_value": "5dc74364-5ff9-4bfa-a0de-cf8996d51590",
                        "old_value": None,
                    },
                    {
                        "key": "case_type_version_uuid",
                        "new_value": "080a66c0-1428-4725-b068-749966a1de3d",
                        "old_value": None,
                    },
                    {"key": "case_type_id", "new_value": 1, "old_value": None},
                    {
                        "key": "case_type_node_id",
                        "new_value": 6,
                        "old_value": None,
                    },
                    {
                        "key": "request_trigger",
                        "new_value": "extern",
                        "old_value": None,
                    },
                    {
                        "key": "group_id",
                        "new_value": "18685486-b834-4921-9df2-9f4ae92e4ea9",
                        "old_value": None,
                    },
                    {
                        "key": "route_role",
                        "new_value": "aa60f010-a4e9-4228-a8b5-2d73e2e3125b",
                        "old_value": None,
                    },
                    {
                        "key": "department",
                        "new_value": "Development",
                        "old_value": None,
                    },
                    {"key": "subject", "new_value": "", "old_value": None},
                    {
                        "key": "subject_extern",
                        "new_value": "",
                        "old_value": None,
                    },
                    {
                        "key": "target_completion_date",
                        "new_value": "2019-09-27",
                        "old_value": None,
                    },
                    {
                        "key": "confidentiality",
                        "new_value": "public",
                        "old_value": None,
                    },
                ],
                "context": "dev.zaaksysteem.nl",
                "correlation_id": "ede50e21-86fe-4af6-86ad-6c00b8286042",
                "created_date": "2019-09-20T10:08:49.245271",
                "domain": "zsnl_domains.case_management",
                "entity_data": {},
                "entity_id": "7c38eb3e-8e53-40ca-8516-05eedbcf8931",
                "entity_type": "Case",
                "event_name": "CaseCreated",
                "id": "a1fa93e5-a6ec-4c50-a701-fbbf1890e3ee",
                "user_uuid": "1e2cbe4d-96a0-4578-8b29-5429f6ba9702",
                "user_info": {
                    "type": "UserInfo",
                    "user_uuid": "1e2cbe4d-96a0-4578-8b29-5429f6ba9702",
                    "permissions": {"admin": True},
                },
            }
        )
        message.method = {"routing_key": "not_handled_by_mock_handler"}
        self.base_consumer(message=message)

        assert handler.called_count == 0
        message.ack.assert_called_once_with()
        message.reject.assert_not_called()

    def test_consumer_call_exception(self):
        message = mock.Mock()
        message.body = json.dumps(
            {
                "changes": [
                    {
                        "key": "requestor",
                        "new_value": {
                            "id": "1e2cbe4d-96a0-4578-8b29-5429f6ba9702",
                            "type": "employee",
                        },
                        "old_value": None,
                    },
                    {
                        "key": "contact_channel",
                        "new_value": "email",
                        "old_value": None,
                    },
                    {"key": "status", "new_value": "new", "old_value": None},
                    {
                        "key": "created_date",
                        "new_value": "2019-09-20 10:08:49.245124",
                        "old_value": None,
                    },
                    {
                        "key": "registration_date",
                        "new_value": "2019-09-20",
                        "old_value": None,
                    },
                    {
                        "key": "last_modified_date",
                        "new_value": "2019-09-20 10:08:49.245124",
                        "old_value": None,
                    },
                    {"key": "milestone", "new_value": 1, "old_value": None},
                    {
                        "key": "case_type_uuid",
                        "new_value": "5dc74364-5ff9-4bfa-a0de-cf8996d51590",
                        "old_value": None,
                    },
                    {
                        "key": "case_type_version_uuid",
                        "new_value": "080a66c0-1428-4725-b068-749966a1de3d",
                        "old_value": None,
                    },
                    {"key": "case_type_id", "new_value": 1, "old_value": None},
                    {
                        "key": "case_type_node_id",
                        "new_value": 6,
                        "old_value": None,
                    },
                    {
                        "key": "request_trigger",
                        "new_value": "extern",
                        "old_value": None,
                    },
                    {
                        "key": "group_id",
                        "new_value": "18685486-b834-4921-9df2-9f4ae92e4ea9",
                        "old_value": None,
                    },
                    {
                        "key": "route_role",
                        "new_value": "aa60f010-a4e9-4228-a8b5-2d73e2e3125b",
                        "old_value": None,
                    },
                    {
                        "key": "department",
                        "new_value": "Development",
                        "old_value": None,
                    },
                    {"key": "subject", "new_value": "", "old_value": None},
                    {
                        "key": "subject_extern",
                        "new_value": "",
                        "old_value": None,
                    },
                    {
                        "key": "target_completion_date",
                        "new_value": "2019-09-27",
                        "old_value": None,
                    },
                    {
                        "key": "confidentiality",
                        "new_value": "public",
                        "old_value": None,
                    },
                ],
                "context": "dev.zaaksysteem.nl",
                "correlation_id": "ede50e21-86fe-4af6-86ad-6c00b8286042",
                "created_date": "2019-09-20T10:08:49.245271",
                "domain": "zsnl_domains.case_management",
                "entity_data": {},
                "entity_id": "7c38eb3e-8e53-40ca-8516-05eedbcf8931",
                "entity_type": "Case",
                "event_name": "CaseCreated",
                "id": "a1fa93e5-a6ec-4c50-a701-fbbf1890e3ee",
                "user_uuid": "1e2cbe4d-96a0-4578-8b29-5429f6ba9702",
                "user_info": {
                    "type": "UserInfo",
                    "user_uuid": "1e2cbe4d-96a0-4578-8b29-5429f6ba9702",
                    "permissions": {"admin": True},
                },
            }
        )
        message.method = {
            "routing_key": "zsnl.v2.zsnl_domains_case_management.Case.CaseStatusSet"
        }
        self.base_consumer._known_handlers = [MockDyingHandler()]
        self.base_consumer(message=message)

        message.ack.assert_not_called()
        message.reject.assert_called_once_with(requeue=False)


class TestAMQPConsumerDeadLetter:
    def setup_method(self):
        self.base_consumer = MockConsumer(
            queue="queue",
            exchange="simple_exchange",
            cqrs={"cqrs": "object"},
            qos_prefetch_count=99,
            dead_letter_config={
                "exchange": "retry_exchange",
                "retry_time_ms": 5000,
                "queue": "legacy_retry",
            },
        )
        self.base_consumer.routing_keys = [
            "zsnl.v2.zsnl_domains_case_management.Case.CaseStatusSet"
        ]

    def test_consumer_start_with_dead_letter_exchange(self):
        event = threading.Event()
        with mock.MagicMock() as mock_connection:
            self.base_consumer.start(connection=mock_connection, ready=event)

            mock_connection.channel.assert_called()

            self.base_consumer.channel.basic.qos.assert_called_with(99)
            self.base_consumer.channel.queue.declare.assert_called_with(
                queue="queue",
                durable=True,
                arguments={"x-dead-letter-exchange": "retry_exchange"},
            )
            assert self.base_consumer.channel.queue.declare.call_count == 2

            self.base_consumer.channel.queue.bind.assert_called_with(
                queue="queue",
                exchange="simple_exchange",
                routing_key="zsnl.v2.zsnl_domains_case_management.Case.CaseStatusSet",
            )
            assert self.base_consumer.channel.queue.bind.call_count == 2

            self.base_consumer.channel.basic.consume.assert_called_with(
                self.base_consumer, "queue", no_ack=False
            )
            assert event.is_set()
            self.base_consumer.channel.start_consuming.assert_called()

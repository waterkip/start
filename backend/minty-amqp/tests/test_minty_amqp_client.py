# SPDX-FileCopyrightText: 2020 Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import amqpstorm
import pytest
import threading
from minty_amqp.client import AMQPClient
from minty_amqp.consumer import BaseConsumer
from unittest import mock


class MockConsumer(BaseConsumer):
    def __init__(
        self,
        dead_letter_config=None,
        queue=None,
        routing_keys=None,
        exchange=None,
        cqrs=None,
        qos_prefetch_count: int = 1,
    ):
        self.cqrs = cqrs
        self.queue = queue
        self.exchange = exchange
        self.routing_keys = routing_keys
        self.channel = None
        self.active = False
        self.qos_prefetch_count = qos_prefetch_count
        # extra mock methods
        self.started_called = threading.Event()
        self.active = False

    def stop(self):
        self.stopped = True

    def __call__(self, message):
        pass

    def _register_routing(self):
        pass

    def start(self, connection: amqpstorm.Connection, ready: threading.Event):
        self.started_called.set()


class TestAMQPClient:
    def setup_method(self):
        self.base_config = {
            "amqp": {
                "url": "http://local.rabbitmq:5672",
                "publish_settings": {"exchange": "minty_exchange"},
                "MockConsumer": {
                    "queue_name": "case_management_queue",
                    "exchange": "minty_exchange",
                    "qos_prefetching": "1",
                    "number_of_channels": "1",
                    "dead_letter_exchange": {
                        "exchange": "minty_exchange_retry",
                        "retry_time_ms": "60000",
                    },
                },
            }
        }

        self.client = AMQPClient(
            self.base_config, max_retries=1, cqrs={"cqrs": "object"}
        )
        assert self.client.rabbitmq_url == "http://local.rabbitmq:5672"
        assert self.client.max_retries == 1
        assert self.client.cqrs == {"cqrs": "object"}

    @mock.patch("minty_amqp.client.UriConnection")
    def test_start(self, mock_connection):
        with mock.patch.object(
            AMQPClient, "_update_consumers"
        ) as mock_update_consumers:
            with mock.MagicMock() as mock_stopped:
                mock_stopped.is_set.side_effect = [False, True]
                mock_connection.is_closed = False
                self.client._stopped = mock_stopped
                self.client._connection = mock_connection
                self.client._threading_events.append(threading.Event())
                self.client.start()

                self.client._connection.check_for_errors.assert_called()
                mock_connection.check_for_errors.assert_called()
                mock_update_consumers.assert_called()

        # run with connection error
        with mock.MagicMock() as mock_stopped:
            with mock.patch.object(
                AMQPClient, "_create_connection", return_value=""
            ) as mock_create_connection:
                mock_stopped.is_set.side_effect = [False, True]
                mock_connection.is_closed = True
                mock_create_connection.return_value = "sure"
                self.client._connection = mock_connection
                self.client._stopped = mock_stopped
                self.client.start()
                mock_create_connection.assert_called()

    def test_stop(self):
        assert self.client._stopped.is_set() is False
        mock_consumer = MockConsumer()
        self.client._active_consumers = [mock_consumer]
        with mock.MagicMock() as mock_connection:
            self.client._connection = mock_connection
            self.client.stop()
            assert mock_consumer.stopped is True
            assert self.client._stopped.is_set() is True
            assert self.client._active_consumers == []

    @mock.patch("minty_amqp.client.UriConnection")
    def test_create_connection(self, connection_mock):
        self.client._stopped.clear()
        connection_mock.return_value = "connection"
        self.client._create_connection()
        connection_mock.assert_called_with("http://local.rabbitmq:5672")
        assert self.client._connection == "connection"

        with pytest.raises(Exception) as exception:
            connection_mock.side_effect = amqpstorm.AMQPError
            self.client._create_connection()
            assert exception == "max number of retries reached"

    def test_update_consumers(self):
        mock_cons1 = MockConsumer()
        mock_cons2 = MockConsumer()
        self.client._active_consumers = []
        self.client._registered_consumers = [mock_cons1, mock_cons2]

        with mock.patch.object(
            AMQPClient, "_start_consumer", return_value=""
        ) as _start_consumer_mock:
            self.client._update_consumers()

            assert len(self.client._active_consumers) == len(
                self.client._registered_consumers
            )
            _start_consumer_mock.assert_called()
        # test if _start consumer is not called a second time
        with mock.patch.object(
            AMQPClient, "_start_consumer", return_value=""
        ) as _start_consumer_mock:
            self.client._update_consumers()

            assert len(self.client._active_consumers) == len(
                self.client._registered_consumers
            )
            _start_consumer_mock.assert_not_called()

    def test_check_consumers(self):
        mock_consumer1 = MockConsumer()
        mock_consumer1.active = False
        mock_consumer1.failed_attempts = 1
        with mock.patch.object(
            AMQPClient, "_start_consumer", return_value=""
        ) as _start_consumer_mock:
            self.client._active_consumers = [mock_consumer1]
            self.client._check_consumers()
            _start_consumer_mock.assert_called()

        mock_consumer2 = MockConsumer()
        mock_consumer2.active = False
        mock_consumer2.failed_attempts = 3

        with mock.patch.object(
            AMQPClient, "stop", return_value=""
        ) as stop_mock:
            self.client._active_consumers = [mock_consumer2]
            self.client._check_consumers()
            stop_mock.assert_called()

    def test_stop_consumers(self):
        mock_consumer1 = MockConsumer()
        self.client._active_consumers = [mock_consumer1]
        self.client._stop_consumers()
        assert mock_consumer1.stopped is True

    def test_start_consumer(self):
        assert self.client._threading_events == []
        mock_consumer1 = MockConsumer()
        self.client._start_consumer(mock_consumer1)

        assert mock_consumer1.started_called.wait(1.0)
        assert len(self.client._threading_events) == 1
        assert self.client._threading_events[0].is_set() is False

    def test_cleanup_thread_events(self):
        event1 = threading.Event()
        event2 = threading.Event()
        self.client._threading_events = [event1, event2]

        assert len(self.client._threading_events) == 2
        for event in self.client._threading_events:
            assert event.is_set() is False
        for event in self.client._threading_events:
            event.set()

        for event in self.client._threading_events:
            assert event.is_set() is True

        self.client._cleanup_thread_events()

        assert self.client._threading_events == []

    def test_register_consumers(self):
        self.client.register_consumers([MockConsumer])
        assert len(self.client._registered_consumers) == 1
        for mc in self.client._registered_consumers:
            assert isinstance(mc, MockConsumer)

    def test_bind_routing_keys_to_queue(self):
        pass


class TestAMQPClientDeadLetterExchange:
    def setup_method(self):
        self.base_config = {
            "amqp": {
                "url": "http://local.rabbitmq:5672",
                "publish_settings": {"exchange": "minty_exchange"},
                "MockConsumer": {
                    "queue_name": "case_management_queue",
                    "exchange": "minty_exchange",
                    "qos_prefetching": "1",
                    "number_of_channels": "1",
                    "dead_letter_exchange": {
                        "exchange": "minty_exchange_retry",
                        "retry_time_ms": "60000",
                    },
                },
            }
        }

        self.client = AMQPClient(
            self.base_config, max_retries=1, cqrs={"cqrs": "object"}
        )
        assert self.client.rabbitmq_url == "http://local.rabbitmq:5672"
        assert self.client.max_retries == 1
        assert self.client.cqrs == {"cqrs": "object"}

    def test_register_consumers(self):
        self.client.register_consumers([MockConsumer])
        assert len(self.client._registered_consumers) == 1
        for mc in self.client._registered_consumers:
            assert isinstance(mc, MockConsumer)
        del self.base_config["amqp"]["MockConsumer"]["dead_letter_exchange"]
        self.client.register_consumers([MockConsumer])
        assert len(self.client._registered_consumers) == 2
        for mc in self.client._registered_consumers:
            assert isinstance(mc, MockConsumer)

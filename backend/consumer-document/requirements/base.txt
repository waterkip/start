setuptools>=69.2.0

../minty
../minty-amqp

../minty-infra-amqp
../minty-infra-sqlalchemy
../minty-infra-storage

python-json-logger~=2.0

##  Project specific requirements
../domains

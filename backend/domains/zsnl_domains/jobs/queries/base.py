# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
from ..entities.job import Job
from ..repositories.job import JobRepository
from minty.entity import RedirectResponse
from pydantic import validate_call
from typing import Final, cast
from uuid import UUID


class JobQueryBase(minty.cqrs.SplitQueryBase):
    pass


class GetJobs(JobQueryBase):
    name: Final = "get_jobs"

    @validate_call
    def __call__(self) -> list[Job]:
        jobs_repo = cast(
            JobRepository,
            self.get_repository("jobs"),
        )
        jobs: list[Job] = jobs_repo.get_jobs_for_user(self.qry.user_info)

        return jobs


class GetJob(JobQueryBase):
    name: Final = "get_job"

    @validate_call
    def __call__(self, job_uuid: UUID) -> Job:
        repo: JobRepository = cast(JobRepository, self.get_repository("jobs"))
        return repo.get_job(job_uuid, self.qry.user_info)


class DownloadJobResult(JobQueryBase):
    name: Final = "download_result"

    @validate_call
    def __call__(self, job_uuid: UUID) -> RedirectResponse:
        repo: JobRepository = cast(JobRepository, self.get_repository("jobs"))

        download_url = repo.get_job_result_download_url(
            job_uuid, self.qry.user_info
        )

        return RedirectResponse(location=download_url)


class DownloadJobErrors(JobQueryBase):
    name: Final = "download_errors"

    @validate_call
    def __call__(self, job_uuid: UUID) -> RedirectResponse:
        repo: JobRepository = cast(JobRepository, self.get_repository("jobs"))

        download_url = repo.get_job_errors_download_url(
            job_uuid, self.qry.user_info
        )

        return RedirectResponse(location=download_url)

# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

__version__ = "0.6.247"

import amqpstorm
import minty.cqrs.events
import minty.infrastructure
import redis
import redis.client
import sqlalchemy.orm
import time
from minty.repository import Repository
from minty_infra_amqp import AMQPInfrastructure
from minty_infra_misc import RedisInfrastructure
from minty_infra_sqlalchemy import DatabaseSessionInfrastructure
from sqlalchemy import sql
from typing import cast
from typing_extensions import deprecated


class ZaaksysteemRepositoryBase(Repository):
    REQUIRED_INFRASTRUCTURE = {
        "amqp": AMQPInfrastructure(),
        "redis": RedisInfrastructure(),
    }
    REQUIRED_INFRASTRUCTURE_RW = {
        "database": DatabaseSessionInfrastructure("zaaksysteemdb.")
    }
    REQUIRED_INFRASTRUCTURE_RO = {
        "database": DatabaseSessionInfrastructure("zaaksysteemdb_ro.")
    }

    MAX_QUERYTIME_COUNT_MS = 100

    @property
    def session(self) -> sqlalchemy.orm.Session:
        """
        Return a database session to the default database server for the
        current kind of request (for queries, read-only, for commands,
        read-write).
        """

        return cast(
            sqlalchemy.orm.Session, self._get_infrastructure(name="database")
        )

    @property
    def session_ro(self) -> sqlalchemy.orm.Session:
        """
        Return a database session that's connected to one of the "reader"
        (read-only) database servers.
        """
        return cast(
            sqlalchemy.orm.Session,
            self._get_infrastructure(name="database", read_only=True),
        )

    def is_rate_limited(
        self,
        action_key: str,
        action_parameter: str | None = None,
    ):
        """
        Check whether a given action is rate-limited for the current instance.

        Uses configuration from the `<rate_limits>` block, to determine what
        the limit on the specified action/parameter combination is.

        The parameter is there to allow for "per user" or "per object" rate
        limits (think "max 10 requests per user per minute" or "max 5 actions
        per case per minute").

        It implements a basic 'fixed window' rate limiter that allows a
        configured number of "hits" to an action/parameter combination per
        real-world minute.

        This means that a caller can "use up" their allotted requests in the
        first few seconds of a minute, but also that they can "burst" two
        allotments if they start at the end of one minute and then continue in
        the next one.
        """
        config = self.infrastructure_factory.get_config(context=self.context)

        shortname: str = config["instance_uuid"]
        rate_limit_settings: dict[str, str] = config.get("rate_limits", {})

        if action_key not in rate_limit_settings:
            self.logger.warning(
                f"Unconfigured rate-limiter {action_key} checked. Assuming 'no limit'."
            )
            return False

        rate_limit = int(rate_limit_settings[action_key])
        if rate_limit < 0:
            self.logger.debug(
                f"Rate limit for {action_key} < 0, not rate limiting."
            )
            return False

        cache = cast(redis.Redis, self._get_infrastructure(name="redis"))

        current_minute: int = time.gmtime().tm_min

        if action_parameter:
            cache_key: str = f"{shortname}:rate_limit:{action_key}:{action_parameter}:{current_minute}"
        else:
            cache_key: str = (
                f"{shortname}:rate_limit:{action_key}:{current_minute}"
            )

        current_value = cast(str | None, cache.get(cache_key))

        if current_value and (int(current_value) >= rate_limit):
            self.logger.debug(
                f"{action_key}:{action_parameter} is rate limited: ({current_value} >= {rate_limit})"
            )
            return True

        pipe: redis.client.Pipeline = cache.pipeline()
        pipe.incr(cache_key, 1)
        pipe.expire(cache_key, 60)
        pipe.execute()

        return False

    @deprecated(
        "Pass required secondary repositories to calls that need them (dependency injection)"
    )
    def _get_repo(self, domain, repo_name: str) -> Repository:
        repo: type[Repository] = domain.REQUIRED_REPOSITORIES[repo_name]
        return repo(
            infrastructure_factory=self.infrastructure_factory,
            infrastructure_factory_ro=self.infrastructure_factory_ro,
            context=self.context,
            event_service=self.event_service,
            read_only=self.read_only,
        )

    def _get_count(self, query: sql.expression.Select) -> int:
        """Get the total number of results for this query."""
        count_query = (
            query.with_only_columns(sql.func.count(sql.literal(1)))
            .limit(None)
            .offset(None)
            .order_by(None)
        )

        return self.session.execute(count_query).scalar()

    def _calculate_offset(self, page: int, page_size: int):
        """Given a page number and page size, calculate the offset at which it
        starts"""
        return (page * page_size) - page_size

    def send_query_event(self, event: minty.cqrs.events.Event) -> bool:
        """
        Send out an event on the message bus

        Note that this code does not wait for the rest of the domain code to
        finish before sending the event. This means any active database
        transaction will still be uncommitted and the consumer that picks up
        the event will not be able to see the changes made inside the
        transaction yet.

        That makes this method of limited/no use in commands, and it should
        only be used in queries.

        Examples use cases: "a case was retrieved and this needs to be logged",
        or "please make a thumbnail for this file".
        """
        config = self.infrastructure_factory.get_config(context=event.context)
        publish_to_exchange = config["amqp"]["publish_settings"]["exchange"]

        if self.is_rate_limited(
            action_key=f"query_event:{event.domain}:{event.entity_type}:{event.event_name}",
            action_parameter=str(event.entity_id),
        ):
            return False

        amqp_channel = cast(
            amqpstorm.Channel,
            self._get_infrastructure("amqp"),
        )
        message = amqpstorm.Message.create(
            channel=amqp_channel,
            body=event.as_json(),
            properties={"content_type": "application/json"},
        )
        message.publish(
            routing_key=event.routing_key(), exchange=publish_to_exchange
        )

        return True

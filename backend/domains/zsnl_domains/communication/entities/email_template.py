# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.entity import EntityBase
from uuid import UUID


class EmailTemplate(EntityBase):
    @property
    def entity_id(self):
        return self.uuid

    def __init__(
        self,
        id: int,
        uuid: UUID,
        sender_address: str,
        sender: str,
    ):
        self.id = id
        self.uuid = uuid
        self.sender_address = sender_address
        self.sender = sender

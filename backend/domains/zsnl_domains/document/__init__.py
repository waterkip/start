# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

# This should not be here (importing from a different domain)
from ..case_management.repositories import CaseRepository as CMCaseRepository
from .commands import Commands
from .queries import Queries
from .repositories import (
    CaseRegistrationFormRepository,
    CaseRepository,
    DirectoryEntryRepository,
    DirectoryRepository,
    DocumentArchiveRepository,
    DocumentLabelRepository,
    DocumentRepository,
    FileRepository,
    MessageRepository,
)

REQUIRED_REPOSITORIES = {
    "case": CaseRepository,
    "registration_form": CaseRegistrationFormRepository,
    "cm_case": CMCaseRepository,
    "directory_entry": DirectoryEntryRepository,
    "directory": DirectoryRepository,
    "document_archive_request": DocumentArchiveRepository,
    "document_label": DocumentLabelRepository,
    "document": DocumentRepository,
    "file": FileRepository,
    "message": MessageRepository,
}


def get_query_instance(repository_factory, context, user_uuid):
    return Queries(
        repository_factory=repository_factory,
        context=context,
        user_uuid=user_uuid,
    )


def get_command_instance(
    repository_factory, context, user_uuid, event_service
):
    return Commands(
        repository_factory=repository_factory,
        context=context,
        user_uuid=user_uuid,
        event_service=event_service,
    )

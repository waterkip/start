# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.cqrs import event
from minty.entity import EntityBase
from uuid import UUID


class Directory(EntityBase):
    @property
    def entity_id(self):
        return self.uuid

    def __init__(
        self,
        uuid: UUID,
        case_id: int | None = None,
        name: str | None = None,
        parent=None,
        path: list[str] | None = None,
    ):
        self.uuid = uuid
        self.name = name
        self.case_id = case_id
        self.parent = parent

        self.path = [] if path is None else path

    @event("DirectoryCreated")
    def create(self, case_id: int, name: str, path: list[str]):
        self.case_id = case_id
        self.name = name
        self.original_name = name
        self.path = path

    @event("DirectoryMoved")
    def move(self, path):
        self.path = path

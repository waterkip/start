# SPDX-FileCopyrightText: xxllnc Zaakgericht B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.entity import Entity
from pydantic import Field
from typing import Literal
from uuid import UUID


class CaseRegistrationFormDownload(Entity):
    entity_type: Literal["registration_form_download"] = (
        "registration_form_download"
    )
    entity_id__fields: list[str] = ["uuid"]

    uuid: UUID = Field(..., title="Identifier of the case")


class CaseRegistrationForm(Entity):
    entity_type: Literal["case_registration_form"] = "case_registration_form"
    entity_id__fields: list[str] = ["uuid"]
    entity_internal__fields: list[str] = [
        "storage_location",
        "mimetype",
        "document_uuid",
    ]

    uuid: UUID = Field(..., description="Unique uuid identifier of this case")
    id: int = Field(..., description="Unique integer indentifier of this case")
    document_uuid: UUID = Field(
        ..., description="S3 uuid for the registration form"
    )
    mimetype: str = Field(description="Mimetype for registration form")
    storage_location: str = Field(
        description="Storage location, s3/minio/swift"
    )
    document_name: str = Field(description="Name of the registration form")

    download_object: CaseRegistrationFormDownload | None = Field(
        None, title="Registration form download related to this object"
    )
    entity_relationships: list[str] = [
        "download_object",
    ]

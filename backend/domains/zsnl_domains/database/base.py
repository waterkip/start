# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from sqlalchemy.orm import DeclarativeBase


class Base(DeclarativeBase):
    pass

# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from pydantic.v1 import Field
from zsnl_domains.shared.custom_field.types import base as typebase


class CustomFieldEmail(typebase.CustomFieldValueBase):
    type: str = Field("email", title="A value for a email address.")
    value: str = Field(
        ...,
        title="The value for this email address field.",
    )

# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import operator
from .types import ComparisonFilterCondition, FilterOperator
from sqlalchemy import sql


def apply_comparison_filter(
    column,
    comparison_filters: list[ComparisonFilterCondition],
    filter_operator: FilterOperator = FilterOperator.and_operator,
) -> sql.ColumnElement[bool]:
    """
    Apply a list of `ComparisonFilterCondition` conditions to a
    SQLAlchemy query.

    Example:

    ```
    subquery = self.apply_comparison_filter(
        column=schema.SomeTable.id,
        comparison_filters=[
            ComparisonFilterCondition[int].new_from_str("gt 10"),
            ComparisonFilterCondition[int].new_from_str("le 100")
        ]
    )
    ```

    Returns a subquery with the added conditions.
    """
    subqueries = []
    for filter in comparison_filters:
        if isinstance(filter, list):
            time_period_subquery = []
            for item in filter:
                time_period_subquery.append(
                    getattr(operator, item.operator)(column, item.operand)
                )
            time_period_query = sql.and_(*time_period_subquery).self_group()
            subqueries.append(time_period_query)

        else:
            subqueries.append(
                getattr(operator, filter.operator)(column, filter.operand)
            )

    if filter_operator == FilterOperator.or_operator:
        query: sql.ColumnElement[bool] = sql.or_(*subqueries)
    else:
        query = sql.and_(*subqueries)

    return query


def apply_operator_to_filters(
    operator_filters: FilterOperator,
    filter_subquery,
) -> sql.ColumnElement[bool]:
    if operator_filters == FilterOperator.and_operator:
        query = sql.and_(*filter_subquery)
    elif operator_filters == FilterOperator.or_operator:
        query = sql.and_(sql.or_(*filter_subquery))
    return query

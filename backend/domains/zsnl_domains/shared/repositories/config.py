# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import enum
from ... import ZaaksysteemRepositoryBase
from sqlalchemy import sql
from zsnl_domains.database import schema


class SignatureUploadRole(enum.StrEnum):
    user = "Behandelaar"
    admin = "Zaaksysteembeheerder"


class ConfigurationRepository(ZaaksysteemRepositoryBase):
    def get_signature_upload_role(self) -> SignatureUploadRole:
        """
        Retrieve the name of the role that is allowed to upload new signatures.

        This value is either "Behandelaar" or "Zaaksysteembeheerder". If there
        is no value in the database, the default of "Behandelaar" will be used.
        """
        signature_upload_role_name = (
            self.session.execute(
                sql.select(schema.Config.value).where(
                    schema.Config.parameter == "signature_upload_role",
                )
            ).fetchone()[0]
            or "Behandelaar"
        )

        return SignatureUploadRole(signature_upload_role_name)

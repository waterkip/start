# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
from .base import (
    CreateGeoFeature,
    DeleteGeoFeature,
    SetGeoFeatureRelationships,
    UpdateGeoFeatureRelationships,
)
from .composite import (
    UpdateCaseGeo,
    UpdateCaseRequestor,
    UpdateContactGeo,
    UpdateCustomObjectGeo,
)

GEO_COMMANDS = minty.cqrs.build_command_lookup_table(
    commands={
        UpdateCaseGeo,
        CreateGeoFeature,
        DeleteGeoFeature,
        SetGeoFeatureRelationships,
        UpdateContactGeo,
        UpdateCustomObjectGeo,
        UpdateGeoFeatureRelationships,
        UpdateCaseRequestor,
    }
)

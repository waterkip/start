# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
from .base import GetGeoFeatures

GEO_QUERIES = minty.cqrs.build_query_lookup_table(queries={GetGeoFeatures})

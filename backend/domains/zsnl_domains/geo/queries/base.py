# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
from ..entities import GeoFeature
from ..repositories import GeoFeatureRepository
from minty.exceptions import ValidationError
from minty.validation import validate_with
from pkgutil import get_data
from typing import cast
from uuid import UUID


class GetGeoFeatures(minty.cqrs.SplitQueryBase):
    name = "get_geo_features"

    @validate_with(get_data(__name__, "validation/get_geo_features.json"))
    def __call__(self, uuid: str) -> list[GeoFeature]:
        """Gets all GeoFeatures by UUID. `uuid` can be a comma-separated list of UUIDs."""

        uuids = {UUID(u) for u in uuid.split(",")}

        if len(uuids) > 50:
            raise ValidationError(
                "No more than 50 UUIDs allowed in this query"
            )

        repo = cast(GeoFeatureRepository, self.get_repository("geo_feature"))

        data = repo.get_geo_features(uuids=uuids)
        return data

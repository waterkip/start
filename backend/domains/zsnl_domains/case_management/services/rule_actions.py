# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty
from ..entities import Case
from ..entities.case_type_version import RuleAction
from abc import ABC, abstractmethod
from datetime import date
from minty import date_calculation


class BaseAction(ABC, minty.Base):
    name = None

    def __init__(self, case: Case):
        self.case = case

    @abstractmethod
    def run(self, action: RuleAction):
        pass

    def get_kenmerk_setting(self, action: RuleAction, name: str) -> str | None:
        try:
            if self.case.custom_fields:
                value = self.case.custom_fields[
                    action.settings[name]["field_magic_string"]
                ]["value"][0]
                if value:
                    return value

        except (IndexError, KeyError, TypeError):
            self.logger.info(
                f"Invalid value for {self.name} in ruleaction for case {self.case.uuid}"
            )


class ChangeConfidentiality(BaseAction):
    """Class to change confidentiality of case"""

    name = "change_confidentiality"

    def run(self, action: RuleAction):
        self.case.confidentiality = action.settings["value"]


class SetCustomFieldValue(BaseAction):
    """Class to change custom_field value of a case"""

    name = "set_value"

    def _update_price(self, action: RuleAction):
        try:
            self.case.set_payment_amount(
                payment_amount=float(action.settings["value"])
            )
        except ValueError:
            self.logger.info(
                f"The supplied value {action.settings['value']} bedrag web for case {self.case.uuid} is not a decimal"
            )
        except KeyError:
            self.logger.info(
                f"Ruleaction 'vul waarde in' for 'Bedrag web' does not contain a price for case with uuid {self.case.uuid}"
            )

    def _update_result(self, action: RuleAction):
        try:
            sequence_num = int(action.settings["value"])
            if self.case.case_type_version:
                case_type_result = (
                    self.case.case_type_version.case_type_results[sequence_num]
                    if sequence_num
                    < len(self.case.case_type_version.case_type_results)
                    else None
                )
                if case_type_result:
                    self.case.set_case_result(
                        case_type_result=case_type_result
                    )
                else:
                    self.logger.info(
                        f"No case type result found on index {sequence_num}"
                    )
        except KeyError:
            self.logger.info(
                f"No result value specified for ruleaction set_value (result) for case {self.case.uuid}"
            )
        except ValueError:
            self.logger.info(
                f"Result value '{action.settings['value']}' specified for ruleaction set_value is invalid for case {self.case.uuid}"
            )

    def run(self, action: RuleAction):
        if "kenmerk" not in action.settings:
            self.logger.info(
                f"Invalid set_value rule action for case {self.case.uuid}"
            )
            return
        if action.settings["kenmerk"] == "price":
            self._update_price(action=action)
        elif action.settings["kenmerk"] == "case_result":
            self._update_result(action=action)
        else:
            self.case.update_custom_field(
                magic_string=action.settings["kenmerk"]["field_magic_string"],
                new_value=[action.settings["value"]],
            )


class ChangeRegistrationDate(BaseAction):
    """Class to change registration date of case"""

    name = "wijzig_registratiedatum"

    def run(self, action: RuleAction):
        recalculate_target_completiondate = (
            True
            if action.settings.get("recalculate", "off") == "on"
            else False
        )

        if self.case.custom_fields:
            registration_date = self.get_kenmerk_setting(
                action, "datum_bibliotheek_kenmerken_id"
            )

            def empty_username_provider():
                # No (easy) way to retrieve the username here.
                # (no access to cmd.get_repository and user_uuid)
                # It would be better to invest time to make the
                # CaseTemplateService work in a decorator like the rule_engine
                # does
                return ""

            if registration_date:
                self.case.set_registration_date(
                    registration_date=registration_date,
                    recalculate_target_completiondate=recalculate_target_completiondate,
                )


class DateCalculations(BaseAction):
    """Class to calculate dates"""

    name = "date_calculations"

    def run(self, action: RuleAction):
        try:
            old_date: str = self.get_kenmerk_setting(action, "src_attr_id")
            new_date: date = date.fromisoformat(old_date)
            day_delta: int = self._get_delta(
                action, "days_amount", "days_action"
            )
            # TODO results in errors till the settings have been added in MINTY-10173
            #        work_day_delta: int = self._get_delta(
            #            action, "work_days_amount", "work_days_action"
            #        )
            weeks_delta: int = self._get_delta(
                action, "weeks_amount", "weeks_action"
            )
            months_delta: int = self._get_delta(
                action, "months_amount", "months_action"
            )
            years_delta: int = self._get_delta(
                action, "years_amount", "years_action"
            )

            new_date = date_calculation.add_timedelta_to_date(
                "days", day_delta, new_date
            )
            # TODO results in errors till the settings have been added in MINTY-10173
            #        new_date = date_calculation.add_timedelta_to_date(
            #            "business_days", work_day_delta, new_date
            #        )
            new_date = date_calculation.add_timedelta_to_date(
                "weeks", weeks_delta, new_date
            )
            new_date = date_calculation.add_timedelta_to_date(
                "months", months_delta, new_date
            )
            new_date = date_calculation.add_timedelta_to_date(
                "years", years_delta, new_date
            )

            self.case.update_custom_field(
                magic_string=action.settings["dst_attr_id"][
                    "field_magic_string"
                ],
                new_value=[new_date],
            )
        except ValueError:
            self.logger.info(
                f"Invalid value for action with settings {action.settings} in ruleaction for case {self.case.uuid}"
            )

    def _get_delta(
        self, rule_action: RuleAction, amount: str, action: str
    ) -> int:
        amount_value: int = int(rule_action.settings[amount])
        action_value = rule_action.settings[action]
        delta: int
        if action_value == "add":
            delta = amount_value
        else:
            delta = -1 * amount_value
        return delta

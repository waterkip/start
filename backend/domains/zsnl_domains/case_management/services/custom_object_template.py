# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import datetime
import logging
from ...shared.services.template import TemplateService
from ..entities import CustomObject

logger = logging.getLogger(__name__)


class CustomObjectTemplateService(TemplateService):
    def render_value(
        self, custom_object: CustomObject, template_attribute: str
    ):
        template_attribute = getattr(
            custom_object.custom_object_type, template_attribute
        )

        if template_attribute is None:
            template_attribute = ""

        template_variables = self._prepare_entity(custom_object)
        return self.render(template_attribute, template_variables)

    def _prepare_entity(self, entity: CustomObject):
        attributes = entity.entity_dict()
        relationships = {}
        custom_fields = attributes["custom_fields"]
        updated_value = None
        for item in custom_fields:
            if custom_fields[item] and "value" in custom_fields[item]:
                value = custom_fields[item]["value"]
                if isinstance(value, (datetime.date, datetime.datetime)):
                    updated_value = self._format_date(value)
                    attributes["custom_fields"][item]["value"] = updated_value
                elif isinstance(value, str):
                    updated_value = self._process_string(value)
                    attributes["custom_fields"][item]["value"] = updated_value

        for relationship_key in entity.entity_relationships:
            relationship = attributes.pop(relationship_key)

            if isinstance(relationship, list):
                relationships[relationship_key] = relationship
            else:
                relationships[relationship_key] = [relationship]
        return {"attributes": attributes, "relationships": relationships}

    def _format_date(self, date_value):
        if isinstance(date_value, (datetime.datetime)):
            date_value = date_value.date()
        return date_value.strftime("%d-%m-%Y")

    def _process_string(self, template_value):
        try:
            date_value = datetime.datetime.strptime(template_value, "%Y-%m-%d")
            if isinstance(date_value, datetime.datetime):
                return self._format_date(date_value)
        except ValueError:
            return template_value

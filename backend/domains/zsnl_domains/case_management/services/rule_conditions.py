# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from ..entities import Case
from abc import ABC, abstractmethod
from typing import Any


class BaseCondition(ABC):
    def __init__(self, case: Case):
        self.case = case

    @classmethod
    @abstractmethod
    def name(cls):
        pass

    @abstractmethod
    def match(self, value: Any) -> bool:
        pass


class ConditionContactChannel(BaseCondition):
    def name(self):
        return "contactchannel"

    def match(self, value: Any) -> bool:
        return self.case.contact_channel == value


class ConditionRequestor(BaseCondition):
    def name(self):
        return "aanvrager"

    def match(self, value: Any) -> bool:
        subject_mapping = {
            "natuurlijk persoon": "person",
            "niet natuurlijk persoon": "organization",
            "behandelaar": "employee",
        }
        return self.case.requestor.type == subject_mapping[value.lower()]


class ConditionPresetClient(BaseCondition):
    def name(self):
        return "preset_client"

    def match(self, value: Any) -> bool:
        case_preset_client = (
            "Ja" if self.case.case_type.preset_requestor is not None else "Nee"
        )
        return case_preset_client == value


class ConditionLexSilencioPositivo(BaseCondition):
    def name(self):
        return "lex_silencio_positivo"

    def match(self, value: Any) -> bool:
        return self.case.case_type.lex_silencio_positivo == value


class ConditionWKPB(BaseCondition):
    def name(self):
        return "wkpb"

    def match(self, value: Any) -> bool:
        return self.case.case_type.wkpb == value


class ConditionSuspension(BaseCondition):
    def name(self):
        return "opschorten_mogelijk"

    def match(self, value: Any) -> bool:
        return self.case.case_type.suspension == value


class ConditionExtension(BaseCondition):
    def name(self):
        return "verlenging_mogelijk"

    def match(self, value: Any) -> bool:
        return self.case.case_type.extension == value


class Conditionpenalty(BaseCondition):
    def name(self):
        return "wet_dwangsom"

    def match(self, value: Any) -> bool:
        return self.case.case_type.penalty == value


class ConditionDesignationConfidentiality(BaseCondition):
    def name(self):
        return "vertrouwelijkheidsaanduiding"

    def match(self, value: Any) -> bool:
        return self.case.case_type.designation_of_confidentiality == value


class ConditionBag(BaseCondition):
    def name(self):
        return "bag"

    def match(self, value: Any) -> bool:
        return self.case.case_type.bag == value

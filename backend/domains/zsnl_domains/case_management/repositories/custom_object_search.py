# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import datetime
from ... import ZaaksysteemRepositoryBase
from ..entities import CustomObjectSearch
from minty.cqrs import UserInfo
from sqlalchemy import sql
from uuid import UUID
from zsnl_domains.case_management.entities.custom_object import (
    AuthorizationLevel,
)
from zsnl_domains.database import schema
from zsnl_domains.shared.repositories.object_acl import (
    allowed_object_v2_subquery,
)

co = sql.alias(schema.CustomObject, name="co")
ot = sql.alias(schema.CustomObjectType, name="ot")
otv = sql.alias(schema.CustomObjectTypeVersion, name="otv")
cov = sql.alias(schema.CustomObjectVersion, name="cov")
covc = sql.alias(schema.CustomObjectVersionContent, name="covc")

custom_object_search_query = sql.select(
    otv.c.uuid.label("object_type_uuid"),
    otv.c.name.label("object_type_name"),
    co.c.uuid.label("version_independent_uuid"),
    cov.c.uuid,
    cov.c.title,
    cov.c.subtitle,
    cov.c.external_reference,
    cov.c.status,
    cov.c.version,
    cov.c.date_created,
    cov.c.last_modified,
    cov.c.date_deleted,
    covc.c.custom_fields,
).select_from(
    sql.join(
        co,
        cov,
        sql.and_(
            cov.c.id == co.c.custom_object_version_id,
            sql.or_(
                cov.c.date_deleted.is_(None),
                cov.c.date_deleted > datetime.datetime.now(datetime.UTC),
            ),
        ),
    )
    .join(covc, cov.c.custom_object_version_content_id == covc.c.id)
    .join(otv, otv.c.id == cov.c.custom_object_type_version_id)
    .join(ot, ot.c.id == otv.c.custom_object_type_id)
)

DEFAULT_SORT_ORDER = [sql.desc(cov.c.title)]


class CustomObjectSearchRepository(ZaaksysteemRepositoryBase):
    _for_entity = "CustomObjectSearch"

    def search(
        self,
        page: int,
        page_size: int,
        custom_object_type_uuid: UUID,
        user_info: UserInfo,
    ) -> list[CustomObjectSearch] | None:
        """Get list of Custom objects based on given filter."""

        offset = self._calculate_offset(page, page_size)

        query = custom_object_search_query.where(
            allowed_object_v2_subquery(
                user_info=user_info,
                authorization=AuthorizationLevel.read,
                object_alias=co,
            )
        )

        if custom_object_type_uuid:
            query = query.where(ot.c.uuid == custom_object_type_uuid)

            sort_order = DEFAULT_SORT_ORDER

            custom_objects_list = self.session.execute(
                query.order_by(cov.c.date_created.desc())
                .order_by(*sort_order)
                .limit(page_size)
                .offset(offset)
            ).fetchall()

            return [
                self._entity_from_row(row=row) for row in custom_objects_list
            ]

    def _entity_from_row(self, row) -> CustomObjectSearch:
        mapping = {
            "version_independent_uuid": "version_independent_uuid",
            "uuid": "uuid",
            "name": "object_type_name",
            "title": "title",
            "subtitle": "subtitle",
            "external_reference": "external_reference",
            "custom_fields": "custom_fields",
            "date_created": "date_created",
            "last_modified": "last_modified",
            "date_deleted": "date_deleted",
            "entity_id": "uuid",
            "entity_meta_summary": "title",
        }

        object_type_mapping = {
            "uuid": "object_type_uuid",
            "entity_id": "object_type_uuid",
        }

        entity_obj = {}
        for key, objkey in mapping.items():
            entity_obj[key] = getattr(row, objkey)

        entity_obj["custom_object_type"] = {}
        for key, objkey in object_type_mapping.items():
            entity_obj["custom_object_type"][key] = getattr(row, objkey)

        return CustomObjectSearch.parse_obj(
            {**entity_obj, "_event_service": self.event_service}
        )

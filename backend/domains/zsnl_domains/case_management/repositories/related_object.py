# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from ... import ZaaksysteemRepositoryBase
from ..entities import RelatedObject
from sqlalchemy import sql
from sqlalchemy.dialects import postgresql
from uuid import UUID
from zsnl_domains.database import schema

custom_objects_related_to_contact_query = sql.select(
    schema.CustomObject.uuid,
    schema.CustomObjectVersion.title,
    schema.CustomObjectType.uuid.label("object_type_uuid"),
    schema.CustomObjectTypeVersion.name.label("object_type_name"),
    sql.literal(None).label("magic_strings"),
    sql.literal(None).label("magic_strings_inbound"),
    postgresql.array([schema.CustomObject.uuid]).label("relation_source"),
).select_from(
    sql.join(
        schema.CustomObject,
        schema.CustomObjectVersion,
        schema.CustomObject.custom_object_version_id
        == schema.CustomObjectVersion.id,
    )
    .join(
        schema.CustomObjectTypeVersion,
        schema.CustomObjectVersion.custom_object_type_version_id
        == schema.CustomObjectTypeVersion.id,
    )
    .join(
        schema.CustomObjectType,
        schema.CustomObjectTypeVersion.custom_object_type_id
        == schema.CustomObjectType.id,
    )
)


def _related_custom_objects_query(object_uuid: UUID):
    co = schema.CustomObject
    cor = schema.CustomObjectRelationship
    cov = schema.CustomObjectVersion
    cot = schema.CustomObjectType
    cotv = schema.CustomObjectTypeVersion

    relations_from_object = (
        sql.select(
            co.uuid.label("uuid"),
            cor.related_custom_object_id.label("custom_object_id"),
            cov.title,
            cot.uuid.label("object_type_uuid"),
            cotv.name.label("object_type_name"),
            cor.relationship_magic_string_prefix.label("magic_string"),
            sql.literal(None).label("magic_string_inbound"),
            sql.case(
                (
                    cor.relationship_magic_string_prefix.is_(None),
                    sql.cast(sql.literal(object_uuid), postgresql.UUID),
                ),
                else_=None,
            ).label("relation_source"),
        )
        .select_from(
            sql.join(
                cor,
                co,
                sql.and_(
                    co.id == cor.related_custom_object_id,
                    cor.relationship_type == "custom_object",
                ),
            )
            .join(cov, co.custom_object_version_id == cov.id)
            .join(cotv, cov.custom_object_type_version_id == cotv.id)
            .join(cot, cotv.custom_object_type_id == cot.id)
        )
        .where(
            cor.custom_object_id
            == sql.select(co.id)
            .where(co.uuid == object_uuid)
            .scalar_subquery()
        )
    )
    relations_to_object = (
        sql.select(
            co.uuid.label("uuid"),
            cor.custom_object_id.label("custom_object_id"),
            cov.title,
            cot.uuid.label("object_type_uuid"),
            cotv.name.label("object_type_name"),
            sql.literal(None).label("magic_string"),
            cor.relationship_magic_string_prefix.label("magic_string_inbound"),
            sql.case(
                (
                    cor.relationship_magic_string_prefix.is_(None),
                    co.uuid,
                ),
                else_=None,
            ).label("relation_source"),
        )
        .select_from(
            sql.join(cor, co, co.id == cor.custom_object_id)
            .join(cov, co.custom_object_version_id == cov.id)
            .join(cotv, cov.custom_object_type_version_id == cotv.id)
            .join(cot, cotv.custom_object_type_id == cot.id)
        )
        .where(
            sql.and_(
                cor.relationship_type == "custom_object",
                cor.related_custom_object_id
                == sql.select(co.id)
                .where(co.uuid == object_uuid)
                .scalar_subquery(),
            )
        )
    )

    combined = sql.union_all(relations_from_object, relations_to_object).cte()

    return sql.select(
        combined.c.uuid,
        combined.c.title,
        combined.c.object_type_uuid,
        combined.c.object_type_name,
        sql.func.array_agg(combined.c.magic_string).label("magic_strings"),
        sql.func.array_agg(combined.c.magic_string_inbound).label(
            "magic_strings_inbound"
        ),
        sql.func.array_agg(combined.c.relation_source).label(
            "relation_source"
        ),
    ).group_by(
        combined.c.uuid,
        combined.c.title,
        combined.c.object_type_uuid,
        combined.c.object_type_name,
    )


class RelatedObjectRepository(ZaaksysteemRepositoryBase):
    def find_for_subject(self, subject_uuid: UUID) -> list[RelatedObject]:
        """
        Find related custom objects for a subject.

        :param subject_uuid: UUID of the contact whose relations to retrieve
        :return: custom_objects related to a custom_object
        """

        for_contact_query = custom_objects_related_to_contact_query.where(
            schema.CustomObjectVersion.id.in_(
                sql.select(
                    schema.CustomObjectRelationship.custom_object_version_id
                ).where(
                    sql.and_(
                        schema.CustomObjectRelationship.relationship_type
                        == "subject",
                        schema.CustomObjectRelationship.related_uuid
                        == subject_uuid,
                    )
                )
            )
        )
        result = self.session.execute(for_contact_query).fetchall()

        return [self._inflate_row_to_entity(row) for row in result]

    def find_for_custom_object(self, object_uuid: UUID) -> list[RelatedObject]:
        """
        Find related custom objects for custom_object.

        :param object_uuid: UUID of the custom_object
        :return: custom_objects related to a custom_object
        """

        select_stmt = _related_custom_objects_query(object_uuid)

        result = self.session.execute(select_stmt).fetchall()

        return [self._inflate_row_to_entity(row) for row in result]

    def _inflate_row_to_entity(self, row) -> RelatedObject:
        """
        Inflate database row to RelatedObject Entity.

        :param row: Database Row
        :type row: object
        :return: RelatedObjectEntity
        :rtype: RelatedObject
        """

        magic_strings: list[str] = []
        magic_strings_inbound: list[str] = []
        relation_source: set[UUID] = set()

        self.logger.info(row)

        for i in range(len(row.magic_strings or [])):
            if row.magic_strings[i]:
                magic_strings.append(row.magic_strings[i])

            if row.magic_strings_inbound[i]:
                magic_strings_inbound.append(row.magic_strings_inbound[i])

            if row.relation_source[i]:
                relation_source |= {row.relation_source[i]}

        return RelatedObject(
            entity_id=row.uuid,
            uuid=row.uuid,
            entity_meta_summary=row.title,
            object_type={
                "entity_id": row.object_type_uuid,
                "entity_meta_summary": row.object_type_name,
            },
            related_to_magic_strings=magic_strings,
            related_from_magic_strings=magic_strings_inbound,
            relation_source=list(relation_source),
            _event_service=self.event_service,
        )

# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from ... import ZaaksysteemRepositoryBase
from ..entities import CaseTypeResult, CaseTypeResultBasic
from minty.cqrs import UserInfo
from minty.entity import EntityCollection
from minty.exceptions import NotFound
from sqlalchemy import sql
from sqlalchemy.dialects import postgresql
from sqlalchemy.engine.row import Row
from uuid import UUID
from zsnl_domains.database import schema
from zsnl_domains.shared.repositories.case_acl import (
    user_allowed_cases_subquery,
)

CASE_ALIAS = sql.alias(schema.Case)
ZAAK_TYPE_RESULTATEN_ALIAS = sql.alias(schema.ZaaktypeResultaten)


class CaseTypeResultRepository(ZaaksysteemRepositoryBase):
    ZAAK_TYPE_RESULT_BASE_QUERY = sql.select(
        ZAAK_TYPE_RESULTATEN_ALIAS.c.uuid.label("uuid"),
        ZAAK_TYPE_RESULTATEN_ALIAS.c.label.label("name"),
        ZAAK_TYPE_RESULTATEN_ALIAS.c.resultaat.label("result"),
        ZAAK_TYPE_RESULTATEN_ALIAS.c.trigger_archival.label(
            "trigger_archival"
        ),
        schema.ResultPreservationTerms.label.label("preservation_term_label"),
        schema.ResultPreservationTerms.unit.label("preservation_term_unit"),
        schema.ResultPreservationTerms.unit_amount.label(
            "preservation_term_unit_amount"
        ),
    ).select_from(
        sql.join(
            ZAAK_TYPE_RESULTATEN_ALIAS,
            CASE_ALIAS,
            ZAAK_TYPE_RESULTATEN_ALIAS.c.zaaktype_node_id
            == CASE_ALIAS.c.zaaktype_node_id,
        ).join(
            schema.ResultPreservationTerms,
            schema.ResultPreservationTerms.code
            == ZAAK_TYPE_RESULTATEN_ALIAS.c.bewaartermijn,
        )
    )

    _for_entity = "CaseTypeResult"
    _events_to_calls = {"SavedSearchCreated": "_save_create"}

    def get_multiple(
        self, case_uuid: UUID, user_info: UserInfo, permission
    ) -> EntityCollection[CaseTypeResultBasic]:
        multiple_query = self.ZAAK_TYPE_RESULT_BASE_QUERY.where(
            sql.and_(
                CASE_ALIAS.c.uuid == case_uuid,
                CASE_ALIAS.c.status != "deleted",
            )
        )
        multiple_query = multiple_query.where(
            sql.and_(
                user_allowed_cases_subquery(
                    user_info=user_info,
                    permission=permission,
                    case_alias=CASE_ALIAS.c,
                ),
            )
        )

        query_result = self.session.execute(multiple_query).fetchall()
        entities = [self._inflate(row) for row in query_result]
        return EntityCollection(entities=entities)

    def get(
        self,
        case_uuid: UUID,
        case_type_result_uuid: UUID,
        user_info: UserInfo,
        permission,
    ) -> CaseTypeResultBasic:
        single_query = self.ZAAK_TYPE_RESULT_BASE_QUERY.where(
            sql.and_(
                CASE_ALIAS.c.uuid == case_uuid,
                CASE_ALIAS.c.status != "deleted",
                ZAAK_TYPE_RESULTATEN_ALIAS.c.uuid == case_type_result_uuid,
            )
        )
        single_query = single_query.where(
            sql.and_(
                user_allowed_cases_subquery(
                    user_info=user_info,
                    permission=permission,
                    case_alias=CASE_ALIAS.c,
                ),
            )
        )

        row = self.session.execute(single_query).fetchone()
        if not row:
            raise NotFound(
                f"case_type_result not found {case_type_result_uuid} for case_uuid {case_uuid}"
            )
        return self._inflate(row)

    def _inflate(self, row: Row) -> CaseTypeResultBasic:
        return CaseTypeResultBasic(
            uuid=row.uuid,
            name=row.name,
            result=row.result,
            trigger_archival=row.trigger_archival,
            preservation_term_label=row.preservation_term_label,
            preservation_term_unit=row.preservation_term_unit,
            preservation_term_unit_amount=row.preservation_term_unit_amount,
        )

    def find_case_type_result_by_list_uuids(
        self, case_type_version_uuids: list[UUID]
    ) -> list[CaseTypeResult]:
        query = sql.select(
            ZAAK_TYPE_RESULTATEN_ALIAS.c.uuid.label("uuid"),
            ZAAK_TYPE_RESULTATEN_ALIAS.c.label.label("description"),
            ZAAK_TYPE_RESULTATEN_ALIAS.c.resultaat.label("result"),
            ZAAK_TYPE_RESULTATEN_ALIAS.c.trigger_archival.label(
                "trigger_archival"
            ),
            ZAAK_TYPE_RESULTATEN_ALIAS.c.standaard_keuze.label(
                "standard_choice"
            ),
            ZAAK_TYPE_RESULTATEN_ALIAS.c.selectielijst.label("selection_list"),
            ZAAK_TYPE_RESULTATEN_ALIAS.c.selectielijst_brondatum.label(
                "selection_list_start"
            ),
            ZAAK_TYPE_RESULTATEN_ALIAS.c.selectielijst_einddatum.label(
                "selection_list_end"
            ),
            ZAAK_TYPE_RESULTATEN_ALIAS.c.archiefnominatie.label(
                "type_of_archiving"
            ),
            ZAAK_TYPE_RESULTATEN_ALIAS.c.bewaartermijn.label(
                "retention_period"
            ),
            ZAAK_TYPE_RESULTATEN_ALIAS.c.ingang.label(
                "retention_period_source_date"
            ),
            ZAAK_TYPE_RESULTATEN_ALIAS.c.comments.label("result_explanation"),
            sql.cast(ZAAK_TYPE_RESULTATEN_ALIAS.c.properties, postgresql.JSON)[
                "procestermijn"
            ].astext.label("process_term"),
            sql.cast(ZAAK_TYPE_RESULTATEN_ALIAS.c.properties, postgresql.JSON)[
                "procestype_generiek"
            ].astext.label("process_type_generic"),
            sql.cast(ZAAK_TYPE_RESULTATEN_ALIAS.c.properties, postgresql.JSON)[
                "procestype_naam"
            ].astext.label("process_type_name"),
            sql.cast(ZAAK_TYPE_RESULTATEN_ALIAS.c.properties, postgresql.JSON)[
                "procestype_nummer"
            ].astext.label("process_type_number"),
            sql.cast(ZAAK_TYPE_RESULTATEN_ALIAS.c.properties, postgresql.JSON)[
                "procestype_object"
            ].astext.label("process_type_object"),
            sql.cast(ZAAK_TYPE_RESULTATEN_ALIAS.c.properties, postgresql.JSON)[
                "procestype_omschrijving"
            ].astext.label("process_type_description"),
            sql.cast(ZAAK_TYPE_RESULTATEN_ALIAS.c.properties, postgresql.JSON)[
                "procestype_toelichting"
            ].astext.label("process_type_explanation"),
            sql.cast(ZAAK_TYPE_RESULTATEN_ALIAS.c.properties, postgresql.JSON)[
                "selectielijst_nummer"
            ].astext.label("selection_list_number"),
            sql.cast(ZAAK_TYPE_RESULTATEN_ALIAS.c.properties, postgresql.JSON)[
                "herkomst"
            ].astext.label("origin"),
        ).where(ZAAK_TYPE_RESULTATEN_ALIAS.c.uuid.in_(case_type_version_uuids))

        results = self.session.execute(query).fetchall()
        return [
            self._sqla_to_entity_case_type_result(result) for result in results
        ]

    def _sqla_to_entity_case_type_result(self, row: Row) -> CaseTypeResult:
        return CaseTypeResult(
            entity_id=row.uuid,
            uuid=row.uuid,
            description=row.description,
            result=row.result,
            trigger_archival=row.trigger_archival,
            result_explanation=row.result_explanation,
            standard_choice=row.standard_choice,
            selection_list=row.selection_list,
            selection_list_start=row.selection_list_start,
            selection_list_end=row.selection_list_end,
            type_of_archiving=row.type_of_archiving,
            retention_period=row.retention_period,
            retention_period_source_date=row.retention_period_source_date,
            properties=self._generate_properties_for_case_type_result(row),
        )

    def _generate_properties_for_case_type_result(self, case_type_result):
        properties = {
            "process_type_generic": case_type_result.process_type_generic,
            "process_type_name": case_type_result.process_type_name,
            "process_type_number": case_type_result.process_type_number,
            "process_type_object": case_type_result.process_type_object,
            "process_type_description": case_type_result.process_type_description,
            "process_type_explanation": case_type_result.process_type_explanation,
            "selection_list_number": case_type_result.selection_list_number,
            "process_term": case_type_result.process_term,
            "origin": case_type_result.origin,
        }
        return properties

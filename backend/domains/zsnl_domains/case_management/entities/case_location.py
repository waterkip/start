# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.entity import Entity
from pydantic.v1 import Field
from uuid import UUID
from zsnl_domains.shared.custom_field.types.geojson import GeoJSON


class CaseLocation(Entity):
    entity_type = "case_location"
    entity_id__fields: list[str] = ["uuid"]
    uuid: UUID = Field(..., title="uuid")
    bag_type: str | None = Field(..., title="Bag type of case location")
    bag_id: str | None = Field(..., title="Bag id of case location")
    residential_object_id: str | None = Field(
        ..., title="Residential object number of case location"
    )
    public_space_id: str | None = Field(
        ..., title="Public space number of case location"
    )
    number_indication_id: str | None = Field(
        ..., title="Number indication of case location"
    )
    premise_id: str | None = Field(..., title="Premise id of case location")
    location_id: str | None = Field(..., title="Location id of case location")
    lay_id: str | None = Field(..., title="Lay id of case location")
    coordinates: str | None = Field(..., title="Coordinates of case location")
    full_address: str | None = Field(..., title="Full address of case")
    geo_coordinates: GeoJSON | None = Field(
        ..., title="Geo Coordinates of address"
    )

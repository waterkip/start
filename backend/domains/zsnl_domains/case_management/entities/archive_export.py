# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2


from . import _shared
from datetime import date, datetime
from minty.entity import Entity, ValueObject
from pydantic.v1 import Field
from uuid import UUID


class ArchiveSettings(ValueObject):
    archive_type: str | None = Field(..., title="Type of archive TMLO or MDTO")
    name: list[str] | None = Field(
        ..., title="Name specified in archive settings for tmlo"
    )
    classification_code: list[str] | None = Field(
        ..., title="Classification code  for tmlo"
    )
    classification_description: list[str] | None = Field(
        ..., title="Classification description  for tmlo"
    )
    classification_source_tmlo: list[str] | None = Field(
        ..., title="Classification source  for tmlo"
    )
    classification_date: list[str] | None = Field(
        ..., title="Classification date  for tmlo"
    )
    description_tmlo: list[str] | None = Field(
        ..., title="Description specified in archive settings  for tmlo"
    )
    location: list[str] | None = Field(
        ..., title="Location specified in archive settings  for tmlo"
    )
    coverage_in_time_tmlo: list[str] | None = Field(
        ..., title="Coverage in time info specified in archive settings"
    )
    coverage_in_geo_tmlo: list[str] | None = Field(
        ..., title="Coverage in geo specified in archive settings"
    )
    language_tmlo: str | None = Field(
        ..., title="Language specified in archive settings"
    )
    user_rights: list[str] | None = Field(
        ..., title="User rights specified in archive settings  for tmlo"
    )
    user_rights_description: list[str] | None = Field(
        ...,
        title="Description for user rights specified in archive settings  for tmlo",
    )
    user_rights_date_period: list[str] | None = Field(
        ...,
        title="Date period for user rights specified in archive settings  for tmlo",
    )
    confidentiality: list[str] | None = Field(
        ..., title="Confidentiality specified in archive settings  for tmlo"
    )
    confidentiality_description: list[str] | None = Field(
        ...,
        title="Confidentiality description specified in archive settings  for tmlo",
    )
    confidentiality_date_period: list[str] | None = Field(
        ...,
        title="Date period for confidentiality specified in archive settings  for tmlo",
    )
    form_genre: list[str] | None = Field(
        ..., title="Form genre specified in archive settings  for tmlo"
    )
    form_publication: list[str] | None = Field(
        ..., title="Form publication specified in archive settings  for tmlo"
    )
    structure: list[str] | None = Field(
        ..., title="Structure specified in archive setting  for tmlo"
    )
    generic_metadata_tmlo: list[str] | None = Field(
        ..., title="Generic metadata specified in archive settings  for tmlo"
    )


class RelatedCaseTypeVersion(Entity):
    """Reference to a case_type_version entity"""

    entity_type: str = Field("case_type_version", const=True)
    entity_id__fields: list[str] = ["uuid"]
    entity_meta__fields: list[str] = ["entity_meta_summary"]

    uuid: UUID = Field(..., title="UUID Identifier of the case type version")
    id: str | None = Field(..., title="Identifier of the case type version")
    name: str = Field(..., title="Name of the case type version")


class CaseDocument(Entity):
    """Entity represents document in the case"""

    entity_type = "document"
    id: int | None = Field(None, title="Id of the document")
    uuid: UUID | None = Field(None, title="UUID of the document")
    extension: str | None = Field(None, title="extension of the document")
    filename: str | None = Field(None, title="filename of the document")
    mimetype: str | None = Field(None, title="mimetype of the document")
    size: int | None = Field(None, title="size of the document")
    store_uuid: UUID | None = Field(None, title="store_uuid of the document")
    md5: str | None = Field(None, title="algorithm of the document")
    date_created: datetime | None = Field(
        None, title="created date of the document"
    )
    confidentiality: str | None = Field(
        None, title="confidentiality of the document"
    )
    storage_location: str | None = Field(
        None, title="Storage location of document"
    )
    case_id: int | None = Field(
        None, title="Id of case related to the document"
    )


class ArchiveExportCase(Entity):
    """Entity represents case for archive export"""

    entity_type = "archive_export_case"
    uuid: UUID = Field(..., title="Internal identifier of this entity")

    id: int = Field(..., title="Id of Case")
    custom_fields: dict = Field(..., title="custom fields for the case")

    result: _shared.CaseResult | None = Field(None, title="Case result")

    confidentiality: _shared.CaseConfidentiality = Field(
        ..., title="Confidentiality level of the case"
    )

    summary: str = Field(..., title="Summary of the case")
    public_summary: str = Field(..., title="Public summary of the case")

    case_type_version: RelatedCaseTypeVersion = Field(
        ...,
        title="Reference to the specific version of the case type of this case",
    )
    destruction_date: date | None = Field(
        None, title="Period to calculate destruction date"
    )
    type_of_archiving: str | None = Field(None, title="type of archiving")
    result_explanation: str | None = Field(
        ..., title="Result explanation of the case_type_result"
    )
    authorizations: set[_shared.CaseAuthorizationLevel] = Field(
        ..., title="Authorizations the current user has for this case"
    )
    archive_settings: ArchiveSettings = Field(
        ..., title="Archive settings for the case"
    )
    assignee: _shared.RelatedEmployee | None = Field(
        None, title="Assignee relationship"
    )
    registration_date: date = Field(None, title="case registration date")
    documents: list[CaseDocument] | None = Field(None, title="case documents")


class ArchiveExport(Entity):
    """Entity represents archive export"""

    entity_type = "archive_export"

    case_uuids: list[UUID] | None = Field(
        None, title="UUID of the cases for which you want to do archive export"
    )
    upload_result: dict | None = Field(
        None, title="Dictionary containing details of upload result"
    )
    filestore_uuid: UUID | None = Field(None, title="UUID of stored file")
    export_file_uuid: UUID | None = Field(..., title="export file uuid")

    @classmethod
    @Entity.event(name="ArchiveExportRequested", fire_always=True)
    def request(cls, **kwargs):
        return cls(**kwargs)

    @classmethod
    @Entity.event(name="ArchiveExportCreated", fire_always=True)
    def create(cls, **kwargs):
        return cls(**kwargs)

# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from datetime import date
from dateutil.relativedelta import relativedelta
from minty.entity import Entity, ValueObject
from pydantic.v1 import Field
from uuid import UUID


class CaseTypeResultProperties(ValueObject):
    process_type_generic: str | None = Field(
        ..., title="Result explanation of the case_type_result"
    )
    process_type_name: str | None = Field(
        ..., title="Result explanation of the case_type_result"
    )
    process_type_number: str | None = Field(
        ..., title="Result explanation of the case_type_result"
    )
    process_type_object: str | None = Field(
        ..., title="Result explanation of the case_type_result"
    )
    process_type_description: str | None = Field(
        ..., title="Result explanation of the case_type_result"
    )
    process_type_explanation: str | None = Field(
        ..., title="Result explanation of the case_type_result"
    )
    origin: str | None = Field(..., title="Origin of the case_type_result")
    process_term: str | None = Field(
        ..., title="Process term of the case_type_result"
    )
    selection_list_number: str | None = Field(
        ..., title="Result explanation of the case_type_result"
    )


class CaseTypeResult(Entity):
    entity_type = "case_type_result"
    entity_id__fields: list[str] = ["uuid"]

    uuid: UUID = Field(..., title="Uuid of the case_type_result")
    description: str | None = Field(..., title="Name of the case_type_result")
    result: str = Field(..., title="Result of the case_type_result")
    trigger_archival: bool = Field(..., title="Indicator for archive case")
    result_explanation: str = Field(
        ..., title="Result explanation of the case_type_result"
    )
    standard_choice: bool = Field(
        False, title="Standard_choice of the case_type_result"
    )
    selection_list: str | None = Field(
        ..., title="Selection_list of the case_type_result"
    )
    selection_list_start: date | None = Field(
        None, title="Selection list start date of the case_type_result"
    )
    selection_list_end: date | None = Field(
        None, title="Selection list end date of the case_type_result"
    )
    type_of_archiving: str | None = Field(
        ..., title="Type_of_archiving of the case_type_result"
    )
    retention_period: int | None = Field(
        ..., title="Retention period of the case_type_result"
    )
    retention_period_source_date: str | None = Field(
        ..., title="ingang of the case_type_result"
    )
    properties: CaseTypeResultProperties = Field(
        ..., title="properties of subject"
    )


class CaseTypeResultBasic(Entity):
    entity_type = "case_type_result_basic"

    uuid: UUID = Field(..., title="Uuid of the case_type_result")
    name: str | None = Field(..., title="Name of the case_type_result")
    result: str = Field(..., title="Result of the case_type_result")
    trigger_archival: bool = Field(..., title="Indicator for archive case")
    preservation_term_label: str = Field(
        ..., title="Label of the result_preservation_term"
    )
    preservation_term_unit: str | None = Field(
        ..., title="Unit of the result_preservation_term"
    )
    preservation_term_unit_amount: int | None = Field(
        ..., title="Unit amount of the result_preservation_term"
    )

    def _calculate_destruction_date(self, afhandeldatum: date | None):
        unit = self.preservation_term_unit
        amount = self.preservation_term_unit_amount
        if not unit or not afhandeldatum:
            # in case of 'Bewaren'
            return None

        delta = None
        if unit == "week":
            delta = relativedelta(weeks=amount)
        elif unit == "month":
            delta = relativedelta(months=amount)
        elif unit == "year":
            delta = relativedelta(years=amount)
        else:
            raise NotImplementedError(
                f"Calculate desctruction_date does not support unit {unit}"
            )

        return afhandeldatum + delta

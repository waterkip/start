# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty import entity
from pydantic.v1 import Field
from uuid import UUID


class File(entity.Entity):
    """Information about a file"""

    entity_type = "file"
    entity_id__fields: list[str] = ["uuid"]

    # Properties
    uuid: UUID = Field(..., title="UUID of the file")
    storage_location: list[str] = Field(
        ..., title="Storage location of the field"
    )
    mimetype: str = Field(..., title="Mimetype of the file")
    filename: str = Field(
        ..., title="Filename of the file including the extension"
    )
    download_url: str = Field(..., title="Download url for this file")

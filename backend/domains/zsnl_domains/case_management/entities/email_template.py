# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.entity import Entity
from pydantic.v1 import Field
from typing import Literal
from uuid import UUID


class EmailTemplate(Entity):
    entity_type: Literal["email_template"] = "email_template"
    entity_id__fields: list[str] = ["uuid"]

    uuid: UUID = Field(..., title="Identifier for this department")
    label: str = Field(..., title="Label for the email template")

    subject: str = Field(..., title="Subject of the email")
    body: str = Field(..., title="Email template body")

    sender: str = Field(default="", title="Email sender 'display name'")
    sender_address: str | None = Field(
        default=None, title="Email sender address"
    )

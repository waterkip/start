# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import enum
from ...shared.case_filters import CaseFilter
from . import _shared
from .case_type_version import CaseTypeMetaData, CaseTypePayment, CaseTypeTerms
from datetime import date, datetime
from minty.entity import Entity
from pydantic.v1 import Field
from typing import Literal
from uuid import UUID


class CaseSearch(Entity):
    """Dummy entity to pull in CaseFilter"""

    entity_type: Literal["case_search"]
    filter: CaseFilter


class CaseSearchResultPerson(_shared.RelatedPerson):
    entity_id__fields: list[str] = ["uuid"]

    type: str = Field(..., title="Type of the requestor of the case")
    name: str = Field(..., title="Name of the requestor of the case")


class CaseSearchResultOrganization(_shared.RelatedOrganization):
    entity_id__fields: list[str] = ["uuid"]

    type: str = Field(..., title="Type of the requestor of the case")
    name: str = Field(..., title="Name of the requestor of the case")


class CaseSearchResultEmployee(_shared.RelatedEmployee):
    entity_id__fields: list[str] = ["uuid"]
    entity_type: str = Field("employee", const=True)

    type: str = Field(..., title="Type of the requestor of the case")
    name: str = Field(..., title="Name of the requestor of the case")


class CaseSearchResultCaseType(Entity):
    entity_id__fields: list[str] = ["uuid"]
    entity_type: str = Field("case_type", const=True)

    name: str = Field(..., title="Name of the requestor of the case")


class CaseSearchResultCaseTypeResult(Entity):
    entity_id__fields: list[str] = ["uuid"]
    entity_type: str = Field("case_type_result", const=True)


class CaseSearchResultCaseLocation(Entity):
    entity_id__fields: list[str] = ["uuid"]
    entity_type: str = Field("case_location", const=True)
    type: str = Field(..., title="Case Location")


class CaseRoleDepartment(Entity):
    entity_id__fields: list[str] = ["uuid"]
    entity_type: str = Field("role", const=True)


class CaseSearchOrder(enum.StrEnum):
    number_asc = "number"
    unread_message_count_asc = "unread_message_count"
    unaccepted_files_count_asc = "unaccepted_files_count"
    unaccepted_attribute_update_count_asc = "unaccepted_attribute_update_count"
    registration_date_asc = "registration_date"
    completion_date_asc = "completion_date"

    number_desc = "-number"
    unread_message_count_desc = "-unread_message_count"
    unaccepted_files_count_desc = "-unaccepted_files_count"
    unaccepted_attribute_update_count_desc = (
        "-unaccepted_attribute_update_count"
    )
    registration_date_desc = "-registration_date"
    completion_date_desc = "-completion_date"


class CaseTypeVersionSearchResultEntity(Entity):
    entity_type = "case_type"
    entity_id__fields: list[str] = ["uuid"]

    # Properties
    uuid: UUID = Field(..., title="UUID of the case_type node")
    case_type_uuid: UUID = Field(..., title="UUID of the case_type")

    version: int = Field(
        ...,
        title="Version number of case_type",
    )

    name: str = Field(..., title="Name of case_type")
    created: datetime = Field(
        ...,
        title="Created date of case_type",
    )
    last_modified: datetime = Field(
        ...,
        title="Last modified date for case_type",
    )

    deleted: datetime | None = Field(
        None,
        title="Deleted date of case_type",
    )
    initiator_source: str | None = Field(
        None, title="Initiator source for case_type"
    )
    description: str | None = Field(None, title="Description of case_type")
    identification: str | None = Field(
        None, title="Identification for case_type"
    )
    is_public: bool | None = Field(
        False, title="Boolean indicates if case_type is public"
    )
    tags: str | None = Field(None, title="Tags for case_type")
    initiator_type: str | None = Field(
        None, title="Initiator type for case_type"
    )
    payment: CaseTypePayment = Field(..., title="Case type payment")
    case_summary: str | None = Field(None, title="Summary of case_type")
    case_public_summary: str | None = Field(
        None, title="Public summary of case_type"
    )
    terms: CaseTypeTerms = Field(..., title="Terms for a case_type")

    active: bool = Field(..., title="Active status of case_type")

    initiator_source: str | None = Field(
        None, title="Initiator source for case_type"
    )
    initiator_type: str | None = Field(
        None, title="Initiator type for case_type"
    )
    metadata: CaseTypeMetaData = Field(None, title="Meta data of case_type")


class CaseSearchResult(Entity):
    entity_type: Literal["case_search_result"] = "case_search_result"
    entity_id__fields: list[str] = ["uuid"]
    entity_relationships: list[str] = [
        "assignee",
        "requestor",
        "coordinator",
        "case_type",
        "case_type_result",
        "case_location",
        "role",
    ]

    uuid: UUID = Field(..., title="Uuid of the case")

    number: int = Field(..., title="Id of case")
    status: str = Field(..., title="Status of case")
    destruction_date: date | None = Field(
        None, title="Destruction date of the case"
    )
    archival_state: str | None = Field(
        None, title="Archival state of the case"
    )
    subject: str | None = Field(None, title="Subject of the case")

    subject_external: str | None = Field(
        None, title="External Subject of the case"
    )

    case_type: CaseSearchResultCaseType | None = Field(
        ..., title="Case type of the case"
    )

    case_type_title: str | None = Field(None, title="Title of the case_type")

    requestor: (
        CaseSearchResultPerson
        | CaseSearchResultOrganization
        | CaseSearchResultEmployee
    ) = Field(..., title="Requestor of the case")
    coordinator: CaseSearchResultEmployee | None = Field(
        ..., title="Coordinator of the case"
    )
    assignee: CaseSearchResultEmployee | None = Field(
        ..., title="Assignee of the case"
    )

    percentage_days_left: int | None = Field(
        None,
        title="Percentage of remaining days until completion.",
        description="Indicates with a percentage how much time is left to "
        + "complete the case. The percentage will be above 100 if the "
        + "remaining days are exeeded. When a case has the status stalled, "
        + "then the value is null",
    )

    progress: int = Field(..., title="Progress status of the case")

    unread_message_count: int = Field(..., title="Number of unread messages")
    unaccepted_files_count: int = Field(..., title="Count of unaccepted files")
    unaccepted_attribute_update_count: int = Field(
        ..., title="Count of unaccepted updates"
    )

    registration_date: date | None = Field(
        None, title="Registration date of the case"
    )

    completion_date: date | None = Field(
        None, title="Completion date of the case"
    )
    custom_fields: dict = Field(..., title="custom fields for the case")
    target_completion_date: date | None = Field(
        None, title="Target completion date for the case"
    )

    period_of_preservation_active: bool = Field(
        False, title="period of preservation active"
    )
    case_type_result: CaseSearchResultCaseTypeResult | None = Field(
        ..., title="Case type of the case"
    )
    case_location: CaseSearchResultCaseLocation | None = Field(
        ..., title="Case loation of the case"
    )
    stalled_until: date | None = Field(
        None, title="Stalled_until date of the case"
    )
    contact_channel: str = Field(
        ..., title="Contact channel used to register the case"
    )
    role: CaseRoleDepartment | None = Field(
        ..., title="Role/department case is assigned to"
    )

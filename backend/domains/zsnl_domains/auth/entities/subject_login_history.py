# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from datetime import datetime
from minty.entity import Entity
from pydantic.v1 import Field
from typing import Self
from uuid import UUID


class SubjectLoginHistory(Entity):
    "SubjectLoginHistory"

    entity_type = "subject_login_history"

    entity_id__fields: list[str] = ["id"]

    id: int | None = Field(default=None, title="Id of this entity")

    # Note: ip -> database type is inet
    ip: str = Field(
        ..., title="IP address of the client performing the login attempt"
    )

    subject_username: str = Field(
        default=None, title="Name of the subject performing the login attempt"
    )

    subject_id: int | None = Field(
        ..., title="ID of the subject performing the login attempt"
    )
    subject_uuid: UUID = Field(
        ...,
        title="Internal identifier of subject performing the login attempt",
    )
    success: bool = Field(
        ..., title="Indication of whether the login attempt was successful"
    )
    method: str = Field(default=None, title="Login method used")
    date_attempt: datetime = Field(
        default=None, title="Date on which the login attempt was made"
    )

    @classmethod
    @Entity.event(name="Auth0SubjectLoginHistoryCreated", fire_always=True)
    def create(cls, **kwargs) -> Self:
        subject_login_history: Self = cls(**kwargs)
        return subject_login_history

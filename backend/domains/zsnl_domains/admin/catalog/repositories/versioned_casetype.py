# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
from .... import ZaaksysteemRepositoryBase
from ..entities import versioned_casetype as vcte  # VersionedCaseTypeEntities
from .versioned_casetype_commands import CasetypeBuilder
from datetime import date, datetime
from decimal import Decimal, InvalidOperation
from minty.cqrs import Event, UserInfo
from minty.exceptions import NotFound
from sqlalchemy import sql
from typing import cast
from uuid import UUID
from zsnl_domains.database import schema


class VersionedCaseTypeRepository(ZaaksysteemRepositoryBase):
    _for_entity = "VersionedCaseType"
    _events_to_calls = {
        "VersionedCasetypeCreated": "_create_versioned_casetype",
        "VersionedCasetypeUpdated": "_update_versioned_casetype",
    }

    def find_version_uuid(self, uuid: UUID) -> UUID:
        """Return the latest version uuid for the casetype, based on the version independent uuid"""
        query = (
            sql.select(schema.ZaaktypeNode.uuid)
            .where(
                schema.ZaaktypeNode.id
                == sql.select(schema.Zaaktype.zaaktype_node_id)
                .where(schema.Zaaktype.uuid == uuid)
                .scalar_subquery()
            )
            .limit(1)
        )
        version_independent_uuid = self.session.execute(query).fetchone()
        if not version_independent_uuid:
            raise NotFound(
                f"Version dependent uuid for casetype with uuid '{uuid}' not found."
            )

        return version_independent_uuid.uuid

    def find_by_uuid(
        self, uuid: UUID, user_info: minty.cqrs.UserInfo
    ) -> vcte.VersionedCaseType:
        query_result = self._get_case_type_version(uuid, user_info)
        return self._transform_to_entity(query_result=query_result)

    def transform_price(self, value: str | None) -> Decimal | None:
        if value is None:
            return None
        try:
            # only a price in format of numbers and optionally a .
            # including max 2 number for cents is allowed
            # Examples; "1" "12" "12.3" "12.34"
            # Examples not valid; "1,23" "100.000,00" "100.000.00" "1.123"
            precision = Decimal("0.01")
            number = Decimal(value)
            return number.quantize(precision)
        except InvalidOperation:
            return None

    def _transform_prices(self, web_form: dict) -> dict:
        # prices can be stored in an invalid format (legacy issue)
        # try to convert the value to a Decimal value, or else change it to None
        # This is needed, otherwise cases with invalid price info will not load
        for price in [
            "web",
            "frontdesk",
            "phone",
            "email",
            "assignee",
            "post",
            "chat",
            "other",
        ]:
            web_form["price"][price] = self.transform_price(
                web_form["price"].get(price, None)
            )
        return web_form

    def _transform_monitoring_period_date(
        self, general_attributes: dict
    ) -> dict:
        fields: list[str] = ["legal_period", "service_period"]
        for field in fields:
            period = general_attributes[field]
            if (
                period
                and period["type"] == vcte.PeriodType.vaste_einddatum
                and period["value"] is not None
            ):
                try:
                    general_attributes[field]["value"] = datetime.strptime(
                        general_attributes[field]["value"], "%d-%m-%Y"
                    ).strftime("%Y-%m-%d")
                except ValueError:
                    # if converting the date fails, set the value to None
                    # there are casetypes with invalid data in the database.
                    # this way, the user is still able to load the casetype, and adjust it properly.
                    general_attributes[field]["value"] = None

        return general_attributes

    def __try_convert_date(self, value: str) -> str:
        # input of date can be yyyy-mm-dd or dd-mm-yyyy
        # this method will convert to yyyy-mm-dd if needed
        result = value
        try:
            # test if format is already iso
            date.fromisoformat(value)
        except ValueError:
            # if not iso, convert.
            result = datetime.strptime(value, "%d-%m-%Y").strftime("%Y-%m-%d")
        return result

    def _transform_phases(self, phases: list):
        for phase in phases or []:
            for case in phase.get("cases") or []:
                if case.get("start_after_fixeddate"):
                    # transform date form format dd-mm-yyyy to yyyy-mm-dd
                    case["start_after_fixeddate"] = self.__try_convert_date(
                        case["start_after_fixeddate"]
                    )
                else:
                    case["start_after_fixeddate"] = None

        return phases

    def _transform_to_entity(self, query_result) -> vcte.VersionedCaseType:
        obj = vcte.VersionedCaseType.parse_obj(
            {
                "uuid": query_result.uuid,
                "casetype_uuid": query_result.casetype_uuid,
                "active": query_result.active,
                "catalog_folder": query_result.catalog_folder,
                "general_attributes": self._transform_monitoring_period_date(
                    query_result.general_attributes
                ),
                "documentation": query_result.documentation,
                "relations": query_result.relations,
                "webform": self._transform_prices(query_result.webform),
                "registrationform": query_result.registrationform,
                "case_dossier": query_result.case_dossier,
                "api": query_result.api,
                "authorization": query_result.authorization,
                "child_casetype_settings": query_result.child_casetype_settings,
                "change_log": None,
                "phases": self._transform_phases(query_result.phases),
                "results": query_result.results,
            }
        )
        return obj

    def _get_case_type_version(
        self, case_type_version_uuid: UUID, user_info: minty.cqrs.UserInfo
    ) -> dict:
        query = (
            sql.select(
                schema.ViewCaseTypeVersionV2.uuid,
                schema.ViewCaseTypeVersionV2.casetype_uuid,
                schema.ViewCaseTypeVersionV2.active,
                schema.ViewCaseTypeVersionV2.catalog_folder,
                schema.ViewCaseTypeVersionV2.general_attributes,
                schema.ViewCaseTypeVersionV2.documentation,
                schema.ViewCaseTypeVersionV2.relations,
                schema.ViewCaseTypeVersionV2.webform,
                schema.ViewCaseTypeVersionV2.registrationform,
                schema.ViewCaseTypeVersionV2.case_dossier,
                schema.ViewCaseTypeVersionV2.api,
                schema.ViewCaseTypeVersionV2.authorization,
                schema.ViewCaseTypeVersionV2.child_casetype_settings,
                schema.ViewCaseTypeVersionV2.phases,
                schema.ViewCaseTypeVersionV2.results,
            )
            .select_from(schema.ViewCaseTypeVersionV2)
            .where(schema.ViewCaseTypeVersionV2.uuid == case_type_version_uuid)
        )

        case_type_version = self.session.execute(query).fetchone()
        if not case_type_version:
            raise NotFound(
                f"VersionedCaseType with uuid '{case_type_version_uuid}' not found."
            )
        return case_type_version

    def _create_versioned_casetype(
        self, event: Event, user_info: UserInfo, dry_run: bool = False
    ):
        CasetypeBuilder().create_casetype(
            session=self.session,
            changes=event.format_changes(),
            user_info=event.user_info,
        )

    def _update_versioned_casetype(
        self, event: Event, user_info: UserInfo, dry_run: bool = False
    ):
        CasetypeBuilder().update_casetype(
            session=self.session,
            changes=event.format_changes(),
            user_info=event.user_info,
        )

    def create_versioned_casetype(
        self,
        uuid: UUID,
        casetype_uuid: UUID,
        active: bool,
        catalog_folder: vcte.CatalogFolder | None,
        authorization: list[vcte.Authorization],
        general_attributes: vcte.GeneralAttributes,
        documentation: vcte.Documentation,
        relations: vcte.Relations,
        webform: vcte.WebForm,
        registrationform: vcte.RegistrationForm,
        case_dossier: vcte.CaseDossier,
        api: vcte.ApiSettings,
        change_log: vcte.ChangeLog,
        phases: list[vcte.Phase],
        results: list[vcte.Result],
        child_casetype_settings: vcte.MotherCaseType | None = None,
    ) -> vcte.VersionedCaseType:
        versioned_casetype = cast(
            vcte.VersionedCaseType,
            vcte.VersionedCaseType.create(
                uuid=uuid,
                casetype_uuid=casetype_uuid,
                active=active,
                catalog_folder=catalog_folder,
                general_attributes=general_attributes,
                documentation=documentation,
                relations=relations,
                webform=webform,
                registrationform=registrationform,
                case_dossier=case_dossier,
                api=api,
                authorization=authorization,
                child_casetype_settings=child_casetype_settings,
                change_log=change_log,
                phases=phases,
                results=results,
                _event_service=self.event_service,
            ),
        )
        return versioned_casetype

    def update_versioned_casetype(
        self,
        uuid: UUID,
        casetype_uuid: UUID,
        active: bool,
        catalog_folder: vcte.CatalogFolder | None,
        authorization: list[vcte.Authorization],
        general_attributes: vcte.GeneralAttributes,
        documentation: vcte.Documentation,
        relations: vcte.Relations,
        webform: vcte.WebForm,
        registrationform: vcte.RegistrationForm,
        case_dossier: vcte.CaseDossier,
        api: vcte.ApiSettings,
        change_log: vcte.ChangeLog,
        phases: list[vcte.Phase],
        results: list[vcte.Result],
        child_casetype_settings: vcte.MotherCaseType | None = None,
    ) -> vcte.VersionedCaseType:
        versioned_casetype = cast(
            vcte.VersionedCaseType,
            vcte.VersionedCaseType.update(
                uuid=uuid,
                casetype_uuid=casetype_uuid,
                active=active,
                catalog_folder=catalog_folder,
                general_attributes=general_attributes,
                documentation=documentation,
                relations=relations,
                webform=webform,
                registrationform=registrationform,
                case_dossier=case_dossier,
                api=api,
                authorization=authorization,
                child_casetype_settings=child_casetype_settings,
                change_log=change_log,
                phases=phases,
                results=results,
                _event_service=self.event_service,
            ),
        )
        return versioned_casetype

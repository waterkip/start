# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from abc import ABC, abstractmethod
from collections.abc import Callable
from typing import Generic, TypeVar

E = TypeVar("E")  # Changes (entity)
C = TypeVar("C")  # Context


class DbCommand(ABC, Generic[E, C]):
    _context: C

    def __init__(self, session: Callable, context: C):
        self.session = session
        self._context = context

    @abstractmethod
    def execute(
        self,
        changes: E,
    ) -> None:
        raise NotImplementedError("Abstract method not implemented.")

    def get_context(self) -> C:
        return self._context

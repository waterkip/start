# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .....shared.util import getattr_from_dict
from .casetype_context import CasetypeContext
from .db_command import DbCommand
from sqlalchemy import sql
from zsnl_domains.database import schema


class InsertZaaktypeBetrokkenenCommand(DbCommand[dict, CasetypeContext]):
    def execute(
        self,
        changes: dict,
    ) -> None:
        betrokkenen_to_insert = []
        if getattr_from_dict(changes, "relations") and getattr_from_dict(
            changes, "relations.allowed_requestor_types"
        ):
            for requestor_type in getattr_from_dict(
                changes, "relations.allowed_requestor_types"
            ):
                betrokkenen_to_insert.append(
                    {
                        "betrokkene_type": requestor_type,
                        "zaaktype_node_id": self.get_context().get_zaaktype_node_id(),
                    }
                )

            if len(betrokkenen_to_insert) > 0:
                self.session.execute(
                    sql.insert(schema.ZaaktypeBetrokkenen).values(
                        betrokkenen_to_insert
                    )
                )

# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .....shared.util import getattr_from_dict
from .casetype_context import CasetypeContext
from .db_command import DbCommand
from sqlalchemy import sql
from zsnl_domains.database import schema


class InsertZaaktypeTasksCommand(DbCommand[dict, CasetypeContext]):
    def execute(
        self,
        changes: dict,
    ) -> None:
        for phase_count, phase in enumerate(
            getattr_from_dict(changes, "phases", default_val=[]), 1
        ):
            tasks = getattr_from_dict(phase, "tasks")
            tasks_to_insert = []
            for task in tasks or []:
                tasks_to_insert.append(
                    {
                        "casetype_status_id": self.get_context().get_phase_id(
                            phase_count
                        ),
                        "label": task,
                    }
                )

            if len(tasks_to_insert) > 0:
                self.session.execute(
                    sql.insert(schema.ZaaktypeChecklistItem).values(
                        tasks_to_insert
                    )
                )

# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2
from .....shared.util import getattr_from_dict
from .casetype_context import CasetypeContext, DbKenmerk
from .db_command import DbCommand
from minty.exceptions import NotFound
from sqlalchemy import sql
from uuid import UUID
from zsnl_domains.database import schema


class AssertKenmerkenExist(DbCommand[dict, CasetypeContext]):
    def execute(
        self,
        changes: dict,
    ) -> None:
        self.__assert_kenmerken_by_uuid(changes)

    def __assert_kenmerken_by_uuid(self, changes: dict):
        """Check for changes if all kenmerken, referenced by uuid exist in the db
        If one of the kenmerken does not exist, raise an exception
        If all kenmerken exist, the id and name of the kenmerk is stored in the CasetypeContext,
        so they can be used when inserting the casetype records. Query doesn't have to be performed again then
        """
        kenmerken_uuid: set = set()

        for phase in getattr_from_dict(changes, "phases", default_val=[]):
            for custom_field in getattr_from_dict(
                phase, "custom_fields", default_val=[]
            ):
                kenmerken_uuid.add(getattr_from_dict(custom_field, "uuid"))
                kenmerken_uuid.add(
                    getattr_from_dict(
                        custom_field, "start_date_limitation.reference"
                    )
                )
                kenmerken_uuid.add(
                    getattr_from_dict(
                        custom_field, "end_date_limitation.reference"
                    )
                )
            for case in getattr_from_dict(phase, "cases", []):
                mapping = case.get("copy_attributes_mapping", {})
                if mapping:
                    kenmerken_uuid.update(mapping.values())

        # remove nonevalues and "currentDate". Only magic_strings of attributes should stay
        kenmerken_uuid = {
            UUID(str(k))
            for k in kenmerken_uuid
            if k is not None and k != "currentDate" and k != ""
        }

        # select kenmerken from db and store result in context
        kenmerken_db = [
            DbKenmerk(
                id=r.id,
                name=r.naam_public,
                magic_string=r.magic_string,
                uuid=r.uuid,
            )
            for r in self.session.execute(
                sql.select(
                    schema.BibliotheekKenmerk.id,
                    schema.BibliotheekKenmerk.naam_public,
                    schema.BibliotheekKenmerk.magic_string,
                    schema.BibliotheekKenmerk.uuid,
                ).where(
                    sql.and_(
                        schema.BibliotheekKenmerk.deleted.is_(None),
                        schema.BibliotheekKenmerk.uuid.in_(kenmerken_uuid),
                    )
                )
            ).fetchall()
        ]
        diff = kenmerken_uuid.difference(
            [UUID(str(k.uuid)) for k in kenmerken_db if k is not None]
        )
        if len(diff) > 0:
            raise NotFound(
                f"Attribute(s) with uuid {', '.join(str(diff))} not found."
            )

        for v in kenmerken_db:
            self.get_context().add_attribute(attribute=v)

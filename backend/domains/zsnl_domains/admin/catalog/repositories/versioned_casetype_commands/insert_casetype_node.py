# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .....shared.util import getattr_from_dict
from . import util
from .casetype_context import CasetypeContext
from .db_command import DbCommand
from sqlalchemy import sql
from zsnl_domains.database import schema


class InsertZaaktypeNodeCommand(DbCommand[dict, CasetypeContext]):
    def execute(
        self,
        changes: dict,
    ) -> None:
        id = self.session.execute(
            sql.insert(schema.ZaaktypeNode)
            .values(
                version=self.__get_next_zaaktype_node_version(
                    self.get_context().get_zaaktype_id()
                ),
                zaaktype_id=self.get_context().get_zaaktype_id(),
                zaaktype_definitie_id=self.get_context().get_zaaktype_definitie_id(),
                code=getattr_from_dict(
                    changes,
                    "general_attributes.identification",
                    default_val="",
                ),
                trigger=getattr_from_dict(changes, "relations.trigger"),
                titel=getattr_from_dict(changes, "general_attributes.name"),
                webform_toegang=util.bool_to_int(
                    getattr_from_dict(
                        changes,
                        "webform.actions.enable_webform",
                        default_val=False,
                    )
                ),
                aanvrager_hergebruik=util.bool_to_int(
                    getattr_from_dict(
                        changes,
                        "webform.actions.reuse_casedata",
                        default_val=False,
                    )
                ),
                automatisch_behandelen=util.bool_to_int(
                    getattr_from_dict(
                        changes,
                        "registrationform.allow_assigning_to_self",
                        default_val=False,
                    )
                ),
                toewijzing_zaakintake=util.bool_to_int(
                    getattr_from_dict(
                        changes,
                        "registrationform.allow_assigning",
                        default_val=False,
                    )
                ),
                online_betaling=util.bool_to_int(
                    getattr_from_dict(
                        changes,
                        "webform.actions.enable_online_payment",
                        default_val=False,
                    )
                ),
                adres_andere_locatie=util.bool_to_int(
                    getattr_from_dict(
                        changes,
                        "relations.address_requestor_use_as_case_address",
                        default_val=False,
                    )
                ),
                adres_aanvrager=util.bool_to_int(
                    getattr_from_dict(
                        changes,
                        "relations.address_requestor_use_as_correspondence",
                        default_val=False,
                    )
                ),
                zaaktype_trefwoorden=getattr_from_dict(
                    changes, "general_attributes.tags"
                ),
                zaaktype_omschrijving=getattr_from_dict(
                    changes, "general_attributes.description"
                ),
                extra_relaties_in_aanvraag=getattr_from_dict(
                    changes, "registrationform.allow_add_relations"
                ),
                contact_info_intake=getattr_from_dict(
                    changes,
                    "registrationform.show_contact_details",
                    default_val=False,
                ),
                is_public=getattr_from_dict(
                    changes, "api.is_public", default_val=False
                ),
                prevent_pip=getattr_from_dict(
                    changes,
                    "case_dossier.disable_pip_for_requestor",
                    default_val=False,
                ),
                contact_info_email_required=getattr_from_dict(
                    changes,
                    "webform.actions.email_required",
                    default_val=False,
                ),
                contact_info_phone_required=getattr_from_dict(
                    changes,
                    "webform.actions.phone_required",
                    default_val=False,
                ),
                contact_info_mobile_phone_required=getattr_from_dict(
                    changes,
                    "webform.actions.mobile_required",
                    default_val=False,
                ),
                adres_geojson=getattr_from_dict(
                    changes,
                    "relations.address_requestor_show_on_map",
                    default_val=False,
                ),
                properties=self.__build_zaaktype_node_properties(
                    changes=changes
                ),
            )
            .returning(schema.ZaaktypeNode.id)
        ).fetchone()
        return self.get_context().set_zaaktype_node_id(id.id)

    def __get_next_zaaktype_node_version(self, zaaktype_id: int):
        max_version_statement = (
            sql.select(
                sql.expression.func.max(schema.ZaaktypeNode.version).label(
                    "max_version"
                )
            )
            .select_from(schema.ZaaktypeNode)
            .where(schema.ZaaktypeNode.zaaktype_id == zaaktype_id)
        )
        max_row = self.session.execute(max_version_statement).fetchone()
        return 1 if not max_row.max_version else max_row.max_version + 1

    def __build_zaaktype_node_properties(self, changes: dict) -> dict:
        properties = {}

        if getattr_from_dict(changes, "documentation"):
            self.__update_documentation(
                properties=properties,
                documentation=getattr_from_dict(changes, "documentation"),
            )

        if getattr_from_dict(changes, "relations") and getattr_from_dict(
            changes, "relations.api_preset_assignee"
        ):
            properties.update(
                {
                    "preset_owner_identifier": util.create_legacy_subject_id(
                        session=self.session,
                        subject_uuid=getattr_from_dict(
                            changes, "relations.api_preset_assignee.uuid"
                        ),
                        subject_type=getattr_from_dict(
                            changes, "relations.api_preset_assignee.type"
                        ),
                    )
                }
            )
        if getattr_from_dict(changes, "webform"):
            self.__update_webform(
                properties=properties,
                webform=getattr_from_dict(changes, "webform"),
            )

        if getattr_from_dict(changes, "registrationform"):
            self.__update_registrationform(
                properties=properties,
                registrationform=getattr_from_dict(
                    changes, "registrationform"
                ),
            )
        if getattr_from_dict(changes, "case_dossier"):
            self.__update_case_dossier(
                properties=properties,
                case_dossier=getattr_from_dict(changes, "case_dossier"),
            )

        if getattr_from_dict(changes, "api"):
            self.__update_api_settings(
                properties=properties,
                api_settings=getattr_from_dict(changes, "api"),
            )

        if getattr_from_dict(changes, "child_casetype_settings"):
            self.__update_child_casetype_settings(
                properties=properties,
                child_casetype_settings=getattr_from_dict(
                    changes, "child_casetype_settings"
                ),
            )
        self.__delete_none(properties)
        return properties

    def __delete_none(self, _dict):
        """Delete None values recursively from all of the dictionaries"""
        for key, value in list(_dict.items()):
            if isinstance(value, dict):
                self.__delete_none(value)
            elif value is None:
                del _dict[key]
            elif isinstance(value, list):
                for v_i in value:
                    if isinstance(v_i, dict):
                        self.__delete_none(v_i)

        return _dict

    def __update_documentation(self, properties: dict, documentation: dict):
        properties.update(
            {
                "aanleiding": getattr_from_dict(documentation, "motivation"),
                "doel": getattr_from_dict(documentation, "purpose"),
                "archiefclassicatiecode": getattr_from_dict(
                    documentation, "archive_classification_code"
                ),
                "vertrouwelijkheidsaanduiding": getattr_from_dict(
                    documentation, "designation_of_confidentiality"
                ),
                "verantwoordelijke": getattr_from_dict(
                    documentation, "responsible_subject"
                ),
                "verantwoordingsrelatie": getattr_from_dict(
                    documentation, "responsible_relationship"
                ),
                "beroep_mogelijk": util.bool_to_ja_nee(
                    getattr_from_dict(
                        documentation, "possibility_for_objection_and_appeal"
                    )
                ),
                "publicatie": util.bool_to_ja_nee(
                    getattr_from_dict(documentation, "publication")
                ),
                "publicatietekst": getattr_from_dict(
                    documentation, "publication_text"
                ),
                "bag": util.bool_to_ja_nee(
                    getattr_from_dict(documentation, "bag")
                ),
                "lex_silencio_positivo": util.bool_to_ja_nee(
                    getattr_from_dict(documentation, "lex_silencio_positivo")
                ),
                "opschorten_mogelijk": util.bool_to_ja_nee(
                    getattr_from_dict(documentation, "may_postpone")
                ),
                "verlenging_mogelijk": util.bool_to_ja_nee(
                    getattr_from_dict(documentation, "may_extend")
                ),
                "verlengingstermijn": getattr_from_dict(
                    documentation, "extension_period"
                ),
                "verdagingstermijn": getattr_from_dict(
                    documentation, "adjourn_period"
                ),
                "wet_dwangsom": util.bool_to_ja_nee(
                    getattr_from_dict(documentation, "penalty_law")
                ),
                "wkpb": util.bool_to_ja_nee(
                    getattr_from_dict(documentation, "wkpb_applies")
                ),
                "e_formulier": getattr_from_dict(documentation, "e_webform"),
                "lokale_grondslag": getattr_from_dict(
                    documentation, "local_basis"
                ),
            }
        )
        if getattr_from_dict(documentation, "gdpr"):
            properties.update(
                {
                    "gdpr": {
                        "enabled": util.bool_to_ja_nee(
                            getattr_from_dict(documentation, "gdpr.enabled")
                        ),
                        "processing_type": getattr_from_dict(
                            documentation, "gdpr.processing_type"
                        ),
                        "process_foreign_country": util.bool_to_ja_nee(
                            getattr_from_dict(
                                documentation, "gdpr.process_foreign_country"
                            )
                        ),
                        "process_foreign_country_reason": getattr_from_dict(
                            documentation,
                            "gdpr.process_foreign_country_reason",
                        ),
                        "processing_legal": getattr_from_dict(
                            documentation, "gdpr.processing_legal"
                        ),
                        "processing_legal_reason": getattr_from_dict(
                            documentation, "gdpr.processing_legal_reason"
                        ),
                        "personal": {
                            "basic_details": util.bool_to_on_none(
                                getattr_from_dict(
                                    documentation, "gdpr.kind.basic_details"
                                )
                            ),
                            "personal_id_number": util.bool_to_on_none(
                                getattr_from_dict(
                                    documentation,
                                    "gdpr.kind.personal_id_number",
                                )
                            ),
                            "income": util.bool_to_on_none(
                                getattr_from_dict(
                                    documentation, "gdpr.kind.income"
                                )
                            ),
                            "race_or_ethniticy": util.bool_to_on_none(
                                getattr_from_dict(
                                    documentation,
                                    "gdpr.kind.race_or_ethniticy",
                                )
                            ),
                            "political_views": util.bool_to_on_none(
                                getattr_from_dict(
                                    documentation, "gdpr.kind.political_views"
                                )
                            ),
                            "religion": util.bool_to_on_none(
                                getattr_from_dict(
                                    documentation, "gdpr.kind.religion"
                                )
                            ),
                            "membership_union": util.bool_to_on_none(
                                getattr_from_dict(
                                    documentation, "gdpr.kind.membership_union"
                                )
                            ),
                            "genetic_or_biometric_data": util.bool_to_on_none(
                                getattr_from_dict(
                                    documentation,
                                    "gdpr.kind.genetic_or_biometric_data",
                                )
                            ),
                            "health": util.bool_to_on_none(
                                getattr_from_dict(
                                    documentation, "gdpr.kind.health"
                                )
                            ),
                            "sexual_identity": util.bool_to_on_none(
                                getattr_from_dict(
                                    documentation, "gdpr.kind.sexual_identity"
                                )
                            ),
                            "criminal_record": util.bool_to_on_none(
                                getattr_from_dict(
                                    documentation, "gdpr.kind.criminal_record"
                                )
                            ),
                            "offspring": util.bool_to_on_none(
                                getattr_from_dict(
                                    documentation, "gdpr.kind.offspring"
                                )
                            ),
                        },
                        "source_personal": {
                            "public_source": util.bool_to_on_none(
                                getattr_from_dict(
                                    documentation, "gdpr.source.public_source"
                                )
                            ),
                            "registration": util.bool_to_on_none(
                                getattr_from_dict(
                                    documentation, "gdpr.source.registration"
                                )
                            ),
                            "partner": util.bool_to_on_none(
                                getattr_from_dict(
                                    documentation, "gdpr.source.partner"
                                )
                            ),
                            "sender": util.bool_to_on_none(
                                getattr_from_dict(
                                    documentation, "gdpr.source.sender"
                                )
                            ),
                        },
                    },
                }
            )

    def __update_webform(self, properties: dict, webform: dict):
        properties.update(
            {
                "public_confirmation_message": getattr_from_dict(
                    webform, "public_confirmation_message"
                ),
                "public_confirmation_title": getattr_from_dict(
                    webform, "public_confirmation_title"
                ),
                "case_location_message": getattr_from_dict(
                    webform, "case_location_message"
                ),
                "pip_view_message": getattr_from_dict(
                    webform, "pip_view_message"
                ),
            }
        )
        if getattr_from_dict(webform, "actions"):
            properties.update(
                {
                    "delayed": getattr_from_dict(
                        webform, "actions.create_delayed"
                    ),
                    "case_location_check": getattr_from_dict(
                        webform, "actions.address_check"
                    ),
                    "offline_betaling": getattr_from_dict(
                        webform, "actions.enable_manual_payment"
                    ),
                    "no_captcha": getattr_from_dict(
                        webform, "actions.disable_captcha"
                    ),
                    "pdf_registration_form": getattr_from_dict(
                        webform, "actions.generate_pdf_end_webform"
                    ),
                }
            )
        if getattr_from_dict(webform, "price"):
            properties.update(
                {
                    "pdc_tarief_balie": getattr_from_dict(
                        webform, "price.frontdesk"
                    ),
                    "pdc_tarief_telefoon": getattr_from_dict(
                        webform, "price.phone"
                    ),
                    "pdc_tarief_email": getattr_from_dict(
                        webform, "price.email"
                    ),
                    "pdc_tarief_behandelaar": getattr_from_dict(
                        webform, "price.assignee"
                    ),
                    "pdc_tarief_post": getattr_from_dict(
                        webform, "price.post"
                    ),
                    "pdc_tarief_chat": getattr_from_dict(
                        webform, "price.chat"
                    ),
                    "pdc_tarief_overige": getattr_from_dict(
                        webform, "price.other"
                    ),
                }
            )

    def __update_registrationform(
        self, properties: dict, registrationform: dict
    ):
        properties.update(
            {
                "confidentiality": getattr_from_dict(
                    registrationform, "show_confidentionality"
                ),
            }
        )

    def __update_case_dossier(self, properties: dict, case_dossier: dict):
        properties.update(
            {
                "lock_registration_phase": getattr_from_dict(
                    case_dossier, "lock_registration_phase"
                ),
                "queue_coworker_changes": getattr_from_dict(
                    case_dossier, "queue_coworker_changes"
                ),
                "allow_external_task_assignment": getattr_from_dict(
                    case_dossier, "allow_external_task_assignment"
                ),
                "default_directories": getattr_from_dict(
                    case_dossier, "default_document_folders"
                ),
                "default_html_email_template": getattr_from_dict(
                    case_dossier, "default_html_email_template"
                ),
            }
        )

    def __update_api_settings(self, properties: dict, api_settings: dict):
        properties.update(
            {
                "api_can_transition": getattr_from_dict(
                    api_settings, "api_can_transition"
                )
            }
        )
        if getattr_from_dict(api_settings, "notifications"):
            properties.update(
                {
                    "notify_on_new_case": getattr_from_dict(
                        api_settings,
                        "notifications.external_notify_on_new_case",
                    ),
                    "notify_on_new_document": getattr_from_dict(
                        api_settings,
                        "notifications.external_notify_on_new_document",
                    ),
                    "notify_on_new_message": getattr_from_dict(
                        api_settings,
                        "notifications.external_notify_on_new_message",
                    ),
                    "notify_on_exceed_term": getattr_from_dict(
                        api_settings,
                        "notifications.external_notify_on_exceed_term",
                    ),
                    "notify_on_allocate_case": getattr_from_dict(
                        api_settings,
                        "notifications.external_notify_on_allocate_case",
                    ),
                    "notify_on_phase_transition": getattr_from_dict(
                        api_settings,
                        "notifications.external_notify_on_phase_transition",
                    ),
                    "notify_on_task_change": getattr_from_dict(
                        api_settings,
                        "notifications.external_notify_on_task_change",
                    ),
                    "notify_on_label_change": getattr_from_dict(
                        api_settings,
                        "notifications.external_notify_on_label_change",
                    ),
                    "notify_on_subject_change": getattr_from_dict(
                        api_settings,
                        "notifications.external_notify_on_subject_change",
                    ),
                }
            )

    def __update_child_casetype_settings(
        self, properties: dict, child_casetype_settings: dict
    ):
        properties.update(
            {
                "child_casetype_settings": {
                    "enabled": getattr_from_dict(
                        child_casetype_settings, "enabled"
                    ),
                    "child_casetypes": [
                        {
                            "enabled": getattr_from_dict(child, "enabled"),
                            "casetype": {
                                "uuid": str(
                                    getattr_from_dict(child, "casetype.uuid")
                                ),
                                "name": getattr_from_dict(
                                    child, "casetype.name"
                                ),
                            },
                            "settings": getattr_from_dict(child, "settings"),
                        }
                        for child in getattr_from_dict(
                            child_casetype_settings, "child_casetypes"
                        )
                    ],
                }
            }
        )

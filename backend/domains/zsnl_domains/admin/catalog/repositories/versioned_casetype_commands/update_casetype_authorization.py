# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .....shared.util import getattr_from_dict
from .casetype_context import CasetypeContext
from .db_command import DbCommand
from sqlalchemy import sql
from zsnl_domains.database import schema


class UpdateZaaktypeAuthorizationCommand(DbCommand[dict, CasetypeContext]):
    def execute(
        self,
        changes: dict,
    ) -> None:
        zaaktype_id = self.get_context().get_zaaktype_id()
        # authorizations are only stored for the current version.
        # First delete all old authorizations of this casetype.
        # After that insert the new rights
        self.session.execute(
            sql.delete(schema.ZaaktypeAuthorisation).where(
                schema.ZaaktypeAuthorisation.zaaktype_id == zaaktype_id
            )
        )

        autorisations_to_insert = []
        for autorisation in getattr_from_dict(changes, "authorization"):
            for right in self.__inflate_rights(
                getattr_from_dict(autorisation, "rights")
            ):
                autorisations_to_insert.append(
                    {
                        "zaaktype_node_id": self.get_context().get_zaaktype_node_id(),
                        "recht": right,
                        "role_id": self.get_context()
                        .get_role(getattr_from_dict(autorisation, "role_uuid"))
                        .id,
                        "ou_id": self.get_context()
                        .get_group(
                            getattr_from_dict(autorisation, "department_uuid")
                        )
                        .id,
                        "zaaktype_id": zaaktype_id,
                        "confidential": getattr_from_dict(
                            autorisation, "confidential"
                        ),
                    }
                )
        if len(autorisations_to_insert) > 0:
            self.session.execute(
                sql.insert(schema.ZaaktypeAuthorisation).values(
                    autorisations_to_insert
                )
            )

    def __inflate_rights(self, rights: list[str]) -> set[str]:
        # If a casetype has a right of below list, then it has to also get all rights below it
        # zaak_beheer
        # zaak_edit
        # zaak_read
        # zaak_search
        result = set(rights)
        if "zaak_beheer" in rights:
            result.update(["zaak_edit", "zaak_read", "zaak_search"])
        elif "zaak_edit" in rights:
            result.update(["zaak_read", "zaak_search"])
        elif "zaak_read" in rights:
            result.update(["zaak_search"])
        return result

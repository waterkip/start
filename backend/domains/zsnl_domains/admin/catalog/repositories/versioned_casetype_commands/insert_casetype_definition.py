# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2
from .....shared.util import getattr_from_dict
from . import util
from .casetype_context import CasetypeContext
from .db_command import DbCommand
from datetime import datetime
from sqlalchemy import sql
from zsnl_domains.database import schema


class InsertZaaktypeDefinitionCommand(DbCommand[dict, CasetypeContext]):
    def execute(
        self,
        changes: dict,
    ) -> None:
        afhandeltermijn_type = getattr_from_dict(
            changes, "general_attributes.legal_period.type"
        )
        afhandeltermijn_value = getattr_from_dict(
            changes, "general_attributes.legal_period.value"
        )
        if (
            afhandeltermijn_type == "einddatum"
            and afhandeltermijn_value is not None
        ):
            # convert date back to value "dd-mm-yyyy" for compatibility v1
            afhandeltermijn_value = datetime.strptime(
                afhandeltermijn_value, "%Y-%m-%d"
            ).strftime("%d-%m-%Y")

        servicenorm_value = getattr_from_dict(
            changes, "general_attributes.service_period.value"
        )
        servicenorm_type = getattr_from_dict(
            changes, "general_attributes.service_period.type"
        )
        if servicenorm_type == "einddatum" and servicenorm_value is not None:
            # convert date back to value "dd-mm-yyyy" for compatibility v1
            servicenorm_value = datetime.strptime(
                servicenorm_value, "%Y-%m-%d"
            ).strftime("%d-%m-%Y")

        id = self.session.execute(
            sql.insert(schema.ZaaktypeDefinitie)
            .values(
                handelingsinitiator=getattr_from_dict(
                    changes, "documentation.initiator_type"
                ),
                grondslag=getattr_from_dict(
                    changes, "documentation.legal_basis"
                ),
                procesbeschrijving=getattr_from_dict(
                    changes, "documentation.process_description"
                ),
                afhandeltermijn=afhandeltermijn_value,
                afhandeltermijn_type=afhandeltermijn_type,
                servicenorm=servicenorm_value,
                servicenorm_type=servicenorm_type,
                pdc_tarief=getattr_from_dict(changes, "webform.price.web"),
                omschrijving_upl=None,
                aard=None,
                extra_informatie=getattr_from_dict(
                    changes, "general_attributes.case_summary"
                ),
                preset_client=util.create_legacy_subject_id(
                    session=self.session,
                    subject_uuid=getattr_from_dict(
                        changes, "relations.preset_requestor.uuid"
                    ),
                    subject_type=getattr_from_dict(
                        changes, "relations.preset_requestor.type"
                    ),
                ),
                extra_informatie_extern=getattr_from_dict(
                    changes, "general_attributes.case_public_summary"
                ),
            )
            .returning(schema.ZaaktypeDefinitie.id)
        ).fetchone()
        self.get_context().set_zaaktype_definitie_id(id.id)

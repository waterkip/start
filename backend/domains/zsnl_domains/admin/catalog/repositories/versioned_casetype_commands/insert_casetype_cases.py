# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .....shared.util import getattr_from_dict
from . import util
from .casetype_context import CasetypeContext
from .db_command import DbCommand
from datetime import datetime
from sqlalchemy import sql
from zsnl_domains.database import schema


class InsertZaaktypeCasesCommand(DbCommand[dict, CasetypeContext]):
    def execute(
        self,
        changes: dict,
    ) -> None:
        for phase_count, phase in enumerate(
            getattr_from_dict(changes, "phases", default_val=[]), 1
        ):
            cases = getattr_from_dict(phase, "cases")
            cases_to_insert = []
            for case in cases or []:
                cases_to_insert.append(
                    {
                        "zaaktype_node_id": self.get_context().get_zaaktype_node_id(),
                        "zaaktype_status_id": self.get_context().get_phase_id(
                            phase_count
                        ),
                        "relatie_zaaktype_id": self.get_context()
                        .get_casetype(uuid=case.get("related_casetype"))
                        .id,
                        "relatie_type": case.get("kind"),
                        "eigenaar_type": case.get("requestor_type"),
                        "kopieren_kenmerken": util.bool_to_int(
                            case.get("copy_all_attributes", False)
                        ),
                        "ou_id": self.get_context()
                        .get_group(
                            getattr_from_dict(
                                case, "allocation.department_uuid"
                            )
                        )
                        .id,
                        "role_id": self.get_context()
                        .get_role(
                            getattr_from_dict(case, "allocation.role_uuid")
                        )
                        .id,
                        "automatisch_behandelen": case.get("assign_automatic"),
                        "required": case.get("handle_in_phase"),
                        "betrokkene_authorized": case.get(
                            "subjects_involved_authorized_for_case"
                        ),
                        "betrokkene_notify": case.get(
                            "subjects_involved_email_notifcation"
                        ),
                        "betrokkene_role": case.get("subjects_involved_role"),
                        "betrokkene_role_set": util.bool_to_int(
                            case.get("add_subjects_involved")
                        ),
                        "betrokkene_prefix": case.get(
                            "subject_involved_magic_string"
                        ),
                        "show_in_pip": case.get("show_on_pip"),
                        "pip_label": case.get("pip_label"),
                        "subject_role": case.get("copy_subject_roles"),
                        "copy_subject_role": util.bool_to_int(
                            case.get("copy_subject_roles_enabled")
                        ),
                        "copy_related_cases": case.get("copy_case_relations"),
                        "copy_related_objects": case.get(
                            "copy_object_relations"
                        ),
                        "copy_selected_attributes": {
                            (
                                self.get_context().get_attribute_by_uuid(
                                    source_uuid
                                )
                            ).magic_string: (
                                self.get_context().get_attribute_by_uuid(
                                    target_uuid
                                )
                            ).magic_string
                            for source_uuid, target_uuid in (
                                case.get("copy_attributes_mapping") or {}
                            ).items()
                        },
                        "creation_style": case.get("creation_style"),
                        "eigenaar_id": util.create_legacy_subject_id(
                            session=self.session,
                            subject_uuid=getattr_from_dict(
                                case, "requestor.uuid"
                            ),
                            subject_type=getattr_from_dict(
                                case, "requestor.type"
                            ),
                        ),
                        "betrokkene_id": util.create_legacy_subject_id(
                            session=self.session,
                            subject_uuid=getattr_from_dict(
                                case, "subject_involved.uuid"
                            ),
                            subject_type=getattr_from_dict(
                                case, "subject_involved.type"
                            ),
                        ),
                        "start_delay": self.get_start_delay_value(case),
                        "status": case.get("automatic"),
                        "eigenaar_role": case.get("owner_role"),
                    }
                )
            if len(cases_to_insert) > 0:
                self.session.execute(
                    sql.insert(schema.ZaaktypeRelatie).values(cases_to_insert)
                )

    def get_start_delay_value(self, case: dict):
        result = None
        if case["kind"] == "vervolgzaak_datum" and case.get(
            "start_after_fixeddate"
        ):
            # parse fixed date to dd-mm-yyyy (backwards compatible ctmv1)
            date_value: str = case["start_after_fixeddate"]
            result = datetime.strptime(date_value, "%Y-%m-%d").strftime(
                "%d-%m-%Y"
            )
        elif case["kind"] == "vervolgzaak" and case.get("start_after"):
            # can be int or magicstring
            result = case["start_after"]

        return result

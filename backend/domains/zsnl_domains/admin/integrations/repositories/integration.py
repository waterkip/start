# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .... import ZaaksysteemRepositoryBase
from ..entities.integration import INTEGRATION_MANUAL_TYPE, Integration
from sqlalchemy import sql
from typing import Final
from typing_extensions import TypedDict
from uuid import UUID
from zsnl_domains.database import schema

INTEGRATION_QUERY: Final = (
    sql.select(
        schema.Interface.uuid,
        schema.Interface.id,
        schema.Interface.name,
        schema.Interface.active,
        schema.Interface.module,
    )
    .where(schema.Interface.date_deleted.is_(None))
    .order_by(
        sql.desc(schema.Interface.active),
        sql.asc(schema.Interface.name),
    )
)


class IntegrationModuleConfig(TypedDict):
    manual_type: INTEGRATION_MANUAL_TYPE


INTEGRATION_MODULE_CONFIGS: Final[dict[str, IntegrationModuleConfig]] = {
    "api": {"manual_type": {"text"}},
    "app_meeting": {"manual_type": {"text"}},
    "app_mor": {"manual_type": {"text"}},
    "app_pdc": {"manual_type": {"text"}},
    "app_word": {"manual_type": {"text"}},
    "appointment": {"manual_type": set()},
    "auth_twofactor": {"manual_type": set()},
    "authjwt": {"manual_type": set()},
    "authldap": {"manual_type": set()},
    "authtoken": {"manual_type": {"text"}},
    "bagcsv": {"manual_type": {"file"}},
    "bidownload": {"manual_type": set()},
    "buitenbeter": {"manual_type": {"text"}},
    "controlpanel": {"manual_type": {"text"}},
    "email": {"manual_type": {"text"}},
    "emailconfiguration": {"manual_type": {"text"}},
    "emailintake": {"manual_type": {"text"}},
    "event_sink": {"manual_type": {"text"}},
    "export_queue": {"manual_type": set()},
    "externe_koppeling": {"manual_type": {"text"}},
    "gws4all": {"manual_type": set()},
    "importplugin": {"manual_type": set()},
    "kcc": {"manual_type": {"text"}},
    "key2burgerzakenverhuizing": {"manual_type": {"text"}},
    "key2finance": {"manual_type": {"text"}},
    "koppelapp_appointments": {"manual_type": {"text"}},
    "koppelapp_document_masker": {"manual_type": {"text"}},
    "koppelapp_dso_swf": {"manual_type": {"text"}},
    "koppelapp_objections": {"manual_type": {"text"}},
    "koppelapp_signer": {"manual_type": {"text"}},
    "kvkapi": {"manual_type": set()},
    "legacy_publicaties": {"manual_type": {"text"}},
    "map": {"manual_type": {"text"}},
    "mijnoverheid": {"manual_type": set()},
    "multichannel": {"manual_type": set()},
    "next2know": {"manual_type": {"text"}},
    "ogone": {"manual_type": {"text"}},
    "omgevingsloket": {"manual_type": {"references"}},
    "overheidio": {"manual_type": set()},
    "pop3client": {"manual_type": set()},
    "postex": {"manual_type": set()},
    "publicsearchquery": {"manual_type": {"text"}},
    "qmatic": {"manual_type": {"text"}},
    "samlidp": {"manual_type": {"text"}},
    "samlsp": {"manual_type": {"text"}},
    "scanstraat": {"manual_type": {"text"}},
    "sdu_connect_pdc": {"manual_type": {"file"}},
    "sdu_connect_vac": {"manual_type": {"file"}},
    "stuf_zkn_client": {"manual_type": {"text"}},
    "stuf_zkn": {"manual_type": {"text"}},
    "stufadr": {"manual_type": {"text", "file"}},
    "stufaoa": {"manual_type": {"text"}},
    "stufconfig": {"manual_type": {"text"}},
    "stufdcr": {"manual_type": set()},
    "stufnnp": {"manual_type": {"text", "file"}},
    "stufnps": {"manual_type": {"text", "file"}},
    "stufprs": {"manual_type": {"text", "file"}},
    "stufvbo": {"manual_type": {"text"}},
    "supersaas": {"manual_type": {"text"}},
    "topx": {"manual_type": set()},
    "validsign": {"manual_type": set()},
    "woz": {"manual_type": {"text"}},
    "xential": {"manual_type": {"text"}},
    "zorginstituut": {"manual_type": {"text"}},
}


class IntegrationRepository(ZaaksysteemRepositoryBase):
    def get_active_integrations(
        self, integration_types: set[str]
    ) -> list[Integration]:
        """Get a list of active integrations of the specified types."""
        query = INTEGRATION_QUERY.where(
            sql.and_(
                schema.Interface.active.is_(True),
                schema.Interface.module.in_(integration_types),
            )
        )
        integration_rows = self.session.execute(query).fetchall()

        return [
            self._transform_to_entity(integration)
            for integration in integration_rows
        ]

    def get_multiple(self, integration_uuids: list[UUID]) -> list[Integration]:
        """
        Retrieve multiple integrations, given their UUIDs
        """
        query = INTEGRATION_QUERY.where(
            schema.Interface.uuid.in_(integration_uuids)
        )

        integration_rows = self.session.execute(query).fetchall()
        return [
            self._transform_to_entity(integration)
            for integration in integration_rows
        ]

    def get_all(self) -> list[Integration]:
        """
        Retrieve multiple integrations, given their UUIDs
        """
        integration_rows = self.session.execute(INTEGRATION_QUERY).fetchall()

        return [
            self._transform_to_entity(integration)
            for integration in integration_rows
        ]

    def _transform_to_entity(self, query_result):
        """
        Transform an integration database row to Integration entity.
        """

        # Default to "no manual transaction upload possible" if we don't know
        manual_type = INTEGRATION_MODULE_CONFIGS.get(
            query_result.module, {"manual_type": set()}
        )

        return Integration(
            entity_id=query_result.uuid,
            uuid=query_result.uuid,
            name=query_result.name,
            active=query_result.active,
            module=query_result.module,
            manual_type=manual_type["manual_type"],
            legacy_id=query_result.id,
        )

# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .... import ZaaksysteemRepositoryBase
from ..entities.transaction_record import Transaction, TransactionRecord
from sqlalchemy import sql
from typing import Final
from uuid import UUID
from zsnl_domains.database import schema

RECORDS_QUERY: Final = (
    sql.select(
        schema.TransactionRecord.uuid,
        schema.TransactionRecord.input,
        schema.TransactionRecord.output,
        schema.TransactionRecord.is_error,
        schema.TransactionRecord.date_executed,
        schema.TransactionRecord.preview_string,
        schema.TransactionRecord.last_error,
        schema.Transaction.uuid.label("transaction_uuid"),
    )
    .select_from(
        sql.join(
            schema.TransactionRecord,
            schema.Transaction,
            sql.and_(
                schema.Transaction.id
                == schema.TransactionRecord.transaction_id,
                schema.Transaction.date_deleted.is_(None),
            ),
        )
    )
    .where(schema.Transaction.date_deleted.is_(None))
)


class TransactionRecordRepository(ZaaksysteemRepositoryBase):
    _for_entity = "Transaction"
    _events_to_calls = {}

    def get_list(self, transaction_uuid: UUID) -> list[TransactionRecord]:
        records_query = RECORDS_QUERY.where(
            schema.TransactionRecord.transaction_id
            == sql.select(schema.Transaction.id)
            .where(schema.Transaction.uuid == transaction_uuid)
            .scalar_subquery()
        ).order_by(sql.desc(schema.TransactionRecord.date_executed))

        transaction_rows = self.session.execute(records_query).fetchall()

        return [self._inflate_from_row(row) for row in transaction_rows]

    def _inflate_from_row(self, row) -> TransactionRecord:
        return TransactionRecord(
            entity_id=row.uuid,
            uuid=row.uuid,
            input=row.input,
            output=row.output,
            is_error=row.is_error,
            date_executed=row.date_executed,
            error_message=row.last_error,
            entity_meta_summary=row.preview_string,
            transaction=Transaction(
                entity_id=row.transaction_uuid,
                uuid=row.transaction_uuid,
            ),
        )

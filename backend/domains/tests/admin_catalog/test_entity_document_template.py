# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from unittest import mock
from uuid import uuid4
from zsnl_domains.admin.catalog.entities.document_template import (
    DocumentTemplate,
)


class TestDocumentTemplateEntity:
    def setup_method(self):
        self.document_template = DocumentTemplate(
            uuid=str(uuid4()),
            id=None,
            name=None,
            file_uuid=None,
            file_name=None,
            integration_uuid=None,
            integration_reference=None,
            help=None,
            category_uuid=None,
        )

        self.document_template.event_service = mock.MagicMock()

    def test_create(self):
        fields = {
            "commit_message": "commit message",
            "name": "document",
            "integration_uuid": "ba150f42-b254-490a-84af-203e45074988",
            "integration_reference": "reference to external integration",
            "category_uuid": "6ea6d7f8-932d-11e9-b6e4-bf755288c2e1",
            "help": "extra info",
        }

        self.document_template.create(fields=fields)

        assert self.document_template.commit_message == "commit message"
        assert self.document_template.name == "document"
        assert (
            self.document_template.integration_uuid
            == "ba150f42-b254-490a-84af-203e45074988"
        )
        assert (
            self.document_template.integration_reference
            == "reference to external integration"
        )
        assert (
            self.document_template.category_uuid
            == "6ea6d7f8-932d-11e9-b6e4-bf755288c2e1"
        )
        assert self.document_template.help == "extra info"

    def test_edit(self):
        fields = {
            "commit_message": "edit commit message",
            "name": "doc1",
            "file_uuid": "6ea6d7f8-932d-11e9-b6e4-bf755288c2e1",
            "file_name": "xyz.odt",
            "integration_uuid": "49e4988f-95f3-4a63-96ed-d599aae0ff85",
            "integration_reference": "external reference",
            "category_uuid": "00c9b515-e51b-4d42-b8e5-7f37177c04f2",
            "help": "extra info",
        }
        self.document_template.edit(fields=fields)

        assert self.document_template.commit_message == "edit commit message"
        assert self.document_template.name == "doc1"
        assert (
            self.document_template.file_uuid
            == "6ea6d7f8-932d-11e9-b6e4-bf755288c2e1"
        )
        assert self.document_template.file_name is None
        assert self.document_template.integration_uuid is None
        assert (
            self.document_template.category_uuid
            == "00c9b515-e51b-4d42-b8e5-7f37177c04f2"
        )

        assert (
            self.document_template.integration_reference
            == "external reference"
        )

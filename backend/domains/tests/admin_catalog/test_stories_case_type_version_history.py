# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2


import random
from datetime import datetime, timezone
from minty.cqrs.test import TestBase
from sqlalchemy.dialects import postgresql
from unittest import mock
from uuid import uuid4
from zsnl_domains.admin import catalog
from zsnl_domains.admin.catalog.entities import CaseTypeVersion


class TestCaseTypeVersionHistory(TestBase):
    def setup_method(self):
        self.load_query_instance(catalog)

    def test_get_case_type_history(self):
        case_type_uuid = uuid4()
        now = datetime.now(tz=timezone.utc)

        mock_version = mock.Mock()
        mock_version.configure_mock(
            id=random.randint(1000, 2000),
            uuid=uuid4(),
            name="Case Type Name",
            created=now,
            last_modified=now,
            case_type_uuid=case_type_uuid,
            active=True,
            version=random.randint(1, 100),
            created_by="someuser",
            create_by_name_cache="Some User",
            reason="Changelog blah",
            event_data={
                "commit_message": "changes",
                "components": ["one", "two"],
            },
        )
        self.session.execute().fetchall.side_effect = [
            [mock_version],
        ]
        self.session.reset_mock()

        res = self.qry.get_case_type_history(uuid=case_type_uuid)

        assert len(res) == 1
        assert isinstance(res[0], CaseTypeVersion)
        assert res[0].uuid == mock_version.uuid
        assert res[0].active is True
        assert res[0].version == mock_version.version
        assert res[0].change_note == "changes"
        assert res[0].modified_components == ["one", "two"]

        self.session.execute.assert_called_once()

        (args, kwargs) = self.session.execute.call_args
        compiled = args[0].compile(dialect=postgresql.dialect())

        assert str(compiled) == (
            "SELECT zaaktype_node.id, zaaktype_node.uuid, zaaktype_node.titel AS name, zaaktype_node.created, zaaktype_node.last_modified, zaaktype.uuid AS case_type_uuid, zaaktype_node.version, CAST(zaaktype_node.id = zaaktype.zaaktype_node_id AS BOOLEAN) AS active, logging.created_by AS username, logging.created_by_name_cache AS display_name, logging.onderwerp AS reason, logging.event_data \n"
            "FROM zaaktype_node JOIN zaaktype ON zaaktype_node.zaaktype_id = zaaktype.id LEFT OUTER JOIN logging ON logging.id = zaaktype_node.logging_id \n"
            "WHERE zaaktype.uuid = %(uuid_1)s::UUID ORDER BY zaaktype_node.id"
        )

        assert compiled.params == {"uuid_1": case_type_uuid}

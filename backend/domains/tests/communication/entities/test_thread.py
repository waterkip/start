# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytest
from datetime import datetime, timedelta
from minty.exceptions import Conflict
from unittest import mock
from uuid import uuid4
from zsnl_domains.communication.entities import Case, Contact, Thread


class MockRepositoryFactory:
    def __init__(self, infra_factory):
        self.infrastructure_factory = infra_factory
        self.repositories = {}

    def get_repository(self, name, context):
        mock_repo = self.repositories[name]
        return mock_repo


class TestThreadEntity:
    thread_id = 123
    case_id = 123
    thread_uuid = uuid4()
    contact_uuid = uuid4()
    case_uuid = uuid4()
    thread_type = "employee"
    contact_name = "admin"
    created = last_modified = datetime.now()
    last_message = {"slug": "test test test"}
    contact = Contact(uuid=contact_uuid, name=contact_name)
    case = Case(
        uuid=case_uuid,
        id=case_id,
        description=None,
        description_public=None,
        case_type_name=None,
        status="open",
    )
    number_of_messages = 12
    unread_pip_count = 1
    unread_employee_count = 1
    attachment_count = 1

    def setup_method(self):
        self.thread = Thread(
            id=self.thread_id,
            uuid=self.thread_uuid,
            thread_type="employee",
            created=self.created,
            last_modified=self.last_modified,
            contact=self.contact,
            case=self.case,
            last_message_cache=self.last_message,
            number_of_messages=self.number_of_messages,
            unread_employee_count=self.unread_employee_count,
            unread_pip_count=self.unread_pip_count,
            attachment_count=self.attachment_count,
        )
        self.event_service = mock.MagicMock()
        self.thread.event_service = self.event_service

    def test_contact_initialisation(self):
        assert self.thread.id == self.thread_id
        assert self.thread.uuid == self.thread_uuid
        assert self.thread.entity_id == self.thread_uuid
        assert self.thread.thread_type == self.thread_type
        assert self.thread.contact.name == self.contact_name
        assert self.thread.contact.uuid == self.contact_uuid
        assert self.thread.case.uuid == self.case_uuid
        assert self.thread.case.id == self.case_id

        assert self.thread.created == self.created
        assert self.thread.last_modified == self.last_modified

        assert self.thread.last_message_cache == self.last_message
        assert self.thread.number_of_messages == self.number_of_messages

    def test_create(self):
        args = {
            "thread_type": self.thread_type,
            "contact": self.contact,
            "last_message_cache": self.last_message,
            "case": self.case,
        }
        self.thread.event_service.reset()

        self.thread.create(**args)
        assert self.thread.uuid == self.thread_uuid
        assert self.thread.thread_type == self.thread_type

        assert self.thread.contact.name == self.contact_name
        assert self.thread.contact.uuid == self.contact_uuid
        assert self.thread.last_message_cache == self.last_message
        assert self.thread.case == self.case

        args["contact"] = None
        self.thread.create(**args)

    def test_link_thread_to_case(self):
        last_message_cache = {"message_type": "pip"}
        case = Case(
            id=3,
            uuid=None,
            case_type_name=None,
            description=None,
            description_public="description_public",
            status="open",
        )
        thread = Thread(
            id=2,
            uuid=uuid4(),
            case=case,
            last_message_cache=last_message_cache,
        )
        args = {
            "external_message_type": "not recognized message type",
            "case": self.case,
            "external_message_subject": None,
            "external_message_participants": None,
            "external_message_direction": None,
            "external_message_message_date": None,
        }
        thread.event_service = mock.MagicMock()

        with pytest.raises(Conflict) as excinfo:
            thread.link_thread_to_case(**args)
        assert excinfo.value.args == (
            "Thread with message type 'pip' "
            "could not be linked to another message type of 'not recognized message type'",
            "communication/thread/not_linked",
        )

        last_message_cache_email = {"message_type": "email"}
        another_thread = Thread(
            id=2,
            uuid=uuid4(),
            case=case,
            last_message_cache=last_message_cache_email,
        )
        another_args = {
            "external_message_type": "email",
            "case": self.case,
            "external_message_subject": None,
            "external_message_participants": None,
            "external_message_direction": None,
            "external_message_message_date": None,
        }
        another_thread.event_service = mock.MagicMock()
        with pytest.raises(Conflict) as excinfo:
            another_thread.link_thread_to_case(**another_args)
        assert excinfo.value.args == (
            f"Thread with uuid '{another_thread.uuid}' could not be linked to "
            + f"case '{self.case.id}', because it's already linked to a case.",
            "communication/thread/not_linked",
        )

        last_message_cache1 = {"message_type": "email"}
        thread_uuid = uuid4()
        thread_with_no_case = Thread(
            id=2,
            uuid=thread_uuid,
            case=None,
            last_message_cache=last_message_cache1,
        )
        thread_with_no_case.event_service = mock.MagicMock()
        link_to_case_args = {
            "external_message_type": "email",
            "case": self.case,
            "external_message_subject": None,
            "external_message_participants": None,
            "external_message_direction": None,
            "external_message_message_date": None,
        }
        thread_with_no_case.link_thread_to_case(**link_to_case_args)

        assert thread_with_no_case.uuid == thread_uuid
        assert thread_with_no_case.last_message_cache == {
            "message_type": "email"
        }
        assert thread_with_no_case.case == self.case
        assert thread_with_no_case.external_message_subject is None
        assert thread_with_no_case.external_message_message_date is None
        assert thread_with_no_case.external_message_participants is None
        assert thread_with_no_case.external_message_direction is None

    def test_link_thread_to_case_with_external_message(self):
        last_message_cache = {"message_type": "pip"}
        case = Case(
            id=3,
            uuid=None,
            case_type_name=None,
            description=None,
            description_public="description_public",
            status="open",
        )
        thread = Thread(
            id=2,
            uuid=uuid4(),
            case=case,
            last_message_cache=last_message_cache,
        )
        participants: list[dict] = [
            {
                "role": "from",
                "display_name": "Testing from 1",
                "address": "from1@example.com",
            },
            {
                "role": "from",
                "display_name": "",
                "address": "from1@example.com",
            },
            {
                "role": "to",
                "display_name": "Testing to",
                "address": "to@example.com",
            },
            {
                "role": "cc",
                "display_name": "Testing cc 1",
                "address": "cc1@example.com",
            },
            {
                "role": "cc",
                "display_name": "Testing cc 2",
                "address": "cc2@example.com",
            },
            {
                "role": "bcc",
                "display_name": "Testing bcc 1",
                "address": "bcc1@example.com",
            },
            {
                "role": "bcc",
                "display_name": "Testing bcc 2",
                "address": "bcc2@example.com",
            },
        ]

        message_date = datetime.now() + timedelta(days=1)

        args = {
            "external_message_type": "not recognized message type",
            "case": self.case,
            "external_message_subject": "subject",
            "external_message_participants": participants,
            "external_message_direction": "outgoing",
            "external_message_message_date": message_date,
        }
        thread.event_service = mock.MagicMock()

        with pytest.raises(Conflict) as excinfo:
            thread.link_thread_to_case(**args)
        assert excinfo.value.args == (
            "Thread with message type 'pip' "
            "could not be linked to another message type of 'not recognized message type'",
            "communication/thread/not_linked",
        )

        last_message_cache_email = {"message_type": "email"}
        another_thread = Thread(
            id=2,
            uuid=uuid4(),
            case=case,
            last_message_cache=last_message_cache_email,
        )
        another_args = {
            "external_message_type": "email",
            "case": self.case,
            "external_message_subject": "subject",
            "external_message_participants": participants,
            "external_message_direction": "outgoing",
            "external_message_message_date": message_date,
        }
        another_thread.event_service = mock.MagicMock()
        with pytest.raises(Conflict) as excinfo:
            another_thread.link_thread_to_case(**another_args)
        assert excinfo.value.args == (
            f"Thread with uuid '{another_thread.uuid}' could not be linked to "
            + f"case '{self.case.id}', because it's already linked to a case.",
            "communication/thread/not_linked",
        )

        last_message_cache1 = {"message_type": "email"}
        thread_uuid = uuid4()
        thread_with_no_case = Thread(
            id=2,
            uuid=thread_uuid,
            case=None,
            last_message_cache=last_message_cache1,
        )
        thread_with_no_case.event_service = mock.MagicMock()
        link_to_case_args = {
            "external_message_type": "email",
            "case": self.case,
            "external_message_subject": "subject",
            "external_message_participants": participants,
            "external_message_direction": "outgoing",
            "external_message_message_date": message_date,
        }
        thread_with_no_case.link_thread_to_case(**link_to_case_args)

        assert thread_with_no_case.uuid == thread_uuid
        assert thread_with_no_case.last_message_cache == {
            "message_type": "email"
        }
        assert thread_with_no_case.case == self.case
        assert thread_with_no_case.external_message_subject == "subject"
        assert thread_with_no_case.external_message_message_date is not None
        assert (
            thread_with_no_case.external_message_participants == participants
        )
        assert thread_with_no_case.external_message_direction == "outgoing"

    def test_delete(self):
        self.event_service.reset()
        self.thread.delete()

    def test_increment_unread_count(self):
        self.event_service.reset()
        self.thread.increment_unread_count()
        assert self.thread.unread_pip_count == 2
        assert self.thread.unread_employee_count == 2

        self.thread.increment_unread_count(context="employee")
        assert self.thread.unread_employee_count == 3

        self.thread.increment_unread_count(context="pip")
        assert self.thread.unread_pip_count == 3

    def test_decrement_unread_count(self):
        self.event_service.reset()
        self.thread.decrement_unread_count(context="pip")
        assert self.thread.unread_pip_count == 0
        assert self.thread.unread_employee_count == 1

        self.thread.decrement_unread_count(context="employee")
        assert self.thread.unread_employee_count == 0

    def test_increment_attachment_count(self):
        self.event_service.reset()
        self.thread.increment_attachment_count()
        assert self.thread.attachment_count == 2

    def test_decrement_attachment_count(self):
        self.event_service.reset()
        self.thread.decrement_attachment_count()
        assert self.thread.attachment_count == 0

# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytest
from collections import namedtuple
from minty.exceptions import Conflict, Forbidden, NotFound
from sqlalchemy.sql import and_
from unittest import mock
from uuid import uuid4
from zsnl_domains.communication.entities import Case
from zsnl_domains.communication.repositories import CaseRepository
from zsnl_domains.database import schema


class InfraFactoryMock:
    def __init__(self, infra):
        self.infra = infra

    def get_infrastructure(self, context, infrastructure_name):
        return self.infra[infrastructure_name]


class TestCaseRepository:
    def setup_method(self):
        with mock.patch("sqlalchemy.orm.Session") as mock_session:
            self.mock_infra = mock.MagicMock()
            self.infra = InfraFactoryMock(infra={"database": mock_session})
            self.infra_ro = InfraFactoryMock(infra={"database": mock_session})
            self.case_repo = CaseRepository(
                infrastructure_factory=self.infra,  # type: ignore
                infrastructure_factory_ro=self.infra_ro,  # type: ignore
                context="no context",
                event_service=mock.MagicMock(),
                read_only=False,
            )
            self.db = self.case_repo._get_infrastructure("database")
            self.case_repo.event_service.event_list = ["ThreadCreated"]
            self.event_list = self.case_repo.event_service.event_list

    def test_get_session(self):
        assert self.case_repo.session is self.infra.get_infrastructure(
            context=None, infrastructure_name="database"
        )

    def test_find_case_by_uuid(self):
        qry_result = namedtuple(
            "case_row",
            [
                "case_id",
                "case_uuid",
                "case_status",
                "case_description",
                "case_description_public",
                "case_type_name",
            ],
        )
        mock_case = qry_result(
            case_uuid=uuid4(),
            case_id=2,
            case_description=None,
            case_description_public=None,
            case_type_name=None,
            case_status="open",
        )

        self.case_repo._get_infrastructure(
            "database"
        ).execute().fetchall.return_value = [mock_case]

        case = self.case_repo.find_case_by_uuid(
            case_uuid=str(uuid4()), user_uuid=str(uuid4()), permission="read"
        )

        assert isinstance(case, Case)
        assert case.id == 2
        assert case.uuid == mock_case.case_uuid
        assert case.description == mock_case.case_description
        assert case.description_public == mock_case.case_description_public
        assert case.status == mock_case.case_status
        assert case.case_type_name == mock_case.case_type_name

    def test_find_case_by_uuid_no_result(self):
        self.case_repo._get_infrastructure(
            "database"
        ).execute().fetchall.return_value = []

        with pytest.raises(NotFound):
            self.case_repo.find_case_by_uuid(
                case_uuid=str(uuid4()), user_uuid=str(uuid4()), permission=""
            )

    def test_find_case_by_uuid_multiple_results(self):
        # In real life this never happens, because the database has a uniqueness
        # constraint.
        self.case_repo._get_infrastructure(
            "database"
        ).execute().fetchall.return_value = [1, 2, 3]

        with pytest.raises(Conflict):
            self.case_repo.find_case_by_uuid(
                case_uuid=str(uuid4()), user_uuid=str(uuid4()), permission=""
            )

    def test_get_case_for_pip_user(self):
        case_uuid = uuid4()
        CaseRow = namedtuple(
            "case_row",
            [
                "case_id",
                "case_uuid",
                "case_status",
                "case_description",
                "case_description_public",
                "case_type_name",
            ],
        )
        case_row = CaseRow(
            case_id=2,
            case_uuid=case_uuid,
            case_status="open",
            case_description="Description",
            case_description_public="Public Description",
            case_type_name="Case Type",
        )

        self.case_repo._get_infrastructure(
            "database"
        ).execute().fetchone.return_value = case_row
        case = self.case_repo.get_case_for_pip_user(
            case_uuid=case_uuid, user_uuid=str(uuid4())
        )
        assert isinstance(case, Case)
        assert case.id == 2
        assert case.uuid == case_uuid

    def test_get_case_for_pip_user_no_result(self):
        self.case_repo._get_infrastructure(
            "database"
        ).execute().fetchone.return_value = None

        with pytest.raises(Forbidden):
            self.case_repo.get_case_for_pip_user(
                case_uuid=str(uuid4()), user_uuid=str(uuid4())
            )

    def test_case_find_by_thread_uuid(self):
        user_uuid = uuid4()
        thread_uuid = uuid4()
        case_uuid = uuid4()
        qry_result = namedtuple(
            "case_row",
            [
                "case_id",
                "case_uuid",
                "case_status",
                "case_description",
                "case_description_public",
                "case_type_name",
            ],
        )
        db_result = (
            self.case_repo._get_infrastructure("database").execute().fetchone
        )
        user_info = mock.MagicMock()
        user_info.permissions = {}

        # no result
        db_result.return_value = None
        with pytest.raises(NotFound):
            self.case_repo.find_case_by_thread_uuid(
                thread_uuid=thread_uuid,
                user_uuid=user_uuid,
                user_info=user_info,
            )

        # no case_id
        result = qry_result(
            case_id=None,
            case_uuid=None,
            case_status=None,
            case_description=None,
            case_description_public=None,
            case_type_name=None,
        )
        db_result.return_value = result
        res = self.case_repo.find_case_by_thread_uuid(
            thread_uuid=thread_uuid, user_uuid=user_uuid, user_info=user_info
        )
        assert res is None

        # success - allowed
        result = qry_result(
            case_id=1234,
            case_uuid=case_uuid,
            case_status="open",
            case_description=None,
            case_description_public=None,
            case_type_name=None,
        )
        db_result.return_value = result
        res = self.case_repo.find_case_by_thread_uuid(
            thread_uuid=thread_uuid, user_uuid=user_uuid, user_info=user_info
        )
        assert res.id == result.case_id
        assert res.uuid == case_uuid

        # success - forbidden
        result = qry_result(
            case_id=1234,
            case_uuid=None,
            case_status="open",
            case_description=None,
            case_description_public=None,
            case_type_name=None,
        )

        db_result.return_value = result
        with pytest.raises(Forbidden):
            self.case_repo.find_case_by_thread_uuid(
                thread_uuid=thread_uuid,
                user_uuid=user_uuid,
                user_info=user_info,
            )

    def test_case_find_by_thread_uuid_for_pip_user(self):
        user_uuid = uuid4()
        thread_uuid = uuid4()
        case_uuid = uuid4()
        qry_result = namedtuple(
            "case_row",
            [
                "case_id",
                "case_uuid",
                "case_status",
                "case_description",
                "case_description_public",
                "case_type_name",
            ],
        )
        db_result = (
            self.case_repo._get_infrastructure("database").execute().fetchone
        )
        user_info = mock.MagicMock()
        user_info.permissions = {"pip_user": True}

        # no result
        db_result.return_value = None
        with pytest.raises(NotFound):
            self.case_repo.find_case_by_thread_uuid(
                thread_uuid=thread_uuid,
                user_uuid=user_uuid,
                user_info=user_info,
            )

        # success - allowed
        result = qry_result(
            case_id=1234,
            case_uuid=case_uuid,
            case_status="open",
            case_description="",
            case_description_public="",
            case_type_name="",
        )
        db_result.return_value = result
        res = self.case_repo.find_case_by_thread_uuid(
            thread_uuid=thread_uuid, user_uuid=user_uuid, user_info=user_info
        )

        assert res.id == result.case_id
        assert res.uuid == case_uuid

    def test_search_case(self):
        user_uuid = uuid4()
        cases = self.case_repo.search_cases(
            search_term="test search",
            permission="read",
            case_status_filter=["open", "new", "stalled"],
            user_uuid=user_uuid,
            limit=6,
        )
        assert isinstance(cases, list)

        for case in cases:
            assert isinstance(case, Case)

    @mock.patch(
        "zsnl_domains.communication.repositories.database_queries.list_cases_query"
    )
    def test_get_case_list_for_contact(self, mock_list):
        user_uuid = str(uuid4())
        contact_uuid = str(uuid4())

        self.case_repo.session.execute().fetchall.return_value = ["test"]

        with mock.patch.object(
            self.case_repo, "_transform_to_entity", return_value="xxx"
        ) as transformer:
            result = self.case_repo.get_case_list_for_contact(
                is_pip_user=True,
                user_uuid=user_uuid,
                contact_uuid=contact_uuid,
            )

            transformer.assert_called_once_with("test")

        mock_list.assert_called_once_with(
            is_pip_user=True,
            user_uuid=user_uuid,
            contact_uuid=contact_uuid,
            db=self.case_repo.session,
        )
        assert result == ["xxx"]

    def test_transform_to_entity(self):
        query_res = mock.MagicMock()
        query_res.case_uuid = uuid4()
        query_res.case_id = 2
        query_res.case_description = "onderwerp"
        query_res.case_description_public = "onderwerp public"
        query_res.case_type_name = "case type name"
        query_res.status = "resolved"

        case = self.case_repo._transform_to_entity(query_res)
        assert isinstance(case, Case)
        assert case.id == query_res.case_id
        assert case.uuid == query_res.case_uuid
        assert case.description == query_res.case_description
        assert case.description_public == query_res.case_description_public
        assert case.case_type_name == query_res.case_type_name
        assert case.status == query_res.case_status

    @mock.patch(
        "zsnl_domains.communication.repositories.database_queries.unsafe_case_query"
    )
    def test_get_case_by_id(self, case_query):
        qry_result = namedtuple(
            "case_row",
            [
                "case_id",
                "case_uuid",
                "case_status",
                "case_description",
                "case_description_public",
                "case_type_name",
            ],
        )
        test_case = qry_result(
            case_uuid=uuid4(),
            case_id=2,
            case_description=None,
            case_description_public=None,
            case_type_name=None,
            case_status="open",
        )

        self.case_repo._get_infrastructure(
            "database"
        ).execute().fetchall.return_value = [test_case]

        case = self.case_repo.get_case_by_id(case_id=123)

        assert str(case_query.where.call_args[0][0]) == str(
            and_(schema.Case.id == 123, schema.Case.status != "resolved")
        )

        assert case.uuid == test_case.case_uuid
        assert case.id == test_case.case_id
        assert case.description == test_case.case_description
        assert case.description_public == test_case.case_description_public
        assert case.case_type_name == test_case.case_type_name
        assert case.status == test_case.case_status

    @mock.patch(
        "zsnl_domains.communication.repositories.database_queries.unsafe_case_query"
    )
    def test_get_case_by_id_notfound(self, case_query):
        self.case_repo._get_infrastructure(
            "database"
        ).execute().fetchall.return_value = []

        with pytest.raises(NotFound):
            self.case_repo.get_case_by_id(case_id=123)

        assert str(case_query.where.call_args[0][0]) == str(
            and_(schema.Case.id == 123, schema.Case.status != "resolved")
        )

    @mock.patch(
        "zsnl_domains.communication.repositories.database_queries.unsafe_case_query"
    )
    def test_get_case_by_id_multiple(self, case_query):
        # This never happens IRL, as case.id is unique
        self.case_repo._get_infrastructure(
            "database"
        ).execute().fetchall.return_value = [1, 2, 3]

        with pytest.raises(Conflict):
            self.case_repo.get_case_by_id(case_id=123)

        assert str(case_query.where.call_args[0][0]) == str(
            and_(schema.Case.id == 123, schema.Case.status != "resolved")
        )

# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from uuid import uuid4
from zsnl_domains.communication.repositories.util import (
    base62_decode,
    base62_encode,
)

test_cases = []


def test_base62():
    for _ in range(100):
        test_case = uuid4().int & (1 << 64) - 1
        assert base62_decode(base62_encode(test_case)) == test_case

    assert base62_decode(base62_encode(0)) == 0

# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2
from datetime import datetime
from minty.cqrs import UserInfo
from minty.cqrs.test import TestBase
from sqlalchemy.dialects import postgresql
from unittest import mock
from unittest.mock import patch
from uuid import uuid4
from zsnl_domains import case_management
from zsnl_domains.case_management.queries.case_search_result import CaseFilter
from zsnl_domains.shared.entities.case import (
    ValidCaseArchivalState,
    ValidCaseConfidentiality,
    ValidCasePaymentStatus,
    ValidCaseResult,
    ValidCaseRetentionPeriodSourceDate,
    ValidCaseStatus,
    ValidCaseUrgency,
    ValidContactChannel,
)


class Test_Case_Search_Queries(TestBase):
    mock_db_rows = [mock.Mock(), mock.Mock()]
    mock_db_rows[0].configure_mock(
        id=2,
        uuid=uuid4(),
        status="new",
        vernietigingsdatum=None,
        archival_state=None,
        percentage_days_left=4,
        onderwerp="subject of case",
        onderwerp_extern="subject extern of case",
        progress=0.5,
        titel="case type title",
        case_type_version=uuid4(),
        case_type_result=uuid4(),
        unaccepted_files_count=1,
        unread_communication_count=2,
        unaccepted_attribute_update_count=3,
        requestor_name="Diederik-Jan",
        requestor_type="person",
        requestor_uuid=uuid4(),
        assignee_name="Sjaak",
        assignee_uuid=uuid4(),
        coordinator_uuid=uuid4(),
        coordinator_name="Sjaak",
        registratiedatum="2022-02-08",
        afhandeldatum="2022-02-08",
        target_completion_date="2025-02-08",
        last_modified="2023-11-25",
        period_of_preservation_active=False,
        case_location=uuid4(),
        stalled_until="2025-02-08",
        contact_channel="email",
        custom_fields=[
            {
                "name": "custom_field_1",
                "magic_string": "test_subject",
                "type": "text",
                "value": ["Test Subject"],
                "is_multiple": False,
            },
            {
                "name": "custom_field_2",
                "magic_string": "object_relation_1",
                "type": "relationship",
                "value": [
                    '{"type": "relationship", "value": "479bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-1"}, "relationship_type": "custom_object"}}'
                ],
                "is_multiple": False,
            },
            {
                "name": "custom_field_3",
                "magic_string": "object_relation_2",
                "type": "relationship",
                "value": [
                    '{"type": "relationship", "value": "579bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-2"}, "relationship_type": "custom_object"}}'
                ],
                "is_multiple": False,
            },
        ],
        file_custom_fields=[
            {
                "name": "some name",
                "magic_string": "magicstring2",
                "type": "file",
                "is_multiple": False,
                "value": [
                    {
                        "md5": "4216455ceebbc3038bd0550c85b6a3bf",
                        "size": 3,
                        "uuid": uuid4(),
                        "filename": "hello.txt",
                        "mimetype": "text/plain",
                        "is_archivable": True,
                        "original_name": "hello.txt",
                        "thumbnail_uuid": uuid4(),
                    },
                ],
            }
        ],
        case_role_department=uuid4(),
    )
    mock_db_rows[1].configure_mock(
        id=3,
        uuid=uuid4(),
        status="pending",
        vernietigingsdatum=datetime.now(),
        archival_state="vernietigen",
        percentage_days_left=152,
        onderwerp="subject of case",
        onderwerp_extern="subject extern of case",
        progress=1.0,
        titel="case type title2",
        case_type_version=uuid4(),
        case_type_result=uuid4(),
        unaccepted_files_count=4,
        unread_communication_count=5,
        unaccepted_attribute_update_count=6,
        requestor_name="Donald Duck",
        requestor_type="person",
        requestor_uuid=uuid4(),
        assignee_name="Dagobert",
        assignee_uuid=uuid4(),
        coordinator_uuid=uuid4(),
        coordinator_name="Sjaak",
        registratiedatum="2022-02-08",
        afhandeldatum="2022-02-08",
        target_completion_date="2025-02-08",
        last_modified="2022-02-08",
        period_of_preservation_active=False,
        case_location=uuid4(),
        stalled_until="2025-02-08",
        contact_channel="email",
        custom_fields=[
            {
                "name": "custom_field_1",
                "magic_string": "test_subject",
                "type": "text",
                "value": ["Test Subject"],
                "is_multiple": False,
            },
            {
                "name": "custom_field_2",
                "magic_string": "object_relation_1",
                "type": "relationship",
                "value": [
                    '{"type": "relationship", "value": "479bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-1"}, "relationship_type": "custom_object"}}'
                ],
                "is_multiple": False,
            },
            {
                "name": "custom_field_3",
                "magic_string": "object_relation_2",
                "type": "relationship",
                "value": [
                    '{"type": "relationship", "value": "579bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-2"}, "relationship_type": "custom_object"}}'
                ],
                "is_multiple": False,
            },
        ],
        file_custom_fields=[
            {
                "name": "some name",
                "magic_string": "magicstring2",
                "type": "file",
                "is_multiple": False,
                "value": [
                    {
                        "md5": "4216455ceebbc3038bd0550c85b6a3bf",
                        "size": 3,
                        "uuid": uuid4(),
                        "filename": "hello.txt",
                        "mimetype": "text/plain",
                        "is_archivable": True,
                        "original_name": "hello.txt",
                        "thumbnail_uuid": uuid4(),
                    },
                ],
            }
        ],
        case_role_department=uuid4(),
    )

    def setup_method(self):
        self.load_query_instance(case_management)
        self.user_uuid = uuid4()
        self.user_info = UserInfo(
            user_uuid=self.user_uuid,
            permissions={"admin": False, "beheer": True},
        )
        self.qry.user_info = self.user_info

    def test_search_case(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        case_search_result = self.qry.search_case(page=1, page_size=10)
        assert len(case_search_result.entities) == 2
        # verify 1st search result
        search_result1 = case_search_result.entities[0]
        assert search_result1.number == 2
        assert search_result1.status == "new"
        assert search_result1.destruction_date is None
        assert search_result1.archival_state is None
        assert search_result1.percentage_days_left == 4
        assert search_result1.subject == "subject of case"
        assert search_result1.case_type_title == "case type title"
        assert search_result1.requestor.type == "person"
        assert search_result1.progress == 50
        assert search_result1.unread_message_count == 2
        assert search_result1.unaccepted_files_count == 1
        assert search_result1.unaccepted_attribute_update_count == 3

        # verify 2nd search result
        search_result2 = case_search_result.entities[1]
        assert search_result2.number == 3
        assert search_result2.status == "pending"
        assert search_result2.destruction_date is not None
        assert search_result2.archival_state == "vernietigen"
        assert search_result2.percentage_days_left == 152
        assert search_result2.subject == "subject of case"
        assert search_result2.case_type_title == "case type title2"
        assert search_result2.requestor.type == "person"
        assert search_result2.progress == 100
        assert search_result2.unread_message_count == 5
        assert search_result2.unaccepted_files_count == 4
        assert search_result2.unaccepted_attribute_update_count == 6

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        # query
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_status(self):
        self.user_info = UserInfo(
            user_uuid=self.user_uuid,
            permissions={"admin": False},
        )
        self.qry.user_info = self.user_info
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_status = {ValidCaseStatus.new, ValidCaseStatus.open}
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        # query
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id AND ((CAST(bibliotheek_kenmerken.properties AS JSON) ->> %(param_3)s) IS NULL OR (CAST(bibliotheek_kenmerken.properties AS JSON) ->> %(param_4)s) != %(param_5)s))) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND zaak_1.status IN (%(status_2_1)s, %(status_2_2)s) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_6)s OFFSET %(param_7)s"
        )

    def test_search_case_filter_casetype_uuid(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseFilter()
        filters.case_type_uuids = {uuid4(), uuid4()}
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        # query
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND zaaktype.uuid IN (%(uuid_2_1)s::UUID, %(uuid_2_2)s::UUID) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_assignee(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseFilter()
        filters.assignee_uuids = {uuid4(), uuid4()}
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        # query
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND zaak_betrokkenen_1.subject_id IN (%(subject_id_1_1)s::UUID, %(subject_id_1_2)s::UUID) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_coordinator(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseFilter()
        filters.coordinator_uuids = {uuid4(), uuid4()}
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        # query
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND zaak_betrokkenen_2.subject_id IN (%(subject_id_1_1)s::UUID, %(subject_id_1_2)s::UUID) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_requestor(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseFilter()
        filters.requestor_uuids = {uuid4(), uuid4()}
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        # query
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND zaak_betrokkenen_3.subject_id IN (%(subject_id_1_1)s::UUID, %(subject_id_1_2)s::UUID) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_registration_date(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_registration_date = ["gt 2022-02-08T11:13:16Z"]
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND zaak_1.registratiedatum > %(registratiedatum_1)s ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

        filters = CaseFilter()
        filters.filter_registration_date = [
            ["gt 2022-02-08T11:13:16Z", "lt 2022-02-18T11:13:16Z"]
        ]
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        select_statement = call_list[1][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND (zaak_1.registratiedatum > %(registratiedatum_1)s AND zaak_1.registratiedatum < %(registratiedatum_2)s) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_completion_date(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_completion_date = ["gt 2022-01-04T14:02:11Z"]
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND zaak_1.afhandeldatum > %(afhandeldatum_1)s ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_last_modified(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_last_modified = ["gt 2022-02-08T01:00:00Z"]
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND zaak_1.last_modified > %(last_modified_1)s ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_payment_status(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_payment_status = {
            ValidCasePaymentStatus.success,
            ValidCasePaymentStatus.pending,
        }
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        # query
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND zaak_1.payment_status IN (%(payment_status_1_1)s, %(payment_status_1_2)s) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_channel_of_contact(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_channel_of_contact = {
            ValidContactChannel.behandelaar,
            ValidContactChannel.post,
        }
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        # query
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND zaak_1.contactkanaal IN (%(contactkanaal_1_1)s, %(contactkanaal_1_2)s) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_confidentiality(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_confidentiality = {
            ValidCaseConfidentiality.public,
            ValidCaseConfidentiality.internal,
        }
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        # query
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND zaak_1.confidentiality IN (%(confidentiality_1_1)s, %(confidentiality_1_2)s) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_archival_state(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_archival_state = {
            ValidCaseArchivalState.vernietigen,
            ValidCaseArchivalState.overdragen,
        }
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        # query
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND (zaak_1.archival_state = %(archival_state_1)s OR (zaak_1.archival_state = %(archival_state_2)s AND zaak_1.status = %(status_2)s AND zaak_1.vernietigingsdatum < %(vernietigingsdatum_1)s)) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_retention_period_source_date(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_retention_period_source_date = [
            ValidCaseRetentionPeriodSourceDate.vervallen
        ]
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1
        # query
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND zaaktype_resultaten_1.ingang IN (%(ingang_1_1)s) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_result(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_result = {ValidCaseResult.aangegaan}
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        # query
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND zaak_1.resultaat IN (%(resultaat_1_1)s) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_case_location(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_case_location = {
            "values": [
                "nummeraanduiding-0437200000001964",
                "openbareruimte-0437300000000021",
            ]
        }
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        # query
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND (zaak_bag_1.bag_nummeraanduiding_id IN (%(bag_nummeraanduiding_id_1_1)s, %(bag_nummeraanduiding_id_1_2)s) OR zaak_bag_1.bag_openbareruimte_id IN (%(bag_openbareruimte_id_1_1)s, %(bag_openbareruimte_id_1_2)s)) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_num_unread_messages_and_sort_asc(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_num_unread_messages = ["gt 2"]
        self.qry.search_case(
            page=1, page_size=10, filters=filters, sort="unread_message_count"
        )

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND zaak_meta.unread_communication_count > %(unread_communication_count_1)s ORDER BY zaak_meta.unread_communication_count ASC, zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_num_unaccepted_files_sort_desc(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_num_unaccepted_files = ["gt 1"]
        self.qry.search_case(
            page=1,
            page_size=10,
            filters=filters,
            sort="-unaccepted_files_count",
        )

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND zaak_meta.unaccepted_files_count > %(unaccepted_files_count_1)s ORDER BY zaak_meta.unaccepted_files_count DESC, zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_num_unaccepted_updates_sort_asc(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_num_unaccepted_updates = ["gt 1"]
        self.qry.search_case(
            page=1,
            page_size=10,
            filters=filters,
            sort="unaccepted_attribute_update_count",
        )

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND zaak_meta.unaccepted_attribute_update_count > %(unaccepted_attribute_update_count_1)s ORDER BY zaak_meta.unaccepted_attribute_update_count ASC, zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_keyword(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_keyword = {"operator": "or", "values": ["test"]}
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND zaak_1.id = zaak_meta.zaak_id AND (zaak_meta.text_vector @@ to_tsquery(%(to_tsquery_1)s)) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_keyword_and_operator(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_keyword = {
            "operator": "and",
            "values": ["test", "case"],
        }
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND zaak_1.id = zaak_meta.zaak_id AND (zaak_meta.text_vector @@ to_tsquery(%(to_tsquery_1)s)) AND zaak_1.id = zaak_meta.zaak_id AND (zaak_meta.text_vector @@ to_tsquery(%(to_tsquery_2)s)) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_period_of_preservation_active(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_period_of_preservation_active = True
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND zaaktype_resultaten_1.trigger_archival IS true ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

        # filter_period_of_preservation_active = False
        filters.filter_period_of_preservation_active = False
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        select_statement = call_list[1][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND (zaaktype_resultaten_1.trigger_archival IS false OR zaak_1.resultaat_id IS NULL) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_subject(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_subject = {
            "values": ["test"],
        }
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND zaak_1.onderwerp ILIKE %(onderwerp_1)s ESCAPE '~' ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_total_count(self):
        self.session.execute().fetchall.return_value = 2
        self.session.reset_mock()

        self.qry.search_case_total_results()

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        # query
        query = call_list[0][0][0]
        query = query.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(query) == (
            "SELECT count(%(param_1)s) AS count_1 \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID)))"
        )

    def test_search_case_includes_contacts(self):
        employee_row = mock.MagicMock()
        employee_row.configure_mock(
            uuid=uuid4(),
            active=True,
            surname="",
            title=None,
            first_name="beheerder",
            name="testgebruiker",
            gender="Female",
            inside_municipality=True,
            properties={"anonymous": "0"},
            department={"uuid": uuid4(), "name": "Back office"},
            roles=[
                {
                    "name": "admin",
                    "uuid": uuid4(),
                    "department": {"name": "Back office", "uuid": uuid4()},
                }
            ],
            contact_information={"email": "test@test.com"},
            related_custom_object_uuid=uuid4(),
            summary="beheerder",
        )
        person_uuid = uuid4()
        person_row = mock.MagicMock()
        person_row.configure_mock(
            uuid=person_uuid,
            authenticated=True,
            authenticatedby="source",
            date_of_birth=datetime(1918, 4, 26),
            date_of_death=datetime(2004, 1, 25),
            first_names="Fanny",
            family_name="Koen",
            initials=None,
            insertions="Blankers",
            surname="Koen",
            noble_title=None,
            name="Fanny Blankers-Koen",
            gender="V",
            inside_municipality=True,
            properties={"anonymous": "0"},
            address={
                "street": "Lage Vuursche ",
                "zipcode": "1234RT",
                "street_number": 3,
                "city": "Baarn",
                "country_code": 6030,
                "bag_id": "9876543210098765",
            },
            correspondence_address={
                "street": "Lage Vuursche ",
                "zipcode": "1234RT",
                "street_number": 3,
                "city": "Baarn",
                "country_code": 6030,
            },
            contact_information={"email": "test@test.com"},
            related_custom_object_uuid=uuid4(),
            preferred_contact_channel="pip",
            summary="Fanny Blankers-Koen",
            external_identifier=None,
        )

        self.session.execute().fetchall.side_effect = (
            self.mock_db_rows,
            [person_row],
            [employee_row],
        )
        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_subject = {
            "values": ["test"],
        }
        includes = ["assignee", "coordinator", "requestor"]
        self.qry.search_case(
            page=1, page_size=10, filters=filters, includes=includes
        )

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 3

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND zaak_1.onderwerp ILIKE %(onderwerp_1)s ESCAPE '~' ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

        select_statement = call_list[1][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT natuurlijk_persoon.uuid, natuurlijk_persoon.authenticated, natuurlijk_persoon.authenticatedby, natuurlijk_persoon.voornamen AS first_names, natuurlijk_persoon.voorletters AS initials, natuurlijk_persoon.voorvoegsel AS insertions, natuurlijk_persoon.geslachtsnaam AS family_name, natuurlijk_persoon.adellijke_titel AS noble_title, natuurlijk_persoon.surname AS surname, natuurlijk_persoon.geboortedatum AS date_of_birth, natuurlijk_persoon.datum_overlijden AS date_of_death, natuurlijk_persoon.geslachtsaanduiding AS gender, natuurlijk_persoon.in_gemeente AS inside_municipality, natuurlijk_persoon.landcode, natuurlijk_persoon.preferred_contact_channel, natuurlijk_persoon.active AS active, (SELECT json_build_object(%(json_build_object_2)s, contact_data.email, %(json_build_object_3)s, contact_data.telefoonnummer, %(json_build_object_4)s, contact_data.mobiel, %(json_build_object_5)s, contact_data.note) AS json_build_object_1 \n"
            "FROM contact_data \n"
            "WHERE contact_data.gegevens_magazijn_id = natuurlijk_persoon.id AND contact_data.betrokkene_type = %(betrokkene_type_1)s) AS contact_information, (SELECT CASE WHEN (adres_1.straatnaam != %(straatnaam_1)s AND (adres_1.landcode = %(landcode_1)s OR adres_1.landcode = %(landcode_2)s)) THEN json_build_object(%(json_build_object_6)s, adres_1.straatnaam, %(json_build_object_7)s, adres_1.postcode, %(json_build_object_8)s, adres_1.huisnummer, %(json_build_object_9)s, adres_1.huisletter, %(json_build_object_10)s, adres_1.huisnummertoevoeging, %(json_build_object_11)s, adres_1.woonplaats, %(json_build_object_12)s, adres_1.landcode, %(json_build_object_13)s, adres_1.bag_id, %(json_build_object_14)s, %(json_build_object_15)s, %(json_build_object_16)s, adres_1.geo_lat_long) ELSE json_build_object(%(json_build_object_17)s, adres_1.landcode, %(json_build_object_18)s, adres_1.adres_buitenland1, %(json_build_object_19)s, adres_1.adres_buitenland2, %(json_build_object_20)s, adres_1.adres_buitenland3, %(json_build_object_21)s, NULL, %(json_build_object_22)s, %(json_build_object_23)s, %(json_build_object_24)s, adres_1.geo_lat_long) END AS anon_1 \n"
            "FROM adres AS adres_1 \n"
            "WHERE adres_1.natuurlijk_persoon_id = natuurlijk_persoon.id AND adres_1.functie_adres = %(functie_adres_1)s) AS address, (SELECT CASE WHEN (adres_2.straatnaam != %(straatnaam_2)s AND (adres_2.landcode = %(landcode_3)s OR adres_2.landcode = %(landcode_4)s)) THEN json_build_object(%(json_build_object_25)s, adres_2.straatnaam, %(json_build_object_26)s, adres_2.postcode, %(json_build_object_27)s, adres_2.huisnummer, %(json_build_object_28)s, adres_2.huisletter, %(json_build_object_29)s, adres_2.huisnummertoevoeging, %(json_build_object_30)s, adres_2.woonplaats, %(json_build_object_31)s, adres_2.landcode, %(json_build_object_32)s, adres_2.bag_id, %(json_build_object_33)s, %(json_build_object_34)s, %(json_build_object_35)s, adres_2.geo_lat_long) ELSE json_build_object(%(json_build_object_36)s, adres_2.landcode, %(json_build_object_37)s, adres_2.adres_buitenland1, %(json_build_object_38)s, adres_2.adres_buitenland2, %(json_build_object_39)s, adres_2.adres_buitenland3, %(json_build_object_40)s, NULL, %(json_build_object_41)s, %(json_build_object_42)s, %(json_build_object_43)s, adres_2.geo_lat_long) END AS anon_2 \n"
            "FROM adres AS adres_2 \n"
            "WHERE adres_2.natuurlijk_persoon_id = natuurlijk_persoon.id AND adres_2.functie_adres = %(functie_adres_2)s) AS correspondence_address, natuurlijk_persoon.indicatie_geheim AS indicatie_geheim, custom_object.uuid AS related_custom_object_uuid, subject.properties, concat(natuurlijk_persoon.adellijke_titel || %(adellijke_titel_1)s, natuurlijk_persoon.voorletters || %(voorletters_1)s, natuurlijk_persoon.surname) AS summary, object_subscription.external_id AS external_identifier \n"
            "FROM natuurlijk_persoon LEFT OUTER JOIN custom_object ON natuurlijk_persoon.related_custom_object_id = custom_object.id LEFT OUTER JOIN subject ON subject.uuid = natuurlijk_persoon.uuid AND subject.subject_type = %(subject_type_1)s LEFT OUTER JOIN object_subscription ON natuurlijk_persoon.id = CAST(object_subscription.local_id AS INTEGER) AND object_subscription.local_table = %(local_table_1)s AND object_subscription.date_deleted IS NULL \n"
            "WHERE natuurlijk_persoon.deleted_on IS NULL AND natuurlijk_persoon.uuid IN (%(uuid_1_1)s::UUID, %(uuid_1_2)s::UUID)"
        )

        select_statement = call_list[2][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT subject.uuid, subject.username AS name, coalesce(CAST(subject.properties AS JSON) -> %(param_1)s, NULL) AS first_name, coalesce(CAST(subject.properties AS JSON) -> %(param_2)s, NULL) AS surname, coalesce(CAST(subject.properties AS JSON) -> %(param_3)s, NULL) AS title, json_build_object(%(json_build_object_1)s, coalesce(CAST(subject.properties AS JSON) -> %(param_4)s, NULL), %(json_build_object_2)s, coalesce(CAST(subject.properties AS JSON) -> %(param_5)s, NULL)) AS contact_information, (SELECT coalesce(user_entity.active, %(coalesce_2)s) AS coalesce_1 \n"
            "FROM user_entity \n"
            "WHERE user_entity.subject_id = subject.id \n"
            " LIMIT %(param_6)s) AS active, (SELECT json_build_object(%(json_build_object_4)s, groups.uuid, %(json_build_object_5)s, groups.name) AS json_build_object_3 \n"
            "FROM groups \n"
            "WHERE groups.id = subject.group_ids[%(group_ids_1)s]) AS department, array((SELECT json_build_object(%(json_build_object_7)s, roles.uuid, %(json_build_object_8)s, roles.name, %(json_build_object_9)s, json_build_object(%(json_build_object_10)s, groups_1.uuid, %(json_build_object_11)s, groups_1.name)) AS json_build_object_6 \n"
            "FROM subject AS subject_1 JOIN LATERAL json_array_elements_text(array_to_json(subject_1.role_ids)) AS role_id ON subject_1.uuid = subject.uuid JOIN roles ON roles.id = CAST(role_id AS INTEGER) JOIN groups AS groups_1 ON groups_1.id = roles.parent_group_id)) AS roles, custom_object.uuid AS related_custom_object_uuid, CAST(subject.properties AS JSON) ->> %(param_7)s AS summary \n"
            "FROM subject LEFT OUTER JOIN custom_object ON subject.related_custom_object_id = custom_object.id \n"
            "WHERE subject.uuid IN (%(uuid_1_1)s::UUID, %(uuid_1_2)s::UUID, %(uuid_1_3)s::UUID, %(uuid_1_4)s::UUID) AND subject.subject_type = %(subject_type_1)s"
        )

    def test_search_case_includes_organization(self):
        employee_row = mock.MagicMock()
        employee_row.configure_mock(
            uuid=uuid4(),
            active=True,
            surname="",
            title=None,
            first_name="beheerder",
            name="testgebruiker",
            gender="Female",
            inside_municipality=True,
            properties={"anonymous": "0"},
            department={"uuid": uuid4(), "name": "Back office"},
            roles=[
                {
                    "name": "admin",
                    "uuid": uuid4(),
                    "department": {"name": "Back office", "uuid": uuid4()},
                }
            ],
            contact_information={"email": "test@test.com"},
            related_custom_object_uuid=uuid4(),
            summary="beheerder",
        )

        person_uuid = uuid4()
        person_row = mock.MagicMock()
        person_row.configure_mock(
            uuid=person_uuid,
            authenticated=True,
            authenticatedby="source",
            date_of_birth=datetime(1918, 4, 26),
            date_of_death=datetime(2004, 1, 25),
            first_names="Fanny",
            family_name="Koen",
            initials=None,
            insertions="Blankers",
            surname="Koen",
            noble_title=None,
            name="Fanny Blankers-Koen",
            gender="V",
            inside_municipality=True,
            properties={"anonymous": "0"},
            address={
                "street": "Lage Vuursche ",
                "zipcode": "1234RT",
                "street_number": 3,
                "city": "Baarn",
                "country_code": 6030,
                "bag_id": "9876543210098765",
            },
            correspondence_address={
                "street": "Lage Vuursche ",
                "zipcode": "1234RT",
                "street_number": 3,
                "city": "Baarn",
                "country_code": 6030,
            },
            contact_information={"email": "test@test.com"},
            related_custom_object_uuid=uuid4(),
            preferred_contact_channel="pip",
            summary="Fanny Blankers-Koen",
            external_identifier=None,
        )
        org_uuid = uuid4()
        org_row = mock.MagicMock()
        org_row.configure_mock(
            uuid=org_uuid,
            authenticated=True,
            authenticatedby="Intern",
            name="Company Name 1",
            coc_number="12345678",
            coc_location_number="123456789012",
            date_founded=None,
            date_registered=None,
            date_ceased=None,
            rsin=None,
            oin=None,
            main_activity={"code": 123, "description": "Something"},
            secondary_activities=[{"code": 31337, "description": "Test"}],
            business_entity_code=1,  # Eenmanszaak
            location_address={
                "street": "Test street",
                "street_number": 123,
                "street_number_letter": "Z",
                "street_number_suffix": None,
                "zipcode": "1234AB",
                "city": "Test City",
                "country": 6030,
                "bag_id": None,
            },
            correspondence_address=None,
            contact_information={},
            related_custom_object_uuid=None,
            preferred_contact_channel="pip",
            contact_person_info={},
            summary="Company Name 1",
        )
        self.mock_db_rows[0].requestor_type = "company"

        self.session.execute().fetchall.side_effect = (
            self.mock_db_rows,
            [person_row],
            [employee_row],
            [org_row],
        )
        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_subject = {
            "operator": "or",
            "values": ["test", "test1"],
        }
        includes = ["assignee", "requestor"]
        self.qry.search_case(
            page=1, page_size=10, filters=filters, includes=includes
        )

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 4

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND (zaak_1.onderwerp ILIKE %(onderwerp_1)s ESCAPE '~' OR zaak_1.onderwerp ILIKE %(onderwerp_2)s ESCAPE '~') ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

        select_statement = call_list[1][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT natuurlijk_persoon.uuid, natuurlijk_persoon.authenticated, natuurlijk_persoon.authenticatedby, natuurlijk_persoon.voornamen AS first_names, natuurlijk_persoon.voorletters AS initials, natuurlijk_persoon.voorvoegsel AS insertions, natuurlijk_persoon.geslachtsnaam AS family_name, natuurlijk_persoon.adellijke_titel AS noble_title, natuurlijk_persoon.surname AS surname, natuurlijk_persoon.geboortedatum AS date_of_birth, natuurlijk_persoon.datum_overlijden AS date_of_death, natuurlijk_persoon.geslachtsaanduiding AS gender, natuurlijk_persoon.in_gemeente AS inside_municipality, natuurlijk_persoon.landcode, natuurlijk_persoon.preferred_contact_channel, natuurlijk_persoon.active AS active, (SELECT json_build_object(%(json_build_object_2)s, contact_data.email, %(json_build_object_3)s, contact_data.telefoonnummer, %(json_build_object_4)s, contact_data.mobiel, %(json_build_object_5)s, contact_data.note) AS json_build_object_1 \n"
            "FROM contact_data \n"
            "WHERE contact_data.gegevens_magazijn_id = natuurlijk_persoon.id AND contact_data.betrokkene_type = %(betrokkene_type_1)s) AS contact_information, (SELECT CASE WHEN (adres_1.straatnaam != %(straatnaam_1)s AND (adres_1.landcode = %(landcode_1)s OR adres_1.landcode = %(landcode_2)s)) THEN json_build_object(%(json_build_object_6)s, adres_1.straatnaam, %(json_build_object_7)s, adres_1.postcode, %(json_build_object_8)s, adres_1.huisnummer, %(json_build_object_9)s, adres_1.huisletter, %(json_build_object_10)s, adres_1.huisnummertoevoeging, %(json_build_object_11)s, adres_1.woonplaats, %(json_build_object_12)s, adres_1.landcode, %(json_build_object_13)s, adres_1.bag_id, %(json_build_object_14)s, %(json_build_object_15)s, %(json_build_object_16)s, adres_1.geo_lat_long) ELSE json_build_object(%(json_build_object_17)s, adres_1.landcode, %(json_build_object_18)s, adres_1.adres_buitenland1, %(json_build_object_19)s, adres_1.adres_buitenland2, %(json_build_object_20)s, adres_1.adres_buitenland3, %(json_build_object_21)s, NULL, %(json_build_object_22)s, %(json_build_object_23)s, %(json_build_object_24)s, adres_1.geo_lat_long) END AS anon_1 \n"
            "FROM adres AS adres_1 \n"
            "WHERE adres_1.natuurlijk_persoon_id = natuurlijk_persoon.id AND adres_1.functie_adres = %(functie_adres_1)s) AS address, (SELECT CASE WHEN (adres_2.straatnaam != %(straatnaam_2)s AND (adres_2.landcode = %(landcode_3)s OR adres_2.landcode = %(landcode_4)s)) THEN json_build_object(%(json_build_object_25)s, adres_2.straatnaam, %(json_build_object_26)s, adres_2.postcode, %(json_build_object_27)s, adres_2.huisnummer, %(json_build_object_28)s, adres_2.huisletter, %(json_build_object_29)s, adres_2.huisnummertoevoeging, %(json_build_object_30)s, adres_2.woonplaats, %(json_build_object_31)s, adres_2.landcode, %(json_build_object_32)s, adres_2.bag_id, %(json_build_object_33)s, %(json_build_object_34)s, %(json_build_object_35)s, adres_2.geo_lat_long) ELSE json_build_object(%(json_build_object_36)s, adres_2.landcode, %(json_build_object_37)s, adres_2.adres_buitenland1, %(json_build_object_38)s, adres_2.adres_buitenland2, %(json_build_object_39)s, adres_2.adres_buitenland3, %(json_build_object_40)s, NULL, %(json_build_object_41)s, %(json_build_object_42)s, %(json_build_object_43)s, adres_2.geo_lat_long) END AS anon_2 \n"
            "FROM adres AS adres_2 \n"
            "WHERE adres_2.natuurlijk_persoon_id = natuurlijk_persoon.id AND adres_2.functie_adres = %(functie_adres_2)s) AS correspondence_address, natuurlijk_persoon.indicatie_geheim AS indicatie_geheim, custom_object.uuid AS related_custom_object_uuid, subject.properties, concat(natuurlijk_persoon.adellijke_titel || %(adellijke_titel_1)s, natuurlijk_persoon.voorletters || %(voorletters_1)s, natuurlijk_persoon.surname) AS summary, object_subscription.external_id AS external_identifier \n"
            "FROM natuurlijk_persoon LEFT OUTER JOIN custom_object ON natuurlijk_persoon.related_custom_object_id = custom_object.id LEFT OUTER JOIN subject ON subject.uuid = natuurlijk_persoon.uuid AND subject.subject_type = %(subject_type_1)s LEFT OUTER JOIN object_subscription ON natuurlijk_persoon.id = CAST(object_subscription.local_id AS INTEGER) AND object_subscription.local_table = %(local_table_1)s AND object_subscription.date_deleted IS NULL \n"
            "WHERE natuurlijk_persoon.deleted_on IS NULL AND natuurlijk_persoon.uuid IN (%(uuid_1_1)s::UUID)"
        )

        select_statement = call_list[3][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT bedrijf.uuid, bedrijf.authenticated, bedrijf.authenticatedby, bedrijf.handelsnaam AS name, bedrijf.date_founded, bedrijf.date_registration AS date_registered, bedrijf.date_ceased, bedrijf.rsin, bedrijf.oin, bedrijf.dossiernummer AS coc_number, bedrijf.vestigingsnummer AS coc_location_number, bedrijf.rechtsvorm AS business_entity_code, bedrijf.main_activity, bedrijf.secondairy_activities AS secondary_activities, bedrijf.preferred_contact_channel, json_build_object(%(json_build_object_1)s, contact_data.email, %(json_build_object_2)s, contact_data.telefoonnummer, %(json_build_object_3)s, contact_data.mobiel, %(json_build_object_4)s, contact_data.note, %(json_build_object_5)s, bedrijf.preferred_contact_channel, %(json_build_object_6)s, CASE WHEN (subject.properties IS NOT NULL) THEN CAST(CAST(subject.properties AS JSONB) ->> %(param_1)s AS BOOLEAN) ELSE %(param_2)s END) AS contact_information, CASE WHEN (coalesce(bedrijf.vestiging_adres_buitenland1, bedrijf.vestiging_adres_buitenland2, bedrijf.vestiging_adres_buitenland3, bedrijf.vestiging_straatnaam, %(coalesce_1)s) = %(coalesce_2)s) THEN NULL WHEN (bedrijf.vestiging_straatnaam != %(vestiging_straatnaam_1)s AND (bedrijf.vestiging_landcode = %(vestiging_landcode_1)s OR bedrijf.vestiging_landcode = %(vestiging_landcode_2)s)) THEN CAST(json_build_object(%(json_build_object_7)s, bedrijf.vestiging_straatnaam, %(json_build_object_8)s, bedrijf.vestiging_postcode, %(json_build_object_9)s, bedrijf.vestiging_huisnummer, %(json_build_object_10)s, bedrijf.vestiging_huisletter, %(json_build_object_11)s, bedrijf.vestiging_huisnummertoevoeging, %(json_build_object_12)s, bedrijf.vestiging_woonplaats, %(json_build_object_13)s, bedrijf.vestiging_landcode, %(json_build_object_14)s, bedrijf.vestiging_bag_id, %(json_build_object_15)s, %(json_build_object_16)s, %(json_build_object_17)s, bedrijf.vestiging_latlong) AS JSON) ELSE CAST(json_build_object(%(json_build_object_18)s, bedrijf.vestiging_adres_buitenland1, %(json_build_object_19)s, bedrijf.vestiging_adres_buitenland2, %(json_build_object_20)s, bedrijf.vestiging_adres_buitenland3, %(json_build_object_21)s, bedrijf.vestiging_landcode, %(json_build_object_22)s, NULL, %(json_build_object_23)s, %(json_build_object_24)s, %(json_build_object_25)s, bedrijf.vestiging_latlong) AS JSON) END AS location_address, CASE WHEN (coalesce(bedrijf.correspondentie_adres_buitenland1, bedrijf.correspondentie_adres_buitenland2, bedrijf.correspondentie_adres_buitenland3, bedrijf.correspondentie_straatnaam, %(coalesce_3)s) = %(coalesce_4)s) THEN NULL WHEN (bedrijf.correspondentie_straatnaam != %(correspondentie_straatnaam_1)s AND (bedrijf.correspondentie_landcode = %(correspondentie_landcode_1)s OR bedrijf.correspondentie_landcode = %(correspondentie_landcode_2)s)) THEN CAST(json_build_object(%(json_build_object_26)s, bedrijf.correspondentie_straatnaam, %(json_build_object_27)s, bedrijf.correspondentie_postcode, %(json_build_object_28)s, bedrijf.correspondentie_huisnummer, %(json_build_object_29)s, bedrijf.correspondentie_huisletter, %(json_build_object_30)s, bedrijf.correspondentie_huisnummertoevoeging, %(json_build_object_31)s, bedrijf.correspondentie_woonplaats, %(json_build_object_32)s, bedrijf.correspondentie_landcode, %(json_build_object_33)s, %(json_build_object_34)s) AS JSON) ELSE CAST(json_build_object(%(json_build_object_35)s, bedrijf.correspondentie_adres_buitenland1, %(json_build_object_36)s, bedrijf.correspondentie_adres_buitenland2, %(json_build_object_37)s, bedrijf.correspondentie_adres_buitenland3, %(json_build_object_38)s, bedrijf.correspondentie_landcode, %(json_build_object_39)s, %(json_build_object_40)s) AS JSON) END AS correspondence_address, custom_object.uuid AS related_custom_object_uuid, json_build_object(%(json_build_object_41)s, bedrijf.contact_voorletters, %(json_build_object_42)s, bedrijf.contact_voorvoegsel, %(json_build_object_43)s, bedrijf.contact_geslachtsnaam, %(json_build_object_44)s, bedrijf.contact_naam) AS contact_person_info, bedrijf.handelsnaam AS summary \n"
            "FROM bedrijf LEFT OUTER JOIN custom_object ON bedrijf.related_custom_object_id = custom_object.id LEFT OUTER JOIN contact_data ON contact_data.gegevens_magazijn_id = bedrijf.id AND contact_data.betrokkene_type = %(betrokkene_type_1)s LEFT OUTER JOIN subject ON subject.uuid = bedrijf.uuid AND subject.subject_type = %(subject_type_1)s \n"
            "WHERE bedrijf.deleted_on IS NULL AND bedrijf.uuid IN (%(uuid_1_1)s::UUID)"
        )

    def test_search_case_filter_urgency(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_urgency = [ValidCaseUrgency.late]
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        # query
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND coalesce(zaak_1.afhandeldatum, timezone(%(timezone_1)s, now())) >= zaak_1.streefafhandeldatum AND zaak_1.status != %(status_2)s ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_department_role(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseFilter.parse_obj(
            {
                "attributes.department_role": {
                    "operator": "or",
                    "values": [
                        {
                            "department_uuid": str(uuid4()),
                            "role_uuid": str(uuid4()),
                        },
                        {
                            "department_uuid": str(uuid4()),
                            "role_uuid": str(uuid4()),
                        },
                    ],
                }
            }
        )
        self.qry.search_case(
            page=1, page_size=10, filters=filters, full_content=True
        )

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        # query
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, zaak_1.onderwerp, zaak_1.onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND (zaak_1.route_ou = (SELECT groups.id \n"
            "FROM groups \n"
            "WHERE groups.uuid = %(uuid_2)s::UUID) AND zaak_1.route_role = (SELECT roles.id \n"
            "FROM roles \n"
            "WHERE roles.uuid = %(uuid_3)s::UUID) OR zaak_1.route_ou = (SELECT groups.id \n"
            "FROM groups \n"
            "WHERE groups.uuid = %(uuid_4)s::UUID) AND zaak_1.route_role = (SELECT roles.id \n"
            "FROM roles \n"
            "WHERE roles.uuid = %(uuid_5)s::UUID)) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_includes_case_type(self):
        metadata = mock.MagicMock(
            legal_basis="MINTY-10925_case",
            process_description="",
            may_postpone="Nee",
            may_extend="Nee",
            extension_period="",
            adjourn_period="",
            e_webform="",
            motivation="",
            purpose="",
            archive_classification_code="",
            designation_of_confidentiality="-",
            responsible_subject="",
            responsible_relationship="",
            possibility_for_objection_and_appeal="Nee",
            publication="Nee",
            publication_text="MINTY-10925_case",
            bag="Nee",
            lex_silencio_positivo="Nee",
            penalty_law="Nee",
            wkpb_applies="Nee",
            local_basis="MINTY-10925_case",
        )
        metadata.configure_mock()
        case_type_version_uuid = uuid4()
        case_type_version_row = mock.MagicMock()
        case_type_version_row.configure_mock(
            uuid=case_type_version_uuid,
            case_type_uuid=uuid4(),
            version=1,
            name="MINTY-10925_case",
            created="2023-06-14T11:35:02.708397",
            last_modified="2023-06-14T11:35:02.708397",
            deleted=None,
            initiator_source="internextern",
            description="",
            identification="MINTY-10925_case",
            is_public=False,
            tags="",
            initiator_type="aangaan",
            payment={
                "assignee": {"amount": ""},
                "frontdesk": {"amount": ""},
                "phone": {"amount": ""},
                "mail": {"amount": ""},
                "webform": {"amount": ""},
                "post": {"amount": ""},
            },
            case_summary="",
            case_public_summary="",
            terms={
                "lead_time_legal": {"type": "kalenderdagen", "value": "100"},
                "lead_time_service": {"type": "kalenderdagen", "value": "100"},
            },
            active=True,
            metadata=metadata,
        )

        self.session.execute().fetchall.side_effect = (
            self.mock_db_rows,
            case_type_version_row,
        )
        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_subject = {
            "operator": "and",
            "values": ["test", "test1"],
        }
        includes = ["case_type"]
        self.qry.search_case(
            page=1, page_size=10, filters=filters, includes=includes
        )

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 2

        select_statement = call_list[1][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT zaaktype_node.uuid AS uuid, zaaktype.uuid AS case_type_uuid, zaaktype_node.version AS version, zaaktype.active AS active, zaaktype_node.titel AS name, zaaktype_node.created AS created, zaaktype_node.deleted AS deleted, zaaktype_node.last_modified AS last_modified, zaaktype_node.trigger AS initiator_source, zaaktype_node.code AS identification, zaaktype_node.is_public AS is_public, zaaktype_node.zaaktype_trefwoorden AS tags, zaaktype_node.zaaktype_omschrijving AS description, CAST(zaaktype_node.properties AS JSON) AS properties, zaaktype_definitie.handelingsinitiator AS initiator_type, zaaktype_definitie.pdc_tarief AS webform_amount, zaaktype_definitie.procesbeschrijving AS process_description, coalesce(zaaktype_definitie.extra_informatie, %(coalesce_1)s) AS case_summary, coalesce(zaaktype_definitie.extra_informatie_extern, %(coalesce_2)s) AS case_public_summary, zaaktype_definitie.grondslag AS legal_basis, json_build_object(%(json_build_object_1)s, json_build_object(%(json_build_object_2)s, zaaktype_definitie.afhandeltermijn_type, %(json_build_object_3)s, zaaktype_definitie.afhandeltermijn), %(json_build_object_4)s, json_build_object(%(json_build_object_5)s, zaaktype_definitie.servicenorm_type, %(json_build_object_6)s, zaaktype_definitie.servicenorm)) AS terms, json_build_object(%(json_build_object_7)s, bibliotheek_categorie.uuid, %(json_build_object_8)s, bibliotheek_categorie.naam) AS catalog_folder \n"
            "FROM zaaktype_node, zaaktype, zaaktype_definitie, bibliotheek_categorie \n"
            "WHERE zaaktype_node.uuid IN (%(uuid_1_1)s::UUID, %(uuid_1_2)s::UUID) AND zaaktype.id = zaaktype_node.zaaktype_id AND zaaktype.deleted IS NULL AND zaaktype_definitie.id = zaaktype_node.zaaktype_definitie_id AND bibliotheek_categorie.id = zaaktype.bibliotheek_categorie_id"
        )

    def test_search_case_includes_case_type_result(self):
        case_type_result_uuid = uuid4()
        case_type_result_row = mock.Mock()
        case_type_result_row.configure_mock(
            uuid=case_type_result_uuid,
            description="description",
            result="MINTY-10925_case",
            trigger_archival=True,
            result_explanation="result_explanation",
            standard_choice=True,
            selection_list="selection_list",
            selection_list_start="2023-06-14",
            selection_list_end="2023-06-14",
            type_of_archiving="type_of_archiving",
            retention_period=1,
            retention_period_source_date="2023-06-14",
            process_type_generic="process_type_generic",
            process_type_name="process_type_name",
            process_type_number="process_type_number",
            process_type_object="process_type_object",
            process_type_description="process_type_description",
            process_type_explanation="process_type_explanation",
            origin="origin",
            process_term="process_term",
            selection_list_number="selection_list_number",
        )
        self.session.execute().fetchall.side_effect = (
            self.mock_db_rows,
            [case_type_result_row],
        )
        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_subject = {
            "operator": "or",
            "values": ["test", "test1"],
        }
        includes = ["case_type_result"]
        self.qry.search_case(
            page=1, page_size=10, filters=filters, includes=includes
        )

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 2

        select_statement = call_list[1][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT zaaktype_resultaten_1.uuid AS uuid, zaaktype_resultaten_1.label AS description, zaaktype_resultaten_1.resultaat AS result, zaaktype_resultaten_1.trigger_archival AS trigger_archival, zaaktype_resultaten_1.standaard_keuze AS standard_choice, zaaktype_resultaten_1.selectielijst AS selection_list, zaaktype_resultaten_1.selectielijst_brondatum AS selection_list_start, zaaktype_resultaten_1.selectielijst_einddatum AS selection_list_end, zaaktype_resultaten_1.archiefnominatie AS type_of_archiving, zaaktype_resultaten_1.bewaartermijn AS retention_period, zaaktype_resultaten_1.ingang AS retention_period_source_date, zaaktype_resultaten_1.comments AS result_explanation, CAST(zaaktype_resultaten_1.properties AS JSON) ->> %(param_1)s AS process_term, CAST(zaaktype_resultaten_1.properties AS JSON) ->> %(param_2)s AS process_type_generic, CAST(zaaktype_resultaten_1.properties AS JSON) ->> %(param_3)s AS process_type_name, CAST(zaaktype_resultaten_1.properties AS JSON) ->> %(param_4)s AS process_type_number, CAST(zaaktype_resultaten_1.properties AS JSON) ->> %(param_5)s AS process_type_object, CAST(zaaktype_resultaten_1.properties AS JSON) ->> %(param_6)s AS process_type_description, CAST(zaaktype_resultaten_1.properties AS JSON) ->> %(param_7)s AS process_type_explanation, CAST(zaaktype_resultaten_1.properties AS JSON) ->> %(param_8)s AS selection_list_number, CAST(zaaktype_resultaten_1.properties AS JSON) ->> %(param_9)s AS origin \n"
            "FROM zaaktype_resultaten AS zaaktype_resultaten_1 \n"
            "WHERE zaaktype_resultaten_1.uuid IN (%(uuid_1_1)s::UUID, %(uuid_1_2)s::UUID)"
        )

    def test_search_case_filter_phase(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_case_phase = {
            "operator": "or",
            "values": ["test", "test1"],
        }
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        # query
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND zaak_1.milestone IN (SELECT zaaktype_status.status - %(status_2)s AS anon_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaak_1.zaaktype_node_id AND (zaaktype_status.fase ILIKE %(fase_1)s ESCAPE '~' OR zaaktype_status.fase ILIKE %(fase_2)s ESCAPE '~')) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

        # test filter_case_phase with and operator
        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_case_phase = {
            "operator": "and",
            "values": ["test", "test1"],
        }
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        # query
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND zaak_1.milestone IN (SELECT zaaktype_status.status - %(status_2)s AS anon_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaak_1.zaaktype_node_id AND zaaktype_status.fase ILIKE %(fase_1)s ESCAPE '~' AND zaaktype_status.fase ILIKE %(fase_2)s ESCAPE '~') ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_price_filter(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_case_price = {"values": ["gt 2.34", "lt 1.2"]}
        self.qry.search_case(
            page=1,
            page_size=10,
            filters=filters,
            sort="number",
        )

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND zaak_1.payment_amount > %(payment_amount_1)s AND zaak_1.payment_amount < %(payment_amount_2)s ORDER BY zaak_1.id ASC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

        filters.filter_case_price = {
            "operator": "or",
            "values": ["gt 2.34", "lt 1.3"],
        }
        self.qry.search_case(
            page=1,
            page_size=10,
            filters=filters,
            sort="number",
        )

        select_statement = call_list[1][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND (zaak_1.payment_amount > %(payment_amount_1)s OR zaak_1.payment_amount < %(payment_amount_2)s) ORDER BY zaak_1.id ASC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_custom_master_number_filter(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_case_custom_number = ["test-1234", "1122"]
        filters.filter_number_master = [11]
        filters.filter_case_number = [12]
        self.qry.search_case(
            page=1,
            page_size=10,
            filters=filters,
            sort="number",
        )

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND (zaak_1.prefix ILIKE %(prefix_1)s ESCAPE '~' OR zaak_1.prefix ILIKE %(prefix_2)s ESCAPE '~') AND zaak_1.id IN (%(id_1_1)s) AND zaak_1.number_master IN (%(number_master_1_1)s) ORDER BY zaak_1.id ASC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_external_reference(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_external_reference = {
            "operator": "or",
            "values": ["test"],
        }
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND zaak_1.onderwerp_extern ILIKE %(onderwerp_extern_1)s ESCAPE '~' ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_external_reference = {
            "operator": "and",
            "values": ["test", "zaak"],
        }
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND zaak_1.onderwerp_extern ILIKE %(onderwerp_extern_1)s ESCAPE '~' AND zaak_1.onderwerp_extern ILIKE %(onderwerp_extern_2)s ESCAPE '~' ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_includes_case_location_no_geocoordinates(self):
        case_location = mock.Mock()
        case_location_uuid = uuid4()
        case_location.configure_mock(
            entity_id=case_location_uuid,
            uuid=case_location_uuid,
            bag_type="nummeraanduiding",
            bag_id="0381200000105873",
            residential_object_id="0381010000029177",
            public_space_id="0381300000100096",
            number_indication_id="0381200000105873",
            premise_id="0381100000118106",
            location_id=None,
            lay_id=None,
            coordinates=None,
            full_address="Ellermanstraat 11, 1122AB Amsterdam",
            geo_coordinates="5.18153304-52.02462029)",
        )

        self.session.execute().fetchall.side_effect = (
            self.mock_db_rows,
            [case_location],
        )
        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_subject = {
            "operator": "or",
            "values": ["test", "test1"],
        }

        includes = ["case_location"]
        self.qry.search_case(
            page=1, page_size=10, filters=filters, includes=includes
        )

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 2

    def test_search_case__apply_filter_case_type_confidentiality(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_case_type_confidentiality = [
            "Zaakvertrouwelijk",
            "Geheim",
        ]
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND ((CAST(zaaktype_node.properties AS JSON) ->> %(param_3)s) = %(param_4)s OR (CAST(zaaktype_node.properties AS JSON) ->> %(param_5)s) = %(param_6)s) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_7)s OFFSET %(param_8)s"
        )

    def test_search_case__apply_filter_case_type_identification(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_case_type_identification = ["string1", "string2"]
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND (zaaktype_node.code ILIKE %(code_1)s ESCAPE '~' OR zaaktype_node.code ILIKE %(code_2)s ESCAPE '~') ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case__apply_filter_case_type_version(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_case_type_version = [uuid4()]
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND zaaktype_node.uuid IN (%(uuid_2_1)s::UUID) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_requestor_coc_number_zipcode(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_requestor_coc_number = ["11223344"]
        filters.filter_requestor_zipcode = ["1122AA"]
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND (((((zaak_1.requestor_v1_json -> %(requestor_v1_json_4)s) -> %(param_3)s) -> %(param_4)s)) ->> %(param_5)s) IN (%(param_6_1)s) AND (((((((zaak_1.requestor_v1_json -> %(requestor_v1_json_5)s) -> %(param_7)s) -> %(param_8)s) -> %(param_9)s) -> %(param_10)s)) ->> %(param_11)s) IN (%(param_12_1)s) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_13)s OFFSET %(param_14)s"
        )

    def test_search_case_requestor_details(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_requestor_noble_title = ["MR"]
        filters.filter_requestor_coc_location_number = ["121212"]
        filters.filter_requestor_trade_name = ["Trade"]
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND lower((((((zaak_1.requestor_v1_json -> %(requestor_v1_json_4)s) -> %(param_3)s) -> %(param_4)s)) ->> %(param_5)s)) IN (%(lower_1_1)s) AND (((((zaak_1.requestor_v1_json -> %(requestor_v1_json_5)s) -> %(param_6)s) -> %(param_7)s)) ->> %(param_8)s) IN (%(param_9_1)s) AND zaak_1.aanvrager_gm_id IN (SELECT bedrijf.id \n"
            "FROM bedrijf \n"
            "WHERE bedrijf.handelsnaam ILIKE %(handelsnaam_1)s ESCAPE '~') AND zaak_1.aanvrager_type = %(aanvrager_type_2)s ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_10)s OFFSET %(param_11)s"
        )

    def test_search_case_requestor_investigation_secret(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_requestor_is_secret = True
        filters.filter_requestor_investigation = True
        filters.operator = "or"
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND (CAST(((((zaak_1.requestor_v1_json -> %(requestor_v1_json_4)s) -> %(param_3)s) -> %(param_4)s) -> %(param_5)s) AS BOOLEAN) IS true OR zaak_1.aanvrager_gm_id IN (SELECT natuurlijk_persoon_1.id \n"
            "FROM natuurlijk_persoon AS natuurlijk_persoon_1 \n"
            "WHERE natuurlijk_persoon_1.onderzoek_persoon IS true) AND zaak_1.aanvrager_type = %(aanvrager_type_2)s) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_6)s OFFSET %(param_7)s"
        )

    def test_search_case_requestor_dates_filter(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_requestor_date_of_birth = ["gt 1930-02-08T11:13:16Z"]
        filters.filter_requestor_date_of_death = ["lt 2024-02-08T11:13:16Z"]
        filters.filter_requestor_date_of_marriage = ["gt 1960-02-08T11:13:16Z"]
        filters.operator = "or"
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND zaak_1.aanvrager_gm_id IN (SELECT natuurlijk_persoon_1.id \n"
            "FROM natuurlijk_persoon AS natuurlijk_persoon_1 \n"
            "WHERE natuurlijk_persoon_1.geboortedatum > %(geboortedatum_1)s OR natuurlijk_persoon_1.datum_overlijden < %(datum_overlijden_1)s OR natuurlijk_persoon_1.datum_huwelijk > %(datum_huwelijk_1)s) AND zaak_1.aanvrager_type = %(aanvrager_type_2)s ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_requestor_gender(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_requestor_gender = ["F"]
        filters.operator = "or"
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND (((((zaak_1.requestor_v1_json -> %(requestor_v1_json_4)s) -> %(param_3)s) -> %(param_4)s)) ->> %(param_5)s) IN (%(param_6_1)s, %(param_6_2)s) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_7)s OFFSET %(param_8)s"
        )

        assert compiled_select.params["param_6_1"] == "F"
        assert compiled_select.params["param_6_2"] == "V"

        self.session.reset_mock()
        filters.filter_requestor_gender = ["X"]
        filters.operator = "or"
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert compiled_select.params["param_6_1"] == "X"
        assert compiled_select.params["param_6_2"] == "O"

    def test_search_case_filter_requestor_department(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_requestor_department = [uuid4()]
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND zaak_1.aanvrager_gm_id IN (SELECT subject.id \n"
            "FROM groups JOIN subject ON groups.id = subject.group_ids[%(group_ids_1)s] \n"
            "WHERE groups.uuid IN (%(uuid_2_1)s::UUID)) AND zaak_1.aanvrager_type = %(aanvrager_type_2)s ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_requestor_correspondence_address_filter(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_requestor_correspondence_zipcode = ["1122AA"]
        filters.filter_requestor_correspondence_street = ["street"]
        filters.filter_requestor_correspondence_city = ["city"]
        filters.filter_requestor_street = ["street"]
        filters.filter_requestor_city = ["city"]
        filters.operator = "or"
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND ((((((((zaak_1.requestor_v1_json -> %(requestor_v1_json_4)s) -> %(param_3)s) -> %(param_4)s) -> %(param_5)s) -> %(param_6)s)) ->> %(param_7)s) IN (%(param_8_1)s) OR (((((((zaak_1.requestor_v1_json -> %(requestor_v1_json_5)s) -> %(param_9)s) -> %(param_10)s) -> %(param_11)s) -> %(param_12)s)) ->> %(param_13)s) ILIKE %(param_14)s ESCAPE '~' OR (((((((zaak_1.requestor_v1_json -> %(requestor_v1_json_6)s) -> %(param_15)s) -> %(param_16)s) -> %(param_17)s) -> %(param_18)s)) ->> %(param_19)s) ILIKE %(param_20)s ESCAPE '~' OR (((((((zaak_1.requestor_v1_json -> %(requestor_v1_json_7)s) -> %(param_21)s) -> %(param_22)s) -> %(param_23)s) -> %(param_24)s)) ->> %(param_25)s) ILIKE %(param_26)s ESCAPE '~' OR (((((((zaak_1.requestor_v1_json -> %(requestor_v1_json_8)s) -> %(param_27)s) -> %(param_28)s) -> %(param_29)s) -> %(param_30)s)) ->> %(param_31)s) ILIKE %(param_32)s ESCAPE '~') ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_33)s OFFSET %(param_34)s"
        )

    def test_search_case_attribute_address_filter(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_custom_fields = {
            "operator": "or",
            "values": [
                {
                    "magic_string": "address_v2",
                    "type": "address_v2",
                    "values": ["string"],
                }
            ],
        }
        filters.operator = "or"
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND zaak_1.id IN (SELECT anon_1.id \n"
            "FROM (SELECT zaak_kenmerk.zaak_id AS id \n"
            "FROM zaak_kenmerk \n"
            "WHERE (zaak_kenmerk.magic_string = %(magic_string_1)s AND zaak_kenmerk.value_type = %(value_type_2)s AND ((CAST(zaak_kenmerk.value[%(value_1)s] AS JSONB) -> %(param_3)s) ->> %(param_4)s) = %(param_5)s)) AS anon_1) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_6)s OFFSET %(param_7)s"
        )
        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_custom_fields = {
            "operator": "and",
            "values": [
                {
                    "magic_string": "bag_openbareruimte",
                    "type": "bag_openbareruimte",
                    "values": ["string1", "bag_string_2"],
                }
            ],
        }
        filters.operator = "or"
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1
        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_custom_fields = {
            "operator": "or",
            "values": [
                {
                    "magic_string": "bag_adressen",
                    "type": "bag_adressen",
                    "values": ["string1", "bag_string_2"],
                }
            ],
        }
        filters.operator = "or"
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

    def test_search_case_attribute_address_filter_no_type(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_custom_fields = {
            "operator": "or",
            "values": [
                {
                    "magic_string": "test_type",
                    "type": "test_type",
                    "values": ["string1", "bag_string_2"],
                }
            ],
        }
        filters.operator = "or"
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_attribute_choise_filter(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_custom_fields = {
            "operator": "or",
            "values": [
                {
                    "magic_string": "checkbox_attribute",
                    "type": "checkbox",
                    "values": ["string1", "bag_string_2"],
                },
                {
                    "magic_string": "checkbox_attribute2",
                    "type": "checkbox",
                    "values": ["string1", "bag_string_2"],
                    "operator": "and",
                },
                {
                    "magic_string": "option_attribute",
                    "type": "option",
                    "values": ["option1"],
                },
            ],
        }
        filters.operator = "or"
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND zaak_1.id IN (SELECT anon_1.id \n"
            "FROM (SELECT zaak_kenmerk.zaak_id AS id \n"
            "FROM zaak_kenmerk \n"
            "WHERE (zaak_kenmerk.magic_string = %(magic_string_1)s AND zaak_kenmerk.value_type = %(value_type_2)s AND ((zaak_kenmerk.value @> %(value_1)s::TEXT[]) OR (zaak_kenmerk.value @> %(value_2)s::TEXT[]))) OR (zaak_kenmerk.magic_string = %(magic_string_2)s AND zaak_kenmerk.value_type = %(value_type_3)s AND (zaak_kenmerk.value @> %(value_3)s::TEXT[])) OR (zaak_kenmerk.magic_string = %(magic_string_3)s AND zaak_kenmerk.value_type = %(value_type_4)s AND CAST(zaak_kenmerk.value[%(value_4)s] AS TEXT) = %(param_3)s)) AS anon_1) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_4)s OFFSET %(param_5)s"
        )

    def test_search_case_attribute_relationship(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseFilter()
        subject_1_uuid = uuid4()
        subject_2_uuid = uuid4()
        filters.filter_custom_fields = {
            "operator": "or",
            "values": [
                {
                    "magic_string": "attributerelationshipsubject",
                    "type": "relationship_subject",
                    "values": [subject_1_uuid, subject_2_uuid],
                },
                {
                    "magic_string": "attributerelationshipobject",
                    "type": "relationship_custom_object",
                },
            ],
        }
        filters.operator = "or"
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND zaak_1.id IN (SELECT anon_1.id \n"
            "FROM (SELECT zaak_kenmerk.zaak_id AS id \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE (zaak_kenmerk.magic_string = %(magic_string_1)s AND bibliotheek_kenmerken.relationship_type = %(relationship_type_1)s AND zaak_kenmerk.value_type = %(value_type_2)s AND ((CAST(zaak_kenmerk.value[%(value_1)s] AS JSONB) ->> %(param_3)s) = %(param_4)s::UUID OR (CAST(zaak_kenmerk.value[%(value_2)s] AS JSONB) ->> %(param_5)s) = %(param_6)s::UUID)) OR (zaak_kenmerk.value != %(value_3)s AND zaak_kenmerk.magic_string = %(magic_string_2)s AND bibliotheek_kenmerken.relationship_type = %(relationship_type_2)s AND zaak_kenmerk.value_type = %(value_type_3)s)) AS anon_1) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_7)s OFFSET %(param_8)s"
        )

    def test_search_case_filter_handle_error(self):
        mock_db_rows = [mock.Mock()]
        mock_db_rows[0].configure_mock(
            id=2,
            uuid=uuid4(),
            case_type_version=uuid4(),
            case_type_result=uuid4(),
            unaccepted_files_count=1,
            unread_communication_count=2,
            unaccepted_attribute_update_count=3,
            requestor_name="Diederik-Jan",
            requestor_type="person",
            requestor_uuid=uuid4(),
            assignee_name="Sjaak",
            assignee_uuid=uuid4(),
            coordinator_uuid=uuid4(),
            coordinator_name="Sjaak",
            registratiedatum="2022-02-08",
        )
        self.user_info = UserInfo(
            user_uuid=self.user_uuid,
            permissions={"admin": False},
        )
        self.qry.user_info = self.user_info
        self.session.execute().fetchall.return_value = mock_db_rows
        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_status = {ValidCaseStatus.new, ValidCaseStatus.open}
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        # query
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id AND ((CAST(bibliotheek_kenmerken.properties AS JSON) ->> %(param_3)s) IS NULL OR (CAST(bibliotheek_kenmerken.properties AS JSON) ->> %(param_4)s) != %(param_5)s))) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND zaak_1.status IN (%(status_2_1)s, %(status_2_2)s) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_6)s OFFSET %(param_7)s"
        )

    @mock.patch(
        "zsnl_domains.case_management.repositories.employee.EmployeeRepository.get_employees_by_uuid"
    )
    def test_search_case_query_handle_error(self, mock_find_employee):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        mock_find_employee.side_effect = TypeError
        self.session.reset_mock()

        includes = ["assignee", "coordinator", "requestor"]
        self.qry.search_case(page=1, page_size=10, includes=includes)

    def test_search_case_attribute_email_filter(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_custom_fields = {
            "operator": "or",
            "values": [
                {
                    "magic_string": "attribute_email",
                    "type": "email",
                    "values": ["EmaIL@email.com"],
                },
            ],
        }
        filters.operator = "or"
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND zaak_1.id IN (SELECT anon_1.id \n"
            "FROM (SELECT zaak_kenmerk.zaak_id AS id \n"
            "FROM zaak_kenmerk \n"
            "WHERE (zaak_kenmerk.magic_string = %(magic_string_1)s AND zaak_kenmerk.value_type = %(value_type_2)s AND lower(CAST(zaak_kenmerk.value[%(value_1)s] AS TEXT)) = %(lower_1)s)) AS anon_1) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_attribute_date(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_custom_fields = {
            "values": [
                {
                    "magic_string": "attributedate",
                    "type": "date",
                    "values": [
                        ["gt 2021-05-01", "lt 2025-05-01"],
                    ],
                },
            ],
        }
        filters.operator = "or"
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND zaak_1.id IN (SELECT anon_1.id \n"
            "FROM (SELECT zaak_kenmerk.zaak_id AS id \n"
            "FROM zaak_kenmerk \n"
            "WHERE (zaak_kenmerk.magic_string = %(magic_string_1)s AND zaak_kenmerk.value_type = %(value_type_2)s AND ((zaak_kenmerk.value[%(value_1)s] ~ %(param_3)s) AND CAST(immutable_to_date(zaak_kenmerk.value[%(value_2)s], %(immutable_to_date_1)s) AS DATE) > %(param_4)s OR (zaak_kenmerk.value[%(value_3)s] ~ %(param_5)s) AND immutable_text_to_date(zaak_kenmerk.value[%(value_4)s]) > %(immutable_text_to_date_1)s OR (zaak_kenmerk.value[%(value_5)s] ~ %(param_6)s) AND CAST(immutable_to_date(zaak_kenmerk.value[%(value_6)s], %(immutable_to_date_2)s) AS DATE) > %(param_7)s) AND ((zaak_kenmerk.value[%(value_7)s] ~ %(param_8)s) AND CAST(immutable_to_date(zaak_kenmerk.value[%(value_2)s], %(immutable_to_date_1)s) AS DATE) < %(param_9)s OR (zaak_kenmerk.value[%(value_8)s] ~ %(param_10)s) AND immutable_text_to_date(zaak_kenmerk.value[%(value_4)s]) < %(immutable_text_to_date_2)s OR (zaak_kenmerk.value[%(value_9)s] ~ %(param_11)s) AND CAST(immutable_to_date(zaak_kenmerk.value[%(value_6)s], %(immutable_to_date_2)s) AS DATE) < %(param_12)s))) AS anon_1) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_13)s OFFSET %(param_14)s"
        )

        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_custom_fields = {
            "values": [
                {
                    "magic_string": "attributedate",
                    "type": "date",
                    "values": [
                        "eq 2021-05-01",
                        "eq 2021-05-07",
                    ],
                    "operator": "or",
                },
            ],
        }
        filters.operator = "or"
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND zaak_1.id IN (SELECT anon_1.id \n"
            "FROM (SELECT zaak_kenmerk.zaak_id AS id \n"
            "FROM zaak_kenmerk \n"
            "WHERE (zaak_kenmerk.magic_string = %(magic_string_1)s AND zaak_kenmerk.value_type = %(value_type_2)s AND ((zaak_kenmerk.value[%(value_1)s] ~ %(param_3)s) AND CAST(immutable_to_date(zaak_kenmerk.value[%(value_2)s], %(immutable_to_date_1)s) AS DATE) = %(param_4)s OR (zaak_kenmerk.value[%(value_3)s] ~ %(param_5)s) AND immutable_text_to_date(zaak_kenmerk.value[%(value_4)s]) = %(immutable_text_to_date_1)s OR (zaak_kenmerk.value[%(value_5)s] ~ %(param_6)s) AND CAST(immutable_to_date(zaak_kenmerk.value[%(value_6)s], %(immutable_to_date_2)s) AS DATE) = %(param_7)s OR (zaak_kenmerk.value[%(value_7)s] ~ %(param_8)s) AND CAST(immutable_to_date(zaak_kenmerk.value[%(value_2)s], %(immutable_to_date_1)s) AS DATE) = %(param_9)s OR (zaak_kenmerk.value[%(value_8)s] ~ %(param_10)s) AND immutable_text_to_date(zaak_kenmerk.value[%(value_4)s]) = %(immutable_text_to_date_2)s OR (zaak_kenmerk.value[%(value_9)s] ~ %(param_11)s) AND CAST(immutable_to_date(zaak_kenmerk.value[%(value_6)s], %(immutable_to_date_2)s) AS DATE) = %(param_12)s))) AS anon_1) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_13)s OFFSET %(param_14)s"
        )

    def test_search_case_attribute_numeric_filter(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_custom_fields = {
            "values": [
                {
                    "magic_string": "attribute_numeric",
                    "type": "numeric",
                    "values": ["11223344"],
                },
                {
                    "magic_string": "attribute_bankaccount",
                    "type": "bankaccount",
                    "values": ["NL91ABNA0123456700"],
                },
            ],
        }
        filters.operator = "and"
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND zaak_1.id IN (SELECT anon_1.id \n"
            "FROM (SELECT zaak_kenmerk.zaak_id AS id \n"
            "FROM zaak_kenmerk \n"
            "WHERE (zaak_kenmerk.magic_string = %(magic_string_1)s AND zaak_kenmerk.value_type = %(value_type_2)s AND %(param_3)s = ANY (zaak_kenmerk.value)) OR (zaak_kenmerk.magic_string = %(magic_string_2)s AND zaak_kenmerk.value_type = %(value_type_3)s AND CAST(zaak_kenmerk.value[%(value_1)s] AS TEXT) = %(param_4)s) GROUP BY zaak_kenmerk.zaak_id \n"
            "HAVING count(distinct(zaak_kenmerk.value)) >= %(count_2)s) AS anon_1) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_5)s OFFSET %(param_6)s"
        )

    def test_search_case_attribute_valuta_filter(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_custom_fields = {
            "operator": "or",
            "values": [
                {
                    "magic_string": "attribute_valuta",
                    "type": "valuta",
                    "operator": "and",
                    "values": ["lt 12.3", "gt 3.5"],
                },
            ],
        }
        filters.operator = "or"
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND zaak_1.id IN (SELECT anon_1.id \n"
            "FROM (SELECT zaak_kenmerk.zaak_id AS id \n"
            "FROM zaak_kenmerk \n"
            "WHERE (zaak_kenmerk.magic_string = %(magic_string_1)s AND zaak_kenmerk.value_type = %(value_type_2)s AND CAST(zaak_kenmerk.value[%(value_1)s] AS FLOAT) < %(param_3)s AND CAST(zaak_kenmerk.value[%(value_1)s] AS FLOAT) > %(param_4)s)) AS anon_1) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_5)s OFFSET %(param_6)s"
        )

        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_custom_fields = {
            "operator": "or",
            "values": [
                {
                    "magic_string": "attribute_valuta",
                    "type": "valuta",
                    "operator": "or",
                    "values": ["lt 2.3", "gt 3.5"],
                },
            ],
        }
        filters.operator = "or"
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND zaak_1.id IN (SELECT anon_1.id \n"
            "FROM (SELECT zaak_kenmerk.zaak_id AS id \n"
            "FROM zaak_kenmerk \n"
            "WHERE (zaak_kenmerk.magic_string = %(magic_string_1)s AND zaak_kenmerk.value_type = %(value_type_2)s AND (CAST(zaak_kenmerk.value[%(value_1)s] AS FLOAT) < %(param_3)s OR CAST(zaak_kenmerk.value[%(value_1)s] AS FLOAT) > %(param_4)s))) AS anon_1) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_5)s OFFSET %(param_6)s"
        )

    def test_search_case_attribute_file_filter(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_custom_fields = {
            "values": [
                {
                    "magic_string": "attribute_file",
                    "type": "file",
                },
                {
                    "magic_string": "attribute_file_1",
                    "type": "file",
                },
            ],
        }
        filters.operator = "and"
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND zaak_1.id IN (SELECT anon_1.id \n"
            "FROM (SELECT file_case_document.case_id AS id \n"
            "FROM file_case_document \n"
            "WHERE file_case_document.magic_string IN (%(magic_string_1_1)s, %(magic_string_1_2)s) GROUP BY file_case_document.case_id \n"
            "HAVING count(distinct(file_case_document.magic_string)) >= %(count_2)s) AS anon_1) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_custom_fields = {
            "values": [
                {
                    "magic_string": "attribute_file",
                    "type": "file",
                },
                {
                    "magic_string": "attribute_file_1",
                    "type": "file",
                },
            ],
        }
        filters.operator = "or"
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND zaak_1.id IN (SELECT anon_1.id \n"
            "FROM (SELECT file_case_document.case_id AS id \n"
            "FROM file_case_document \n"
            "WHERE file_case_document.magic_string IN (%(magic_string_1_1)s, %(magic_string_1_2)s) GROUP BY file_case_document.case_id) AS anon_1) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_custom_fields = {
            "values": [
                {
                    "magic_string": "attribute_file",
                    "type": "file",
                },
                {
                    "magic_string": "attribute_file_1",
                    "type": "file",
                },
                {
                    "magic_string": "attribute_numeric",
                    "type": "numeric",
                    "values": ["11223344"],
                },
            ],
        }
        filters.operator = "or"
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND zaak_1.id IN (SELECT anon_1.id \n"
            "FROM (SELECT zaak_kenmerk.zaak_id AS id \n"
            "FROM zaak_kenmerk \n"
            "WHERE (zaak_kenmerk.magic_string = %(magic_string_1)s AND zaak_kenmerk.value_type = %(value_type_2)s AND %(param_3)s = ANY (zaak_kenmerk.value)) UNION SELECT file_case_document.case_id AS id \n"
            "FROM file_case_document \n"
            "WHERE file_case_document.magic_string IN (%(magic_string_2_1)s, %(magic_string_2_2)s) GROUP BY file_case_document.case_id) AS anon_1) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_4)s OFFSET %(param_5)s"
        )

        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_custom_fields = {
            "values": [
                {
                    "magic_string": "attribute_file",
                    "type": "file",
                },
                {
                    "magic_string": "attribute_file_1",
                    "type": "file",
                },
                {
                    "magic_string": "attribute_numeric",
                    "type": "numeric",
                    "values": ["11223344"],
                },
            ],
        }
        filters.operator = "and"
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND zaak_1.id IN (SELECT anon_1.id \n"
            "FROM (SELECT zaak_kenmerk.zaak_id AS id \n"
            "FROM zaak_kenmerk \n"
            "WHERE (zaak_kenmerk.magic_string = %(magic_string_1)s AND zaak_kenmerk.value_type = %(value_type_2)s AND %(param_3)s = ANY (zaak_kenmerk.value)) INTERSECT SELECT file_case_document.case_id AS id \n"
            "FROM file_case_document \n"
            "WHERE file_case_document.magic_string IN (%(magic_string_2_1)s, %(magic_string_2_2)s) GROUP BY file_case_document.case_id \n"
            "HAVING count(distinct(file_case_document.magic_string)) >= %(count_2)s) AS anon_1) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_4)s OFFSET %(param_5)s"
        )

    @patch("zsnl_domains.shared.case_filters.apply_filter_file")
    def test_search_case_attribute_file_filter_case_no_file_attributes(
        self, mock_apply_filter_file
    ):
        mock_apply_filter_file.return_value = None
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_custom_fields = {
            "values": [
                {
                    "magic_string": "attribute_file",
                    "type": "file",
                },
                {
                    "magic_string": "attribute_file_1",
                    "type": "file",
                },
                {
                    "magic_string": "attribute_valuta",
                    "type": "valuta",
                    "operator": "and",
                    "values": ["lt 12.3", "gt 3.5"],
                },
            ],
        }
        filters.operator = "and"
        self.qry.search_case(page=1, page_size=10, filters=filters)

        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_custom_fields = {
            "values": [
                {
                    "magic_string": "attribute_file",
                    "type": "file",
                },
                {
                    "magic_string": "attribute_file_1",
                    "type": "file",
                },
                {
                    "magic_string": "attribute_valuta",
                    "type": "valuta",
                    "operator": "and",
                    "values": ["lt 12.3", "gt 3.5"],
                },
            ],
        }
        filters.operator = "or"
        self.qry.search_case(page=1, page_size=10, filters=filters)

    def test_search_case_attribute_text_filter(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_custom_fields = {
            "values": [
                {
                    "magic_string": "attribute_text",
                    "type": "text",
                    "values": ["text_test", "test_ text"],
                    "operator": "and",
                },
            ],
        }
        filters.operator = "or"
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND zaak_1.id IN (SELECT anon_1.id \n"
            "FROM (SELECT zaak_kenmerk.zaak_id AS id \n"
            "FROM zaak_kenmerk \n"
            "WHERE (zaak_kenmerk.magic_string = %(magic_string_1)s AND zaak_kenmerk.value_type = %(value_type_2)s AND immutable_array_to_string(zaak_kenmerk.value) ILIKE %(immutable_array_to_string_1)s ESCAPE '~' AND immutable_array_to_string(zaak_kenmerk.value) ILIKE %(immutable_array_to_string_2)s ESCAPE '~')) AS anon_1) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_custom_fields = {
            "values": [
                {
                    "magic_string": "attribute_text",
                    "type": "text",
                    "values": ["text_test", "test_ text"],
                    "operator": "or",
                },
            ],
        }
        filters.operator = "or"
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND zaak_1.id IN (SELECT anon_1.id \n"
            "FROM (SELECT zaak_kenmerk.zaak_id AS id \n"
            "FROM zaak_kenmerk \n"
            "WHERE (zaak_kenmerk.magic_string = %(magic_string_1)s AND zaak_kenmerk.value_type = %(value_type_2)s AND (immutable_array_to_string(zaak_kenmerk.value) ILIKE %(immutable_array_to_string_1)s ESCAPE '~' OR immutable_array_to_string(zaak_kenmerk.value) ILIKE %(immutable_array_to_string_2)s ESCAPE '~'))) AS anon_1) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_custom_fields = {
            "values": [
                {
                    "magic_string": "attribute_richtext",
                    "type": "richtext",
                    "values": ["richtext_test", "rich text"],
                },
            ],
        }
        filters.operator = "or"
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND zaak_1.id IN (SELECT anon_1.id \n"
            "FROM (SELECT zaak_kenmerk.zaak_id AS id \n"
            "FROM zaak_kenmerk \n"
            "WHERE (zaak_kenmerk.magic_string = %(magic_string_1)s AND zaak_kenmerk.value_type = %(value_type_2)s AND (attribute_richtext_strip_html_tags(zaak_kenmerk.value) ILIKE %(attribute_richtext_strip_html_tags_1)s ESCAPE '~' OR attribute_richtext_strip_html_tags(zaak_kenmerk.value) ILIKE %(attribute_richtext_strip_html_tags_2)s ESCAPE '~'))) AS anon_1) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_custom_fields = {
            "values": [
                {
                    "magic_string": "attribute_richtext",
                    "type": "richtext",
                    "values": ["richtext_test", "rich text"],
                    "operator": "and",
                },
            ],
        }
        filters.operator = "or"
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND zaak_1.id IN (SELECT anon_1.id \n"
            "FROM (SELECT zaak_kenmerk.zaak_id AS id \n"
            "FROM zaak_kenmerk \n"
            "WHERE (zaak_kenmerk.magic_string = %(magic_string_1)s AND zaak_kenmerk.value_type = %(value_type_2)s AND attribute_richtext_strip_html_tags(zaak_kenmerk.value) ILIKE %(attribute_richtext_strip_html_tags_1)s ESCAPE '~' AND attribute_richtext_strip_html_tags(zaak_kenmerk.value) ILIKE %(attribute_richtext_strip_html_tags_2)s ESCAPE '~')) AS anon_1) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_type_of_archiving_filter(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_type_of_archiving = ["Bewaren (B)"]
        filters.operator = "or"
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND zaaktype_resultaten_1.archiefnominatie IN (%(archiefnominatie_1_1)s) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_includes_case_role_department(self):
        role_uuid = uuid4()
        role_row = mock.MagicMock()
        role_row.configure_mock(uuid=role_uuid, name="role")

        self.session.execute().fetchall.side_effect = (
            self.mock_db_rows,
            role_row,
        )
        self.session.reset_mock()

        filters = CaseFilter()
        includes = ["case_role_department"]
        self.qry.search_case(
            page=1, page_size=10, filters=filters, includes=includes
        )

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 2

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, (SELECT roles.uuid \n"
            "FROM roles \n"
            "WHERE roles.id = zaak_1.route_role) AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_using_cursor_order_by_id(self):
        role_uuid = uuid4()
        role_row = mock.MagicMock()
        role_row.configure_mock(uuid=role_uuid, name="role")

        self.session.execute().fetchall.side_effect = (
            self.mock_db_rows,
            role_row,
        )
        self.session.reset_mock()
        cursor = "eyJwYWdlIjogIm5leHQiLCAicGFnZV9zaXplIjogMiwgImxhc3RfaWQiOiAyNCwgImxhc3RfaXRlbSI6ICIyNCJ9"

        filters = CaseFilter()
        includes = []
        self.qry.search_case(cursor=cursor, filters=filters, includes=includes)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND zaak_1.id < %(id_1)s ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s"
        )

        cursor = "eyJwYWdlIjogInByZXZpb3VzIiwgInBhZ2Vfc2l6ZSI6IDIsICJsYXN0X2lkIjogMjQsICJsYXN0X2l0ZW0iOiAiMjQifQ=="
        filters = CaseFilter()
        includes = []
        self.qry.search_case(cursor=cursor, filters=filters, includes=includes)

        call_list = self.session.execute.call_args_list

        select_statement = call_list[1][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT anon_1.id, anon_1.uuid, anon_1.status, anon_1.vernietigingsdatum, anon_1.archival_state, anon_1.registratiedatum, anon_1.afhandeldatum, anon_1.target_completion_date, anon_1.percentage_days_left, anon_1.onderwerp, anon_1.onderwerp_extern, anon_1.progress, anon_1.titel, anon_1.case_type_version, anon_1.unaccepted_files_count, anon_1.unread_communication_count, anon_1.unaccepted_attribute_update_count, anon_1.requestor_name, anon_1.requestor_type, anon_1.requestor_uuid, anon_1.assignee_name, anon_1.assignee_uuid, anon_1.coordinator_name, anon_1.coordinator_uuid, anon_1.custom_fields, anon_1.file_custom_fields, anon_1.period_of_preservation_active, anon_1.case_type_result, anon_1.case_location, anon_1.stalled_until, anon_1.contact_channel, anon_1.role \n"
            "FROM (SELECT zaak_1.id AS id, zaak_1.uuid AS uuid, zaak_1.status AS status, zaak_1.vernietigingsdatum AS vernietigingsdatum, zaak_1.archival_state AS archival_state, zaak_1.registratiedatum AS registratiedatum, zaak_1.afhandeldatum AS afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel AS titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count AS unaccepted_files_count, zaak_meta.unread_communication_count AS unread_communication_count, zaak_meta.unaccepted_attribute_update_count AS unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND zaak_1.id > %(id_1)s ORDER BY zaak_1.id ASC \n"
            " LIMIT %(param_3)s) AS anon_1 ORDER BY anon_1.id DESC"
        )

    def test_search_case_intake_filter_as_admin(self):
        self.user_info.permissions = {
            "admin": True,
            "zaak_route_default": True,
        }
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseFilter.parse_obj({"intake": True})
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND zaak_1.status = %(status_2)s AND zaak_1.registratiedatum <= timezone(%(timezone_1)s, now()) AND (zaak_betrokkenen_1.subject_id = %(subject_id_1)s::UUID OR zaak_1.behandelaar IS NULL) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_intake_filter_as_regular_user(self):
        self.user_info.permissions = {
            "admin": False,
            "zaak_route_default": False,
        }
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseFilter.parse_obj({"intake": True})
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id AND ((CAST(bibliotheek_kenmerken.properties AS JSON) ->> %(param_3)s) IS NULL OR (CAST(bibliotheek_kenmerken.properties AS JSON) ->> %(param_4)s) != %(param_5)s))) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND zaak_1.status = %(status_2)s AND zaak_1.registratiedatum <= timezone(%(timezone_1)s, now()) AND (zaak_betrokkenen_1.subject_id = %(subject_id_1)s::UUID OR zaak_1.behandelaar IS NULL AND zaak_1.route_ou IN (SELECT unnest(subject.group_ids) AS unnest_1 \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s::UUID) AND zaak_1.route_role IN (SELECT unnest(subject.role_ids) AS unnest_2 \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_3)s::UUID)) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_6)s OFFSET %(param_7)s"
        )

    def test_search_case_intake_filter_as_zaakbeheerder(self):
        self.user_info.permissions = {
            "admin": False,
            "zaak_route_default": True,
        }
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseFilter.parse_obj({"intake": True})
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id AND ((CAST(bibliotheek_kenmerken.properties AS JSON) ->> %(param_3)s) IS NULL OR (CAST(bibliotheek_kenmerken.properties AS JSON) ->> %(param_4)s) != %(param_5)s))) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND zaak_1.status = %(status_2)s AND zaak_1.registratiedatum <= timezone(%(timezone_1)s, now()) AND (zaak_betrokkenen_1.subject_id = %(subject_id_1)s::UUID OR zaak_1.behandelaar IS NULL) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_6)s OFFSET %(param_7)s"
        )

    def test_search_case_intake_filter_false(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseFilter.parse_obj({"intake": False})
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_custom_object_uuid_filter(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseFilter()
        filters.filter_custom_object_uuid = [uuid4(), uuid4()]
        filters.operator = "or"
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, SUBSTRING(zaak_1.onderwerp FROM %(substring_1)s FOR %(substring_2)s) AS onderwerp, SUBSTRING(zaak_1.onderwerp_extern FROM %(substring_3)s FOR %(substring_4)s) AS onderwerp_extern, CAST(zaak_1.milestone AS FLOAT) / CAST((SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS NUMERIC) AS progress, zaaktype_node.titel, zaaktype_node.uuid AS case_type_version, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields, zaaktype_resultaten_1.trigger_archival AS period_of_preservation_active, zaaktype_resultaten_1.uuid AS case_type_result, zaak_bag_1.uuid AS case_location, zaak_1.stalled_until AS stalled_until, zaak_1.contactkanaal AS contact_channel, NULL AS role \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID))) AND (EXISTS (SELECT 1 \n"
            "FROM custom_object_relationship, custom_object \n"
            "WHERE custom_object.uuid IN (%(uuid_2_1)s::UUID, %(uuid_2_2)s::UUID) AND custom_object_relationship.custom_object_id = custom_object.id AND custom_object_relationship.relationship_type = %(relationship_type_1)s AND custom_object_relationship.related_case_id = zaak_1.id)) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
import minty.entity
import minty.exceptions
import pytest
from minty.cqrs.test import TestBase
from sqlalchemy.dialects import postgresql
from unittest import mock
from uuid import UUID, uuid4
from zsnl_domains import case_management
from zsnl_domains.case_management.entities import dashboard as entity


class TestCreateDashboard(TestBase):
    def setup_method(self):
        user_uuid = uuid4()
        self.load_command_instance(case_management)
        self.cmd.user_info = minty.cqrs.UserInfo(
            user_uuid=user_uuid, permissions={"admin": True}
        )

    def test_create_dashboard_with_no_widgets(self):
        dashboard_uuid = str(self.cmd.user_info.user_uuid)
        widgets = None
        self.session.execute().fetchone.return_value = None
        self.cmd.update_dashboard(uuid=dashboard_uuid, widgets=widgets)

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 3
        # Insert dashboard
        dashboard_query = call_list[2][0][0].compile(
            dialect=postgresql.dialect()
        )

        assert (
            str(dashboard_query)
            == "DELETE FROM dashboard WHERE dashboard.uuid = %(uuid_1)s::UUID"
        )
        assert dashboard_query.params == {"uuid_1": dashboard_uuid}

    def test_create_dashboard_with_empty_widgets(self):
        dashboard_uuid = str(self.cmd.user_info.user_uuid)
        widgets = []
        self.session.execute().fetchone.return_value = None
        self.cmd.update_dashboard(uuid=dashboard_uuid, widgets=widgets)

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 3
        # Insert dashboard
        dashboard_query = call_list[2][0][0].compile(
            dialect=postgresql.dialect()
        )

        assert (
            str(dashboard_query)
            == "INSERT INTO dashboard (uuid, widgets) VALUES (%(uuid)s::UUID, %(widgets)s::JSONB) ON CONFLICT (uuid) DO UPDATE SET widgets = %(param_1)s::JSONB RETURNING dashboard.id"
        )
        assert dashboard_query.params == {
            "uuid": dashboard_uuid,
            "widgets": [],
            "param_1": [],
        }

    def test_create_dashboard_with_saved_search_widgets(self):
        dashboard_uuid = str(self.cmd.user_info.user_uuid)
        saved_search_id = str(uuid4())
        widgets = [
            {
                "dimensions": {"x": 8, "y": 1, "width": 10, "height": 20},
                "parameters": {
                    "type": "saved_search",
                    "saved_search_id": saved_search_id,
                    "page_size": 10,
                    "kind": "case",
                },
            }
        ]
        self.session.execute().fetchone.return_value = []
        self.cmd.update_dashboard(uuid=dashboard_uuid, widgets=widgets)

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 3

        # Insert dashboard
        dashboard_query = call_list[2][0][0].compile(
            dialect=postgresql.dialect()
        )

        assert (
            str(dashboard_query)
            == "INSERT INTO dashboard (uuid, widgets) VALUES (%(uuid)s::UUID, %(widgets)s::JSONB) ON CONFLICT (uuid) DO UPDATE SET widgets = %(param_1)s::JSONB RETURNING dashboard.id"
        )

        assert dashboard_query.params == {
            "uuid": dashboard_uuid,
            "widgets": [
                {
                    "dimensions": {"x": 8, "y": 1, "width": 10, "height": 20},
                    "parameters": {
                        "type": "saved_search",
                        "saved_search_id": saved_search_id,
                        "page_size": 10,
                        "kind": "case",
                    },
                }
            ],
            "param_1": [
                {
                    "dimensions": {"x": 8, "y": 1, "width": 10, "height": 20},
                    "parameters": {
                        "type": "saved_search",
                        "saved_search_id": saved_search_id,
                        "page_size": 10,
                        "kind": "case",
                    },
                }
            ],
        }

    def test_create_dashboard_with_task_widgets(self):
        dashboard_uuid = str(self.cmd.user_info.user_uuid)
        widgets = [
            {
                "dimensions": {"x": 0, "y": 1, "width": 12, "height": 20},
                "parameters": {
                    "type": "tasks",
                    "columns": [
                        {
                            "id": "display_number",
                            "value": "Zaaknummer",
                            "active": False,
                            "is_new": False,
                        }
                    ],
                    "filters": [
                        {
                            "label": "Admin",
                            "value": "398375e4-ed8b-469c-8f27-1f8da190beb1",
                            "data": {"type": "assignee"},
                        }
                    ],
                },
            }
        ]
        self.session.execute().fetchone.return_value = []
        self.cmd.update_dashboard(uuid=dashboard_uuid, widgets=widgets)

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 3

        # Insert dashboard
        dashboard_query = call_list[2][0][0].compile(
            dialect=postgresql.dialect()
        )

        assert (
            str(dashboard_query)
            == "INSERT INTO dashboard (uuid, widgets) VALUES (%(uuid)s::UUID, %(widgets)s::JSONB) ON CONFLICT (uuid) DO UPDATE SET widgets = %(param_1)s::JSONB RETURNING dashboard.id"
        )

        assert dashboard_query.params == {
            "uuid": dashboard_uuid,
            "widgets": [
                {
                    "dimensions": {"x": 0, "y": 1, "width": 12, "height": 20},
                    "parameters": {
                        "type": "tasks",
                        "columns": [
                            {
                                "id": "display_number",
                                "value": "Zaaknummer",
                                "active": False,
                                "is_new": False,
                            }
                        ],
                        "filters": [
                            {
                                "label": "Admin",
                                "value": "398375e4-ed8b-469c-8f27-1f8da190beb1",
                                "data": {"type": "assignee"},
                            }
                        ],
                    },
                }
            ],
            "param_1": [
                {
                    "dimensions": {"x": 0, "y": 1, "width": 12, "height": 20},
                    "parameters": {
                        "type": "tasks",
                        "columns": [
                            {
                                "id": "display_number",
                                "value": "Zaaknummer",
                                "active": False,
                                "is_new": False,
                            }
                        ],
                        "filters": [
                            {
                                "label": "Admin",
                                "value": "398375e4-ed8b-469c-8f27-1f8da190beb1",
                                "data": {"type": "assignee"},
                            }
                        ],
                    },
                }
            ],
        }

    def test_create_dashboard_with_favourit_case_type_widgets(self):
        dashboard_uuid = str(self.cmd.user_info.user_uuid)

        widgets = [
            {
                "dimensions": {"x": 8, "y": 1, "width": 10, "height": 20},
                "parameters": {
                    "type": "favorite_case_type",
                },
            }
        ]
        self.session.execute().fetchone.return_value = []
        self.cmd.update_dashboard(uuid=dashboard_uuid, widgets=widgets)

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 3

        # Insert dashboard
        dashboard_query = call_list[2][0][0].compile(
            dialect=postgresql.dialect()
        )

        assert (
            str(dashboard_query)
            == "INSERT INTO dashboard (uuid, widgets) VALUES (%(uuid)s::UUID, %(widgets)s::JSONB) ON CONFLICT (uuid) DO UPDATE SET widgets = %(param_1)s::JSONB RETURNING dashboard.id"
        )

        assert dashboard_query.params == {
            "uuid": dashboard_uuid,
            "widgets": [
                {
                    "dimensions": {"x": 8, "y": 1, "width": 10, "height": 20},
                    "parameters": {
                        "type": "favorite_case_type",
                    },
                }
            ],
            "param_1": [
                {
                    "dimensions": {"x": 8, "y": 1, "width": 10, "height": 20},
                    "parameters": {
                        "type": "favorite_case_type",
                    },
                }
            ],
        }

    def test_create_dashboard_with_external_url_widgets(self):
        dashboard_uuid = str(self.cmd.user_info.user_uuid)

        widgets = [
            {
                "dimensions": {"x": 8, "y": 1, "width": 10, "height": 20},
                "parameters": {
                    "type": "external_url",
                    "url": "https://xyz.com",
                    "title": "sample url",
                },
            }
        ]
        self.session.execute().fetchone.return_value = []
        self.cmd.update_dashboard(uuid=dashboard_uuid, widgets=widgets)

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 3

        # Insert dashboard
        dashboard_query = call_list[2][0][0].compile(
            dialect=postgresql.dialect()
        )

        assert (
            str(dashboard_query)
            == "INSERT INTO dashboard (uuid, widgets) VALUES (%(uuid)s::UUID, %(widgets)s::JSONB) ON CONFLICT (uuid) DO UPDATE SET widgets = %(param_1)s::JSONB RETURNING dashboard.id"
        )

        assert dashboard_query.params == {
            "uuid": dashboard_uuid,
            "widgets": [
                {
                    "dimensions": {"x": 8, "y": 1, "width": 10, "height": 20},
                    "parameters": {
                        "type": "external_url",
                        "url": "https://xyz.com",
                        "title": "sample url",
                    },
                }
            ],
            "param_1": [
                {
                    "dimensions": {"x": 8, "y": 1, "width": 10, "height": 20},
                    "parameters": {
                        "type": "external_url",
                        "url": "https://xyz.com",
                        "title": "sample url",
                    },
                }
            ],
        }

    def test_create_dashboard_with_multiple_widgets(self):
        dashboard_uuid = str(self.cmd.user_info.user_uuid)
        saved_search_id = str(uuid4())

        widgets = [
            {
                "dimensions": {"x": 8, "y": 1, "width": 10, "height": 20},
                "parameters": {
                    "type": "external_url",
                    "url": "https://xyz.com",
                    "title": "sample url",
                },
            },
            {
                "dimensions": {"x": 8, "y": 1, "width": 10, "height": 20},
                "parameters": {
                    "type": "saved_search",
                    "saved_search_id": saved_search_id,
                    "page_size": 10,
                    "kind": "custom_object",
                },
            },
        ]
        self.session.execute().fetchone.return_value = []
        self.cmd.update_dashboard(uuid=dashboard_uuid, widgets=widgets)

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 3

        # Insert dashboard
        dashboard_query = call_list[2][0][0].compile(
            dialect=postgresql.dialect()
        )

        assert (
            str(dashboard_query)
            == "INSERT INTO dashboard (uuid, widgets) VALUES (%(uuid)s::UUID, %(widgets)s::JSONB) ON CONFLICT (uuid) DO UPDATE SET widgets = %(param_1)s::JSONB RETURNING dashboard.id"
        )

        assert dashboard_query.params == {
            "uuid": dashboard_uuid,
            "widgets": [
                {
                    "dimensions": {"x": 8, "y": 1, "width": 10, "height": 20},
                    "parameters": {
                        "type": "external_url",
                        "url": "https://xyz.com",
                        "title": "sample url",
                    },
                },
                {
                    "dimensions": {"x": 8, "y": 1, "width": 10, "height": 20},
                    "parameters": {
                        "type": "saved_search",
                        "saved_search_id": saved_search_id,
                        "page_size": 10,
                        "kind": "custom_object",
                    },
                },
            ],
            "param_1": [
                {
                    "dimensions": {"x": 8, "y": 1, "width": 10, "height": 20},
                    "parameters": {
                        "type": "external_url",
                        "url": "https://xyz.com",
                        "title": "sample url",
                    },
                },
                {
                    "dimensions": {"x": 8, "y": 1, "width": 10, "height": 20},
                    "parameters": {
                        "type": "saved_search",
                        "saved_search_id": saved_search_id,
                        "page_size": 10,
                        "kind": "custom_object",
                    },
                },
            ],
        }

    def test_update_dashboard_incorrect_uuid(self):
        dashboard_uuid = str(uuid4())
        widgets = [
            {
                "dimensions": {"x": 8, "y": 1, "width": 10, "height": 20},
                "parameters": {
                    "type": "external_url",
                    "url": "https://xyz.com",
                    "title": "sample url",
                },
            }
        ]

        with pytest.raises(minty.exceptions.Forbidden) as excinfo:
            self.cmd.update_dashboard(uuid=dashboard_uuid, widgets=widgets)

        assert excinfo.value.args == (
            "Current user is not allowed to view or update this dashboard",
            "dashboard/action/not_allowed",
        )


class TestGetDashboard(TestBase):
    def setup_method(self):
        self.load_query_instance(case_management)
        user_uuid = uuid4()
        self.qry.user_info = minty.cqrs.UserInfo(
            user_uuid=user_uuid, permissions={"admin": True}
        )

    def test_get_dashboard(self):
        dashboard_uuid = str(self.qry.user_info.user_uuid)
        saved_search_uuid = str(uuid4())

        mock_db_row = mock.Mock()
        mock_db_row.configure_mock(
            uuid=dashboard_uuid,
            widgets=[
                {
                    "dimensions": {"x": 8, "y": 1, "width": 10, "height": 20},
                    "parameters": {
                        "page_size": 10,
                        "saved_search_id": saved_search_uuid,
                        "type": "saved_search",
                        "kind": "case",
                    },
                }
            ],
        )

        self.session.execute().fetchone.return_value = mock_db_row
        self.session.execute.reset_mock()

        dashboard = self.qry.get_dashboard()

        assert dashboard == entity.Dashboard.parse_obj(
            {
                "entity_id": UUID(dashboard_uuid),
                "uuid": UUID(dashboard_uuid),
                "widgets": [
                    {
                        "dimensions": {
                            "x": 8,
                            "y": 1,
                            "width": 10,
                            "height": 20,
                        },
                        "parameters": {
                            "page_size": 10,
                            "saved_search_id": UUID(saved_search_uuid),
                            "type": "saved_search",
                            "kind": "case",
                        },
                    }
                ],
                "_event_service": None,
            }
        )

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 1

        dashboard_query = call_list[0][0][0].compile(
            dialect=postgresql.dialect()
        )

        assert str(dashboard_query) == (
            "SELECT dashboard.uuid, dashboard.widgets \n"
            "FROM dashboard \n"
            "WHERE dashboard.uuid = %(uuid_1)s::UUID"
        )

        assert dashboard_query.params == {"uuid_1": UUID(dashboard_uuid)}

    def test_get_dashboard_initial(self):
        dashboard_uuid = self.qry.user_info.user_uuid
        self.session.execute().fetchone.return_value = None
        self.session.execute.reset_mock()

        dashboard = self.qry.get_dashboard()

        assert dashboard == entity.Dashboard.parse_obj(
            {
                "entity_id": dashboard_uuid,
                "uuid": dashboard_uuid,
                "widgets": None,
                "_event_service": None,
            }
        )

# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
import minty.cqrs.test
import pytest
from datetime import datetime
from minty.cqrs.events import Event, EventService
from minty.exceptions import Conflict, Forbidden, NotFound, ValidationError
from sqlalchemy.dialects import postgresql as sqlalchemy_pg
from unittest import mock
from uuid import uuid4
from zsnl_domains import case_management
from zsnl_domains.case_management import entities
from zsnl_domains.case_management.entities._shared import (
    ContactInformation,
    InternationalAddress,
    RelatedCustomObject,
)
from zsnl_domains.case_management.repositories.organization import (
    OrganizationRepository,
)
from zsnl_domains.case_management.repositories.person import PersonRepository


class Test_Retrieve_Organization(minty.cqrs.test.TestBase):
    def setup_method(self):
        amqp = {
            "amqp": mock.Mock(),
        }

        self.load_query_instance(case_management, amqp)

        self.qry.repository_factory.infrastructure_factory.get_config = (
            lambda context: {
                "instance_uuid": "test",
                "amqp": {"publish_settings": {"exchange": "dummy_exchange"}},
            }
        )

    def test_get_organization_by_uuid(self):
        org_uuid = uuid4()

        org_row = mock.Mock()
        org_row.configure_mock(
            uuid=org_uuid,
            authenticated=True,
            authenticatedby="Intern",
            name="Company Name",
            coc_number=1234567,
            coc_location_number="123456789012",
            date_founded=None,
            date_registered=None,
            date_ceased=None,
            rsin=None,
            oin=None,
            main_activity={"code": 123, "description": "Something"},
            secondary_activities=[{"code": 31337, "description": "Test"}],
            business_entity_code=1,  # Eenmanszaak
            location_address=None,
            correspondence_address=None,
            contact_information={},
            related_custom_object_uuid=None,
            preferred_contact_channel="pip",
            contact_person_info={
                "initials": "L",
                "insertions": "van",
                "family_name": "Langen",
            },
            summary="Company Name",
        )

        self.session.execute().fetchone.side_effect = [org_row]

        org = self.qry.get_organization_by_uuid(organization_uuid=org_uuid)

        assert "result" in org
        assert org["result"].uuid == org_uuid
        assert org["result"].name == "Company Name"

    def test_get_organization_by_uui_no_coc_number(self):
        org_uuid = uuid4()

        org_row = mock.Mock()
        org_row.configure_mock(
            uuid=org_uuid,
            authenticated=True,
            authenticatedby="Intern",
            name="Company Name",
            coc_number=None,
            coc_location_number="123456789012",
            date_founded=None,
            date_registered=None,
            date_ceased=None,
            rsin=None,
            oin=None,
            main_activity={"code": 123, "description": "Something"},
            secondary_activities=[{"code": 31337, "description": "Test"}],
            business_entity_code=1,  # Eenmanszaak
            location_address=None,
            correspondence_address=None,
            contact_information={},
            related_custom_object_uuid=None,
            preferred_contact_channel="pip",
            contact_person_info={
                "initials": "L",
                "insertions": "van",
                "family_name": "Langen",
            },
            summary="Company Name",
        )

        self.session.execute().fetchone.side_effect = [org_row]

        org = self.qry.get_organization_by_uuid(organization_uuid=org_uuid)

        assert "result" in org
        assert org["result"].uuid == org_uuid
        assert org["result"].name == "Company Name"

    def test_get_organization_by_uuid_notfound(self):
        org_uuid = uuid4()

        org_row = mock.Mock()
        org_row.configure_mock(
            uuid=org_uuid,
            authenticated=True,
            authenticatedby="Intern",
            name="Company Name",
            coc_number="12345678",
            coc_location_number="123456789012",
            business_entity_code=1,  # Eenmanszaak
            location_address=None,
            correspondence_address=None,
            contact_information={},
            related_custom_object_uuid=None,
            preferred_contact_channel="pip",
            contact_person_info={},
        )

        self.session.execute().fetchone.side_effect = [None]

        with pytest.raises(NotFound):
            self.qry.get_organization_by_uuid(organization_uuid=str(org_uuid))

    def test_get_organizations_by_uuid(self):
        org_uuids = [uuid4(), uuid4()]

        org_row1 = mock.Mock()
        org_row1.configure_mock(
            uuid=org_uuids[0],
            authenticated=True,
            authenticatedby="Intern",
            name="Company Name 1",
            coc_number="12345678",
            coc_location_number="123456789012",
            date_founded=None,
            date_registered=None,
            date_ceased=None,
            rsin=None,
            oin=None,
            main_activity={"code": 123, "description": "Something"},
            secondary_activities=[{"code": 31337, "description": "Test"}],
            business_entity_code=1,  # Eenmanszaak
            location_address={
                "street": "Test street",
                "street_number": 123,
                "street_number_letter": "Z",
                "street_number_suffix": None,
                "zipcode": "1234AB",
                "city": "Test City",
                "country": 6030,
                "bag_id": None,
            },
            correspondence_address=None,
            contact_information={},
            related_custom_object_uuid=None,
            preferred_contact_channel="pip",
            contact_person_info={},
            summary="Company Name 1",
        )
        org_row2 = mock.Mock()
        org_row2.configure_mock(
            uuid=org_uuids[1],
            authenticated=True,
            authenticatedby="Intern",
            name="Company Name 2",
            coc_number="12345679",
            coc_location_number="123456789013",
            date_founded=None,
            date_registered=None,
            date_ceased=None,
            rsin=None,
            oin=None,
            main_activity={"code": 123, "description": "Something"},
            secondary_activities=[{"code": 31337, "description": "Test"}],
            business_entity_code=1,  # Eenmanszaak
            location_address=None,
            correspondence_address={
                "street": "Test street",
                "street_number": 123,
                "street_number_letter": "Z",
                "street_number_suffix": None,
                "zipcode": "1234AB",
                "city": "Test City",
                "country": 6030,
            },
            contact_information={},
            related_custom_object_uuid=None,
            preferred_contact_channel="pip",
            contact_person_info={},
            summary="Company Name 2",
        )

        org_rows = [org_row1, org_row2]

        self.session.execute().fetchall.side_effect = [[*org_rows]]

        org = self.qry.get_organizations_by_uuid(
            organization_uuids=[str(org_uuids[0]), str(org_uuids[1])]
        )

        assert org[0].uuid == org_uuids[0]
        assert org[0].name == "Company Name 1"

        assert org[1].uuid == org_uuids[1]
        assert org[1].name == "Company Name 2"

    def test_get_contact_data_for_organization(self):
        org_uuid = uuid4()

        org_row = mock.Mock()
        org_row.configure_mock(
            uuid=org_uuid,
            authenticated=True,
            authenticatedby="Intern",
            name="Company Name",
            coc_number="12345678",
            coc_location_number="123456789012",
            date_founded=None,
            date_registered=None,
            date_ceased=None,
            rsin=None,
            oin=None,
            main_activity={"code": 123, "description": "Something"},
            secondary_activities=[{"code": 31337, "description": "Test"}],
            business_entity_code=1,  # Eenmanszaak
            location_address=None,
            correspondence_address=None,
            contact_information={},
            related_custom_object_uuid=None,
            preferred_contact_channel="pip",
            contact_person_info={},
            summary="Company Name",
        )

        self.session.execute().fetchone.side_effect = [org_row]

        org = self.qry.get_contact(type="organization", uuid=org_uuid)
        assert org.uuid == org_uuid
        assert org.name == "Company Name"

    def test_get_contact_data_for_organization_limited(self):
        org_uuid = uuid4()

        org_row = mock.Mock()
        org_row.configure_mock(
            uuid=org_uuid,
            authenticated=True,
            authenticatedby="Intern",
            name="Company Name",
            coc_number="12345678",
            coc_location_number="123456789012",
            date_founded=None,
            date_registered=None,
            date_ceased="01-01-2000",
            rsin=None,
            oin=None,
            main_activity={"code": 123, "description": "Something"},
            secondary_activities=[{"code": 31337, "description": "Test"}],
            business_entity_code=1,  # Eenmanszaak
            location_address={
                "street": "Lage Vuursche",
                "zipcode": "1234RT",
                "street_number": 3,
                "city": "Baarn",
                "landcode": 6030,
                "bag_id": "123",
            },
            correspondence_address={
                "street": "Lage Vuursche",
                "zipcode": "1234RT",
                "street_number": 3,
                "city": "Baarn",
                "landcode": 6030,
                "bag_id": "123",
            },
            contact_information={},
            related_custom_object_uuid=None,
            preferred_contact_channel="pip",
            contact_person_info={},
            summary="Company Name",
        )

        self.session.execute().fetchone.side_effect = [org_row]

        org = self.qry.get_contact_limited(type="organization", uuid=org_uuid)
        assert org.uuid == org_uuid


class Test_Retrieve_Person(minty.cqrs.test.TestBase):
    def setup_method(self):
        amqp = {
            "amqp": mock.Mock(),
        }

        self.load_query_instance(case_management, amqp)

        self.qry.repository_factory.infrastructure_factory.get_config = (
            lambda context: {
                "instance_uuid": "test",
                "amqp": {"publish_settings": {"exchange": "dummy_exchange"}},
            }
        )

    def test_get_person_by_uuid(self):
        person_uuid = uuid4()
        person_row = mock.Mock()
        person_row.configure_mock(
            uuid=person_uuid,
            authenticated=True,
            authenticatedby="source",
            date_of_birth=datetime(1918, 4, 26),
            date_of_death=datetime(2004, 1, 25),
            first_names="Fanny",
            family_name="Koen",
            initials=None,
            insertions="Blankers",
            surname="Koen",
            noble_title=None,
            name="Fanny Blankers-Koen",
            gender="Female",
            inside_municipality=True,
            properties={"anonymous": "0"},
            address={
                "street": "Lage Vuursche ",
                "zipcode": "1234RT",
                "street_number": 3,
                "city": "Baarn",
                "country_code": 6030,
                "bag_id": "1234123412341234",
                "is_foreign": False,
            },
            correspondence_address={
                "street": "Lage Vuursche ",
                "zipcode": "1234RT",
                "street_number": 3,
                "city": "Baarn",
                "country_code": 6030,
                "is_foreign": False,
            },
            contact_information={"email": "test@test.com"},
            related_custom_object_uuid=uuid4(),
            preferred_contact_channel="pip",
            summary="Fanny Blankers-Koen (Koen)",
            indicatie_geheim="1",
            external_identifier=None,
        )

        self.session.execute().fetchone.return_value = person_row

        person = self.qry.get_person_by_uuid(person_uuid=str(person_uuid))
        assert "result" in person
        assert person["result"].uuid == person_uuid
        assert person["result"].name == "Koen"

        # when person lives outside netherlands
        person_row.address = {
            "address_line_1": "Address line 1",
            "address_line_2": "Address line 2",
            "address_line_3": "Address line 3",
            "country_code": 5001,
            "bag_id": None,
            "is_foreign": True,
        }
        self.session.execute().fetchone.return_value = person_row

        person = self.qry.get_person_by_uuid(person_uuid=str(person_uuid))
        assert person["result"].residence_address.country == "Canada"

        # Curacao address
        person_row.address = {
            "street": "Lage Vuursche",
            "zipcode": "1234RT",
            "street_number": 3,
            "street_number_letter": "a",
            "city": "Baarn",
            "country_code": 5107,
            "bag_id": None,
        }
        self.session.execute().fetchone.return_value = person_row

        person = self.qry.get_person_by_uuid(person_uuid=str(person_uuid))
        assert person["result"].residence_address.country == "Curaçao"

        person = self.qry.get_person_by_uuid(person_uuid=str(person_uuid))
        assert person["result"].residence_address.country == "Curaçao"

        # when person has invalid property "anonymous"
        person_row.properties["anonymous"] = "test"
        self.session.execute().fetchone.return_value = person_row
        person = self.qry.get_person_by_uuid(person_uuid=str(person_uuid))
        assert (
            person["result"].contact_information.is_anonymous_contact is False
        )

    def test_get_person_by_uuid_notfound(self):
        self.session.execute().fetchone.return_value = None

        with pytest.raises(NotFound):
            self.qry.get_person_by_uuid(person_uuid=str(uuid4()))

    def test_get_persons_by_uuid(self):
        person_uuids = [uuid4(), uuid4(), uuid4()]

        person_row1 = mock.Mock()
        person_row1.configure_mock(
            uuid=person_uuids[0],
            authenticated=True,
            authenticatedby="source",
            date_of_birth=datetime(1918, 4, 26),
            date_of_death=datetime(2004, 1, 25),
            first_names="Fanny",
            family_name="Koen",
            initials=None,
            insertions="Blankers",
            surname="Koen",
            noble_title=None,
            name="Fanny Blankers-Koen",
            gender="V",
            inside_municipality=True,
            properties={"anonymous": "0"},
            address={
                "street": "Lage Vuursche ",
                "zipcode": "1234RT",
                "street_number": 3,
                "city": "Baarn",
                "country_code": 6030,
                "bag_id": "9876543210098765",
            },
            correspondence_address={
                "street": "Lage Vuursche ",
                "zipcode": "1234RT",
                "street_number": 3,
                "city": "Baarn",
                "country_code": 6030,
            },
            contact_information={"email": "test@test.com"},
            related_custom_object_uuid=uuid4(),
            preferred_contact_channel="pip",
            summary="Fanny Blankers-Koen",
            external_identifier=None,
        )

        person_row2 = mock.Mock()
        person_row2.configure_mock(
            uuid=person_uuids[1],
            authenticated=True,
            authenticatedby="source",
            date_of_birth=datetime(1966, 2, 9),
            date_of_death=None,
            first_names="Ellen",
            family_name="Langen",
            initials=None,
            insertions="van",
            surname="Langen",
            noble_title=None,
            name="Ellen van Langen",
            gender="M",
            inside_municipality=True,
            properties={"anonymous": "1"},
            address={
                "street": "Oldenzaal",
                "zipcode": "1234RT",
                "street_number": 3,
                "city": "Overijssel",
                "country_code": 6030,
                "bag_id": None,
            },
            correspondence_address={
                "street": "Oldenzaal",
                "zipcode": "1234RT",
                "street_number": 3,
                "city": "Overijssel",
                "country_code": 6030,
            },
            contact_information={"email": "test@test.com"},
            related_custom_object_uuid=uuid4(),
            preferred_contact_channel="pip",
            summary="Ellen van Langen",
            external_identifier=None,
        )

        person_row3 = mock.Mock()
        person_row3.configure_mock(
            uuid=person_uuids[1],
            authenticated=True,
            authenticatedby="source",
            date_of_birth=datetime(1966, 2, 9),
            date_of_death=None,
            first_names="Ellen",
            family_name="Langen",
            initials=None,
            insertions="van",
            surname="Langen",
            noble_title=None,
            name="Ellen van Langen",
            gender="X",
            inside_municipality=True,
            properties={"anonymous": "1"},
            address={
                "street": "Oldenzaal",
                "zipcode": "1234RT",
                "street_number": 3,
                "city": "Overijssel",
                "country_code": 5107,
                "bag_id": None,
            },
            correspondence_address={
                "street": "Oldenzaal",
                "zipcode": "1234RT",
                "street_number": 3,
                "city": "Overijssel",
                "country_code": 5107,
            },
            contact_information={"email": "test@test.com"},
            related_custom_object_uuid=uuid4(),
            preferred_contact_channel="pip",
            summary="Ellen van Langen",
            external_identifier=None,
        )

        person_rows = [person_row1, person_row2, person_row3]

        self.session.execute().fetchall.return_value = person_rows

        org = self.qry.get_persons_by_uuid(
            person_uuids=[
                str(person_uuids[0]),
                str(person_uuids[1]),
                str(person_uuids[2]),
            ]
        )

        assert org[0].uuid == person_uuids[0]
        assert org[0].name == "Koen"
        assert org[0].gender == "F"

        assert org[1].uuid == person_uuids[1]
        assert org[1].name == "Langen"
        assert org[1].gender == "M"

        assert org[2].gender == "X"

    def test_get_contact_data_for_person(self):
        person_uuid = uuid4()

        person_row = mock.Mock()
        person_row.configure_mock(
            uuid=person_uuid,
            authenticated=True,
            authenticatedby="source",
            date_of_birth=datetime(1918, 4, 26),
            date_of_death=datetime(2004, 1, 25),
            first_names="Fanny",
            family_name="Koen",
            initials=None,
            insertions="Blankers",
            surname="Koen",
            noble_title=None,
            name="Fanny Blankers-Koen",
            gender="Female",
            inside_municipality=True,
            properties={"anonymous": "0"},
            address={
                "street": "Lage Vuursche",
                "zipcode": "1234RT",
                "street_number": 3,
                "city": "Baarn",
                "country_code": 6030,
                "bag_id": None,
            },
            correspondence_address={
                "street": "Lage Vuursche",
                "zipcode": "1234RT",
                "street_number": 3,
                "city": "Baarn",
                "country_code": 6030,
            },
            contact_information={"email": "test@test.com"},
            related_custom_object_uuid=uuid4(),
            preferred_contact_channel="pip",
            summary="Fanny Blankers-Koen",
            indicatie_geheim=None,
            external_identifier=None,
        )

        self.session.execute().fetchone.return_value = person_row

        result = self.qry.get_contact(type="person", uuid=person_uuid)
        assert result.uuid == person_uuid

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile(dialect=sqlalchemy_pg.dialect())

        assert (
            str(query)
            == "SELECT natuurlijk_persoon.uuid, natuurlijk_persoon.authenticated, natuurlijk_persoon.authenticatedby, natuurlijk_persoon.voornamen AS first_names, natuurlijk_persoon.voorletters AS initials, natuurlijk_persoon.voorvoegsel AS insertions, natuurlijk_persoon.geslachtsnaam AS family_name, natuurlijk_persoon.adellijke_titel AS noble_title, natuurlijk_persoon.surname AS surname, natuurlijk_persoon.geboortedatum AS date_of_birth, natuurlijk_persoon.datum_overlijden AS date_of_death, natuurlijk_persoon.geslachtsaanduiding AS gender, natuurlijk_persoon.in_gemeente AS inside_municipality, natuurlijk_persoon.landcode, natuurlijk_persoon.preferred_contact_channel, natuurlijk_persoon.active AS active, (SELECT json_build_object(%(json_build_object_2)s, contact_data.email, %(json_build_object_3)s, contact_data.telefoonnummer, %(json_build_object_4)s, contact_data.mobiel, %(json_build_object_5)s, contact_data.note) AS json_build_object_1 \n"
            "FROM contact_data \n"
            "WHERE contact_data.gegevens_magazijn_id = natuurlijk_persoon.id AND contact_data.betrokkene_type = %(betrokkene_type_1)s) AS contact_information, (SELECT CASE WHEN (adres_1.straatnaam != %(straatnaam_1)s AND (adres_1.landcode = %(landcode_1)s OR adres_1.landcode = %(landcode_2)s)) THEN json_build_object(%(json_build_object_6)s, adres_1.straatnaam, %(json_build_object_7)s, adres_1.postcode, %(json_build_object_8)s, adres_1.huisnummer, %(json_build_object_9)s, adres_1.huisletter, %(json_build_object_10)s, adres_1.huisnummertoevoeging, %(json_build_object_11)s, adres_1.woonplaats, %(json_build_object_12)s, adres_1.landcode, %(json_build_object_13)s, adres_1.bag_id, %(json_build_object_14)s, %(json_build_object_15)s, %(json_build_object_16)s, adres_1.geo_lat_long) ELSE json_build_object(%(json_build_object_17)s, adres_1.landcode, %(json_build_object_18)s, adres_1.adres_buitenland1, %(json_build_object_19)s, adres_1.adres_buitenland2, %(json_build_object_20)s, adres_1.adres_buitenland3, %(json_build_object_21)s, NULL, %(json_build_object_22)s, %(json_build_object_23)s, %(json_build_object_24)s, adres_1.geo_lat_long) END AS anon_1 \n"
            "FROM adres AS adres_1 \n"
            "WHERE adres_1.natuurlijk_persoon_id = natuurlijk_persoon.id AND adres_1.functie_adres = %(functie_adres_1)s) AS address, (SELECT CASE WHEN (adres_2.straatnaam != %(straatnaam_2)s AND (adres_2.landcode = %(landcode_3)s OR adres_2.landcode = %(landcode_4)s)) THEN json_build_object(%(json_build_object_25)s, adres_2.straatnaam, %(json_build_object_26)s, adres_2.postcode, %(json_build_object_27)s, adres_2.huisnummer, %(json_build_object_28)s, adres_2.huisletter, %(json_build_object_29)s, adres_2.huisnummertoevoeging, %(json_build_object_30)s, adres_2.woonplaats, %(json_build_object_31)s, adres_2.landcode, %(json_build_object_32)s, adres_2.bag_id, %(json_build_object_33)s, %(json_build_object_34)s, %(json_build_object_35)s, adres_2.geo_lat_long) ELSE json_build_object(%(json_build_object_36)s, adres_2.landcode, %(json_build_object_37)s, adres_2.adres_buitenland1, %(json_build_object_38)s, adres_2.adres_buitenland2, %(json_build_object_39)s, adres_2.adres_buitenland3, %(json_build_object_40)s, NULL, %(json_build_object_41)s, %(json_build_object_42)s, %(json_build_object_43)s, adres_2.geo_lat_long) END AS anon_2 \n"
            "FROM adres AS adres_2 \n"
            "WHERE adres_2.natuurlijk_persoon_id = natuurlijk_persoon.id AND adres_2.functie_adres = %(functie_adres_2)s) AS correspondence_address, natuurlijk_persoon.indicatie_geheim AS indicatie_geheim, custom_object.uuid AS related_custom_object_uuid, subject.properties, concat(natuurlijk_persoon.adellijke_titel || %(adellijke_titel_1)s, natuurlijk_persoon.voorletters || %(voorletters_1)s, natuurlijk_persoon.surname) AS summary, object_subscription.external_id AS external_identifier \n"
            "FROM natuurlijk_persoon LEFT OUTER JOIN custom_object ON natuurlijk_persoon.related_custom_object_id = custom_object.id LEFT OUTER JOIN subject ON subject.uuid = natuurlijk_persoon.uuid AND subject.subject_type = %(subject_type_1)s LEFT OUTER JOIN object_subscription ON natuurlijk_persoon.id = CAST(object_subscription.local_id AS INTEGER) AND object_subscription.local_table = %(local_table_1)s AND object_subscription.date_deleted IS NULL \n"
            "WHERE natuurlijk_persoon.deleted_on IS NULL AND natuurlijk_persoon.uuid = %(uuid_1)s::UUID"
        )

        # Test get_contact data for person without address
        person_row.address = None
        person_row.correspondence_address = None
        result = self.qry.get_contact(type="person", uuid=person_uuid)
        assert result.residence_address is None
        assert result.correspondence_address is None

    def test_get_contact_limited_data_for_person(self):
        person_uuid = uuid4()

        person_row = mock.Mock()
        person_row.configure_mock(
            uuid=person_uuid,
            authenticated=True,
            authenticatedby="source",
            date_of_birth=datetime(1918, 4, 26),
            date_of_death=datetime(2004, 1, 25),
            first_names="Fanny",
            family_name="Koen",
            initials=None,
            insertions="Blankers",
            surname="Koen",
            noble_title=None,
            name="Fanny Blankers-Koen",
            gender="Female",
            inside_municipality=True,
            properties={"anonymous": "0"},
            address={
                "street": "Lage Vuursche",
                "zipcode": "1234RT",
                "street_number": 3,
                "city": "Baarn",
                "landcode": 6030,
                "bag_id": "1234123412341234",
            },
            correspondence_address={
                "street": "Lage Vuursche",
                "zipcode": "1234RT",
                "street_number": 3,
                "city": "Baarn",
                "landcode": 6030,
            },
            contact_information={"email": "test@test.com"},
            related_custom_object_uuid=uuid4(),
            preferred_contact_channel="pip",
            summary="Fanny Blankers-Koen",
            indicatie_geheim=None,
            external_identifier=None,
        )

        self.session.execute().fetchone.return_value = person_row

        result = self.qry.get_contact_limited(type="person", uuid=person_uuid)
        assert result.uuid == person_uuid

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile(dialect=sqlalchemy_pg.dialect())

        assert (
            str(query)
            == "SELECT natuurlijk_persoon.uuid, natuurlijk_persoon.datum_overlijden AS date_of_death, natuurlijk_persoon.active AS active, (SELECT CASE WHEN (adres_1.straatnaam != %(straatnaam_1)s AND (adres_1.landcode = %(landcode_1)s OR adres_1.landcode = %(landcode_2)s)) THEN json_build_object(%(json_build_object_1)s, adres_1.straatnaam, %(json_build_object_2)s, adres_1.postcode, %(json_build_object_3)s, adres_1.huisnummer, %(json_build_object_4)s, adres_1.huisletter, %(json_build_object_5)s, adres_1.huisnummertoevoeging, %(json_build_object_6)s, adres_1.woonplaats, %(json_build_object_7)s, adres_1.landcode, %(json_build_object_8)s, adres_1.bag_id, %(json_build_object_9)s, %(json_build_object_10)s, %(json_build_object_11)s, adres_1.geo_lat_long) ELSE json_build_object(%(json_build_object_12)s, adres_1.landcode, %(json_build_object_13)s, adres_1.adres_buitenland1, %(json_build_object_14)s, adres_1.adres_buitenland2, %(json_build_object_15)s, adres_1.adres_buitenland3, %(json_build_object_16)s, NULL, %(json_build_object_17)s, %(json_build_object_18)s, %(json_build_object_19)s, adres_1.geo_lat_long) END AS anon_1 \n"
            "FROM adres AS adres_1 \n"
            "WHERE adres_1.natuurlijk_persoon_id = natuurlijk_persoon.id AND adres_1.functie_adres = %(functie_adres_1)s) AS address, (SELECT CASE WHEN (adres_2.straatnaam != %(straatnaam_2)s AND (adres_2.landcode = %(landcode_3)s OR adres_2.landcode = %(landcode_4)s)) THEN json_build_object(%(json_build_object_20)s, adres_2.straatnaam, %(json_build_object_21)s, adres_2.postcode, %(json_build_object_22)s, adres_2.huisnummer, %(json_build_object_23)s, adres_2.huisletter, %(json_build_object_24)s, adres_2.huisnummertoevoeging, %(json_build_object_25)s, adres_2.woonplaats, %(json_build_object_26)s, adres_2.landcode, %(json_build_object_27)s, adres_2.bag_id, %(json_build_object_28)s, %(json_build_object_29)s, %(json_build_object_30)s, adres_2.geo_lat_long) ELSE json_build_object(%(json_build_object_31)s, adres_2.landcode, %(json_build_object_32)s, adres_2.adres_buitenland1, %(json_build_object_33)s, adres_2.adres_buitenland2, %(json_build_object_34)s, adres_2.adres_buitenland3, %(json_build_object_35)s, NULL, %(json_build_object_36)s, %(json_build_object_37)s, %(json_build_object_38)s, adres_2.geo_lat_long) END AS anon_2 \n"
            "FROM adres AS adres_2 \n"
            "WHERE adres_2.natuurlijk_persoon_id = natuurlijk_persoon.id AND adres_2.functie_adres = %(functie_adres_2)s) AS correspondence_address, natuurlijk_persoon.indicatie_geheim AS indicatie_geheim, concat(natuurlijk_persoon.adellijke_titel || %(adellijke_titel_1)s, natuurlijk_persoon.voorletters || %(voorletters_1)s, natuurlijk_persoon.surname) AS summary, natuurlijk_persoon.onderzoek_persoon, natuurlijk_persoon.onderzoek_huwelijk, natuurlijk_persoon.onderzoek_overlijden, natuurlijk_persoon.onderzoek_verblijfplaats \n"
            "FROM natuurlijk_persoon \n"
            "WHERE natuurlijk_persoon.deleted_on IS NULL AND natuurlijk_persoon.uuid = %(uuid_1)s::UUID"
        )

        # Test get_contact data for person without address
        person_row.address = None
        person_row.correspondence_address = None
        result = self.qry.get_contact_limited(type="person", uuid=person_uuid)
        assert result.has_valid_address is False
        assert result.has_correspondence_address is False


class Test_Retrieve_Employee(minty.cqrs.test.TestBase):
    def setup_method(self):
        amqp = {
            "amqp": mock.Mock(),
        }

        self.load_query_instance(case_management, amqp)

        self.qry.repository_factory.infrastructure_factory.get_config = (
            lambda context: {
                "instance_uuid": "test",
                "amqp": {"publish_settings": {"exchange": "dummy_exchange"}},
            }
        )

    def test_get_employee_by_uuid(self):
        employee_uuid = uuid4()
        employee_row = mock.Mock()
        employee_row.configure_mock(
            uuid=employee_uuid,
            active=True,
            surname="",
            title=None,
            name="beheerder",
            first_name="beheerder",
            department={"uuid": uuid4(), "name": "Back office"},
            roles=[
                {
                    "name": "admin",
                    "uuid": uuid4(),
                    "department": {"name": "Back office", "uuid": uuid4()},
                }
            ],
            contact_information={"email": "test@test.com"},
            related_custom_object_uuid=uuid4(),
            summary="beheerder",
        )

        self.session.execute().fetchone.return_value = employee_row

        employee = self.qry.get_employee_by_uuid(
            employee_uuid=str(employee_uuid)
        )
        assert "result" in employee
        assert employee["result"].uuid == employee_uuid
        assert employee["result"].name == "beheerder"

    def test_get_employee_by_uuid_notfound(self):
        self.session.execute().fetchone.return_value = None

        with pytest.raises(NotFound):
            self.qry.get_employee_by_uuid(employee_uuid=str(uuid4()))

    def test_get_employees_by_uuid(self):
        employee_uuids = [uuid4(), uuid4()]

        employee_row1 = mock.Mock()
        employee_row1.configure_mock(
            uuid=employee_uuids[0],
            active=True,
            surname="",
            title=None,
            first_name="beheerder",
            name="beheerder",
            department={"uuid": uuid4(), "name": "Back office"},
            roles=[
                {
                    "name": "admin",
                    "uuid": uuid4(),
                    "department": {"name": "Back office", "uuid": uuid4()},
                }
            ],
            contact_information={"email": "test@test.com"},
            related_custom_object_uuid=uuid4(),
            summary="beheerder",
        )

        employee_row2 = mock.Mock()
        employee_row2.configure_mock(
            uuid=employee_uuids[1],
            active=True,
            surname="",
            title=None,
            first_name="testgebruiker",
            name="testgebruiker",
            department={"uuid": uuid4(), "name": "Back office"},
            roles=[
                {
                    "name": "admin",
                    "uuid": uuid4(),
                    "department": {"name": "Back office", "uuid": uuid4()},
                }
            ],
            contact_information={"email": "test@test.com"},
            related_custom_object_uuid=uuid4(),
            summary="testgebruiker",
        )

        employee_rows = [employee_row1, employee_row2]

        self.session.execute().fetchall.return_value = employee_rows

        employees = self.qry.get_employees_by_uuid(
            employee_uuids=[str(employee_uuids[0]), str(employee_uuids[1])]
        )

        assert employees[0].uuid == employee_uuids[0]
        assert employees[0].name == "beheerder"

        assert employees[1].uuid == employee_uuids[1]
        assert employees[1].name == "testgebruiker"

    def test_get_contact_data_for_employee(self):
        employee_uuid = uuid4()

        employee_row = mock.Mock()
        employee_row.configure_mock(
            uuid=employee_uuid,
            active=True,
            surname="",
            title=None,
            first_name="beheerder",
            name="testgebruiker",
            gender="Female",
            inside_municipality=True,
            properties={"anonymous": "0"},
            department={"uuid": uuid4(), "name": "Back office"},
            roles=[
                {
                    "name": "admin",
                    "uuid": uuid4(),
                    "department": {"name": "Back office", "uuid": uuid4()},
                }
            ],
            contact_information={"email": "test@test.com"},
            related_custom_object_uuid=uuid4(),
            summary="beheerder",
        )

        self.session.execute().fetchone.return_value = employee_row

        result = self.qry.get_contact(type="employee", uuid=employee_uuid)
        assert result.uuid == employee_uuid

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile()

        assert (
            str(query)
            == "SELECT subject.uuid, subject.username AS name, coalesce(CAST(subject.properties AS JSON)[:param_1], NULL) AS first_name, coalesce(CAST(subject.properties AS JSON)[:param_2], NULL) AS surname, coalesce(CAST(subject.properties AS JSON)[:param_3], NULL) AS title, json_build_object(:json_build_object_1, coalesce(CAST(subject.properties AS JSON)[:param_4], NULL), :json_build_object_2, coalesce(CAST(subject.properties AS JSON)[:param_5], NULL)) AS contact_information, (SELECT coalesce(user_entity.active, :coalesce_2) AS coalesce_1 \n"
            "FROM user_entity \n"
            "WHERE user_entity.subject_id = subject.id\n"
            " LIMIT :param_6) AS active, (SELECT json_build_object(:json_build_object_4, groups.uuid, :json_build_object_5, groups.name) AS json_build_object_3 \n"
            "FROM groups \n"
            "WHERE groups.id = subject.group_ids[:group_ids_1]) AS department, array((SELECT json_build_object(:json_build_object_7, roles.uuid, :json_build_object_8, roles.name, :json_build_object_9, json_build_object(:json_build_object_10, groups_1.uuid, :json_build_object_11, groups_1.name)) AS json_build_object_6 \n"
            "FROM subject AS subject_1 JOIN LATERAL json_array_elements_text(array_to_json(subject_1.role_ids)) AS role_id ON subject_1.uuid = subject.uuid JOIN roles ON roles.id = CAST(role_id AS INTEGER) JOIN groups AS groups_1 ON groups_1.id = roles.parent_group_id)) AS roles, custom_object.uuid AS related_custom_object_uuid, CAST(subject.properties AS JSON) ->> :param_7 AS summary \n"
            "FROM subject LEFT OUTER JOIN custom_object ON subject.related_custom_object_id = custom_object.id \n"
            "WHERE subject.uuid = :uuid_1 AND subject.subject_type = :subject_type_1"
        )

    def test_get_contact_limited_data_for_employee(self):
        employee_uuid = uuid4()

        employee_row = mock.Mock()
        employee_row.configure_mock(
            uuid=employee_uuid,
            active=True,
            surname="",
            title=None,
            first_name="beheerder",
            name="testgebruiker",
            gender="Female",
            inside_municipality=True,
            properties={"anonymous": "0"},
            department={"uuid": uuid4(), "name": "Back office"},
            roles=[
                {
                    "name": "admin",
                    "uuid": uuid4(),
                    "department": {"name": "Back office", "uuid": uuid4()},
                }
            ],
            contact_information={"email": "test@test.com"},
            related_custom_object_uuid=uuid4(),
            summary="beheerder",
        )

        self.session.execute().fetchone.return_value = employee_row

        result = self.qry.get_contact_limited(
            type="employee", uuid=employee_uuid
        )
        assert result.uuid == employee_uuid

    def test_get_contact_settings_as_admin(self):
        employee_uuid = uuid4()
        signature_uuid = uuid4()
        self.qry.user_info = minty.cqrs.UserInfo(
            user_uuid=uuid4(), permissions={"admin": True}
        )
        employee_settings_row = mock.Mock()
        employee_settings_row.configure_mock(
            employee_uuid=employee_uuid,
            notification_settings={
                "new_document": True,
                "new_assigned_case": None,
                "case_suspension_term_exceeded": True,
                "new_attribute_proposal": False,
                "new_ext_pip_message": False,
                "case_term_exceeded": False,
            },
            signature_uuid=signature_uuid,
            phone_extension="123",
        )

        self.session.execute().fetchone.return_value = employee_settings_row

        result = self.qry.get_contact_settings(uuid=str(employee_uuid))
        assert result.notification_settings.new_document is True
        assert result.notification_settings.new_assigned_case is False
        assert result.signature.entity_id == signature_uuid
        assert result.phone_extension == "123"

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile()

        assert (
            str(query)
            == "SELECT subject.uuid AS employee_uuid, CAST(subject.settings AS JSON)[:param_1] AS notification_settings, (CAST(subject.settings AS JSON)[:param_2]) ->> :param_3 AS phone_extension, filestore.uuid AS signature_uuid \n"
            "FROM subject LEFT OUTER JOIN filestore ON filestore.id = CAST((CAST(subject.settings AS JSON) ->> :param_4) AS INTEGER) \n"
            "WHERE subject.uuid = :uuid_1 AND subject.subject_type = :subject_type_1"
        )

    def test_get_contact_settings_as_non_admin(self):
        employee_uuid = uuid4()
        signature_uuid = uuid4()
        self.qry.user_info = minty.cqrs.UserInfo(
            user_uuid=employee_uuid, permissions={}
        )
        employee_settings_row = mock.Mock()
        employee_settings_row.configure_mock(
            employee_uuid=employee_uuid,
            notification_settings={
                "new_document": True,
                "new_assigned_case": True,
                "case_suspension_term_exceeded": True,
                "new_attribute_proposal": False,
                "new_ext_pip_message": False,
                "case_term_exceeded": False,
            },
            signature_uuid=signature_uuid,
            phone_extension="123",
        )

        self.session.execute().fetchone.return_value = employee_settings_row

        result = self.qry.get_contact_settings(uuid=str(employee_uuid))
        assert result.notification_settings.new_document is True
        assert result.notification_settings.new_assigned_case is True
        assert result.signature.entity_id == signature_uuid
        assert result.phone_extension == "123"

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile()

        assert (
            str(query)
            == "SELECT subject.uuid AS employee_uuid, CAST(subject.settings AS JSON)[:param_1] AS notification_settings, (CAST(subject.settings AS JSON)[:param_2]) ->> :param_3 AS phone_extension, filestore.uuid AS signature_uuid \n"
            "FROM subject LEFT OUTER JOIN filestore ON filestore.id = CAST((CAST(subject.settings AS JSON) ->> :param_4) AS INTEGER) \n"
            "WHERE subject.uuid = :uuid_1 AND subject.subject_type = :subject_type_1"
        )

    def test_get_contact_settings_notfound(self):
        employee_uuid = uuid4()
        self.qry.user_info = minty.cqrs.UserInfo(
            user_uuid=employee_uuid, permissions={"admin": True}
        )
        self.session.execute().fetchone.return_value = None

        with pytest.raises(NotFound) as excinfo:
            self.qry.get_contact_settings(uuid=str(employee_uuid))

        assert excinfo.value.args == (
            f"No notification settings found for employee with uuid '{employee_uuid}'",
            "employee_settings/not_found",
        )

    def test_get_contact_settings_user_has_no_rights(self):
        employee_uuid = uuid4()
        signature_uuid = uuid4()
        self.qry.user_info = minty.cqrs.UserInfo(
            user_uuid=uuid4(), permissions={"admin": False}
        )
        employee_settings_row = mock.Mock()
        employee_settings_row.configure_mock(
            employee_uuid=employee_uuid,
            notification_settings={
                "new_document": True,
                "new_assigned_case": True,
                "case_suspension_term_exceeded": True,
                "new_attribute_proposal": False,
                "new_ext_pip_message": False,
                "case_term_exceeded": False,
            },
            signature_uuid=signature_uuid,
            phone_extension="1234",
        )
        self.session.execute().fetchone.return_value = employee_settings_row

        employee_settings = self.qry.get_contact_settings(
            uuid=str(employee_uuid)
        )
        assert employee_settings.notification_settings is None


class Test_Save_Contact_Settings(minty.cqrs.test.TestBase):
    def setup_method(self):
        self.load_command_instance(case_management)

    def test_set_notification_settings(self):
        employee_uuid = uuid4()
        self.cmd.user_info = minty.cqrs.UserInfo(
            user_uuid=employee_uuid, permissions={"admin": True}
        )

        employee_settings_row = mock.Mock()
        employee_settings_row.configure_mock(
            employee_uuid=employee_uuid,
            notification_settings={
                "new_document": True,
                "new_assigned_case": True,
                "case_suspension_term_exceeded": True,
                "new_attribute_proposal": False,
                "new_ext_pip_message": False,
                "case_term_exceeded": False,
            },
            signature_uuid=uuid4(),
            phone_extension=None,
        )
        mock_get_contact_settings = mock.Mock()
        mock_get_contact_settings.fetchone.return_value = employee_settings_row

        mock_get_employee_settings = mock.Mock()
        mock_get_employee_settings.fetchone.return_value = ({},)

        mock_update_employee_settings = mock.Mock()

        mock_rows = [
            mock_get_contact_settings,
            mock_get_employee_settings,
            mock_update_employee_settings,
        ]

        self.execute_queries = []

        def mock_execute(query):
            self.execute_queries.append(query)
            return mock_rows.pop(0)

        self.session.execute = mock_execute

        notification_settings = {
            "new_document": True,
            "new_assigned_case": True,
            "case_term_exceeded": True,
            "new_ext_pip_message": True,
            "new_attribute_proposal": True,
            "case_suspension_term_exceeded": True,
        }
        self.cmd.set_notification_settings(
            uuid=employee_uuid, notification_settings=notification_settings
        )

        update_settings_statement = self.execute_queries[2]
        compiled_update = update_settings_statement.compile(
            dialect=sqlalchemy_pg.dialect()
        )
        assert compiled_update.params == {
            "settings": {
                "notifications": {
                    "new_document": True,
                    "new_assigned_case": True,
                    "case_term_exceeded": True,
                    "new_ext_pip_message": True,
                    "new_attribute_proposal": True,
                    "case_suspension_term_exceeded": True,
                }
            },
            "subject_type_1": "employee",
            "uuid_1": str(employee_uuid),
        }
        assert (
            str(compiled_update)
            == "UPDATE subject SET settings=%(settings)s WHERE subject.uuid = %(uuid_1)s::UUID AND subject.subject_type = %(subject_type_1)s"
        )

    def test_set_notification_settings_user_has_no_rights(self):
        employee_uuid = uuid4()
        self.cmd.user_info = minty.cqrs.UserInfo(
            user_uuid=uuid4(), permissions={"admin": False}
        )

        with pytest.raises(Forbidden) as excinfo:
            self.cmd.set_notification_settings(
                uuid=employee_uuid, notification_settings={}
            )

        assert excinfo.value.args == (
            f"You don't have enough rights to set notification_settings for employee with uuid '{employee_uuid!s}'",
            "contact/set_notification_settings/permission_denied",
        )


class Test_Save_Signature(minty.cqrs.test.TestBase):
    def setup_method(self):
        self.load_command_instance(case_management)

    def test_save_signature(self):
        employee_uuid = uuid4()
        signature_filestore_uuid = uuid4()

        self.cmd.user_info = minty.cqrs.UserInfo(
            user_uuid=employee_uuid, permissions={"admin": True}
        )

        employee_settings_row = mock.Mock()
        employee_settings_row.configure_mock(
            employee_uuid=employee_uuid,
            notification_settings={
                "new_document": True,
                "new_assigned_case": True,
                "case_suspension_term_exceeded": True,
                "new_attribute_proposal": False,
                "new_ext_pip_message": False,
                "case_term_exceeded": False,
            },
            signature_uuid=None,
            phone_extension=None,
        )
        mock_get_contact_settings = mock.Mock()
        mock_get_contact_settings.fetchone.return_value = employee_settings_row

        mock_get_signature_upload_role = mock.Mock()
        mock_get_signature_upload_role.fetchone.return_value = ("Behandelaar",)

        filestore_row = mock.Mock()
        filestore_row.configure_mock(id=1, mimetype="image/jpeg")
        mock_get_filestore = mock.Mock()
        mock_get_filestore.fetchone.return_value = filestore_row

        mock_update_employee_settings = mock.Mock()

        mock_rows = [
            mock_get_signature_upload_role,
            mock_get_contact_settings,
            mock_get_filestore,
            mock_update_employee_settings,
        ]

        self.execute_queries = []

        def mock_execute(query):
            self.execute_queries.append(
                query.compile(dialect=sqlalchemy_pg.dialect())
            )
            return mock_rows.pop(0)

        self.session.execute = mock_execute
        self.cmd.save_signature(
            uuid=str(employee_uuid),
            file_uuid=str(signature_filestore_uuid),
        )

        update_settings_statement = self.execute_queries[3]
        assert update_settings_statement.params == {
            "param_1": {"signature_filestore_id": 1},
            "subject_type_1": "employee",
            "uuid_1": str(employee_uuid),
        }
        assert (
            str(update_settings_statement)
            == "UPDATE subject SET settings=(CAST(subject.settings AS JSONB) || %(param_1)s::JSONB) WHERE subject.uuid = %(uuid_1)s::UUID AND subject.subject_type = %(subject_type_1)s"
        )

    def test_save_signature_file_not_supported(self):
        employee_uuid = uuid4()
        signature_filestore_uuid = uuid4()

        self.cmd.user_info = minty.cqrs.UserInfo(
            user_uuid=employee_uuid, permissions={"admin": True}
        )

        employee_settings_row = mock.Mock()
        employee_settings_row.configure_mock(
            employee_uuid=employee_uuid,
            notification_settings={
                "new_document": True,
                "new_assigned_case": True,
                "case_suspension_term_exceeded": True,
                "new_attribute_proposal": False,
                "new_ext_pip_message": False,
                "case_term_exceeded": False,
            },
            signature_uuid=None,
            phone_extension=None,
        )

        mock_get_signature_upload_role = mock.Mock()
        mock_get_signature_upload_role.fetchone.return_value = ("Behandelaar",)

        mock_get_contact_settings = mock.Mock()
        mock_get_contact_settings.fetchone.return_value = employee_settings_row

        filestore_row = mock.Mock()
        filestore_row.configure_mock(id=1, mimetype="text/plain")
        mock_get_filestore = mock.Mock()
        mock_get_filestore.fetchone.return_value = filestore_row

        mock_update_employee_settings = mock.Mock()

        mock_rows = [
            mock_get_signature_upload_role,
            mock_get_contact_settings,
            mock_get_filestore,
            mock_update_employee_settings,
        ]

        self.execute_queries = []

        def mock_execute(query):
            self.execute_queries.append(
                query.compile(dialect=sqlalchemy_pg.dialect())
            )
            return mock_rows.pop(0)

        self.session.execute = mock_execute

        with pytest.raises(Conflict) as excinfo:
            self.cmd.save_signature(
                uuid=str(employee_uuid),
                file_uuid=str(signature_filestore_uuid),
            )

        assert excinfo.value.args == (
            "File of type 'text/plain' is not supported for signature",
            "contact/save_signature/file_not_supported",
        )

    def test_save_signature_file_not_found(self):
        employee_uuid = uuid4()
        signature_filestore_uuid = uuid4()

        self.cmd.user_info = minty.cqrs.UserInfo(
            user_uuid=employee_uuid, permissions={"admin": True}
        )

        employee_settings_row = mock.Mock()
        employee_settings_row.configure_mock(
            employee_uuid=employee_uuid,
            notification_settings={
                "new_document": True,
                "new_assigned_case": True,
                "case_suspension_term_exceeded": True,
                "new_attribute_proposal": False,
                "new_ext_pip_message": False,
                "case_term_exceeded": False,
            },
            signature_uuid=None,
            phone_extension=None,
        )
        mock_get_contact_settings = mock.Mock()
        mock_get_contact_settings.fetchone.return_value = employee_settings_row

        mock_get_signature_upload_role = mock.Mock()
        mock_get_signature_upload_role.fetchone.return_value = ("Behandelaar",)

        mock_get_filestore = mock.Mock()
        mock_get_filestore.fetchone.return_value = None

        mock_update_employee_settings = mock.Mock()

        mock_rows = [
            mock_get_signature_upload_role,
            mock_get_contact_settings,
            mock_get_filestore,
            mock_update_employee_settings,
        ]

        self.execute_queries = []

        def mock_execute(query):
            self.execute_queries.append(
                query.compile(dialect=sqlalchemy_pg.dialect())
            )
            return mock_rows.pop(0)

        self.session.execute = mock_execute

        with pytest.raises(NotFound) as excinfo:
            self.cmd.save_signature(
                uuid=str(employee_uuid),
                file_uuid=str(signature_filestore_uuid),
            )

        assert excinfo.value.args == (
            f"File not found with uuid '{signature_filestore_uuid}'",
            "contact/save_signature/file_not_found",
        )

    def test_save_signature_user_has_no_permission(self):
        employee_uuid = uuid4()
        signature_filestore_uuid = uuid4()

        self.cmd.user_info = minty.cqrs.UserInfo(
            user_uuid=uuid4(), permissions={"admin": False}
        )

        employee_settings_row = mock.Mock()
        employee_settings_row.configure_mock(
            employee_uuid=employee_uuid,
            notification_settings={
                "new_document": True,
                "new_assigned_case": True,
                "case_suspension_term_exceeded": True,
                "new_attribute_proposal": False,
                "new_ext_pip_message": False,
                "case_term_exceeded": False,
            },
            signature_uuid=None,
            phone_extension=None,
        )
        mock_get_contact_settings = mock.Mock()
        mock_get_contact_settings.fetchone.return_value = employee_settings_row

        mock_get_signature_upload_role = mock.Mock()
        mock_get_signature_upload_role.fetchone.return_value = ("Behandelaar",)

        filestore_row = mock.Mock()
        filestore_row.configure_mock(id=1, mimetype="image/jpeg")
        mock_get_filestore = mock.Mock()
        mock_get_filestore.fetchone.return_value = filestore_row

        mock_update_employee_settings = mock.Mock()

        mock_rows = [
            mock_get_signature_upload_role,
            mock_get_contact_settings,
            mock_get_filestore,
            mock_update_employee_settings,
        ]

        self.execute_queries = []

        def mock_execute(query):
            self.execute_queries.append(
                query.compile(dialect=sqlalchemy_pg.dialect())
            )
            return mock_rows.pop(0)

        self.session.execute = mock_execute

        with pytest.raises(Forbidden) as excinfo:
            self.cmd.save_signature(
                uuid=str(employee_uuid),
                file_uuid=str(signature_filestore_uuid),
            )

        assert excinfo.value.args == (
            f"You don't have enough rights to save signature for employee with uuid '{employee_uuid!s}'",
            "contact/save_signature/permission_denied",
        )

        # Reset to check if changing _another_ user is disallowed too:
        self.cmd.user_info = minty.cqrs.UserInfo(
            user_uuid=uuid4(), permissions={"owner_signatures": False}
        )

        mock_rows = [
            mock_get_signature_upload_role,
            mock_get_contact_settings,
            mock_get_filestore,
            mock_update_employee_settings,
        ]

        with pytest.raises(Forbidden) as excinfo:
            self.cmd.save_signature(
                uuid=str(employee_uuid),
                file_uuid=str(signature_filestore_uuid),
            )

        assert excinfo.value.args == (
            f"You don't have enough rights to save signature for employee with uuid '{employee_uuid!s}'",
            "contact/save_signature/permission_denied",
        )

    def test_delete_signature(self):
        employee_uuid = uuid4()
        signature_user = uuid4()

        self.cmd.user_info = minty.cqrs.UserInfo(
            user_uuid=employee_uuid, permissions={"owner_signatures": True}
        )

        mock_get_signature_upload_role = mock.Mock()
        mock_get_signature_upload_role.fetchone.return_value = ("Behandelaar",)

        employee_settings_row = mock.Mock()
        employee_settings_row.configure_mock(
            employee_uuid=signature_user,
            notification_settings={
                "new_document": True,
                "new_assigned_case": True,
                "case_suspension_term_exceeded": True,
                "new_attribute_proposal": False,
                "new_ext_pip_message": False,
                "case_term_exceeded": False,
            },
            signature_uuid=uuid4(),
            phone_extension=None,
        )
        mock_get_contact_settings = mock.Mock()
        mock_get_contact_settings.fetchone.return_value = employee_settings_row

        mock_get_employee_settings = mock.Mock()
        mock_get_employee_settings.fetchone.return_value = ({},)

        mock_update_employee_settings = mock.Mock()

        mock_rows = [
            mock_get_signature_upload_role,
            mock_get_contact_settings,
            mock_update_employee_settings,
        ]

        self.execute_queries = []

        def mock_execute(query):
            self.execute_queries.append(
                query.compile(dialect=sqlalchemy_pg.dialect())
            )
            return mock_rows.pop(0)

        self.session.execute = mock_execute
        self.cmd.delete_signature(uuid=str(signature_user))

        update_settings_statement = self.execute_queries[2]
        assert update_settings_statement.params == {
            "param_1": {"signature_filestore_id": None},
            "subject_type_1": "employee",
            "uuid_1": str(signature_user),
        }
        assert (
            str(update_settings_statement)
            == "UPDATE subject SET settings=(CAST(subject.settings AS JSONB) || %(param_1)s::JSONB) WHERE subject.uuid = %(uuid_1)s::UUID AND subject.subject_type = %(subject_type_1)s"
        )


class TestChangePhoneExtension(minty.cqrs.test.TestBase):
    def setup_method(self):
        self.load_command_instance(case_management)

    def test_change_phone_extension(self):
        employee_uuid = uuid4()
        employee_settings_row = mock.Mock()
        employee_settings_row.configure_mock(
            employee_uuid=employee_uuid,
            notification_settings={
                "new_document": True,
                "new_assigned_case": True,
                "case_suspension_term_exceeded": True,
                "new_attribute_proposal": False,
                "new_ext_pip_message": False,
                "case_term_exceeded": False,
            },
            signature_uuid=uuid4(),
            phone_extension=None,
        )

        self.cmd.user_info = minty.cqrs.UserInfo(
            user_uuid=employee_uuid,
            permissions={"admin": True},
        )

        mock_get_contact_settings = mock.Mock()
        mock_get_contact_settings.fetchone.return_value = employee_settings_row

        mock_update_employee_settings = mock.Mock()
        mock_rows = [
            mock_get_contact_settings,
            mock_update_employee_settings,
        ]

        self.execute_queries = []

        def mock_execute(query):
            self.execute_queries.append(
                query.compile(dialect=sqlalchemy_pg.dialect())
            )
            return mock_rows.pop(0)

        self.session.execute = mock_execute
        self.cmd.change_phone_extension(
            uuid=str(employee_uuid), extension="abc"
        )

        update_settings_statement = self.execute_queries[1]
        assert update_settings_statement.params == {
            "uuid_1": str(employee_uuid),
            "param_1": {"kcc": {"extension": "abc"}},
            "subject_type_1": "employee",
        }

        assert (
            str(update_settings_statement)
            == "UPDATE subject SET settings=(CAST(subject.settings AS JSONB) || %(param_1)s::JSONB) WHERE subject.uuid = %(uuid_1)s::UUID AND subject.subject_type = %(subject_type_1)s"
        )

    def test_change_phone_extension_not_allowed(self):
        employee_uuid = uuid4()
        other_employee_uuid = uuid4()

        self.cmd.user_info = minty.cqrs.UserInfo(
            user_uuid=employee_uuid,
            permissions={"admin": False},
        )

        with pytest.raises(Forbidden) as excinfo:
            self.cmd.change_phone_extension(
                uuid=str(other_employee_uuid), extension="abc"
            )

        assert excinfo.value.args == (
            f"You don't have enough rights to set phone extension for employee with uuid '{other_employee_uuid!s}'",
            "contact/change_phone_extension/permission_denied",
        )


class TestAs_An_admin_I_want_to_set_a_related_object_for_a_contact(
    minty.cqrs.test.TestBase
):
    def setup_method(self):
        self.load_command_instance(case_management)

    def test_set_related_object_person(self):
        custom_object_uuid = str(uuid4())
        contact_uuid = uuid4()

        person_row = mock.MagicMock()
        person_row.configure_mock(
            uuid=contact_uuid,
            authenticated=True,
            authenticatedby=None,
            first_names=None,
            insertions=None,
            family_name=None,
            surname="beheerder",
            initials="J",
            noble_title=None,
            inside_municipality=False,
            address={
                "street": "street",
                "street_number": "23",
                "zipcode": "1568GE",
                "city": "City",
                "landcode": 6030,
                "country_code": 6030,
                "bag_id": "1234123412341234",
            },
            landcode=6030,
            correspondence_address=None,
            related_custom_object_uuid=None,
            preferred_contact_channel="pip",
            summary="beheerder",
            indicatie_geheim="0",
            external_identifier=None,
        )
        custom_object_row = mock.Mock()
        custom_object_row.id = 42

        self.session.execute().fetchone.side_effect = [
            person_row,
            custom_object_row,
        ]
        self.session.execute.reset_mock()

        self.cmd.set_subject_related_custom_object(
            custom_object_uuid=custom_object_uuid,
            person_uuid=str(contact_uuid),
        )

        execute_calls = self.session.execute.call_args_list
        assert len(execute_calls) == 3

        # first call, args, first arg
        person_select = execute_calls[0][0][0]
        person_query = person_select.compile(dialect=sqlalchemy_pg.dialect())

        assert str(person_query) == (
            "SELECT natuurlijk_persoon.uuid, natuurlijk_persoon.authenticated, natuurlijk_persoon.authenticatedby, natuurlijk_persoon.voornamen AS first_names, natuurlijk_persoon.voorletters AS initials, natuurlijk_persoon.voorvoegsel AS insertions, natuurlijk_persoon.geslachtsnaam AS family_name, natuurlijk_persoon.adellijke_titel AS noble_title, natuurlijk_persoon.surname AS surname, natuurlijk_persoon.geboortedatum AS date_of_birth, natuurlijk_persoon.datum_overlijden AS date_of_death, natuurlijk_persoon.geslachtsaanduiding AS gender, natuurlijk_persoon.in_gemeente AS inside_municipality, natuurlijk_persoon.landcode, natuurlijk_persoon.preferred_contact_channel, natuurlijk_persoon.active AS active, (SELECT json_build_object(%(json_build_object_2)s, contact_data.email, %(json_build_object_3)s, contact_data.telefoonnummer, %(json_build_object_4)s, contact_data.mobiel, %(json_build_object_5)s, contact_data.note) AS json_build_object_1 \n"
            "FROM contact_data \n"
            "WHERE contact_data.gegevens_magazijn_id = natuurlijk_persoon.id AND contact_data.betrokkene_type = %(betrokkene_type_1)s) AS contact_information, (SELECT CASE WHEN (adres_1.straatnaam != %(straatnaam_1)s AND (adres_1.landcode = %(landcode_1)s OR adres_1.landcode = %(landcode_2)s)) THEN json_build_object(%(json_build_object_6)s, adres_1.straatnaam, %(json_build_object_7)s, adres_1.postcode, %(json_build_object_8)s, adres_1.huisnummer, %(json_build_object_9)s, adres_1.huisletter, %(json_build_object_10)s, adres_1.huisnummertoevoeging, %(json_build_object_11)s, adres_1.woonplaats, %(json_build_object_12)s, adres_1.landcode, %(json_build_object_13)s, adres_1.bag_id, %(json_build_object_14)s, %(json_build_object_15)s, %(json_build_object_16)s, adres_1.geo_lat_long) ELSE json_build_object(%(json_build_object_17)s, adres_1.landcode, %(json_build_object_18)s, adres_1.adres_buitenland1, %(json_build_object_19)s, adres_1.adres_buitenland2, %(json_build_object_20)s, adres_1.adres_buitenland3, %(json_build_object_21)s, NULL, %(json_build_object_22)s, %(json_build_object_23)s, %(json_build_object_24)s, adres_1.geo_lat_long) END AS anon_1 \n"
            "FROM adres AS adres_1 \n"
            "WHERE adres_1.natuurlijk_persoon_id = natuurlijk_persoon.id AND adres_1.functie_adres = %(functie_adres_1)s) AS address, (SELECT CASE WHEN (adres_2.straatnaam != %(straatnaam_2)s AND (adres_2.landcode = %(landcode_3)s OR adres_2.landcode = %(landcode_4)s)) THEN json_build_object(%(json_build_object_25)s, adres_2.straatnaam, %(json_build_object_26)s, adres_2.postcode, %(json_build_object_27)s, adres_2.huisnummer, %(json_build_object_28)s, adres_2.huisletter, %(json_build_object_29)s, adres_2.huisnummertoevoeging, %(json_build_object_30)s, adres_2.woonplaats, %(json_build_object_31)s, adres_2.landcode, %(json_build_object_32)s, adres_2.bag_id, %(json_build_object_33)s, %(json_build_object_34)s, %(json_build_object_35)s, adres_2.geo_lat_long) ELSE json_build_object(%(json_build_object_36)s, adres_2.landcode, %(json_build_object_37)s, adres_2.adres_buitenland1, %(json_build_object_38)s, adres_2.adres_buitenland2, %(json_build_object_39)s, adres_2.adres_buitenland3, %(json_build_object_40)s, NULL, %(json_build_object_41)s, %(json_build_object_42)s, %(json_build_object_43)s, adres_2.geo_lat_long) END AS anon_2 \n"
            "FROM adres AS adres_2 \n"
            "WHERE adres_2.natuurlijk_persoon_id = natuurlijk_persoon.id AND adres_2.functie_adres = %(functie_adres_2)s) AS correspondence_address, natuurlijk_persoon.indicatie_geheim AS indicatie_geheim, custom_object.uuid AS related_custom_object_uuid, subject.properties, concat(natuurlijk_persoon.adellijke_titel || %(adellijke_titel_1)s, natuurlijk_persoon.voorletters || %(voorletters_1)s, natuurlijk_persoon.surname) AS summary, object_subscription.external_id AS external_identifier \n"
            "FROM natuurlijk_persoon LEFT OUTER JOIN custom_object ON natuurlijk_persoon.related_custom_object_id = custom_object.id LEFT OUTER JOIN subject ON subject.uuid = natuurlijk_persoon.uuid AND subject.subject_type = %(subject_type_1)s LEFT OUTER JOIN object_subscription ON natuurlijk_persoon.id = CAST(object_subscription.local_id AS INTEGER) AND object_subscription.local_table = %(local_table_1)s AND object_subscription.date_deleted IS NULL \n"
            "WHERE natuurlijk_persoon.deleted_on IS NULL AND natuurlijk_persoon.uuid = %(uuid_1)s::UUID"
        )
        assert person_query.params["uuid_1"] == contact_uuid

        # second call, args, first arg
        custom_object_select = execute_calls[1][0][0]
        custom_object_query = custom_object_select.compile(
            dialect=sqlalchemy_pg.dialect()
        )

        assert str(custom_object_query) == (
            "SELECT custom_object.id \n"
            "FROM custom_object \n"
            "WHERE custom_object.uuid = %(uuid_1)s::UUID"
        )
        assert custom_object_query.params["uuid_1"] == custom_object_uuid

        # third call, args, first arg
        person_update = execute_calls[2][0][0]
        update_query = person_update.compile(dialect=sqlalchemy_pg.dialect())

        assert str(update_query) == (
            "UPDATE natuurlijk_persoon SET related_custom_object_id=%(related_custom_object_id)s WHERE natuurlijk_persoon.uuid = %(uuid_1)s::UUID"
        )
        assert update_query.params["related_custom_object_id"] == 42
        assert update_query.params["uuid_1"] == str(contact_uuid)

    def test_set_related_object_person_clear(self):
        custom_object_uuid = None
        contact_uuid = uuid4()

        person_row = mock.MagicMock()
        person_row.configure_mock(
            uuid=contact_uuid,
            authenticated=True,
            authenticatedby=None,
            first_names=None,
            insertions=None,
            family_name=None,
            surname="test user",
            initials="J",
            noble_title=None,
            inside_municipality=False,
            address={
                "street": "street",
                "street_number": "23",
                "zipcode": "1568GE",
                "city": "City",
                "country_code": 6030,
                "bag_id": "1234567890123456",
            },
            landcode=6030,
            correspondence_address=None,
            related_custom_object_uuid=uuid4(),
            preferred_contact_channel="pip",
            summary="test user",
            indicatie_geheim="0",
            external_identifier=None,
        )

        self.session.execute().fetchone.side_effect = [person_row]
        self.session.execute.reset_mock()

        self.cmd.set_subject_related_custom_object(
            custom_object_uuid=custom_object_uuid,
            person_uuid=str(contact_uuid),
        )

        execute_calls = self.session.execute.call_args_list
        assert len(execute_calls) == 2

        # first call, args, first arg
        person_select = execute_calls[0][0][0]
        person_query = person_select.compile(dialect=sqlalchemy_pg.dialect())

        assert str(person_query) == (
            "SELECT natuurlijk_persoon.uuid, natuurlijk_persoon.authenticated, natuurlijk_persoon.authenticatedby, natuurlijk_persoon.voornamen AS first_names, natuurlijk_persoon.voorletters AS initials, natuurlijk_persoon.voorvoegsel AS insertions, natuurlijk_persoon.geslachtsnaam AS family_name, natuurlijk_persoon.adellijke_titel AS noble_title, natuurlijk_persoon.surname AS surname, natuurlijk_persoon.geboortedatum AS date_of_birth, natuurlijk_persoon.datum_overlijden AS date_of_death, natuurlijk_persoon.geslachtsaanduiding AS gender, natuurlijk_persoon.in_gemeente AS inside_municipality, natuurlijk_persoon.landcode, natuurlijk_persoon.preferred_contact_channel, natuurlijk_persoon.active AS active, (SELECT json_build_object(%(json_build_object_2)s, contact_data.email, %(json_build_object_3)s, contact_data.telefoonnummer, %(json_build_object_4)s, contact_data.mobiel, %(json_build_object_5)s, contact_data.note) AS json_build_object_1 \n"
            "FROM contact_data \n"
            "WHERE contact_data.gegevens_magazijn_id = natuurlijk_persoon.id AND contact_data.betrokkene_type = %(betrokkene_type_1)s) AS contact_information, (SELECT CASE WHEN (adres_1.straatnaam != %(straatnaam_1)s AND (adres_1.landcode = %(landcode_1)s OR adres_1.landcode = %(landcode_2)s)) THEN json_build_object(%(json_build_object_6)s, adres_1.straatnaam, %(json_build_object_7)s, adres_1.postcode, %(json_build_object_8)s, adres_1.huisnummer, %(json_build_object_9)s, adres_1.huisletter, %(json_build_object_10)s, adres_1.huisnummertoevoeging, %(json_build_object_11)s, adres_1.woonplaats, %(json_build_object_12)s, adres_1.landcode, %(json_build_object_13)s, adres_1.bag_id, %(json_build_object_14)s, %(json_build_object_15)s, %(json_build_object_16)s, adres_1.geo_lat_long) ELSE json_build_object(%(json_build_object_17)s, adres_1.landcode, %(json_build_object_18)s, adres_1.adres_buitenland1, %(json_build_object_19)s, adres_1.adres_buitenland2, %(json_build_object_20)s, adres_1.adres_buitenland3, %(json_build_object_21)s, NULL, %(json_build_object_22)s, %(json_build_object_23)s, %(json_build_object_24)s, adres_1.geo_lat_long) END AS anon_1 \n"
            "FROM adres AS adres_1 \n"
            "WHERE adres_1.natuurlijk_persoon_id = natuurlijk_persoon.id AND adres_1.functie_adres = %(functie_adres_1)s) AS address, (SELECT CASE WHEN (adres_2.straatnaam != %(straatnaam_2)s AND (adres_2.landcode = %(landcode_3)s OR adres_2.landcode = %(landcode_4)s)) THEN json_build_object(%(json_build_object_25)s, adres_2.straatnaam, %(json_build_object_26)s, adres_2.postcode, %(json_build_object_27)s, adres_2.huisnummer, %(json_build_object_28)s, adres_2.huisletter, %(json_build_object_29)s, adres_2.huisnummertoevoeging, %(json_build_object_30)s, adres_2.woonplaats, %(json_build_object_31)s, adres_2.landcode, %(json_build_object_32)s, adres_2.bag_id, %(json_build_object_33)s, %(json_build_object_34)s, %(json_build_object_35)s, adres_2.geo_lat_long) ELSE json_build_object(%(json_build_object_36)s, adres_2.landcode, %(json_build_object_37)s, adres_2.adres_buitenland1, %(json_build_object_38)s, adres_2.adres_buitenland2, %(json_build_object_39)s, adres_2.adres_buitenland3, %(json_build_object_40)s, NULL, %(json_build_object_41)s, %(json_build_object_42)s, %(json_build_object_43)s, adres_2.geo_lat_long) END AS anon_2 \n"
            "FROM adres AS adres_2 \n"
            "WHERE adres_2.natuurlijk_persoon_id = natuurlijk_persoon.id AND adres_2.functie_adres = %(functie_adres_2)s) AS correspondence_address, natuurlijk_persoon.indicatie_geheim AS indicatie_geheim, custom_object.uuid AS related_custom_object_uuid, subject.properties, concat(natuurlijk_persoon.adellijke_titel || %(adellijke_titel_1)s, natuurlijk_persoon.voorletters || %(voorletters_1)s, natuurlijk_persoon.surname) AS summary, object_subscription.external_id AS external_identifier \n"
            "FROM natuurlijk_persoon LEFT OUTER JOIN custom_object ON natuurlijk_persoon.related_custom_object_id = custom_object.id LEFT OUTER JOIN subject ON subject.uuid = natuurlijk_persoon.uuid AND subject.subject_type = %(subject_type_1)s LEFT OUTER JOIN object_subscription ON natuurlijk_persoon.id = CAST(object_subscription.local_id AS INTEGER) AND object_subscription.local_table = %(local_table_1)s AND object_subscription.date_deleted IS NULL \n"
            "WHERE natuurlijk_persoon.deleted_on IS NULL AND natuurlijk_persoon.uuid = %(uuid_1)s::UUID"
        )
        assert person_query.params["uuid_1"] == contact_uuid

        # second call, args, first arg
        person_update = execute_calls[1][0][0]
        update_query = person_update.compile(dialect=sqlalchemy_pg.dialect())

        assert str(update_query) == (
            "UPDATE natuurlijk_persoon SET related_custom_object_id=%(related_custom_object_id)s WHERE natuurlijk_persoon.uuid = %(uuid_1)s::UUID"
        )
        assert update_query.params["related_custom_object_id"] is None
        assert update_query.params["uuid_1"] == str(contact_uuid)

    def test_set_related_object_person_unknown_object(self):
        custom_object_uuid = str(uuid4())
        contact_uuid = uuid4()

        person_row = mock.MagicMock()
        person_row.configure_mock(
            uuid=contact_uuid,
            authenticated=True,
            authenticatedby=None,
            first_names=None,
            insertions=None,
            family_name=None,
            surname="beheerder",
            initials="J",
            noble_title=None,
            inside_municipality=False,
            address={
                "street": "street",
                "street_number": "23",
                "zipcode": "1568GE",
                "city": "City",
                "country_code": 6030,
                "bag_id": None,
            },
            landcode=6030,
            correspondence_address=None,
            related_custom_object_uuid=None,
            preferred_contact_channel="pip",
            summary="beheerder",
            external_identifier=None,
        )

        self.session.execute().fetchone.side_effect = [
            person_row,
            None,
        ]
        self.session.execute.reset_mock()

        with pytest.raises(NotFound):
            self.cmd.set_subject_related_custom_object(
                custom_object_uuid=custom_object_uuid,
                person_uuid=str(contact_uuid),
            )

        execute_calls = self.session.execute.call_args_list
        assert len(execute_calls) == 2

    def test_set_related_object_organization(self):
        custom_object_uuid = uuid4()
        contact_uuid = uuid4()

        organization_row = mock.Mock()
        organization_row.configure_mock(
            uuid=contact_uuid,
            authenticated=True,
            authenticatedby="Intern",
            name="Company Name",
            coc_number="12345678",
            coc_location_number="123456789012",
            date_founded=None,
            date_registered=None,
            date_ceased=None,
            rsin=None,
            oin=None,
            main_activity={"code": 123, "description": "Something"},
            secondary_activities=[{"code": 31337, "description": "Test"}],
            business_entity_code=1,  # Eenmanszaak
            location_address=None,
            correspondence_address=None,
            contact_information={},
            related_custom_object_uuid=None,
            preferred_contact_channel="pip",
            contact_person_info={},
            summary="Company Name",
        )
        custom_object_row = mock.Mock()
        custom_object_row.id = 42

        self.session.execute().fetchone.side_effect = [
            organization_row,
            custom_object_row,
        ]
        self.session.execute.reset_mock()

        self.cmd.set_subject_related_custom_object(
            custom_object_uuid=str(custom_object_uuid),
            organization_uuid=str(contact_uuid),
        )

        execute_calls = self.session.execute.call_args_list
        assert len(execute_calls) == 3

        # first call, args, first arg
        organization_select = execute_calls[0][0][0]
        organization_query = organization_select.compile(
            dialect=sqlalchemy_pg.dialect()
        )
        assert str(organization_query) == (
            "SELECT bedrijf.uuid, bedrijf.authenticated, bedrijf.authenticatedby, bedrijf.handelsnaam AS name, bedrijf.date_founded, bedrijf.date_registration AS date_registered, bedrijf.date_ceased, bedrijf.rsin, bedrijf.oin, bedrijf.dossiernummer AS coc_number, bedrijf.vestigingsnummer AS coc_location_number, bedrijf.rechtsvorm AS business_entity_code, bedrijf.main_activity, bedrijf.secondairy_activities AS secondary_activities, bedrijf.preferred_contact_channel, json_build_object(%(json_build_object_1)s, contact_data.email, %(json_build_object_2)s, contact_data.telefoonnummer, %(json_build_object_3)s, contact_data.mobiel, %(json_build_object_4)s, contact_data.note, %(json_build_object_5)s, bedrijf.preferred_contact_channel, %(json_build_object_6)s, CASE WHEN (subject.properties IS NOT NULL) THEN CAST(CAST(subject.properties AS JSONB) ->> %(param_1)s AS BOOLEAN) ELSE %(param_2)s END) AS contact_information, CASE WHEN (coalesce(bedrijf.vestiging_adres_buitenland1, bedrijf.vestiging_adres_buitenland2, bedrijf.vestiging_adres_buitenland3, bedrijf.vestiging_straatnaam, %(coalesce_1)s) = %(coalesce_2)s) THEN NULL WHEN (bedrijf.vestiging_straatnaam != %(vestiging_straatnaam_1)s AND (bedrijf.vestiging_landcode = %(vestiging_landcode_1)s OR bedrijf.vestiging_landcode = %(vestiging_landcode_2)s)) THEN CAST(json_build_object(%(json_build_object_7)s, bedrijf.vestiging_straatnaam, %(json_build_object_8)s, bedrijf.vestiging_postcode, %(json_build_object_9)s, bedrijf.vestiging_huisnummer, %(json_build_object_10)s, bedrijf.vestiging_huisletter, %(json_build_object_11)s, bedrijf.vestiging_huisnummertoevoeging, %(json_build_object_12)s, bedrijf.vestiging_woonplaats, %(json_build_object_13)s, bedrijf.vestiging_landcode, %(json_build_object_14)s, bedrijf.vestiging_bag_id, %(json_build_object_15)s, %(json_build_object_16)s, %(json_build_object_17)s, bedrijf.vestiging_latlong) AS JSON) ELSE CAST(json_build_object(%(json_build_object_18)s, bedrijf.vestiging_adres_buitenland1, %(json_build_object_19)s, bedrijf.vestiging_adres_buitenland2, %(json_build_object_20)s, bedrijf.vestiging_adres_buitenland3, %(json_build_object_21)s, bedrijf.vestiging_landcode, %(json_build_object_22)s, NULL, %(json_build_object_23)s, %(json_build_object_24)s, %(json_build_object_25)s, bedrijf.vestiging_latlong) AS JSON) END AS location_address, CASE WHEN (coalesce(bedrijf.correspondentie_adres_buitenland1, bedrijf.correspondentie_adres_buitenland2, bedrijf.correspondentie_adres_buitenland3, bedrijf.correspondentie_straatnaam, %(coalesce_3)s) = %(coalesce_4)s) THEN NULL WHEN (bedrijf.correspondentie_straatnaam != %(correspondentie_straatnaam_1)s AND (bedrijf.correspondentie_landcode = %(correspondentie_landcode_1)s OR bedrijf.correspondentie_landcode = %(correspondentie_landcode_2)s)) THEN CAST(json_build_object(%(json_build_object_26)s, bedrijf.correspondentie_straatnaam, %(json_build_object_27)s, bedrijf.correspondentie_postcode, %(json_build_object_28)s, bedrijf.correspondentie_huisnummer, %(json_build_object_29)s, bedrijf.correspondentie_huisletter, %(json_build_object_30)s, bedrijf.correspondentie_huisnummertoevoeging, %(json_build_object_31)s, bedrijf.correspondentie_woonplaats, %(json_build_object_32)s, bedrijf.correspondentie_landcode, %(json_build_object_33)s, %(json_build_object_34)s) AS JSON) ELSE CAST(json_build_object(%(json_build_object_35)s, bedrijf.correspondentie_adres_buitenland1, %(json_build_object_36)s, bedrijf.correspondentie_adres_buitenland2, %(json_build_object_37)s, bedrijf.correspondentie_adres_buitenland3, %(json_build_object_38)s, bedrijf.correspondentie_landcode, %(json_build_object_39)s, %(json_build_object_40)s) AS JSON) END AS correspondence_address, custom_object.uuid AS related_custom_object_uuid, json_build_object(%(json_build_object_41)s, bedrijf.contact_voorletters, %(json_build_object_42)s, bedrijf.contact_voorvoegsel, %(json_build_object_43)s, bedrijf.contact_geslachtsnaam, %(json_build_object_44)s, bedrijf.contact_naam) AS contact_person_info, bedrijf.handelsnaam AS summary \n"
            "FROM bedrijf LEFT OUTER JOIN custom_object ON bedrijf.related_custom_object_id = custom_object.id LEFT OUTER JOIN contact_data ON contact_data.gegevens_magazijn_id = bedrijf.id AND contact_data.betrokkene_type = %(betrokkene_type_1)s LEFT OUTER JOIN subject ON subject.uuid = bedrijf.uuid AND subject.subject_type = %(subject_type_1)s \n"
            "WHERE bedrijf.deleted_on IS NULL AND bedrijf.uuid = %(uuid_1)s::UUID"
        )
        assert organization_query.params["uuid_1"] == contact_uuid

        # second call, args, first arg
        custom_object_select = execute_calls[1][0][0]
        custom_object_query = custom_object_select.compile(
            dialect=sqlalchemy_pg.dialect()
        )

        assert str(custom_object_query) == (
            "SELECT custom_object.id \n"
            "FROM custom_object \n"
            "WHERE custom_object.uuid = %(uuid_1)s::UUID"
        )
        assert custom_object_query.params["uuid_1"] == str(custom_object_uuid)

        # third call, args, first arg
        organization_update = execute_calls[2][0][0]
        update_query = organization_update.compile(
            dialect=sqlalchemy_pg.dialect()
        )

        assert str(update_query) == (
            "UPDATE bedrijf SET related_custom_object_id=%(related_custom_object_id)s WHERE bedrijf.uuid = %(uuid_1)s::UUID"
        )
        assert update_query.params["related_custom_object_id"] == 42
        assert update_query.params["uuid_1"] == str(contact_uuid)

    def test_set_related_object_organization_clear(self):
        custom_object_uuid = None
        contact_uuid = uuid4()

        organization_row = mock.Mock()
        organization_row.configure_mock(
            uuid=contact_uuid,
            authenticated=True,
            authenticatedby="Intern",
            name="Company Name",
            coc_number="12345678",
            coc_location_number="123456789012",
            date_founded=None,
            date_registered=None,
            date_ceased=None,
            rsin=None,
            oin=None,
            main_activity=None,
            secondary_activities=[],
            business_entity_code=1,  # Eenmanszaak
            location_address=None,
            correspondence_address=None,
            contact_information={},
            related_custom_object_uuid=uuid4(),
            preferred_contact_channel="pip",
            contact_person_info={},
            summary="Company Name",
        )

        self.session.execute().fetchone.side_effect = [organization_row]
        self.session.execute.reset_mock()

        self.cmd.set_subject_related_custom_object(
            custom_object_uuid=custom_object_uuid,
            organization_uuid=str(contact_uuid),
        )

        execute_calls = self.session.execute.call_args_list
        assert len(execute_calls) == 2

        # first call, args, first arg
        organization_select = execute_calls[0][0][0]
        organization_query = organization_select.compile(
            dialect=sqlalchemy_pg.dialect()
        )

        assert str(organization_query) == (
            "SELECT bedrijf.uuid, bedrijf.authenticated, bedrijf.authenticatedby, bedrijf.handelsnaam AS name, bedrijf.date_founded, bedrijf.date_registration AS date_registered, bedrijf.date_ceased, bedrijf.rsin, bedrijf.oin, bedrijf.dossiernummer AS coc_number, bedrijf.vestigingsnummer AS coc_location_number, bedrijf.rechtsvorm AS business_entity_code, bedrijf.main_activity, bedrijf.secondairy_activities AS secondary_activities, bedrijf.preferred_contact_channel, json_build_object(%(json_build_object_1)s, contact_data.email, %(json_build_object_2)s, contact_data.telefoonnummer, %(json_build_object_3)s, contact_data.mobiel, %(json_build_object_4)s, contact_data.note, %(json_build_object_5)s, bedrijf.preferred_contact_channel, %(json_build_object_6)s, CASE WHEN (subject.properties IS NOT NULL) THEN CAST(CAST(subject.properties AS JSONB) ->> %(param_1)s AS BOOLEAN) ELSE %(param_2)s END) AS contact_information, CASE WHEN (coalesce(bedrijf.vestiging_adres_buitenland1, bedrijf.vestiging_adres_buitenland2, bedrijf.vestiging_adres_buitenland3, bedrijf.vestiging_straatnaam, %(coalesce_1)s) = %(coalesce_2)s) THEN NULL WHEN (bedrijf.vestiging_straatnaam != %(vestiging_straatnaam_1)s AND (bedrijf.vestiging_landcode = %(vestiging_landcode_1)s OR bedrijf.vestiging_landcode = %(vestiging_landcode_2)s)) THEN CAST(json_build_object(%(json_build_object_7)s, bedrijf.vestiging_straatnaam, %(json_build_object_8)s, bedrijf.vestiging_postcode, %(json_build_object_9)s, bedrijf.vestiging_huisnummer, %(json_build_object_10)s, bedrijf.vestiging_huisletter, %(json_build_object_11)s, bedrijf.vestiging_huisnummertoevoeging, %(json_build_object_12)s, bedrijf.vestiging_woonplaats, %(json_build_object_13)s, bedrijf.vestiging_landcode, %(json_build_object_14)s, bedrijf.vestiging_bag_id, %(json_build_object_15)s, %(json_build_object_16)s, %(json_build_object_17)s, bedrijf.vestiging_latlong) AS JSON) ELSE CAST(json_build_object(%(json_build_object_18)s, bedrijf.vestiging_adres_buitenland1, %(json_build_object_19)s, bedrijf.vestiging_adres_buitenland2, %(json_build_object_20)s, bedrijf.vestiging_adres_buitenland3, %(json_build_object_21)s, bedrijf.vestiging_landcode, %(json_build_object_22)s, NULL, %(json_build_object_23)s, %(json_build_object_24)s, %(json_build_object_25)s, bedrijf.vestiging_latlong) AS JSON) END AS location_address, CASE WHEN (coalesce(bedrijf.correspondentie_adres_buitenland1, bedrijf.correspondentie_adres_buitenland2, bedrijf.correspondentie_adres_buitenland3, bedrijf.correspondentie_straatnaam, %(coalesce_3)s) = %(coalesce_4)s) THEN NULL WHEN (bedrijf.correspondentie_straatnaam != %(correspondentie_straatnaam_1)s AND (bedrijf.correspondentie_landcode = %(correspondentie_landcode_1)s OR bedrijf.correspondentie_landcode = %(correspondentie_landcode_2)s)) THEN CAST(json_build_object(%(json_build_object_26)s, bedrijf.correspondentie_straatnaam, %(json_build_object_27)s, bedrijf.correspondentie_postcode, %(json_build_object_28)s, bedrijf.correspondentie_huisnummer, %(json_build_object_29)s, bedrijf.correspondentie_huisletter, %(json_build_object_30)s, bedrijf.correspondentie_huisnummertoevoeging, %(json_build_object_31)s, bedrijf.correspondentie_woonplaats, %(json_build_object_32)s, bedrijf.correspondentie_landcode, %(json_build_object_33)s, %(json_build_object_34)s) AS JSON) ELSE CAST(json_build_object(%(json_build_object_35)s, bedrijf.correspondentie_adres_buitenland1, %(json_build_object_36)s, bedrijf.correspondentie_adres_buitenland2, %(json_build_object_37)s, bedrijf.correspondentie_adres_buitenland3, %(json_build_object_38)s, bedrijf.correspondentie_landcode, %(json_build_object_39)s, %(json_build_object_40)s) AS JSON) END AS correspondence_address, custom_object.uuid AS related_custom_object_uuid, json_build_object(%(json_build_object_41)s, bedrijf.contact_voorletters, %(json_build_object_42)s, bedrijf.contact_voorvoegsel, %(json_build_object_43)s, bedrijf.contact_geslachtsnaam, %(json_build_object_44)s, bedrijf.contact_naam) AS contact_person_info, bedrijf.handelsnaam AS summary \n"
            "FROM bedrijf LEFT OUTER JOIN custom_object ON bedrijf.related_custom_object_id = custom_object.id LEFT OUTER JOIN contact_data ON contact_data.gegevens_magazijn_id = bedrijf.id AND contact_data.betrokkene_type = %(betrokkene_type_1)s LEFT OUTER JOIN subject ON subject.uuid = bedrijf.uuid AND subject.subject_type = %(subject_type_1)s \n"
            "WHERE bedrijf.deleted_on IS NULL AND bedrijf.uuid = %(uuid_1)s::UUID"
        )
        assert organization_query.params["uuid_1"] == contact_uuid

        # second call, args, first arg
        organization_update = execute_calls[1][0][0]
        update_query = organization_update.compile(
            dialect=sqlalchemy_pg.dialect()
        )

        assert str(update_query) == (
            "UPDATE bedrijf SET related_custom_object_id=%(related_custom_object_id)s WHERE bedrijf.uuid = %(uuid_1)s::UUID"
        )
        assert update_query.params["related_custom_object_id"] is None
        assert update_query.params["uuid_1"] == str(contact_uuid)

    def test_set_related_object_organization_unknown_object(self):
        custom_object_uuid = str(uuid4())
        contact_uuid = str(uuid4())

        organization_row = mock.MagicMock()
        organization_row.configure_mock(
            uuid=contact_uuid,
            authenticated=True,
            authenticatedby="Intern",
            name="Company Name",
            coc_number="12345678",
            coc_location_number="123456789012",
            date_founded=None,
            date_registered=None,
            date_ceased=None,
            rsin=None,
            oin=None,
            main_activity=None,
            secondary_activities=[],
            business_entity_code=1,  # Eenmanszaak
            location_address=None,
            correspondence_address=None,
            contact_information={},
            related_custom_object_uuid=uuid4(),
            preferred_contact_channel="pip",
            contact_person_info={},
            summary="Company Name",
        )

        self.session.execute().fetchone.side_effect = [
            organization_row,
            None,
        ]
        self.session.execute.reset_mock()

        with pytest.raises(NotFound):
            self.cmd.set_subject_related_custom_object(
                custom_object_uuid=custom_object_uuid,
                organization_uuid=contact_uuid,
            )

        execute_calls = self.session.execute.call_args_list
        assert len(execute_calls) == 2

    def test_set_related_object_employee(self):
        custom_object_uuid = str(uuid4())
        contact_uuid = uuid4()

        employee_row = mock.MagicMock()
        employee_row.configure_mock(
            uuid=contact_uuid,
            name="beheerder",
            first_name="beheerder",
            surname="beheerder",
            title=None,
            contact_information={},
            active=True,
            role=[
                {
                    "uuid": uuid4(),
                    "name": "Role",
                    "parent": {"uuid": uuid4(), "name": "Testdepartment"},
                }
            ],
            department={"uuid": uuid4(), "name": "Testdepartment"},
            related_custom_object_uuid=None,
            preferred_contact_channel="pip",
            summary="beheerder",
        )

        custom_object_row = mock.Mock()
        custom_object_row.id = 42

        self.session.execute().fetchone.side_effect = [
            employee_row,
            custom_object_row,
        ]
        self.session.execute.reset_mock()

        self.cmd.set_subject_related_custom_object(
            custom_object_uuid=custom_object_uuid,
            employee_uuid=str(contact_uuid),
        )

        execute_calls = self.session.execute.call_args_list
        assert len(execute_calls) == 3

        # first call, args, first arg
        employee_select = execute_calls[0][0][0]
        employee_query = employee_select.compile(
            dialect=sqlalchemy_pg.dialect()
        )

        assert str(employee_query) == (
            "SELECT subject.uuid, subject.username AS name, coalesce(CAST(subject.properties AS JSON) -> %(param_1)s, NULL) AS first_name, coalesce(CAST(subject.properties AS JSON) -> %(param_2)s, NULL) AS surname, coalesce(CAST(subject.properties AS JSON) -> %(param_3)s, NULL) AS title, json_build_object(%(json_build_object_1)s, coalesce(CAST(subject.properties AS JSON) -> %(param_4)s, NULL), %(json_build_object_2)s, coalesce(CAST(subject.properties AS JSON) -> %(param_5)s, NULL)) AS contact_information, (SELECT coalesce(user_entity.active, %(coalesce_2)s) AS coalesce_1 \n"
            "FROM user_entity \n"
            "WHERE user_entity.subject_id = subject.id \n"
            " LIMIT %(param_6)s) AS active, (SELECT json_build_object(%(json_build_object_4)s, groups.uuid, %(json_build_object_5)s, groups.name) AS json_build_object_3 \n"
            "FROM groups \n"
            "WHERE groups.id = subject.group_ids[%(group_ids_1)s]) AS department, array((SELECT json_build_object(%(json_build_object_7)s, roles.uuid, %(json_build_object_8)s, roles.name, %(json_build_object_9)s, json_build_object(%(json_build_object_10)s, groups_1.uuid, %(json_build_object_11)s, groups_1.name)) AS json_build_object_6 \n"
            "FROM subject AS subject_1 JOIN LATERAL json_array_elements_text(array_to_json(subject_1.role_ids)) AS role_id ON subject_1.uuid = subject.uuid JOIN roles ON roles.id = CAST(role_id AS INTEGER) JOIN groups AS groups_1 ON groups_1.id = roles.parent_group_id)) AS roles, custom_object.uuid AS related_custom_object_uuid, CAST(subject.properties AS JSON) ->> %(param_7)s AS summary \n"
            "FROM subject LEFT OUTER JOIN custom_object ON subject.related_custom_object_id = custom_object.id \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID AND subject.subject_type = %(subject_type_1)s"
        )
        assert employee_query.params["uuid_1"] == contact_uuid

        # second call, args, first arg
        custom_object_select = execute_calls[1][0][0]
        custom_object_query = custom_object_select.compile(
            dialect=sqlalchemy_pg.dialect()
        )

        assert str(custom_object_query) == (
            "SELECT custom_object.id \n"
            "FROM custom_object \n"
            "WHERE custom_object.uuid = %(uuid_1)s::UUID"
        )
        assert custom_object_query.params["uuid_1"] == custom_object_uuid

        # third call, args, first arg
        employee_update = execute_calls[2][0][0]
        update_query = employee_update.compile(dialect=sqlalchemy_pg.dialect())

        assert str(update_query) == (
            "UPDATE subject SET related_custom_object_id=%(related_custom_object_id)s WHERE subject.uuid = %(uuid_1)s::UUID"
        )
        assert update_query.params["related_custom_object_id"] == 42
        assert update_query.params["uuid_1"] == str(contact_uuid)

    def test_set_related_object_employee_clear(self):
        custom_object_uuid = None
        contact_uuid = uuid4()

        employee_row = mock.MagicMock()
        employee_row.configure_mock(
            uuid=contact_uuid,
            name="beheerder",
            first_name="beheerder",
            surname="beheerder",
            title=None,
            contact_information={},
            active=True,
            role=[
                {
                    "uuid": uuid4(),
                    "name": "Role",
                    "parent": {"uuid": uuid4(), "name": "Testdepartment"},
                }
            ],
            department={"uuid": uuid4(), "name": "Testdepartment"},
            related_custom_object_uuid=None,
            summary="beheerder",
        )

        self.session.execute().fetchone.side_effect = [employee_row]
        self.session.execute.reset_mock()

        self.cmd.set_subject_related_custom_object(
            custom_object_uuid=custom_object_uuid,
            employee_uuid=str(contact_uuid),
        )

        execute_calls = self.session.execute.call_args_list
        assert len(execute_calls) == 2

        # first call, args, first arg
        employee_select = execute_calls[0][0][0]
        employee_query = employee_select.compile(
            dialect=sqlalchemy_pg.dialect()
        )

        assert str(employee_query) == (
            "SELECT subject.uuid, subject.username AS name, coalesce(CAST(subject.properties AS JSON) -> %(param_1)s, NULL) AS first_name, coalesce(CAST(subject.properties AS JSON) -> %(param_2)s, NULL) AS surname, coalesce(CAST(subject.properties AS JSON) -> %(param_3)s, NULL) AS title, json_build_object(%(json_build_object_1)s, coalesce(CAST(subject.properties AS JSON) -> %(param_4)s, NULL), %(json_build_object_2)s, coalesce(CAST(subject.properties AS JSON) -> %(param_5)s, NULL)) AS contact_information, (SELECT coalesce(user_entity.active, %(coalesce_2)s) AS coalesce_1 \n"
            "FROM user_entity \n"
            "WHERE user_entity.subject_id = subject.id \n"
            " LIMIT %(param_6)s) AS active, (SELECT json_build_object(%(json_build_object_4)s, groups.uuid, %(json_build_object_5)s, groups.name) AS json_build_object_3 \n"
            "FROM groups \n"
            "WHERE groups.id = subject.group_ids[%(group_ids_1)s]) AS department, array((SELECT json_build_object(%(json_build_object_7)s, roles.uuid, %(json_build_object_8)s, roles.name, %(json_build_object_9)s, json_build_object(%(json_build_object_10)s, groups_1.uuid, %(json_build_object_11)s, groups_1.name)) AS json_build_object_6 \n"
            "FROM subject AS subject_1 JOIN LATERAL json_array_elements_text(array_to_json(subject_1.role_ids)) AS role_id ON subject_1.uuid = subject.uuid JOIN roles ON roles.id = CAST(role_id AS INTEGER) JOIN groups AS groups_1 ON groups_1.id = roles.parent_group_id)) AS roles, custom_object.uuid AS related_custom_object_uuid, CAST(subject.properties AS JSON) ->> %(param_7)s AS summary \n"
            "FROM subject LEFT OUTER JOIN custom_object ON subject.related_custom_object_id = custom_object.id \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID AND subject.subject_type = %(subject_type_1)s"
        )
        assert employee_query.params["uuid_1"] == contact_uuid

        # second call, args, first arg
        employee_update = execute_calls[1][0][0]
        update_query = employee_update.compile(dialect=sqlalchemy_pg.dialect())

        assert str(update_query) == (
            "UPDATE subject SET related_custom_object_id=%(related_custom_object_id)s WHERE subject.uuid = %(uuid_1)s::UUID"
        )
        assert update_query.params["related_custom_object_id"] is None
        assert update_query.params["uuid_1"] == str(contact_uuid)

    def test_set_related_object_employee_unknown_object(self):
        custom_object_uuid = str(uuid4())
        contact_uuid = uuid4()

        employee_row = mock.MagicMock()
        employee_row.configure_mock(
            uuid=contact_uuid,
            name="beheerder",
            first_name="beheerder",
            surname="beheerder",
            title=None,
            contact_information={},
            active=True,
            role=[
                {
                    "uuid": uuid4(),
                    "name": "Role",
                    "parent": {"uuid": uuid4(), "name": "Testdepartment"},
                }
            ],
            department={"uuid": uuid4(), "name": "Testdepartment"},
            related_custom_object_uuid=None,
            preferred_contact_channel="pip",
            summary="beheerder",
        )

        self.session.execute().fetchone.side_effect = [
            employee_row,
            None,
        ]
        self.session.execute.reset_mock()

        with pytest.raises(NotFound):
            self.cmd.set_subject_related_custom_object(
                custom_object_uuid=custom_object_uuid,
                employee_uuid=str(contact_uuid),
            )

        execute_calls = self.session.execute.call_args_list
        assert len(execute_calls) == 2


class TestAs_An_User_I_Want_To_Log_When_Bsn_Is_Retrieved(
    minty.cqrs.test.TestBase
):
    def setup_method(self):
        self.load_command_instance(case_management)
        self.cmd.user_info = minty.cqrs.UserInfo(
            user_uuid=uuid4(),
            permissions={"view_sensitive_contact_data": True},
        )

    def test_set_related_object_person(self):
        subject_uuid = str(uuid4())
        contact_uuid = uuid4()

        person_row = mock.MagicMock()
        person_row.configure_mock(
            uuid=contact_uuid,
            authenticated=True,
            authenticatedby=None,
            first_names=None,
            insertions=None,
            family_name=None,
            surname="beheerder",
            initials="J",
            noble_title=None,
            inside_municipality=False,
            address={
                "street": "street",
                "street_number": "23",
                "zipcode": "1568GE",
                "city": "City",
                "country_code": 6030,
                "bag_id": None,
            },
            landcode=6030,
            correspondence_address=None,
            related_custom_object_uuid=None,
            preferred_contact_channel="pip",
            summary="beheerder",
            external_identifier=None,
        )

        self.session.execute().fetchone.side_effect = [person_row]
        self.session.execute.reset_mock()

        self.cmd.create_log_for_bsn_retrieved(subject_uuid=subject_uuid)

        self.assert_has_event_name("BsnRetrieved")

    def test_create_log_for_bsn_retieved_user_has_no_rights(self):
        subject_uuid = str(uuid4())
        self.cmd.user_info = minty.cqrs.UserInfo(
            user_uuid=uuid4(),
            permissions={"view_sensitive_contact_data": False},
        )

        with pytest.raises(Forbidden) as excinfo:
            self.cmd.create_log_for_bsn_retrieved(subject_uuid=subject_uuid)

        assert excinfo.value.args == (
            "You don't have enough rights to log bsn retrieved entry",
            "contact/log_entry_for_bsn_retrieved/permission_denied",
        )


class TestAs_An_User_I_Want_To_Get_Sensitive_Peerson_Data(
    minty.cqrs.test.TestBase
):
    def setup_method(self):
        self.load_query_instance(case_management)
        self.qry.user_info = minty.cqrs.UserInfo(
            user_uuid=uuid4(),
            permissions={"view_sensitive_contact_data": True},
        )

    def test_get_person_sensitive_data(self):
        uuid = uuid4()
        person_row = mock.MagicMock()
        person_row.configure_mock(
            uuid=uuid, personal_number=12345, sedula_number="1231231231"
        )

        self.session.execute().fetchone.side_effect = [person_row]
        self.session.execute.reset_mock()

        self.qry.get_person_sensitive_data(
            uuid=str(uuid), sensitive_data="sedula_number"
        )
        execute_calls = self.session.execute.call_args_list

        assert len(execute_calls) == 1

        person_info_select = execute_calls[0][0][0]
        person_query = person_info_select.compile(
            dialect=sqlalchemy_pg.dialect()
        )

        assert str(person_query) == (
            "SELECT natuurlijk_persoon.uuid, natuurlijk_persoon.burgerservicenummer AS personal_number, natuurlijk_persoon.persoonsnummer AS sedula_number \n"
            "FROM natuurlijk_persoon LEFT OUTER JOIN subject ON subject.uuid = natuurlijk_persoon.uuid \n"
            "WHERE natuurlijk_persoon.deleted_on IS NULL AND natuurlijk_persoon.uuid = %(uuid_1)s::UUID"
        )

    def test_get_person_sensitive_data_by_choise(self):
        uuid = uuid4()
        person_row = mock.MagicMock()
        person_row.configure_mock(
            uuid=uuid, personal_number=12345, sedula_number="1231231231"
        )

        self.session.execute().fetchone.side_effect = [person_row]
        self.session.execute.reset_mock()
        result = self.qry.get_person_sensitive_data(
            uuid=str(uuid), sensitive_data="personal_number"
        )
        assert result.personal_number == "000012345"

        self.session.execute().fetchone.side_effect = [person_row]
        self.session.execute.reset_mock()
        result = self.qry.get_person_sensitive_data(
            uuid=str(uuid), sensitive_data="sedula_number"
        )
        assert result.sedula_number == "1231231231"

    def test_get_person_sensitive_data_user_has_no_rights(self):
        uuid = uuid4()
        self.qry.user_info = minty.cqrs.UserInfo(
            user_uuid=uuid4(),
            permissions={"view_sensitive_contact_data": False},
        )

        with pytest.raises(Forbidden) as excinfo:
            self.qry.get_person_sensitive_data(
                uuid=str(uuid), sensitive_data="personal_number"
            )

        assert excinfo.value.args == (
            "You don't have enough rights to get sensitive information for the given person",
            "contact/get_person_sensitive_data/premission_denied",
        )

    def test_get_person_sensitive_data_not_found(self):
        uuid = uuid4()

        self.session.execute().fetchone.return_value = None
        with pytest.raises(NotFound) as excinfo:
            self.qry.get_person_sensitive_data(
                uuid=str(uuid), sensitive_data="sedula_number"
            )

        assert excinfo.value.args == (
            f"Person with uuid '{uuid}' not found.",
            "person/not_found",
        )


class TestAs_An_admin_I_want_to_save_contact_info_for_a_contact(
    minty.cqrs.test.TestBase
):
    def setup_method(self):
        self.load_command_instance(case_management)

    def test_save_contact_info_person(self):
        user_info = mock.MagicMock()
        user_info = minty.cqrs.UserInfo(
            user_uuid=str(uuid4()),
            permissions={"admin": False},
        )
        self.cmd.user_info = user_info
        # custom_object_uuid = str(uuid4())
        contact_uuid = uuid4()

        person_row = mock.MagicMock()
        person_row.configure_mock(
            uuid=contact_uuid,
            authenticated=True,
            authenticatedby=None,
            first_names=None,
            insertions=None,
            family_name=None,
            surname="beheerder",
            initials="J",
            noble_title=None,
            inside_municipality=False,
            address={
                "street": "street",
                "street_number": "23",
                "zipcode": "1568GE",
                "city": "City",
                "country_code": 6030,
                "bag_id": None,
            },
            landcode=6030,
            correspondence_address=None,
            related_custom_object_uuid=None,
            preferred_contact_channel="pip",
            summary="beheerder",
            indicatie_geheim="3",
            external_identifier=None,
        )

        subject_propeties_row = [{}]

        search_term_data = mock.MagicMock()
        search_term_data._mapping = {
            "initials": person_row.initials,
            "address.street": "street",
            "address.street_number": "23",
            "address.zipcode": "1568GE",
            "address.city": "City",
        }

        contact_data_row = mock.Mock()
        contact_data_row.id = 2

        self.session.execute().fetchone.side_effect = [
            person_row,
            subject_propeties_row,
            search_term_data,
            contact_data_row,
        ]
        self.session.execute.reset_mock()
        self.cmd.save_contact_information(
            contact_information={
                "email": "test@test.com",
                "internal_note": "note",
                "preferred_contact_channel": "pip",
                "is_anonymous_contact": False,
            },
            type="person",
            uuid=str(contact_uuid),
        )

        execute_calls = self.session.execute.call_args_list
        assert len(execute_calls) == 7

        # first call, args, first arg
        person_select = execute_calls[0][0][0]
        person_query = person_select.compile(dialect=sqlalchemy_pg.dialect())

        assert str(person_query) == (
            "SELECT natuurlijk_persoon.uuid, natuurlijk_persoon.authenticated, natuurlijk_persoon.authenticatedby, natuurlijk_persoon.voornamen AS first_names, natuurlijk_persoon.voorletters AS initials, natuurlijk_persoon.voorvoegsel AS insertions, natuurlijk_persoon.geslachtsnaam AS family_name, natuurlijk_persoon.adellijke_titel AS noble_title, natuurlijk_persoon.surname AS surname, natuurlijk_persoon.geboortedatum AS date_of_birth, natuurlijk_persoon.datum_overlijden AS date_of_death, natuurlijk_persoon.geslachtsaanduiding AS gender, natuurlijk_persoon.in_gemeente AS inside_municipality, natuurlijk_persoon.landcode, natuurlijk_persoon.preferred_contact_channel, natuurlijk_persoon.active AS active, (SELECT json_build_object(%(json_build_object_2)s, contact_data.email, %(json_build_object_3)s, contact_data.telefoonnummer, %(json_build_object_4)s, contact_data.mobiel, %(json_build_object_5)s, contact_data.note) AS json_build_object_1 \n"
            "FROM contact_data \n"
            "WHERE contact_data.gegevens_magazijn_id = natuurlijk_persoon.id AND contact_data.betrokkene_type = %(betrokkene_type_1)s) AS contact_information, (SELECT CASE WHEN (adres_1.straatnaam != %(straatnaam_1)s AND (adres_1.landcode = %(landcode_1)s OR adres_1.landcode = %(landcode_2)s)) THEN json_build_object(%(json_build_object_6)s, adres_1.straatnaam, %(json_build_object_7)s, adres_1.postcode, %(json_build_object_8)s, adres_1.huisnummer, %(json_build_object_9)s, adres_1.huisletter, %(json_build_object_10)s, adres_1.huisnummertoevoeging, %(json_build_object_11)s, adres_1.woonplaats, %(json_build_object_12)s, adres_1.landcode, %(json_build_object_13)s, adres_1.bag_id, %(json_build_object_14)s, %(json_build_object_15)s, %(json_build_object_16)s, adres_1.geo_lat_long) ELSE json_build_object(%(json_build_object_17)s, adres_1.landcode, %(json_build_object_18)s, adres_1.adres_buitenland1, %(json_build_object_19)s, adres_1.adres_buitenland2, %(json_build_object_20)s, adres_1.adres_buitenland3, %(json_build_object_21)s, NULL, %(json_build_object_22)s, %(json_build_object_23)s, %(json_build_object_24)s, adres_1.geo_lat_long) END AS anon_1 \n"
            "FROM adres AS adres_1 \n"
            "WHERE adres_1.natuurlijk_persoon_id = natuurlijk_persoon.id AND adres_1.functie_adres = %(functie_adres_1)s) AS address, (SELECT CASE WHEN (adres_2.straatnaam != %(straatnaam_2)s AND (adres_2.landcode = %(landcode_3)s OR adres_2.landcode = %(landcode_4)s)) THEN json_build_object(%(json_build_object_25)s, adres_2.straatnaam, %(json_build_object_26)s, adres_2.postcode, %(json_build_object_27)s, adres_2.huisnummer, %(json_build_object_28)s, adres_2.huisletter, %(json_build_object_29)s, adres_2.huisnummertoevoeging, %(json_build_object_30)s, adres_2.woonplaats, %(json_build_object_31)s, adres_2.landcode, %(json_build_object_32)s, adres_2.bag_id, %(json_build_object_33)s, %(json_build_object_34)s, %(json_build_object_35)s, adres_2.geo_lat_long) ELSE json_build_object(%(json_build_object_36)s, adres_2.landcode, %(json_build_object_37)s, adres_2.adres_buitenland1, %(json_build_object_38)s, adres_2.adres_buitenland2, %(json_build_object_39)s, adres_2.adres_buitenland3, %(json_build_object_40)s, NULL, %(json_build_object_41)s, %(json_build_object_42)s, %(json_build_object_43)s, adres_2.geo_lat_long) END AS anon_2 \n"
            "FROM adres AS adres_2 \n"
            "WHERE adres_2.natuurlijk_persoon_id = natuurlijk_persoon.id AND adres_2.functie_adres = %(functie_adres_2)s) AS correspondence_address, natuurlijk_persoon.indicatie_geheim AS indicatie_geheim, custom_object.uuid AS related_custom_object_uuid, subject.properties, concat(natuurlijk_persoon.adellijke_titel || %(adellijke_titel_1)s, natuurlijk_persoon.voorletters || %(voorletters_1)s, natuurlijk_persoon.surname) AS summary, object_subscription.external_id AS external_identifier \n"
            "FROM natuurlijk_persoon LEFT OUTER JOIN custom_object ON natuurlijk_persoon.related_custom_object_id = custom_object.id LEFT OUTER JOIN subject ON subject.uuid = natuurlijk_persoon.uuid AND subject.subject_type = %(subject_type_1)s LEFT OUTER JOIN object_subscription ON natuurlijk_persoon.id = CAST(object_subscription.local_id AS INTEGER) AND object_subscription.local_table = %(local_table_1)s AND object_subscription.date_deleted IS NULL \n"
            "WHERE natuurlijk_persoon.deleted_on IS NULL AND natuurlijk_persoon.uuid = %(uuid_1)s::UUID"
        )
        assert person_query.params["uuid_1"] == contact_uuid

        # second call, args, first arg
        person_properties_select = execute_calls[1][0][0]
        person_properties_query = person_properties_select.compile(
            dialect=sqlalchemy_pg.dialect()
        )

        assert str(person_properties_query) == (
            "SELECT subject.properties \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID AND subject.subject_type = %(subject_type_1)s"
        )
        assert person_properties_query.params["uuid_1"] == str(contact_uuid)

        # third call, args, first arg
        subject_property_update = execute_calls[2][0][0]
        subject_property_update_query = subject_property_update.compile(
            dialect=sqlalchemy_pg.dialect()
        )

        assert str(subject_property_update_query) == (
            "UPDATE subject SET properties=%(properties)s FROM natuurlijk_persoon WHERE natuurlijk_persoon.uuid = %(uuid_1)s::UUID AND subject.uuid = natuurlijk_persoon.uuid AND subject.subject_type = %(subject_type_1)s"
        )
        assert subject_property_update_query.params["properties"] == {
            "anonymous": "0"
        }
        assert subject_property_update_query.params["uuid_1"] == str(
            contact_uuid
        )

        # third call, search terms query
        search_term_query = execute_calls[3][0][0]
        search_term_query_compiled = search_term_query.compile(
            dialect=sqlalchemy_pg.dialect()
        )
        assert str(search_term_query_compiled) == (
            'SELECT natuurlijk_persoon.voornamen AS first_names, natuurlijk_persoon.voorletters AS initials, natuurlijk_persoon.voorvoegsel AS insertions, natuurlijk_persoon.geslachtsnaam AS family_name, natuurlijk_persoon.burgerservicenummer AS burgerservicenummer, natuurlijk_persoon.persoonsnummer AS person_number, natuurlijk_persoon.geboortedatum AS date_of_birth, natuurlijk_persoon.datum_overlijden AS date_of_death, adres.straatnaam AS "address.street", adres.huisnummer AS "address.street_number", adres.huisletter AS "address.street_number_letter", adres.woonplaats AS "address.city", adres.postcode AS "address.zipcode", adres_1.straatnaam AS "correspondence_address.street", adres_1.huisnummer AS "correspondence_address.street_number", adres_1.huisletter AS "correspondence_address.street_number_letter", adres_1.woonplaats AS "correspondence_address.city", adres_1.postcode AS "correspondence_address.zipcode", contact_data.email AS "contact_information.email" \n'
            "FROM natuurlijk_persoon LEFT OUTER JOIN adres ON natuurlijk_persoon.adres_id = adres.id LEFT OUTER JOIN adres AS adres_1 ON natuurlijk_persoon.id = adres_1.natuurlijk_persoon_id AND adres_1.functie_adres = %(functie_adres_1)s JOIN contact_data ON contact_data.gegevens_magazijn_id = natuurlijk_persoon.id AND contact_data.betrokkene_type = %(betrokkene_type_1)s \n"
            "WHERE natuurlijk_persoon.uuid = %(uuid_1)s::UUID"
        )

        # fourth call, args, first arg
        preferred_contact_channel_update = execute_calls[4][0][0]
        preferred_contact_channel_update_query = (
            preferred_contact_channel_update.compile(
                dialect=sqlalchemy_pg.dialect()
            )
        )
        assert str(preferred_contact_channel_update_query) == (
            "UPDATE natuurlijk_persoon SET search_term=%(search_term)s, preferred_contact_channel=%(preferred_contact_channel)s WHERE natuurlijk_persoon.uuid = %(uuid_1)s::UUID"
        )
        assert (
            preferred_contact_channel_update_query.params[
                "preferred_contact_channel"
            ]
            == "pip"
        )
        assert preferred_contact_channel_update_query.params["uuid_1"] == str(
            contact_uuid
        )
        assert preferred_contact_channel_update_query.params[
            "search_term"
        ] == str("J street 23 1568GE City test@test.com")

        # sixth call, args, first arg
        contact_data_update = execute_calls[6][0][0]
        contact_data_update_query = contact_data_update.compile(
            dialect=sqlalchemy_pg.dialect()
        )

        assert str(contact_data_update_query) == (
            "UPDATE contact_data SET mobiel=%(mobiel)s, telefoonnummer=%(telefoonnummer)s, email=%(email)s, last_modified=%(last_modified)s, note=%(note)s FROM natuurlijk_persoon WHERE natuurlijk_persoon.uuid = %(uuid_1)s::UUID AND contact_data.gegevens_magazijn_id = natuurlijk_persoon.id AND contact_data.betrokkene_type = %(betrokkene_type_1)s"
        )
        assert contact_data_update_query.params["email"] == "test@test.com"
        assert contact_data_update_query.params["uuid_1"] == str(contact_uuid)
        assert contact_data_update_query.params["last_modified"] is None

    def test_save_contact_info_person_by_inserting_contact_data_and_subject_properties(
        self,
    ):
        user_info = mock.MagicMock()
        user_info = minty.cqrs.UserInfo(
            user_uuid=str(uuid4()),
            permissions={"admin": True},
        )
        self.cmd.user_info = user_info
        # custom_object_uuid = str(uuid4())
        contact_uuid = uuid4()

        person_row = mock.MagicMock()
        person_row.configure_mock(
            uuid=contact_uuid,
            authenticated=True,
            authenticatedby=None,
            first_names=None,
            insertions=None,
            family_name=None,
            surname="beheerder",
            initials="J",
            noble_title=None,
            inside_municipality=False,
            address={
                "street": "street",
                "street_number": "23",
                "zipcode": "1568GE",
                "city": "City",
                "country_code": 6030,
                "bag_id": "1234567890123456",
            },
            landcode=6030,
            correspondence_address=None,
            related_custom_object_uuid=None,
            preferred_contact_channel="pip",
            summary="beheerder",
            indicatie_geheim=None,
            external_identifier=None,
        )

        self.session.execute().fetchone.side_effect = [
            person_row,
            None,
            mock.MagicMock(),
            None,
        ]
        self.session.execute.reset_mock()

        self.cmd.save_contact_information(
            contact_information={
                "email": "test@test.com",
                "internal_note": "note",
                "preferred_contact_channel": "pip",
                "is_anonymous_contact": False,
            },
            type="person",
            uuid=str(contact_uuid),
        )

        execute_calls = self.session.execute.call_args_list
        assert len(execute_calls) == 7

        # first call, args, first arg
        person_select = execute_calls[0][0][0]
        person_query = person_select.compile(dialect=sqlalchemy_pg.dialect())

        assert str(person_query) == (
            "SELECT natuurlijk_persoon.uuid, natuurlijk_persoon.authenticated, natuurlijk_persoon.authenticatedby, natuurlijk_persoon.voornamen AS first_names, natuurlijk_persoon.voorletters AS initials, natuurlijk_persoon.voorvoegsel AS insertions, natuurlijk_persoon.geslachtsnaam AS family_name, natuurlijk_persoon.adellijke_titel AS noble_title, natuurlijk_persoon.surname AS surname, natuurlijk_persoon.geboortedatum AS date_of_birth, natuurlijk_persoon.datum_overlijden AS date_of_death, natuurlijk_persoon.geslachtsaanduiding AS gender, natuurlijk_persoon.in_gemeente AS inside_municipality, natuurlijk_persoon.landcode, natuurlijk_persoon.preferred_contact_channel, natuurlijk_persoon.active AS active, (SELECT json_build_object(%(json_build_object_2)s, contact_data.email, %(json_build_object_3)s, contact_data.telefoonnummer, %(json_build_object_4)s, contact_data.mobiel, %(json_build_object_5)s, contact_data.note) AS json_build_object_1 \n"
            "FROM contact_data \n"
            "WHERE contact_data.gegevens_magazijn_id = natuurlijk_persoon.id AND contact_data.betrokkene_type = %(betrokkene_type_1)s) AS contact_information, (SELECT CASE WHEN (adres_1.straatnaam != %(straatnaam_1)s AND (adres_1.landcode = %(landcode_1)s OR adres_1.landcode = %(landcode_2)s)) THEN json_build_object(%(json_build_object_6)s, adres_1.straatnaam, %(json_build_object_7)s, adres_1.postcode, %(json_build_object_8)s, adres_1.huisnummer, %(json_build_object_9)s, adres_1.huisletter, %(json_build_object_10)s, adres_1.huisnummertoevoeging, %(json_build_object_11)s, adres_1.woonplaats, %(json_build_object_12)s, adres_1.landcode, %(json_build_object_13)s, adres_1.bag_id, %(json_build_object_14)s, %(json_build_object_15)s, %(json_build_object_16)s, adres_1.geo_lat_long) ELSE json_build_object(%(json_build_object_17)s, adres_1.landcode, %(json_build_object_18)s, adres_1.adres_buitenland1, %(json_build_object_19)s, adres_1.adres_buitenland2, %(json_build_object_20)s, adres_1.adres_buitenland3, %(json_build_object_21)s, NULL, %(json_build_object_22)s, %(json_build_object_23)s, %(json_build_object_24)s, adres_1.geo_lat_long) END AS anon_1 \n"
            "FROM adres AS adres_1 \n"
            "WHERE adres_1.natuurlijk_persoon_id = natuurlijk_persoon.id AND adres_1.functie_adres = %(functie_adres_1)s) AS address, (SELECT CASE WHEN (adres_2.straatnaam != %(straatnaam_2)s AND (adres_2.landcode = %(landcode_3)s OR adres_2.landcode = %(landcode_4)s)) THEN json_build_object(%(json_build_object_25)s, adres_2.straatnaam, %(json_build_object_26)s, adres_2.postcode, %(json_build_object_27)s, adres_2.huisnummer, %(json_build_object_28)s, adres_2.huisletter, %(json_build_object_29)s, adres_2.huisnummertoevoeging, %(json_build_object_30)s, adres_2.woonplaats, %(json_build_object_31)s, adres_2.landcode, %(json_build_object_32)s, adres_2.bag_id, %(json_build_object_33)s, %(json_build_object_34)s, %(json_build_object_35)s, adres_2.geo_lat_long) ELSE json_build_object(%(json_build_object_36)s, adres_2.landcode, %(json_build_object_37)s, adres_2.adres_buitenland1, %(json_build_object_38)s, adres_2.adres_buitenland2, %(json_build_object_39)s, adres_2.adres_buitenland3, %(json_build_object_40)s, NULL, %(json_build_object_41)s, %(json_build_object_42)s, %(json_build_object_43)s, adres_2.geo_lat_long) END AS anon_2 \n"
            "FROM adres AS adres_2 \n"
            "WHERE adres_2.natuurlijk_persoon_id = natuurlijk_persoon.id AND adres_2.functie_adres = %(functie_adres_2)s) AS correspondence_address, natuurlijk_persoon.indicatie_geheim AS indicatie_geheim, custom_object.uuid AS related_custom_object_uuid, subject.properties, concat(natuurlijk_persoon.adellijke_titel || %(adellijke_titel_1)s, natuurlijk_persoon.voorletters || %(voorletters_1)s, natuurlijk_persoon.surname) AS summary, object_subscription.external_id AS external_identifier \n"
            "FROM natuurlijk_persoon LEFT OUTER JOIN custom_object ON natuurlijk_persoon.related_custom_object_id = custom_object.id LEFT OUTER JOIN subject ON subject.uuid = natuurlijk_persoon.uuid AND subject.subject_type = %(subject_type_1)s LEFT OUTER JOIN object_subscription ON natuurlijk_persoon.id = CAST(object_subscription.local_id AS INTEGER) AND object_subscription.local_table = %(local_table_1)s AND object_subscription.date_deleted IS NULL \n"
            "WHERE natuurlijk_persoon.deleted_on IS NULL AND natuurlijk_persoon.uuid = %(uuid_1)s::UUID"
        )
        assert person_query.params["uuid_1"] == contact_uuid

        # second call, args, first arg
        person_properties_select = execute_calls[1][0][0]
        person_properties_query = person_properties_select.compile(
            dialect=sqlalchemy_pg.dialect()
        )

        assert str(person_properties_query) == (
            "SELECT subject.properties \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID AND subject.subject_type = %(subject_type_1)s"
        )
        assert person_properties_query.params["uuid_1"] == str(contact_uuid)

        # third call, args, first arg
        subject_property_insert = execute_calls[2][0][0]
        subject_property_insert_query = subject_property_insert.compile(
            dialect=sqlalchemy_pg.dialect()
        )

        assert str(subject_property_insert_query) == (
            "INSERT INTO subject (uuid, subject_type, username, properties, nobody, system) VALUES (%(uuid)s::UUID, %(subject_type)s, %(username)s, %(properties)s, %(nobody)s, %(system)s) RETURNING subject.id"
        )
        assert subject_property_insert_query.params["properties"] == {
            "anonymous": "0"
        }
        assert subject_property_insert_query.params["uuid"] == str(
            contact_uuid
        )

        # fifth call, args, first arg
        contact_data_insert = execute_calls[6][0][0]
        contact_data_insert_query = contact_data_insert.compile(
            dialect=sqlalchemy_pg.dialect()
        )

        assert str(contact_data_insert_query) == (
            "INSERT INTO contact_data (gegevens_magazijn_id, betrokkene_type, mobiel, telefoonnummer, email, note) VALUES ((SELECT natuurlijk_persoon.id \n"
            "FROM natuurlijk_persoon \n"
            "WHERE natuurlijk_persoon.uuid = %(uuid_1)s::UUID), %(betrokkene_type)s, %(mobiel)s, %(telefoonnummer)s, %(email)s, %(note)s) RETURNING contact_data.id"
        )
        assert contact_data_insert_query.params["email"] == "test@test.com"
        assert contact_data_insert_query.params["uuid_1"] == str(contact_uuid)

    def test_save_contact_info_organization(self):
        user_info = minty.cqrs.UserInfo(
            user_uuid=str(uuid4()),
            permissions={"admin": False},
        )
        self.cmd.user_info = user_info

        contact_uuid = uuid4()

        organization_row = mock.Mock()
        organization_row.configure_mock(
            uuid=contact_uuid,
            authenticated=True,
            authenticatedby="Intern",
            name="Company Name",
            coc_number="12345678",
            coc_location_number="123456789012",
            date_founded=None,
            date_registered=None,
            date_ceased=None,
            rsin=None,
            oin=None,
            main_activity=None,
            secondary_activities=[],
            business_entity_code=1,  # Eenmanszaak
            location_address=None,
            correspondence_address=None,
            contact_information={},
            related_custom_object_uuid=uuid4(),
            preferred_contact_channel="pip",
            contact_person_info={},
            summary="Conpany Name",
        )

        contact_data_row = mock.Mock()
        contact_data_row.id = 2

        self.session.execute().fetchone.side_effect = [
            organization_row,
            contact_data_row,
        ]
        self.session.execute.reset_mock()

        self.cmd.save_contact_information(
            contact_information={
                "email": "test@test.com",
                "internal_note": "note",
                "preferred_contact_channel": "pip",
                "is_anonymous_contact": False,
            },
            uuid=str(contact_uuid),
            type="organization",
        )

        execute_calls = self.session.execute.call_args_list
        assert len(execute_calls) == 6

        # first call, args, first arg
        person_select = execute_calls[0][0][0]
        organization_query = person_select.compile(
            dialect=sqlalchemy_pg.dialect()
        )
        assert str(organization_query) == (
            "SELECT bedrijf.uuid, bedrijf.authenticated, bedrijf.authenticatedby, bedrijf.handelsnaam AS name, bedrijf.date_founded, bedrijf.date_registration AS date_registered, bedrijf.date_ceased, bedrijf.rsin, bedrijf.oin, bedrijf.dossiernummer AS coc_number, bedrijf.vestigingsnummer AS coc_location_number, bedrijf.rechtsvorm AS business_entity_code, bedrijf.main_activity, bedrijf.secondairy_activities AS secondary_activities, bedrijf.preferred_contact_channel, json_build_object(%(json_build_object_1)s, contact_data.email, %(json_build_object_2)s, contact_data.telefoonnummer, %(json_build_object_3)s, contact_data.mobiel, %(json_build_object_4)s, contact_data.note, %(json_build_object_5)s, bedrijf.preferred_contact_channel, %(json_build_object_6)s, CASE WHEN (subject.properties IS NOT NULL) THEN CAST(CAST(subject.properties AS JSONB) ->> %(param_1)s AS BOOLEAN) ELSE %(param_2)s END) AS contact_information, CASE WHEN (coalesce(bedrijf.vestiging_adres_buitenland1, bedrijf.vestiging_adres_buitenland2, bedrijf.vestiging_adres_buitenland3, bedrijf.vestiging_straatnaam, %(coalesce_1)s) = %(coalesce_2)s) THEN NULL WHEN (bedrijf.vestiging_straatnaam != %(vestiging_straatnaam_1)s AND (bedrijf.vestiging_landcode = %(vestiging_landcode_1)s OR bedrijf.vestiging_landcode = %(vestiging_landcode_2)s)) THEN CAST(json_build_object(%(json_build_object_7)s, bedrijf.vestiging_straatnaam, %(json_build_object_8)s, bedrijf.vestiging_postcode, %(json_build_object_9)s, bedrijf.vestiging_huisnummer, %(json_build_object_10)s, bedrijf.vestiging_huisletter, %(json_build_object_11)s, bedrijf.vestiging_huisnummertoevoeging, %(json_build_object_12)s, bedrijf.vestiging_woonplaats, %(json_build_object_13)s, bedrijf.vestiging_landcode, %(json_build_object_14)s, bedrijf.vestiging_bag_id, %(json_build_object_15)s, %(json_build_object_16)s, %(json_build_object_17)s, bedrijf.vestiging_latlong) AS JSON) ELSE CAST(json_build_object(%(json_build_object_18)s, bedrijf.vestiging_adres_buitenland1, %(json_build_object_19)s, bedrijf.vestiging_adres_buitenland2, %(json_build_object_20)s, bedrijf.vestiging_adres_buitenland3, %(json_build_object_21)s, bedrijf.vestiging_landcode, %(json_build_object_22)s, NULL, %(json_build_object_23)s, %(json_build_object_24)s, %(json_build_object_25)s, bedrijf.vestiging_latlong) AS JSON) END AS location_address, CASE WHEN (coalesce(bedrijf.correspondentie_adres_buitenland1, bedrijf.correspondentie_adres_buitenland2, bedrijf.correspondentie_adres_buitenland3, bedrijf.correspondentie_straatnaam, %(coalesce_3)s) = %(coalesce_4)s) THEN NULL WHEN (bedrijf.correspondentie_straatnaam != %(correspondentie_straatnaam_1)s AND (bedrijf.correspondentie_landcode = %(correspondentie_landcode_1)s OR bedrijf.correspondentie_landcode = %(correspondentie_landcode_2)s)) THEN CAST(json_build_object(%(json_build_object_26)s, bedrijf.correspondentie_straatnaam, %(json_build_object_27)s, bedrijf.correspondentie_postcode, %(json_build_object_28)s, bedrijf.correspondentie_huisnummer, %(json_build_object_29)s, bedrijf.correspondentie_huisletter, %(json_build_object_30)s, bedrijf.correspondentie_huisnummertoevoeging, %(json_build_object_31)s, bedrijf.correspondentie_woonplaats, %(json_build_object_32)s, bedrijf.correspondentie_landcode, %(json_build_object_33)s, %(json_build_object_34)s) AS JSON) ELSE CAST(json_build_object(%(json_build_object_35)s, bedrijf.correspondentie_adres_buitenland1, %(json_build_object_36)s, bedrijf.correspondentie_adres_buitenland2, %(json_build_object_37)s, bedrijf.correspondentie_adres_buitenland3, %(json_build_object_38)s, bedrijf.correspondentie_landcode, %(json_build_object_39)s, %(json_build_object_40)s) AS JSON) END AS correspondence_address, custom_object.uuid AS related_custom_object_uuid, json_build_object(%(json_build_object_41)s, bedrijf.contact_voorletters, %(json_build_object_42)s, bedrijf.contact_voorvoegsel, %(json_build_object_43)s, bedrijf.contact_geslachtsnaam, %(json_build_object_44)s, bedrijf.contact_naam) AS contact_person_info, bedrijf.handelsnaam AS summary \n"
            "FROM bedrijf LEFT OUTER JOIN custom_object ON bedrijf.related_custom_object_id = custom_object.id LEFT OUTER JOIN contact_data ON contact_data.gegevens_magazijn_id = bedrijf.id AND contact_data.betrokkene_type = %(betrokkene_type_1)s LEFT OUTER JOIN subject ON subject.uuid = bedrijf.uuid AND subject.subject_type = %(subject_type_1)s \n"
            "WHERE bedrijf.deleted_on IS NULL AND bedrijf.uuid = %(uuid_1)s::UUID"
        )
        assert organization_query.params["uuid_1"] == contact_uuid

        # second call, args, first arg
        anonymous_update = execute_calls[2][0][0]
        anonymous_update_query = anonymous_update.compile(
            dialect=sqlalchemy_pg.dialect()
        )

        assert str(anonymous_update_query) == (
            "UPDATE subject SET properties=%(properties)s FROM bedrijf WHERE bedrijf.uuid = %(uuid_1)s::UUID AND subject.uuid = bedrijf.uuid AND subject.subject_type = %(subject_type_1)s"
        )
        assert anonymous_update_query.params["properties"] == {
            "anonymous": "0"
        }
        assert anonymous_update_query.params["uuid_1"] == str(contact_uuid)

        preferred_contact_channel_update = execute_calls[3][0][0]
        preferred_contact_channel_update_query = (
            preferred_contact_channel_update.compile(
                dialect=sqlalchemy_pg.dialect()
            )
        )
        assert str(preferred_contact_channel_update_query) == (
            "UPDATE bedrijf SET preferred_contact_channel=%(preferred_contact_channel)s WHERE bedrijf.uuid = %(uuid_1)s::UUID"
        )
        assert (
            preferred_contact_channel_update_query.params[
                "preferred_contact_channel"
            ]
            == "pip"
        )
        assert preferred_contact_channel_update_query.params["uuid_1"] == str(
            contact_uuid
        )

        # fourth call, args, first arg
        contact_data_update = execute_calls[5][0][0]
        contact_data_update_query = contact_data_update.compile(
            dialect=sqlalchemy_pg.dialect()
        )

        assert str(contact_data_update_query) == (
            "UPDATE contact_data SET mobiel=%(mobiel)s, telefoonnummer=%(telefoonnummer)s, email=%(email)s, last_modified=%(last_modified)s, note=%(note)s FROM bedrijf WHERE bedrijf.uuid = %(uuid_1)s::UUID AND contact_data.gegevens_magazijn_id = bedrijf.id AND contact_data.betrokkene_type = %(betrokkene_type_1)s"
        )
        assert contact_data_update_query.params["email"] == "test@test.com"
        assert contact_data_update_query.params["uuid_1"] == str(contact_uuid)

    def test_save_contact_info_organization_by_inserting_contact_data(self):
        user_info = mock.MagicMock()
        user_info = minty.cqrs.UserInfo(
            user_uuid="d3887763-675c-4988-9347-c5e1448fd20d",
            permissions={"admin": False},
        )
        self.cmd.user_info = user_info

        contact_uuid = uuid4()

        organization_row = mock.Mock()
        organization_row.configure_mock(
            uuid=contact_uuid,
            authenticated=True,
            authenticatedby="Intern",
            name="Company Name",
            coc_number="12345678",
            coc_location_number="123456789012",
            date_founded=None,
            date_registered=None,
            date_ceased=None,
            rsin=None,
            oin=None,
            main_activity=None,
            secondary_activities=[],
            business_entity_code=1,  # Eenmanszaak
            location_address=None,
            correspondence_address=None,
            contact_information={},
            related_custom_object_uuid=uuid4(),
            preferred_contact_channel="pip",
            contact_person_info={},
            summary="Company Name",
        )

        self.session.execute().fetchone.side_effect = [
            organization_row,
            None,
        ]
        self.session.execute().scalar_one_or_none.side_effect = [None]
        self.session.execute.reset_mock()

        self.cmd.save_contact_information(
            contact_information={
                "email": "test@test.com",
                "internal_note": "note",
                "preferred_contact_channel": "pip",
                "is_anonymous_contact": False,
            },
            uuid=str(contact_uuid),
            type="organization",
        )

        execute_calls = self.session.execute.call_args_list
        assert len(execute_calls) == 6

        # first call, args, first arg
        person_select = execute_calls[0][0][0]
        organization_query = person_select.compile(
            dialect=sqlalchemy_pg.dialect()
        )
        assert str(organization_query) == (
            "SELECT bedrijf.uuid, bedrijf.authenticated, bedrijf.authenticatedby, bedrijf.handelsnaam AS name, bedrijf.date_founded, bedrijf.date_registration AS date_registered, bedrijf.date_ceased, bedrijf.rsin, bedrijf.oin, bedrijf.dossiernummer AS coc_number, bedrijf.vestigingsnummer AS coc_location_number, bedrijf.rechtsvorm AS business_entity_code, bedrijf.main_activity, bedrijf.secondairy_activities AS secondary_activities, bedrijf.preferred_contact_channel, json_build_object(%(json_build_object_1)s, contact_data.email, %(json_build_object_2)s, contact_data.telefoonnummer, %(json_build_object_3)s, contact_data.mobiel, %(json_build_object_4)s, contact_data.note, %(json_build_object_5)s, bedrijf.preferred_contact_channel, %(json_build_object_6)s, CASE WHEN (subject.properties IS NOT NULL) THEN CAST(CAST(subject.properties AS JSONB) ->> %(param_1)s AS BOOLEAN) ELSE %(param_2)s END) AS contact_information, CASE WHEN (coalesce(bedrijf.vestiging_adres_buitenland1, bedrijf.vestiging_adres_buitenland2, bedrijf.vestiging_adres_buitenland3, bedrijf.vestiging_straatnaam, %(coalesce_1)s) = %(coalesce_2)s) THEN NULL WHEN (bedrijf.vestiging_straatnaam != %(vestiging_straatnaam_1)s AND (bedrijf.vestiging_landcode = %(vestiging_landcode_1)s OR bedrijf.vestiging_landcode = %(vestiging_landcode_2)s)) THEN CAST(json_build_object(%(json_build_object_7)s, bedrijf.vestiging_straatnaam, %(json_build_object_8)s, bedrijf.vestiging_postcode, %(json_build_object_9)s, bedrijf.vestiging_huisnummer, %(json_build_object_10)s, bedrijf.vestiging_huisletter, %(json_build_object_11)s, bedrijf.vestiging_huisnummertoevoeging, %(json_build_object_12)s, bedrijf.vestiging_woonplaats, %(json_build_object_13)s, bedrijf.vestiging_landcode, %(json_build_object_14)s, bedrijf.vestiging_bag_id, %(json_build_object_15)s, %(json_build_object_16)s, %(json_build_object_17)s, bedrijf.vestiging_latlong) AS JSON) ELSE CAST(json_build_object(%(json_build_object_18)s, bedrijf.vestiging_adres_buitenland1, %(json_build_object_19)s, bedrijf.vestiging_adres_buitenland2, %(json_build_object_20)s, bedrijf.vestiging_adres_buitenland3, %(json_build_object_21)s, bedrijf.vestiging_landcode, %(json_build_object_22)s, NULL, %(json_build_object_23)s, %(json_build_object_24)s, %(json_build_object_25)s, bedrijf.vestiging_latlong) AS JSON) END AS location_address, CASE WHEN (coalesce(bedrijf.correspondentie_adres_buitenland1, bedrijf.correspondentie_adres_buitenland2, bedrijf.correspondentie_adres_buitenland3, bedrijf.correspondentie_straatnaam, %(coalesce_3)s) = %(coalesce_4)s) THEN NULL WHEN (bedrijf.correspondentie_straatnaam != %(correspondentie_straatnaam_1)s AND (bedrijf.correspondentie_landcode = %(correspondentie_landcode_1)s OR bedrijf.correspondentie_landcode = %(correspondentie_landcode_2)s)) THEN CAST(json_build_object(%(json_build_object_26)s, bedrijf.correspondentie_straatnaam, %(json_build_object_27)s, bedrijf.correspondentie_postcode, %(json_build_object_28)s, bedrijf.correspondentie_huisnummer, %(json_build_object_29)s, bedrijf.correspondentie_huisletter, %(json_build_object_30)s, bedrijf.correspondentie_huisnummertoevoeging, %(json_build_object_31)s, bedrijf.correspondentie_woonplaats, %(json_build_object_32)s, bedrijf.correspondentie_landcode, %(json_build_object_33)s, %(json_build_object_34)s) AS JSON) ELSE CAST(json_build_object(%(json_build_object_35)s, bedrijf.correspondentie_adres_buitenland1, %(json_build_object_36)s, bedrijf.correspondentie_adres_buitenland2, %(json_build_object_37)s, bedrijf.correspondentie_adres_buitenland3, %(json_build_object_38)s, bedrijf.correspondentie_landcode, %(json_build_object_39)s, %(json_build_object_40)s) AS JSON) END AS correspondence_address, custom_object.uuid AS related_custom_object_uuid, json_build_object(%(json_build_object_41)s, bedrijf.contact_voorletters, %(json_build_object_42)s, bedrijf.contact_voorvoegsel, %(json_build_object_43)s, bedrijf.contact_geslachtsnaam, %(json_build_object_44)s, bedrijf.contact_naam) AS contact_person_info, bedrijf.handelsnaam AS summary \n"
            "FROM bedrijf LEFT OUTER JOIN custom_object ON bedrijf.related_custom_object_id = custom_object.id LEFT OUTER JOIN contact_data ON contact_data.gegevens_magazijn_id = bedrijf.id AND contact_data.betrokkene_type = %(betrokkene_type_1)s LEFT OUTER JOIN subject ON subject.uuid = bedrijf.uuid AND subject.subject_type = %(subject_type_1)s \n"
            "WHERE bedrijf.deleted_on IS NULL AND bedrijf.uuid = %(uuid_1)s::UUID"
        )
        assert organization_query.params["uuid_1"] == contact_uuid

        # Third call
        subject_insert = execute_calls[2][0][0]
        subject_insert_query = subject_insert.compile(
            dialect=sqlalchemy_pg.dialect()
        )

        assert str(subject_insert_query) == (
            "INSERT INTO subject (uuid, subject_type, username, properties, nobody, system) VALUES (%(uuid)s::UUID, %(subject_type)s, %(username)s, %(properties)s, %(nobody)s, %(system)s) RETURNING subject.id"
        )
        assert subject_insert_query.params["uuid"] == str(contact_uuid)
        assert subject_insert_query.params["properties"] == {"anonymous": "0"}

        # 4th call
        preferred_contact_channel_update = execute_calls[3][0][0]
        preferred_contact_channel_update_query = (
            preferred_contact_channel_update.compile(
                dialect=sqlalchemy_pg.dialect()
            )
        )

        assert str(preferred_contact_channel_update_query) == (
            "UPDATE bedrijf SET preferred_contact_channel=%(preferred_contact_channel)s WHERE bedrijf.uuid = %(uuid_1)s::UUID"
        )
        assert (
            preferred_contact_channel_update_query.params[
                "preferred_contact_channel"
            ]
            == "pip"
        )
        assert preferred_contact_channel_update_query.params["uuid_1"] == str(
            contact_uuid
        )

        # fourth call, args, first arg
        contact_data_update = execute_calls[5][0][0]
        contact_data_update_query = contact_data_update.compile(
            dialect=sqlalchemy_pg.dialect()
        )

        assert str(contact_data_update_query) == (
            "INSERT INTO contact_data (gegevens_magazijn_id, betrokkene_type, mobiel, telefoonnummer, email, note) VALUES ((SELECT bedrijf.id \n"
            "FROM bedrijf \n"
            "WHERE bedrijf.uuid = %(uuid_1)s::UUID), %(betrokkene_type)s, %(mobiel)s, %(telefoonnummer)s, %(email)s, %(note)s) RETURNING contact_data.id"
        )
        assert contact_data_update_query.params["email"] == "test@test.com"
        assert contact_data_update_query.params["uuid_1"] == str(contact_uuid)

    def test_allow_admin_to_update_person_is_anonymous(self):
        user_info = mock.MagicMock()
        user_info = minty.cqrs.UserInfo(
            user_uuid=str(uuid4()),
            permissions={"admin": True},
        )
        self.cmd.user_info = user_info
        # custom_object_uuid = str(uuid4())
        contact_uuid = uuid4()

        person_row = mock.MagicMock()
        person_row.configure_mock(
            uuid=contact_uuid,
            authenticated=True,
            authenticatedby=None,
            first_names=None,
            insertions=None,
            family_name=None,
            surname="beheerder",
            initials="J",
            noble_title=None,
            inside_municipality=False,
            address={
                "street": "street",
                "street_number": "23",
                "zipcode": "1568GE",
                "city": "City",
                "country_code": 6030,
                "bag_id": None,
            },
            landcode=6030,
            correspondence_address=None,
            related_custom_object_uuid=None,
            preferred_contact_channel="pip",
            summary="beheerder",
            external_identifier=None,
        )

        subject_propeties_row = [{}]

        contact_data_row = mock.Mock()
        contact_data_row.id = 2

        self.session.execute().fetchone.side_effect = [
            person_row,
            subject_propeties_row,
            mock.MagicMock(),
            contact_data_row,
        ]
        self.session.execute.reset_mock()
        self.cmd.save_contact_information(
            contact_information={
                "email": "test@test.com",
                "internal_note": "note",
                "preferred_contact_channel": "pip",
                "is_anonymous_contact": True,
            },
            type="person",
            uuid=str(contact_uuid),
        )

        execute_calls = self.session.execute.call_args_list
        assert len(execute_calls) == 7

    def test_allow_admin_to_update_organization_is_locked(self):
        user_info = mock.MagicMock()
        user_info = minty.cqrs.UserInfo(
            user_uuid=str(uuid4()),
            permissions={"admin": True},
        )
        self.cmd.user_info = user_info
        # custom_object_uuid = str(uuid4())
        contact_uuid = uuid4()

        organization_row = mock.Mock()
        organization_row.configure_mock(
            uuid=contact_uuid,
            authenticated=True,
            authenticatedby="Intern",
            name="Company Name",
            coc_number="12345678",
            coc_location_number="123456789012",
            date_founded=None,
            date_registered=None,
            date_ceased=None,
            rsin=None,
            oin=None,
            main_activity={"code": 123, "description": "Something"},
            secondary_activities=[{"code": 31337, "description": "Test"}],
            business_entity_code=1,  # Eenmanszaak
            location_address=None,
            correspondence_address=None,
            contact_information={},
            related_custom_object_uuid=None,
            preferred_contact_channel="pip",
            contact_person_info={},
            summary="Company Name",
        )

        subject_properties_row = [{}]

        contact_data_row = mock.Mock()
        contact_data_row.id = 2

        self.session.execute().fetchone.side_effect = [
            organization_row,
            subject_properties_row,
            mock.MagicMock(),
            contact_data_row,
        ]
        self.session.execute().scalar_one_or_none.side_effect = [None]
        self.session.execute.reset_mock()
        self.cmd.save_contact_information(
            contact_information={
                "email": "test@test.com",
                "internal_note": "note",
                "preferred_contact_channel": "pip",
                "is_anonymous_contact": True,
            },
            type="organization",
            uuid=str(contact_uuid),
        )

        execute_calls = self.session.execute.call_args_list
        assert len(execute_calls) == 6

        insert_subject = execute_calls[2][0][0]
        insert_subject_compiled = insert_subject.compile(
            dialect=sqlalchemy_pg.dialect()
        )
        assert (
            str(insert_subject_compiled)
            == "INSERT INTO subject (uuid, subject_type, username, properties, nobody, system) VALUES (%(uuid)s::UUID, %(subject_type)s, %(username)s, %(properties)s, %(nobody)s, %(system)s) RETURNING subject.id"
        )
        assert insert_subject_compiled.params["properties"] == {
            "anonymous": "1"
        }
        assert insert_subject_compiled.params["uuid"] == str(contact_uuid)


class Test_ContactInformation(minty.cqrs.test.TestBase):
    def setup_method(self):
        self.load_command_instance(case_management)

        event_service = EventService(
            correlation_id="correlation_id",
            domain="domain",
            context="context",
            user_uuid=uuid4(),
        )
        self.user = entities.Person(
            uuid=uuid4(),
            authenticated=True,
            name="Name",
            surname="Surname",
            gender="F",
            has_valid_address=True,
            is_active=True,
            contact_information=ContactInformation(is_anonymous_contact=False),
            _event_service=event_service,
        )

    def test_contact_info_update(self):
        user_info = minty.cqrs.UserInfo(
            user_uuid=uuid4(),
            permissions={"admin": False},
        )
        self.user.save_contact_information(
            ContactInformation(
                email="aa@aa.com",
                phone_number="+46737310455",
                mobile_number="+46737310455",
                internal_note="",
                is_anonymous_contact=False,
            ),
            user_info,
        )
        assert self.user.contact_information.is_anonymous_contact is False
        assert self.user.contact_information.email == "aa@aa.com"

    def test_conflict_when_updating_anonymous_person(self):
        user_info = minty.cqrs.UserInfo(
            user_uuid=uuid4(),
            permissions={"admin": True},
        )
        self.user.save_contact_information(
            ContactInformation(
                email="aa@aa.com",
                phone_number="+46737310455",
                mobile_number="+46737310455",
                internal_note="",
                is_anonymous_contact=True,
            ),
            user_info,
        )
        user_info = minty.cqrs.UserInfo(
            user_uuid=uuid4(),
            permissions={"admin": False},
        )
        with pytest.raises(Conflict) as excinfo:
            self.user.save_contact_information(
                ContactInformation(
                    email="aa@aa.com",
                    phone_number="+46737310456",
                    mobile_number="+46737310455",
                    internal_note="",
                    is_anonymous_contact=True,
                ),
                user_info,
            )
        assert excinfo.value.args == (
            "Can not update details of anonymous person",
            "person/update_anonymous_not_allowed",
        )

    def test_allow_admin_update_anonymous(self):
        user_info = minty.cqrs.UserInfo(
            user_uuid=uuid4(),
            permissions={"admin": True},
        )
        self.user.save_contact_information(
            ContactInformation(
                email="aa@aa.com",
                phone_number="+46737310455",
                mobile_number="+46737310455",
                internal_note="",
                is_anonymous_contact=True,
            ),
            user_info,
        )
        assert self.user.contact_information.is_anonymous_contact is True

    def test_raise_conflict_non_admin_update_anonymous(self):
        user_info = minty.cqrs.UserInfo(
            user_uuid=uuid4(),
            permissions={"admin": False},
        )
        with pytest.raises(Forbidden) as excinfo:
            self.user.save_contact_information(
                ContactInformation(
                    email="aa@aa.com",
                    phone_number="+46737310456",
                    mobile_number="+46737310455",
                    internal_note="",
                    is_anonymous_contact=True,
                ),
                user_info,
            )
        assert excinfo.value.args == (
            "Only Admin can set the contact as anonymous.",
            "person/set_anonymous_not_allowed",
        )

    def test_update_locked_organization_as_admin(self):
        contact_uuid = uuid4()

        organization_row = mock.Mock()
        organization_row.configure_mock(
            uuid=contact_uuid,
            authenticated=True,
            authenticatedby="Intern",
            name="Company Name",
            coc_number="12345678",
            coc_location_number="123456789012",
            date_founded=None,
            date_registered=None,
            date_ceased=None,
            rsin=None,
            oin=None,
            main_activity=None,
            secondary_activities=[],
            business_entity_code=1,  # Eenmanszaak
            location_address=None,
            correspondence_address=None,
            contact_information={"is_anonymous_contact": True},
            related_custom_object_uuid=uuid4(),
            preferred_contact_channel="pip",
            contact_person_info={},
            summary="Conpany Name",
        )

        contact_data_row = mock.Mock()
        contact_data_row.id = 2

        self.session.execute().fetchone.side_effect = [
            organization_row,
            contact_data_row,
        ]
        self.session.execute.reset_mock()

        user_info = minty.cqrs.UserInfo(
            user_uuid=str(uuid4()),
            permissions={"admin": True},
        )
        self.cmd.user_info = user_info

        with pytest.raises(minty.exceptions.Conflict):
            self.cmd.save_contact_information(
                contact_information={
                    "email": "test@test.com",
                    "internal_note": "note",
                    "preferred_contact_channel": "pip",
                    "is_anonymous_contact": True,
                },
                uuid=str(contact_uuid),
                type="organization",
            )

    def test_update_non_admin_unlock_organization(self):
        contact_uuid = uuid4()

        organization_row = mock.Mock()
        organization_row.configure_mock(
            uuid=contact_uuid,
            authenticated=True,
            authenticatedby="Intern",
            name="Company Name",
            coc_number="12345678",
            coc_location_number="123456789012",
            date_founded=None,
            date_registered=None,
            date_ceased=None,
            rsin=None,
            oin=None,
            main_activity=None,
            secondary_activities=[],
            business_entity_code=1,  # Eenmanszaak
            location_address=None,
            correspondence_address=None,
            contact_information={"is_anonymous_contact": True},
            related_custom_object_uuid=uuid4(),
            preferred_contact_channel="pip",
            contact_person_info={},
            summary="Conpany Name",
        )

        contact_data_row = mock.Mock()
        contact_data_row.id = 2

        self.session.execute().fetchone.side_effect = [
            organization_row,
            contact_data_row,
        ]
        self.session.execute.reset_mock()

        user_info = minty.cqrs.UserInfo(
            user_uuid=str(uuid4()),
            permissions={"gebruiker": True},
        )
        self.cmd.user_info = user_info

        with pytest.raises(minty.exceptions.Forbidden):
            self.cmd.save_contact_information(
                contact_information={
                    "email": "test@test.com",
                    "internal_note": "note",
                    "preferred_contact_channel": "pip",
                    "is_anonymous_contact": False,
                },
                uuid=str(contact_uuid),
                type="organization",
            )

    def test_update_admin_unlock_organization(self):
        contact_uuid = uuid4()

        organization_row = mock.Mock()
        organization_row.configure_mock(
            uuid=contact_uuid,
            authenticated=True,
            authenticatedby="Intern",
            name="Company Name",
            coc_number="12345678",
            coc_location_number="123456789012",
            date_founded=None,
            date_registered=None,
            date_ceased=None,
            rsin=None,
            oin=None,
            main_activity=None,
            secondary_activities=[],
            business_entity_code=1,  # Eenmanszaak
            location_address=None,
            correspondence_address=None,
            contact_information={"is_anonymous_contact": True},
            related_custom_object_uuid=uuid4(),
            preferred_contact_channel="pip",
            contact_person_info={},
            summary="Conpany Name",
        )

        contact_data_row = mock.Mock()
        contact_data_row.id = 2

        self.session.execute().fetchone.side_effect = [
            organization_row,
            contact_data_row,
        ]
        self.session.execute.reset_mock()

        user_info = minty.cqrs.UserInfo(
            user_uuid=str(uuid4()),
            permissions={"admin": True},
        )
        self.cmd.user_info = user_info

        # Admin can unlock
        self.cmd.save_contact_information(
            contact_information={
                "email": "test@test.com",
                "internal_note": "note",
                "preferred_contact_channel": "pip",
                "is_anonymous_contact": False,
            },
            uuid=str(contact_uuid),
            type="organization",
        )


class TestUpdateNonAuthenticPerson(minty.cqrs.test.TestBase):
    def setup_method(self):
        self.load_command_instance(case_management)
        self.cmd.user_info = minty.cqrs.UserInfo(
            user_uuid=uuid4(),
            permissions={"admin": True, "contact_nieuw": True},
        )

    def test_update_non_authentic_person(self):
        mock_db_row_person = mock.MagicMock(name="PersonDBRow")
        mock_db_row_person.configure_mock(
            id=987,
            uuid=uuid4(),
            authenticated=False,
            authenticatedby=None,
            first_names="Piet",
            initials="P",
            insertions="",
            family_name="Testpersoon",
            noble_title=None,
            surname="Testpersoon",
            date_of_birth="2000-01-01T00:00:00Z",
            date_of_death=None,
            gender="X",
            inside_municipality=True,
            landcode=6030,
            preferred_contact_channel="pip",
            active=True,
            contact_information={},
            address={
                "street": "Test",
                "zipcode": "1111AA",
                "street_number": 100,
                "street_number_letter": None,
                "street_number_suffix": None,
                "city": "Some Town",
                "country_code": 6030,
                "bag_id": None,
                "is_foreign": False,
            },
            correspondence_address=None,
            indicatie_geheim=" ",
            related_custom_object_uuid=None,
            properties=None,
            summary="Some kind of name",
            external_identifier=None,
        )
        self.session.execute().fetchone.return_value = mock_db_row_person

        self.session.execute.reset_mock()

        self.cmd.update_non_authentic_contact(
            data={
                "first_name": "test",
                "family_name": "test",
                "gender": "X",
                "inside_municipality": True,
                "surname_prefix": "Prefix",
                "noble_title": "Noble title",
                "country_code": "6030",
                "mailing_address": True,
            },
            address={
                "landcode": 6030,
                "country_code": 6030,
                "street": "Test Two",
                "street_number": 100,
                "street_number_letter": "",
                "street_number_suffix": "",
                "city": "Some Town",
                "country": "Nederland",
            },
            uuid=mock_db_row_person.uuid,
            type="person",
        )

        calls = self.session.execute.call_args_list
        assert len(calls) == 6

        # Find person by UUID
        compiled_1 = calls[0][0][0].compile(dialect=sqlalchemy_pg.dialect())
        assert str(compiled_1) == (
            "SELECT natuurlijk_persoon.uuid, natuurlijk_persoon.authenticated, natuurlijk_persoon.authenticatedby, natuurlijk_persoon.voornamen AS first_names, natuurlijk_persoon.voorletters AS initials, natuurlijk_persoon.voorvoegsel AS insertions, natuurlijk_persoon.geslachtsnaam AS family_name, natuurlijk_persoon.adellijke_titel AS noble_title, natuurlijk_persoon.surname AS surname, natuurlijk_persoon.geboortedatum AS date_of_birth, natuurlijk_persoon.datum_overlijden AS date_of_death, natuurlijk_persoon.geslachtsaanduiding AS gender, natuurlijk_persoon.in_gemeente AS inside_municipality, natuurlijk_persoon.landcode, natuurlijk_persoon.preferred_contact_channel, natuurlijk_persoon.active AS active, (SELECT json_build_object(%(json_build_object_2)s, contact_data.email, %(json_build_object_3)s, contact_data.telefoonnummer, %(json_build_object_4)s, contact_data.mobiel, %(json_build_object_5)s, contact_data.note) AS json_build_object_1 \n"
            "FROM contact_data \n"
            "WHERE contact_data.gegevens_magazijn_id = natuurlijk_persoon.id AND contact_data.betrokkene_type = %(betrokkene_type_1)s) AS contact_information, (SELECT CASE WHEN (adres_1.straatnaam != %(straatnaam_1)s AND (adres_1.landcode = %(landcode_1)s OR adres_1.landcode = %(landcode_2)s)) THEN json_build_object(%(json_build_object_6)s, adres_1.straatnaam, %(json_build_object_7)s, adres_1.postcode, %(json_build_object_8)s, adres_1.huisnummer, %(json_build_object_9)s, adres_1.huisletter, %(json_build_object_10)s, adres_1.huisnummertoevoeging, %(json_build_object_11)s, adres_1.woonplaats, %(json_build_object_12)s, adres_1.landcode, %(json_build_object_13)s, adres_1.bag_id, %(json_build_object_14)s, %(json_build_object_15)s, %(json_build_object_16)s, adres_1.geo_lat_long) ELSE json_build_object(%(json_build_object_17)s, adres_1.landcode, %(json_build_object_18)s, adres_1.adres_buitenland1, %(json_build_object_19)s, adres_1.adres_buitenland2, %(json_build_object_20)s, adres_1.adres_buitenland3, %(json_build_object_21)s, NULL, %(json_build_object_22)s, %(json_build_object_23)s, %(json_build_object_24)s, adres_1.geo_lat_long) END AS anon_1 \n"
            "FROM adres AS adres_1 \n"
            "WHERE adres_1.natuurlijk_persoon_id = natuurlijk_persoon.id AND adres_1.functie_adres = %(functie_adres_1)s) AS address, (SELECT CASE WHEN (adres_2.straatnaam != %(straatnaam_2)s AND (adres_2.landcode = %(landcode_3)s OR adres_2.landcode = %(landcode_4)s)) THEN json_build_object(%(json_build_object_25)s, adres_2.straatnaam, %(json_build_object_26)s, adres_2.postcode, %(json_build_object_27)s, adres_2.huisnummer, %(json_build_object_28)s, adres_2.huisletter, %(json_build_object_29)s, adres_2.huisnummertoevoeging, %(json_build_object_30)s, adres_2.woonplaats, %(json_build_object_31)s, adres_2.landcode, %(json_build_object_32)s, adres_2.bag_id, %(json_build_object_33)s, %(json_build_object_34)s, %(json_build_object_35)s, adres_2.geo_lat_long) ELSE json_build_object(%(json_build_object_36)s, adres_2.landcode, %(json_build_object_37)s, adres_2.adres_buitenland1, %(json_build_object_38)s, adres_2.adres_buitenland2, %(json_build_object_39)s, adres_2.adres_buitenland3, %(json_build_object_40)s, NULL, %(json_build_object_41)s, %(json_build_object_42)s, %(json_build_object_43)s, adres_2.geo_lat_long) END AS anon_2 \n"
            "FROM adres AS adres_2 \n"
            "WHERE adres_2.natuurlijk_persoon_id = natuurlijk_persoon.id AND adres_2.functie_adres = %(functie_adres_2)s) AS correspondence_address, natuurlijk_persoon.indicatie_geheim AS indicatie_geheim, custom_object.uuid AS related_custom_object_uuid, subject.properties, concat(natuurlijk_persoon.adellijke_titel || %(adellijke_titel_1)s, natuurlijk_persoon.voorletters || %(voorletters_1)s, natuurlijk_persoon.surname) AS summary, object_subscription.external_id AS external_identifier \n"
            "FROM natuurlijk_persoon LEFT OUTER JOIN custom_object ON natuurlijk_persoon.related_custom_object_id = custom_object.id LEFT OUTER JOIN subject ON subject.uuid = natuurlijk_persoon.uuid AND subject.subject_type = %(subject_type_1)s LEFT OUTER JOIN object_subscription ON natuurlijk_persoon.id = CAST(object_subscription.local_id AS INTEGER) AND object_subscription.local_table = %(local_table_1)s AND object_subscription.date_deleted IS NULL \n"
            "WHERE natuurlijk_persoon.deleted_on IS NULL AND natuurlijk_persoon.uuid = %(uuid_1)s::UUID"
        )
        assert compiled_1.params == {
            "adellijke_titel_1": " ",
            "betrokkene_type_1": 1,
            "functie_adres_1": "W",
            "functie_adres_2": "B",
            "json_build_object_10": "street_number_suffix",
            "json_build_object_11": "city",
            "json_build_object_12": "country_code",
            "json_build_object_13": "bag_id",
            "json_build_object_14": "is_foreign",
            "json_build_object_15": False,
            "json_build_object_16": "geo_lat_long",
            "json_build_object_17": "country_code",
            "json_build_object_18": "address_line_1",
            "json_build_object_19": "address_line_2",
            "json_build_object_2": "email",
            "json_build_object_20": "address_line_3",
            "json_build_object_21": "bag_id",
            "json_build_object_22": "is_foreign",
            "json_build_object_23": True,
            "json_build_object_24": "geo_lat_long",
            "json_build_object_25": "street",
            "json_build_object_26": "zipcode",
            "json_build_object_27": "street_number",
            "json_build_object_28": "street_number_letter",
            "json_build_object_29": "street_number_suffix",
            "json_build_object_3": "phone_number",
            "json_build_object_30": "city",
            "json_build_object_31": "country_code",
            "json_build_object_32": "bag_id",
            "json_build_object_33": "is_foreign",
            "json_build_object_34": False,
            "json_build_object_35": "geo_lat_long",
            "json_build_object_36": "country_code",
            "json_build_object_37": "address_line_1",
            "json_build_object_38": "address_line_2",
            "json_build_object_39": "address_line_3",
            "json_build_object_4": "mobile_number",
            "json_build_object_5": "internal_note",
            "json_build_object_6": "street",
            "json_build_object_7": "zipcode",
            "json_build_object_8": "street_number",
            "json_build_object_9": "street_number_letter",
            "json_build_object_40": "bag_id",
            "json_build_object_41": "is_foreign",
            "json_build_object_42": True,
            "json_build_object_43": "geo_lat_long",
            "landcode_1": 5107,
            "landcode_2": 6030,
            "landcode_3": 5107,
            "landcode_4": 6030,
            "local_table_1": "NatuurlijkPersoon",
            "uuid_1": mock_db_row_person.uuid,
            "voorletters_1": " ",
            "subject_type_1": "person",
            "straatnaam_1": "",
            "straatnaam_2": "",
        }

        # Part of update: find id for natuurlijk persoon uuid
        compiled_2 = calls[1][0][0].compile(dialect=sqlalchemy_pg.dialect())
        assert str(compiled_2) == (
            "SELECT natuurlijk_persoon.id \n"
            "FROM natuurlijk_persoon \n"
            "WHERE natuurlijk_persoon.uuid = %(uuid_1)s::UUID"
        )
        assert compiled_2.params == {"uuid_1": str(mock_db_row_person.uuid)}

        compiled_3 = calls[2][0][0].compile(dialect=sqlalchemy_pg.dialect())
        assert str(compiled_3) == (
            "INSERT INTO adres (straatnaam, huisnummer, huisletter, huisnummertoevoeging, postcode, woonplaats, functie_adres, landcode, natuurlijk_persoon_id) VALUES (%(straatnaam)s, %(huisnummer)s, %(huisletter)s, %(huisnummertoevoeging)s, %(postcode)s, %(woonplaats)s, %(functie_adres)s, %(landcode)s, %(natuurlijk_persoon_id)s) RETURNING adres.id"
        )
        assert compiled_3.params == {
            "natuurlijk_persoon_id": 987,
            "functie_adres": "W",
            "straatnaam": "Test Two",
            "huisnummer": 100,
            "huisletter": None,
            "huisnummertoevoeging": "",
            "postcode": None,
            "woonplaats": "Some Town",
            "landcode": 6030,
        }

        compiled_5 = calls[4][0][0].compile(dialect=sqlalchemy_pg.dialect())
        assert str(compiled_5) == (
            "UPDATE natuurlijk_persoon SET search_term=%(search_term)s, search_order=%(search_order)s, voorletters=%(voorletters)s, voornamen=%(voornamen)s, geslachtsnaam=%(geslachtsnaam)s, voorvoegsel=%(voorvoegsel)s, geslachtsaanduiding=%(geslachtsaanduiding)s, adres_id=%(adres_id)s, in_gemeente=%(in_gemeente)s, landcode=%(landcode)s, naamgebruik=%(naamgebruik)s, adellijke_titel=%(adellijke_titel)s WHERE natuurlijk_persoon.uuid = %(uuid_1)s::UUID"
        )
        assert compiled_5.params == {
            "adellijke_titel": "Noble title",
            "adres_id": self.session.execute().inserted_primary_key.__getitem__(),
            "geslachtsaanduiding": "X",
            "geslachtsnaam": "test",
            "in_gemeente": True,
            "naamgebruik": "Prefix test",
            "search_term": "test t. Prefix test Test Two 100 Some Town",
            "search_order": "test t. Prefix test Test Two 100 Some Town",
            "uuid_1": str(mock_db_row_person.uuid),
            "voorletters": "t.",
            "voornamen": "test",
            "voorvoegsel": "Prefix",
            "landcode": 6030,
        }

        compiled_6 = calls[5][0][0].compile(
            dialect=sqlalchemy_pg.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_6) == (
            "DELETE FROM adres WHERE adres.natuurlijk_persoon_id = %(natuurlijk_persoon_id_1)s AND (adres.id NOT IN (%(id_1_1)s))"
        )
        assert compiled_6.params == {
            "id_1_1": self.session.execute().inserted_primary_key.__getitem__(),
            "natuurlijk_persoon_id_1": 987,
        }


class TestUpdateNonAuthenticUser(minty.cqrs.test.TestBase):
    def setup_method(self):
        self.mock_repo_factory = mock.MagicMock()
        self.load_command_instance(case_management)
        event_service = EventService(
            correlation_id="correlation_id",
            domain="domain",
            context="context",
            user_uuid=uuid4(),
        )
        self.user_uuid = uuid4()
        self.cmd.user_info = minty.cqrs.UserInfo(
            user_uuid=self.user_uuid,
            permissions={"admin": True, "contact_nieuw": True},
        )
        self.user = entities.Person(
            uuid=self.user_uuid,
            name="Name",
            surname="Surname",
            gender="F",
            has_valid_address=True,
            is_active=True,
            contact_information=ContactInformation(is_anonymous_contact=False),
            _event_service=event_service,
            authenticated=False,
            sedula_number=None,
        )

    def test_contact_info_update(self):
        self.user.update_non_authentic(
            first_name="test",
            family_name="test",
            gender="F",
            inside_municipality=True,
            address=None,
            surname_prefix="",
            noble_title="",
            correspondence_address=None,
        )

        assert self.user.first_names == "test"

    def test_update_na_contact_authenticated(self):
        self.user.authenticated = True
        with pytest.raises(Conflict) as excinfo:
            self.user.update_non_authentic(
                first_name="test",
                family_name="test",
                gender="V",
                inside_municipality=True,
                address=None,
                surname_prefix="",
                noble_title="",
                correspondence_address=None,
            )
        assert excinfo.value.args == ("Can not update Authenticated Contact",)

    def test_update_bsn_na_contact_authenticated(self):
        self.user.authenticated = True
        with pytest.raises(Conflict) as excinfo:
            self.user.update_bsn_non_authentic(bsn="112233")
        assert excinfo.value.args == ("Can not update Authenticated Contact",)

    def test_update_sedula_number_na_contact_authenticated(self):
        self.user.authenticated = True
        self.user.country_code = "5107"
        with pytest.raises(Conflict) as excinfo:
            self.user.update_sedula_number_non_authentic(
                sedula_number="1122334455"
            )
        assert excinfo.value.args == ("Can not update Authenticated Contact",)

    def test_update_sedula_number_na_contact_not_Curacao(self):
        self.user.country_code = "1122"
        with pytest.raises(Conflict) as excinfo:
            self.user.update_sedula_number_non_authentic(
                sedula_number="1122334455"
            )
        assert excinfo.value.args == ("Sedula Number only valid for Curaçao",)

    def test_update_sedula_number_na(self):
        self.user.authenticated = False
        self.user.country_code = "5107"
        self.user.update_sedula_number_non_authentic(
            sedula_number="1122334455"
        )
        assert self.user.sedula_number == "1122334455"

    def test_contact_invalid_bsn(self):
        with pytest.raises(ValueError):
            self.user.update_bsn_non_authentic(bsn="11")

        with pytest.raises(ValueError):
            self.user.update_bsn_non_authentic(bsn="232262537")

        with pytest.raises(ValueError):
            self.user.update_bsn_non_authentic(bsn="23226253711")

    def test_contact_invalid_sedula_number(self):
        self.user.country_code = "5107"
        with pytest.raises(ValueError):
            self.user.update_sedula_number_non_authentic(sedula_number="11")

    @mock.patch.object(PersonRepository, "find_person_by_uuid")
    def test_na_contact_update_no_address(self, mock_find_by_uuid):
        mock_find_by_uuid.return_value = self.user
        self.session.execute.reset_mock()

        residence_address = InternationalAddress(
            address_line_1="line", country="Zwitserland"
        )
        self.user.residence_address = residence_address
        self.cmd.update_non_authentic_contact(
            data={
                "bsn": "111222333",
                "first_name": "test",
                "family_name": "test",
                "gender": "F",
                "inside_municipality": True,
                "surname_prefix": "Prefix",
                "noble_title": "Noble title",
                "country_code": "5107",
                "mailing_address": True,
            },
            contact_information={
                "email": "test@1test.com",
                "internal_note": "note",
                "preferred_contact_channel": "pip",
                "is_anonymous_contact": False,
            },
            uuid=str(self.user.uuid),
            type="person",
        )

        mock_find_by_uuid.assert_called_once_with(uuid=self.user.uuid)
        assert self.user.residence_address.address_line_1 == "line"

    @mock.patch.object(PersonRepository, "find_person_by_uuid")
    def test_na_contact_update_sedula_number(self, mock_find_by_uuid):
        self.user.country_code = "5107"
        mock_find_by_uuid.return_value = self.user
        self.session.execute.reset_mock()

        residence_address = InternationalAddress(
            address_line_1="line", country="Curaçao"
        )
        self.user.residence_address = residence_address
        self.cmd.update_non_authentic_contact(
            data={
                "bsn": "111222333",
                "first_name": "test",
                "family_name": "test",
                "gender": "F",
                "inside_municipality": True,
                "surname_prefix": "Prefix",
                "noble_title": "Noble title",
                "country_code": "5107",
                "mailing_address": True,
                "sedula_number": "1122334455",
            },
            contact_information={
                "email": "test1@test.com",
                "internal_note": "note",
                "preferred_contact_channel": "pip",
                "is_anonymous_contact": False,
            },
            uuid=str(self.user.uuid),
            type="person",
        )

        mock_find_by_uuid.assert_called_once_with(uuid=self.user.uuid)
        assert self.user.residence_address.address_line_1 == "line"
        assert self.user.sedula_number == "1122334455"

    @mock.patch.object(PersonRepository, "find_person_by_uuid")
    def test_na_contact_update(self, mock_find_by_uuid):
        mock_find_by_uuid.return_value = self.user
        self.session.execute.reset_mock()
        self.cmd.update_non_authentic_contact(
            data={
                "bsn": "111222333",
                "first_name": "test",
                "family_name": "test",
                "gender": "M",
                "inside_municipality": True,
                "surname_prefix": "Prefix",
                "noble_title": "Noble title",
                "country_code": "6030",
                "mailing_address": True,
            },
            address={
                "street": "Address",
                "street_number": "1",
                "zipcode": "3522AD",
                "city": "Utrecht",
                "country": "Nederland",
            },
            contact_information={
                "email": "test1@test.com",
                "internal_note": "note",
                "preferred_contact_channel": "pip",
                "is_anonymous_contact": False,
            },
            uuid=str(self.user.uuid),
            type="person",
        )

        mock_find_by_uuid.assert_called_once_with(uuid=self.user.uuid)

    @mock.patch(
        "zsnl_domains.case_management.repositories.person.PersonRepository._get_id_of_person_by_uuid"
    )
    @mock.patch(
        "zsnl_domains.case_management.repositories.person.PersonRepository._generate_search_term"
    )
    def test_update_non_authentic_repo(
        self, mock_get_id_of_person_by_uuid, mock_search_term
    ):
        mock_infra = mock.MagicMock()
        mock_infra_ro = mock.MagicMock()
        event_service = EventService(
            correlation_id="correlation_id",
            domain="domain",
            context="context",
            user_uuid=uuid4(),
        )
        person_repo = PersonRepository(
            infrastructure_factory=mock_infra,
            infrastructure_factory_ro=mock_infra_ro,
            context="devtest",
            event_service=event_service,
            read_only=False,
        )
        person_uuid = uuid4()
        event = mock.MagicMock()
        event.entity_id = person_uuid
        event.context = "context"
        event.changes = [
            {"key": "gender", "old_value": None, "new_value": "X"},
            {
                "key": "first_names",
                "old_value": None,
                "new_value": "string",
            },
            {
                "key": "family_name",
                "old_value": "string",
                "new_value": "string",
            },
            {"key": "initials", "old_value": None, "new_value": "s.s"},
            {"key": "has_briefadres", "old_value": None, "new_value": True},
            {
                "key": "inside_municipality",
                "old_value": True,
                "new_value": True,
            },
            {
                "key": "insertions",
                "old_value": "string",
                "new_value": "string",
            },
            {
                "key": "noble_title",
                "old_value": "string",
                "new_value": "string",
            },
            {
                "key": "residence_address",
                "old_value": None,
                "new_value": {
                    "country": "Nederland",
                    "address_line_1": "string",
                    "address_line_2": None,
                    "address_line_3": None,
                    "is_foreign": True,
                },
            },
            {
                "key": "correspondence_address",
                "old_value": None,
                "new_value": {
                    "country": "Nederland",
                    "address_line_1": "string",
                    "address_line_2": None,
                    "address_line_3": None,
                    "is_foreign": True,
                },
            },
        ]
        mock_get_id_of_person_by_uuid.return_value = 1
        mock_search_term.return_value = "Search Term"
        person_repo._update_non_authentic(event, self.cmd.user_info)

    @mock.patch(
        "zsnl_domains.case_management.repositories.person.PersonRepository._get_id_of_person_by_uuid"
    )
    def test_update_bsn_non_authentic_repo(
        self, mock_get_id_of_person_by_uuid
    ):
        mock_infra = mock.MagicMock()
        mock_infra_ro = mock.MagicMock()
        event_service = EventService(
            correlation_id="correlation_id",
            domain="domain",
            context="context",
            user_uuid=uuid4(),
        )
        person_repo = PersonRepository(
            infrastructure_factory=mock_infra,
            infrastructure_factory_ro=mock_infra_ro,
            context="devtest",
            event_service=event_service,
            read_only=False,
        )
        person_uuid = uuid4()
        event = mock.MagicMock()
        event.entity_id = person_uuid
        event.context = "context"
        event.changes = [
            {"key": "bsn", "old_value": None, "new_value": "1111111"}
        ]
        mock_get_id_of_person_by_uuid.return_value = 1
        person_repo._update_bsn_non_authentic(event, self.cmd.user_info)

    @mock.patch(
        "zsnl_domains.case_management.repositories.person.PersonRepository._get_id_of_person_by_uuid"
    )
    def test_update_sedula_number_non_authentic_repo(
        self, mock_get_id_of_person_by_uuid
    ):
        mock_infra = mock.MagicMock()
        mock_infra_ro = mock.MagicMock()
        event_service = EventService(
            correlation_id="correlation_id",
            domain="domain",
            context="context",
            user_uuid=uuid4(),
        )
        person_repo = PersonRepository(
            infrastructure_factory=mock_infra,
            infrastructure_factory_ro=mock_infra_ro,
            context="devtest",
            event_service=event_service,
            read_only=False,
        )
        person_uuid = uuid4()
        event = mock.MagicMock()
        event.entity_id = person_uuid
        event.context = "context"
        event.changes = [
            {
                "key": "sedula_number",
                "old_value": None,
                "new_value": "1111111111",
            }
        ]
        mock_get_id_of_person_by_uuid.return_value = 1
        person_repo._update_sedula_number_non_authentic(
            event, self.cmd.user_info
        )

    def test_na_user_add_dutch_address(self):
        mock_infra = mock.MagicMock()
        mock_infra_ro = mock.MagicMock()
        event_service = EventService(
            correlation_id="correlation_id",
            domain="domain",
            context="context",
            user_uuid=uuid4(),
        )
        person_repo = PersonRepository(
            infrastructure_factory=mock_infra,
            infrastructure_factory_ro=mock_infra_ro,
            context="devtest",
            event_service=event_service,
            read_only=False,
        )
        address = {
            "street": "Address",
            "street_number": "1",
            "zipcode": "3522AD",
            "city": "Utrecht",
            "is_foreign": False,
        }
        person_repo._add_address_of_contact(address, 1, True, "6030")
        (args, kwargs) = person_repo.session.execute.call_args
        query = args[0].compile()
        assert str(query) == (
            "INSERT INTO adres (straatnaam, huisnummer, huisletter, huisnummertoevoeging, postcode, woonplaats, functie_adres, landcode, natuurlijk_persoon_id) VALUES (:straatnaam, :huisnummer, :huisletter, :huisnummertoevoeging, :postcode, :woonplaats, :functie_adres, :landcode, :natuurlijk_persoon_id)"
        )

    def test_na_search_term(self):
        mock_infra = mock.MagicMock()
        mock_infra_ro = mock.MagicMock()
        event_service = EventService(
            correlation_id="correlation_id",
            domain="domain",
            context="context",
            user_uuid=uuid4(),
        )
        person_repo = PersonRepository(
            infrastructure_factory=mock_infra,
            infrastructure_factory_ro=mock_infra_ro,
            context="devtest",
            event_service=event_service,
            read_only=False,
        )
        changes = {
            "first_names": "string",
            "insertions": "Ins",
            "family_name": "Family",
            "address": {
                "address_line_1": "string",
                "address_line_2": None,
                "address_line_3": None,
            },
        }

        result = person_repo._generate_search_term(
            changes, natuurlijk_persoon_uuid=uuid4()
        )
        assert result == "string Ins Family"

    @mock.patch.object(PersonRepository, "find_person_by_uuid")
    def test_na_contact_update_address_errors(self, mock_find_by_uuid):
        mock_find_by_uuid.return_value = self.user
        self.session.execute.reset_mock()
        with pytest.raises(ValueError):
            self.cmd.update_non_authentic_contact(
                data={
                    "bsn": "111222333",
                    "first_name": "test",
                    "family_name": "test",
                    "gender": "M",
                    "inside_municipality": True,
                    "surname_prefix": "Prefix",
                    "noble_title": "Noble title",
                    "country_code": "6030",
                    "mailing_address": True,
                },
                address={
                    "street": "Address",
                    "street_number": "1",
                    "zipcode": "3522AD",
                    "city": "Utrecht",
                    "country": "Nederland",
                    "street_number_letter": "aa",
                },
                uuid=str(self.user.uuid),
                type="person",
            )
        with pytest.raises(ValueError):
            self.cmd.update_non_authentic_contact(
                data={
                    "bsn": "111222333",
                    "first_name": "test",
                    "family_name": "test",
                    "gender": "M",
                    "inside_municipality": True,
                    "surname_prefix": "Prefix",
                    "noble_title": "Noble title",
                    "country_code": "6030",
                    "mailing_address": True,
                },
                address={
                    "street": "Address",
                    "street_number": "1",
                    "zipcode": "3522AD",
                    "city": "Utrecht",
                    "country": "Nederlandeee",
                },
                uuid=str(self.user.uuid),
                type="person",
            )

        with pytest.raises(ValueError):
            self.cmd.update_non_authentic_contact(
                data={
                    "sedula_number": "111222333",
                    "first_name": "test",
                    "family_name": "test",
                    "gender": "M",
                    "inside_municipality": True,
                    "surname_prefix": "Prefix",
                    "noble_title": "Noble title",
                    "country_code": "5107",
                    "mailing_address": True,
                },
                address={
                    "street": "Address",
                    "street_number": "1",
                    "zipcode": "3522AD",
                    "city": "Utrecht",
                    "country": "Neder",
                    "street_number_letter": "a",
                },
                uuid=str(self.user.uuid),
                type="person",
            )
        with pytest.raises(ValueError):
            self.cmd.update_non_authentic_contact(
                data={
                    "sedula_number": "111222333",
                    "first_name": "test",
                    "family_name": "test",
                    "gender": "M",
                    "inside_municipality": True,
                    "surname_prefix": "Prefix",
                    "noble_title": "Noble title",
                    "country_code": "5107",
                    "mailing_address": True,
                },
                address={
                    "street": "Address",
                    "street_number": "1",
                    "zipcode": "3522AD",
                    "city": "Utrecht",
                    "country": "",
                    "street_number_letter": "aa",
                },
                uuid=str(self.user.uuid),
                type="person",
            )

    @mock.patch.object(PersonRepository, "find_person_by_uuid")
    def test_na_contact_update_address_validations(self, mock_find_by_uuid):
        mock_find_by_uuid.return_value = self.user
        self.session.execute.reset_mock()
        self.cmd.update_non_authentic_contact(
            data={
                "bsn": "111222333",
                "first_name": "test",
                "family_name": "test",
                "gender": "M",
                "inside_municipality": True,
                "surname_prefix": "Prefix",
                "noble_title": "Noble title",
                "country_code": "6030",
                "mailing_address": True,
            },
            address={
                "street": "Address",
                "street_number": "1",
                "zipcode": "3522AD",
                "city": "Utrecht",
                "country": "",
            },
            uuid=str(self.user.uuid),
            type="person",
        )

        self.cmd.update_non_authentic_contact(
            data={
                "bsn": "111222333",
                "first_name": "test",
                "family_name": "test",
                "gender": "M",
                "inside_municipality": True,
                "surname_prefix": "Prefix",
                "noble_title": "Noble title",
                "country_code": "5107",
                "mailing_address": True,
            },
            address={
                "street": "Address",
                "street_number": "1",
                "city": "Curacao City",
                "country": "",
            },
            uuid=str(self.user.uuid),
            type="person",
        )

        self.cmd.update_non_authentic_contact(
            data={
                "sedula_number": "1112223334",
                "first_name": "test",
                "family_name": "test",
                "gender": "M",
                "inside_municipality": True,
                "surname_prefix": "Prefix",
                "noble_title": "Noble title",
                "country_code": "5107",
                "mailing_address": True,
            },
            address={
                "street": "Address",
                "street_number": "1",
                "zipcode": "3522AD",
                "city": "Utrecht",
                "country": "Curaçao",
                "street_number_letter": "",
            },
            uuid=str(self.user.uuid),
            type="person",
        )

    @mock.patch.object(PersonRepository, "find_person_by_uuid")
    def test_na_contact_update_contact_info_validator(self, mock_find_by_uuid):
        mock_find_by_uuid.return_value = self.user
        self.session.execute.reset_mock()

        residence_address = InternationalAddress(
            address_line_1="line", country="Zwitserland"
        )
        self.user.residence_address = residence_address
        with pytest.raises(ValidationError):
            self.cmd.update_non_authentic_contact(
                data={
                    "bsn": "111222333",
                    "first_name": "test",
                    "family_name": "test",
                    "gender": "F",
                    "inside_municipality": True,
                    "surname_prefix": "Prefix",
                    "noble_title": "Noble title",
                    "country_code": "5107",
                    "mailing_address": True,
                },
                contact_information={
                    "email": "error_format_email",
                    "internal_note": "note",
                    "preferred_contact_channel": "pip",
                    "is_anonymous_contact": False,
                },
                uuid=str(self.user.uuid),
                type="person",
            )

        with pytest.raises(ValidationError):
            self.cmd.update_non_authentic_contact(
                data={
                    "bsn": "111222333",
                    "first_name": "test",
                    "family_name": "test",
                    "gender": "F",
                    "inside_municipality": True,
                    "surname_prefix": "Prefix",
                    "noble_title": "Noble title",
                    "country_code": "5107",
                    "mailing_address": True,
                },
                contact_information={
                    "email": "test@email.com",
                    "internal_note": "note",
                    "preferred_contact_channel": "pip",
                    "is_anonymous_contact": False,
                    "phone_number": "phone_number_format_error",
                },
                uuid=str(self.user.uuid),
                type="person",
            )
        with pytest.raises(ValidationError):
            self.cmd.update_non_authentic_contact(
                data={
                    "bsn": "111222333",
                    "first_name": "test",
                    "family_name": "test",
                    "gender": "F",
                    "inside_municipality": True,
                    "surname_prefix": "Prefix",
                    "noble_title": "Noble title",
                    "country_code": "5107",
                    "mailing_address": True,
                },
                contact_information={
                    "email": "test@email.com",
                    "internal_note": "note",
                    "preferred_contact_channel": "pip",
                    "is_anonymous_contact": False,
                    "mobile_number": "mobile_number_format_error",
                },
                uuid=str(self.user.uuid),
                type="person",
            )


class TestUpdateNonAuthenticOrganization(minty.cqrs.test.TestBase):
    def setup_method(self):
        self.mock_repo_factory = mock.MagicMock()
        self.load_command_instance(case_management)
        event_service = EventService(
            correlation_id="correlation_id",
            domain="domain",
            context="context",
            user_uuid=uuid4(),
        )
        self.user_uuid = uuid4()
        self.cmd.user_info = minty.cqrs.UserInfo(
            user_uuid=self.user_uuid,
            permissions={"admin": True, "contact_nieuw": True},
        )
        related_custom_object_uuid = uuid4()
        self.organization = entities.Organization(
            entity_id=uuid4(),
            uuid=uuid4(),
            name="beheerder",
            source="source",
            coc_number="12345678",
            coc_location_number="012345678912",
            date_founded=None,
            date_registered=None,
            date_ceased=None,
            rsin=None,
            oin=None,
            main_activity=None,
            secondary_activities=[],
            organization_type=None,
            location_address=None,
            correspondence_address=None,
            contact_information={},
            related_custom_object=RelatedCustomObject(
                entity_id=related_custom_object_uuid,
                uuid=related_custom_object_uuid,
            ),
            has_valid_address=True,
            authenticated=False,
            _event_service=event_service,
        )

    @mock.patch.object(OrganizationRepository, "find_organization_by_uuid")
    def test_na_organization_update(self, mock_find_by_uuid):
        mock_find_by_uuid.return_value = self.organization
        self.session.execute.reset_mock()
        self.cmd.update_non_authentic_contact(
            data={
                "coc_number": "433326",
                "coc_location_number": "12321313",
                "organization_type": "Maatschap",
                "contact_last_name": "Surname",
                "contact_first_name": "Name",
                "contact_insertions": "van",
                "name": "COMPANY NAME",
            },
            correspondence_address={
                "address_line_1": "line1",
                "country": "Zwitserland",
            },
            address={
                "street": "Address",
                "street_number": "1",
                "zipcode": "3522AD",
                "city": "Utrecht",
                "country": "Nederland",
            },
            contact_information={
                "email": "test1@test.com",
                "internal_note": "note",
                "preferred_contact_channel": "pip",
                "is_anonymous_contact": False,
            },
            uuid=str(self.organization.uuid),
            type="organization",
        )

        mock_find_by_uuid.assert_called_once_with(uuid=self.organization.uuid)
        assert self.organization.coc_location_number == 12321313
        assert self.organization.organization_type == "7"
        assert self.organization.contact_person.first_name == "Name"
        assert self.organization.name == "COMPANY NAME"
        assert self.organization.contact_information.email == "test1@test.com"

        self.cmd.update_non_authentic_contact(
            data={
                "coc_number": "433326",
                "coc_location_number": "12321313",
                "organization_type": "Maatschap",
                "contact_last_name": "Surname",
                "contact_first_name": "Name",
                "contact_insertions": "van",
                "name": "COMPANY NAME",
            },
            correspondence_address={
                "address_line_1": "line1",
                "country": "Zwitserland",
            },
            address={
                "street": "Address",
                "street_number": "1",
                "zipcode": "3522AD",
                "city": "Utrecht",
                "country": "Nederland",
            },
            uuid=str(self.organization.uuid),
            type="organization",
        )
        assert self.organization.contact_information.email == "test1@test.com"

    @mock.patch.object(OrganizationRepository, "find_organization_by_uuid")
    def test_na_organization_update_coc_value_error(self, mock_find_by_uuid):
        mock_find_by_uuid.return_value = self.organization
        self.session.execute.reset_mock()
        with pytest.raises(ValueError) as excinfo:
            self.cmd.update_non_authentic_contact(
                data={
                    "coc_number": "2243332612",
                    "coc_location_number": "12321313",
                    "organization_type": "Maatschap",
                    "contact_last_name": "Surname",
                    "contact_first_name": "Name",
                    "contact_title": "van",
                    "name": "COMPANY NAME",
                },
                correspondence_address={
                    "address_line_1": "line1",
                    "country": "Zwitserland",
                },
                address={
                    "street": "Address",
                    "street_number": "1",
                    "zipcode": "3522AD",
                    "city": "Utrecht",
                    "country": "Nederland",
                },
                contact_information={
                    "email": "test1@test.com",
                    "internal_note": "note",
                    "preferred_contact_channel": "pip",
                    "is_anonymous_contact": False,
                },
                uuid=str(self.organization.uuid),
                type="organization",
            )
        assert "Invalid KVK" in str(excinfo.value)

    def test_update_na_organization_authenticated(self):
        self.organization.authenticated = True
        address = {
            "street": "Address",
            "street_number": "1",
            "zipcode": "3522AD",
            "city": "Utrecht",
            "country": "Nederland",
        }
        correspondence_address = {
            "street": "Croesestraat",
            "street_number": "93",
            "city": "Utrecht",
            "zipcode": "3522AD",
            "country": "Nederland",
        }
        with pytest.raises(Conflict) as excinfo:
            self.organization.update_non_authentic(
                update_data={},
                address=address,
                correspondence_address=correspondence_address,
            )
        assert excinfo.value.args == (
            "Can not update Authenticated Organization",
        )

    def test_update_non_authentic_organization_repo(self):
        mock_infra = mock.MagicMock()
        mock_infra_ro = mock.MagicMock()
        event_service = EventService(
            correlation_id="correlation_id",
            domain="domain",
            context="context",
            user_uuid=uuid4(),
        )
        organization_repo = OrganizationRepository(
            infrastructure_factory=mock_infra,
            infrastructure_factory_ro=mock_infra_ro,
            context="devtest",
            event_service=event_service,
            read_only=False,
        )
        organization_uuid = uuid4()
        changes = [
            {"key": "coc_number", "old_value": None, "new_value": "111222333"},
            {
                "key": "correspondence_address",
                "old_value": {
                    "country": "Zwitserland",
                    "address_line_1": "line1",
                },
                "new_value": {
                    "country": "Nederland",
                    "street": "cr",
                    "street_number": 11,
                    "street_number_letter": None,
                    "street_number_suffix": None,
                    "zipcode": "3455ad",
                    "city": "Utrecht",
                    "is_foreign": False,
                },
            },
            {
                "key": "contact_person",
                "old_value": None,
                "new_value": {"first_name": "First", "faamily_name": "Family"},
            },
            {
                "key": "location_address",
                "old_value": {
                    "country": "Nederland",
                    "street": "cr",
                    "street_number": 11,
                    "street_number_letter": None,
                    "street_number_suffix": None,
                    "zipcode": "3455ad",
                    "city": "Utrecht",
                    "is_foreign": False,
                },
                "new_value": {
                    "country": "Zwitserland",
                    "address_line_1": "line1",
                },
            },
        ]
        event = Event(
            uuid=organization_uuid,
            created_date="2019-05-31",
            correlation_id=organization_uuid,
            domain="domain",
            context="context",
            user_uuid=uuid4(),
            entity_type="Organization",
            entity_id=organization_uuid,
            event_name="OrganizationUpdated",
            changes=changes,
            entity_data={"attribute_type": "file"},
        )
        event.entity_id = organization_uuid
        event.context = "context"

        organization_repo._update_non_authentic(event, self.cmd.user_info)
        (args, kwargs) = organization_repo.session.execute.call_args
        query = args[0].compile()
        assert str(query) == (
            "UPDATE bedrijf SET handelsnaam=:handelsnaam, dossiernummer=:dossiernummer, rechtsvorm=:rechtsvorm, contact_naam=:contact_naam, contact_voorvoegsel=:contact_voorvoegsel, contact_voorletters=:contact_voorletters, contact_geslachtsnaam=:contact_geslachtsnaam, vestiging_straatnaam=:vestiging_straatnaam, vestiging_huisnummer=:vestiging_huisnummer, vestiging_huisnummertoevoeging=:vestiging_huisnummertoevoeging, vestiging_postcode=:vestiging_postcode, vestiging_woonplaats=:vestiging_woonplaats, correspondentie_straatnaam=:correspondentie_straatnaam, correspondentie_huisnummer=:correspondentie_huisnummer, correspondentie_huisnummertoevoeging=:correspondentie_huisnummertoevoeging, correspondentie_postcode=:correspondentie_postcode, correspondentie_woonplaats=:correspondentie_woonplaats, vestigingsnummer=:vestigingsnummer, vestiging_huisletter=:vestiging_huisletter, correspondentie_huisletter=:correspondentie_huisletter, vestiging_adres_buitenland1=:vestiging_adres_buitenland1, vestiging_adres_buitenland2=:vestiging_adres_buitenland2, vestiging_adres_buitenland3=:vestiging_adres_buitenland3, vestiging_landcode=:vestiging_landcode, correspondentie_adres_buitenland1=:correspondentie_adres_buitenland1, correspondentie_adres_buitenland2=:correspondentie_adres_buitenland2, correspondentie_adres_buitenland3=:correspondentie_adres_buitenland3, correspondentie_landcode=:correspondentie_landcode, search_term=:search_term, search_order=:search_order WHERE bedrijf.uuid = :uuid_1"
        )

    def test_na_search_term(self):
        mock_infra = mock.MagicMock()
        mock_infra_ro = mock.MagicMock()
        event_service = EventService(
            correlation_id="correlation_id",
            domain="domain",
            context="context",
            user_uuid=uuid4(),
        )
        organization_repo = OrganizationRepository(
            infrastructure_factory=mock_infra,
            infrastructure_factory_ro=mock_infra_ro,
            context="devtest",
            event_service=event_service,
            read_only=False,
        )
        changes = {
            "name": "name",
            "location_address": {
                "address_line_1": "string",
                "address_line_2": None,
                "address_line_3": None,
            },
        }

        result = organization_repo._search_term(changes)
        assert result == "name string"


class TestUpdateNonAuthenticUserPermissionDenied(minty.cqrs.test.TestBase):
    def setup_method(self):
        self.mock_repo_factory = mock.MagicMock()
        self.load_command_instance(case_management)
        self.user_uuid = uuid4()
        self.cmd.user_info = minty.cqrs.UserInfo(
            user_uuid=self.user_uuid,
            permissions={
                "contact_nieuw": False,
                "admin": False,
                "contact_search": False,
            },
        )

    @mock.patch.object(PersonRepository, "find_person_by_uuid")
    def test_na_contact_update_without_permissions(self, mock_find_by_uuid):
        self.session.execute.reset_mock()
        with pytest.raises(Forbidden):
            self.cmd.update_non_authentic_contact(
                data={
                    "first_name": "test",
                    "family_name": "test",
                    "gender": "M",
                    "inside_municipality": True,
                    "surname_prefix": "Prefix",
                    "noble_title": "Noble title",
                    "country_code": "6030",
                    "mailing_address": True,
                },
                address={
                    "street": "Address",
                    "street_number": "1",
                    "zipcode": "3522AD",
                    "city": "Utrecht",
                    "country": "Nederland",
                },
                contact_information={
                    "email": "test1@test.com",
                    "internal_note": "note",
                    "preferred_contact_channel": "pip",
                    "is_anonymous_contact": False,
                },
                uuid=str(self.user_uuid),
                type="person",
            )

    @mock.patch.object(PersonRepository, "find_person_by_uuid")
    def test_na_contact_bsn_update_without_permissions(
        self, mock_find_by_uuid
    ):
        self.cmd.user_info = minty.cqrs.UserInfo(
            user_uuid=self.user_uuid,
            permissions={
                "contact_nieuw": True,
                "admin": False,
                "view_sensitive_contact_data": False,
            },
        )
        self.session.execute.reset_mock()
        with pytest.raises(Forbidden):
            self.cmd.update_non_authentic_contact(
                data={
                    "bsn": "111",
                    "first_name": "test",
                    "family_name": "test",
                    "gender": "M",
                    "inside_municipality": True,
                    "surname_prefix": "Prefix",
                    "noble_title": "Noble title",
                    "country_code": "6030",
                    "mailing_address": True,
                },
                address={
                    "street": "Address",
                    "street_number": "1",
                    "zipcode": "3522AD",
                    "city": "Utrecht",
                    "country": "Nederland",
                },
                contact_information={
                    "email": "test1@test.com",
                    "internal_note": "note",
                    "preferred_contact_channel": "pip",
                    "is_anonymous_contact": False,
                },
                uuid=str(self.user_uuid),
                type="person",
            )

import logging
import os
import sys
import threading
import time
from helper import Connection
from queue import Queue

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
logFormatter = logging.Formatter(
    "%(name)-12s %(asctime)s %(levelname)-8s %(filename)s:%(funcName)s %(message)s"
)
consoleHandler = logging.StreamHandler(
    sys.stdout
)  # set streamhandler to stdout
consoleHandler.setFormatter(logFormatter)
logger.addHandler(consoleHandler)


def worker(worker_id: int):
    while not DATABASE_QUEUE.empty():
        db, index = DATABASE_QUEUE.get()
        conn = Connection(db=db)
        now = time.perf_counter()
        for function in FUNCTIONS:
            logger.info(f"Worker-{worker_id}: Running {function=} on {db=}")
            try:
                start_function = time.perf_counter()
                result = conn.query(f"SELECT {function}();")
                end_function = time.perf_counter()
                logger.info(
                    f"Worker-{worker_id}: Finished running {function=} on {db=}, result={result}. Query took {(end_function - start_function):.4f}"
                )
            except RuntimeError as ex:
                logger.error(ex)
        post = time.perf_counter()
        logger.info(
            f"Worker-{worker_id}: Queries on {db} took {(post - now):.2f} seconds [{index}/{DATABASE_COUNT}]"
        )
        conn.close()


FUNCTIONS = [
    function for function in os.environ.get("DB_FUNCTIONS").split(",")
]
DATABASE_QUEUE = Queue()
DATABASE_LIST = Connection().list_databases()
DATABASE_COUNT = len(DATABASE_LIST)
WORKER_COUNT = 50

for index, db in enumerate(DATABASE_LIST):
    DATABASE_QUEUE.put((db, index))

threads = []
for worker_id in range(WORKER_COUNT):
    thread = threading.Thread(target=worker, args=(worker_id,))
    threads.append(thread)
    thread.start()

for thread in threads:
    thread.join()

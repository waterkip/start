# Zaaksysteem database cleaner

This container is able to execute sql functions on a zaaksysteem customer database.

Our goal is to have the customer databases cleaned. With that we mean unnecessary data that can be removed.

To achieve this, we have to write database functions that will clean this data is small chuncks.

This database-cleaner container will be triggered with an interval, and will call the cleanup database functions on all customer databases.

## Configuration
To configure the database cleaner tool, the following environment variables can be used:

* `DB_HOST` - Hostname of the database that will be connected to.
* `DB_PORT` - Port of the database that will be connected to.
* `DB_USER` - Username to connect to the database.
* `DB_PASS` - Passowrd to connect to the database.
* `DB_FUNCTIONS` - Comma sperated list of database function names that will be called. For example `cleanup_queue,cleanup_transaction,cleanup_object_data` .
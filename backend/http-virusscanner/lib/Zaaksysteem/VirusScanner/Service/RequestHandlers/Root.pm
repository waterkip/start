package Zaaksysteem::VirusScanner::Service::RequestHandlers::Root;

use Moose;

with qw[
    Zaaksysteem::Service::HTTP::RequestHandler
    MooseX::Log::Log4perl
];

=head1 NAME

Zaaksysteem::VirusScanner::Service::RequestHandlers::Root - One-line description here

=head1 DESCRIPTION

Description here

=head1 SYNOPSIS

    use Zaaksysteem::VirusScanner::Service::RequestHandlers::Root;

    # Example usage here

=cut

use BTTW::Tools;

use Zaaksysteem::VirusScanner::Service;

use URI;

=head1 METHODS

=head2 name

Implements the name interface required by
L<Zaaksysteem::Service::HTTP::RequestHandler>.

Returns the empty string, this handler attaches to the root of the
application.

=cut

sub name { '' }

=head2 dispatch

Implements the dispatcher for the root handler. Returns a L<Plack::Response>
with metadata about the service.

=cut

sub dispatch {
    my ($self, $request, $response) = @_;

    return Syzygy::Object::Model->new_object(service => {
        name => 'Zaaksysteem VirusScanner service',
        repository => URI->new ('https://gitlab.com/zaaksysteem/zaaksysteem-virus_scanner-service'),
        version => "$Zaaksysteem::VirusScanner::Service::VERSION"
    });

}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the C<CONTRIBUTORS> file.

Zaaksysteem::VirusScanner uses the EUPL license, for more information please have a look at the C<LICENSE> file.


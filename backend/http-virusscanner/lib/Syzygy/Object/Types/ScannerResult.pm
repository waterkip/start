package Syzygy::Object::Types::ScannerResult;

use Moose;
use namespace::autoclean;

extends 'Syzygy::Object::Type::Moose';

=head1 NAME

Syzygy::Object::Types::ScannerResult - Object type for C<Scanner> instances

=head1 DESCRIPTION

This class declares the C<scanner_result> object type, which is used by
L<Zaaksysteem::Service::VirusScan> services for informational purposes.

=cut

use Syzygy::Syntax;

=head1 OBJECT TYPE ATTRIBUTES

=head2 viruses

Lists the viruses that were found in the file being scanned.

=cut

szg_attr viruses => (
    value_type_name => 'array'
);

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.

# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import amqpstorm  # type: ignore
from minty_infra_amqp import AMQPInfrastructure
from unittest import mock


class TestAMQPInfrastructure:
    """Test the AMQP infrastructure"""

    def setup_method(self) -> None:
        with mock.patch("amqpstorm.UriConnection") as mock_amqp:
            amqp_infra = AMQPInfrastructure()
            assert amqp_infra.channels == {}
            assert amqp_infra.connection is None
            self.amqp_infra = amqp_infra
            self.config = {"amqp": {"url": "amqp://url/here"}}
            self.mock_channel = mock.MagicMock()
            mock_amqp().channel.return_value = self.mock_channel
            channel = self.amqp_infra(self.config)
            assert channel == self.mock_channel
            assert list(self.amqp_infra.channels.keys()) == ["amqp://url/here"]
            assert amqp_infra.connection is not None

    def test_amqp_infrastructure_cached_connection(self) -> None:
        self.mock_channel.check_for_errors.return_value = True
        channel2 = self.amqp_infra(self.config)
        assert channel2 == self.mock_channel

    def test_amqp_infrastructure_channel_error(self) -> None:
        assert self.amqp_infra.connection is not None

        self.mock_channel.check_for_errors.side_effect = (
            amqpstorm.AMQPChannelError
        )
        self.amqp_infra.connection.channel.return_value = (
            "channel_called_again"
        )
        channel = self.amqp_infra(self.config)
        assert channel == "channel_called_again"

    @mock.patch("amqpstorm.UriConnection")
    def test_amqp_infrastructure_connection_error(
        self, mock_amqp: mock.Mock
    ) -> None:
        self.mock_channel.check_for_errors.side_effect = (
            amqpstorm.AMQPConnectionError
        )
        mock_amqp().channel.return_value = "channel"

        channel = self.amqp_infra(self.config)
        assert channel == "channel"

package Zaaksysteem::Test::Auth::DigiD;

use Zaaksysteem::Test;

use BTTW::Tools::RandomData qw(generate_uuid_v4 generate_bsn);
use IO::All;

use Zaaksysteem::Auth::DigiD;
use Zaaksysteem::Store::Redis;
use Digest::SHA qw(sha1_base64);

sub test_credential_glue {

    my $xml = io->catfile(
        qw(t data saml2 digid-sls-soap.xml)
    )->slurp;

    my ($get, $del, $set, $expire, $time, $session);

    my $session_id = generate_uuid_v4;
    my $nameid = 's00000000:' . generate_bsn;

    $xml =~ s/s00000000:BSN/$nameid/;

    my $redis = Zaaksysteem::Store::Redis->new(
        redis => mock_strict(
            get => sub {
                $get = shift;
                return $session_id;
            },
            del => sub {
                $del = shift;
                return 1;
            },
            set => sub {
                $set = shift;
                $session = shift;
                return 1;
            },
            expire => sub {
                $expire = shift;
                $time = shift;
                return 1;
            },
            exists => 1,
        ),
    );

    my @cookies;
    my $cookiejar = Zaaksysteem::Store::Redis->new(
        redis => mock_strict(
            del => sub {
                push(@cookies, shift);
                return 1;
            },
            exists => 1,
        ),
    );

    my $digid = Zaaksysteem::Auth::DigiD->new(
        redis     => $redis,
        cookiejar => $cookiejar,
    );

    isa_ok $digid, 'Zaaksysteem::Auth::DigiD';

    my $redis_id = $digid->_generate_redis_id($nameid);

    is(
        $redis_id,
        join(':', 'digid', sha1_base64($nameid)),
        "Redis doesn't store plaintext data"
    );

    $digid->start_federated_session($nameid, $session_id);
    is($set, $redis_id, "Set the federated stuf in Redis");
    is($session, "yaml:session:$session_id" , ".. with the correct session key");
    is($expire, $redis_id, ".. and set the expire item");
    is($time, 10800, ".. with the correct expire time");

    $digid->end_federated_session($xml);
    is($get, $redis_id, "Found the DigiD redis key");
    is($del, $redis_id, ".. and deleted it as well");

    my $sid = $session_id =~ s/session://r;

    cmp_deeply(
        \@cookies,
        [$session_id, "yaml:$session_id", "json:$session_id", "expires:$sid"],
        ".. and the cookiejar is now also empty"
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.

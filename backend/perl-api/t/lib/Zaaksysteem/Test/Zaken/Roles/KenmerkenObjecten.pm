package Zaaksysteem::Test::Zaken::Roles::KenmerkenObjecten;

use Zaaksysteem::Test;

=head1 NAME

Zaaksysteem::Test::Zaken::Roles::KenmerkenObjecten - Tests for case attribute
logic.

=head1 SYNOPSIS

    prove -l :: Zaaksysteem::Test::Zaken::Roles::KenmerkenObjecten;

=cut

sub _mock_zaak;

sub test_handle_attribute_triggers {
    my $zaak = _mock_zaak(
        _expected_attribute_id => 123,
        _map_case_location_toggle => 0, # Attribute not configured as 'case location source'
    );

    my $created_item_spec;
    my $created_item_was_queued;

    my $queue = mock_one(
        create_item => sub {
            $created_item_spec = shift;
            
            return 'itemXYZ'
        },

        queue_item => sub {
            $created_item_was_queued //= shift eq 'itemXYZ'
        }
    );

    my $event_mock_does;
    my $event_attr_value_type = 'abc';
    my $event_attr_id = 987;

    my $event = mock_one(
        does => sub { return $event_mock_does },
        onderwerp => 'my subject line',
        id => 'event_id_123',
        event_data => sub {
            return {
                casetype_attribute_id => $event_attr_id,
                attribute_value => '123.456,789.012'
            }
        },
        attribute => mock_one(
            value_type => sub { return $event_attr_value_type }
        )
    );

    $zaak->handle_attribute_triggers($queue, $event);

    ok !defined $created_item_spec, 'no queue item was created for non-attribute update event';
    ok !defined $created_item_was_queued, 'no queue item was queued';

    # Event now ->does(*);
    $event_mock_does = 1;

    $zaak->handle_attribute_triggers($queue, $event);

    ok !defined $created_item_spec, 'no queue item was created for non-geolatlon attribute update';
    ok !defined $created_item_was_queued, 'no queue item was queued';

    $event_attr_value_type = 'geolatlon';

    $zaak->handle_attribute_triggers($queue, $event);

    ok !defined $created_item_spec, 'no queue item was created if attribute could not resolve';
    ok !defined $created_item_was_queued, 'no queue item was queued';

    # Get warnings and cleanup for next run
    my @warns = @{ delete $zaak->_loglines->{ warn } };

    is scalar @warns, 1, 'one warning logline issued for unresolvable attribute id';
    ok $warns[0] =~ m[my subject line], 'onderwerp noted in logline';
    ok $warns[0] =~ m[event_id_123], 'event_id noted in logline';

    # Setup expected id
    $event_attr_id = 123;

    $zaak->handle_attribute_triggers($queue, $event);

    ok !defined $created_item_spec, 'no queue item was created if attribute is not configured as case location source';
    ok !defined $created_item_was_queued, 'no queue item was queued';
    ok !defined $zaak->_loglines->{ warn }, 'no warnings issued if attribute not configured as case location';

    $zaak->_map_case_location_toggle(1);

    $zaak->handle_attribute_triggers($queue, $event);

    ok defined $created_item_spec, 'no queue item was created if attribute is not configured as case location source';
    ok defined $created_item_was_queued, 'no queue item was queued';
    ok !defined $zaak->_loglines->{ warn }, 'no warnings issued if attribute not configured as case location';

    is_deeply $created_item_spec, {
        type => 'update_case_location',
        object_id => '<uuid>',
        label => 'Zaaklocatie instellen',
        disable_acl => 1,
        data => { latitude => '123.456', longitude => '789.012' },
    }, 'created queue item looks ok';
}


sub _mock_zaak {
    return Zaaksysteem::Test::Zaken::Roles::KenmerkenObjecten::MockZaak->new(@_);
}

package Zaaksysteem::Test::Zaken::Roles::KenmerkenObjecten::MockZaak;

use Moose;
use Zaaksysteem::Zaken::AdvanceResult;
use Test::Mock::One;

with qw[
    Zaaksysteem::Zaken::Roles::KenmerkenObjecten
];

has _expected_attribute_id => (
    is => 'rw',
    isa => 'Defined',
    required => 1
);

has _map_case_location_toggle => (
    is => 'rw',
    isa => 'Defined',
    required => 1
);

has _loglines => (
    is => 'rw',
    isa => 'HashRef',
    default => sub {
        return { warn => [] };
    }
);

sub can_volgende_fase { return Zaaksysteem::Zaken::AdvanceResult->new };

sub zaaktype_node_id {
    my $zaak = shift;

    return Test::Mock::One->new(
        zaaktype_kenmerken => Test::Mock::One->new(
            find => sub {
                my $id = shift;

                return unless $id eq $zaak->_expected_attribute_id;

                return Test::Mock::One->new(
                    properties => sub {
                        return {
                            map_case_location => $zaak->_map_case_location_toggle
                        };
                    }
                );
            }
        )
    );
}

sub get_column {
    return { uuid => '<uuid>' }->{ $_[1] };
}

sub log {
    my $self = shift;

    return Test::Mock::One->new(
        warn => sub {
            my $warnlog = $self->_loglines->{ warn } //= [];

            push @{ $warnlog }, @_;
        }
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.

package Zaaksysteem::Test::Backend::Sysin::STUFDCR::Model;
use Zaaksysteem::Test;

use Zaaksysteem::Backend::Sysin::STUFDCR::Model;
use IO::All;

sub test_parse_nonsoap {
    my $mock_soap = mock_one();

    my $model = Zaaksysteem::Backend::Sysin::STUFDCR::Model->new(
        recipient_application => 'test_app',
        soap_client => $mock_soap,
    );

    my $xml = <<'EOT';
<?xml version="1.0"?>
<definitely_not_soap/>
EOT

    my $x = exception { $model->parse_incoming_xml($xml); };
    isa_ok(
        $x,
        'BTTW::Exception::Base',
        'When return XML is not SOAP, an exception is thrown',
    );
    is(
        $x->type,
        'stufdcr/not_soap',
        'When return XML is not SOAP, the correct kind of exception is thrown',
    );
}

sub test_parse_soap {
    my $mock_soap = mock_one();

    my $model = Zaaksysteem::Backend::Sysin::STUFDCR::Model->new(
        recipient_application => 'test_app',
        soap_client => $mock_soap,
    );

    my $xml = <<'EOT';
<?xml version="1.0" encoding="UTF-8"?>
<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
  <S:Body>
    <leverDocumentcreatieResultaatDu02 xmlns="http://www.kinggemeenten.nl/StUF/sector/dcr/0310" xmlns:ns2="http://www.egem.nl/StUF/StUF0301" xmlns:ns3="http://www.egem.nl/StUF/sector/zkn/0310" xmlns:ns4="http://www.egem.nl/StUF/sector/bg/0310" xmlns:ns5="http://www.opengis.net/gml" xmlns:ns6="http://www.w3.org/1999/xlink" xmlns:xmime="http://www.w3.org/2005/05/xmlmime">
      <stuurgegevens>
        <ns2:berichtcode>Du02</ns2:berichtcode>
        <ns2:zender>
          <ns2:applicatie>applicatie_naam_hier</ns2:applicatie>
        </ns2:zender>
        <ns2:ontvanger>
          <ns2:applicatie>Zaaksysteem</ns2:applicatie>
        </ns2:ontvanger>
        <ns2:referentienummer>4e240159-fd61-4eaf-9a4a-e99d31c99c70</ns2:referentienummer>
        <ns2:tijdstipBericht>20161005130216000</ns2:tijdstipBericht>
        <ns2:functie>DocumentcreatieResultaat</ns2:functie>
      </stuurgegevens>
      <parameters>
        <jobidentificatie>4e240159-fd61-4eaf-9a4a-e99d31c99c70</jobidentificatie>
        <creatieJobStatus>klaar</creatieJobStatus>
        <resultaatUrl>https://example.com/</resultaatUrl>
      </parameters>
      <document ns2:functie="entiteit" ns2:entiteittype="EDC">
        <ns3:formaat>txt</ns3:formaat>
        <ns3:inhoud ns2:bestandsnaam="TestTemplate.txt" xmime:contentType="text/plain">aG9pCg==</ns3:inhoud>
        <ns2:tijdstipRegistratie xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:nil="true"/>
        <ns3:isRelevantVoor>
          <ns3:gerelateerde ns2:entiteittype="ZAK">
            <ns3:identificatie>0000230</ns3:identificatie>
            <ns3:startdatum>20160914</ns3:startdatum>
            <ns3:registratiedatum>20160914</ns3:registratiedatum>
            <ns3:einddatumGepland>20161017</ns3:einddatumGepland>
            <ns3:uiterlijkeEinddatum>20161017</ns3:uiterlijkeEinddatum>
            <ns3:einddatum xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ns2:noValue="geenWaarde" xsi:nil="true"/>
            <ns3:datumVernietigingDossier xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ns2:noValue="geenWaarde" xsi:nil="true"/>
            <ns3:zaakniveau>1</ns3:zaakniveau>
            <ns3:deelzakenIndicatie>N</ns3:deelzakenIndicatie>
            <ns2:tijdstipRegistratie xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:nil="true"/>
          </ns3:gerelateerde>
          <ns2:tijdstipRegistratie xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:nil="true"/>
        </ns3:isRelevantVoor>
      </document>
    </leverDocumentcreatieResultaatDu02>
  </S:Body>
</S:Envelope>
EOT

    my ($xc, $root) = $model->parse_incoming_xml($xml);
    isa_ok($xc, 'XML::LibXML::XPathContext', 'Parsing resulted in an XPath context');
    isa_ok($root, 'XML::LibXML::Node', 'Parsing also resulted in a root node');

    my $result = $model->dispatch($xc, $root);

    is($result->{job_id}, '4e240159-fd61-4eaf-9a4a-e99d31c99c70', 'Job ID found correctly');
    is($result->{document}{filename}, 'TestTemplate.txt', 'File name found correctly');

    my $content = io($result->{document}{content}->filename)->slurp;
    is($content, "hoi\n", 'File contents found correctly');

    cmp_deeply(
        $result->{stuurgegevens},
        {
            'reference'             => '4e240159-fd61-4eaf-9a4a-e99d31c99c70',
            'receiver'              => 'Zaaksysteem',
            'receiver_admin'        => '',
            'receiver_organisation' => '',
            'receiver_user'         => '',
            'sender'                => 'applicatie_naam_hier',
            'sender_admin'          => '',
            'sender_organisation'   => '',
            'sender_user'           => ''
        },
        'StUF stuurgegevens are parsed correctly',
    );
}

sub test_parse_soap_error {
    my $mock_soap = mock_one();

    my $model = Zaaksysteem::Backend::Sysin::STUFDCR::Model->new(
        recipient_application => 'test_app',
        soap_client => $mock_soap,
    );

    my $xml = <<'EOT';
<?xml version="1.0" encoding="UTF-8"?>
<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/1999/XMLSchema-instance" xmlns:xsd = "http://www.w3.org/1999/XMLSchema">
  <S:Body>
    <S:Fault>
      <faultcode xsi:type="xsd:string">SOAP-ENV:Client</faultcode>
      <faultstring xsi:type="xsd:string">It broke.</faultstring>
    </S:Fault>
  </S:Body>
</S:Envelope>
EOT

    my ($xc, $root) = $model->parse_incoming_xml($xml);
    isa_ok($xc, 'XML::LibXML::XPathContext', 'Parsing resulted in an XPath context');
    isa_ok($root, 'XML::LibXML::Node', 'Parsing also resulted in a root node');

    my $handled_error = $model->handle_error($xc, $root);

    my $expected_error = 'Received an error. XML = <S:Fault>
      <faultcode xsi:type="xsd:string">SOAP-ENV:Client</faultcode>
      <faultstring xsi:type="xsd:string">It broke.</faultstring>
    </S:Fault>';

    is($handled_error->{error}, $expected_error, "Error message is returned correctly");
    is($handled_error->{status}, "soap-error", "Status is 'soap-error'");
}

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

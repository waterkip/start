import random from 'lodash/random';
import {
    selectFirstSuggestion
} from './../select';
import inputDate from './inputDate';
import inputFile from './inputFile';
import {
    mouseOver
} from './../mouse';
import getAttributeDefaultValue from './../getValue/getAttributeDefaultValue';

/**
 * @param {Object} attribute
 * @return {Promise}
 */
export const getAttributeClass = attribute =>
    attribute
        .$('vorm-field')
        .getAttribute('class');

/**
 * @param {Object} attribute
 * @return {Promise}
 */
export const getAttributeDataType = attribute =>
    attribute
        .$('vorm-field')
        .getAttribute('data-type');

/**
 * @param {Object} attribute
 * @return {Promise}
 */
export const getAttributeMultiple = attribute =>
    attribute
        .$('vorm-field')
        .getAttribute('data-multiple')
        .then(multiple =>
            (multiple === '1' ? '' : 'multiple')
        );

/**
 * @param {Object} attribute
 * @return {Promise}
 */
export const getAttributeType = attribute => {
    const attributeClass = getAttributeClass(attribute);
    const attributeDataType = getAttributeDataType(attribute);
    const attributeMultiple = getAttributeMultiple(attribute);

    return Promise
        .all([attributeClass, attributeDataType, attributeMultiple])
        .then(results => {
            const [attrClass, attrDataType, attrMultiple] = results;
            const attributeType = attrDataType ? `${attrDataType}${attrMultiple}` : attrClass.replace(/ng-isolate-scope|attribute-type-|object-suggest-multiple|empty|invalid|valid|required| /g, '');
            
            return attributeType;
        });
};

export const clearAttribute = attribute => {
    getAttributeType(attribute).then(attributeType => {
        const attributesWithCommonClear = [
            'bag_adres', 'bag_openbareruimte', 'bag_straat_adres', 'bag_adressenmultiple', 'bag_openbareruimtesmultiple', 'bag_straat_adressenmultiple',
            'geolatlon', 'googlemaps',
            'text', 'textmultiple', 'text_uc', 'text_ucmultiple', 'textarea', 'textareamultiple', 'numeric', 'numericmultiple',
            'valuta', 'valutaex', 'valutaex21', 'valutaex6', 'valutain', 'valutain21', 'valutain6',
            'bankaccount', 'date', 'email', 'image_from_url', 'url', 'richtext',
            'option', 'select', 'selectmultiple'
        ];
        const attributesWithSpecialClear = {
            filemultiple: () => {
                const clearButtons = $$('.value-item .mdi-close');

                clearButtons
                    .count()
                    .then(count => {
                        mouseOver(attribute);

                        for (let i = count - 1; i > 0; i-- ) {
                            clearButtons.get(i - 1).click();
                        }
                    });
            },
            checkbox: () => {
                const checkedBoxes = $$('input[checked="checked"]');

                checkedBoxes
                    .count()
                    .then(count => {
                        mouseOver(attribute);

                        for (let i = count; i > 0; i-- ) {
                            checkedBoxes.get(i - 1).click();
                        }
                    });
            }
        };

        if ( attributesWithCommonClear.includes(attributeType) ) {
            const clearButtons = $$('.mdi-close');

            clearButtons
                .count()
                .then(count => {
                    mouseOver(attribute);

                    for (let i = count; i > 0; i-- ) {
                        clearButtons.get(i - 1).click();
                    }
                });
        } else {
            attributesWithSpecialClear[attributeType]();
        }
    });
};

export const inputAttribute = ( attribute, input ) => {
    getAttributeType(attribute).then(attributeType => {
        const definedInput = input || getAttributeDefaultValue(attributeType);

        switch (attributeType) {

        case 'bag_adres':
        case 'bag_openbareruimte':
        case 'bag_straat_adres':
        case 'bag_adressenmultiple':
        case 'bag_openbareruimtesmultiple':
        case 'bag_straat_adressenmultiple': {
            const valuesToInput = definedInput.constructor === Array ? definedInput : [ definedInput ];

            valuesToInput
                .forEach(valueToInput => {
                    selectFirstSuggestion(attribute.$('input'), valueToInput);
                });

            break;
        }

        case 'checkbox': {
            const checkboxesToInput = definedInput.constructor === Array ? definedInput : [ definedInput ];

            checkboxesToInput
                .forEach(checkboxToInput => {
                    attribute
                        .$(`label:nth-child(${checkboxToInput}) [type="checkbox"]`)
                        .click();
            });

            break;
        }

        case 'date':
            inputDate(attribute, definedInput);

            break;

        case 'filemultiple':
            inputFile(attribute, definedInput);

            break;

        case 'geolatlon':
        case 'googlemaps':
            attribute
                .$('input')
                .sendKeys(definedInput);

            browser.waitForAngular();

            attribute
                .$$('.suggestion-list button')
                .first()
                .click();
        
            break;

        case 'option':
            attribute.$(`label:nth-child(${definedInput}) input`).click();

            break;

        case 'richtext':
            attribute
                .$('.rich-text-editor.dummy')
                .click();

            browser.sleep(2000);

            attribute
                .$('.ql-editor')
                .sendKeys(definedInput);
        
            break;

        case 'select':
            attribute
                .$('select')
                .click();

            attribute
                .$(`select option:nth-child(${definedInput + 1})`)
                .click();

            break;

        case 'selectmultiple':
            definedInput.forEach((input, index) => {
                attribute
                    .$(`li:nth-child(${index + 1}) select`)
                    .click();

                attribute
                    .$(`li:nth-child(${index + 1}) select option:nth-child(${input + 1})`)
                    .click();

                if ( definedInput.length - 1 > index ) {
                    attribute
                        .$('.vorm-field-add-button')
                        .click();
                }
            });

            break;

        case 'numericmultiple':
        case 'textmultiple':
        case 'text_ucmultiple':
            definedInput.forEach((input, index) => {
                attribute
                    .$(`li:nth-child(${index + 1}) input`)
                    .sendKeys(input);
                
                if ( definedInput.length - 1 > index ) {
                    attribute
                        .$('.vorm-field-add-button')
                        .click();
                }
            });
        
            break;

        case 'textarea':
            attribute.$('textarea').sendKeys(definedInput);
        
            break;

        case 'textareamultiple':
            definedInput.forEach((input, index) => {
                attribute
                    .$(`li:nth-child(${index + 1}) textarea`)
                    .sendKeys(input);

                if ( definedInput.length - 1 > index ) {
                    attribute
                        .$('.vorm-field-add-button')
                        .click();
                }
            });
        
            break;

        case 'bankaccount':
        case 'url':
        case 'email':
        case 'image_from_url':
        case 'numeric':
        case 'text':
        case 'text_uc':
        case 'valuta':
        case 'valutaex':
        case 'valutaex21':
        case 'valutaex6':
        case 'valutain':
        case 'valutain21':
        case 'valutain6':
            attribute
                .$('input')
                .sendKeys(definedInput);
        
            break;

        default:
        break;

        };
    });
};

export const clearAndInputAttribute = ( attribute, input ) => {
    clearAttribute(attribute);
    inputAttribute(attribute, input);
};

export const inputAttributes = ( data = [] ) => {
    data.forEach(({attr, input, clear}) => {
        inputAttribute(attr, input, clear);
    });
};

export const status = attribute =>
    attribute
        .$('ul.disabled')
        .isPresent()
        .then(presence =>
            !presence
        );

export const statuses = attributes =>
    attributes
        .map(attr =>
            status(attr)
        );

export const getValue = attribute => {
    return getAttributeType(attribute).then(attributeType => {
        switch ( attributeType ) {

        case 'bag_adres':
        case 'bag_openbareruimte':
        case 'bag_straat_adres':
        case 'bag_adressenmultiple':
        case 'bag_openbareruimtesmultiple':
        case 'bag_straat_adressenmultiple':
        case 'geolatlon':
        case 'googlemaps':
            return attribute.$('.value-list').getText();

        case 'bankaccount':
        case 'date':
        case 'email':
        case 'image_from_url':
        case 'text':
        case 'text_uc':
        case 'numeric':
        case 'url':
        case 'valuta':
        case 'valutaex':
        case 'valutaex21':
        case 'valutaex6':
        case 'valutain':
        case 'valutain21':
        case 'valutain6':
            return attribute.$('.value-list input').getAttribute('value');

        case 'checkbox': {
            const checkboxes = attribute.$$('input');

            return checkboxes.map(checkbox =>
                checkbox
                    .getAttribute('checked')
                    .then(checked => checked === 'true')
            );
        }

        case 'filemultiple': {
            const files = attribute.$$('.value-list .file-list-item');

            return files.map(file =>
                file.getText()
            );
        }

        case 'numericmultiple':
        case 'textmultiple':
        case 'text_ucmultiple': {
            let fields = attribute.$$('input');

            return fields.map(field =>
                field.getAttribute('value')
            );
        }

        case 'option': {
            const checkedOption = attribute.$('input:checked');

            return checkedOption.getAttribute('value');
        }

        case 'richtext':
            return attribute.$('.value-list .ql-editor').getText();

        case 'select':
            return attribute.$('.value-list select').getAttribute('value');

        case 'selectmultiple': {
            const selects = attribute.$$('select');

            return selects.map(selectOptions =>
                selectOptions.getAttribute('value')
            );
        }

        case 'textarea':
            return attribute.$('.value-list textarea').getAttribute('value');

        case 'textareamultiple': {
            const fields = attribute.$$('textarea');

            return fields.map(field =>
                field.getAttribute('value')
            );
        }

        default:
        break;

        }
    });
};

export const getClosedValue = attribute =>
    attribute
        .$('.value-list')
        .getText();

export const getValutaDisplay = attribute =>
    attribute
        .$('zs-btw-display')
        .getText();

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/

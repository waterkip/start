import {
    openPage,
    openPageAs
} from './../../../functions/common/navigate';
import waitForElement from './../../../functions/common/waitForElement';
import {
    createContactMoment
} from './../../../functions/intern/plusMenu';
import {
    openTab
} from './../../../functions/intern/caseView/caseNav';

describe('when creating a contact moment', () => {
    const newContactMoment = {
        type: 'natuurlijk_persoon',
        bsn: '111222333',
        case: '43',
        message: 'this is my contact moment'
    };

    beforeAll(() => {
        openPageAs();
        createContactMoment(newContactMoment);
    });

    it('should display a success message', () => {
        expect($('.snack-message-content').getText()).toContain(newContactMoment.case);
    });

    describe('and when opening the timeline of the case', () => {
        beforeAll(() => {
            openPageAs('admin', `/intern/zaak/${(newContactMoment.case)}`);
            waitForElement('.case-view');
            browser.ignoreSynchronization = true;
            openTab('timeline');
            waitForElement('[data-event-type="subject/contactmoment/create"]');
        });

        it('the contact moment should be present', () => {
            expect($('[data-event-type="subject/contactmoment/create"]').isPresent()).toBe(true);
        });

        it('the contact moment should contain the message', () => {
            const contactMomentMessage = $('[data-event-type="subject/contactmoment/create"] .timeline-item-content div:nth-child(2) pre');

            expect(contactMomentMessage.getText()).toEqual(newContactMoment.message);
        });

        afterAll(() => {
            browser.ignoreSynchronization = false;
            openPage();
        });
    });
});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/

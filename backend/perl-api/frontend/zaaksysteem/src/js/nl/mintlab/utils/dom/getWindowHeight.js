// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

(function () {
  window.zsDefine('nl.mintlab.utils.dom.getWindowHeight', function () {
    var win = window,
      doc = document.documentElement,
      body = document.body,
      func;

    if ('innerHeight' in win) {
      func = function () {
        return win.innerHeight;
      };
    } else {
      func = function () {
        return Math.min(doc.clientHeight, body.clientHeight);
      };
    }

    return func;
  });
})();

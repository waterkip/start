// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular
    .module('Zaaksysteem.admin')
    .controller('nl.mintlab.admin.EditTextFieldController', [
      '$scope',
      function ($scope) {
        $scope.addField = function () {
          $scope.values.push('');
        };

        $scope.editField = function (index, value) {
          $scope.values[index] = value;
        };

        $scope.removeField = function (index) {
          var oldArray = $scope.values;
          var newArray = oldArray
            .slice(0, index)
            .concat(oldArray.slice(index + 1));

          $scope.values = newArray;
        };
      },
    ]);
})();

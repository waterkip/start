// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular
    .module('Zaaksysteem.pip')
    .controller('nl.mintlab.pip.PipFileController', [
      '$scope',
      '$window',
      function ($scope, $window) {
        $scope.downloadFile = function (event) {
          var url =
            '/pip/zaak/' +
            $scope.caseId +
            '/document/' +
            $scope.doc.id +
            '/download';
          $window.open(url);
          event.stopPropagation();
        };
      },
    ]);
})();

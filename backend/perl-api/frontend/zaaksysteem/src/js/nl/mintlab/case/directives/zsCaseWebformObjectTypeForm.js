// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem.case').directive('zsCaseWebformObjectTypeForm', [
    'objectFormService',
    'systemMessageService',
    'formService',
    function (objectFormService, systemMessageService, formService) {
      var generateUid = window.zsFetch('nl.mintlab.utils.generateUid');

      return {
        require: ['zsCaseWebformObjectTypeForm', '^zsCaseWebformRuleManager'],
        templateUrl: '/widgets/general/veldoptie/objecttype/form.html',
        controller: [
          '$scope',
          '$attrs',
          function ($scope, $attrs) {
            var ctrl = this,
              config = null,
              loading = true,
              objectTypeId,
              zsCaseWebformRuleManager;

            function setValues() {
              var values = angular.copy(
                  $scope.$eval($attrs.zsCaseWebformObjectTypeFormValues)
                ),
                defaults = {},
                attrNames = zsCaseWebformRuleManager.getAttributeNames(),
                fields = {};

              defaults = _.assign(
                angular.copy(zsCaseWebformRuleManager.getValues()),
                _.mapValues(_.keyBy(attrNames), function (value, key) {
                  return zsCaseWebformRuleManager.getValue(key);
                })
              );

              _.each(config.fieldsets, function (fieldset) {
                _.each(fieldset.fields, function (field) {
                  fields[field.name] = field;
                });
              });

              _.each(values, function (value, key) {
                var field = fields[key];

                if (field) {
                  field.value = formService.formatValue(field.type, value);
                }
              });

              _.each(defaults, function (value, key) {
                var field = fields[key];

                if (field) {
                  // case values use another format than mutation API
                  // mutation API doesn't accept arrays where a single
                  // value is expected and vice versa. case api demands
                  // single value if array.length === 1

                  if (
                    (field.type === 'select' || field.type === 'radio') &&
                    _.isArray(value)
                  ) {
                    value = value[0];
                  } else if (
                    (field.type === 'checkbox' ||
                      field.type === 'checkbox-list') &&
                    !_.isArray(value)
                  ) {
                    value = [value];
                  } else if (
                    _.isArray(value) &&
                    value.length === 1 &&
                    field.type !== 'checkbox-list'
                  ) {
                    value = value[0];
                  }

                  field['default'] = formService.formatValue(field.type, value);
                }
              });
            }

            ctrl.link = function (controllers) {
              zsCaseWebformRuleManager = controllers[0];
            };

            ctrl.getForm = function () {
              return config;
            };

            ctrl.isLoading = function () {
              return loading;
            };

            ctrl.getValues = function () {
              var form, values;

              if (config) {
                form = formService.get(config.name);
                values = form.scope.getValues(false);
                _.each(values, function (value, key) {
                  var field;

                  if (value === undefined || value === null) {
                    delete values[key];
                  } else {
                    field = _.find(config.fieldsets[0].fields, { name: key });
                    if (field) {
                      values[key] = formService.parseValue(field.type, value);
                    }
                  }
                });
              }

              return values;
            };

            ctrl.isValid = function () {
              return true;
            };

            $scope.$watch($attrs.zsCaseWebformObjectTypeFormOpen, function (
              nwValue,
              oldValue
            ) {
              if (nwValue && !oldValue && config) {
                setValues();
              }
            });

            objectTypeId = $scope.$eval($attrs.objectTypeId);

            objectFormService
              .getForm(objectTypeId)
              .then(
                function (f) {
                  config = angular.copy(f);
                  config.name = generateUid()
                    .replace(/-/g, '_')
                    .replace(/^\d+/, '');

                  _.each(config.fieldsets, function (fieldset) {
                    _.each(
                      _.filter(fieldset.fields, { type: 'checkbox-list' }),
                      function (field) {
                        field.data.maxVisible = 50;
                      }
                    );
                  });

                  setValues();
                },
                function () {
                  systemMessageService.emitLoadError('object gegevens');
                }
              )
              ['finally'](function () {
                loading = false;
              });

            return ctrl;
          },
        ],
        controllerAs: 'caseWebformObjectTypeForm',
        link: function (scope, element, attrs, controllers) {
          controllers[0].link(controllers.slice(1));
        },
      };
    },
  ]);
})();

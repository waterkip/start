// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular
    .module('Zaaksysteem.pip')
    .controller('nl.mintlab.pip.PipSkipAreaController', [
      '$scope',
      '$element',
      function ($scope, $element) {
        const focusHandler = (event) => {
          event.preventDefault();
          document.querySelector('#content').contentEditable = true;
          document.querySelector('#content').focus();
          document.querySelector('#content').contentEditable = false;
        };

        $element.on('click', focusHandler);
        $scope.$on('$destroy', () => {
          element.off('click', focusHandler);
        });
      },
    ]);
})();

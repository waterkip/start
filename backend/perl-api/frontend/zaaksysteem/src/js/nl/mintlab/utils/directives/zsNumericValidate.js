// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  'use strict';
  angular.module('Zaaksysteem').directive('zsNumericValidate', [
    '$parse',
    function ($parse) {
      function isValidNumeric(value) {
        return (
          typeof value === 'string' &&
          /^-?\d+$/.test(value) &&
          value.length <= 18
        );
      }

      return {
        restrict: 'A',
        require: ['ngModel', '?^zsCaseWebformNumericField'],
        scope: false,
        link: function (scope, element, attrs, controllers) {
          var ngModel = controllers[0],
            zsCaseWebformNumericField = controllers[1];

          if (zsCaseWebformNumericField) {
            zsCaseWebformNumericField.setNumericValidate({
              setValue: function (value) {
                $parse(attrs.ngModel).assign(scope, value);
              },
            });
          }

          var validator = function (value) {
            // For now required fields checking has to be done by the backend
            // therefore empty is considered valid by this validator.
            // $isEmpty considers NaN, undefined and null empty as well,
            // though only empty string should count. Chose $isEmpty to
            // follow common angularjs pattern.
            var valid = ngModel.$isEmpty(value) || isValidNumeric(value);
            ngModel.$setValidity('zsNumericValidate', valid);
            return valid ? value : undefined;
          };

          ngModel.$parsers.unshift(validator);
          ngModel.$formatters.unshift(validator);

          if (zsCaseWebformNumericField) {
            scope.$watch('$destroy', function () {
              zsCaseWebformNumericField.unsetNumericValidate();
            });
          }
        },
      };
    },
  ]);
})();

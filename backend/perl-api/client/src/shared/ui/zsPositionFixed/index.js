// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import controller from './controller';

module.exports = angular
  .module('zsPositionFixed', [])
  .directive('zsPositionFixed', [
    '$document',
    '$animate',
    ($document, $animate) => {
      return {
        restrict: 'A',
        link: (scope, element, attrs) => {
          const positioner = controller(
            scope,
            element,
            $document,
            $animate,
            scope.$eval(attrs.zsPositionFixed)
          );

          scope.$evalAsync(() => {
            positioner.enable();
            scope.$on('$destroy', positioner.disable);
          });
        },
      };
    },
  ]).name;

// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import uiViewTransitionModule from './../../uiViewTransition';
import zsNProgressModule from './..';

module.exports = angular
  .module('zsUiViewProgress', [uiViewTransitionModule, zsNProgressModule])
  .directive('zsUiViewProgress', [
    'uiViewTransitionService',
    (uiViewTransitionService) => {
      return {
        restrict: 'E',
        template: `<zs-n-progress
						data-active="vm.isTransitioning()"
						show-spinner="vm.showSpinner()"
					>
					</zs-n-progress>`,
        scope: {
          showSpinner: '&',
          viewName: '@',
        },
        bindToController: true,
        controller: [
          function () {
            let ctrl = this;

            ctrl.isTransitioning = () =>
              uiViewTransitionService.isViewTransitioning(ctrl.viewName || '');
          },
        ],
        controllerAs: 'vm',
      };
    },
  ]).name;

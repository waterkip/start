// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import zsKeyboardNavigableList from '../zsKeyboardNavigableList';
import template from './template.html';
import controller from './controller';

module.exports = angular
  .module('shared.ui.zsSuggestionList', [zsKeyboardNavigableList])
  .component('zsSuggestionList', {
    bindings: {
      keyInputDelegate: '&',
      suggestions: '&',
      onSelect: '&',
      label: '@',
    },
    controller,
    template,
  }).name;

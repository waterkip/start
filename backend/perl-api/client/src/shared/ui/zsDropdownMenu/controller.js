// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import assign from 'lodash/assign';
import uniqueId from 'lodash/uniqueId';
import defaultsDeep from 'lodash/defaultsDeep';

export default function ($document, $scope, $element, $animate, capabilities) {
  let ctrl = this,
    isOpen = false,
    mode = typeof ctrl.mode === 'function' ? ctrl.mode() : null,
    onOpen = ctrl.onOpen || (() => {}),
    onClose = ctrl.onClose || (() => {}),
    id = uniqueId('dropdown'),
    positionOptions = defaultsDeep(
      {},
      ctrl.positionOptions ? ctrl.positionOptions() : null,
      {
        attachment: 'top right',
        target: 'bottom right',
        autosize: false,
        reference: $element.find('button').eq(0),
      }
    );

  const closeOnClick = this.autoClose !== false;

  if (!mode || capabilities.touch) {
    mode = 'click';
  }

  let handleDocumentClick = (event) => {
    if ($element[0] !== event.target && !$element[0].contains(event.target)) {
      $scope.$apply(ctrl.closeMenu);
    }
  };

  let handleKeyUp = (event) => {
    if (event.keyCode === 27) {
      $scope.$apply(ctrl.closeMenu);
    }
  };

  let addListeners = () => {
    $document[0].addEventListener('click', handleDocumentClick, true);
    $document.bind('keyup', handleKeyUp);
  };

  let removeListeners = () => {
    $document[0].removeEventListener('click', handleDocumentClick, true);
    $document.unbind('keyup', handleKeyUp);
  };

  ctrl.openMenu = (event) => {
    if (!isOpen) {
      isOpen = true;

      if (mode === 'click') {
        addListeners();
      }

      onOpen({ $event: event });
    }
  };

  ctrl.closeMenu = (event) => {
    if (isOpen) {
      isOpen = false;
      if (mode === 'click') {
        removeListeners();
      }

      onClose({ $event: event });
    }
  };

  ctrl.toggleMenu = ($event) => {
    if (isOpen) {
      ctrl.closeMenu($event);
    } else {
      ctrl.openMenu($event);
    }

    $event.stopPropagation();
    $event.preventDefault();
  };

  ctrl.getButtonLabel = () => ctrl.buttonLabel;

  ctrl.getButtonStyle = () => {
    return assign(
      {
        'popup-menu-button-active': ctrl.isMenuOpen(),
      },
      ctrl.buttonStyle ? ctrl.buttonStyle() : {}
    );
  };

  ctrl.isMenuOpen = () => isOpen;

  ctrl.id = () => id;

  ctrl.handleOptionClick = (option, $event) => {
    let result = option.click ? option.click($event) : null;

    if (closeOnClick) {
      if (result && result.then && typeof result.then === 'function') {
        result.then(ctrl.closeMenu);
      } else if (mode === 'click') {
        ctrl.closeMenu($event);
      }
    }

    if (option.type !== 'link') {
      $event.stopPropagation();
      $event.preventDefault();
    }
  };

  ctrl.getPositionOptions = () => positionOptions;

  if (mode === 'hover') {
    $element.bind('mouseenter', (event) => {
      $scope.$apply(ctrl.openMenu.bind(null, event));
    });

    $element.bind('mouseleave', (event) => {
      $scope.$apply(ctrl.closeMenu.bind(null, event));
    });
  }

  $scope.$on('$destroy', () => {
    removeListeners();
  });
}

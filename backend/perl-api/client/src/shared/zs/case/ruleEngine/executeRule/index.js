// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import matchRule from './../matchRule';
import evaluateFormula from './evaluateFormula';
import defaults from 'lodash/defaults';
import merge from 'lodash/merge';
import partition from 'lodash/partition';
import get from 'lodash/get';
import propCheck from './../../../../util/propCheck';
import matchCondition from './../matchRule/matchCondition';
import calculateDate from './calculateDate';

export default (rule, current, options = {}) => {
  propCheck.throw(
    propCheck.shape({
      debug: propCheck.bool.optional,
    }),
    options
  );

  let matches = matchRule(rule, current.values, current.hidden),
    actions = matches ? rule.then : rule.else,
    debug = !!options.debug,
    debugInfo = current.debug;

  let addDebug = (attrName, type) => {
    debugInfo = debugInfo ? debugInfo : {};

    if (!debugInfo[attrName]) {
      debugInfo[attrName] = [];
    }

    debugInfo[attrName].push({
      rule: {
        label: rule.label,
        matches,
      },
      type,
    });
  };

  return actions.reduce((result, action) => {
    let attrName = action.data.attribute_name;
    let attrType = get(action, 'data.attribute_type');
    let itemsToBeHidden;

    switch (action.type) {
      case 'set_value_magic_string':
        // You can also look at action.data.can_change
        // but for now the rule is always "cannot change"
        result.disabled[attrName] = true;
        break;

      case 'set_value':
        {
          let value = action.data.value === '' ? null : action.data.value;

          if (attrType === 'number') {
            value =
              action.data.value === null ? null : Number(action.data.value);
          }

          // as a result of some feature, the 'rules' API changed with regards to results
          // the case call returns the value of a result as 'result_id: <number>'
          // and the rules call now returns the value of a result as 'result: { id: <number> }'
          // the if-statement below performs the necessary conversion
          if (attrType === 'result') {
            attrName = 'case.result_id';
            value = get(action, 'data.value.id');
          }

          if (!action.data.can_change) {
            result.values[attrName] = value;
            result.disabled[attrName] = true;
            if (debug) {
              addDebug(attrName, 'set_value');
            }
          } else {
            result.prefill[attrName] = value;
            if (debug) {
              addDebug(attrName, 'set_value_editable');
            }
          }
        }
        break;

      case 'hide_attribute':
      case 'show_attribute':
        // only unhide attribute if it isn't hidden by group

        result.hidden[attrName] =
          action.type === 'hide_attribute' || result.hiddenByGroup[attrName];
        result.hiddenByAttribute[attrName] = action.type === 'hide_attribute';

        if (debug) {
          addDebug(attrName, action.type);
        }

        break;

      case 'hide_group':
      case 'show_group':
        if (debug) {
          addDebug(attrName, action.type);
        }

        itemsToBeHidden = get(action, 'data.related_attributes', [])
          .concat(get(action, 'data.related_objects', []))
          .concat(
            get(action, 'data.related_text_blocks', []).map(
              (blockId) => `text_block_${blockId}`
            )
          );

        itemsToBeHidden.forEach((attr) => {
          result.hiddenByGroup[attr] = action.type === 'hide_group';

          // only unhide attribute if it wasn't hidden individually
          if (
            action.type === 'hide_group' ||
            result.hiddenByAttribute[attr] !== true
          ) {
            result.hidden[attr] = action.type === 'hide_group';
          }

          if (debug) {
            addDebug(attr, action.type);
          }
        });
        break;

      case 'hide_text_block':
      case 'show_text_block':
        attrName = `text_block_${action.data.attribute_name}`;

        result.hidden[attrName] =
          action.type === 'hide_text_block' || result.hiddenByGroup[attrName];
        result.hiddenByAttribute[attrName] = action.type === 'hide_text_block';

        if (debug) {
          addDebug(attrName, action.type);
        }
        break;

      case 'set_value_formula':
        result.values[attrName] = evaluateFormula(
          action.data.formula,
          result.values
        );
        result.disabled[attrName] = true;

        if (debug) {
          addDebug(attrName, action.type);
        }

        break;

      case 'set_allocation':
        result.allocation = {
          unit: String(action.data.group_id),
          role: String(action.data.role_id),
        };
        break;

      case 'pause_application':
        {
          let conditions = partition(
              rule.conditions.conditions.filter(
                (condition) => !!condition.attribute_name
              ),
              (condition) =>
                matchCondition(condition, current.values, current.hidden)
            ),
            conditionsToMap = matches ? conditions[0] : conditions[1];

          result.pause_application = merge(action.data, {
            attribute_names: conditionsToMap.map(
              (condition) => condition.attribute_name
            ),
          });
        }

        break;

      case 'date_calculations': {
        addDebug(action.data.target_attribute, action.type);

        const sourceAttr = action.data.source_attribute;
        const targetAttr = action.data.target_attribute;
        let baseValue = result.values[sourceAttr];

        const noValue = !baseValue;
        const noValueAsArray = Array.isArray(baseValue) && !baseValue[0];

        if (noValue || noValueAsArray) break;

        const country = 'nl';
        const institutionType = 'government';

        const date = calculateDate(
          baseValue,
          action.data.math,
          country,
          institutionType
        );

        result.values[targetAttr] = date;
        result.disabled[targetAttr] = true;
        break;
      }
    }

    return result;
  }, defaults(current, { hidden: {}, hiddenByGroup: {}, hiddenByAttribute: {}, values: {}, disabled: {}, prefill: {}, debug: {} }));
};

// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import resourceReducerModule from './../resourceReducer';
import first from 'lodash/head';
import tail from 'lodash/tail';
import ComposedReducer from './../resourceReducer/ComposedReducer';
import BaseReducer from './../resourceReducer/BaseReducer';
import createReducerFromValue from './createReducerFromValue';
import createReducerFromFunction from './createReducerFromFunction';
import ApiResource from './../ApiResource';
import createReducerFromResource from './createReducerFromResource';
import PromiseReducer from './../resourceReducer/PromiseReducer';
import defaultsDeep from 'lodash/defaultsDeep';

module.exports = angular
  .module('composedReducer', [resourceReducerModule])
  .factory('composedReducer', [
    () => {
      return (...rest) => {
        let options = first(rest),
          children = tail(rest),
          reducer;

        if (typeof options === 'function' || options instanceof BaseReducer) {
          options = {};
          children = [options].concat(children);
        }

        if (options.scope === undefined) {
          if (ENV.IS_DEV) {
            //eslint-disable-line
            console.warn(
              'Using a composedReducer without a scope is deprecated.'
            );
            console.trace();
          }
        } else {
          options.scope.$on('$destroy', () => {
            reducer.destroy();
          });
        }

        options = defaultsDeep(
          { eager: options.waitUntilResolved === false },
          options
        );

        reducer = new ComposedReducer(
          options,
          children.map((child) => {
            if (child instanceof BaseReducer) {
              return child;
            }

            if (child instanceof ApiResource) {
              return createReducerFromResource(child);
            }

            if (typeof child === 'function') {
              return createReducerFromFunction(options.scope, child);
            }

            if (child && typeof child.then === 'function') {
              return new PromiseReducer(child);
            }

            return createReducerFromValue(child);
          })
        );

        reducer.data = () => reducer.value();

        reducer.onUpdate = (fn, opts) => {
          if (ENV.IS_DEV) {
            //eslint-disable-line
            console.warn(
              'ComposedReducer.onUpdate is deprecated. Please use ComposedReducer.subscribe()'
            );
          }

          return reducer.subscribe(fn, opts);
        };

        if (options.scope && options.mode === 'hot') {
          options.scope.$watch(reducer.data);
        }

        return reducer;
      };
    },
  ]).name;

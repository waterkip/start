// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import template from './template.html';
import get from 'lodash/get';
import intersection from 'lodash/intersection';
import first from 'lodash/head';
import once from 'lodash/once';
import './styles.scss';

const APP_ID = 'yb14xufd';
const DEV_APP_ID = 'r2tqi722';

const ROLES = ['Administrator', 'Zaaksysteembeheerder'];

module.exports = angular.module('zsIntercom', []).component('zsIntercom', {
  template,
  bindings: {
    user: '<',
    company: '<',
    development: '<',
    instanceId: '<',
    onOpen: '&',
    onClose: '&',
    open: '<',
  },
  controller: [
    '$rootScope',
    '$window',
    '$document',
    '$interval',
    '$q',
    function ($rootScope, $window, $document, $interval, $q) {
      let ctrl = this,
        appId,
        i = function () {
          i.c(arguments);
        },
        initialized;

      i.q = [];
      i.c = (args) => {
        i.q.push(args);
      };

      $window.Intercom = i;

      let call = (name, ...rest) => {
        $window.Intercom(name, ...rest); //eslint-disable-line
      };

      let updateSettings = () => {
        if (ctrl.user) {
          call('update');
        }
      };

      let initSettings = () => {
        let user = ctrl.user,
          company = ctrl.company;

        $window.intercomSettings = user
          ? {
              app_id: appId,
              name: user.display_name,
              first_name: user.given_name,
              last_name: user.surname,
              company: company.instance.company,
              user_id: `${ctrl.instanceId}-${user.id}`,
              email: get(user, 'email'),
              system_role: first(user.system_roles),
              system_roles: user.system_roles.join(','),
            }
          : null;

        if (user) {
          $window.Intercom('reattach_activator'); //eslint-disable-line
          $window.Intercom('update', $window.intercomSettings); //eslint-disable-line
        }
      };

      let load = once(() => {
        let script = $document[0].createElement('script');

        appId = !ctrl.development ? APP_ID : DEV_APP_ID;

        script.src = `https://widget.intercom.io/widget/${appId}`;

        $document.find('body').append(script);

        initSettings();

        $rootScope.$on('$stateChangeSuccess', () => {
          updateSettings();
        });

        return $q((resolve) => {
          let intervalId = $interval(
            () => {
              let intercomLauncher = $document[0].querySelector(
                  '#intercom-container'
                ),
                intercomMessenger,
                isOpen;

              if (intercomLauncher) {
                $interval.cancel(intervalId);

                $document.find('body').append(intercomLauncher);

                intercomMessenger = intercomLauncher.querySelector(
                  '.intercom-messenger'
                );

                if (
                  intercomMessenger &&
                  angular
                    .element(intercomMessenger)
                    .hasClass('intercom-messenger-active')
                ) {
                  isOpen = true;
                }

                call('onShow', () => {
                  ctrl.onOpen();
                });

                call('onHide', () => {
                  ctrl.onClose();
                });

                resolve(isOpen);
              }
            },
            100,
            false
          );
        });
      });

      let evaluate = () => {
        if (
          ctrl.user &&
          ctrl.company &&
          typeof ctrl.development === 'boolean'
        ) {
          load().then((openOnInit) => {
            if (!initialized) {
              initialized = true;

              if (openOnInit) {
                ctrl.onOpen();
              } else {
                ctrl.onClose();
              }
            } else {
              if (ctrl.open) {
                call('show');
              } else {
                call('hide');
              }
            }
          });
        }
      };

      ctrl.isVisible = () => {
        return intersection(ROLES, get(ctrl.user, 'system_roles')).length > 0;
      };

      ctrl.$onChanges = () => {
        evaluate();
      };

      if (ctrl.user && ctrl.company) {
        evaluate();
      }
    },
  ],
}).name;

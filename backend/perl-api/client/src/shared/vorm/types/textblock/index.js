// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import vormTemplateServiceModule from './../../vormTemplateService';
import zsTruncateHtmlModule from './../../../ui/zsTruncate/zsTruncateHtml';

module.exports = angular
  .module('vorm.types.textblock', [
    vormTemplateServiceModule,
    zsTruncateHtmlModule,
  ])
  .run([
    'vormTemplateService',
    function (vormTemplateService) {
      vormTemplateService.registerType('text_block', {
        control: angular.element(
          '<zs-truncate-html ng-model data-value="delegate.value"></zs-truncate-html>'
        ),
        display: angular.element(
          '<zs-truncate-html data-value="delegate.value"></zs-truncate-html>'
        ),
        defaults: {
          editMode: 'empty',
          disableClear: true,
        },
      });
    },
  ]).name;

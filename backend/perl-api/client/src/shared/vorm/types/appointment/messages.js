// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
export const APPOINTMENT_CREATE_PENDING = 'Afspraak wordt ingepland';
export const APPOINTMENT_CREATE_ERROR =
  'Afspraak kon niet worden ingepland. Neem contact op met uw beheerder voor meer informatie.';
export const APPOINTMENT_CREATE_THROW =
  'API reported it couldn’t book appointment successfully.';
/**
 * @param {string} date
 * @param {string} timeStart
 * @param {string} timeEnd
 * @return {string}
 */
export const appointmentCreateSuccess = (date, timeStart, timeEnd) =>
  `U heeft een afspraak ingepland op ${date} van ${timeStart} tot ${timeEnd}`;

export const APPOINTMENT_READ_PENDING = 'Afspraakinformatie wordt opgehaald';

export const APPOINTMENT_REMOVE_PENDING = 'Afspraak wordt verwijderd';
export const APPOINTMENT_REMOVE_ERROR =
  'Afspraak kon niet worden verwijderd. Neem contact op met uw beheerder voor meer informatie';

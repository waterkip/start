// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import angularSanitize from 'angular-sanitize';
import vormTemplateServiceModule from './../../vormTemplateService';
import zsRichTextAreaModule from './../../../ui/zsRichTextArea';
import zsTruncateHtmlModule from './../../../ui/zsTruncate/zsTruncateHtml';

module.exports = angular
  .module('vorm.types.richText', [
    vormTemplateServiceModule,
    zsRichTextAreaModule,
    zsTruncateHtmlModule,
    angularSanitize,
  ])
  .directive('vormRichTextArea', [
    () => {
      return {
        restrict: 'E',
        template: `<zs-rich-text-area
						data-html="vormRichTextArea.getHtml()"
						data-on-change="vormRichTextArea.onHtmlChange($html)"
					></zs-rich-text-area>`,
        require: ['vormRichTextArea', 'ngModel'],
        controller: [
          function () {
            let ctrl = this,
              ngModel;

            ctrl.link = (...rest) => {
              [ngModel] = rest;
            };

            ctrl.getHtml = () => ngModel.$viewValue;

            ctrl.onHtmlChange = (html) => {
              ngModel.$setViewValue(html);
            };
          },
        ],
        controllerAs: 'vormRichTextArea',
        link: (scope, element, attrs, controllers) => {
          controllers.shift().link(...controllers);
        },
      };
    },
  ])
  .run([
    'vormTemplateService',
    (vormTemplateService) => {
      vormTemplateService.registerType('rich-text', {
        control: angular.element(
          '<vorm-rich-text-area ng-model></vorm-rich-text-area>'
        ),
        display: angular.element(
          '<zs-truncate-html data-value="delegate.value" data-length=500></zs-truncate-html>'
        ),
      });
    },
  ]).name;

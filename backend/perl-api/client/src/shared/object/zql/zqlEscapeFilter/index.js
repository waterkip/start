// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import filter from './filter';

module.exports = angular
  .module('shared.object.zql.zqlEscapeFilter', [])
  .filter('zqlEscape', [
    () => {
      return filter;
    },
  ]).name;

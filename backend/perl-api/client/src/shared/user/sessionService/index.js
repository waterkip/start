// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import resourceModule from './../../api/resource';
import first from 'lodash/head';

module.exports = angular
  .module('sessionService', [resourceModule])
  .factory('sessionService', [
    'resource',
    (resource) => {
      let sessionService = {};

      sessionService.createResource = (scope, options) => {
        return resource(
          {
            url: '/api/v1/session/current',
            headers: {
              'Disable-Digest-Authentication': 1,
            },
          },
          {
            scope,
            options,
          }
        ).reduce((requestOptions, data) => first(data));
      };

      return sessionService;
    },
  ]).name;

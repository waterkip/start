// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import findIndex from 'lodash/findIndex';

export default (steps, currentGroup) => {
  let index = findIndex(steps, (step) => step.name === currentGroup.name),
    isFirst = index === 0,
    isLast = index === steps.length - 1;

  return [
    {
      name: 'previous',
      type: 'button',
      label: isFirst ? 'Sluiten' : 'Vorige',
      classes: {
        'btn-flat': true,
      },
    },
    {
      name: 'next',
      type: 'submit',
      label: isLast ? 'Versturen' : 'Volgende',
      classes: {
        'btn-primary': true,
      },
    },
  ];
};

// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import get from 'lodash/get';

// v1 to v2
const subjectTypeDict = {
  person: 'person',
  company: 'organization',
  employee: 'employee',
};

const formatRequestor = (requestor) => {
  const subjectType = get(requestor, 'instance.subject_type');

  return {
    type: subjectTypeDict[subjectType],
    id: get(requestor, 'reference'),
  };
};

export default formatRequestor;

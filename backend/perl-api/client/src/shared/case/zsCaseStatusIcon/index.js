// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import caseStatusLabelFilterModule from './../caseStatusLabelFilter';
import zsNotificationCounterModule from './../../ui/zsNotificationCounter';
import zsIconModule from './../../ui/zsIcon';
import template from './template.html';

module.exports = angular
  .module('zsCaseStatusIcon', [
    caseStatusLabelFilterModule,
    zsIconModule,
    zsNotificationCounterModule,
  ])
  .directive('zsCaseStatusIcon', [
    () => {
      return {
        restrict: 'E',
        template,
        scope: {
          status: '&',
          updates: '&',
          archivalState: '&',
          destructable: '&',
        },
        bindToController: true,
        controller: [
          function () {
            let ctrl = this;

            const count = ctrl.updates();
            const plural = count > 1 ? 's' : '';

            ctrl.title = count
              ? `U heeft ${count} notificatie${plural} op deze zaak`
              : '';

            ctrl.getIcon = () => {
              let status = ctrl.status(),
                archivalState = ctrl.archivalState(),
                icon;

              if (archivalState === 'overdragen') {
                icon = 'overdragen';
              } else if (
                ctrl.destructable() &&
                status === 'resolved' &&
                archivalState === 'vernietigen'
              ) {
                icon = 'deleted';
              } else {
                icon = status;
              }

              return icon;
            };
          },
        ],
        controllerAs: 'vm',
      };
    },
  ]).name;

// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import padLeft from '../pad/padLeft';

/**
 * @param {Date} date
 * @param {string} pattern
 * @returns {Object}
 * @property {string} year
 * @property {string} month
 * @property {string} day
 */
function extract(date, pattern) {
  const newDate = new Date(date);

  return {
    get year() {
      return newDate.getFullYear();
    },

    get month() {
      const month = String(newDate.getMonth() + 1);

      return padLeft(month, pattern);
    },

    get day() {
      const day = String(newDate.getDate());

      return padLeft(day, pattern);
    },
  };
}

/**
 * Format a date object as local ISO 8601 calendar date.
 *
 * Cf. {@link fromIsoCalendarDate}
 *
 * @param {Date} dateObject
 * @returns {string}
 */
export default function toIsoCalendarDate(dateObject) {
  const { year, month, day } = extract(dateObject, '00');
  const isoCalendarDate = [year, month, day].join('-');

  return isoCalendarDate;
}

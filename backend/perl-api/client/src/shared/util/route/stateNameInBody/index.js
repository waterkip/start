// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import angularUiRouterModule from 'angular-ui-router';
import getActiveStates from './../getActiveStates';
import includes from 'lodash/includes';

module.exports = angular
  .module('stateNameInBody', [angularUiRouterModule])

  .run([
    '$document',
    '$rootScope',
    '$animate',
    '$state',
    ($document, $rootScope, $animate, $state) => {
      let body = $document.find('body'),
        currentNames = [];

      let getParentStates = () => {
        return getActiveStates($state.$current)
          .reverse()
          .map((state) => state.self);
      };

      let setClasses = (names) => {
        currentNames
          .filter((name) => !includes(names, name))
          .forEach((name) => {
            $animate.removeClass(body, name);
          });

        currentNames = names;

        currentNames.forEach((name) => {
          $animate.addClass(body, name);
        });
      };

      let startTransition = (targetState) => {
        setClasses(
          getParentStates()
            .concat(targetState)
            .map((state) => state.name)
        );
      };

      let stopTransition = () => {
        setClasses(getParentStates().map((state) => state.name));
      };

      $rootScope.$on('$stateChangeStart', (event, targetState) => {
        startTransition(targetState);
      });

      $rootScope.$on('$stateChangeError', stopTransition);

      $rootScope.$on('$stateChangeSuccess', stopTransition);
    },
  ]).name;

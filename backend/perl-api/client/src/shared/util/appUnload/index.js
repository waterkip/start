// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import invoke from 'lodash/invokeMap';

module.exports = angular
  .module('shared.util.appUnload', [])
  .factory('appUnload', [
    () => {
      let listeners = [];

      return {
        onUnload: (fn) => {
          listeners.push(fn);
        },
        triggerUnload: () => {
          invoke(listeners, 'call', null);
        },
      };
    },
  ]).name;

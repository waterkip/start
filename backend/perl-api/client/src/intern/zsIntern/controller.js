// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import zsTopBarModule from './../../shared/navigation/zsTopBar';
import zsClickOutsideModule from './../../shared/ui/zsClickOutside';
import zsContextualActionMenuModule from './../../shared/ui/zsContextualActionMenu';
import zsActiveSubjectModule from './zsActiveSubject';
import get from 'lodash/get';
import first from 'lodash/head';
import take from 'lodash/take';
import includes from 'lodash/includes';
import zsBackdropServiceModule from '../../shared/navigation/zsBackdropService';
import composedReducerModule from '../../shared/api/resource/composedReducer';

import './styles.scss';

module.exports = angular
  .module('zsInternController', [
    zsTopBarModule,
    zsClickOutsideModule,
    zsContextualActionMenuModule,
    zsBackdropServiceModule,
    zsActiveSubjectModule,
    composedReducerModule,
  ])
  .controller('zsInternController', [
    '$scope',
    'resource',
    'zsBackdropService',
    'composedReducer',
    function ($scope, resource, zsBackdropService, composedReducer) {
      let ctrl = this,
        userResource = resource(
          {
            url: '/api/v1/session/current',
            headers: { 'Disable-Digest-Authentication': 1 },
          },
          { scope: $scope, cache: { every: 15 * 60 * 1000 } }
        ).reduce((requestOptions, data) => first(data)),
        development,
        instanceId,
        isAllowedReducer = composedReducer(
          { scope: $scope },
          userResource
        ).reduce((user) => {
          const capabilities = user.instance.logged_in_user.capabilities;
          return (
            includes(capabilities, 'gebruiker') &&
            includes(capabilities, 'dashboard')
          );
        });

      ctrl.isContentVisible = () => true;

      ctrl.isMenuOpen = () => {
        return zsBackdropService.isEnabled();
      };

      ctrl.getCompany = () => get(userResource.data(), 'instance.account');

      ctrl.getUser = () => get(userResource.data(), 'instance.logged_in_user');

      userResource.onUpdate(() => {
        let spl = get(userResource.source(), 'request_id', '').split('-');

        development = !!get(userResource.source(), 'development', true);
        instanceId = take(spl, spl.length - 2).join('-');
      });

      ctrl.isDevelopment = () => development;

      ctrl.getInstanceId = () => instanceId;

      ctrl.isAllowed = isAllowedReducer.data;
    },
  ]).name;

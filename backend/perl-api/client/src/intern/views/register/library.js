// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { isArray } from 'lodash';
import get from 'lodash/get';

export const getAction = (data, casetype) => {
  // aggregate all actions from different phases
  const action = data.reduce((acc, phase) => {
    const actions = get(phase, 'items');

    if (!actions) {
      return acc;
    }

    return [...acc, ...actions];
  }, []);

  // find the relevant action by matching the uuids from
  // - the casetype of the form that we're in
  // - the casetype as defined in the action
  return action.find((action) => {
    const casetypeUuid = casetype.reference;

    return casetypeUuid === get(action, 'data.related_casetype_uuid');
  });
};

// the values of type checkbox are of format ['one', 'three']
// the form expects the format { 'one': true, 'two': false, 'three': true }
const mapCheckbox = (value) =>
  Object.fromEntries(value.map((key) => [key, true]));

const mapDate = (value) => new Date(value);

const mapFileFromCase = ({ filename, uuid }) => ({ filename, reference: uuid });
const mapFileFromObject = ({ label, value }) => ({
  filename: label,
  reference: value,
});

const mapRelation = ({ specifics, value }) => ({
  label: specifics.metadata.summary,
  id: value,
});

// values is of format = { attribute[parent_magicstring]: value }
//   and also contains non-attribute data
// we filter out the attributes to format = { parent_magicstring: value }
const formatValues = (values) =>
  Object.entries(values).reduce((acc, [key, value]) => {
    const [prefix, name] = key.split('.');

    if (prefix !== 'attribute') return acc;

    acc[name] = value;

    return acc;
  }, {});

// mapping is of format = { parent_magicstring: child_magicstring }
// values is of format  = { attribute[parent_magicstring]: value }
// note: 2 attributes can be mapped to 1 attribute. Last one wins.
const mapValues = (values, mapping) =>
  Object.keys(mapping).reduce((acc, nameParent) => {
    const nameChild = mapping[nameParent];

    acc[nameChild] = values[`attribute.${nameParent}`];

    return acc;
  }, {});

export const getValuesFromParent = (parentCase, action, casetype) => {
  const vals = parentCase.values;
  const shouldCopyAllAttributes = get(action, 'data.kopieren_kenmerken');
  const attributeMapping = get(action, 'data.copy_selected_attributes');

  // copy all attributes 'off' && no attribute mapping ==> no copy
  // copy all attribures 'on'  && no attribute mapping ==> copy all
  // copy all attributes 'on'  &&    attribute mapping ==> copy all, ignore mapping
  // copy all attributes 'off' &&    attribute mapping ==> copy according to mapping
  if (!shouldCopyAllAttributes && !attributeMapping) return null;

  const formattedValues = shouldCopyAllAttributes
    ? formatValues(vals)
    : mapValues(vals, attributeMapping);

  const fields = casetype.instance.phases.reduce(
    (acc, phase) => [...acc, ...phase.fields],
    []
  );

  let values = {};

  Object.entries(formattedValues).forEach(([key, value]) => {
    const attributeConfig = fields.find((field) => field.magic_string === key);
    const type = get(attributeConfig, 'type');

    if (!value) return;

    switch (type) {
      case 'checkbox': {
        values[key] = mapCheckbox(value);
        break;
      }
      case 'date': {
        values[key] = mapDate(value);
        break;
      }
      case 'file': {
        values[key] = value.map(mapFileFromCase);
        break;
      }
      case 'relationship': {
        if (!isArray(value)) {
          values[key] = [value].map(mapRelation);
        } else {
          values[key] = value[0].value.map(mapRelation);
        }
        break;
      }
      default: {
        values[key] = value;
        break;
      }
    }
  });

  return values;
};

export const getValuesFromCustomObject = (customObject) => {
  const fields = customObject.attributes.custom_fields;
  let values = {};

  Object.entries(fields).forEach(([key, field]) => {
    if (!field) return;

    const { type, value } = field;

    switch (type) {
      case 'checkbox': {
        values[key] = mapCheckbox(value);
        break;
      }
      case 'date': {
        values[key] = mapDate(value);
        break;
      }
      case 'document': {
        values[key] = value.map(mapFileFromObject);
        break;
      }
      case 'relationship': {
        values[key] = value.map(mapRelation);
        break;
      }
      default: {
        values[key] = value;
      }
    }
  });

  return values;
};

// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import propCheck from '../../../../../shared/util/propCheck';

export default (data) => {
  let capabilities = {};

  propCheck.throw(propCheck.shape({}), data);

  return {
    getDefaults: () => {
      return {};
    },
    processChange: (name, value, values) => {
      let vals = values;

      return vals.merge({ [name]: value });
    },
    getCapabilities: () => capabilities,
    fields: () => {
      return [
        {
          name: 'description',
          label: '',
          template: {
            inherits: 'text',
            control: () => {
              return angular.element(
                `<div ng-model>
                  <span>Objectmutatie nu uitvoeren?</span>
                </div>`
              );
            },
          },
          disabled: false,
          required: false,
        },
      ];
    },
  };
};

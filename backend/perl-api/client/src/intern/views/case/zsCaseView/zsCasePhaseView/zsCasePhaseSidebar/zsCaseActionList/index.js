// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import angularUiRouterModule from 'angular-ui-router';
import composedReducerModule from './../../../../../../../shared/api/resource/composedReducer';
import zsTooltipModule from './../../../../../../../shared/ui/zsTooltip';
import zsModalModule from './../../../../../../../shared/ui/zsModal';
import zsCaseActionFormModule from './zsCaseActionForm';
import snackbarServiceModule from './../../../../../../../shared/ui/zsSnackbar/snackbarService';

import controller from './CaseActionListController';
import template from './template.html';

module.exports = angular
  .module('zsCaseActionList', [
    angularUiRouterModule,
    composedReducerModule,
    zsTooltipModule,
    zsModalModule,
    zsCaseActionFormModule,
    snackbarServiceModule,
  ])
  .component('zsCaseActionList', {
    bindings: {
      actions: '&',
      actionsLoading: '&',
      caseId: '&',
      caseResource: '&',
      emailTemplateData: '&',
      onActionAutomaticToggle: '&',
      onActionUntaint: '&',
      onActionTrigger: '&',
      subjectRelations: '&',
      requestor: '&',
      phases: '&',
      templates: '&',
      caseDocuments: '&',
      disabled: '&',
      phaseState: '&',
    },
    controller,
    template,
  }).name;

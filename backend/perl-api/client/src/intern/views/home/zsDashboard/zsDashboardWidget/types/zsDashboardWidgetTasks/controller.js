// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import immutable from 'seamless-immutable';
import get from 'lodash/get';
import first from 'lodash/first';
import orderBy from 'lodash/orderBy';
import find from 'lodash/find';
import shortid from 'shortid';
import daysLeftTemplate from './days-left-template.html';

export default function (
  resource,
  composedReducer,
  sessionService,
  state,
  scope
) {
  let ctrl = this,
    userSort = immutable({});

  const userResource = sessionService.createResource(scope);
  const userId = get(userResource.data(), 'instance.logged_in_user.uuid', '');

  const tasksResource = resource(
    `/api/v2/cm/task/get_task_list?page_size=10000&filter[attributes.completed]=false&filter[relationships.assignee.id]=${userId}`,
    { scope }
  );

  const columnConfig = [
    {
      id: 'caseNumber',
      resolve: 'relationships.case.meta.display_number',
      sort: 'caseNumber',
      label: '#',
      template:
        '<a href="{{::item.href}}">{{::item.relationships.case.meta.display_number}}</a>',
    },
    {
      id: 'title',
      resolve: 'attributes.title',
      sort: 'title',
      label: 'Titel',
    },
    {
      id: 'deadline',
      resolve: 'attributes.due_date',
      filter: 'date',
      sort: 'deadline',
      label: 'Deadline',
    },
    {
      id: 'days',
      resolve: 'attributes.due_date',
      sort: 'deadline',
      template: daysLeftTemplate,
      label: 'Dagen',
    },
  ];

  const taskReducer = composedReducer(
    { scope },
    tasksResource,
    columnConfig,
    ctrl.filterQuery
  ).reduce((tasks, columns, query) => {
    return tasks
      .filter((item) => item.attributes.completed === false)
      .filter((item) => {
        if (!query) {
          return true;
        }

        return (
          columns
            .map((column) => get(item, column.resolve))
            .filter((value) => value && value.toString().indexOf(query) !== -1)
            .length > 0
        );
      })
      .map((item) => {
        const dueDate = item.attributes.due_date;

        if (item.attributes.due_date) {
          const oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
          const today = new Date().setHours(0, 0, 0);
          const [year, month, day] = dueDate
            .split('-')
            .map((part) => Number(part));
          const monthIndex = month - 1;
          const deadline = new Date(
            year,
            monthIndex,
            day,
            0,
            0,
            0,
            0
          ).getTime();
          const daysLeft = Math.round((deadline - today) / oneDay);

          return item.merge({
            attributes: item.attributes.merge({
              days_left: daysLeft,
            }),
          });
        }

        return item;
      });
  });

  const columnReducer = composedReducer({ scope }).reduce(() => {
    return columnConfig.map((columnToMap) => {
      let column = immutable(columnToMap),
        data = columnConfig[column.id] || {},
        resolve;

      column = column.merge(data);

      if (!column.template) {
        resolve = column.resolve
          ? `item.${column.resolve}`
          : `item.${column.id}`;

        if (column.filter) {
          resolve = `${resolve} | ${column.filter}`;
        }

        column = column.merge({
          template: `<span>{{::${resolve}}}</span>`,
        });
      }

      return column;
    });
  });

  const sortReducer = composedReducer(
    { scope },
    columnReducer,
    () => userSort
  ).reduce((columns, userSortData) => {
    let sort,
      defaultSort = immutable({
        by: 'deadline',
      }),
      sortColumn;

    sort = defaultSort.merge([userSortData]);
    sortColumn = find(columns, { id: sort.by });

    sort = sort.merge({
      path: get(sortColumn, 'resolve'),
    });

    if (!sort.by) {
      sort = {
        by: get(first(columns), 'id'),
        order: 'asc',
      };
    }

    return sort;
  });

  const itemReducer = composedReducer(
    { scope },
    taskReducer,
    sortReducer
  ).reduce((tasks, sort) => {
    let items = tasks;
    // make sure we are able to differentiate between empty and non-existent result sets
    if (!items) {
      return null;
    }

    items = items.map((item) => {
      let caseId = get(item, 'relationships.case.meta.display_number');

      return item.merge({
        id: shortid(),
        href: state.href('case', { caseId }),
        values: columnConfig.map((column) => get(item, column.resolve)),
      });
    });

    return orderBy(
      items,
      [
        (item) => {
          const sortAttribute = get(item, sort.path, '');
          return typeof sortAttribute === 'string'
            ? sortAttribute.toLowerCase()
            : sortAttribute;
        },
        (item) => get(item, 'relationships.case.meta.display_number'),
      ],
      [sort.order, sort.order]
    );
  });

  ctrl.getTasks = taskReducer.data;

  ctrl.getRows = itemReducer.data;

  ctrl.getColumns = composedReducer(
    { scope },
    columnReducer,
    sortReducer
  ).reduce((columns, sort) => {
    let visibleColumns = columns;

    let styledColumns = visibleColumns.map((column) => {
      let sortedOn = column.id === get(sort, 'by'),
        sortReversed = sortedOn && get(sort, 'order', 'asc') === 'desc',
        iconType;

      if (sortedOn) {
        iconType = `chevron-${sortReversed ? 'up' : 'down'}`;
      }

      return column.merge({
        style: {
          'column-sort-active': sortedOn,
          'column-sort-reversed': sortReversed,
          'column-sort-supported': column.sort,
        },
        iconType,
      });
    });

    return styledColumns;
  }).data;

  ctrl.handleColumnClick = (columnId) => {
    let sort = sortReducer.data(),
      column = find(columnReducer.data(), { id: columnId });

    if (!get(column, 'sort', null)) {
      return;
    }

    if (sort.by === columnId) {
      sort = sort.merge({ order: sort.order === 'desc' ? 'asc' : 'desc' });
    } else {
      sort = sort.merge({ by: columnId, order: 'asc' });
    }

    userSort = sort;
  };
}

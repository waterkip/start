#!/bin/bash

set -e

SHARE=/usr/local/share/ca-certificates

if compgen -G "/etc/custom-certs/*" > /dev/null; then
  sudo cp -rp /etc/custom-certs/* $SHARE
fi

if compgen -G "$SHARE" > /dev/null; then
  sudo update-ca-certificates
fi

if [ -n "$ZS_PRE_ACTION" ]; then
    $ZS_PRE_ACTION
fi

exec "$@"

package Zaaksysteem::Filestore::Engine::S3::AssumeRoleWebIdentity;

use Moose;
use MooseX::StrictConstructor;
use Paws; # The Paws plugin below does not load Paws itself...
use Paws::Credential::AssumeRoleWebIdentity;

extends 'Net::Amazon::S3::Authorization';

=head1 NAME

Zaaksysteem::Filestore::Engine::S3::AssumeRoleWebIdentity - Implementation of
Net::Amazon::S3::Authorization that uses keys obtained using the AWS
"AssumeRoleWebIdentity" API.

=head1 DESCRIPTION

To enable "per Kubernetes pod" S3 access, we need to request access tokens from
Amazon STS using a JWT token that's stored on disk.

This is handled by L<Paws::Credential::AssumeRoleWebIdentity>.

This is an adapter class to enable using those access tokens with
L<Net::Amazon::S3> (which can do presigned URLs, which L<Paws> cannot) .

=cut

has sts_region => (
    is => 'ro',
    isa => 'Str|Undef',
    default => sub { undef },
);

has _credentials => (
    is => 'ro',
    init_arg => undef,
    lazy => 1,
    builder => '_build_credentials',
    handles => {
        aws_access_key_id => 'access_key',
        aws_secret_access_key => 'secret_key',
        aws_session_token => 'session_token',
    }
);

sub _build_credentials {
    my $self = shift;
    my $creds = Paws::Credential::AssumeRoleWebIdentity->new(
        sts => Paws->service(
            'STS',
            region => $self->sts_region,
            credentials => Paws::Credential::None->new
        )
    );

    return $creds;
}

around authorization_headers => sub {
    my ($orig, $self) = @_;

    return +(
        $self->$orig,
        'x-amz-security-token' => $self->aws_session_token,
    );
};

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.

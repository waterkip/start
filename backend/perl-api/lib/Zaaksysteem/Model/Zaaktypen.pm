package Zaaksysteem::Model::Zaaktypen;

use Moose;
use namespace::autoclean;

extends 'Catalyst::Model::Factory::PerRequest';

__PACKAGE__->config(
    class       => 'Zaaksysteem::Zaaktypen',
    constructor => 'new',
);

=head1 NAME

Zaaksysteem::Model::Zaaktypen - Zaaktype model factory

=head1 SYNOPSIS

    my $model = $c->model('Zaaktypen')

For more information see L<Zaaksysteem::Zaaktypen>.

=head1 METHODS

=head2 prepare_arguments

Implements the argument factory interface for
L<Catalyst::Model::Factory::PerRequest>.

=cut

sub prepare_arguments {
    my ($self, $c) = @_;

    return {
        prod => $c->config->{otap} eq 'prod' ? 1 : undef,
        dbic => $c->model('DB'),
        object_model => $c->model('Object'),
    };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

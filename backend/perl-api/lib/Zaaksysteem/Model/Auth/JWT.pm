package Zaaksysteem::Model::Auth::JWT;
use Moose;
use namespace::autoclean;

extends 'Catalyst::Model::Factory::PerRequest';

__PACKAGE__->config(
    class => 'Zaaksysteem::Auth::JWT',
    constructor => 'new'
);

=head1 NAME

Zaaksysteem::Model::Auth::JWT - Catalyst model factory for
L<Zaaksysteem::Auth::JWT>

=head1 SYNOPSIS

    my $model = $c->model('Auth::JWT');

=head1 METHODS

=head2 prepare_arguments

Prepares the arguments for L<<Zaaksysteem::Auth::JWT->new>>.

=cut

sub prepare_arguments {
    my ($self, $c) = @_;

    return {
        redis     => $c->model('Redis'),
        cookiejar => $self->_build_cookiejar($c),
    };
}

sub _build_cookiejar {
    my ($self, $c) = @_;

    my $cfg = $c->_session_plugin_config;
    my %args;
    foreach (qw(server debug reconnect db)) {
        next unless exists $cfg->{ 'redis_' . $_ };
        $args{$_} = $cfg->{ 'redis_' . $_ };
    }
    return Zaaksysteem::Store::Redis->new(%args);
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.

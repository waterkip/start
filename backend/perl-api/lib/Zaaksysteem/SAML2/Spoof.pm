package Zaaksysteem::SAML2::Spoof;
use Moose;

has interface => (
    is => 'rw',
);

has id => (
    is => 'rw',
    default => sub { return 42 },
);

has issuer => (
    is => 'rw',
    default => sub { return 42 },
);

sub new_from_interface {
    my ($self, %opts) = @_;

    return $self->new(%opts);
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 NAME

Zaaksysteem::SAML2::Spoof - A SAML2 Spoof mode module

=head1 DESCRIPTION

On development environments you don't always have real DigiD and/or eHerkenning interfaces.
Nor do you want to setup a complete production like environment to test PIP related things.
This module makes it easier to spoof L<Zaaksysteem::SAML2::SP> and L<Zaakysteem::SAML2::IdP>
modes so you can test.

=head1 METHODS

=head2 new_from_interface

Creates a Zaaksysteem::SAML2::Spoof object from the interface.

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

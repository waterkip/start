package Zaaksysteem::Backend::Email;
use Zaaksysteem::Moose;

use Authen::SASL;
use Authen::SASL::Perl::XOAUTH2;
use BTTW::Tools;
use Email::Address;
use Email::Valid;
use Encode qw(encode_utf8);
use File::Temp;
use HTML::Entities qw(encode_entities);
use IO::All;
use List::Util qw(first all);
use Mail::DKIM::Signer;
use Mail::Track;
use POSIX                   qw(strftime);
use BTTW::Tools::RandomData qw(generate_uuid_v4);
use Zaaksysteem::Backend::Email::Transport;
use Zaaksysteem::Email::ZTT;
use Zaaksysteem::Object::Model;

with qw(
    Zaaksysteem::Moose::Role::Schema
);

=head2 additional_ztt_context

Used to provide an extra context for magic string retrieval. ZTT will
also source magic strings from this context if present.

=cut

has additional_ztt_context => (is => 'rw',);

=head2 sender

Determine the sender, varying from

=head3 Arguments

=over

=item sender [optional]

The name of the email sender, e.g. 'Internal Affairs Department'

=item from [optional]

The email-address of the sender, e.g. internal.affairs@shield.org

If empty, the default configured from address will be used.

=back

=cut

define_profile sender => (optional => [qw/sender_address sender/]);

sub sender {
    my $self   = shift;
    my $params = assert_profile(shift)->valid;

    my $from = $params->{sender_address} // $self->schema->resultset('Config')
        ->get_customer_config->{zaak_email};

    my $email = Email::Address->new($params->{sender}, $from);

    return $email if $self->schema->is_allowed_mailhost($email->host);

    throw("zaaksysteem/email/not_allowed",
        $email->host . " is not allowed to send e-mail");
}

sub _get_dkim {
    my $self   = shift;
    my $config = shift;
    my $dkim_key_file;
    my $dkim_selector;
    if ($config->{dkim_key}[0]{id}) {
        $self->log->trace("Creating DKIM signing object from filestore");

        my $keyfile = $self->schema->resultset('Filestore')
            ->find($config->{dkim_key}[0]{id});
        $dkim_key_file = $keyfile->get_path;
        $dkim_selector = $config->{dkim_selector};
    }
    else {
        $self->log->trace("Creating DKIM signing object from environment");

        $dkim_key_file = $ENV{DKIM_KEY_FILE};
        $dkim_selector = $ENV{DKIM_CURRENT_SELECTOR};
    }

    unless ($dkim_key_file && $dkim_selector) {
        my $error = "DKIM enabled, but no DKIM key or selector configured.";
        $self->log->error($error);

        throw("case/mail/dkim_settings", $error,);
    }

    return try {
        Mail::DKIM::Signer->new(
            Algorithm => $config->{dkim_algorithm} // 'rsa-sha256',
            Method    => $config->{dkim_method}    // 'relaxed',
            Domain    => $config->{dkim_domain},
            Selector  => $dkim_selector,
            KeyFile   => $dkim_key_file,
        );
    }
    catch {
        $self->log->error("Error while sending email: $_");

        throw("case/email/dkim_signer",
            'Interne fout bij het versturen van email (DKIM signer)',
        );
    };
}

sub _interface_to_mail_track {
    my $self      = shift;
    my $interface = shift;

    my $config    = $interface->get_interface_config;
    my $transport = $self->_create_smtp_transport($interface, $config)
        if $config->{use_smarthost};

    my $schema = $self->schema;
    my $dkim   = $config->{use_dkim} ? $self->_get_dkim($config) : undef;

    my $mt = Mail::Track->new(
        subject_prefix_name => $config->{subject},

        $dkim      ? (dkim      => $dkim)      : (),
        $transport ? (transport => $transport) : (),
    );


    return $mt;
}

define_profile send_email => (

    required => {
        body      => 'Str',
        subject   => 'Str',
        recipient => 'Str',
    },
    optional => {
        html_image       => 'Any',
        html_template    => 'Any',
        cc               => 'Str',
        bcc              => 'Str',
        request_id       => 'Str',
        attachments      => 'Any',
        identifier       => 'Str',
        identifier_regex => 'RegexpRef',
        sender_address   => 'Str',
        sender           => 'Str',
    },
);

sub send_email {
    my $self   = shift;
    my $params = assert_profile({@_})->valid;

    my $body          = $params->{body};
    my $subject       = $params->{subject};
    my $html_template = $params->{html_template};
    my $html_images   = $params->{html_image};

    my $interface = $params->{interface}
        // $self->schema->resultset('Interface')
        ->search_active({ module => 'emailconfiguration' })->first;

    my $mt
        = $interface
        ? $self->_interface_to_mail_track($interface)
        : Mail::Track->new();

    foreach (qw(identifier identifier_regex)) {
      $mt->$_($params->{$_}) if $params->{$_};
    }

    my $config   = $interface ? $interface->get_interface_config : {};
    my $max_size = $config->{max_size} || 10;

    $params->{sender_address} = $config->{api_user}
        if !defined $params->{sender_address} && $config->{api_user};

    $params->{sender} = $config->{sender_name}
        if !defined $params->{sender} && $config->{sender_name};

    my $from = $self->sender($params);

    $self->log->info(
        sprintf(
            "Sending email. Sender: '%s'; Recipient: '%s'",
            $from->format, $params->{recipient},
        )
    );

    foreach (qw(recipient cc bcc)) {
        $params->{$_} = $self->sanitize_mail($params->{$_});
    }

    my $msg = $mt->prepare(
        {
            from    => $from->format,
            to      => $params->{recipient},
            subject => $subject,
            cc      => $params->{cc},
            bcc     => $params->{bcc},

            extra_headers =>
                { 'X-ZS-Request' => $params->{request_id} // 'unknown', },
        }
    );

    $msg->add_body_part({ content => encode_utf8($body) });

    if ($html_template) {

        # Escape
        my $html_escaped_body = encode_entities($body);

        # Single newlines disappear (HTML whitespace folding), double newlines
        # start a new "paragraph".
        $html_escaped_body =~ s/\n/<br>/g;

        (my $html_body = $html_template)
            =~ s/\{\{message\}\}/$html_escaped_body/;

        my $image_url = "";
        my @related_parts;
        if ($html_images && @$html_images > 0) {
            my %image_data = %{ $html_images->[0] };
            $image_url = "cid:$image_data{uuid}";

            my $filestore
                = $self->schema->resultset('Filestore')->find($image_data{id});
            my $fh = $filestore->get_path;
            $self->_add_fh($fh);

            push @related_parts,
                {
                "id"   => $image_data{uuid},
                "type" => $image_data{mimetype},
                "data" => $fh,
                "name" => $image_data{original_name},
                };
        }

        $html_body =~ s/\{\{image_url\}\}/$image_url/;
        $msg->add_body_part(
            {
                content_type  => 'text/html',
                content       => $html_body,
                related_parts => \@related_parts,
            }
        );
    }

    my %has_files;
    foreach (@{ $params->{attachments} // [] }) {
        my $fs  = $_->filestore;
        my $md5 = $fs->md5;
        next if $has_files{$md5};
        my $fh = $fs->get_path;
        $self->_add_fh($fh);
        $has_files{$md5} = 1;

        $msg->add_attachment(
            filename     => $_->filename,
            content_type => $fs->mimetype,
            path         => "$fh",
        );
    }

    my $size = $msg->size;

    die "No subject in email found" if !defined $subject || !length($subject);
    die "No body in email found"    if !defined $body    || !length($body);

    if ($size >= ($max_size * (1000 * 1000))) {
        die
            'De maximale grootte van de mail is overschreden (%.2fMB / Max: %dMB)',
            ($size / (1000 * 1000)), $max_size;
    }

    my $msg_sent = try {
        my $sent = $msg->send;
        $self->log->error("Empty message sent?") unless $sent;
        return $sent;
    }
    catch {
        die "Unable to send message with : $_";
    };

    return $msg_sent;
}

has _fh => (
    is      => 'rw',
    isa     => 'ArrayRef',
    traits  => ['Array'],
    handles => { _add_fh => 'push' }
);

sub _create_smtp_transport {
    my $self = shift;
    my ($interface, $config) = @_;

    my %smtp_options = (
        ssl         => 'starttls',
        ssl_options => {

            # Use system-default SSL
            SSL_ca_path => '/etc/ssl/certs'
        },
    );

    if (($config->{kind} // '') eq 'microsoft') {

        my $access_token = $self->schema->execute_reliably(
            sub {
                my $interface_module = $interface->module_object;
                $interface
                    = $self->schema->resultset('Interface')
                    ->search({ id => $interface->id }, { for => 'update' })
                    ->first;
                return $interface_module->refresh_access_token($interface);
            }
        );

        $smtp_options{host} = 'smtp.office365.com';
        $smtp_options{port} = 587;

        $smtp_options{sasl} = Authen::SASL->new(
            mechanism => 'XOAUTH2',
            callback  => {
                user         => $config->{ms_username},
                access_token => $access_token,
            },
        );

        $self->log->debug(
            sprintf(
                "Creating SMTP transport for host:port '%s:%d', oauth2 address: '%s'",
                $smtp_options{host}, $smtp_options{port},
                $config->{ms_username},
            )
        );
    }
    else {
        $smtp_options{host} = $config->{smarthost_hostname};
        $smtp_options{port} = $config->{smarthost_port};

        if ($config->{smarthost_username} && $config->{smarthost_password}) {
            $smtp_options{sasl_username} = $config->{smarthost_username};
            $smtp_options{sasl_password} = $config->{smarthost_password};
        }

        $self->log->debug(
            sprintf(
                "Creating SMTP transport for host:port '%s:%d', username '%s' (%s)",
                $smtp_options{host},
                $smtp_options{port},
                (
                    $smtp_options{sasl_username} ? $smtp_options{sasl_username}
                    : 'without username'
                ),
                (
                    $smtp_options{sasl_password} ? "with password"
                    : "without password"
                )
            )
        );
    }

    return Zaaksysteem::Backend::Email::Transport->new(%smtp_options);
}

sub sanitize_mail {
    my $self   = shift;
    my $value  = shift // '';
    my @emails = split /\s*[;,]\s*/, $value;
    return join(',', grep { Email::Valid->address($_) } @emails);
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2023, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.

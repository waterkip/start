package Zaaksysteem::Backend::Sysin::Modules::Shared::OAuth2Client;
use warnings;
use strict;

use Zaaksysteem::ZAPI::Form::Field;

use Exporter 'import';
our @EXPORT_OK = qw(
    build_oauth2_client_fields
    build_oauth2_form_object_values
);

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::Shared::OAuth2Client

=head1 SYNOPSIS

Configuration fields that are used by multiple integrations that offer
functionality requiring Zaaksysteem to act as an OAuth2 client.

=head1 DEFINITIONS

=head2 build_oauth2_client_fields($when_condition)

Returns an array of integration config fields used to add everything needed
for an OAuth2 client to integrations.

Include the return value in C<INTERFACE_CONFIG_FIELDS>.

Contains the C<interface_kind> configuration setting, that can be used
to enable/disable fields based on OAuth2 being active or not.

If "$when_condition" is specified, it will be added to each field, so they can
be made to show up conditionally.

=cut

sub _build_when_clause {
    my $when = join(" && ", grep { $_ } @_);

    if ($when) {
        return (
            "when" => $when
        );
    }

    return;
};

sub build_oauth2_client_fields {
    my ($when_condition) = @_;

    return (
        Zaaksysteem::ZAPI::Form::Field->new(
            name        => 'interface_kind',
            type        => 'select',
            label       => 'Soort configuratie',
            required    => 1,
            description => '<p>Kies &quot;eigen instellingen&quot; om zelf een server, gebruikersnaam en wachtwoord in te stellen, of &quot;Microsoft&quot; om OAuth2 icm. de online services van Microsoft te gebruiken.</p>',
            default     => 'custom',
            _build_when_clause($when_condition),
            data => {
                options => [
                    { value => 'custom', label => 'Eigen instellingen' },
                    { value => 'microsoft',  label => 'Microsoft 365' },
                ],
            },
        ),

        Zaaksysteem::ZAPI::Form::Field->new(
            name        => 'interface_ms_username',
            type        => 'text',
            label       => 'Gebruikersnaam',
            required    => 1,
            _build_when_clause(
                $when_condition,
                'interface_kind === "microsoft"'
            ),
            description => 'Gebruikersnaam voor de mailbox, of het emailadres als het een gedeelde mailbox is.',
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name        => 'interface_ms_tenant_id',
            type        => 'text',
            label       => 'Tenant-ID',
            required    => 1,
            _build_when_clause(
                $when_condition,
                'interface_kind === "microsoft"'
            ),
            description => 'Tenant-ID van de Microsoft-omgeving waarop de mailbox zich bevindt.',
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name        => 'interface_ms_client_id',
            type        => 'text',
            label       => 'Client-ID',
            required    => 1,
            _build_when_clause(
                $when_condition,
                'interface_kind === "microsoft"'
            ),
            description => 'Client-ID voor deze applicatie uit Azure AD.',
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name        => 'interface_ms_client_secret',
            type        => 'password',
            label       => 'Client Secret',
            required    => 1,
            _build_when_clause(
                $when_condition,
                'interface_kind === "microsoft"'
            ),
            description => 'Client secret van deze applicatie in Azure AD.',
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name        => 'interface_ms_client_secret_expiry_date',
            type        => 'date',
            label       => 'Vervaldatum client secret',
            required    => 0,
            _build_when_clause(
                $when_condition,
                'interface_kind === "microsoft"'
            ),
            description => 'Vervaldatum van het client secret. Kan in de toekomst gebruikt worden voor signalering dat het secret vernieuwd moet worden.',
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name => 'interface_ms_login_link',
            type => 'display',
            label => 'Inloggen',
            description => '',
            required => 0,
            _build_when_clause(
                $when_condition,
                'interface_kind === "microsoft"'
            ),
            data => {
                template => '<a href="<[field.value]>" target="_blank">Inloggen via Microsoft</a>'
            }
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name => 'interface_ms_logged_in',
            type => 'display',
            label => 'Login geslaagd?',
            required => 0,
            _build_when_clause(
                $when_condition,
                'interface_kind === "microsoft"'
            ),
            data => {
                template => '<[field.value]>'
            }
        )
    );
}

=head2 build_oauth2_form_object_values

Build some "prefilled" values for the integration configuration form
for OAuth2 clients:

=over

=item * Login link to the OAuth2 identity service 

=item * Yes/no indicator that shows whether a refresh token is present

=back

Call this from C<_load_values_into_form_object>, and put the return value
into the hashref given to C<load_values({...here...})>.

=cut

sub build_oauth2_form_object_values {
    my $opts = shift;
    my $interface = shift;

    return (
        interface_ms_login_link => 
            $opts->{ base_url } .
            sprintf('api/v2/admin/integration/start_oauth2_flow?integration_uuid=%s', $interface->uuid),
        interface_ms_logged_in => 
            (
                $interface->internal_config->{refresh_token}
                && not $interface->internal_config->{access_token_failed}
            ) ? 'Ja' : 'Nee',
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2022, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

package Zaaksysteem::Backend::Sysin::Modules::SuperSaaS;
use Moose;
use namespace::autoclean;

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw/
    Zaaksysteem::Backend::Sysin::Modules::Roles::ProcessorParams
    Zaaksysteem::Backend::Sysin::Modules::Roles::Tests
    Zaaksysteem::Backend::Sysin::Modules::Roles::CertificateInfo
/;

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::SuperSaaS - Interface to talk to SuperSaaS' calendaring platform

=head1 DESCRIPTION

L<SuperSaaS|https://www.supersaas.com/> is a SaaS provider of, among other
things, a calendaring platform. This module allows Zaaksysteem to interact with
it using its L<API|http://www.supersaas.com/info/doc/integration/appointment_api>.

=cut

use HTTP::Request::Common ();
use LWP::UserAgent;
use URI;
use URI::Escape qw(uri_escape_utf8);
use XML::Simple;
use BTTW::Tools;
use Zaaksysteem::ZAPI::Form::Field;

use constant INTERFACE_ID => 'supersaas';

use constant INTERFACE_CONFIG_FIELDS => [
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_endpoint',
        type        => 'text',
        label       => 'SuperSaaS API endpoint',
        required    => 1,
        default     => 'https://www.supersaas.com/api',
        description => 'SuperSaaS API endpoint, base URL. Moet waarschijnlijk altijd https://www.supersaas.com/api zijn'
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_ca_cert',
        type        => 'file',
        label       => 'CA-certificaat',
        required    => 0,
        description => "CA-certificaat van de CA die het certificaat van de SuperSaaS-API uitgegeven heeft",
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_password',
        type        => 'password',
        label       => 'Superuser-wachtwoord',
        required    => 1,
        description => "Wachtwoord van de superuser van het SuperSaaS-account",
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_future_days',
        type        => 'number',
        label       => "Aantal dagen in de toekomst",
        required    => 1,
        description => "Maximaal aantal dagen in de toekomst dat een afspraak te maken is",
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_schedule_ids',
        type        => 'text',
        label       => "Te gebruiken agenda-IDs",
        required    => 1,
        description => "Te gebruiken agenda-IDs, door komma's gescheiden. Een agenda-ID is te vinden door op de SuperSaaS-omgeving op de 'Configureren'-knop bij een agenda te klikken. Het nummer aan het einde van de URL is het agenda-ID",
    ),
];

use constant MODULE_SETTINGS => {
    name                          => INTERFACE_ID,
    label                         => 'SuperSaaS Agenda',
    interface_config              => INTERFACE_CONFIG_FIELDS,
    sensitive_config_fields       => [qw(password)],
    direction                     => 'incoming',
    manual_type                   => ['text'],
    is_multiple                   => 0,
    is_manual                     => 1,
    retry_on_error                => 0,
    allow_multiple_configurations => 0,
    is_casetype_interface         => 0,
    has_attributes                => 1,
    attribute_list => [
    ],
    trigger_definition => {
        getProduct => {
            method => 'get_product',
            update => 1
        },
        getAvailableAppointmentDays => {
            method => 'get_available_dates',
            update => 1
        },
        getAvailableAppointmentTimes => {
            method => 'get_available_times',
            update => 1
        },
        bookAppointment => {
            method => 'book_appointment',
            update => 1,
        },
        updateAppointment => {
            method => 'update_appointment',
            update => 1,
        },
        deleteAppointment => {
            method => 'delete_appointment',
            update => 1,
        },
    },
    test_interface  => 1,
    test_definition => {
        description => qq{
            Om te controleren of de applicatie goed geconfigureerd is, kunt u
            hieronder een aantal tests uitvoeren. Hiermee controleert u de verbinding
            van uw profiel.
        },
        tests => [
            {
                id          => 1,
                label       => 'Test CA-certificaat',
                name        => 'ca_file_test',
                method      => 'supersaas_test_ca_certificate',
                description => 'Test of de CA-certificaatketen correct en compleet is.'
            },
            {
                id          => 2,
                label       => 'Test verbinding',
                name        => 'connection_test',
                method      => 'supersaas_test_connection',
                description => 'Test verbinding naar SuperSaaS URL.'
            }
        ],
    },
    interface_update_callback => sub {
        return shift->_update_interface_config(@_);
    },
};

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig( %{ MODULE_SETTINGS() } );
};

=head1 METHODS

=head2 dynamic_attribute_list

Generates the list of mappable attributes. For this interface, the list is
created from the comma-separated list in the C<schedule_ids> configuration
item, as SuperSaaS doesn't offer a way to retrieve a list of identifiers using
their API. Only I<resource calendars> are currently supported.

Zaaksysteem users can use this list to map calendar IDs to attributes that can
be used in cases.

=cut

sub dynamic_attribute_list {
    my ($self, $interface) = @_;

    my $schedule_ids = $interface->jpath('$.schedule_ids');
    return if not $schedule_ids;

    my @attributes;
    for my $id (split /\s*,\s*/, $schedule_ids) {
        push @attributes, {
            attribute_type => 'magic_string',
            all_casetypes  => 1,
            checked        => 1,
            schedule_id    => $id,
            external_name  => "Agenda '$id'",
        };
    }

    return \@attributes;
}

=head2 supersaas_test_ca_certificate

Run a test to check whether the uploaded CA certificate file is correct.

=cut

sub supersaas_test_ca_certificate {
    my $self = shift;
    my $interface = shift;

    my $file_id = $interface->jpath('$.ca_cert[0].id');
    return if not $file_id;

    my $schema = $interface->result_source->schema;
    my $ca_file = $schema->resultset('Filestore')->find($file_id);
    $self->test_ca_chain($ca_file->get_path);

    return;
}

=head2 supersaas_test_connection

Test the connection to SuperSaaS, by setting up a TLS connection to the host
from the configurd endpoint URL using the configured CA chain.

=cut

sub supersaas_test_connection {
    my $self = shift;
    my ($interface) = @_;

    my $schema = $interface->result_source->schema;
    my $ca_file = $schema->resultset('Filestore')->find($interface->jpath('$.ca_cert[0].id'));

    $self->test_host_port_ssl(
        $interface->jpath('$.endpoint'),
        ($ca_file ? $ca_file->get_path : ()),
    );
}

=head2 get_product

Retrieve the product details (schedule id) for a kenmerk (specified by its
magic string).

=cut

sub get_product {
    my ($self, $params, $interface) = @_;

    my $magic_string = $params->{magic_string};
    my $mapping = $interface->get_interface_config->{attribute_mapping};

    foreach my $item (@$mapping) {
        my $object_id = $item->{internal_name}->{searchable_object_id};

        if ($object_id && $object_id eq $magic_string) {
            return $item;
        }
    }

    return;
}

=head2 get_available_dates

Return a list of all dates on which appointments could potentially be made.

This call exists because QMatic (the first calendar provider Zaaksysteem
integrated with) has a separate SOAP call to retrieve such a list. For
SuperSaaS, we fake this by making a list of days. Appointments can't be made
more than that (configurable) number of days into the future.

=cut

sub get_available_dates {
    my ($self, $params, $interface) = @_;

    my $available_days  = $interface->jpath('$.future_days') || 30;

    my $now = DateTime->today();

    my @days;
    for (0 .. $available_days) {
        push @days, $now->strftime('%FT%TZ');

        $now->add(days => 1);
    }

    return \@days;
}

=head2 get_available_times

Given a specific date, retrieves a list of available time slots.

The SuperSaaS API only allows us to retrieve a set number of free time slots
later than a given time, so we just request 200 slots (which is more than the
144 5-minute slots that are potentially available in a 12-hour period).

We then filter out the ones that aren't on the specified date.

=cut

sub get_available_times {
    my ($self, $params, $interface) = @_;

    my $from = DateTime::Format::DateParse
        ->parse_datetime($params->{appDate}, 'Europe/Amsterdam');

    my $slots_xml = $self->supersaas_http(
        'GET',
        $interface,
        sprintf('/free/%d.xml', $params->{productLinkID}),
        {
            # Even with 5-minute slots x 12 hour days, this should be more than enough
            maxresults => 200,
            # Only ask for slots after
            from       => $from->strftime('%F %T'),
        }
    );

    my $slots = XMLin(
        $slots_xml->decoded_content,
        ForceArray => [qw(slot)],
        KeyAttr    => [],
    );

    my @available = map {
        my $start  = DateTime::Format::DateParse
            ->parse_datetime($_->{start}, 'Europe/Amsterdam')
            ->set_time_zone('UTC');
        my $finish = DateTime::Format::DateParse
            ->parse_datetime($_->{finish}, 'Europe/Amsterdam')
            ->set_time_zone('UTC');

        {
            start  => $start->iso8601 . 'Z',
            finish => $finish->iso8601 . 'Z',
            resource_id => $_->{title}{id},
            resource_title => $_->{title}{content},
        };
    } grep {
        my $start = DateTime::Format::DateParse->parse_datetime($_->{start}, 'Europe/Amsterdam');

        $from->date eq $start->date
        && $_->{count};
    } @{ $slots->{slot} };

    return \@available;
}

=head2 book_appointment

Create an appointment in the SuperSaaS application.

=cut

sub book_appointment {
    my ($self, $params, $interface) = @_;

    if ($params->{previous_appointment_id}) {
        my $deleted_result = $self->delete_appointment(
            {
                productLinkID => $params->{productLinkID},
                appointmentId => $params->{previous_appointment_id},
            },
            $interface,
        );
    }
    my $aanvrager = $self->_retrieve_aanvrager($interface, $params->{aanvrager});

    my $start  = DateTime::Format::DateParse
        ->parse_datetime($params->{appTimeStart})
        ->set_time_zone('Europe/Amsterdam');
    my $finish = DateTime::Format::DateParse
        ->parse_datetime($params->{appTimeFinish})
        ->set_time_zone('Europe/Amsterdam');

    my $booking = $self->supersaas_http(
        'POST',
        $interface,
        '/bookings',
        {
            'schedule_id'        => $params->{productLinkID},
            'booking[start]'     => $start->iso8601,
            'booking[finish]'    => $finish->iso8601,
            'booking[full_name]' => $aanvrager->naam,
            'booking[resource_id]' => $params->{resourceID},
        }
    );

    my $location = $booking->header('Location');

    if (!$location) {
        die;
    }

    my ($booking_id) = $location =~ m#/([^/]+)\.xml$#;

    return [
        {
            appointmentId => $booking_id
        }
    ];
}

=head2 update_appointment

Update an existing appointment's description

=cut

sub update_appointment {
    my ($self, $params, $interface) = @_;

    my $result = $self->supersaas_http(
        'POST',
        $interface,
        sprintf('/bookings/%s.xml', $params->{appointmentId}),
        {
            'schedule_id'          => $params->{productLinkID},
            'booking[description]' => $params->{description},
        },
    );

    return [$result ? $result->code : 500];
}

=head2 delete_appointment

Remove a previously planned appointment from the SuperSaaS schedule.

=cut

sub delete_appointment {
    my ($self, $params, $interface) = @_;

    my $result = $self->supersaas_http(
        'DELETE',
        $interface,
        sprintf('/bookings/%s.xml', $params->{appointmentId}),
        {
            schedule_id => $params->{productLinkID},
        },
    );

    return [$result ? $result->code : 500];
}

sub _retrieve_aanvrager {
    my ($self, $interface, $betrokkene_string) = @_;

    my $betrokkene_model = $interface->result_source->schema->resultset('Zaak')->betrokkene_model;

    return $betrokkene_model->get_by_string($betrokkene_string);
}

=head2 supersaas_http

Perform a HTTP "call" to the SuperSaaS API.

Accepts 3 positional parameters:

=over

=item * Method to use ("POST", "GET", "DELETE", etc, used to construct a HTTP::Request)

=item * Interface object, used to retrieve configuration (CA certificate, endpoint base URL)

=item * URL to call. This will be appended to the base URL, so include a "/"!

=item * A hash reference containing parameters to send with the request.

=back

=cut

sub supersaas_http {
    my $self = shift;
    my ($method, $interface, $url_part, $params) = @_;

    $interface->process({
        external_transaction_id => 'unknown',
        input_data              => 'unknown',
        processor_params        => {
            processor => '_process_http',
            method   => $method,
            url_part => $url_part,
            params   => $params,
        },
    });

    return $self->response;
}

sub _process_http {
    my $self = shift;
    my $record = shift;

    my $transaction      = $self->process_stash->{transaction};
    my $processor_params = $transaction->get_processor_params;
    my $interface        = $transaction->interface;

    my $method   = $processor_params->{method};
    my $url_part = $processor_params->{url_part};
    my $params   = $processor_params->{params};

    my $schema  = $interface->result_source->schema;
    my $ca_file = $schema->resultset('Filestore')->find($interface->jpath('$.ca_cert[0].id'));

    $self->ua->ssl_opts(
        verify_hostname => 1,
        SSL_ca_file     => $ca_file ? $ca_file->get_path : undef,
        SSL_ca_path     => '/etc/ssl/certs',
    );

    my $url = URI->new($interface->jpath('$.endpoint') . $url_part);
    $params->{password} = $interface->jpath('$.password');

    my $post_content;
    if ($method eq 'POST') {
        $post_content = _url_escape_hash($params);
    }
    else {
        $url->query_form($params);
    }

    my $request_method = HTTP::Request::Common->can($method);

    my $request = $request_method->(
        $url,
        ($method eq 'POST')
            ? ( Content => $post_content )
            : ()
    );
    my $res = $self->ua->request($request);

    $transaction->input_data($record->input($request->as_string));
    $record->output($res->as_string);

    if ($res->is_success) {
        $self->response($res);
    }
    else {
        throw(
            "interface/supersaas_http",
            "SuperSaaS - HTTP error: " . $res->code,
            { response => $res }
        );
    }
}

# Somehow, the internal "make a query string" bits in HTTP::Request::Common and
# URI don't cope well with the whole characters vs bytes thing.
# So we do it ourselves.
sub _url_escape_hash {
    my $params = shift;

    my @rv;
    for my $k (keys %$params) {
        my $v = uri_escape_utf8($params->{$k});

        push @rv, "$k=$v";
    }

    return join("&", @rv);
}

=head1 ATTRIBUTES

=head2 response

The HTTP response received from SuperSaaS. Used internally when patching a call
through the transaction mechanism.

=cut

has response => (
    is => 'rw',
    isa => 'HTTP::Response',
);

=head2 ua

L<LWP::UserAgent> instance used to connect to the SuperSaaS API. By default, a new instance is created.

=cut

has ua => (
    is       => 'rw',
    isa      => 'LWP::UserAgent',
    default  => sub {
        my $self = shift;
        my $ua = LWP::UserAgent->new();
        $ua->add_handler("request_send",  sub { $self->log->trace(shift->dump); return });
        $ua->add_handler("response_done", sub { $self->log->trace(shift->dump); return });
        return $ua;
    },
    lazy     => 1,
    required => 0,
);

sub _update_interface_config {
    my ($module, $interface, $model) = @_;

    my $config = $interface->get_interface_config;

    $module->get_certificate_info(
        $interface,
        $config,
        qw(ca_cert)
    );

    $interface->update_interface_config($config);
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

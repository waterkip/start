package Zaaksysteem::Backend::ObjectSubscription::Component;

use Moose;

extends 'Zaaksysteem::Backend::Component';

has '_json_data'    => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        my $self                    = shift;

        unless (DateTime->can('TO_JSON')) {
            no strict 'refs';
            *DateTime::TO_JSON          = sub { shift->iso8601 };
            use strict;
        }

        my $pub_info = {
            id          => $self->id,
            local_table => $self->local_table,
            local_id    => $self->local_id,
            date_create    => $self->date_created,
            date_deleted   => $self->date_deleted,
            external_id    => $self->external_id,
            object_preview => $self->object_preview,
            interface_id   => ($self->interface_id ? $self->interface_id->TO_JSON({ignore_errors => 1 }) : undef),
        };

        return $pub_info;
    },
);

=head2 object_subscription_delete()

Will mark this record as deleted

=cut

sub object_subscription_delete {
    my $self        = shift;

    # XXX: Broken domain, this method should receive a queue model.
    # Sadly, this method is called via the complex beast of sysin/stuf, so
    # no easy injection possible.
    my $qrs = $self->result_source->schema->resultset('Queue');

    my $item = $qrs->create_item('disable_object_subscription', {
        label => 'Disable object subscription',
        metadata => {
            target => 'backend',
        },
        data => {
            subscription_id     => $self->id,
            interface_id        => $self->get_column('interface_id'),
            config_interface_id => $self->get_column('config_interface_id'),
        }
    });

    $qrs->queue_item($item);

    return 1;
}

=head2 get_local_entry

Returns the local entry by looking up local_table and local_id

=cut

sub get_local_entry {
    my $self        = shift;

    return $self->result_source->schema->resultset($self->local_table)->find($self->local_id);
}

1;


__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


package Zaaksysteem::Backend::Object::Data::Roles::Case;

use Moose::Role;

use List::MoreUtils qw[uniq];

use BTTW::Tools;

use List::MoreUtils qw[all any];

=head1 NAME

Zaaksysteem::Backend::Object::Data::Roles::Case - Object behaviors for the
'case' object_class instances

=head1 SYNOPSIS

Implements specific code behaviors exclusive to object instances with class
'case'.

=head1 CONSTANTS

=head2 ATTRIBUTES

This constant is an ARRAYREF containing a listing of magic string names that
should be included in the free-text vector.

=cut

use constant ATTRIBUTES => [qw[
    case.requestor.full_name
    case.requestor.first_names
    case.requestor.family_name
    case.requestor.bsn
    case.requestor.email
    case.requestor.name
    case.recipient.full_name
    case.recipient.first_names
    case.recipient.family_name
    case.recipient.bsn
    case.recipient.email
    case.assignee
    case.assignee.email
    case.assignee.phone_number
    case.coordinator

    case.number
    case.subject
    case.date_of_registration
    case.date_of_completion

    case.casetype.name
]];

=head2 SEARCH_ONLY_ATTRIBUTES

This constant contains the list of attributes that subjects with the 'search'
permission on a case are able to see. It should be sufficient to identify the
case and find the responsible parties.

=cut

use constant SEARCH_ONLY_ATTRIBUTES => [qw[
    number
    requestor.name
    casetype.name
    status
    assignee
    progress_status
    subject
    phase
    result
    date_of_registration
    date_target
    days_left
]];

=head2 ACTIONS

This constant defines an array of actions that can be performed on cases.

Each action is encoded as a hashref, like the following:

    {
        implied_via => '<original permission type>',
        slug => '<action shorthand name>'
    }

=cut

use constant ACTIONS => [
    { implied_via => 'read',   slug => 'relate'   },
    { implied_via => 'read',   slug => 'export'   },

    { implied_via => 'write',  slug => 'allocate' },
    { implied_via => 'write',  slug => 'acquire'  },
    { implied_via => 'write',  slug => 'suspend'  },
    { implied_via => 'write',  slug => 'resume'   },
    { implied_via => 'write',  slug => 'prolong'  },
    { implied_via => 'write',  slug => 'publish'  },

    { implied_via => 'manage', slug => 'manage'   },
    { implied_via => 'manage', slug => 'destroy'  }
];

=head1 METHODS

=head2 text_vector_terms

This method overrides L<Zaaksysteem::Backend::Object::Data::Component>'s
C<text_vector_terms> method, since case-like objects should not have a
generic free-text vector. See the C<ATTRIBUTES> constant for a listing of
magic strings we want to include in the vector.

=cut

override text_vector_terms => sub {
    my $self = shift;
    my @terms;

    for my $attr (@{ $self->object_attributes }) {
        # Yeah, this is a bit nasty, but we can't call
        # $object->get_attribute_by_name yet, so it'll have to do.
        # TODO refactor when ZS-2178 lands in quarterly.
        next unless grep { $attr->name eq $_ } @{ ATTRIBUTES() };

        push @terms, $attr->vectorize_value;
    }

    return uniq grep { defined } @terms;
};

=head2 get_source_object

This method overrides L<Zaaksysteem::Backend::Object::Data::Component>'s
C<get_source_object> to retrieve a C<Zaak> object based on the C<object_id> of
this row.

=cut

override get_source_object => sub {
    my $self = shift;

    my $object_id = $self->object_id;
    my $zaak = $self->result_source->schema->resultset('Zaak')->find($object_id);

    return $zaak;
};

=head2 trigger_logging

Overrides L<Zaaksysteem::Backend::Object::Data::Component/trigger_logging>.
This method intercepts logging triggers for objects so that for case objects
events are logged via the old case infrastructure.

=cut

override trigger_logging => sub {
    my $self = shift;
    my $type = shift;
    my $fields = shift // {};

    $fields->{ object_uuid } = $self->uuid;
    $fields->{ component } = 'zaak';
    $fields->{ component_id } = $self->object_id;

    return $self->get_source_object->trigger_logging($type, $fields);
};

=head2 documents

Returns a resultset with all accepted, non-destroyed/deleted files currently
associated with the case this method is invoked on.

    my $files_rs = $case_object->documents;

The search parameters for finding the files can be configured by passing
this method a hashlist of additional or overriding parameters;

    my $unaccepted_files = $case_object->documents(accepted => 0);

=cut

sig documents => '%Any';

sub documents {
    my $self = shift;
   
    # WARNING: The contents of this subrouting is also used in our cached version
    # in /api/v1/case. Where case_documents are loaded in our serialized opts to prevent
    # querying our documents table at every hit. When you change something below, please
    # change it also in Controller/API/v1/Case.pm

    my %conditions = (
        case_id => $self->object_id,
        accepted => 1,
        date_deleted => undef,
        destroyed => 0,
        @_
    );

    return $self->result_source->schema->resultset('File')->search_rs(
        \%conditions,
        { prefetch => [qw[filestore_id metadata_id]] }
    );
}

=head2 TO_JSON

This method wraps L<Zaaksysteem::Backend::Object::Data::Component/TO_JSON> and
adds a key, C<actions>, to the original method's return value. The values for
the added key represent possible actions that can be performed on objects of
this type.

=cut

around TO_JSON => sub {
    my $orig = shift;
    my $self = shift;

    my $retval = $self->$orig(@_);

    my @capabilities = @{ $self->acl_capabilities };
    my @actions;

    if (scalar @capabilities) {
        for my $action (@{ ACTIONS() }) {
            next unless grep { $action->{ implied_via } eq $_ } @capabilities;

            push @actions, $action->{ slug };
        }
    } else {
        @actions = map { $_->{ slug } } @{ ACTIONS() };
    }

    $retval->{ actions } = \@actions;

    my @only_search = $self->get_search_attributes();

    if (@only_search) {
        my %values = %{$retval->{values}};
        for my $key (keys %values) {
            next if any { $_ eq $key } @only_search;
            $values{ $key } = undef;
        }
        $retval->{ values } = \%values;
    }
    return $retval;
};

sub get_search_attributes {
    my $self = shift;
    my @capabilities = @{ $self->acl_capabilities };
    if (scalar @capabilities && all { $_ eq 'search' } @capabilities) {
        my @core_keys = map { sprintf 'case.%s', $_ } @{
            SEARCH_ONLY_ATTRIBUTES()
        };
        return @core_keys;
    }
    return;
}

sub _init_casetype_relation {
    my $self = shift;

    my $casetype_rel = $self->object_relation_object_ids->search_rs(
        { object_type => 'casetype' })->first;

    return $casetype_rel if defined $casetype_rel;

    return $self->get_source_object->_rewrite_object_casetype($self);
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

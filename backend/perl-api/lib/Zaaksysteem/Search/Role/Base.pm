package Zaaksysteem::Search::Role::Base;
use Moose::Role;

use Zaaksysteem::Search::ZQL;

requires qw(
    build_resultset
    search_documents
    search_select
    search_intake
    search_distinct
);

has user => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Schema::Subject',
    required => 1,
);

has query => (
    is => 'ro',
    isa => 'Str',
    predicate => 'has_query',
);

has base_rs => (
    is        => 'ro',
    isa       => 'Defined',
    predicate => 'has_base_rs',
);

has zql => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Search::ZQL',
    lazy     => 1,
    builder  => '_build_zql_parser',
    init_arg => undef,
);

sub _build_zql_parser {
    my $self = shift;
    return Zaaksysteem::Search::ZQL->new($self->query);
}

has resultset => (
    is       => 'ro',
    isa      => 'Defined',
    lazy     => 1,
    builder  => '_build_resultset',
    init_arg => undef,
);

sub _build_resultset {
    my $self = shift;
    return $self->zql->apply_to_resultset(
        $self->has_base_rs ? $self->base_rs : $self->object_model->acl_rs);
}

has case_rs => (
    is       => 'ro',
    isa      => 'Defined',
    lazy     => 1,
    builder  => '_build_case_rs',
    init_arg => undef,
);

sub _build_case_rs {
    my $self = shift;
    return $self->build_resultset('Zaak');
}

has library_attribute_rs => (
    is       => 'ro',
    isa      => 'Defined',
    lazy     => 1,
    builder  => '_build_library_attribute_rs',
    init_arg => undef,
);

sub _build_library_attribute_rs {
    my $self = shift;
    return $self->build_resultset('BibliotheekKenmerken');
}


has is_intake => (
    is      => 'ro',
    isa     => 'Bool',
    lazy    => 1,
    builder => '_build_is_intake',
);

sub _build_is_intake {
    my $self = shift;
    return $self->zql->cmd->can('is_intake') && $self->zql->cmd->is_intake;
}


has blacklisting => (
    is      => 'ro',
    isa     => 'HashRef',
    lazy    => 1,
    builder => '_build_blacklisted_attributes',
);

# TODO: Move to different function in bibliotheek-kenmerken-resultset
sub _build_blacklisted_attributes {
    my $self = shift;

    my $rs     = $self->library_attribute_rs;
    my $search = "@> '{\"sensitive_field\":\"on\"}'::jsonb";
    $rs
        = $rs->search_rs(
        { deleted => undef, 'properties::jsonb' => \"$search" },
        { select  => [qw(id magic_string)] });

    my %blacklist;

    my $prefix = 'attribute.';
    while (my $found = $rs->next) {
        $blacklist{ $prefix . $found->magic_string } = $found;
    }

    # Also hide keys found in zs::attributes
    my @attrs = grep { $_->is_sensitive }
        Zaaksysteem::Attributes->predefined_case_attributes();
    foreach (@attrs) {
        $blacklist{ $_->name } = $_;
    }
    return \%blacklist;
}

has is_search_distinct => (
    is      => 'ro',
    isa     => 'Bool',
    lazy    => 1,
    builder => '_is_search_distinct',
);

sub _is_search_distinct {
    my $self = shift;
    return $self->zql->cmd->distinct ? 1 : 0;
}

sub assert_zql_command_for {
    my ($self, $type) = @_;

    return 1 if $self->check_zql_command_for($type);

    my $zql = $self->zql->cmd->from->value;
    croak("zql from '$zql' does not support '$type'");
}

sub check_zql_command_for {
    my ($self, $type) = @_;
    return $type eq $self->zql->cmd->from->value;
}

sub search {
    my $self = shift;

    if ($self->zql->cmd->isa('Zaaksysteem::Search::ZQL::Command::Count')) {
        return $self->search_count();
    }
    elsif ($self->zql->cmd->isa('Zaaksysteem::Search::ZQL::Command::Select')) {
        return $self->search_select();
    }
    croak("Unable to search on ZQL cmd " . $self->zql->cmd);
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.

package Zaaksysteem::Search::Term::Literal;
use Zaaksysteem::Moose;

extends 'Zaaksysteem::Search::Term';

use Moose::Util::TypeConstraints qw(find_type_constraint);
use Zaaksysteem::DB::HStore;
use Scalar::Util qw(blessed);

with 'MooseX::Log::Log4perl';

=head1 NAME

Zaaksysteem::Search::Term::Literal - "Literal" term of a conditional

=head1 ATTRIBUTES

=head2 value

The value of this term.

=head1 METHODS

=head2 evaluate

Evaluate the term. Returns an array:

    "?", $self->value

=cut

has value => (
    is       => 'ro',
    required => 1,
);

override 'evaluate' => sub {
    my $self = shift;
    my ($resultset, $conditional) = @_;

    my $value = $self->value;
    my $type = $self->guess_type($value);

    if ($type eq 'DATE') {
        return (sprintf("hstore_to_timestamp('%s')::date", $value->rfc3339));
    }

    if ($conditional->lterm->does('Zaaksysteem::Search::Term::HStoreColumn')) {
        $value = Zaaksysteem::DB::HStore::encode({ $conditional->lterm->column => $value });
    }

    return ("?", [{} => $value]);
};

{
    my %types = (
        Int => find_type_constraint('Int'),
        Num => find_type_constraint('Num'),
    );

    sub guess_type {
        my $self = shift;

        if (blessed($self->value) && $self->value->isa('DateTime')) {
            return 'DATE';
        }
        elsif ($types{Int}->check($self->value)) {
            return 'NUMERIC';
        }
        elsif ($types{Num}->check($self->value)) {
            return 'NUMERIC';
        }

        return 'TEXT';
    }
}

__PACKAGE__->meta->make_immutable();



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 guess_type

TODO: Fix the POD

=cut


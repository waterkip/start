use utf8;
package Zaaksysteem::Schema::CaseAttributesV1;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::CaseAttributesV1

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';
__PACKAGE__->table_class("DBIx::Class::ResultSource::View");

=head1 TABLE: C<case_attributes_v1>

=cut

__PACKAGE__->table("case_attributes_v1");
__PACKAGE__->result_source_instance->view_definition(" SELECT case_attributes.case_id,\n    case_attributes.magic_string,\n    case_attributes.library_id,\n    attribute_value_to_jsonb(case_attributes.value, case_attributes.value_type) AS value\n   FROM case_attributes\nUNION ALL\n SELECT case_attributes_appointments.case_id,\n    case_attributes_appointments.magic_string,\n    case_attributes_appointments.library_id,\n    appointment_attribute_value_to_jsonb(case_attributes_appointments.value, case_attributes_appointments.reference) AS value\n   FROM case_attributes_appointments\nUNION ALL\n SELECT case_documents_cache.case_id,\n    case_documents_cache.magic_string,\n    case_documents_cache.library_id,\n    case_documents_cache.value_v1 AS value\n   FROM case_documents_cache");

=head1 ACCESSORS

=head2 case_id

  data_type: 'bigint'
  is_nullable: 1

=head2 magic_string

  data_type: 'text'
  is_nullable: 1

=head2 library_id

  data_type: 'integer'
  is_nullable: 1

=head2 value

  data_type: 'jsonb'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "case_id",
  { data_type => "bigint", is_nullable => 1 },
  "magic_string",
  { data_type => "text", is_nullable => 1 },
  "library_id",
  { data_type => "integer", is_nullable => 1 },
  "value",
  { data_type => "jsonb", is_nullable => 1 },
);


# Created by DBIx::Class::Schema::Loader v0.07051 @ 2023-02-24 00:07:52
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:a+K6/sd5DmNY1l99L1DYFg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.

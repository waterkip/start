use utf8;
package Zaaksysteem::Schema::CasetypeEndStatus;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::CasetypeEndStatus

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<casetype_end_status>

=cut

__PACKAGE__->table("casetype_end_status");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_nullable: 1

=head2 zaaktype_node_id

  data_type: 'integer'
  is_nullable: 1

=head2 status

  data_type: 'integer'
  is_nullable: 1

=head2 status_type

  data_type: 'text'
  is_nullable: 1

=head2 naam

  data_type: 'text'
  is_nullable: 1

=head2 created

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 last_modified

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 ou_id

  data_type: 'integer'
  is_nullable: 1

=head2 role_id

  data_type: 'integer'
  is_nullable: 1

=head2 checklist

  data_type: 'integer'
  is_nullable: 1

=head2 fase

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 role_set

  data_type: 'integer'
  is_nullable: 1

=head2 termijn

  data_type: 'integer'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_nullable => 1 },
  "zaaktype_node_id",
  { data_type => "integer", is_nullable => 1 },
  "status",
  { data_type => "integer", is_nullable => 1 },
  "status_type",
  { data_type => "text", is_nullable => 1 },
  "naam",
  { data_type => "text", is_nullable => 1 },
  "created",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "last_modified",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "ou_id",
  { data_type => "integer", is_nullable => 1 },
  "role_id",
  { data_type => "integer", is_nullable => 1 },
  "checklist",
  { data_type => "integer", is_nullable => 1 },
  "fase",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "role_set",
  { data_type => "integer", is_nullable => 1 },
  "termijn",
  { data_type => "integer", is_nullable => 1 },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2021-03-29 14:32:34
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:E9EgDe3XDA5Nb4qx0jL81w


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.

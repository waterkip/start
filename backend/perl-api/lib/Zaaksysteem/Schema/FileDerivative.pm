use utf8;
package Zaaksysteem::Schema::FileDerivative;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::FileDerivative

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<file_derivative>

=cut

__PACKAGE__->table("file_derivative");

=head1 ACCESSORS

=head2 id

  data_type: 'uuid'
  default_value: uuid_generate_v4()
  is_nullable: 0
  retrieve_on_insert: 1
  size: 16

=head2 file_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 filestore_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 max_width

  data_type: 'integer'
  is_nullable: 0

=head2 max_height

  data_type: 'integer'
  is_nullable: 0

=head2 date_generated

  data_type: 'timestamp'
  default_value: timezone('UTC'::text, current_timestamp)
  is_nullable: 0
  timezone: 'UTC'

=head2 type

  data_type: 'text'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type => "uuid",
    default_value => \"uuid_generate_v4()",
    is_nullable => 0,
    retrieve_on_insert => 1,
    size => 16,
  },
  "file_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "filestore_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "max_width",
  { data_type => "integer", is_nullable => 0 },
  "max_height",
  { data_type => "integer", is_nullable => 0 },
  "date_generated",
  {
    data_type     => "timestamp",
    default_value => \"timezone('UTC'::text, current_timestamp)",
    is_nullable   => 0,
    timezone      => "UTC",
  },
  "type",
  { data_type => "text", is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<file_derivative_unique>

=over 4

=item * L</file_id>

=item * L</type>

=item * L</max_width>

=item * L</max_height>

=back

=cut

__PACKAGE__->add_unique_constraint(
  "file_derivative_unique",
  ["file_id", "type", "max_width", "max_height"],
);

=head1 RELATIONS

=head2 file_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::File>

=cut

__PACKAGE__->belongs_to("file_id", "Zaaksysteem::Schema::File", { id => "file_id" });

=head2 filestore_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Filestore>

=cut

__PACKAGE__->belongs_to(
  "filestore_id",
  "Zaaksysteem::Schema::Filestore",
  { id => "filestore_id" },
);


# Created by DBIx::Class::Schema::Loader v0.07052 @ 2024-01-29 06:08:34
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:NJNsK/mqH/pc7kFvCVMwHA


1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

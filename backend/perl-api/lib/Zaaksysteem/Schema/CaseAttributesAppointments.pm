use utf8;
package Zaaksysteem::Schema::CaseAttributesAppointments;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::CaseAttributesAppointments

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';
__PACKAGE__->table_class("DBIx::Class::ResultSource::View");

=head1 TABLE: C<case_attributes_appointments>

=cut

__PACKAGE__->table("case_attributes_appointments");
__PACKAGE__->result_source_instance->view_definition(" SELECT z.id AS case_id,\n    COALESCE(od.properties, '{}'::text) AS value,\n    bk.magic_string,\n    bk.id AS library_id,\n    od.uuid AS reference\n   FROM ((((zaak z\n     JOIN zaaktype_kenmerken ztk ON ((z.zaaktype_node_id = ztk.zaaktype_node_id)))\n     JOIN bibliotheek_kenmerken bk ON (((bk.id = ztk.bibliotheek_kenmerken_id) AND (bk.value_type = 'appointment'::text))))\n     LEFT JOIN zaak_kenmerk zk ON (((z.id = zk.zaak_id) AND (zk.bibliotheek_kenmerken_id = ztk.bibliotheek_kenmerken_id))))\n     LEFT JOIN object_data od ON (((zk.value[1])::uuid = od.uuid)))\n  GROUP BY z.id, COALESCE(od.properties, '{}'::text), bk.magic_string, bk.id, od.uuid");

=head1 ACCESSORS

=head2 case_id

  data_type: 'bigint'
  is_nullable: 1

=head2 value

  data_type: 'text'
  is_nullable: 1

=head2 magic_string

  data_type: 'text'
  is_nullable: 1

=head2 library_id

  data_type: 'integer'
  is_nullable: 1

=head2 reference

  data_type: 'uuid'
  is_nullable: 1
  size: 16

=cut

__PACKAGE__->add_columns(
  "case_id",
  { data_type => "bigint", is_nullable => 1 },
  "value",
  { data_type => "text", is_nullable => 1 },
  "magic_string",
  { data_type => "text", is_nullable => 1 },
  "library_id",
  { data_type => "integer", is_nullable => 1 },
  "reference",
  { data_type => "uuid", is_nullable => 1, size => 16 },
);


# Created by DBIx::Class::Schema::Loader v0.07051 @ 2023-02-24 00:07:52
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:ia0ErhrQJSgLccv2qeN5xQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.

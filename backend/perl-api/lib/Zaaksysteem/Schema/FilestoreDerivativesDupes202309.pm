use utf8;
package Zaaksysteem::Schema::FilestoreDerivativesDupes202309;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::FilestoreDerivativesDupes202309

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<filestore_derivatives_dupes_202309>

=cut

__PACKAGE__->table("filestore_derivatives_dupes_202309");

=head1 ACCESSORS

=head2 id

  data_type: 'uuid'
  is_nullable: 1
  size: 16

=head2 file_id

  data_type: 'integer'
  is_nullable: 1

=head2 filestore_id

  data_type: 'integer'
  is_nullable: 1

=head2 max_width

  data_type: 'integer'
  is_nullable: 1

=head2 max_height

  data_type: 'integer'
  is_nullable: 1

=head2 date_generated

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 type

  data_type: 'text'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "uuid", is_nullable => 1, size => 16 },
  "file_id",
  { data_type => "integer", is_nullable => 1 },
  "filestore_id",
  { data_type => "integer", is_nullable => 1 },
  "max_width",
  { data_type => "integer", is_nullable => 1 },
  "max_height",
  { data_type => "integer", is_nullable => 1 },
  "date_generated",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "type",
  { data_type => "text", is_nullable => 1 },
);


# Created by DBIx::Class::Schema::Loader v0.07051 @ 2023-09-28 13:50:51
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:1J/w9kJPSyn3/+jYEiCi/Q


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2023, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.

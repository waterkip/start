package Zaaksysteem::DB::Component::Logging::Auth::Alternative::EmailAddress;
use Moose::Role;
with qw(Zaaksysteem::Moose::Role::LoggingSubject);


=head2 subject

Returns a L<Zaaksysteem::Model::DB::Subject> object

=cut

has subject => (
    is      => 'ro',
    lazy    => 1,
    default => sub {
        my $self = shift;
        $self->rs('Subject')->find($self->component_id);
    }
);

=head2 onderwerp

Pretty print the subject

=cut

sub onderwerp {
    my $self = shift;

    my $subject = $self->subject;

    if ($subject) {
        return sprintf("Emailadres aangepast voor '%s' van '%s' naar '%s'", $subject->username,
            $self->data->{old}, $self->data->{new});
    }
    else {
        return sprintf("Emailadres aangepast van '%s' naar '%s'",
            $self->data->{old}, $self->data->{new});
    }
}

sub event_category { 'system'; }

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

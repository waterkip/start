package Zaaksysteem::DB::Component::Logging::Export::AdvancedSearch;
use Moose::Role;
with qw(Zaaksysteem::Moose::Role::LoggingSubject);


=head1 NAME

Zaaksysteem::DB::Component::Logging::Export::AdvanceSearch - Logging component for exports of the advance search

=head2 onderwerp

Generate the main description for this event.

=cut

sub onderwerp {
    my $self = shift;

    my $data = $self->data;

    return sprintf("Export '%s' van '%s' is %s",
        $data->{export}, $data->{username},
        $data->{error} ? "mislukt" : "afgerond");
}


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

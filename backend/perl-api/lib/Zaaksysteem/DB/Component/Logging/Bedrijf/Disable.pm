package Zaaksysteem::DB::Component::Logging::Bedrijf::Disable;
use Moose::Role;
with qw(Zaaksysteem::Moose::Role::LoggingSubject);

=head1 NAME

Zaaksysteem::DB::Component::Logging::Bedrijf::Disable - Logging component for Natuurlijk Persoon disable actions

=head2 onderwerp

Generate the main description for this event.

=cut

sub onderwerp {
    my $self = shift;

    my $data = $self->data;

    return sprintf(
      'Bedrijf met KvK-nummer %s (id: %d) is %s.',
      $data->{coc_number}, $data->{id},
      $data->{deleted} ? 'verwijderd' : 'geinactiveerd'
    );
}


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

package Zaaksysteem::DB::Component::Logging::NatuurlijkPersoon::Create;
use Moose::Role;
with qw(Zaaksysteem::Moose::Role::LoggingSubject);


=head1 NAME

Zaaksysteem::DB::Component::Logging::NatuurlijkPersoon::Create - Logging component for Natuurlijk Persoon create actions

=head2 onderwerp

Generate the main description for this event.

=cut

sub onderwerp {
    my $self = shift;

    my $data = $self->data;

    return sprintf(
        'Natuurlijk persoon (id: %d) is aangemaakt',
        $data->{id}
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2023, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.

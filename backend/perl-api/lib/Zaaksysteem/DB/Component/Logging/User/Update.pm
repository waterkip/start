package Zaaksysteem::DB::Component::Logging::User::Update;

use Moose::Role;
with qw(Zaaksysteem::Moose::Role::LoggingSubject);


sub onderwerp {
    my $self = shift;

    return
        sprintf("Gebruiker '%s' aangepast", $self->data->{new}{displayname});
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2024, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.

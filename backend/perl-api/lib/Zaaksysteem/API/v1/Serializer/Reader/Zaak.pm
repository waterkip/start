package Zaaksysteem::API::v1::Serializer::Reader::Zaak;

use Moose;

with 'Zaaksysteem::API::v1::Serializer::ReaderRole';

=head1 NAME

=head1 SYNOPSIS

=head1 METHODS

=head2 class

=cut

sub class { 'Zaaksysteem::Model::DB::Zaak' }

=head2 read

=cut

my @objects_to_serialize = qw(
    milestone
    outcome
    route
    requestor
    assignee
    coordinator
);

sub read {
    my ($class, $serializer, $case, $opts) = @_;

    return $case->as_object;
}


__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.

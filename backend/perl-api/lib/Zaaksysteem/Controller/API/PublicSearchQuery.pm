package Zaaksysteem::Controller::API::PublicSearchQuery;

use Moose;
use namespace::autoclean;
use JSON;

use BTTW::Tools;
use Zaaksysteem::Search::Object;

use Zaaksysteem::Types qw/UUID/;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

=head2 index

Base controller for SuperSaaS API-calls

=cut

=head2 public_search_base

Base controller for public_search_base

=cut

sub public_search_base : Chained('/api/base') : PathPart('public_search') : CaptureArgs(1): DisableACL {
    my ($self, $c, $interface_id) = @_;
    my $raw_search_query;

    throw('api/public_search/invalid_interface_id', 'Invalid ID given') unless $interface_id =~ /^\d+$/;

    my $interface           = $c->model('DB::Interface')->search_active(
        {
            id          => $interface_id,
            module      => 'publicsearchquery'
        }
    )->first;

    if (!$interface) {
        throw('api/public_search/not_found', 'Public search query not found');
    }

    my $search_query_id     = $interface->get_interface_config->{query}->{id};
    $raw_search_query       = $c->model('Object')->retrieve(uuid => $search_query_id) if $search_query_id;

    my $query = from_json($raw_search_query->{query})->{zql};

    if (!$raw_search_query) {
        throw('api/public_search/no_saved_search', 'Saved search not found')
    }

    ### Retrieve user
    my $user = $c->model('DB::Subject')->search_active(
        { 'me.id' => $interface->get_interface_config->{user}->{id} }
    )->first;

    if (!$user) {
        throw('api/public_search/no_user_found', 'No user to query ZQL found');
    }

    my $model = try {
        my $om = Zaaksysteem::Search::Case::Object->new(
            schema => $c->model('DB')->schema,
            user   => $user,
            query  => $query,
        );
        $om->check_zql_command_for('case');
        return $om;
    } catch {
        return if $_ =~ m#^case/zql/object_type:#;
        $self->log->info("$_");
        die $_;
    };

    $model //= Zaaksysteem::Search::Object->new(
        query        => $query,
        object_model => $c->model('Object', user => $user),
        user         => $user,
        schema       => $c->model('DB')->schema,
    );

    $c->stash->{object_search} = $model;
    $c->stash->{zql}           = $model->zql;
    $c->stash->{search_rs}     = $model->resultset;
}


=head2 search

Returns a resultset of objects for the given type. Now, only the object type
case is here to use.

=cut

sub search : Chained('public_search_base') : PathPart('search') : Args(0) : DisableACL: ZAPI {
    my ($self, $c) = @_;

    $c->forward('/api/object/search_select');

    ### Protect attributes from going to the outside
    # $c->stash->{zapi}->object_requested_attributes([qw/case.status/]);
}


=head2 configuration_base

Public URL for getting search configuration

=cut

sub configuration_base : Chained('/api/base') : PathPart('public_search') : CaptureArgs(1): DisableACL {}


=head2 configuration

Public URL for getting search configuration

=cut

sub configuration : Chained('configuration_base') : PathPart('config') : Args(1) : DisableACL: ZAPI {
    my ($self, $c) = @_;

    $c->forward('/api/search/configuration', ['case']);
}

=head2 file_base

Public URL for getting files via de public search API

=cut

sub file_base : Chained('public_search_base') : PathPart('file') : CaptureArgs(1): DisableACL {
    my ($self, $c, $filestore_id) = @_;

    $c->detach unless UUID->check($filestore_id);

    my $filestore
        = $c->model('DB::Filestore')->search({ uuid => $filestore_id })->first;

    if (!$filestore) {
        throw(
            'api/public_search/no_file_found',
            "No file found by UUID: $filestore_id",
        );
    }

    my $file_rs = $filestore->files;

    my @cases;

    while (my $file = $file_rs->next) {
        my $case_id = $file->get_column('case_id');
        push(@cases, $case_id) if $case_id;
    }

    if (!@cases) {
        throw(
            'api/public_search/no_file_found',
            "No file found by UUID: $filestore_id",
        );
    }

    my $model = $c->stash->{object_search};

    my $case_rs = $model->search_select();
    $case_rs = $case_rs->search_restricted('read', $model->user);
    $case_rs = $case_rs->search_rs({ 'me.id' => { -in => \@cases } }, { rows => 1 });

    if ($case_rs->first) {
        $c->stash->{filestore} = $filestore;
        return;
    }

    throw(
        'api/public_search/permission_denied',
        'Insufficient permissions'
    );
}

=head2 file

Public URL for getting files via de public search API

=cut

sub file : Chained('file_base') : PathPart('') : Args(0) : DisableACL {
    my ($self, $c) = @_;

    $c->forward('/api/object/serve_file', [$c->stash->{filestore}]);
}


__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

package Zaaksysteem::Controller::API::Case;
use Zaaksysteem::Moose;

use JSON;
use List::MoreUtils qw[none];
use Zaaksysteem::ZTT;
use Zaaksysteem::Zaken::PlannedCase;
use Zaaksysteem::Zaken::RelatedObjects;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

=head1 NAME

Zaaksysteem::Controller::API::Case - ZAPI Case controller

=head1 DESCRIPTION

Case API. Based on "old" case backend.

=head1 ACTIONS

=head2 base

=cut

sub _get_case {
    my ($self, $c, $case_id) = @_;

    my $case = try {
        return $c->model('DB::Zaak')->find($case_id)
    } catch {
        throw('api/case/retrieval_fault', sprintf(
            'Internal error while retrieving case by id "%s"',
            $case_id
        ));
    };

    if (!defined $case || $case->is_deleted) {
        throw('api/case/find_case', sprintf(
            'Unable to find case by id "%s"',
            $case_id
        ));
    }

    return $case
        if $c->check_any_given_zaak_permission($case,
            qw[zaak_read zaak_beheer zaak_edit]
        );

    throw('api/case/authorization', 'Access denied');
}

sub base : Chained('/api/base') : PathPart('case') : CaptureArgs(1) {
    my ($self, $c, $case_id) = @_;

    $c->stash->{zaak} = $self->_get_case($c, $case_id);
    $c->stash->{case} = $c->stash->{zaak}->object_data;

    $c->stash->{zapi} = [];

    # We're special, this controller has a seperate implementation, we don't
    # want the Object model to get in our way.
    $c->model('Object')->prefetch_relations(0);
}

=head2 caseless_base

This base action exists such that APIs that are considered part of the Case
domain can chain off the API like L<Zaaksysteem::Controller::API::Case::File>
does, but without a case number.

=cut

sub caseless_base : Chained('/api/base') : PathPart('case') : CaptureArgs(0) { }

=head2 get

=head3 URL

C</api/case/[CASE_ID]>

=cut

sub get: Chained('base') : PathPart('') : Args(0): DB('ROW') : ZAPI {
    my ($self, $c) = @_;
    return;
}

sub planned_cases : Chained('base') : PathPart('planned-cases') : ZAPI
    : DB(RO) {
    my ($self, $c, $case_id) = @_;

    my $model = Zaaksysteem::Zaken::PlannedCase->new(
        schema => $c->model('DB')->schema,
        case   => $c->stash->{zaak},
    );

    $c->stash->{zapi} = $model->get_planned_cases;
}

sub related_objects : Chained('base') : PathPart('related-objects') : ZAPI
    : DB(RO) {
    my ($self, $c, $case_id) = @_;

    my $model = Zaaksysteem::Zaken::RelatedObjects->new(
        schema => $c->model('DB')->schema,
        case   => $c->stash->{zaak},
    );

    $c->stash->{zapi} = $model->get_related_objects;
}

=head2 confidentiality

Set a new confidentiality category for the referenced case.

=head3 URL

C</api/case/[CASE_ID]/confidentiality>

=head3 Parameters

=over 4

=item confidentiality

=back

=head3 Response

Return value: ZAPI on success

    {
        results: {
            message: 'Confidentiality updated',
            value: $params->{confidentiality}
        }
    }

=cut

### TODO: Parameter checking
sub confidentiality: Chained('base') : PathPart('confidentiality') : ZAPI {
    my ($self, $c) = @_;

    throw ('api/case/confidentiality', 'access violation')
        unless $c->check_any_zaak_permission('zaak_read','zaak_beheer','zaak_edit');

    throw ('api/case/confidentiality/method_not_post', 'only POST requests allowed')
        unless $c->req->method eq 'POST';

    my $new_value = $c->req->params->{confidentiality};

    $c->stash->{zaak}->set_confidentiality($new_value);
    $c->stash->{zaak}->update;

    $c->stash->{zapi} = [{
        message => 'Confidentiality updated',
        value   => $c->stash->{zaak}->confidentiality
    }];
}

=head2 request_attribute_update

request update to a field. the citizens are not allowed to change fields
without an official reviewing first, this sends a request to the official.

=head3 URL

C</api/case/[CASE_ID]/request_attribute_update>

=cut

sub request_attribute_update : Chained('base') : PathPart('request_attribute_update') : ZAPI {
    my ($self, $c, $bibliotheek_kenmerken_id) = @_;

    my $params = $c->req->params;

    throw('api/case/request_attribute_update', "need bibliotheek_kenmerken_id")
        unless $bibliotheek_kenmerken_id;

    my $kenmerk = $c->stash->{zaak}->zaaktype_node_id->zaaktype_kenmerken->search_rs(
        {
            bibliotheek_kenmerken_id => $bibliotheek_kenmerken_id
        },
        {
            prefetch => 'bibliotheek_kenmerken_id'
        },
    )->first;

    if (!$kenmerk) {
        throw('pip/update_field', "Couldn't find requested attribute.");
    }

    # get it, put on the stash where we need it
    $c->forward('stash_submitted_field_value', [ $kenmerk, $params ]);

    my $subject_identifier = $c->stash->{ subject_identifier } // ($c->user_exists ? $c->user->betrokkene_identifier : undef);

    if ($self->_can_skip_change_approval($c, $kenmerk)) {
        $c->stash->{ zaak }->zaak_kenmerken->update_fields({
            new_values => { $bibliotheek_kenmerken_id => $c->stash->{ veldoptie_value } },
            zaak       => $c->stash->{zaak},
        });
    }
    else {
        my $scheduled_job = $c->model('DB')->resultset('ScheduledJobs')->create_task({
            task                        => 'case/update_kenmerk',
            bibliotheek_kenmerken_id    => $bibliotheek_kenmerken_id,
            value                       => $c->stash->{ veldoptie_value },

            # as soon as the subjects table /user management system
            # include the citizens we could add them here
            created_by                  => $subject_identifier,
            reason                      => $params->{ toelichting },
            case                        => $c->stash->{ zaak },
        });

        # This will save the attribute values in the DB for case_property table
        $c->stash->{zaak}->ddd_set_attribute_values(
            $kenmerk->bibliotheek_kenmerken_id->magic_string
        );

        $c->stash->{zaak}->update_unaccepted_attribute_updates;

        # Update num_unaccepted_XXXX fields in the case object
        $c->stash->{zaak}->touch();

        $self->_log_update_field($c, {
            subject_name => $scheduled_job->format_created_by,
            reason       => $params->{toelichting},
            new_values   => $c->stash->{ veldoptie_value },
            value_type   => $kenmerk->bibliotheek_kenmerken_id->value_type,
            kenmerk      => $kenmerk->label || $kenmerk->bibliotheek_kenmerken_id->naam,

            # If coming from PIP, other event type
            $c->session->{pip} ? (event_type => 'case/pip/updatefield')  : (),
        });
    }

    # this functionality will quickly become viable, so let's keep it in commented form
    #$c->forward('update_field_message', [$scheduled_job->apply_roles->description]);
    if ($c->session->{pip}) {
        $c->stash->{ template } = 'widgets/general/veldoptie_view.tt';
        $c->stash->{ nowrapper } = 1;

        $c->stash->{ veldoptie_multiple } = $kenmerk->bibliotheek_kenmerken_id->type_multiple;
        $c->stash->{ veldoptie_type } = $kenmerk->type;
        # veldoptie_value is put on the stash by sub-action

        my $html = $c->view('TT')->render($c, 'widgets/general/veldoptie_view.tt');
        $c->stash->{zapi} = [{ attribute_value_as_html => $html }];
    }
}

sub _can_skip_change_approval {
    my $self = shift;
    my ($c, $kenmerk) = @_;

    # Appointment updates don't require approval.
    return 1 if ($kenmerk->bibliotheek_kenmerken_id->value_type eq 'appointment');
    return 1 if ($kenmerk->bibliotheek_kenmerken_id->value_type eq 'calendar_supersaas');
    return 1 if ($kenmerk->bibliotheek_kenmerken_id->value_type eq 'appointment_v2');

    # In other cases: only logged-in users with special permission can skip the
    # approval queue, and the only for specially configured attributes.
    return if not $c->user_exists;
    return if not $kenmerk->properties->{skip_change_approval};

    return 1 if $c->check_field_permission($kenmerk);

    return;
}

=head2 stash_submitted_field_value

B<PRIVATE>

=cut

sub stash_submitted_field_value : Private {
    my ($self, $c, $kenmerk, $params) = @_;

    my $key = 'kenmerk_id_' . $kenmerk->get_column('bibliotheek_kenmerken_id');
    my $value = $params->{ $key };

    # if no checkboxes are checked, we will receive nothing, which would be OK
    if ($params->{ $key . '_checkbox' }) {
        $value //= [];
    }

    unless (exists $params->{$key} || exists $params->{"${key}_checkbox"}) {
        throw('pip/field_value', 'Unable to get value for attribute "%s"', $kenmerk->bibliotheek_kenmerken_id->naam);
    }

    $c->stash->{ veldoptie_value } = $value;
}

=head2 end

Implements logic to be executed after every case api request.

=cut

sub end {
    my ($self, $c) = @_;

    $c->forward('/broadcast_queued_items');

    return;
}

=head1 HELPER METHODS

=head2 _log_update_field

=cut

define_profile _log_update_field => (
    required => [qw[kenmerk value_type subject_name]],
    optional => [qw[new_values reason event_type]],
    defaults => { event_type => 'case/update/field' },
);

sub _log_update_field {
    my ($self, $c, $opts) = @_;

    my $valid_opts = assert_profile($opts)->valid;

    my $new_values = $valid_opts->{value_type} =~ m|^bag| ?
        $c->model('Gegevens::Bag')->humanize($valid_opts->{new_values}) :
        $valid_opts->{ new_values };

    if (ref $new_values eq 'ARRAY') {
        ($new_values) = @{ $new_values } if scalar @{ $new_values } == 1;
    }

    my $log = $c->model('DB::Logging')->trigger($valid_opts->{event_type}, {
        component => 'zaak',
        zaak_id   => $c->stash->{zaak}->id,
        data => {
            subject_name => $valid_opts->{ subject_name },
            kenmerk      => $valid_opts->{ kenmerk },
            toelichting  => $valid_opts->{reason} // '',
            new_values   => $new_values
        }
    });

    $c->stash->{zaak}->create_message_for_behandelaar(
        message    => $valid_opts->{reason} // "Geen toelichting gegeven",
        event_type => $valid_opts->{event_type},
        log        => $log,
    );
    return 1;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

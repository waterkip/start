package Zaaksysteem::Controller::API::v1::Case::Document;

use Moose;
use namespace::autoclean;

BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

=head1 NAME

Zaaksysteem::Controller::API::v1::Case - APIv1 controller for case objects

=head1 DESCRIPTION

This is the controller API class for C<api/v1/case>. Extensive documentation about this
API can be found in:

L<Zaaksysteem::Manual::API::V1::Case>

Extensive tests about the usage via the JSON API can be found in:

L<TestFor::Catalyst::Controller::API::V1::Case>

=cut

use BTTW::Tools;
use Zaaksysteem::Types qw[UUID];
use Zaaksysteem::API::v1::ResultSet;

sub BUILD {
    my $self = shift;

    $self->add_api_control_module_type('api','app');
    $self->add_api_context_permission('extern');
}

=head1 ACTIONS

=head2 base

Reserves the C</api/v1/case/[CASE_UUID]/document> routing namespace.

=cut

sub base : Chained('/api/v1/case/instance_base') : PathPart('document') : CaptureArgs(0) {
    my ($self, $c) = @_;

    unless ($c->is_allowed_zql_field('case.documents')) {
        throw('api/v1/case/document',
            'Configured ZQL query does not allow you to view metadata of documents'
        );
    }

    $c->stash->{documents} = $c->stash->{zaak}
        ->active_files->search_rs(undef, { prefetch => 'filestore_id' },);
}

=head2 instance_base

Reserves the C</api/v1/case/[CASE_UUID]/document/[DOCUMENT_UUID]> routing namespace.

=cut

sub instance_base : Chained('base') : PathPart('') : CaptureArgs(1) {
    my ($self, $c, $uuid) = @_;

    unless (UUID->check($uuid)) {
        throw('api/v1/case/document/invalid_uuid', sprintf(
            'Document UUID "%s" invalid',
            $uuid
        ));
    }

    $c->stash->{document} = $c->stash->{documents}->search(
        {
            'filestore_id.uuid' => $uuid,
        }
    )->first;

    unless (defined $c->stash->{ document }) {
        throw('api/v1/case/document/not_found', sprintf(
            "The document object with UUID '%s' could not be found.",
            $uuid
        ), { http_code => 404 });
    }
}

=head2 list

Returns a C<set> of documents associated with the C<case> instance.

=head3 URL Path

C</api/v1/case/[CASE_UUID]/document>

=cut

sub list : Chained('base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;

    my $rs = $c->stash->{zaak}->case_documents;

    my $serializer = Zaaksysteem::API::v1::Serializer->new;

    my @documents;
    while (my $doc = $rs->next) {
        push(@documents, $serializer->read($doc));
    }

    $c->stash->{ result } = {
        type => 'set',
        instance => {
            rows => \@documents
        }
    }
}

=head2 all

Get all the documents from a case, response is similar to api/v1/document

=head3 URL Path

C</api/v1/case/[CASE_UUID]/document/all>

=cut

sub all : Chained('base') : PathPart('all') : Args(0) : RO {
    my ($self, $c) = @_;

    my $document_model = $c->model("Document");
    my $list = $document_model->list->search_rs({
        'me.case_id'        => $c->stash->{zaak}->id,
        'me.active_version' => 1,
        'me.accepted'       => 1,
    });

    $c->stash->{result} = Zaaksysteem::API::v1::ResultSet->new(
        iterator => $list,
    )->init_paging($c->req);

    return;
}

=head2 get

=head3 URL Path

C</api/v1/case/[CASE_UUID]/document/[DOCUMENT_UUID]>

=cut

sub get : Chained('instance_base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;

    $c->stash->{ result } = $c->stash->{ document };
}


=head2 download

=head3 URL Path

C</api/v1/case/[CASE_UUID]/document/[DOCUMENT_UUID]/download>

=cut

sub download : Chained('instance_base') : PathPart('download') : Args(0) : RO {
    my ($self, $c) = @_;

    my $filestore = $c->stash->{ document }->filestore_id;

    # Prevent View::API::v1 from mangling the response.
    $c->stash->{ api_unauthorized_response } = 1;

    $c->serve_filestore(
        $filestore,
        {
            "response-cache-control" => "private, must-revalidate, max-age=180",
            "response-content-disposition" => sprintf(
                'attachment; filename="%s"',
                $c->stash->{ document }->filename
            )
        },
        1, # direct download (no S3 redirect)
    );

    $c->res->header('Cache-Control', 'must-revalidate');
    $c->res->header('Pragma', 'private');
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, 2021 Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.

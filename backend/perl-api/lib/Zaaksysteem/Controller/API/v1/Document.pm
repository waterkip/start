package Zaaksysteem::Controller::API::v1::Document;
use Zaaksysteem::Moose;

BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

=head1 NAME

Zaaksysteem::Controller::API::v1::Document - APIv1 controller for documents in Zaaksysteem

=head1 DESCRIPTION

=cut

sub BUILD {
    my $self = shift;

    $self->add_api_control_module_type('api');
    $self->add_api_context_permission('extern', 'intern');
}

=head1 ACTIONS

=head2 base

Reserves the C</api/v1/document> routing namespace.

=cut

sub base : Chained('/api/v1/base') : PathPart('document') : CaptureArgs(0) {}

=head2 reserve_number

=head3 URL Path

C</api/v1/document/reserve_number>

Reserve a document number to upload a file to on a later moment

=cut

sub reserve_number : Chained('base') : PathPart('reserve_number') : Args(0) : RW {
    my ($self, $c) = @_;

    $c->stash->{result} = $c->model('Document')->generate_serial_number;
}

=head2 number_base

Reserves the C</api/v1/document/get_by_number> routing namespace.

=cut

sub number_base : Chained('base') : PathPart('get_by_number') : CaptureArgs(1) : RO {
    my ($self, $c, $id) = @_;

    my $file = $c->model('Document')->get_by_serial_number($id);

    $c->stash->{file} = $file;
    $c->stash->{file_id} = $id;

    return if !$file;

    if (my $case = $file->case_id) {
        # Let the case controller deal with the API/v1 search permissions
        $c->forward('/api/v1/case/base');
        $c->forward('/api/v1/case/instance_base', [$case->get_column('uuid')]);
    }

}

=head2 show_by_number

=head3 URL Path

C</api/v1/document/get_by_number/[ID]>

Show a file based on the serial

=cut

sub show_by_number : Chained('number_base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;

    if (!$c->stash->{file}) {
        throw(
            "api/v1/document/id/not_found",
            sprintf("Unable to find document with id: %d",
                $c->stash->{file_id}),
            { http_code => 404 },
        );
    }
    $c->stash->{result} = $c->stash->{file}->as_object;
}

=head2 upload_by_number

Upload endpoint for documents

=head3 URI Path

C</api/v1/document/get_by_number/[ID]/upload>

=cut

sub upload_by_number : Chained('number_base') : PathPart('upload') : Args(0) : RW {
    my ($self, $c) = @_;

    my @uploads = map { ref $_ eq 'ARRAY' ? @$_ : $_ } values %{ $c->req->uploads };

    my $count = @uploads;

    if ($count == 0) {
        throw(
            'api/v1/document/upload',
            sprintf('Upload missing.'),
            { http_code => 400 }
        );
    }
    elsif ($count > 1) {
        throw(
            'api/v1/document/upload',
            sprintf('Multiple uploads, only accepting one.'),
            { http_code => 400 }
        );
    }

    if (my $file = $c->stash->{file}) {
        $c->model('Document')->assert_mutation($file);
    }

    my $file = $c->model('Document')->create_new_version(
        $c->stash->{file},
        filename => $uploads[0]->filename,
        path     => $uploads[0]->tempname,
        id       => $c->stash->{file_id},
    );

    if ($file->get_column('case_id')) {
        $c->model('Queue')->emit_case_event(
            {
                case        => $file->case_id,
                event_name  => 'DocumentAddedToCase',
                description => 'Document geupload via api/v1/document',
                changes => [],
            }
        );
    }

    $c->stash->{result} = $file->as_object;
}

=head2 update_by_number

Endpoint for updating the metadata of a document

=head3 URI Path

C</api/v1/document/get_by_number/[ID]/update>

=cut

sub update_by_number : Chained('number_base') : PathPart('update') : Args(0) : RW {
    my ($self, $c) = @_;

    $c->assert_post;

    my %params = %{ $c->req->params };

    my $file = $c->stash->{file};

    if ($params{case_number}) {
        $c->model('Zaak')->assert_case_id($params{case_number});
        $file = $c->model('Document')->update_case_id($file, $params{case_number});
        $c->model('Queue')->emit_case_event(
            {
                case        => $file->case_id,
                event_name  => 'DocumentAddedToCase',
                description => 'Document toegevoegd via api/v1/document',
                changes => [],
            }
        );
    }

    my $filename = delete $params{name};
    # External users must abide the API itself
    delete $params{metadata}{file_name};
    if (defined $filename) {
        $params{metadata}{file_name} = $filename;
    }

    $c->model('Document')->assert_mutation($file);
    $file = $c->model('Document')->update_metadata($file, %{$params{metadata}}) if $params{metadata};

    $c->stash->{result} = $file->as_object;
}


__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

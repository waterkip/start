package Zaaksysteem::Controller::API::User::Annotations;

use Moose;

use BTTW::Tools;
use Zaaksysteem::Constants qw/LOGGING_COMPONENT_USER/;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

=head2 clear

Clear all annotations for a user. The idea is that when a user leaves
the organisation this is a tool so they can clear up their personal
notes.

=cut

sub clear : Local : ZAPI {
    my ($self, $c) = @_;

    my $params = $c->req->params;

    throw('api/user/annotations/method_not_post', 'method should be POST')
        unless $c->req->method eq 'POST';

    # extra check, the user will not get here per the default settings in Page.pm.
    # but in case of future changes this function will still be protected.
    throw('api/user/annotations/user_not_logged_in', 'user not logged in')
        unless $c->user_exists;

    # since this thing is pretty powerful and accessible through a simple call,
    # we add an extra parameter so the GUI needs to be damn sure before committing
    throw('api/user/annotations/confirm_missing', 'confirm missing')
        unless $params->{confirm};

    my $rs = $c->model('DB::FileAnnotation')->search({
        subject => 'betrokkene-medewerker-' . $c->user->uidnumber
    });

    # give the GUI an opportunity to show how many items have been cleared.
    my $count = $rs->count;

    $rs->delete;

    # We can't log which notes on which files were cleared, since that
    # would defeat the purpose of cleaning up - this is an extermination
    # process so no trace are left.
    # It is up to the user interface to make this not to easy.
    $c->model('DB::Logging')->trigger('user/annotations/clear', {
        component => LOGGING_COMPONENT_USER,
        data => {}
    });

    $c->stash->{zapi} = [{
        type    => 'user/annotations/clear',
        message => 'All annotations have been cleared',
        count   => $count
    }];
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 LOGGING_COMPONENT_USER

TODO: Fix the POD

=cut


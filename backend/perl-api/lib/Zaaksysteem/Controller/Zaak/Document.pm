package Zaaksysteem::Controller::Zaak::Document;

use Moose;

use BTTW::Tools;
use Zaaksysteem::Types qw/UUID/;
use Zaaksysteem::ZAPI::Error;

use File::Temp qw[tempfile];

with 'MooseX::Log::Log4perl';

BEGIN { extends 'Zaaksysteem::Controller' }

sub remove_file : Chained('/zaak/base') : PathPart('remove_file') : Args(2) {
    my ($self, $c, $attribute_id, $file_id) = @_;

    my $files = $c->stash->{ zaak }->files->search(
        {
            'case_documents.bibliotheek_kenmerken_id' => $attribute_id,
        },
        {join => {case_documents => 'file_id'}}
    );

    if($file_id eq 'all') {
        $files->delete_all;
    } else {
        $files->search({ id => $file_id })->delete_all;
    }

    $c->stash->{ json } = {
        success => 1,
        message => 'Removed case document(s)'
    };

    $c->detach('Zaaksysteem::View::JSON');
}

sub document_base : Chained('/zaak/base') : PathPart('document') : CaptureArgs(1) {
    my ($self, $c, $file_id) = @_;

    my $file;

    my @case_ids = $c->stash->{zaak}->id;

    my $parent_id = $c->stash->{zaak}->get_column('pid');
    if (defined $parent_id) {
        push @case_ids, $parent_id;
    }

    if ($file_id =~ m/^\d+$/) {
        $file = $c->model('DB::File')->search(
            {
                'me.case_id' => \@case_ids,
                'me.id' => $file_id,
            },
            {
                prefetch    => 'filestore',
            }
        )->first;
    } elsif (UUID->check($file_id)) {
        $file = $c->model('DB::File')->search(
            {
                'me.case_id'     => \@case_ids,
                'filestore.uuid' => $file_id
            },
            {
                join        => 'filestore',
            }
        )->first;
    } else {
        throw('request/invalid_parameter',
            'file_id parameter must be numeric or a uuid');
    }

    unless($file) {
        throw('request/invalid_parameter',
            'file_id parameter did not resolve to a file in the database');
    }

    if($file->destroyed) {
        throw('file/not_available',
            'file_id parameter resolved to a file that has been purged');
    }

    $c->stash->{ file } = $file;

}

=head2 download

Download a file in a particular filetype (PDF/ODT/DOC).

=head3 URI

    C</zaak/:zaak_id/document/:document_id/download/:format>

The download format can be: C<odt>, C<pdf>, C<doc>

Note that not all files can be downloaded in the specified format.

=cut

define_profile download => (
    optional    => {
        specific_version => 'Bool',
        inline  => 'Bool',
    }
);

sub download : Chained('document_base') : PathPart('download') {
    my ($self, $c, $format) = @_;
    my $opts = assert_profile($c->req->params)->valid;

    my $file = $c->stash->{file};
    my $case = $c->stash->{zaak};

    if (not defined $opts->{specific_version}) {
        # If caller didn't ask for a specific version, assume they
        # want the current version of the document
        $file = $file->get_last_version;
    }

    $case->trigger_logging(
        'case/document/download',
        {
            data => {
                filename => $file->filename,
                file_id => $file->id,
                inline => $opts->{inline} ? 1 : 0
            }
        }
    );

    $c->serve_file($file, $format, $opts->{inline});

    $c->detach;
}

=head2 copy

Copy a file to PDF

=head3 URI

    C</zaak/:zaak_id/document/:document_id/copy>

=cut

sub copy : Chained('document_base') : PathPart('copy') {
    my ($self, $c, $format) = @_;

    # "Copy as PDF" always works on the latest/current version; if a user
    # wants the PDF version of an older version they have to restore that
    # old version first.
    my $file = $c->stash->{ file }->get_last_version;

    $c->stash->{ zapi } = try {
        return [
            $file->save_copy(
                type => 'application/pdf',
                subject => $c->user->as_object
            )
        ];
    } catch {
        $self->log->error($_);

        return Zaaksysteem::ZAPI::Error->new_from_error($_, 'document/copy');
    };

    $c->detach($c->view('ZAPI'));
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 document_base

TODO: Fix the POD

=cut

=head2 remove_file

TODO: Fix the POD

=cut


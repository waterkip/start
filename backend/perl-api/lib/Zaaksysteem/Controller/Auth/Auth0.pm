package Zaaksysteem::Controller::Auth::Auth0;

use URI::Escape;
use Zaaksysteem::Moose;

BEGIN { extends 'Zaaksysteem::Controller' }

=head1 NAME

Zaaksysteem::Controller::Auth::Auth0;

=head1 METHODS

=head2 authorize

Start authorize process

=cut

sub authorize : Chained('/') : PathPart('auth/auth0') {
    my ($self, $c) = @_;

    my $oidc_domain = $c->config->{oidc}{domain} // '';
    if (!$oidc_domain) {
        $c->logger->warn("Invalid configuration, OIDC domain value not found, redirecting to login page.");
        $c->detach('/auth/page/login');
    }

    my $oidc_client_id = $c->config->{oidc}{client_id} // '';
    if (!$oidc_client_id) {
        $c->logger->warn("Invalid configuration, OIDC client id value not found, redirecting to login page.");
        $c->detach('/auth/page/login');
    }

    my $oidc_audience = $c->config->{oidc}{audience} // '';
    if (!$oidc_audience) {
        $c->logger->warn("Invalid configuration, OIDC audience value not found, redirecting to login page.");
        $c->detach('/auth/page/login');
    }

    my $oidc_organization_id =  $c->config->{oidc_organization_id} // '';
    if (!$oidc_organization_id) {
        $c->logger->warn("Invalid configuration, OIDC organization id value not found, redirecting to login page.");
        $c->detach('/auth/page/login');
    }

    my $response_type = 'code';
    my $scope = 'openid email';
    my $sessionid =  $c->sessionid;

    my $auth0_authorize_state = $c->model('Auth::Auth0')->create({
        date_expires => DateTime->now->add(minutes => 60),
        session_id => $sessionid,
    });

    my $redirect_uri = $c->uri_for('/api/v2/auth/auth0/authorization_code_flow/callback');

    my $oauth_authorize_url = (
         sprintf('https://%s/authorize?response_type=%s&client_id=%s&audience=%s&scope=%s&redirect_uri=%s&state=%s&organization=%s', 
         uri_escape($oidc_domain),
         uri_escape($response_type),
         uri_escape($oidc_client_id),
         uri_escape($oidc_audience),
         uri_escape($scope),
         uri_escape($redirect_uri),
         uri_escape($auth0_authorize_state),
         uri_escape($oidc_organization_id)
         ),
    );

    $c->res->redirect(
        $oauth_authorize_url 
    );
    $c->res->body('');
    $c->detach;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2024, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.

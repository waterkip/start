package Zaaksysteem::Controller::Health;

use Moose;
use namespace::autoclean;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

=head1 NAME

Zaaksysteem::Controller::Health - Simple controller used for health checks

=head1 ACTIONS

=head2 base

Base action, linked to `/health` -- returns a simple ZAPI response indicating
the system is healthy.

=cut

sub base : Path('/health') : CaptureArgs(0) {
    my ($self, $c) = @_;

    $c->stash->{zapi} = [ { "healthy" => \1 } ];

    return;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2024, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

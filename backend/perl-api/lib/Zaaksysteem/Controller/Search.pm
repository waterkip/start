package Zaaksysteem::Controller::Search;

use Moose;
use BTTW::Tools;

BEGIN { extends 'Zaaksysteem::Controller' }

=head1 NAME

Zaaksysteem::Controller::Search - Controller that loads the "object search" screen

=head1 DESCRIPTION

A small controller that processes and shows the "object search" template.

=head1 ACTIONS

=head2 base

Base anchor for the /search namespace. Puts some useful stuff on the stash.

=cut

sub base :Chained("/") :PathPart("search") :CaptureArgs(0) {
    my ($self, $c) = @_;

    $c->stash->{user_uidnumber} = $c->user->uidnumber if $c->user_exists;
    if (!$c->user->has_legacy_permission('search')) {
        throw(
            "api/unauthorized",
            "Unauthorized",
            { http_code => 403 }
        );
    }

}

=head2 search

Show the search screen, with the "case" object type selected.

=head3 URL

/search

=cut

sub search : Chained("base") :PathPart("") :Args() {
    my ($self, $c) = @_;

    $c->stash->{ template } = 'search/widgets/search-widget.tt';
    $c->stash->{ object_type } = 'case';
}

=head2 with_id

Search using a specific pre-defined query (saved search id, or one of the
predefined names).

=head3 URL

/search/some_name

=cut

sub with_id : Chained("base") : PathPart("") : Args(1) {
    my ($self, $c, $search_query_id) = @_;

    $c->stash->{search_id} = $search_query_id;
    $c->forward('search');
}
__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

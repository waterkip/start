package Zaaksysteem::Event::Model;
use Zaaksysteem::Moose;

use BTTW::Tools::RandomData qw(generate_uuid_v4);
use Zaaksysteem::Event::RMQ;
use Zaaksysteem::StatsD;

has amqp => (
    isa => 'Net::AMQP::RabbitMQ',
    is => 'ro',
    lazy => 1,
    builder => '_build_amqp'
);

has amqp_factory => (
    is       => 'ro',
    isa      => 'CodeRef',
    required => 1,
    traits   => [qw(Code)],
    handles  => { '_build_amqp' => 'execute' }
);

has channel => (
    isa => 'Int',
    is => 'ro',
    required => 1,
);

has exchange => (
    isa => 'Str',
    is => 'ro',
    required => 1,
);

has json => (
    is => 'rw',
    isa => 'JSON::XS',
    default => sub { JSON::XS->new->convert_blessed(1) }
);

sub _publish {
    my $self = shift;
    my ($routing_key, $parameters) = @_;

    $self->log->info(
        sprintf(
            "Broadcasting v2 event. routing_key=%s id=%s correlation_id=%s",
            $routing_key, $parameters->{id}, $parameters->{correlation_id},
        )
    );

    my $t0 = Zaaksysteem::StatsD->statsd->start();

    local *DateTime::TO_JSON = sub { shift->iso8601 };

    $self->amqp->publish(
        $self->channel,
        $routing_key,
        $self->json->encode($parameters),
        { exchange => $self->exchange },
        { content_type => "application/json" },
    );

    Zaaksysteem::StatsD->statsd->end('amqp_write_duration', $t0);
    Zaaksysteem::StatsD->statsd->increment('amqp_write_number');

    return;
}

sub publish_queued_events {
    my $self = shift;
    my $user_uuid = shift;
    my $events = shift;

    while(@$events) {
        my $event = shift @$events;

        if (not exists $event->{parameters}{user_uuid}) {
            $event->{parameters}{user_uuid} = $user_uuid;
        }

        $self->_publish($event->{routing_key}, $event->{parameters});
    }

    return;
}

sub publish_incoming_email_event {
    my $self = shift;
    my $schema = shift;
    my $filestore_uuid = shift;

    $self->log->info(
        sprintf(
            "Broadcasting event for '%s'",
            $filestore_uuid,
        )
    );

    my $rmq = Zaaksysteem::Event::RMQ->new(schema => $schema);
    my $event = $rmq->create_incoming_email_event($filestore_uuid);

    $self->_publish($event->{routing_key}, $event->{parameters});
}

__PACKAGE__->meta->make_immutable();

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019-2020, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

package Zaaksysteem::Object::AttributeRole::CaseDestructionBlocked;

use Moose::Role;

use DateTime;
use BTTW::Tools;

with 'Zaaksysteem::Object::AttributeRole';

=head1 NAME

Zaaksysteem::Object::AttributeRole::CaseDestructionBlocked - "Is this destruction of this case blocked" - calculated attribute

=head1 SYNOPSIS

    my $attr = Zaaksysteem::Object::Attribute->new(
        dynamic_class => 'CaseDestructionBlocked',
        parent_object => $some_obj,
        ...
    );
    print "Destruction is" . ($attr->value ? '' : " not") . " blocked";

=head1 METHODS

See L<Zaaksysteem::Object::AttributeRole> for the inherited wrapper
around C<value>.

=cut

sub _calculate_value {
    my $self = shift;

    my $parent = $self->parent_object;
    my $case;
    if ($parent->isa('Zaaksysteem::Model::DB::Zaak')) {
        $case = $parent;
    }
    else {
        $case = $parent->get_source_object();
    }

    return ($case->can_delete()) ? 0 : 1;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.


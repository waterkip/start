package Zaaksysteem::Object::Queue::Model::NatuurlijkPersoon;
use Moose::Role;

use BTTW::Tools;
use Zaaksysteem::BR::Subject;
use Zaaksysteem::BR::Subject::Constants 'REMOTE_SEARCH_MODULE_NAME_STUFNP';

with 'MooseX::Log::Log4perl';

=head1 NAME

Zaaksysteem::Object::Queue::Model::NatuurlijkPersoon - NatuurlijkPersoon queue item handler

=head1 DESCRIPTION

=head1 METHODS

=head2 disable_natuurlijk_persoon

Disable natuurlijk personen

=head2 enable_natuurlijk_persoon

Enable natuurlijk personen

=cut

sig disable_natuurlijk_persoon => 'Zaaksysteem::Backend::Object::Queue::Component';

sub disable_natuurlijk_persoon {
    my ($self, $item) = @_;

    my $schema = $item->result_source->schema;

    my $np = $schema->resultset('NatuurlijkPersoon')
        ->find($item->data->{natuurlijk_persoon_id});

    $np->disable_natuurlijk_persoon;

    return 1;
}

sig enable_natuurlijk_persoon => 'Zaaksysteem::Backend::Object::Queue::Component';

sub enable_natuurlijk_persoon {
    my ($self, $item) = @_;

    my $schema = $item->result_source->schema;

    my $np = $schema->resultset('NatuurlijkPersoon')
        ->find($item->data->{natuurlijk_persoon_id});

    try {
        $np->enable_natuurlijk_persoon;
        $np->discard_changes;
        if (my $os = $np->subscription_id) {
            $self->_import_from_gbav($schema, $np->bsn, $os->config_interface_id);
        }
    }
    catch {
        $self->log->info($_);
        $_->throw();
    };

    return 1;
}

sub _import_from_gbav {
    my ($self, $schema, $bsn, $interface) = @_;

    my $subject_bridge = Zaaksysteem::BR::Subject->new(
        schema              => $schema,
        remote_search       => REMOTE_SEARCH_MODULE_NAME_STUFNP,
        config_interface_id => $interface->id,
    );

    my $np;
    try {
        ($np) = $subject_bridge->search(
            {
                subject_type              => 'person',
                'subject.personal_number' => $bsn,
            }
        );
    }
    catch {
        $self->log->info($_);
    };

    if (!$np) {
        throw(
            "queue/enable_natuurlijk_persoon/notfound/gbav",
            "Unable to find person with BSN $bsn via GBA-V"
        );
    }

    $subject_bridge->remote_import($np);
    return 1;
}

sub fix_gm_natuurlijk_personen {
    my ($self, $item) = @_;

    my $rs = $self->build_resultset('GmNatuurlijkPersoon');
    while (my $np = $rs->next) {
        $np->update;
    }
}


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

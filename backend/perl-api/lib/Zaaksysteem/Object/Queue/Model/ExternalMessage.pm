package Zaaksysteem::Object::Queue::Model::ExternalMessage;

use Moose::Role;

use BTTW::Tools;
use Zaaksysteem::API::v1::Message::Case::PhaseTransition;

requires qw(build_resultset);

=head1 NAME

Zaaksysteem::Object::Queue::Model::ExternalMessage - External message queue item handler

=head1 DESCRIPTION

=head1 METHODS

=head2 export

=cut


sig send_external_messages => 'Zaaksysteem::Backend::Object::Queue::Component';

sub send_external_messages {
    my $self = shift;
    my $item = shift;

    my $data = $item->data;

    my $case = $self->build_resultset('Zaak')->find($data->{case_id});

    for my $message (@{ $data->{rules_result} }) {
        my ($module, $id) = split m[_], $message->{ type };

        my $where = {
            module => $module
        };

        if ($id) {
            $where->{ id } = $id;
        }

        # Be backward compatible with the old style:
        # 'buitenbeter' and 'api_<number>'. The code is rewritten to only
        # have the ID of the interface. This allows multiple interfaces of
        # the same kind to be used.
        if ($module =~ /^\d+$/ && !$id) {
            $where = { id => $module };
            $module = undef;
        }

        my $interfaces = $self->build_resultset('Interface');
        my $interface  = $interfaces->search_active($where)->first;

        unless (defined $interface) {
            $self->log->warn(sprintf(
                'Case was configured to trigger a %sprocess%s, but no interface could be found.',
                ($module ? "\"$module\""          : ''),
                ($id     ? " (interface \"$id\")" : ''),
            ));

            next;
        }

        $self->log->info(sprintf(
            'Processing case generic send_external_system_message trigger for "%s" (%d)',
            $interface->name,
            $interface->id
        ));

        try {
            if ($interface->module eq 'api' ) {
                my $logging_data = {
                    component => $module,
                    data => {
                        interface_name      => $interface->name,
                        interface_id        => $interface->id,
                    }
                };

                $case->trigger_logging( 'case/post_message' => $logging_data );

                return $interface->process_trigger(
                    'post_message',
                    {
                        object => Zaaksysteem::API::v1::Message::Case::PhaseTransition->new(
                            base_url => URI->new($data->{base_url}),
                            case_id  => $case->get_column('uuid'),
                        ),
                    }
                );
            }
            elsif ($interface->module eq 'key2burgerzakenverhuizing') {
                return $interface->process_trigger(request_verhuizing => {
                    case => $case
                });
            }

            # Interfaces that support external system messages will need to have a
            # trigger that supports this API
            return $interface->process_trigger('PostStatusUpdate', {
                case_id    => $case->id,
                kenmerken  => $case->field_values,
                message    => $message,
                base_url   => $data->{ base_url },
                statusText => $message->{ message },
                statusCode => $message->{ status },
            });
        } catch {
            $self->log->warn(sprintf(
                'Caught exception during "%s" (%d) trigger: %s',
                $interface->name,
                $interface->id,
                $_
            ));
        };
    }

    return;
}


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.

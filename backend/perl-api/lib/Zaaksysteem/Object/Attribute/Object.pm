package Zaaksysteem::Object::Attribute::Object;

use Moose::Role;

=head2 _build_human_value

On our "Object" role, the "human value" (as returned by ZAPI calls, for
instance) is identical to the internal value.

=cut

sub _build_human_value {
    my $self                        = shift;
    return $self->value;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.


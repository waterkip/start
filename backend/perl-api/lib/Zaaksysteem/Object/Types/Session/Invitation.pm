package Zaaksysteem::Object::Types::Session::Invitation;

use Moose;
use namespace::autoclean;

extends 'Zaaksysteem::Object';

=head1 NAME

Zaaksysteem::Object::Types::Session::Invitation - Session invitation data type

=head1 DESCRIPTION

=cut

use BTTW::Tools;
use JSON::XS qw[];
use MIME::Base64 qw[encode_base64];
use Moose::Util::TypeConstraints qw[role_type];
use URI;

use Zaaksysteem::Types qw[Timestamp];

=head1 ATTRIBUTES

=head2 subject

=cut

has subject => (
    is => 'rw',
    type => 'subject',
    traits => [qw[OR]],
    label => 'Subject',
    required => 1
);

=head2 date_expires

=cut

has date_expires => (
    is => 'rw',
    isa => Timestamp,
    type => 'datetime',
    label => 'Expiration timestamp',
    coerce => 1,
    traits => [qw[OA]],
    required => 0
);

=head2 object

=cut

has object => (
    is => 'rw',
    # OR-attributes usually use the 'type' field, but we want global object
    # reference support here, so we use a plain 'isa' instead.
    isa => role_type('Zaaksysteem::Object::Reference'),
    label => 'Object',
    traits => [qw[OR]],
    predicate => 'has_object'
);

=head2 action_path

=cut

has action_path => (
    is => 'rw',
    isa => 'Str',
    label => 'API namespace constraint',
    traits => [qw[OA]],
    predicate => 'has_action_path'
);

=head2 token

=cut

has token => (
    is => 'rw',
    isa => 'Str',
    label => 'Authentication token',
    traits => [qw[OA]],
    required => 1
);

=head1 METHODS

=head2 as_uri

Returns the 'zaaksysteem://' URI containing this session invitation

=cut

sig as_uri => 'URI, Int';

sub as_uri {
    my ($self, $base_uri, $case_id) = @_;

    my $json = JSON::XS->new->canonical;

    my $case_part = "case_id=$case_id";

    my $data = $json->encode({
        auth_token   => $self->token,
        base_uri     => $base_uri->host . $base_uri->path,
        version      => 2,

        # Hardcoded actions with relative paths, no reasonable way to do this
        # without implementing generic object actions, TODO for now.
        download     => join("?", 'download', $case_part),
        upload       => join("?", 'upload', $case_part),
        lock_get     => join("?", 'lock', $case_part),
        lock_acquire => join("?", 'lock/acquire', $case_part),
        lock_extend  => join("?", 'lock/extend', $case_part),
        lock_release => join("?", 'lock/release', $case_part),
    });

    return sprintf('zaaksysteem://%s', encode_base64($data, ''));
}

sig as_uri_documentwatcher_16 => 'URI';

sub as_uri_documentwatcher_16 {
    my ($self, $base_uri) = @_;

    my $json = JSON::XS->new->canonical;

    my $data = $json->encode({
        auth_token   => $self->token,
        base_uri     => $base_uri->host . $base_uri->path,
        version      => 2,

        # Hardcoded actions with relative paths, no reasonable way to do this
        # without implementing generic object actions, TODO for now.
        download     => 'download',
        upload       => 'upload',
        lock_get     => 'lock',
        lock_acquire => 'lock/acquire',
        lock_extend  => 'lock/extend',
        lock_release => 'lock/release',
    });

    return sprintf('zaaksysteem://%s', encode_base64($data, ''));
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

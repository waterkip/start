package Zaaksysteem::Object::Types::Serial;
use Moose;
use namespace::autoclean;

use Moose::Util::TypeConstraints qw[enum union];
use Zaaksysteem::Types qw(UUID);
extends 'Zaaksysteem::Object';

=head1 NAME

Zaaksysteem::Object::Types::Serial - A objective representation of a serial

=head1 DESCRIPTION

=head1 SYNOPSIS

    use Zaaksysteem::Object::Types::Serial;

    my $generated_id = generate_me_an_id();
    my $object = Zaaksysteem::Object::Types::Serial->new(
        serial       => $generated_id,
        name         => 'number',
        object_class => 'case',
    );

=head1 ATTRIBUTES

=head2 serial

The actual serial

=head2 name

The name of the serial, this is used in the JSON of an create/update call

=head2 object_class

The object_class of the serial, eg C<case>

=cut

has serial => (
    is       => 'ro',
    isa      => union(['Int', UUID]),
    required => 1,
    label   => "Serial",
    traits => [qw(OA)],
);

has name => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
    label   => "Name",
    traits => [qw(OA)],
);

has object_class => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
    label   => "Object class",
    traits => [qw(OA)],
);

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

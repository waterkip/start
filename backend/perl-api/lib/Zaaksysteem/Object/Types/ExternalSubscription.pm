package Zaaksysteem::Object::Types::ExternalSubscription;
use Moose;
use namespace::autoclean;

extends 'Zaaksysteem::Object';
with qw/Zaaksysteem::BR::Subject::Types::ExternalSubscription/;

use BTTW::Tools;
use Zaaksysteem::Types qw(UUID);

=head1 NAME

Zaaksysteem::Object::Types::ExternalSubscription - ExternalSubscription object containing references
                                                   to this object in external applications

=head1 DESCRIPTION

An object class for external object subscriptions in other systems.

=head1 ATTRIBUTES

=head2 internal_identifier

The primary key (identifier) of the C<ObjectSubscription> schema.

=cut

has internal_identifier => (
    is       => 'rw',
    isa      => 'Num',
    traits   => [qw(OA)],
    label    => 'Internal ID',
);

=head2 external_identifier

The primary key (identifier) of the other party. Think of a reference to the object in StUF, or the
reference to the object in e.g. another application.

=cut

has external_identifier => (
    is       => 'rw',
    isa      => 'Str',
    traits   => [qw(OA)],
    label    => 'External ID',
);

=head2 interface_uuid

The interface uuid for this type of external subscription. Like the UUID of the active StUF NPS interface

=cut

has interface_uuid => (
    is       => 'rw',
    isa      => UUID,
    traits   => [qw(OA)],
    label    => 'Interface UUID',
);

=head2 config_interface_uuid

The UUID of the configuration interface, this interface controls the interface which is found by the C<interface_uuid> attribute.

=cut

has config_interface_uuid => (
    is       => 'rw',
    isa      => UUID,
    traits   => [qw(OA)],
    label    => 'Configuration interface UUID',
);

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

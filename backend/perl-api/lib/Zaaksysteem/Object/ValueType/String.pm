package Zaaksysteem::Object::ValueType::String;

use Moose;
use namespace::autoclean;

with 'Zaaksysteem::Interface::ValueType';

=head1 NAME

Zaaksysteem::Object::ValueType::String - L<Zaaksysteem::Object>
L<C<string>|Zaaksysteem::Manual::API::V1::ValueTypes/string> value class.

=head1 DESCRIPTION

This value type abstracts the concept of a simple string of characters within
the object infrastucture, and is usually used for single words, identifiers,
or short descriptive sentences.

=head1 METHODS

=head2 name

Returns the static type name for this value type (C<string>).

=cut

sub name {
    return 'string';
}

=head2 equal

Implements equality testing of two values in C<string> context.

=cut

sub equal {
    my $self = shift;
    my $a = $self->as_string(shift);
    my $b = $self->as_string(shift);

    return unless defined $a && defined $b;

    return $a eq $b;
}

=head2 not_equal

Implements not-equality testing of two values in C<string> context.

=cut

sub not_equal {
    return not shift->equal(@_);
}

=head2 as_string

Returns a scalar Perl string value, if one can be dereferenced.

=cut

sub as_string {
    my $self = shift;
    my $value = shift;

    return unless $value->has_value;

    return $value->value if $value->type_name eq 'string';
    return $value->value if $value->type_name eq 'text';

    return $value->coerce('string');
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

package Zaaksysteem::Object::Query::Expression::Literal;

use Moose;

use BTTW::Tools;

with 'Zaaksysteem::Object::Query::Expression';

=head1 NAME

Zaaksysteem::Object::Query::Expression::Literal - Abstracts literal expression
values (strings, timestamps, etc)

=head1 DESCRIPTION

=head1 ATTRIBUTES

=head2 type

Type of the value.

=cut

has type => (
    is => 'rw',
    isa => 'Str', # TODO: enum
    required => 1
);

=head2 value

The value of the literal.

=cut

has value => (
    is => 'rw',
    isa => 'Any',
    required => 1
);

=head1 METHODS

=head2 stringify

Implements logic required by
L<Zaaksysteem::Object::Query::Expression/stringify>.

    # "text(abc)"
    qb_lit('text', 'abc')->stringify

    # "object(person(unsynched))"
    qb_lit('object', Zaaksysteem::Object::Person->new)->stringify

    # "object(case(...fb3599))"
    qb_lit('object', $my_case_instance)->stringify

=cut

sub stringify {
    my $self = shift;
    my $str = $self->value;

    if ($self->type eq 'object') {
        $str = $str->_as_string;
    }

    return sprintf('%s(%s)', $self->type, $str);
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

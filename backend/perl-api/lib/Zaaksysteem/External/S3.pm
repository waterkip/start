package Zaaksysteem::External::S3;
use strict;
use warnings;
use utf8;
use v5.30;

use BTTW::Tools qw(dump_terse);
use BTTW::Tools::UA qw(new_user_agent);
use Log::Log4perl;
use Net::Amazon::S3;
use Net::Amazon::S3::Authorization::Basic;
use Net::Amazon::S3::Authorization::IAM;
use Net::Amazon::S3::Operation::Object::Fetch::Request;
use Net::Amazon::S3::Signature::V4;
use Net::Amazon::S3::Vendor::Generic;
use Zaaksysteem::Filestore::Engine::S3::AssumeRoleWebIdentity;

my $log;

sub build_s3 {
    my ($class, %args) = @_;

    $log //= Log::Log4perl->get_logger(__PACKAGE__);

    my $host = $args{host};
    my $secure = $args{secure} // 1;

    # Remove default ports from hostnames, as they break S3 "pre-signed" URLs
    $args{secure} || $host =~ s/:80$//;
    $args{secure} && $host =~ s/:443$//;

    my %s3_args = (
        # Show the error from Amazon in the logs, if applicable
        error_handler_class => 'Net::Amazon::S3::Error::Handler::Confess',

        vendor => Net::Amazon::S3::Vendor::Generic->new (
            host => $host,
            use_https => $secure,
            use_virtual_host => $args{use_virtual_host},
            authorization_method => 'Net::Amazon::S3::Signature::V4',
            default_region => $args{region} // 'eu-central-1',
        ),
    );

    if ($args{use_iam_role}) {
        if ($ENV{AWS_WEB_IDENTITY_TOKEN_FILE}) {
            $s3_args{authorization_context} = Zaaksysteem::Filestore::Engine::S3::AssumeRoleWebIdentity->new(
                sts_region => $args{sts_region},
            );
        } else {
            $s3_args{authorization_context} = Net::Amazon::S3::Authorization::IAM->new();
        }
    } else {
        $s3_args{authorization_context} = Net::Amazon::S3::Authorization::Basic->new(
            aws_access_key_id     => $args{access_key},
            aws_secret_access_key => $args{secret_key},
        );
    }

    $log->trace("Configuring S3 with values: " . dump_terse(\%s3_args)) if $log->is_trace;

    my $s3 = Net::Amazon::S3->new(%s3_args);
    $s3->ua(new_user_agent());

    return $s3;
}

=head2 get_download_url

A reimplementation of Net::Amazon::S3::Bucket::query_string_authentication_uri
.. except it has a way to specify the extra response headers (as
"response-content-type", etc.).

=head3 Arguments

=over

=item $s3

L<Net::Amazon::S3> instance to use.

=item $bucket

L<Net::Amazon::S3::Bucket> to use.

=item $filename

Filename / key in the bucket to make a presigned URL for.

=item $headers

Hashref containing any (extra) query parameters.

Can be used to specify, for instance, the content-type or content-disposition
as follows:

    {
        "response-content-type": "text/plain",
        "response-content-disposition": 'attachment; filename="foo.txt"',
    }

=back

=cut

sub get_download_url {
    my ($cls, $s3, $bucket, $filename, $query_params) = @_;

    my %params = %{ $query_params // {} };

    if (not exists $params{"response-cache-control"}) {
        $params{"response-cache-control"} = "private, max-age=180";
    }

    if (not exists $params{"response-content-type"}) {
        $params{"response-content-type"} = "application/octet-stream";
    }
    
    my $expires_at = time + 180;

    my $req = Net::Amazon::S3::Operation::Object::Fetch::Request->new(
        s3 => $s3,
        bucket => $bucket,
        key => $filename,
        method => 'GET',
    );

    return $req->query_string_authentication_uri(
        $expires_at,
        \%params,
    );
}

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

aaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

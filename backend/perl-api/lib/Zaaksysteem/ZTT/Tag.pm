package Zaaksysteem::ZTT::Tag;

use Moose;

use BTTW::Tools;

=head1 NAME

Zaaksysteem::ZTT::Tag - Package for wrapping data regarding a tag found in
templates

=head1 ATTRIBUTES

=head2 expression

Holds a simple (scalar), or complex (hashref/arrayref) expression that must
be evaluated and will be used as the value for the selection this tag
replaces.

=cut

has expression => (
    is => 'ro',
    isa => 'Defined',
    required => 1
);

=head2 formatter

A formatter is a hashref value that references some pre-defined 'formatter',
or filter that manipulates the value that will be injected in the selection.

=cut

has formatter => (
    is => 'ro',
    isa => 'Maybe[HashRef]',
    predicate => 'has_formatter'
);

=head1 METHODS

=head2 name

This method provides a read-only backwards-compatibility layer for the
previous tag implementation, which only used attribute names instead of
expressions.

Will return the name of an attribute that can be used to retrieve the
to-be-injected value associated with this tag.

=cut

sub name {
    my $self = shift;

    # Truthy-ness testing, empty strings couldn't possibly be attribute
    # references.
    return unless $self->expression;

    my $ref = ref $self->expression;

    if (defined $ref && not $ref eq 'SCALAR') {
        # If we've got a reference here (expression) we have to give up, no
        # way we can know for sure evaluating it will result in a valid
        # attribute name, so for now we'll refuse.

        throw(
            'ztt/tag/invalid_expression_type',
            'Name of complex expression was requested.'
        );
    }

    # 'Do what I mean' semantics, if it's a reference it definitively refers
    # to something that was parsed as an attribute name. If plain scalar,
    # assume it's also an attribute name.
    return $ref ? ${ $self->expression } : $self->expression;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

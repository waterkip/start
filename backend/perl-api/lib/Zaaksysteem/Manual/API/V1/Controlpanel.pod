=head1 NAME

Zaaksysteem::Manual::API::V1::Controlpanel - Controlpanel retrieval and creation

=head1 Description

This API-document describes the usage of our JSON Controlpanel API. Via the Controlpanel API it is possible
to retrieve, create and edit controlpanels per customer.

=head2 API

This document is based on the V1 API of Zaaksysteem, more information about the default format
of this API can be found in L<Zaaksysteem::Manual::API::V1>. Please make sure you read this
document before continuing.

=head2 URL

The base URL for this API is:

    /api/v1/controlpanel

Make sure you use the HTTP Method C<GET> for retrieving, and C<POST> for change requests.

=head1 Retrieve data

=head2 get

   /api/v1/controlpanel/d7c73f04-5fcc-4c03-b089-b5fc760a08d5

Retrieving an object from our database is as simple as calling the URL C</api/v1/controlpanel/[UUID]>. You will get
the response in the C<result> parameter. It will only contain one single result, and as always, the C<type>
will tell you what kind of object you received. The C<instance> property will contain the contents of this
object.

B<Example call>

  curl --anyauth -H "Content-Type: application/json" --digest -u "username:password" https://localhost/api/v1/controlpanel/d7c73f04-5fcc-4c03-b089-b5fc760a08d5

B<Request JSON>

An empty request body suffices

B<Response JSON>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-f00d74-d239bf",
   "development" : false,
   "result" : {
      "instance" : {
         "customer_type" : "government",
         "id" : "a7871819-7246-4223-8c80-6b73aba5922d",
         "owner" : "betrokkene-bedrijf-348",
         "shortname" : "betrokkene-bedrijf-348",
         "template" : "mintlab"
      },
      "reference" : "a7871819-7246-4223-8c80-6b73aba5922d",
      "type" : "controlpanel"
   },
   "status_code" : 200
}

=end javascript

=head2 list

   /api/v1/controlpanel

Retrieving multiple objects from our database is as simple as calling the URL C</api/v1/controlpanel> without arguments. The
C<result> property will contain an object of type B<set>, which allows us to page the data.

B<Params>

=over 4

=item zql

You can send a ZQL query to the list command, which will restrict the call

   /api/v1/controlpanel?zql=select {} from controlpanel where owner="betrokkene-bedrijf-2323"

=back

B<Example call>

 curl --anyauth -H "Content-Type: application/json" --digest -u "username:password" https://localhost/api/v1/controlpanel

B<Request JSON>

An empty request body suffices

B<Response JSON>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-f00d74-42d4e6",
   "development" : false,
   "result" : {
      "instance" : {
         "pager" : {
            "next" : null,
            "page" : 1,
            "pages" : 1,
            "prev" : null,
            "rows" : 2,
            "total_rows" : 2
         },
         "rows" : [
            {
               "instance" : {
                  "customer_type" : "government",
                  "id" : "f0409f31-11e9-4036-93f6-0787be397275",
                  "owner" : "betrokkene-bedrijf-354",
                  "shortname" : "betrokkene-bedrijf-354",
                  "template" : "mintlab"
               },
               "reference" : "f0409f31-11e9-4036-93f6-0787be397275",
               "type" : "controlpanel"
            },
            {
               "instance" : {
                  "customer_type" : "government",
                  "id" : "c1897a1e-5ad1-4a5b-bb91-b015313ec469",
                  "owner" : "betrokkene-bedrijf-353",
                  "shortname" : "betrokkene-bedrijf-353",
                  "template" : "mintlab"
               },
               "reference" : "c1897a1e-5ad1-4a5b-bb91-b015313ec469",
               "type" : "controlpanel"
            }
         ]
      },
      "reference" : null,
      "type" : "set"
   },
   "status_code" : 200
}

=end javascript

=head1 Mutate data

Mutations via our API will have to be send via the HTTP C<POST> method. The inputdata for these mutations
are one single JSON object containing the parameters. When no input is needed, make sure you send an empty
object (C< {} >)

=head2 create

   /api/v1/controlpanel/create

It is possible to create a controlpanel in zaaksysteem.nl.

The input (request) data for this call is a JSON object containing the following properties

B<Properties>

=over 4

=item owner [required]

B<TYPE>: Identifier of betrokkene (betrokkene-bedrijf-ID)

The owner of this controlpanel, the customer where these panels belong to.

=item customer_type [optional]

B<TYPE>: String (enum: "gov","com")
B<Default>: gov

The customer_type for this controlpanel, defines in which cloud this customer will fall

=item template [optional]

B<TYPE>: String
B<Default>: mintlab

The template to use for the layout of all controlpanel instances

=back

B<Example call>

 curl --anyauth -H "Content-Type: application/json" --data @file_with_request_json --digest -u "username:password" https://localhost/api/v1/controlpanel/create

B<Request JSON JSON>

=begin javascript

{
   "customer_type" : "government",
   "owner" : "betrokkene-bedrijf-348"
}

=end javascript

B<Response JSON JSON>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-f00d74-d239bf",
   "development" : false,
   "result" : {
      "instance" : {
         "customer_type" : "government",
         "id" : "a7871819-7246-4223-8c80-6b73aba5922d",
         "owner" : "betrokkene-bedrijf-348",
         "shortname" : "betrokkene-bedrijf-348",
         "template" : "mintlab"
      },
      "reference" : "a7871819-7246-4223-8c80-6b73aba5922d",
      "type" : "controlpanel"
   },
   "status_code" : 200
}

=end javascript

=head2 update

   /api/v1/controlpanel/UUID/update

It is possible to update a controlpanel in zaaksysteem.nl.

The input (request) data for this call is a JSON object containing the following properties

B<Properties>

=over 4

=item template [optional]

B<TYPE>: String
B<Default>: mintlab

The template to use for the layout of all controlpanel instances

=back

B<Example call>

 curl --anyauth -H "Content-Type: application/json" --data @file_with_request_json --digest -u "username:password" https://localhost/api/v1/controlpanel/c25d2daf-0e00-4fa5-8bc7-b61fff072234/update

B<Request JSON JSON>

=begin javascript

{
   "customer_type" : "government",
   "owner" : "betrokkene-bedrijf-357"
}

=end javascript

B<Response JSON JSON>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-f00d74-e1ab19",
   "development" : false,
   "result" : {
      "instance" : {
         "customer_type" : "government",
         "id" : "329bd292-d8d5-4c06-803b-02caa4a17f0f",
         "owner" : "betrokkene-bedrijf-357",
         "shortname" : "betrokkene-bedrijf-357",
         "template" : "mintlab"
      },
      "reference" : "329bd292-d8d5-4c06-803b-02caa4a17f0f",
      "type" : "controlpanel"
   },
   "status_code" : 200
}

=end javascript

=head1 Objects

=head2 Controlpanel

Most of the calls in this document return an instance of type C<controlpanel>. Below we provide more information about
the contents of this object.

B<Properties>

=over 4

=item id

B<TYPE>: UUID

The unique identifier of this controlpanel. You can use this identifier to request more information about the
related objects of this object, like the download API call.

=item customer_type

B<TYPE>: Str (enum: "gov" or "com")

The customer_type for this controlpanel, defines in which cloud this customer will fall

=item owner

B<TYPE>: Identifier of betrokkene (betrokkene-bedrijf-ID)

The owner of this controlpanel, the customer where these panels belong to.

=item template

B<TYPE>: String
B<Default>: mintlab

The custom CSS template this customer uses for zaaksysteem.

=item available_templates

B<TYPE>: Array

A list of possible templates to choose from

=item domain

B<TYPE>: FQDN

The domainname where all "omgevingen" (instances) are created. Like 'zaaksysteem.nl' or 'zaaksysteem.net'

=back

=head1 Support

The data in this document is supported by the following test. Please make sure you use the API as described
in this test. Any use of this API outside the scope of this test is B<unsupported>

L<TestFor::Catalyst::API::V1::Controlpanel>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

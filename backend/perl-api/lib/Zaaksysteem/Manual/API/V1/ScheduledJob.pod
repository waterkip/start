=head1 NAME

Zaaksysteem::Manual::API::V1::ScheduledJobs - ScheduledJobs retreival via ZQL

=head1 Description

Get scheduled jobs objects from Zaaksysteem

=head2 API

This document is based on the V1 API of Zaaksysteem, more information about the default format
of this API can be found in L<Zaaksysteem::Manual::API::V1>. Please make sure you read this
document before continuing.

=head2 URL

The base URL for this API is:

    /api/v1/scheduled_job

Make sure you use the HTTP Method C<GET> for retrieving.
You need to be logged in to Zaaksysteem to speak to this API.

=head1 Retrieve data

=head2 list

   /api/v1/scheduled_job

This will list all the scheduled jobs in the system

B<Response JSON>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab_sprint-bcc00d-ba6442",
   "development" : false,
   "result" : {
      "instance" : {
         "pager" : {
            "next" : null,
            "page" : 1,
            "pages" : 1,
            "prev" : null,
            "rows" : 3,
            "total_rows" : 3
         },
         "rows" : [
            {
               "instance" : {
                  "data" : {
                     "label" : "Synchronisatie taak voor koppeling \"SDU Connect PDC\" (collectie 2195)"
                  },
                  "date_created" : "2015-12-29T14:19:40Z",
                  "date_modified" : "2016-02-03T14:27:37Z",
                  "id" : "15a30d11-35b2-4998-a1bd-8c1d2086a0d7",
                  "interface_id" : 101,
                  "interval_period" : "days",
                  "interval_value" : 1,
                  "job" : "SDUConnectSync",
                  "next_run" : "2016-02-05T14:19:40Z",
                  "runs_left" : null
               },
               "reference" : "15a30d11-35b2-4998-a1bd-8c1d2086a0d7",
               "type" : "scheduled_job"
            },
            {
               "instance" : {
                  "data" : {
                     "label" : "Synchronisatie taak voor koppeling \"SDU Connect VAC\" (collectie 123)"
                  },
                  "date_created" : "2015-12-28T13:29:41Z",
                  "date_modified" : "2016-02-03T14:03:39Z",
                  "id" : "0b0f7d93-1961-4b85-aed6-378934d8743d",
                  "interface_id" : 104,
                  "interval_period" : "days",
                  "interval_value" : 1,
                  "job" : "SDUConnectSync",
                  "next_run" : "2016-02-05T13:29:41Z",
                  "runs_left" : null
               },
               "reference" : "0b0f7d93-1961-4b85-aed6-378934d8743d",
               "type" : "scheduled_job"
            },
            {
               "instance" : {
                  "id" : "3b08991f-c475-4dd0-87cb-4436224dbfee",
                  "interval_period" : "years",
                  "interval_value" : 2,
                  "job" : "CreateCase",
                  "next_run" : "2016-12-02T17:00:00Z",
                  "runs_left" : 2
               },
               "reference" : "3b08991f-c475-4dd0-87cb-4436224dbfee",
               "type" : "scheduled_job"
            }
         ]
      },
      "reference" : null,
      "type" : "set"
   },
   "status_code" : 200
}

=end javascript

=head2 get

    /api/v1/scheduled_job/15a30d11-35b2-4998-a1bd-8c1d2086a0d7

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab_sprint-bcc00d-279b60",
   "development" : false,
   "result" : {
      "instance" : {
         "data" : {
            "label" : "Synchronisatie taak voor koppeling \"SDU Connect PDC\" (collectie 2195)"
         },
         "date_created" : "2015-12-29T14:19:40Z",
         "date_modified" : "2016-02-03T14:27:37Z",
         "id" : "15a30d11-35b2-4998-a1bd-8c1d2086a0d7",
         "interface_id" : 101,
         "interval_period" : "days",
         "interval_value" : 1,
         "job" : "SDUConnectSync",
         "next_run" : "2016-02-05T14:19:40Z",
         "runs_left" : null
      },
      "reference" : "15a30d11-35b2-4998-a1bd-8c1d2086a0d7",
      "type" : "scheduled_job"
   },
   "status_code" : 200
}

=end javascript

=head1 Modify scheduled jobs

You can create, update and delete scheduled jobs via the following calls. All modification requests are done by a POST.

=head2 create

    /api/v1/scheduled_job/create

The input (request) data for this call is a JSON object containing the following properties

B<Properties>

=over

=item job [required]

C<CreateCase> and others

=item next_run [required]

A DateTime string in UTC. We consider datetime strings to be in the current local
time in the floating timezone, this will then be stored as UTC in our database.

=item case or case_uuid [required]

You can either supply just a UUID of a case by using C<case_uuid>, or you can use
a case obect by using C<case>.

=item casetype or casetype_uuid [required]

You can either supply just a UUID of a casetype by using C<casetype_uuid>, or you can use
a casetype obect by using C<casetype>.

=item interval_value [optional]

An integer value

=item interval_period [optional]

A interval period, the current supported options are:
C<days>, C<weeks>, C<months>, C<years>.

=item runs_left [optional]

The amount of runs left.

=back

=head3 Example create JSON

=begin javascript

    // A minimalistic example, with all the required attributes

    {
        "job" : "CreateCase",
        "next_run" : "2012-04-23T18:25:43Z",
        "case" : { "reference" : "91015549-aa5c-4d4d-bcbe-51ab05804eef", type => "case" },
        "casetype" : { "reference" : "765e4d0d-8b7f-4ef8-9739-f472d43edc23", type => "casetype" },
    }

    {
        "job": "CreateCase",
        "next_run" : "2012-04-23T18:25:43Z",
        "case_uuid" : "91015549-aa5c-4d4d-bcbe-51ab05804eef"
        "casetype_uuid" : "765e4d0d-8b7f-4ef8-9739-f472d43edc23",
        "copy_relations": true,
        "interval_value" : 1,
        "interval_period" : "weeks",
        "runs_left": 2
    }

=end javascript

=cut

=head2 update

    /api/v1/scheduled_job/[UUID]/update

The update call only supports updating the following attributes of a scheduled job:

=over

=item next_run

=item interval_value

=item interval_period

=item runs_left

=back

=head3 Example update JSON

=begin javascript

    {
        "job" : "CreateCase",
        "next_run" : "2012-04-23T18:25:43Z",
    }

=end javascript

=head2 delete

    /api/v1/scheduled_job/[UUID]/delete

The delete call does not require any JSON.

If you call this you will get a list of all existing scheduled jobs.

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

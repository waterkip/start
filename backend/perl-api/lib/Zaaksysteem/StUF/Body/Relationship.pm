package Zaaksysteem::StUF::Body::Relationship;

use Moose;

use BTTW::Tools;

use Zaaksysteem::StUF::Body::PRS;
use Zaaksysteem::StUF::Body::ADR;
use Zaaksysteem::StUF::Body::Adres0310;
use Zaaksysteem::StUF::Body::WoonplaatsIngelegen;

has 'name'              => (
    is      => 'rw',
);

has '_value'            => (
    is      => 'rw',
);

has 'value'             => (
    is      => 'rw',
);

has 'definition'        => (
    is      => 'rw',
);

has 'related'           => (
    is      => 'rw',
);

has 'is_active'         => (
    is      => 'rw',
    lazy    => 1,
    default => 0,
);

has 'is_description'    => (
    is      => 'rw',
);

has 'no_value'          => (
    is => 'rw',
);

sub BUILD {
    my $self        = shift;

    my $values      = $self->_value;
    my $definition  = $self->definition;

    my $related_class = 'Zaaksysteem::StUF::Body::' . $definition->{related};

    my @relations;
    if (UNIVERSAL::isa($values, 'HASH')) {
        push(@relations, $values);
    } elsif (UNIVERSAL::isa($values, 'ARRAY')) {
        push(@relations, @{ $values });
    }

    $self->value([]);
    # Not all new objects will have values given to them upon building. This block
    # makes sure we generate empty fields for is_description.
    if (!@relations && $self->is_description) {
        my $object = $related_class->new();

        $object->is_description(1);
        $object->TO_STUF;
        push(
            @{ $self->value },
            $object
        )
    }
    else {
        for my $relation (@relations) {
            my $value;

            if ($definition->{local}) {
                $value          = $relation;
            } elsif ($definition->{via}) {
                $value       = $relation->{ $definition->{via} };
            } else {
                $value       = $relation->{ $definition->{related} };
            }

            next unless $value;

            my $object      = $related_class->new(
                _body_params    => $value,
            );

            $object->_body_params($value);
            $object->_load_body_params;
            $object->is_active($self->_is_active_relationship($relation));

            $object->datumSluiting(
                Zaaksysteem::StUF::Body::Field->new(
                        _value  => $relation->{'datumSluiting'}
                    )->value
            ) if $relation->{'datumSluiting'};

            push(
                @{ $self->value },
                $object
            );
        }
    }

    $self->related($definition->{related});

    #warn(Data::Dumper::Dumper($self));
}

sub _is_active_relationship {
    my $self            = shift;
    my $related         = shift;

    ### Check huwelijk
    if (
        exists ($related->{datumSluiting})
    ) {
        return $self->_is_active(
            $related->{datumSluiting},
            $related->{datumOntbinding}
        );
    }

    if (
        exists ($related->{tijdvakRelatie})
    ) {
        return $self->_is_active(
            $related    ->{tijdvakRelatie}
                        ->{begindatumRelatie},
            $related    ->{tijdvakRelatie}
                        ->{einddatumRelatie}
        );
    }

    return 1;
}

sub _is_active {
    my $self                = shift;
    my ($newdate, $olddate) = @_;


    my $startdate   = Zaaksysteem::StUF::Body::Field->new(
        _value  => $newdate
    )->value;
    my $enddate     = Zaaksysteem::StUF::Body::Field->new(
        _value  => $olddate
    )->value;

    ### Unknown entries are active.
    if (!$startdate && !$enddate) {
        return 1;
    }

    my $currentdate = DateTime->now->strftime('%Y%m%d');

    if ($startdate && $startdate =~ /^\d+$/ && $startdate <= $currentdate) {
        if (!$enddate || $enddate !~ /^\d+$/ || $enddate > $currentdate) {
            return 1;
        }
    }

    return 0;
}

# For XML::Compile
sub TO_STUF {
    my $self            = shift;

    return {
        $self->definition->{related} => $self->value->TO_STUF
    };
}

sub as_params {
    my $self            = shift;
    my $rv              = [];

    for my $object (@{ $self->value }) {
        push(
            @{ $rv },
            $object->as_params
        );
    }

    return $rv;
    # return {
    #     is_active       => $self->is_active,
    #     %{ $self->value->as_params }
    # }
}

1;


__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 BUILD

TODO: Fix the POD

=cut

=head2 TO_STUF

TODO: Fix the POD

=cut

=head2 as_params

TODO: Fix the POD

=cut


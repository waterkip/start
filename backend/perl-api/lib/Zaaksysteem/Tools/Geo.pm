package Zaaksysteem::Tools::Geo;
use warnings;
use strict;
use autodie;

use Exporter qw(import);

our @EXPORT_OK = qw(
    point_to_geo_json
);

=head1 NAME

Zaaksysteem::Tools::Geo - Play with Geo

=head1 DESCRIPTION

Namespace for dealing with Geo like data so we keep it DRY.

=head1 METHODS


=head2 point_to_geo_json

Encode a point C<(0,0)> to something GeoJSON things understand

=cut

sub point_to_geo_json {
    my $point = shift;
    return unless $point;
    $point =~ s/[\(\)]//g;

    my @lat_long = map { $_+0 } split(',', $point);

    return {
        type     => 'Feature',
        geometry => { type => 'Point', coordinates => [reverse(@lat_long)] }
    };
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.

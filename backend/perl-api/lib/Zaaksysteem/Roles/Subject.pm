package Zaaksysteem::Roles::Subject;
use Moose::Role;

=head1 NAME

Zaaksysteem::Roles::Subject - A role for subjects

=head1 DESCRIPTION

A role that implements 


=head1 SYNOPSIS

    with qw(Zaaksysteem::Roles::Subject);
    # requires:
    # sub bid()
    # sub get_cases()

    $self->involved_with_cases();
    $self->has_cases();
    $self->has_concept_cases();

=cut

requires qw(
    get_cases
    bid
);

=head2 involved_with_cases

Returns true or false if is linked to cases (both normal and concept cases).

=cut

sub involved_with_cases {
    my $self = shift;

    return 1 if $self->has_cases;
    return 1 if $self->has_concept_cases;
    return 0;
}

=head2 has_cases

Returns true or false if is linked to cases

=cut

sub has_cases {
    my $self = shift;

    my $cases = $self->get_cases(all_cases => 1, rows => 1, page => 1);
    return $cases->first ? 1 : 0;
}

=head2 has_concept_cases

Returns true or false if is linked to concept cases

=cut


sub has_concept_cases {
    my $self = shift;
    my $cases = $self->result_source->schema->resultset('ZaakOnafgerond')
        ->search_rs({ betrokkene => $self->bid });
    return $cases->first ? 1 : 0;
}


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.

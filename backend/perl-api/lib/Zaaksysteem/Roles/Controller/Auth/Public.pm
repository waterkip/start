package Zaaksysteem::Roles::Controller::Auth::Public;
use Moose::Role;
use namespace::autoclean;

=head1 NAME

Zaaksysteem::Roles::Controller::Auth::Public - A public auth controller role

=head1 DESCRIPTION

This modules tries to solve the problem of having multiple code paths in /pip
and /form that fetch the betrokkene from Zaaksysteem based on the
authentication module in use.

=head1 SYNOPSIS

    package Foo;
    use Moose;

    with qw(
        Zaaksysteem::Roles::Controller::Auth::Public
        MooseX::Log::Log4perl
    )

    my $betrokkene = $self->get_betrokkene_from_authentication_method($c,
        auth_module => $module,
        id          => $id,
        type        => $type,
        saml_data   => $saml_data,
    );
    if ($betrokkene) {
        print "betrokkene found";
    }
    else {
        print "betrokkene not found";
    }

=cut

use List::Util qw(any);
use BTTW::Tools;

requires qw(log);

=head1 METHODS

=head2 get_betrokkene_from_authentication_method

Get the betrokkene from any given authentication module in use by ZS. Returns
the old school betrokkene object or undef.

    my $betrokkene = $self->get_betrokkene_from_authentication_method(
        $c,
        auth_module => 'twofactor',
        id          => 'someid',
        type        => 'niet_natuurlijk_persoon',
        saml_data   => { data => 'here' },
    );

=cut

define_profile get_betrokkene_from_authentication_method => (
    required => {
        auth_module => 'Str',
        type        => 'Str',
    },
    optional        => {
        id          => 'Str',
        saml_data   => 'HashRef',
    },
);

sub _get_kvk_vestigingsnummer_override {
    my ($self, $c) = @_;


    my $override = $c->req->params->{'vestigingsnummer_override'};
    my $custom   = $c->req->params->{'vestigingsnummer_override_custom'};

    if (defined $custom && length($custom) && $custom =~ /^[0-9]+$/) {
        return __set_vestigingsnr_override($c, $custom);
    }
    elsif (defined $override && length($override)) {
        return __set_vestigingsnr_override($c, $override);
    }
    elsif (defined $custom) {
        $c->stash->{vestigingsnummer_invalid} = 1;
        return;
    }
    elsif ($c->session->{_kvk}{vestigingsnummer}) {
        return $c->session->{_kvk}{vestigingsnummer};
    }
    return;
}

sub __set_vestigingsnr_override {
    my ($c, $number) = @_;
    $number = int($number);
    $c->log->info("Setting override to $number");
    $c->session->{_kvk}{vestigingsnummer} = $number;
    $c->session->{vestigingsnummer_override_used} = 1;
    return $number;
}

sub get_betrokkene_from_authentication_method {
    my ($self, $c, %args) = @_;
    my $args = assert_profile(\%args)->valid;

    if ($c->session->{pip} && $c->session->{pip}{ztc_aanvrager}) {
        $self->log->debug("Skipping lookup, we already have a logged in user");
        return $c->model('Betrokkene')->get_by_string(
            $c->session->{pip}{ztc_aanvrager}
        );
    }

    my $verified       = $args->{auth_module};
    my $aanvrager_id   = $args->{id};
    my $aanvrager_type = $args->{type};
    my $saml_data      = $args->{saml_data};

    $self->log->debug(
        sprintf(
            "Requestor type %s was verified via %s with ID %s",
            $aanvrager_type, $verified, $aanvrager_id
        )
    );

    _assert_requestor_type_and_verification_tool($verified, $aanvrager_type);

    if ($verified eq 'preset_client') {
        # /form specific knowledge
        if ($aanvrager_type eq 'unknown') {
            return $c->model('Betrokkene')
                ->get({}, $c->session->{_zaak_create}{aanvrager});
        }
        # PIP
        return $c->model('Betrokkene')
            ->get({}, join('-', 'betrokkene', $aanvrager_type, $aanvrager_id));
    }

    my %searchopts;
    if ($aanvrager_type eq 'natuurlijk_persoon') {
        if ($verified eq 'eidas') {
            $c->stash->{eidas} = 1;
            # This is the "acting subject" - could be an authorized other person
            my $saml_id = $saml_data->{acting_subject_id};

            # This is a unique (per interface) ID of the person we're interested in.
            my $name_id = $saml_data->{nameid};

            $c->stash->{prefill} = {
                first_name    => $saml_data->{firstname},
                surname       => $saml_data->{surname},
                date_of_birth => $saml_data->{date_of_birth},
            };

            my $user_entity = $c->model('DB::UserEntity')->search(
                {
                    source_interface_id => $c->session->{_saml_state}{idp_id},
                    source_identifier   => $name_id,
                }
            )->first;

            if ($user_entity) {
                $searchopts{uuid} = $user_entity->subject_id->uuid;
            }
            else {
                # We don't have someone to lookum
                return;
            }
        }
        elsif ($verified eq 'twofactor') {
            $searchopts{uuid} = $c->session->{_twofactor}{authenticated_id};
        }
        else {
            $searchopts{burgerservicenummer} = $aanvrager_id;
        }
        my $brs = $c->model('Betrokkene')->search(
            {
                type   => $aanvrager_type,
                intern => 0,
            },
            \%searchopts
        );
        if ($brs) {
            my $person = $brs->next;

            return if !defined $person;
            my $np = $person->current->gm_np;

            if ($np->authenticated && !$np->active) {
                my $model = $c->model('BR::Subject', { remote_search => 'stufconfig' });
                my @subject;
                try {
                    @subject = $model->search(
                        {
                            subject_type => 'person',
                            'subject.personal_number' => $np->burgerservicenummer,
                        }
                    );
                    if (@subject == 1) {
                        $model->remote_import($subject[0]);
                        my $brs = $c->model('Betrokkene')->search(
                            {
                                type   => $aanvrager_type,
                                intern => 0,
                            },
                            \%searchopts
                        );
                        $person = $brs->next;
                    } else {
                        $self->log->info(
                            "Unable to update person, broker returned != 1 results or failure"
                        );
                    }
                }
                catch {
                    $self->log->info("Unable to query stuf broker: $_");
                };

                $np->discard_changes();
                return unless $np->active;
            }

            return $person;
        }
        return;
    }

    if ($aanvrager_type eq 'niet_natuurlijk_persoon') {
        my $override = $self->_get_kvk_vestigingsnummer_override($c);

        if ($verified eq 'eherkenning') {
            substr($c->session->{_saml}{uid}, 8) = sprintf("%012d", int($override)) if $override;
            my ($kvk, $ves) = _kvk_en_vestigingsnummer($c->session->{_saml}{uid});
            $searchopts{dossiernummer} = $kvk;
            $searchopts{vestigingsnummer} = $ves if $ves;
        }
        else {
            substr($aanvrager_id, 8) = sprintf("%012d", int($override)) if $override;
            my ($kvk, $ves) = _kvk_en_vestigingsnummer($aanvrager_id);
            $searchopts{dossiernummer} = $kvk;
            $searchopts{vestigingsnummer} = $ves if $ves;
        }
        $c->stash->{paging_rows} = 100;
        my $betrokkene = $self->_search_company($c, \%searchopts);
        return $betrokkene if $betrokkene;
        return;
    }
}

=head2 _search_company

Search the company via the L<Zaaksysteem::Model::RemoteSubject> and saves the
information in the database when needed. Deals with the picker logic for when
the main owner of a multi-location company logs in.

=cut

sub _search_company {
    my ($self, $c, $searchopts) = @_;

    my %betrokkene_opts = (
        type   => 'bedrijf',
        intern => 0,
    );

    my $copy;

    my $remote_subject_model = $c->model('RemoteSubject');
    if ($remote_subject_model->has_eherkenning) {
        my $objects;
        try {
            $objects = $remote_subject_model->get_company_from_remote(
                kvknummer        => $searchopts->{dossiernummer},
                vestigingsnummer => $searchopts->{vestigingsnummer}
            );
        }
        catch {
            if (   blessed($_)
                && $_->can('type')
                && $_->type eq 'remote_subject/company_not_found')
            {
                $c->log_detach(
                    error => "$_",
                    human_readable =>
                        "Uw organisatie kon niet gevonden worden"
                );
            }
            else {
                $c->log_detach(
                    error => "$_",
                    human_readable =>
                        "Communicatiefout bij ophalen bedrijfsinformatie"
                );
            }
        };

        $c->stash->{openkvk_objects} = $objects;
        $self->log->trace(dump_terse($objects)) if $self->log->is_trace;

        if (@$objects > 1) {
            $self->log->trace("Sending to picker");

            if ($c->session->{pip} || $c->session->{pip_login}) {
                my $betrokkene_resultset = $c->model('Betrokkene')
                    ->search(\%betrokkene_opts, $searchopts);

                if ($betrokkene_resultset->count > 1) {
                    $c->session->{vestigingsnummer_override_used} = 1;
                    $c->stash->{betrokkene_resultset} = $betrokkene_resultset;
                    $c->stash->{template} = 'plugins/pip/layouts/aanvraag_picker.tt';
                    $c->detach;
                }
                else {
                    my $betrokkene = $betrokkene_resultset->next;
                    return $betrokkene if $betrokkene;
                }
            }
            else {
                $c->session->{vestigingsnummer_override_used} = 1;
                $c->stash->{template} = 'form/aanvraag_picker_openkvk.tt';
                $c->detach;
            }
        }
        else {
            $self->log->trace("Not sending to picker");
            if (my $ves = $objects->[0]->subject->coc_location_number) {
                $searchopts->{vestigingsnummer} = $ves;
            }
            $c->session->{vestigingsnummer_override_used} = 1;
        }

        $copy = $c->model('Betrokkene')->search(\%betrokkene_opts, $searchopts);
    }

    my $searchcolumn = 'gm_bedrijf_id';
    my $betrokkene_resultset
        = $c->model('Betrokkene')->search(\%betrokkene_opts, $searchopts);

    if ($copy) {
        while (my $betrokkene = $copy->next) {
            $remote_subject_model->update_company_from_remote(
                $c->session, $betrokkene);
        }
    }

    # eHerkenning problem: when no vestigingsnumber is given, and their are
    # more companies in our system, we have to abort.
    if (   !exists $searchopts->{vestigingsnummer}
        && $betrokkene_resultset
        && $betrokkene_resultset->count > 1)
    {
        $c->session->{vestigingsnummer_override_used} = 1;
        $c->stash->{betrokkene_resultset}             = $betrokkene_resultset;
        if ($c->session->{pip} || $c->session->{pip_login}) {
            $c->stash->{template} = 'plugins/pip/layouts/aanvraag_picker.tt';
        }
        else {
            $c->stash->{template} = 'form/aanvraag_picker.tt';
        }
        $c->detach;
    }

    my $betrokkene = $betrokkene_resultset->next;
    return $betrokkene if $betrokkene;
    return;
}

=head2 _assert_requestor_type_and_verification_tool

Asserts if the requestor type and the verification tool make sense.

=cut

sub _assert_requestor_type_and_verification_tool {
    my ($verified, $type) = @_;

    if (!defined $verified) {
        throw(
            "form/auth/no_verification",
            "No verification was done, who are you?"
        );
    }
    elsif ($type eq 'unknown' && $verified eq 'preset_client') {
        return 1;
    }
    elsif ($type eq 'natuurlijk_persoon' && any { $verified eq $_ }
        qw(digid eidas twofactor spoof preset_client))
    {
        return 1;
    }
    elsif ($type eq 'niet_natuurlijk_persoon' && any { $verified eq $_ }
        qw(eherkenning bedrijfid twofactor spoof preset_client))
    {
        return 1;
    }

    throw(
        "form/auth/requestor_type/mismatch",
        "Invalid requestor type '$type' for verification tool '$verified'"
    ),
}

=head2 _kvk_en_vestigingsnummer

Split the ID we get from various login providers to a KvK and vestigingsnummer.
Returns an array with the first element being the kvk number and the second
element (if it exists) is the vestigingsnummer.

=cut

sub _kvk_en_vestigingsnummer {
    my $id = shift;
    my @kvk;
    push(@kvk, int(substr($id, 0, 8)));
    my $l = length($id);
    if ($l > 8 && substr($id, 8, 12) ne '') {
        push(@kvk, int(substr($id, 8, 12)));
    }
    return @kvk;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.

package Catalyst::Helper::Controller::ZAPI;

use strict;

=head1 NAME

Catalyst::Helper::Controller::REST::DBIC::Item - Helper to create REST controllers

=head1 SYNOPSIS

    $ catalyst.pl myapp
    $ cd myapp
    $ script/myapp_create.pl controller Book REST::DBIC::Item Schema::Book

=head1 DESCRIPTION

Helper to create REST Base classes.  After this, you should be reading up on
L<Catalyst::Controller::REST::DBIC::Item>

=head1 METHODS

=head2 mk_compclass

This generates the individual REST classes

=cut

sub mk_compclass {
    my ( $self, $helper, $schema_class ) = @_;
    my $file = $helper->{file};
    $helper->render_file( 'compclass', $file );
}

=head1 SEE ALSO

L<Catalyst::Controller::REST::DBIC::Item>

L<Catalyst::Action::REST>

L<Catalyst::Model::DBIC::Schema>

=head1 AUTHOR

J. Shirley C<cpan@coldhardcode.com>

=head1 LICENSE

This library is free software . You can redistribute it and/or modify
it under the same terms as perl itself.

=cut



1;

__DATA__

__compclass__
package [% class %];

use Moose;
use namespace::autoclean;

BEGIN {extends 'Zaaksysteem::General::ZAPIController'; }

=head1 NAME

[% class %] - ZAPI Controller

=head1 SYNOPSIS

 # Will give a description of this API, and makes it possible to test the API
 /[% name | lower %]/help

=head1 DESCRIPTION

Zaaksysteem API Controller.

=head1 METHODS

=head2 help

Gives the API Help page describing this API and a test interface.

=cut

sub index : Chained('/') : PathPart('[% name | lower %]') : Args(0) : ZAPI {
    my ($self, $c) = @_;

    $c->stash->{zapi}   = ['World', 'Wide', 'Web'];
}

sub create : Chained('/') : PathPart('[% name | lower %]/create') : Args(0) : ZAPI : Profile('create') : AssertProfile {
    my ($self, $c) = @_;

    $c->stash->{zapi}   = ['Hello', 'World'];
}

sub base : Chained('/') : PathPart('[% name | lower %]') : CaptureArgs(1) {
    my ($self, $c, $id) = @_;
}

sub read : Chained('base') : PathPart('') : Args(0): ZAPI {
    my ($self, $c) = @_;

    $c->stash->{zapi}   = ['World'];
}

sub update : Chained('base') : PathPart('update') : Args(0) : Profile('update'): ZAPI {}

sub delete : Chained('base') : PathPart('delete') : Args(0) : Profile('delete'): ZAPI {}

=head1 AUTHOR

[% author %]

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;

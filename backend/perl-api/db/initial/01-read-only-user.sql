BEGIN;
    CREATE USER zs_readonly WITH PASSWORD 'zs_readonly';
    GRANT CONNECT ON DATABASE zaaksysteem TO zs_readonly;
    GRANT USAGE ON SCHEMA public TO zs_readonly;

    GRANT SELECT ON ALL TABLES IN SCHEMA public TO zs_readonly;

    ALTER DEFAULT PRIVILEGES IN SCHEMA public
        GRANT SELECT ON TABLES TO zs_readonly;
COMMIT;

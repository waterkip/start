#!/usr/bin/perl -w

use Moose;
use Data::Dumper;
use JSON;

use Cwd 'realpath';

use FindBin;
use lib "$FindBin::Bin/../../lib";

use Catalyst qw[ConfigLoader];

use Zaaksysteem::Log::CallingLogger;
use Catalyst::Model::DBIC::Schema;

use Net::LDAP;
use Net::LDAP::Control::Paged;
use Net::LDAP::Constant qw( LDAP_CONTROL_PAGED );

use Config::Any;


my $log = Zaaksysteem::Log::CallingLogger->new();

error("USAGE: $0 [hostname] [configfile] [customer.d-dir] [commit]") unless @ARGV && scalar(@ARGV) >= 3;
my ($hostname, $config_file, $config_dir, $commit)    = @ARGV;

my $mig_config                              = {};

my $current_hostname;
sub load_customer_d {
    my $config_root = $config_dir;

    # Fetch configuration files
    opendir CONFIG, $config_root or die "Error reading config root: $!";
    my @confs = grep {/\.conf$/} readdir CONFIG;

    info("Found configuration files: @confs");

    for my $f (@confs) {
        my $config      = _process_config("$config_root/$f");
        my @customers   = $config;

        for my $customer (@customers) {
            for my $config_hostname (keys %$customer) {
                my $config_data     = $customer->{$config_hostname};


                if ($hostname eq $config_hostname ) {
                    info('Upgrading hostname: ' . $hostname);
                    $current_hostname = $hostname;
                    $mig_config->{database_dsn}         = $config_data->{'Model::DB'}->{connect_info}->{dsn};
                    $mig_config->{database_password}    = $config_data->{'Model::DB'}->{connect_info}->{password};

                    $mig_config->{ldap_config_userbase} = $config_data->{'LDAP'}->{basedn};
                }
            }
        }
    }
}

sub load_zaaksysteem_conf {
    my $config_root = 'etc/customer.d/';


    my $config      = _process_config($config_file);

    $mig_config->{ldap_config_hostname}         = $config->{LDAP}->{hostname};
    $mig_config->{ldap_config_password}         = $config->{LDAP}->{password};
    $mig_config->{ldap_config_user}             = $config->{LDAP}->{admin};
}

sub _process_config {
    my ($config) = @_;
    return Config::Any->load_files({
        files       => [$config],
        use_ext     => 1,
        driver_args => {}
    })->[0]->{$config};
}

load_customer_d();
load_zaaksysteem_conf();

error('Cannot find requested hostname: ' . $hostname) unless $current_hostname;

error('Missing one of required config params: ' . Data::Dumper::Dumper($mig_config))
    unless (
        $mig_config->{database_dsn} &&
        $mig_config->{ldap_config_hostname} &&
        $mig_config->{ldap_config_user} &&
        $mig_config->{ldap_config_password}
    );

my $dbic                                = database($mig_config->{database_dsn}, $mig_config->{user}, $mig_config->{database_password});

$dbic->txn_do(sub {
    migrate_subjects($dbic);

    unless ($commit) {
        die("ROLLBACK, COMMIT BIT not given\n");
    }
});

$log->info('All done.');
$log->_flush;

sub database {
    my ($dsn, $user, $password) = @_;

    my %connect_info = (
        dsn             => $dsn,
        pg_enable_utf8  => 1,
    );

    $connect_info{password}     = $password if $password;
    $connect_info{user}         = $user if $user;


    Catalyst::Model::DBIC::Schema->config(
        schema_class => 'Zaaksysteem::Schema',
        connect_info => \%connect_info
    );

    my $schema = Catalyst::Model::DBIC::Schema->new->schema;

    $schema->betrokkene_model->log($log);

    return $schema;
}

sub error {
    my $error = shift;

    $log->error($error);
    $log->_flush;

    die("\n");
}

sub info {
    $log->info(sprintf shift, @_);
    $log->_flush;
}

sub migrate_subjects {
    my $dbic                        = shift;

    my $ldap_entries                = get_all_ldap_users();

    my $interface                   = $dbic->resultset('Interface')->find_by_module_name('authldap') or
        error('Cannot find LDAP interface for authldap, please create first');

    update_zaken($ldap_entries, $interface);
    update_zaakbetrokkenen($ldap_entries, $interface);
    update_contactmoment($ldap_entries, $interface);
    update_files($ldap_entries, $interface);
    update_kennisbank_producten($ldap_entries, $interface);
    update_kennisbank_vragen($ldap_entries, $interface);
    update_searches($ldap_entries, $interface);
    update_notificaties($ldap_entries, $interface);
    update_logging_data($ldap_entries, $interface);
    update_logging($ldap_entries, $interface);
}

sub update_contactmoment {
    my $ldap_entries                = shift;
    my $interface                   = shift;

    my $entries = $dbic->resultset('Contactmoment')->search();

    my $counter = $entries->count;
    while (my $entry = $entries->next) {
        info('Updating "contactmoment" ' . $counter--);

        if ($entry->subject_id && (my ($ldap_id) = $entry->subject_id =~ /medewerker-(\d+)$/)) {
            my $new_id                  = get_new_ldap_id($interface, $ldap_entries, $ldap_id);

            if ($new_id) {
                info('Update entry "contactmoment" subject_id with ID ' . $new_id);
                $entry->subject_id('betrokkene-medewerker-' . $new_id);
            } else {
                info('Failed updating entry "contactmoment" subject_id with ID ' . $entry->id . ' because of missing medewerker');
            }
        }

        if ($entry->created_by && (my ($ldap_id) = $entry->created_by =~ /medewerker-(\d+)$/)) {

            my $new_id                  = get_new_ldap_id($interface, $ldap_entries, $ldap_id);

            if ($new_id) {
                info('Update entry "contactmoment" created_by with ID ' . $new_id);
                $entry->created_by('betrokkene-medewerker-' . $new_id);
            } else {
                info('Failed updating entry "contactmoment" created_by with ID ' . $entry->id . ' because of missing medewerker');
            }
        }

        $entry->update;
    }
}


# sub update_files {
#     my $ldap_entries                = shift;
#     my $interface                   = shift;

#     my $entries = $dbic->resultset('File')->search();

#     my $counter = $entries->count;
#     while (my $entry = $entries->next) {
#         info('Updating "file" ' . $counter--);

#         if ($entry->created_by && (my ($ldap_id) = $entry->created_by =~ /medewerker-(\d+)$/)) {
#             my ($ldap_id)               = $entry->created_by =~ /medewerker-(\d+)$/;

#             my $new_id                  = get_new_ldap_id($interface, $ldap_entries, $ldap_id);

#             if ($new_id) {
#                 info('Update entry "file" created_by with ID ' . $new_id);
#                 $entry->created_by('betrokkene-medewerker-' . $new_id);
#             } else {
#                 info('Failed updating entry "file" created_by with ID ' . $entry->id . ' because of missing medewerker');
#             }
#         }

#         if ($entry->modified_by && (my ($ldap_id) = $entry->modified_by =~ /medewerker-(\d+)$/)) {

#             my $new_id                  = get_new_ldap_id($interface, $ldap_entries, $ldap_id);

#             if ($new_id) {
#                 info('Update entry "file" modified_by with ID ' . $new_id);
#                 $entry->modified_by('betrokkene-medewerker-' . $new_id);
#             } else {
#                 info('Failed updating entry "file" modified_by with ID ' . $entry->id . ' because of missing medewerker');
#             }
#         }

#         if ($entry->deleted_by && (my ($ldap_id) = $entry->deleted_by =~ /medewerker-(\d+)$/)) {
#             my ($ldap_id)               = $entry->deleted_by =~ /medewerker-(\d+)$/;

#             my $new_id                  = get_new_ldap_id($interface, $ldap_entries, $ldap_id);

#             if ($new_id) {
#                 info('Update entry "file" deleted_by with ID ' . $new_id);
#                 $entry->deleted_by('betrokkene-medewerker-' . $new_id);
#             } else {
#                 info('Failed updating entry "file" deleted_by with ID ' . $entry->id . ' because of missing medewerker');
#             }
            
#         }

#         $entry->update;
#     }
# }

sub update_files {
    my $ldap_entries                = shift;
    my $interface                   = shift;

    my %unique_logging_ids;
    for my $attribute (qw/created_by modified_by deleted_by/) {
        my $rs = $dbic->resultset('File')->search(
            {
                '-and'  => [
                    { $attribute      => { '!=' => undef } },
                    { $attribute      => { '!=' => '' }},
                    { $attribute      => { 'like' => 'betrokkene-medewerker-%' }},
                ],
            },
            {
                select      => [ $attribute],
                as          => [$attribute],
                group_by    => $attribute,
            }
        );

        while (my $entry = $rs->next) {
            my ($ldap_id) = $entry->$attribute =~ /medewerker-(\d+)$/;

            my $new_id    = get_new_ldap_id($interface, $ldap_entries, $ldap_id);
            $unique_logging_ids{$entry->$attribute} = 'betrokkene-medewerker-' . $new_id if $new_id;
        }
    }

    for my $attribute (qw/created_by modified_by deleted_by/) {
        for my $logging_id (keys %unique_logging_ids) {
            my $statement = "UPDATE file SET "
                . $attribute . "='" . $unique_logging_ids{$logging_id} . "' WHERE "
                . $attribute . "='" . $logging_id . "';";

            info($statement);

            $dbic->storage->debug(1);
            $dbic->storage->dbh_do(
                sub {
                    my ($storage, $dbh, @args) = @_;

                    $dbh->do($statement);
                }
            );
        }
    }
}


sub update_kennisbank_producten {
    my $ldap_entries                = shift;
    my $interface                   = shift;

    my $entries = $dbic->resultset('KennisbankProducten')->search(
        {
            author  => { '!=' => undef }
        }
    );

    my $counter = $entries->count;
    while (my $entry = $entries->next) {
        info('Updating "kennisbank_producten" ' . $counter--);

        my ($ldap_id)               = $entry->author =~ /medewerker-(\d+)$/;

        my $new_id                  = get_new_ldap_id($interface, $ldap_entries, $ldap_id);

        if ($new_id) {
            info('Update entry "kennisbank_producten" with ID ' . $new_id);
        } else {
            info('Failed updating entry "kennisbank_producten" with ID ' . $entry->id . ' because of missing medewerker');
            next;
        }
        $entry->author('betrokkene-medewerker-' . $new_id);
        $entry->update;
    }
}

sub update_kennisbank_vragen {
    my $ldap_entries                = shift;
    my $interface                   = shift;

    my $entries = $dbic->resultset('KennisbankVragen')->search(
        {
            author  => { '!=' => undef }
        }
    );

    my $counter = $entries->count;
    while (my $entry = $entries->next) {
        info('Updating "kennisbank_vragen" ' . $counter--);

        my ($ldap_id)               = $entry->author =~ /medewerker-(\d+)$/;

        my $new_id                  = get_new_ldap_id($interface, $ldap_entries, $ldap_id);

        if ($new_id) {
            info('Update entry "kennisbank_vragen" with ID ' . $new_id);
        } else {
            info('Failed updating entry "kennisbank_vragen" with ID ' . $entry->id . ' because of missing medewerker');
            next;
        }
        $entry->author('betrokkene-medewerker-' . $new_id);
        $entry->update;
    }
}

sub update_searches {
    my $ldap_entries                = shift;
    my $interface                   = shift;

    my $entries = $dbic->resultset('SearchQuery')->search();

    my $counter = $entries->count;
    while (my $entry = $entries->next) {
        info('Updating "search_query" ' . $counter--);

        my $new_id                  = get_new_ldap_id($interface, $ldap_entries, $entry->ldap_id);

        if ($new_id) {
            info('Update entry "search_query" with ID ' . $new_id);
        } else {
            info('Failed updating entry "search_query" with ID ' . $entry->id . ' because of missing medewerker');
            next;
        }
        $entry->ldap_id($new_id);
        $entry->update;
    }
}

sub update_zaakbetrokkenen {
    my $ldap_entries                = shift;
    my $interface                   = shift;

    my $entries = $dbic->resultset('ZaakBetrokkenen')->search(
        {
            betrokkene_type     => 'medewerker'
        }
    );

    my $counter = $entries->count;
    while (my $entry = $entries->next) {
        info('Updating "zaak_betrokkenen" ' . $counter--);

        my $new_id                  = get_new_ldap_id($interface, $ldap_entries, $entry->gegevens_magazijn_id);

        if ($new_id) {
            info('Update entry "zaak_betrokkenen" with ID ' . $new_id);
            $entry->betrokkene_id($new_id);
            $entry->gegevens_magazijn_id($new_id);
            $entry->update;
        } else {
            info('Failed updating entry "zaak_betrokkenen" with ID ' . $entry->id . ' because of missing medewerker');
        }

        if ($entry->zaak_id) {
            my $zaak   = $entry->zaak_id;

            if (
                $zaak->aanvrager &&
                $zaak->aanvrager->id eq $entry->id
            ) {
                info('Update entry "zaak" with ID ' . $zaak->id . ': Aanvrager');
                $zaak->aanvrager_gm_id($entry->gegevens_magazijn_id);
            }

            if (
                $zaak->behandelaar &&
                $zaak->behandelaar->id eq $entry->id
            ) {
                $zaak->behandelaar_gm_id($entry->gegevens_magazijn_id);
                info('Update entry "zaak" with ID ' . $zaak->id . ': Behandelaar');
            }

            if (
                $zaak->coordinator &&
                $zaak->coordinator->id eq $entry->id
            ) {
                $zaak->coordinator_gm_id($entry->gegevens_magazijn_id);
                info('Update entry "zaak" with ID ' . $zaak->id . ': Coordinator');
            }
            $zaak->touched(1);
            $zaak->update;
        }
    }


}

sub update_zaken {
    my $ldap_entries                = shift;
    my $interface                   = shift;

    my $entries = $dbic->resultset('Zaak')->search(
        {
            '-or'   => [
                { aanvrager_gm_id => { '>' => 5000 }},
                { behandelaar_gm_id => { '>' => 5000 }},
                { coordinator_gm_id => { '>' => 5000 }}
            ]
        }
    );

    my $counter = $entries->count;
    while (my $entry = $entries->next) {
        info('Updating "zaak" ' . $counter--);

        my $zaak   = $entry;

        if (
            $zaak->aanvrager_gm_id &&
            $zaak->aanvrager &&
            $zaak->aanvrager->betrokkene_type eq 'medewerker'
        ) {
            info('Update entry "zaak" with ID ' . $zaak->id . ': Aanvrager');

            my $ldap_id = $zaak->aanvrager_gm_id;

            if ($ldap_id) {
                my $new_id    = get_new_ldap_id($interface, $ldap_entries, $ldap_id);

                if ($new_id) {
                    info('Update entry "zaak" with ID ' . $new_id);

                    $zaak->aanvrager_gm_id($new_id);
                } else {
                    info('Failed updating entry "logging" with ID ' . $zaak->id . ' because of missing medewerker');
                }
            }
        }

        if (
            $zaak->behandelaar_gm_id
        ) {
            info('Update entry "zaak" with ID ' . $zaak->id . ': Behandelaar');

            my $ldap_id = $zaak->behandelaar_gm_id;

            if ($ldap_id) {
                my $new_id    = get_new_ldap_id($interface, $ldap_entries, $ldap_id);

                if ($new_id) {
                    info('Update entry "zaak" with ID ' . $new_id);

                    $zaak->behandelaar_gm_id($new_id);
                } else {
                    info('Failed updating entry "logging" with ID ' . $zaak->id . ' because of missing medewerker');
                }
            }
        }

        if (
            $zaak->coordinator_gm_id
        ) {
            info('Update entry "zaak" with ID ' . $zaak->id . ': Coordinator');

            my $ldap_id = $zaak->coordinator_gm_id;

            if ($ldap_id) {
                my $new_id    = get_new_ldap_id($interface, $ldap_entries, $ldap_id);

                if ($new_id) {
                    info('Update entry "zaak" with ID ' . $new_id);

                    $zaak->coordinator_gm_id($new_id);
                } else {
                    info('Failed updating entry "logging" with ID ' . $zaak->id . ' because of missing medewerker');
                }
            }
        }
        $zaak->touched(1);
        $zaak->update;
    }
}

my %cached_uidnumbers;
sub get_new_ldap_id {
    my $interface                   = shift;
    my $ldap_entries                = shift;
    my $uidnumber                   = shift;

    error('Not a number! :' . $uidnumber) unless $uidnumber =~ /^\d+$/;

    if ($cached_uidnumbers{$uidnumber}) {
        info('Returning user from cache: ' . $cached_uidnumbers{$uidnumber});
        return $cached_uidnumbers{$uidnumber};
    }

    if ($uidnumber < 5000) {
        info('SKIP Probably already merged, since ldap_id is below 5000, SKIP');
        return;
    }

    my $subject;
    if ($ldap_entries->{ $uidnumber }) {
        my $ldap_user = $ldap_entries->{ $uidnumber };

        $subject = $dbic->resultset('Subject')->update_or_create_from_ldap(
            {
                'interface_id'      => $interface->id,
                'ldap_user'         => $ldap_user,
            }
        );

        if ($subject) {
            info('Adding user "' . $ldap_user->get_value('cn') . '": ' . $uidnumber . ' / New ID: ' . $subject->id);
            $cached_uidnumbers{$uidnumber} = $subject->id;
            return $cached_uidnumbers{$uidnumber};
        }


    } else {
        info('Cannot find user for uidnumber, using Systeemgebruiker [' . $uidnumber . ']');

        my $systeem_gebruiker = get_systeem_gebruiker($interface);
        $cached_uidnumbers{$uidnumber} = $systeem_gebruiker;
        return $cached_uidnumbers{$uidnumber};

    }

    return $subject->id;
}

my $systeem_id;
sub get_systeem_gebruiker {
    my $interface           = shift;

    return $systeem_id if $systeem_id;

    my $subject_rs          = $interface
                            ->result_source
                            ->schema
                            ->resultset('Subject');

    my $systeemgebruiker    = $subject_rs
                            ->find(
                                {
                                    'username'  => 'SysteemGebruiker'
                                }
                            );

    if (!$systeemgebruiker) {
        $systeemgebruiker   = $subject_rs
                            ->create(
                            {
                                subject_type    => 'employee',
                                properties      => {
                                    initials        => 'S.',
                                    cn              => 'systeemgebruiker',
                                    givenname       => 'systeemgebruiker',
                                    sn              => 'systeemgebruiker',
                                    displayname     => 'systeemgebruiker',
                                    mail            => 'no-reply@example.com',
                                    dn              => '',
                                },
                                username        => 'SysteemGebruiker'
                            });
    }

    error('Unable to get systeemgebruiker') unless $systeemgebruiker;
    return ($systeem_id = $systeemgebruiker->id);
}

sub get_all_ldap_users {
    my $self                        = shift;

    my $ldap                        = Net::LDAP->new(
        $mig_config->{ldap_config_hostname},
        version => 3,
    );

    $ldap->bind(
        $mig_config->{ldap_config_user},
        password    => $mig_config->{ldap_config_password}
    );

    my $page = Net::LDAP::Control::Paged->new(size => 500);
    my $cookie;

    my $filter          = '(&(cn=*)(objectClass=posixAccount))';

    my $entries = [];
    while (1) {
        my $mesg = $ldap->search( # perform a search
            base   => $mig_config->{ldap_config_userbase},
            filter => $filter,
            scope  => 'subtree',
            control => [$page]
        );
    
        $mesg->code && error("Error on search: $@ : " . $mesg->error);
        push(@{ $entries } , $mesg->entries());
    
        my ($resp) = $mesg->control(LDAP_CONTROL_PAGED) or last;
        $cookie    = $resp->cookie or last;
        # Paging Control
        $page->cookie($cookie);
    }

    if ($cookie) {
        # Abnormal exit, so let the server know we do not want any more
        $page->cookie($cookie);
        $page->size(0);
        $ldap->search(control => [$page]);
        error("abnormal exit: due to mis paging");
    }

    my $mapped_entries = {
        map { $_->get_value('uidNumber') => $_ } @{ $entries }
    };

    return $mapped_entries;
}

sub update_notificaties {
    my $ldap_entries                = shift;
    my $interface                   = shift;

    my $entries = $dbic->resultset('ZaaktypeNotificatie')->search(
        {
            'behandelaar'        => { '!=' => undef},
            'behandelaar'        => { '!=' => '' }
        }
    );

    my $counter = $entries->count;
    while (my $entry = $entries->next) {
        info('Updating "notificaties" ' . $counter--);

        my ($ldap_id) = $entry->behandelaar =~ /medewerker-(\d+)$/;

        if ($ldap_id) {
            my $new_id    = get_new_ldap_id($interface, $ldap_entries, $ldap_id);

            if ($new_id) {
                info('Update entry "notificaties" with ID ' . $new_id);
            } else {
                info('Failed updating entry "notificaties" with ID ' . $entry->id . ' because of missing medewerker');
                next;
            }
            
            $entry->behandelaar('betrokkene-medewerker-' . $new_id);

            $entry->update;
        }
    }
}

sub update_logging_data {
    my $ldap_entries                = shift;
    my $interface                   = shift;

    my $entries = $dbic->resultset('Logging')->search(
        {
            'event_type'        => [qw|subject/create subject/remove|],
        }
    );

    my $counter = $entries->count;
    while (my $entry = $entries->next) {
        info('Updating "logging" [' . $entry->id . '] ' . $counter--);

        if (
            $entry->event_type eq 'subject/create' ||
            $entry->event_type eq 'subject/remove'
        ) {
            my $data        = JSON::from_json($entry->event_data);

            info('Subject id: ' . $data->{subject_id});
            my ($ldap_id) = $data->{subject_id} =~ /medewerker-(\d+)$/;

            if ($ldap_id) {
                my $new_id    = get_new_ldap_id($interface, $ldap_entries, $ldap_id);

                if ($new_id) {
                    info('Update entry "logging" with ID ' . $new_id);

                    $data->{subject_id} = 'betrokkene-medewerker-' . $new_id;
                } else {
                    info('Failed updating entry "logging" with ID ' . $entry->id . ' because of missing medewerker');
                }              
            }

            $entry->event_data(JSON::encode_json($data));
        }

        $entry->update;
    }
}

sub update_logging {
    my $ldap_entries                = shift;
    my $interface                   = shift;

    my %unique_logging_ids;
    for my $attribute (qw/created_by modified_by deleted_by created_for aanvrager_id betrokkene_id/) {
        my $rs = $dbic->resultset('Logging')->search(
            {
                '-and'  => [
                    { $attribute      => { '!=' => undef } },
                    { $attribute      => { '!=' => '' }},
                    { $attribute      => { 'like' => 'betrokkene-medewerker-%' }},
                ],
            },
            {
                select      => [ $attribute],
                as          => [$attribute],
                group_by    => $attribute,
            }
        );

        while (my $entry = $rs->next) {
            my ($ldap_id) = $entry->$attribute =~ /medewerker-(\d+)$/;

            my $new_id    = get_new_ldap_id($interface, $ldap_entries, $ldap_id);
            $unique_logging_ids{$entry->$attribute} = 'betrokkene-medewerker-' . $new_id if $new_id;
        }
    }

    for my $attribute (qw/created_by modified_by deleted_by created_for aanvrager_id betrokkene_id/) {
        for my $logging_id (keys %unique_logging_ids) {
            my $statement = "UPDATE logging SET "
                . $attribute . "='" . $unique_logging_ids{$logging_id} . "' WHERE "
                . $attribute . "='" . $logging_id . "';";

            info($statement);

            $dbic->storage->debug(1);
            $dbic->storage->dbh_do(
                sub {
                    my ($storage, $dbh, @args) = @_;

                    $dbh->do($statement);
                }
            );
        }
    }
}

1;

BEGIN;

  CREATE OR REPLACE FUNCTION get_confidential_mapping(
    IN type varchar,
    OUT mapping JSONB
  )
  LANGUAGE plpgsql
  AS $$
  DECLARE
    mapped text;
  BEGIN

    IF type = 'public'
    THEN
      mapped := 'Openbaar';
    ELSIF type = 'internal'
    THEN
      mapped := 'Intern';
    ELSIF type = 'confidential'
    THEN
      mapped := 'Geheim';
    ELSE
      RAISE EXCEPTION 'Unknown confidentiality type %', type;
    END IF;


    mapping := jsonb_build_object(
      'original', type,
      'mapped', mapped
    );

  END;
  $$;

COMMIT;


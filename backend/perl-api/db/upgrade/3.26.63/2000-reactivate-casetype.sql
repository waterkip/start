BEGIN;

    CREATE TEMP TABLE zaaktypes_reactivate (
        id integer
    );

    INSERT INTO zaaktypes_reactivate (id)
    SELECT
        distinct(odct.object_id)
    FROM
        object_data od
    JOIN
        object_data odct
    ON
        od.class_uuid = odct.uuid
    JOIN
        zaaktype zt
    ON
        odct.object_id = zt.id
    WHERE
        zt.deleted is not null
    AND
        odct.object_class = 'casetype'
    ;

    INSERT INTO queue (type, label, priority, metadata, data)
        SELECT 'touch_casetype', 'Devops: reactivate legacy casetypes', 3000, '{"require_object_model":1, "disable_acl": 1, "target":"backend"}',
            '{"id":' || id || '}'
    FROM zaaktypes_reactivate;

    UPDATE zaaktype set deleted = null
        where id in (SELECT id FROM zaaktypes_reactivate );

COMMIT;


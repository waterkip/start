BEGIN;

CREATE OR REPLACE FUNCTION get_phases_as_json(zaaktype_nodeid int)
RETURNS table(j json)
IMMUTABLE
LANGUAGE plpgsql
AS $$
declare
begin
    return QUERY select json_agg(phases) from (select zts.status as phase_id,
        zts.naam as milestone_name,
        zts.fase as phase_name,
        zts.created,
        zts.last_modified,
        zts.termijn as term_in_days,
        case when g.uuid is null or r.uuid is null then null else
        json_build_object(
            'enabled', case when zts.status = 1 then true else text_to_bool(zts.role_set::text) end,
            'department_uuid', g.uuid,
            'department_name', g.name,
            'role_uuid', r.uuid,
            'role_name', r.name
        ) end as assignment,
        get_kenmerken_for_phase_as_json(zts.id) as custom_fields
        from zaaktype_status zts
        left join roles r on r.id = zts.role_id
        left join groups g on g.id = zts.ou_id
        where zts.zaaktype_node_id = zaaktype_nodeid
        order by zts.status) phases;
END;
$$;

COMMIT;
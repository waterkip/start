BEGIN;

  CREATE OR REPLACE FUNCTION attribute_date_value_to_text(
    IN value text,
    OUT datestamp text
  )
  LANGUAGE plpgsql
  AS $$
  DECLARE
    dd text;
    mm text;
    yy text;
    dt timestamp;
    tz text;
    landcode text;
  BEGIN
    dd := split_part(value, '-', 1);
    mm := split_part(value, '-', 2);
    yy := split_part(value, '-', 3);

    -- We have attributes which are switched around :((
    IF length(yy) = 2 AND length(dd) = 4
    THEN
      -- 2019-10-21
      dt := make_date(dd::int, mm::int, yy::int);
    ELSIF length(yy) > 2 and length(dd) = 4
    THEN
      -- and we have actual timestamps
      dt := value::date;
    ELSE
      -- 25-10-2019
      dt := make_date(yy::int, mm::int, dd::int);
    END IF;

    SELECT INTO landcode COALESCE(config.value, '6030') FROM config WHERE parameter = 'customer_info_country_code';
    IF landcode = '5107'
    THEN
      SET LOCAL timezone = 'America/Curacao';
    ELSE
      SET LOCAL timezone = 'Europe/Amsterdam';
    END IF;

    SELECT INTO datestamp timestamp_to_perl_datetime(dt::timestamp with time zone at time zone 'Europe/Amsterdam');

  EXCEPTION WHEN OTHERS THEN
    RETURN;

  END;
  $$;

COMMIT;


BEGIN;

  UPDATE interface SET
    interface_config = interface_config::jsonb || '{"api_version": "soap" }' WHERE
    module = 'xential';

COMMIT;

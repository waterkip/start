BEGIN;

CREATE OR REPLACE FUNCTION get_kenmerken_for_phase_as_json(zaaktype_status_id int)
RETURNS table(j json)
IMMUTABLE
LANGUAGE plpgsql
AS $$
declare
begin
    return QUERY select json_agg(attributes_for_phase) from (
    with kenmerk_permissions as (
        with kenmerk_permissions_records as (
            select kenmerk_permissions_records.id,
            (kenmerk_permissions_records.permissions->>'role_id')::int as role_id,
            (kenmerk_permissions_records.permissions->>'org_unit_id')::int as group_id
            from (
                select id, JSONB_ARRAY_ELEMENTS(required_permissions::jsonb->'selectedUnits') as permissions
                from zaaktype_kenmerken zk
            ) as kenmerk_permissions_records
        )
        select zk.id, json_agg(
            case when g.uuid is null then '{}' else json_build_object(
                'department_uuid', g.uuid,
                'department_name', g.name,
                'role_uuid', r.uuid,
                'role_name', r.name
            ) end
        ) as permissions
        from zaaktype_kenmerken zk
        left join kenmerk_permissions_records kp on kp.id = zk.id
        join groups g on g.id = kp.group_id
        join roles r on r.id = kp.role_id
        where zk.required_permissions::jsonb->'selectedUnits' is not null 
        and zk.required_permissions::jsonb->'selectedUnits' != '[]'
        group by zk.id
    )
    select bk.uuid as uuid,
        coalesce(zk.is_group::bool, False) as is_group,
        coalesce(zk.referential::bool, False) as referential,
        coalesce(zk.value_mandatory::bool, False) as mandatory,
        coalesce(zk.is_systeemkenmerk::bool, False) as system_attribute,
        zk.label as title,
        zk.help as help_intern,
        zk.help_extern as help_extern,
        zk.label_multiple,
        coalesce(zk.bag_zaakadres::bool, False) as use_as_case_address,
        bk.naam as attribute_name,
        bk.magic_string as attribute_magic_string,
        bk.relationship_type as relationship_type,
        coalesce(bk.type_multiple::bool, False) as is_multiple,
        text_to_bool(zk.pip::text) as publish_pip,
        text_to_bool(zk.pip_can_change::text) as pip_can_change,
        text_to_bool(zk.properties::json->>'skip_change_approval'::text)as skip_change_approval,
        text_to_bool(zk.properties::json->'custom_object'->'create'->>'enabled'::text)as create_custom_object_enabled,
        zk.properties::json->'custom_object'->'create'->>'label'::text as create_custom_object_label,
        zk.properties::json->'custom_object'->'attributes' as create_custom_object_attribute_mapping,
        zk.properties::json->>'relationship_subject_role' as relationship_subject_role,
        text_to_bool(zk.properties::json->>'show_on_map'::text)as show_on_map,
        zk.properties::json->>'map_wms_layer_id' as map_wms_layer_id,
        (select naam from bibliotheek_kenmerken where id = 
            case when zk.properties::json->>'map_wms_feature_attribute_id' ~ E'^\\d+$' then cast(zk.properties::json->>'map_wms_feature_attribute_id' as int) else null end) as map_wms_feature_attribute_label,
        (select uuid from bibliotheek_kenmerken where id = 
            case when zk.properties::json->>'map_wms_feature_attribute_id' ~ E'^\\d+$' then cast(zk.properties::json->>'map_wms_feature_attribute_id' as int) else null end) as map_wms_feature_attribute_id,
        text_to_bool(zk.properties::json->>'map_case_location'::text) as map_case_location,
        case when text_to_bool(zk.properties::json->'date_limit'->'start'->>'active') then 
            json_build_object(
                'value', zk.properties::json->'date_limit'->'start'->>'num',
                'active', text_to_bool(zk.properties::json->'date_limit'->'start'->>'active'),
                'term', zk.properties::json->'date_limit'->'start'->>'term',
                'during', zk.properties::json->'date_limit'->'start'->>'during',
                'reference', case when zk.properties::json->'date_limit'->'start'->>'reference' ~ '^[0-9]+$' then 
                (select inner_bk.uuid from bibliotheek_kenmerken inner_bk join zaaktype_kenmerken inner_ztk on inner_bk.id = inner_ztk.bibliotheek_kenmerken_id where inner_ztk.id = (zk.properties::json->'date_limit'->'start'->>'reference')::int)::text else 'currentDate' end
            ) else null end as start_date_limitation,
        case when text_to_bool(zk.properties::json->'date_limit'->'end'->>'active') then 
            json_build_object(
                'value', zk.properties::json->'date_limit'->'end'->>'num',
                'active', text_to_bool(zk.properties::json->'date_limit'->'end'->>'active'),
                'term', zk.properties::json->'date_limit'->'end'->>'term',
                'during', zk.properties::json->'date_limit'->'end'->>'during',
                'reference', case when zk.properties::json->'date_limit'->'end'->>'reference' ~ '^[0-9]+$' then 
                (select inner_bk.uuid from bibliotheek_kenmerken inner_bk join zaaktype_kenmerken inner_ztk on inner_bk.id = inner_ztk.bibliotheek_kenmerken_id where inner_ztk.id = (zk.properties::json->'date_limit'->'end'->>'reference')::int)::text else 'currentDate' end
            ) else null end as end_date_limitation,
       coalesce(bk.value_type,
           case when bk.value_type is null and zk.is_group then 'group' else null end,
           case when bk.value_type is null and not zk.is_group then 'textblock' else null end
       ) as attribute_type,
       kp.permissions
    from zaaktype_kenmerken zk left join bibliotheek_kenmerken bk on zk.bibliotheek_kenmerken_id = bk.id
    left join kenmerk_permissions kp on kp.id = zk.id
    where zk.zaak_status_id = zaaktype_status_id order by zk.id asc) attributes_for_phase;
END;
$$;

COMMIT;
BEGIN;

  CREATE TEMP TABLE altauthsubjects AS
  SELECT
    s.id,
    s.properties,
    s.subject_type,
    s.uuid
  FROM
    subject s
  LEFT JOIN gegevensmagazijn_subjecten gm ON s.uuid = gm.subject_uuid
  WHERE
    s.subject_type = 'company'
    AND
    gm.subject_uuid IS NULL;

  CREATE TABLE userentity_backup AS
    SELECT * FROM
      user_entity
    WHERE
    subject_id IN (SELECT id FROM altauthsubjects);

  DELETE FROM
    user_entity
  WHERE
    subject_id IN (SELECT id FROM altauthsubjects);

  /* This action may fail with the following error: 
   * ERROR:  update or delete on table "subject" violates foreign key
   * constraint "zaak_behandelaar_gm_id_fkey" on table "zaak"
   * DETAIL:  Key (id)=(127) is still referenced from table "zaak".
   * In case it does, run pre-1000-alt-auth-subjects.disabled
   */
  CREATE TABLE subject_backup AS
    SELECT * FROM
      subject
    WHERE
     uuid IN (SELECT uuid FROM altauthsubjects);

  DELETE
  FROM
    subject
  WHERE
    uuid IN (
      SELECT
        uuid
      FROM
        altauthsubjects
    )
  ;
COMMIT;

BEGIN;

    -- MINTY-660
    ALTER TABLE directory ADD COLUMN uuid uuid UNIQUE;
    ALTER TABLE directory ALTER uuid SET DEFAULT uuid_generate_v4();
    UPDATE directory SET uuid = DEFAULT where uuid IS NULL;
    ALTER TABLE directory ALTER uuid SET NOT NULL;

    -- MINTY-630
    ALTER TABLE bibliotheek_kenmerken ALTER naam_public SET DEFAULT '';
    UPDATE bibliotheek_kenmerken SET naam_public = '' WHERE naam_public IS NULL;
    ALTER TABLE bibliotheek_kenmerken ALTER naam_public SET NOT NULL;

    ALTER TABLE bibliotheek_kenmerken ALTER value_default SET DEFAULT '';
    UPDATE bibliotheek_kenmerken SET value_default = '' WHERE value_default IS NULL;
    ALTER TABLE bibliotheek_kenmerken ALTER value_default SET NOT NULL;

    ALTER TABLE bibliotheek_kenmerken ALTER help SET DEFAULT '';
    UPDATE bibliotheek_kenmerken SET help = '' WHERE help IS NULL;
    ALTER TABLE bibliotheek_kenmerken ALTER help SET NOT NULL;

    -- MINTY-691
    CREATE UNIQUE INDEX IF NOT EXISTS bibliotheek_category_unique_name ON bibliotheek_categorie(naam, COALESCE(pid, -1));

COMMIT;

BEGIN;
CREATE UNIQUE INDEX persoonsnummer_idx ON natuurlijk_persoon (persoonsnummer, authenticated) WHERE deleted_on IS NULL;
COMMIT;

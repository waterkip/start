BEGIN;

INSERT INTO config("parameter", "value") VALUES
      ('bag_spoof_mode', '0')
    , ('bag_local_only', '1')
    , ('bag_priority_gemeentes', '[]');

COMMIT;

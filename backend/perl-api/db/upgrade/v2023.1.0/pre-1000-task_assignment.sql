BEGIN;

    ALTER TABLE checklist_item ADD assignee_person_id INTEGER REFERENCES natuurlijk_persoon(id) ON DELETE SET NULL;
    ALTER TABLE checklist_item ADD assignee_organization_id INTEGER REFERENCES bedrijf(id) ON DELETE SET NULL;

    ALTER TABLE checklist_item ADD CONSTRAINT single_assignee CHECK(
        (CASE WHEN assignee_id IS NULL THEN 0 ELSE 1 END
         + CASE WHEN assignee_person_id IS NULL THEN 0 ELSE 1 END
         + CASE WHEN assignee_organization_id IS NULL THEN 0 ELSE 1 END
        ) <= 1
    );

COMMIT;

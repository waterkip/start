BEGIN;

  ALTER TABLE zaaktype_kenmerken ADD COLUMN custom_object_uuid UUID REFERENCES custom_object_type(uuid);

COMMIT;

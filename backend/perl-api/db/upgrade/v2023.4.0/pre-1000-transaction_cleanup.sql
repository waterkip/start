-- delete items from transaction table older than 120 days or when date_deleted is set
-- this script will run using a cronjob. to limit the number of records to delete / io, a limit of 10000 is used
BEGIN;

   CREATE OR REPLACE FUNCTION cleanup_transaction() RETURNS void LANGUAGE plpgsql AS $$    
   DECLARE
   BEGIN
      delete from transaction where id in ( 
         select id from transaction where date_created < CURRENT_DATE - '120 days'::INTERVAL or date_deleted is not null limit 10000
      );
   END;
   $$;
COMMIT;
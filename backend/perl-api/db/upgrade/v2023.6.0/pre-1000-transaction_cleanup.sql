-- delete items from transaction table:
-- - older than 120 days if not planned email 
-- - when date_deleted is set
BEGIN;

   CREATE OR REPLACE FUNCTION cleanup_transaction() RETURNS void LANGUAGE plpgsql AS $$    
   DECLARE
   BEGIN
      DELETE FROM transaction WHERE id IN ( 
         SELECT t.id FROM transaction t JOIN interface i ON t.interface_id = i.id WHERE (
            (i.module != 'email' AND t.date_created < CURRENT_DATE - '120 days'::INTERVAL)
            OR (t.date_deleted IS NOT NULL)
         ) LIMIT 10000
      );
   END;
   $$;

COMMIT;

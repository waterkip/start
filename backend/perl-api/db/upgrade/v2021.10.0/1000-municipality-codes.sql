BEGIN;

CREATE TABLE municipality_code (
  id SERIAL,
  dutch_code INT NOT NULL UNIQUE,
  label TEXT NOT NULL,
  alternative_name TEXT,
  uuid UUID NOT NULL DEFAULT uuid_generate_v4(),
  historical BOOLEAN DEFAULT FALSE NOT NULL
)
;
INSERT INTO municipality_code (
  dutch_code,
  label,
  alternative_name,
  uuid,
  historical
)
VALUES
(9999, 'Testgemeente', NULL, '02dbb0de-ca03-473b-9f7d-1ca80360e878', false),
(1332, 'Testgemeente (historisch)', NULL, '02dbb0de-ca03-473b-9f7d-1ca80360e878', true),
(1999, 'RNI / Registratie Niet Ingezetenen', NULL, 'b4edc05c-0f29-45d6-b12a-81b986a882fd', false),
(3, 'Appingedam', NULL, '0f44329e-ccb9-4bae-8c54-fef95b4b7328', true),
(5, 'Bedum', NULL, '26f48bd5-9247-45ad-936a-363a358f40e5', true),
(7, 'Bellingwedde', NULL, '7bb9e5f7-b594-42c6-a4c0-5694d9df89e9', true),
(9, 'Ten Boer', NULL, '831b4209-8c06-412d-ae91-44a59f324f3e', true),
(10, 'Delfzijl', NULL, '987667cd-f38f-49ad-8385-d8e56c023234', true),
(14, 'Groningen', NULL, '109e16db-d984-4f4b-9a23-87fe0718636a', false),
(15, 'Grootegast', NULL, 'c812f63d-a7a9-49db-81b4-5ba00cda5d6b', true),
(17, 'Haren', NULL, '38d2429a-5095-4197-aec1-697e48b1552c', true),
(18, 'Hoogezand-Sappemeer', NULL, '998f2f43-c434-42e7-a72b-214f7dc83eff', true),
(22, 'Leek', NULL, 'c0eebf0c-ccf7-4900-9968-6b43bfeed25a', true),
(24, 'Loppersum', NULL, '5039632c-3330-4acf-9b9b-4f4c44f561f8', true),
(25, 'Marum', NULL, 'd84f6efe-3705-479d-bd0d-c5425466f88a', true),
(34, 'Almere', NULL, '17ceec51-4d87-4ac0-ac8e-29eeeda9e5d9', false),
(37, 'Stadskanaal', NULL, '7bba996b-b884-4e36-a7e6-96986416c320', false),
(39, 'Scheemda', NULL, '4e444792-9fc2-4750-8e37-730011f985de', true),
(40, 'Slochteren', NULL, '0e27c360-d2b1-4d7c-a0dc-a7a761046c46', true),
(47, 'Veendam', NULL, '3db65516-da22-40a8-8e45-003cbc1f74bf', false),
(48, 'Vlagtwedde', NULL, '25ed5668-3787-413c-a788-50685f69f64d', true),
(50, 'Zeewolde', NULL, '455865b0-a817-491a-a5f6-d9d6cb3a96e2', false),
(51, 'Skarsterlân', NULL, '4db9aa8c-4167-4bd2-b4f8-f044bffbc063', true),
(52, 'Winschoten', NULL, 'e84c5221-7385-4b21-91d2-df82cbff34bf', true),
(53, 'Winsum', NULL, 'dacc2f85-dd19-40eb-bd30-11c7b75a3625', true),
(55, 'Boarnsterhim', NULL, 'd36988b5-9853-4315-876a-9c5644d0b32c', true),
(56, 'Zuidhorn', NULL, '60cbae0c-de29-4646-b6de-ceb8be152bba', true),
(58, 'Dongeradeel', NULL, '903a98f7-d4b4-4314-830a-0b6941a58608', true),
(59, 'Achtkarspelen', NULL, '04333305-e4c0-4ff1-b316-eb4e31569dbd', false),
(60, 'Ameland', NULL, '38236167-f86f-4aad-aec2-5258be11729a', false),
(63, 'het Bildt', NULL, '294196e0-fb9c-436f-acd8-98d6c90f1298', true),
(64, 'Bolsward', NULL, 'a5e661ab-4eac-4221-bd35-ea8d51a3cffa', true),
(70, 'Franekeradeel', NULL, '1b3d05ff-24bf-47c5-abea-7e6ae3f63284', true),
(72, 'Harlingen', NULL, '0dc4eb94-53ff-4a6c-9687-7eb16da1d4cb', false),
(74, 'Heerenveen', NULL, '93707cf4-10f9-4564-af90-0d0b17a7f02e', false),
(79, 'Kollumerland en Nieuwkruisland', NULL, 'f44c90c4-2faf-485b-bf60-63cc4e920462', true),
(80, 'Leeuwarden', NULL, '61ca9ea2-43c6-409b-8828-f643534de85c', false),
(81, 'Leeuwarderadeel', NULL, '23e6e34e-70f4-4ee1-8b0b-263f7af79933', true),
(82, 'Lemsterland', NULL, 'fde85358-7b39-44cb-b30a-719f9ab4625b', true),
(83, 'Menaldumadeel', NULL, '5c3771d9-239c-4dfc-a317-06f7acfe7933', true),
(85, 'Ooststellingwerf', NULL, '5e70afbe-b617-4ed5-89d3-f9ad9f80cd90', false),
(86, 'Opsterland', NULL, '0429b2b6-8e4d-4102-837a-05d9b3bde6fe', false),
(88, 'Schiermonnikoog', NULL, 'ac482447-79dd-492b-8f85-c3a497c147e3', false),
(90, 'Smallingerland', NULL, '09c3c4a4-2121-4f8b-97a8-c7bb1aa5262e', false),
(91, 'Sneek', NULL, '60c0021f-ec93-414e-a7db-5267eda7e6ed', true),
(93, 'Terschelling', NULL, '6c5ebe67-e7b9-41b5-bf13-e37f43ae63e8', false),
(96, 'Vlieland', NULL, '464ca01d-d868-4bed-a1ee-1c9b37362876', false),
(98, 'Weststellingwerf', NULL, '0101b3cb-2411-49c0-a282-e2e048776efe', false),
(104, 'Nijefurd', NULL, '2dc89308-e768-4d80-9287-252303b793b8', true),
(106, 'Assen', NULL, 'ed386208-789a-4f1b-a0c6-9d4260f890fe', false),
(109, 'Coevorden', NULL, '64abd9cb-fa26-46d8-8fc5-d61f4ea4476f', false),
(114, 'Emmen', NULL, 'd4c29c6f-35ca-4c35-a7de-5fda7a1adc51', false),
(118, 'Hoogeveen', NULL, '9258bd87-99ef-4ea0-aae9-23aad1898554', false),
(119, 'Meppel', NULL, 'b61a52c4-7c52-44c3-984d-f7fb3298e9c6', false),
(140, 'Littenseradiel', NULL, '71cc8847-0a46-4e99-9400-b07ab49cc850', true),
(141, 'Almelo', NULL, 'd34852f9-2cec-4a3e-85c1-f27d12cd994a', false),
(147, 'Borne', NULL, '7434dfbb-2c2b-42b4-9ace-0b18822c350d', false),
(148, 'Dalfsen', NULL, 'b7c063b6-7cb4-4d0c-ae54-090bd8b85e82', false),
(150, 'Deventer', NULL, 'd02401c0-475d-4dc2-ab29-a1e0cacc33ed', false),
(153, 'Enschede', NULL, '10f1b5da-a44b-45a2-b675-80efcdae1892', false),
(158, 'Haaksbergen', NULL, 'b52804a0-7239-4507-9c94-27d33680bb5f', false),
(160, 'Hardenberg', NULL, '33bb37e6-303b-42d6-b602-b748d8e10645', false),
(163, 'Hellendoorn', NULL, '98d3a574-ff92-4f6e-ac77-f940eb2192f5', false),
(164, 'Hengelo', NULL, '501912d8-d21b-4a21-bf18-f44f6bcadb24', false),
(166, 'Kampen', NULL, '6dcea5f9-2048-406f-94a5-3a330a347703', false),
(168, 'Losser', NULL, '7063ae7d-b874-40cd-b217-ba795882fd67', false),
(171, 'Noordoostpolder', NULL, '3e01f991-6560-4489-bdc4-804ef2610c59', false),
(173, 'Oldenzaal', NULL, '889de30f-9f6e-4afe-9615-b641fbc11a26', false),
(175, 'Ommen', NULL, '9f5ae936-33cf-4e3b-829d-1e39abcacd56', false),
(177, 'Raalte', NULL, 'f292f45e-c732-4c26-90fc-bbb1359a722a', false),
(180, 'Staphorst', NULL, '7b7fe3e9-98b7-4259-9077-61fe1dfb100a', false),
(183, 'Tubbergen', NULL, '5a7b179b-38d5-40f9-ad19-a3d01349374a', false),
(184, 'Urk', NULL, 'a6aa39b8-e9b5-4e9f-aee2-4a582216d16a', false),
(189, 'Wierden', NULL, 'ef51400c-436a-47f3-8cac-18dc780df97a', false),
(193, 'Zwolle', NULL, '259dfcd5-3f21-417f-b2d6-51ffdf8538cd', false),
(196, 'Rijnwaarden', NULL, '90a40fa9-0c9d-44d2-a5b5-05a62e566913', true),
(197, 'Aalten', NULL, 'e28937f2-3c4a-4526-9175-61e16bb533d9', false),
(200, 'Apeldoorn', NULL, 'fe8dd89e-7145-4f13-933d-8b0ff09ef146', false),
(202, 'Arnhem', NULL, 'd35b15b9-3f3f-40ef-bdcd-9634f41bff84', false),
(203, 'Barneveld', NULL, '0fbb9c60-907d-46fa-82a5-ee36466610ff', false),
(209, 'Beuningen', NULL, '89b378e6-e3f6-49f1-90f3-6a6d86881d86', false),
(213, 'Brummen', NULL, '8161d3d1-e2ed-4369-a4de-2cc00212046e', false),
(214, 'Buren', NULL, '3cc70992-0b36-4c64-bfdf-7321ef1a82f4', false),
(216, 'Culemborg', NULL, 'db1299c8-acad-417a-bae7-a437d0470a19', false),
(221, 'Doesburg', NULL, '822b4dd1-1699-4dcc-8624-92083280c9fb', false),
(222, 'Doetinchem', NULL, '13a6ef8f-ec49-40bd-8d56-5adb0d213f50', false),
(225, 'Druten', NULL, 'b132892d-97e2-44b1-b71c-963e3f102f25', false),
(226, 'Duiven', NULL, '875f6c86-4e38-4179-9d36-71dc68977d4a', false),
(228, 'Ede', NULL, '29b1cc3f-ef72-4afd-97b9-4faff745db3b', false),
(230, 'Elburg', NULL, '66e73e10-de81-4b6b-a527-0fd651644443', false),
(232, 'Epe', NULL, '23b7a4bb-21d4-4d58-b004-4a67fa8ed167', false),
(233, 'Ermelo', NULL, 'f6b36933-7f2a-4179-95fc-3066ae3b958f', false),
(236, 'Geldermalsen', NULL, '6a40ec8b-f64e-4d36-bf24-73d8a9b8b6e9', true),
(241, 'Groesbeek', NULL, 'abe54589-e9a4-465f-b9aa-d9e9d7891abe', true),
(243, 'Harderwijk', NULL, '946ee42b-aa6d-4e7e-991b-2b16275f300d', false),
(244, 'Hattem', NULL, '2b731ec5-e97c-4712-847b-ec575139eef8', false),
(246, 'Heerde', NULL, '444bb582-4426-4020-8bb9-9326cd49af12', false),
(252, 'Heumen', NULL, 'f7d01400-f3af-4bd3-996a-225a426ec29e', false),
(262, 'Lochem', NULL, '6ecbd429-52ca-4654-a6be-a5ec18c6ba3c', false),
(263, 'Maasdriel', NULL, 'b3f5e4e1-830b-4d7f-a079-e32e436802ba', false),
(265, 'Millingen aan de Rijn', NULL, 'e6feb3e1-8e82-4ac4-8756-81d6818d4451', true),
(267, 'Nijkerk', NULL, '973ad63a-0d66-495f-b398-9f65e80b33e2', false),
(268, 'Nijmegen', NULL, '26f44294-2301-4521-9bc5-e1f1e4072bdb', false),
(269, 'Oldebroek', NULL, 'fa1e9d61-804c-459d-8e45-5b6fadbe75c5', false),
(273, 'Putten', NULL, 'e4e1e25c-bc56-4d48-aa56-f37e5b1c467a', false),
(274, 'Renkum', NULL, '28f18caf-8aa1-4aa9-a097-dc88f3e51e0b', false),
(275, 'Rheden', NULL, 'a9820528-da10-4cc1-bc5a-bfd5ad5cbeac', false),
(277, 'Rozendaal', NULL, '3e509dd2-5d9a-4be7-b94c-2f0bcc309880', false),
(279, 'Scherpenzeel', NULL, '901aad79-8048-4c3b-85f8-b4524483e59f', false),
(281, 'Tiel', NULL, '2928c71f-0e67-488f-b052-72b2912dfe9c', false),
(282, 'Ubbergen', NULL, 'c21f6be1-944e-4ded-aae7-5804ec748915', true),
(285, 'Voorst', NULL, '0d265d53-fc98-4a6e-8584-d3d07d0f030e', false),
(289, 'Wageningen', NULL, 'e1496898-f919-4701-be2d-d67996741f67', false),
(293, 'Westervoort', NULL, '3b6704ac-fcd5-4c40-842b-f99665a4662b', false),
(294, 'Winterswijk', NULL, 'cb11f22f-d954-4537-b7c9-88603bd4f82d', false),
(296, 'Wijchen', NULL, '0ad26d1d-3711-43fa-9134-38daeae48466', false),
(297, 'Zaltbommel', NULL, '0f83e48e-ff21-421d-93cc-86da4e243555', false),
(299, 'Zevenaar', NULL, '313a3d0e-fde5-4c8c-905c-9986a746c3e6', false),
(301, 'Zutphen', NULL, '47c25787-89b3-41c0-a52e-103b48a874a9', false),
(302, 'Nunspeet', NULL, 'b439b4f8-7a22-4bc2-a914-3ef5c999499c', false),
(303, 'Dronten', NULL, 'c3272eb6-1479-4b8f-af09-948e301f68b9', false),
(304, 'Neerijnen', NULL, '3da5f7a4-27be-4219-be89-2ff428cb6bb6', true),
(305, 'Abcoude', NULL, '17a7cf54-fa09-4790-9290-a51ae34408c9', true),
(307, 'Amersfoort', NULL, 'e01f24c7-1eed-4fd2-b656-032bfac31f57', false),
(308, 'Baarn', NULL, '8b3b81aa-7aa9-4bdb-ab1b-cdf380a9da6c', false),
(310, 'De Bilt', NULL, '51deb750-89de-4aba-9e59-bd7d45f1c254', false),
(311, 'Breukelen', NULL, 'b974392a-b026-4753-b27c-5831bb77ab7c', true),
(312, 'Bunnik', NULL, 'ae518c9f-4451-4781-b78f-3f3b6e822846', false),
(313, 'Bunschoten', NULL, '8cc3d752-76a6-4580-9d8c-50a9a2c31664', false),
(317, 'Eemnes', NULL, '6537421b-aca9-44c5-b158-b3385aaf6673', false),
(321, 'Houten', NULL, 'f51f03ea-341a-447b-af55-e54cd23e5d7d', false),
(327, 'Leusden', NULL, '9b48c7e4-b3e2-4bd6-832f-b1c6d248fdfa', false),
(329, 'Loenen', NULL, '87cc0937-5313-4183-86d1-0066c9679510', true),
(331, 'Lopik', NULL, '8f7ee7e3-bbcd-42f8-88d4-08c4c09a0d1f', false),
(333, 'Maarssen', NULL, '7911eb6f-5bd9-4eb8-beb4-17bda9cd20d3', true),
(335, 'Montfoort', NULL, 'e3d13bc1-d593-44b5-8cb3-ad6bc85e7d79', false),
(339, 'Renswoude', NULL, '3087a42a-c236-43ae-bab2-f7ca216ed815', false),
(340, 'Rhenen', NULL, '12d9210e-82c9-475b-af79-ab2b3ef92bcd', false),
(342, 'Soest', NULL, '2a943b87-5bf3-4a4e-8b8a-4eb56bb7a90d', false),
(344, 'Utrecht', NULL, '0425bb80-d193-4082-8b6e-9ff954a33e50', false),
(345, 'Veenendaal', NULL, '6b01d2ef-3316-44c9-8479-022a73ac366c', false),
(351, 'Woudenberg', NULL, 'ead6ed60-3d18-45cc-914d-61c2c9203ff4', false),
(352, 'Wijk bij Duurstede', NULL, 'c1c955e1-0f90-448c-a89c-fad73d060ca6', false),
(353, 'IJsselstein', NULL, '2c55c36a-0785-4f3c-966f-a804df513d8d', false),
(355, 'Zeist', NULL, '8a5ac601-2c81-417f-b801-0591ee57b662', false),
(356, 'Nieuwegein', NULL, '8e536667-446c-4ecc-bcda-00d5a98fd5cf', false),
(358, 'Aalsmeer', NULL, '0b8187cf-7a81-4b10-98f7-f79d42f01053', false),
(361, 'Alkmaar', NULL, '698befda-698a-478b-98c3-e81a00db5b8c', false),
(362, 'Amstelveen', NULL, 'ddd0ce3c-67ab-4665-b7ab-b2eab9fd0925', false),
(363, 'Amsterdam', NULL, '0ad5aae2-a609-4a7e-b87e-3b44c7f2c663', false),
(364, 'Andijk', NULL, '35dce8cd-04dc-48e8-8982-06dcd8398609', true),
(365, 'Graft-De Rijp', NULL, '151a0d57-a7ce-4e70-8448-463ffe4c0690', true),
(366, 'Anna Paulowna', NULL, 'f757570f-3ab0-4b78-bb1e-53e58dae3d60', true),
(370, 'Beemster', NULL, '6c465ac6-55bd-4b9d-93f7-36a9e5e578de', false),
(373, 'Bergen (NH.)', NULL, '846067e9-05b5-4136-a8cf-2b2c8a0163a9', false),
(375, 'Beverwijk', NULL, '78446ef9-8079-4c4c-83db-8a7cd872fcf5', false),
(376, 'Blaricum', NULL, '723ed6d4-2f1b-4a42-bc73-67ccbb1ecce6', false),
(377, 'Bloemendaal', NULL, 'c11989a3-e8ef-4472-9376-7d08e680234e', false),
(381, 'Bussum', NULL, '64c95133-d47b-47d0-b44a-645f01437331', true),
(383, 'Castricum', NULL, 'c56c2510-6865-40b7-bfff-fe592a538f36', false),
(384, 'Diemen', NULL, 'a9e2bf1a-2421-4c2d-b6cf-88903154851d', false),
(385, 'Edam-Volendam', NULL, '08eb5815-d023-4733-84dc-0111c2ab42cb', false),
(388, 'Enkhuizen', NULL, 'e2eb572e-0cdd-47fc-8a66-34ca76157b41', false),
(392, 'Haarlem', NULL, 'c834a1a8-6d2a-46f1-be2a-988eb80ca8ea', false),
(393, 'Haarlemmerliede en Spaarnwoude', NULL, 'b51e3906-2c7f-4e10-9487-db2e6fb1e3b2', true),
(394, 'Haarlemmermeer', NULL, '68d82b04-5159-4f81-a5be-2083475e0cc4', false),
(395, 'Harenkarspel', NULL, 'd97336c4-a49f-4183-894b-da5edfd60c91', true),
(396, 'Heemskerk', NULL, 'dfccd608-bc72-4b43-8d6f-dfe53c5fee81', false),
(397, 'Heemstede', NULL, '25441e7f-b63b-43ea-aa2c-8157c1edbb3a', false),
(398, 'Heerhugowaard', NULL, 'f731c7d8-ff0d-4551-8851-ada4399afc8c', false),
(399, 'Heiloo', NULL, '587e1440-1c83-43d5-8107-71fa1ceacc22', false),
(400, 'Den Helder', NULL, '29a8d264-009c-443f-b431-d74c12a08fc9', false),
(402, 'Hilversum', NULL, '0219a31e-b399-42a7-ba4f-7734da2edb05', false),
(405, 'Hoorn', NULL, 'e87c513b-0ed2-492c-afd2-e3407a117424', false),
(406, 'Huizen', NULL, '74384372-e49d-4459-99a6-830a7a5776c0', false),
(412, 'Niedorp', NULL, '25b6f43c-1685-406c-be0b-bd06cee583c5', true),
(415, 'Landsmeer', NULL, 'd091eb7e-b1e9-464f-8633-c4fa5e1032a2', false),
(416, 'Langedijk', NULL, 'a7038427-2adb-4f04-9c52-99c353197b27', false),
(417, 'Laren', NULL, '8e9c9202-84cc-45f0-8eb3-11577152cdd6', false),
(420, 'Medemblik', NULL, 'a75de19b-fb17-4d9c-9e30-7425bc4335f0', false),
(424, 'Muiden', NULL, 'c7c3f468-1296-4721-9d70-ac64d35188c3', true),
(425, 'Naarden', NULL, 'ff14e4dd-610e-4c37-8199-28a0772e4f79', true),
(431, 'Oostzaan', NULL, 'afbd1ec8-4087-47ca-bc97-84aef54a7a13', false),
(432, 'Opmeer', NULL, 'f42e388e-dfcf-4de6-bc4f-094f67f6d734', false),
(437, 'Ouder-Amstel', NULL, 'e379661e-0c9b-45e6-aefa-52d334a25b75', false),
(439, 'Purmerend', NULL, '8aef286a-7015-46ab-a7e7-db96435698c5', false),
(441, 'Schagen', NULL, 'e8c4aaf9-d2f1-40f5-948b-72d9eca1d905', false),
(448, 'Texel', NULL, 'a837b704-0913-4fb0-847f-ec998f362b12', false),
(450, 'Uitgeest', NULL, '233ca9e5-e17e-46fd-bbe3-6dce90592616', false),
(451, 'Uithoorn', NULL, 'cc3a5dc6-c937-4c74-a0e0-387842fdc6e6', false),
(453, 'Velsen', NULL, '9070e114-e08b-417c-885b-8b2599f32436', false),
(457, 'Weesp', NULL, '6750f363-0962-4d08-9486-fb813aa458fc', false),
(458, 'Schermer', NULL, '180ea3e0-4986-46ba-bafc-507e999c799c', true),
(459, 'Wervershoof', NULL, '955f8f21-aad8-42ff-8891-f8423f5042a3', true),
(462, 'Wieringen', NULL, 'aeb5767e-659b-4160-b07d-d33f3e214355', true),
(463, 'Wieringermeer', NULL, 'a3f4be3f-5622-4e10-b150-798ee0b5391a', true),
(473, 'Zandvoort', NULL, 'abfdeaf0-9b7a-40f4-9403-5502aed0565e', false),
(476, 'Zijpe', NULL, 'ce0e7fde-db94-401d-be43-42cc2a41bc3f', true),
(478, 'Zeevang', NULL, 'd3c769cd-27f7-46d9-8abf-58107f93b1f2', true),
(479, 'Zaanstad', NULL, '2e4c23f0-3328-4214-930f-f2ac66db911a', false),
(482, 'Alblasserdam', NULL, '6ed63920-6a9f-46b4-bafd-0925480ac923', false),
(484, 'Alphen aan den Rijn', NULL, 'a46ef81d-38b8-459d-902e-57187034441b', false),
(489, 'Barendrecht', NULL, '9a4cef81-e701-4587-9c54-bd5e4387a68e', false),
(491, 'Bergambacht', NULL, 'f2e89f65-8a50-4587-a7b4-c62464e48d41', true),
(497, 'Bodegraven', NULL, '8fe05876-a042-4973-b6f1-9ef7d1541cd6', true),
(498, 'Drechterland', NULL, '9fe8c6a0-2f2c-4526-b59b-1d7985024f61', false),
(499, 'Boskoop', NULL, 'fb4dcf45-9279-4060-b34b-49d1cb21650d', true),
(501, 'Brielle', NULL, '7421af3c-880f-4418-8571-d3f39cb93c97', false),
(502, 'Capelle aan den IJssel', NULL, 'ce03f8d7-8c60-48a8-a67a-c278916e576e', false),
(503, 'Delft', NULL, 'c2def0fb-f825-481a-8a02-086d9adda546', false),
(504, 'Dirksland', NULL, 'ba6769eb-8379-4e4f-8210-ae08aa2628ec', true),
(505, 'Dordrecht', NULL, '6e06cf5b-14ad-4915-bb2b-924affc13685', false),
(511, 'Goedereede', NULL, '2f8eae40-a9f6-4cd5-8956-6a31db2a238a', true),
(512, 'Gorinchem', NULL, 'cafd931a-5200-44b2-9223-8f53908e80bb', false),
(513, 'Gouda', NULL, 'cd5085d8-f50a-41f4-ad8e-65ea86762a39', false),
(518, '''s-Gravenhage', 'Den Haag', '0c48f9ea-a528-4c61-a76e-306653334565', false),
(523, 'Hardinxveld-Giessendam', NULL, '8078a749-57bf-4f7d-9753-b60b2c80f981', false),
(530, 'Hellevoetsluis', NULL, 'c6045df2-b3a7-4fef-a40f-70b7330b4939', false),
(531, 'Hendrik-Ido-Ambacht', NULL, 'ca5ccf42-70a7-45cc-9ab1-49cd9abcea60', true),
(532, 'Stede Broec', NULL, '62f93422-b2dc-46de-bab0-2556e6ca7188', false),
(534, 'Hillegom', NULL, '3a91ff81-220f-4d46-97e1-996fac7e9c16', false),
(537, 'Katwijk', NULL, '65163c78-06f2-4fea-a3a5-38e6db2f6ff3', false),
(542, 'Krimpen aan den IJssel', NULL, '6b05ef47-2b79-4d81-a0e1-c7a204957db0', false),
(545, 'Leerdam', NULL, '77f858eb-a6ca-4e15-b585-aa1655259b41', true),
(546, 'Leiden', NULL, 'b3b98f00-b132-4848-8ff9-20f24957f3e6', false),
(547, 'Leiderdorp', NULL, '5f49a31d-0682-4dc1-aecd-d8e64e4da3ef', false),
(553, 'Lisse', NULL, 'fdd42de4-e896-483d-b52e-d1297633b409', false),
(556, 'Maassluis', NULL, '25cd9bf1-1f9a-4a9b-822d-b4f8f10de74d', false),
(559, 'Middelharnis', NULL, '8fc7028b-c1e2-4cda-a01f-c3784b593010', true),
(563, 'Moordrecht', NULL, 'ecd5191b-be80-4a93-944a-832c4ce3856f', true),
(567, 'Nieuwerkerk aan den IJssel', NULL, '4063ede9-fa7c-4cc4-8bae-3d443f7e86be', true),
(568, 'Bernisse', NULL, 'd4cef9f9-7641-434c-865f-2041405b6177', true),
(569, 'Nieuwkoop', NULL, 'b3602387-443b-488e-921d-33920d7f2897', false),
(571, 'Nieuw-Lekkerland', NULL, '3c68346f-7baf-480e-ae03-52ed9a824490', true),
(575, 'Noordwijk', NULL, '9151ab4b-669b-4cca-9334-8b8fc10e8998', false),
(576, 'Noordwijkerhout', NULL, '65476ede-5654-43d7-b99f-dc9812b953d4', true),
(579, 'Oegstgeest', NULL, '4fe3497d-de82-4d1b-b715-1e62cb9d63c4', false),
(580, 'Oostflakkee', NULL, 'fb52517f-c21c-48f3-8031-9279ade94a79', true),
(584, 'Oud-Beijerland', NULL, '007183f4-b7e8-49f0-a9e1-93c655d316d8', true),
(585, 'Binnenmaas', NULL, 'fdb7b845-7d52-41b3-bde3-31c34ed6ff32', true),
(588, 'Korendijk', NULL, '80209b34-9ba6-4b72-9e7a-e73cbe9fe211', true),
(589, 'Oudewater', NULL, '593d1050-9b22-4933-b153-5912454f8a10', false),
(590, 'Papendrecht', NULL, 'f8996c38-6660-4ae6-b1b5-429f48b79159', false),
(595, 'Reeuwijk', NULL, 'b261c8d3-42c8-47be-8c20-6129706c8c14', true),
(597, 'Ridderkerk', NULL, '77ad13d5-1e9e-491d-8fac-18e64ae0edd2', false),
(599, 'Rotterdam', NULL, '5cfba98e-69bf-4ac8-8c5f-194b935e7b0e', false),
(600, 'Rozenburg', NULL, '009852f0-9e3e-4579-8ee0-d87a9551c8ed', true),
(603, 'Rijswijk', NULL, '341fe6f2-f27a-4020-9bcb-e52858067909', false),
(606, 'Schiedam', NULL, 'd7d4bcd1-1f81-4907-9cf1-78205e7c41ff', false),
(608, 'Schoonhoven', NULL, 'e1e0cb83-f905-436b-b831-3f175af0697f', true),
(610, 'Sliedrecht', NULL, '8405e045-a005-4280-be9d-368816f7d705', false),
(611, 'Cromstrijen', NULL, '9bb51188-f5f5-43f4-afed-f301c49a1bd9', true),
(612, 'Spijkenisse', NULL, '8b16d64c-96c6-4987-a76b-23e2adc32694', true),
(613, 'Albrandswaard', NULL, 'a9d775dc-9c30-4e67-80f9-c8bf3a37ecb6', false),
(614, 'Westvoorne', NULL, '415afef4-4f65-4ae1-8657-2706dcc6937f', false),
(617, 'Strijen', NULL, '96e8dfba-b748-48e7-8b16-250fc50714bf', true),
(620, 'Vianen', NULL, 'd62ad226-1b51-4941-b2a1-152a67c9f42b', true),
(622, 'Vlaardingen', NULL, '3e9c2b5d-c84a-4686-b174-15474460b953', false),
(623, 'Vlist', NULL, 'aa90d2bf-d7d1-450d-a561-ba5f3f6c7858', true),
(626, 'Voorschoten', NULL, 'f5d9c20c-78b7-4f57-bb2f-aaa643b98cc5', false),
(627, 'Waddinxveen', NULL, 'd5772984-eaec-40cf-8919-4010f593e434', false),
(629, 'Wassenaar', NULL, '802e4785-6e0b-4d1f-b48b-930b08257dfc', false),
(632, 'Woerden', NULL, 'bfdfbcc2-7740-4924-9fa8-04b4dc171bf2', false),
(637, 'Zoetermeer', NULL, '148f3522-3b65-492f-99a4-08d986a5221b', false),
(638, 'Zoeterwoude', NULL, '98d966ab-afd0-4ee1-b173-1f574b06a10b', false),
(642, 'Zwijndrecht', NULL, 'b9671f2c-231c-48da-ac4e-bb92b2457810', false),
(643, 'Nederlek', NULL, 'a95c9a13-d2d1-48a3-877f-5a92e03fb8b0', true),
(644, 'Ouderkerk', NULL, '4a4b2cfe-8dea-4cdf-a48d-fb3a612586c1', true),
(653, 'Gaasterlân-Sleat', NULL, '6a951aae-0027-43f5-a211-cc4013592ed8', true),
(654, 'Borsele', NULL, '9824623a-f700-4319-aea5-c353655b23e3', false),
(664, 'Goes', NULL, '15592935-c639-4e53-b49f-e52bfd8fe3f4', false),
(668, 'West Maas en Waal', NULL, 'd99bbefd-d989-4b60-8ae8-87a2fc718f44', false),
(677, 'Hulst', NULL, '3ade6e87-aab5-4418-8438-107cc160ce28', false),
(678, 'Kapelle', NULL, '3f74552d-e7b5-4ed0-9b4d-a255bdf2853e', false),
(683, 'Wymbritseradiel', NULL, '6488d30f-4db4-4bb9-83b4-50180bfec315', true),
(687, 'Middelburg', NULL, '80a55b1c-1dda-4565-b916-520d823cc64d', false),
(689, 'Giessenlanden', NULL, '87da38ba-75f3-41de-bac2-321bae23316e', true),
(693, 'Graafstroom', NULL, 'cfc9b87c-4768-4a0c-bac1-c59eeb498248', true),
(694, 'Liesveld', NULL, '66b3556c-1787-410c-a6b2-f7aa6186b340', true),
(703, 'Reimerswaal', NULL, 'd111d90b-c374-465e-b2ee-f314686b387b', false),
(707, 'Zederik', NULL, 'd422524d-72c5-4b2b-afb1-e85e433c5911', true),
(710, 'Wûnseradiel', NULL, '819ed43e-e504-4647-9a52-c854ea3dd8b1', true),
(715, 'Terneuzen', NULL, '6d2448c2-65df-481b-b69c-c8a42c6cc53f', false),
(716, 'Tholen', NULL, 'e8184c8e-fcc9-4630-b11e-b787640a29cd', false),
(717, 'Veere', NULL, '6187d65b-e09c-4756-8f01-9c915bbd5518', false),
(718, 'Vlissingen', NULL, '9bea9e08-79db-40ac-86bb-e5e538ca9094', false),
(733, 'Lingewaal', NULL, '57349338-26d8-4ec0-8243-26f1d7e1d422', true),
(736, 'De Ronde Venen', NULL, 'd24f71e4-2955-4a2a-bd25-be21591c80dd', false),
(737, 'Tytsjerksteradiel', NULL, 'eea75c09-0fe8-417a-b66a-d98b4b005a78', false),
(738, 'Aalburg', NULL, 'a9505342-0efe-480a-8935-8466eeb7c5ef', true),
(743, 'Asten', NULL, 'ef45785b-0e93-49b9-9b10-21f3078f27c3', false),
(744, 'Baarle-Nassau', NULL, '2df0fee1-d44b-4a48-9e53-bd236c8558a2', false),
(748, 'Bergen op Zoom', NULL, '5d884613-3e08-460a-9ebf-09315e890817', false),
(753, 'Best', NULL, '9589e10b-2864-48ad-8f02-6b8585c88832', false),
(755, 'Boekel', NULL, 'f757909a-4090-4cc6-b654-c7bcaf6be2be', false),
(756, 'Boxmeer', NULL, 'ae90e90a-87dc-4c2b-bd33-cd848a08c9c3', false),
(757, 'Boxtel', NULL, 'fe1ffe66-7e82-4a27-bcce-7f5beaec1b13', false),
(758, 'Breda', NULL, '2c29dd15-db81-4866-8a44-fea415ceb76f', false),
(762, 'Deurne', NULL, '2d3d9a83-1736-4b50-a172-cf900a39bbab', false),
(765, 'Pekela', NULL, 'd0290a9f-f269-4322-8a42-bbfae06b0536', false),
(766, 'Dongen', NULL, '50acfeb5-9f16-4de4-bf79-610b3570e2a5', false),
(770, 'Eersel', NULL, '569f660a-169d-4a05-b257-c5209a3f65dd', false),
(1979, 'Eemsdelta', NULL, '2e694b7e-cc66-48d6-bdb0-4c7e5a33c7cb', false),
(772, 'Eindhoven', NULL, 'a3efff87-36fb-4845-aeb4-a4eb81bf2158', false),
(777, 'Etten-Leur', NULL, '1655e5ac-9eb2-4f7e-8dce-60d372bf16e4', false),
(779, 'Geertruidenberg', NULL, 'dcd3a5d5-4aab-440f-b303-920fe885e4ff', false),
(784, 'Gilze en Rijen', NULL, '23ea6063-ef7f-4aa7-acfd-9f9706115a06', false),
(785, 'Goirle', NULL, '8bf37214-8fa4-40f2-874e-2a18cacf989d', false),
(786, 'Grave', NULL, 'fa6d7583-f21a-401d-980a-85c460b5f4ee', false),
(788, 'Haaren', NULL, '11208a44-9d46-4761-b721-a22b869c12d3', true),
(794, 'Helmond', NULL, '6f611717-9d9e-442a-8ab6-53c410134c3f', false),
(796, '''s-Hertogenbosch', 'Den Bosch', '51f2d486-a443-4e55-b43a-09dc8f305633', false),
(797, 'Heusden', NULL, '0fb684b2-b729-4fdf-9fda-b8fc2c0abc22', false),
(798, 'Hilvarenbeek', NULL, 'df1f0d37-4cac-4e3e-96b9-6570e5efbf96', false),
(808, 'Lith', NULL, '47beedaf-8132-48af-83f3-2f8301afdb1e', true),
(809, 'Loon op Zand', NULL, '04fe984f-9629-400e-8e23-05f25f9e5402', false),
(815, 'Mill en Sint Hubert', NULL, '790e66b5-8a8d-4934-b1ac-f3a8b48bf40c', false),
(820, 'Nuenen, Gerwen en Nederwetten', NULL, '4ddf4ac1-c6d3-477b-ae3d-7262a397c9a5', false),
(823, 'Oirschot', NULL, '31ba5cd6-53cd-45c4-8dd6-e7bb1e8286a9', false),
(824, 'Oisterwijk', NULL, '7bdb09b4-4f8f-4043-a9cf-05cab828ba96', false),
(826, 'Oosterhout', NULL, '1165f0c4-c7fd-4627-9308-5c4fc55d0170', false),
(828, 'Oss', NULL, 'baf3a2d0-d78a-49dc-9b85-d4d7230b53ef', false),
(840, 'Rucphen', NULL, 'e37ecc95-70c4-4a78-bb91-1605c689b19b', false),
(844, 'Schijndel', NULL, 'e519e259-2200-49ac-8a7b-b8c7264b6d01', true),
(845, 'Sint-Michielsgestel', NULL, 'bb896dee-15cb-4f5a-a15b-442b866a726e', false),
(846, 'Sint-Oedenrode', NULL, '59b5a32f-2495-4fee-aa68-fc9fa4602c0c', true),
(847, 'Someren', NULL, '2b767bf2-04cb-4894-9d5e-e8a43020e36c', false),
(848, 'Son en Breugel', NULL, '8e6ebdf3-8525-4851-b90b-605be3be4959', false),
(851, 'Steenbergen', NULL, '7fdf92ad-1bed-43c6-a0f4-1aa987324132', false),
(852, 'Waterland', NULL, 'd0789349-ee66-4608-8a5b-dfa47dd20ca3', false),
(855, 'Tilburg', NULL, 'b46f50a9-b3a2-4864-8b62-e857fa826f60', false),
(856, 'Uden', NULL, '5f647cf2-85fa-4fb7-8f6c-a090784d8555', false),
(858, 'Valkenswaard', NULL, '80bd5746-4499-4665-a9f0-a4c56318e825', false),
(860, 'Veghel', NULL, '6ba49c47-10cf-4737-837e-521913be92d3', true),
(861, 'Veldhoven', NULL, 'c3503a95-8be2-4dae-bc14-277b212c5d87', false),
(865, 'Vught', NULL, 'd0684e7d-c051-4f90-ad63-e288a826160a', false),
(866, 'Waalre', NULL, '8515998e-a5bc-4faa-bf42-215ca6c1ceec', false),
(867, 'Waalwijk', NULL, '4040f3dc-0b45-4025-a3a2-b5678f48a7b9', false),
(870, 'Werkendam', NULL, 'e8a32fd6-686c-4461-85e2-94672f25c2e5', true),
(873, 'Woensdrecht', NULL, '3dae1ad0-366a-4960-b951-f2077d9222a9', false),
(874, 'Woudrichem', NULL, '5a0cea87-394e-41a5-8d52-0cecba1f787d', true),
(879, 'Zundert', NULL, 'bc5fd894-0e45-442a-a331-fff56ae5b727', false),
(880, 'Wormerland', NULL, 'b7023a50-19ea-46ca-a198-9cb29b104d5a', false),
(881, 'Onderbanken', NULL, '0b9a1010-ebdb-4a88-a912-aae361fc1c31', true),
(882, 'Landgraaf', NULL, '0a385dbc-e22a-4c1b-9d51-ffdb28325c5a', false),
(885, 'Arcen en Velden', NULL, 'f1563a97-c35f-4f99-ac5d-edd7d9152dc5', false),
(888, 'Beek', NULL, 'f92adb54-e7f2-459a-95b3-a07f40deda2f', false),
(889, 'Beesel', NULL, '87b8bee2-3d21-412c-965c-684beb8dcb8f', false),
(893, 'Bergen (L.)', NULL, 'b0b96a6d-28bc-432e-aea8-c4ba209b8432', false),
(899, 'Brunssum', NULL, '7052ca24-ca2f-4ff3-a899-b246080e6401', false),
(905, 'Eijsden', NULL, 'ca65e142-fb41-4a45-9358-aa6df86581b2', true),
(907, 'Gennep', NULL, '4a42bcf5-cebe-4c7f-948b-e22d7ec48545', false),
(917, 'Heerlen', NULL, '385394d9-f003-472f-aa68-668336e5cb1b', false),
(918, 'Helden', NULL, 'b68d92bf-06d9-47e6-a222-85e4b1cc503e', true),
(928, 'Kerkrade', NULL, '7adc4ec3-b3b4-4138-9aaf-e1984157be90', false),
(929, 'Kessel', NULL, 'da4a8d06-f9cb-4409-aa19-ecfc5493c620', true),
(934, 'Maasbree', NULL, 'eb511d2f-b386-4298-90cf-0ddf11df24d3', true),
(935, 'Maastricht', NULL, 'c53f2ddf-c2e5-4b0a-ba8f-73efdce14836', false),
(936, 'Margraten', NULL, '6636f9de-1a82-48a4-984b-011cf807fdbb', true),
(938, 'Meerssen', NULL, '9e58ff19-7145-4b8e-85ed-185e7161e4c9', false),
(941, 'Meijel', NULL, '5f80bbf2-6a2c-4bff-b084-c8e22be843b4', true),
(944, 'Mook en Middelaar', NULL, '709b3ad6-18c5-4ad6-9073-207183b49532', false),
(946, 'Nederweert', NULL, 'f26a2ba9-64a4-4b35-80c2-1a8f0cc2e325', false),
(951, 'Nuth', NULL, '5b260f6a-1ca3-480a-98ae-60cb63b2031f', true),
(957, 'Roermond', NULL, '6b61d113-7b5c-46f5-9f9b-3027883c9fec', false),
(962, 'Schinnen', NULL, 'a22bfd75-ad84-425c-b0a4-afe83ccbf036', true),
(964, 'Sevenum', NULL, '574e31de-3bec-474b-b4fa-e16e36beae45', true),
(965, 'Simpelveld', NULL, '02a718c6-ba48-425a-a401-e0141c2da6c9', false),
(971, 'Stein', NULL, '1259fc3d-dd12-4cac-aa59-a3c59b176db8', false),
(981, 'Vaals', NULL, '066b779b-567f-4f80-bd7a-e69d06cbdbcf', false),
(983, 'Venlo', NULL, 'a366c12e-4d87-49c4-8f77-b97dd6a70fe4', false),
(984, 'Venray', NULL, '23022112-49e0-4750-9047-d23f973256c1', false),
(986, 'Voerendaal', NULL, 'd09dc573-04ac-4b80-8961-03a060f670b3', false),
(988, 'Weert', NULL, '3b493f4a-abfe-4f71-b279-f26a8eda791d', false),
(993, 'Meerlo-Wanssum', NULL, 'bebbd6d2-c29b-476f-8073-c55c1d4aa4f4', true),
(994, 'Valkenburg aan de Geul', NULL, 'a6d3fa57-d49a-4f90-b847-513c7a07191c', false),
(995, 'Lelystad', NULL, '872c0f65-25a4-4a45-98b7-2bd6ca69e3e5', false),
(1507, 'Horst aan de Maas', NULL, '91a02378-7cee-42eb-9ff8-8e5241f0c4b8', false),
(1509, 'Oude IJsselstreek', NULL, 'e4e8df47-8f4b-4143-a278-142d0d62fa86', false),
(1525, 'Teylingen', NULL, '8d9ada86-f4c2-4ff6-8cb3-74cc46a0e98d', false),
(1581, 'Utrechtse Heuvelrug', NULL, '749c54df-d2a3-4850-ae01-22f35c0fd249', false),
(1586, 'Oost Gelre', NULL, 'c686fbf6-c777-44f7-b751-9ca0e6cb1589', false),
(1598, 'Koggenland', NULL, 'd2eb46d7-73f4-417e-959a-85c1becb6550', false),
(1621, 'Lansingerland', NULL, '75a8bc60-4847-4071-bad7-6078f4b1e801', false),
(1640, 'Leudal', NULL, '49a3c925-960c-4921-8817-96f4292e35b8', false),
(1641, 'Maasgouw', NULL, 'b84d85b5-92c6-4f3a-858c-aaf79ccf2416', false),
(1651, 'Eemsmond', NULL, 'a52b5eaf-4210-40c1-a098-6457e6cbb1d8', true),
(1652, 'Gemert-Bakel', NULL, 'ea91138b-681c-4cd9-9d7c-84c9d5c9c652', false),
(1655, 'Halderberge', NULL, 'dad265ec-8beb-4b5f-b25a-17fe7a04c9f3', false),
(1658, 'Heeze-Leende', NULL, '8bdff903-ffba-438e-82e3-337feb22d183', false),
(1659, 'Laarbeek', NULL, '78944c52-becd-4615-be71-0df792c77374', false),
(1661, 'Reiderland', NULL, '12148dba-0c0b-4ef7-8b5d-9f20989f26a5', true),
(1663, 'De Marne', NULL, 'e1483840-82a8-4dda-a66b-2a696f171181', true),
(1666, 'Zevenhuizen-Moerkapelle', NULL, '42039b7e-b468-440f-9869-0704d8f72cc4', true),
(1667, 'Reusel-De Mierden', NULL, '59bb51a2-7eb7-4e8f-8aa0-15e9099811c9', false),
(1669, 'Roerdalen', NULL, '831d14b0-76b5-41d7-958b-97e2509b41dd', false),
(1671, 'Maasdonk', NULL, 'e4bf5a34-9a61-4cc2-af9b-ba4f471e8cf4', true),
(1672, 'Rijnwoude', NULL, '2d3127ff-6f89-4db5-8fed-eb681b588010', true),
(1674, 'Roosendaal', NULL, 'a5a02f86-fb0e-4ede-ba7a-a7f2df13d56c', false),
(1676, 'Schouwen-Duiveland', NULL, '4ddb6441-4461-4b56-87e8-720f696c171d', false),
(1680, 'Aa en Hunze', NULL, 'b5f083d4-c9d9-4f18-a165-51bed73eb5b5', false),
(1681, 'Borger-Odoorn', NULL, 'f75be851-74c7-4ded-975d-7ab4e795d7f8', false),
(1684, 'Cuijk', NULL, '8d2773f4-a3f3-424b-9789-54e0282abbfd', false),
(1685, 'Landerd', NULL, 'ce61e258-b319-4cf6-b892-8663a1870024', false),
(1690, 'De Wolden', NULL, 'a8b1c1e0-5782-4dbd-9a46-6f55c0b6a809', false),
(1695, 'Noord-Beveland', NULL, '70dc9728-7fd6-4f21-a00b-684da3990a40', false),
(1696, 'Wijdemeren', NULL, 'ae9fda03-87df-43d4-9fbb-8cce5341eaba', false),
(1699, 'Noordenveld', NULL, '2fcaf178-f421-4ce6-a5ba-f541c55c2a7c', false),
(1700, 'Twenterand', NULL, '012197bc-9dce-4d1b-a932-e7e24fc9dd58', false),
(1701, 'Westerveld', NULL, 'b8568581-018b-48a1-9c30-871aa3b65cd0', false),
(1702, 'Sint Anthonis', NULL, 'bb53f60b-5ffa-4f10-b30b-e63ad02d82aa', false),
(1705, 'Lingewaard', NULL, 'b5fc725f-7147-4324-b846-f7ade6ff0c60', false),
(1706, 'Cranendonck', NULL, '42bd3c3d-055e-4487-8802-8d5cb8722cac', false),
(1708, 'Steenwijkerland', NULL, 'b65b4285-c898-45f4-9cd8-b0e85f5100df', false),
(1709, 'Moerdijk', NULL, '0167c404-ab3a-4239-97b5-5dd772098345', false),
(1711, 'Echt-Susteren', NULL, 'c1c9a6a6-56cb-4686-9715-71b9d37c8c38', false),
(1714, 'Sluis', NULL, '50b9ca62-ba02-47ad-b2d2-1fde068aaa32', false),
(1719, 'Drimmelen', NULL, 'd4861114-e193-4a95-9d1b-028cd1540e9e', false),
(1721, 'Bernheze', NULL, '44158269-694d-4145-aca3-011ecd5bcc96', false),
(1722, 'Ferwerderadiel', NULL, 'edff92ed-e242-4544-b99b-6906147a9fa9', true),
(1723, 'Alphen-Chaam', NULL, '977708a2-d798-4974-b7dc-900a82d87f2c', false),
(1724, 'Bergeijk', NULL, 'c4dedfb1-c3c7-4427-a024-4def2091fd2d', false),
(1728, 'Bladel', NULL, 'bd0831f5-bcd5-46ef-befc-7242e12753d6', false),
(1729, 'Gulpen-Wittem', NULL, '22d2ad45-c29a-455c-908b-c83ba9db8310', false),
(1730, 'Tynaarlo', NULL, '302ef79e-41aa-406c-ab13-73712154a6fa', false),
(1731, 'Midden-Drenthe', NULL, '8a89e60c-d874-458f-bd75-1175105ee5c8', false),
(1734, 'Overbetuwe', NULL, 'debbe4de-3b3c-4c00-aa24-2963b0eb3cf3', false),
(1735, 'Hof van Twente', NULL, '4f91209c-5974-47cb-88a5-4d93137604cc', false),
(1740, 'Neder-Betuwe', NULL, '4fea7a0c-18af-4895-b2c7-d195a553b41d', false),
(1742, 'Rijssen-Holten', NULL, '1e8efe2e-953b-427d-ae98-c5a8d58e9391', false),
(1771, 'Geldrop-Mierlo', NULL, 'c96ef787-2263-4671-957e-586cf3db3db3', false),
(1773, 'Olst-Wijhe', NULL, 'c7de1e8d-9eef-49c0-a1a3-5cd955fc5a8f', false),
(1774, 'Dinkelland', NULL, 'e5b51d51-b69e-4308-92b7-a8306813426a', false),
(1783, 'Westland', NULL, 'eb62c259-8700-4ed6-9d4c-53f452ad7a94', false),
(1842, 'Midden-Delfland', NULL, 'ef958472-5820-4fab-b56b-9e04abae240b', false),
(1859, 'Berkelland', NULL, 'cc3f1846-e3be-4ecc-ac85-efc7fdceb1f3', false),
(1876, 'Bronckhorst', NULL, 'ef471ee9-7b22-4271-b148-1c52b4630ea8', false),
(1883, 'Sittard-Geleen', NULL, '49f6715a-49cd-48c0-b3b6-73f2e9cbe327', false),
(1884, 'Kaag en Braassem', NULL, 'dc331066-029a-4a64-be15-d513bd136876', false),
(1891, 'Dantumadiel', NULL, 'f04e2d6f-e181-485d-af97-08edecca6b4d', false),
(1892, 'Zuidplas', NULL, '95e5f9a3-550c-4657-902c-f2f9f1fe6211', false),
(1894, 'Peel en Maas', NULL, 'de18c0b4-96bf-4abf-9fcc-a9358b0fc902', false),
(1895, 'Oldambt', NULL, '380eedfc-59f0-4c37-b7e0-021b559ab0ae', false),
(1896, 'Zwartewaterland', NULL, '8506c310-b71b-45ed-be2f-48524593afb6', false),
(1900, 'Súdwest-Fryslân', NULL, '7cfdec38-b8cb-405f-9053-318fbce5ec12', false),
(1901, 'Bodegraven-Reeuwijk', NULL, '7fcc65f2-d98f-40e2-9d67-773abfcf9c12', false),
(1903, 'Eijsden-Margraten', NULL, '56e7366d-8009-41cb-84d2-bf4bec7b988a', false),
(1904, 'Stichtse Vecht', NULL, 'a9e96779-5c02-497a-9d34-98e8df2939c4', false),
(1908, 'Menameradiel', NULL, 'e2934162-dd43-4294-80f7-6ac0e2c220c2', true),
(1911, 'Hollands Kroon', NULL, '57820002-5119-4bef-ab79-185fd022fad2', false),
(1916, 'Leidschendam-Voorburg', NULL, '1ae14241-893f-4182-bbfa-7796f1011fca', false),
(1924, 'Goeree-Overflakkee', NULL, '6dc42793-60e2-43ac-bcdc-1f737c2087b9', false),
(1926, 'Pijnacker-Nootdorp', NULL, '27469b04-c6f4-466e-a1ee-00982788e741', false),
(1927, 'Molenwaard', NULL, '7f19a8ad-f86c-4752-9df7-e545b354781a', true),
(1930, 'Nissewaard', NULL, '3a7e8a5b-dad1-465a-9a1c-1be6a87572a5', false),
(1931, 'Krimpenerwaard', NULL, '06547322-c619-4d6e-8c3c-5bb41dd75107', false),
(1940, 'De Fryske Marren', NULL, '3ac6da20-2b5f-4698-b63a-811044478250', false),
(1942, 'Gooise Meren', NULL, '3726385e-6a5b-49de-ac6e-e2692e88c2f5', false),
(1945, 'Berg en Dal', NULL, 'e857f0f5-c464-45e3-b448-4bb088b2f22d', false),
(1948, 'Meierijstad', NULL, '3a985cae-26c4-45d1-98ec-6910ebb310b4', false),
(1949, 'Waadhoeke', NULL, '54f983ea-c843-4491-a10e-e4f1de353271', false),
(1950, 'Westerwolde', NULL, '06ad4cd3-b422-44ab-8142-4f8d5beeadf8', false),
(1952, 'Midden-Groningen', NULL, 'd2535d71-2571-4279-b06c-1046a7b9c0e8', false),
(1954, 'Beekdaelen', NULL, '0a10191e-7cb4-4953-b03d-01511cc56303', false),
(1955, 'Montferland', NULL, 'f25fe4c5-556f-443f-97ab-a541743367b6', false),
(1959, 'Altena', NULL, '8a193685-0ac1-4e7c-a3c6-b75973a0db2c', false),
(1960, 'West Betuwe', NULL, '77282143-19cd-4aaa-9304-97b16abb7ff2', false),
(1961, 'Vijfheerenlanden', NULL, '3e9a0754-cb85-418a-80b0-9ca9c2e7980e', false),
(1963, 'Hoeksche Waard', NULL, '8f6bf683-9da8-4bd0-b645-386617752e9f', false),
(1966, 'Het Hogeland', NULL, 'f2099bb7-49de-4c32-8e22-0020c95500b0', false),
(1969, 'Westerkwartier', NULL, 'c6f02ed0-1085-4704-a482-1fec6629b035', false),
(1970, 'Noardeast-Fryslân', NULL, '5a8d21fd-260a-4899-a6f8-5e47e4bb2f3b', false),
(1978, 'Molenlanden', NULL, 'ac67a8a2-a4d8-4b56-866c-8cc40826cbcb', false),
(1987, 'Menterwolde', NULL, 'e055230a-8a06-4e7c-9f58-0594b3d38fe1', true);

COMMIT;

BEGIN;
    
    WITH relations AS (
        SELECT case_id_a AS continuation, case_id_b AS initiator FROM case_relation WHERE type_b = 'initiator'
        UNION
        SELECT case_id_b AS continuation, case_id_a AS initiator FROM case_relation WHERE type_a = 'initiator')
    UPDATE zaak z SET vervolg_van = r.initiator FROM relations r WHERE r.continuation = z.id;

COMMIT;

BEGIN;

CREATE OR REPLACE FUNCTION get_messages_as_json(zaak_statusid int)
RETURNS table(j json)
IMMUTABLE
LANGUAGE plpgsql
AS $$
declare
begin
    return QUERY select json_agg(messages) from (
select bn.uuid as catalog_message_uuid, 
       bn.label as catalog_message_label,
       zn.rcpt as receiver,
       zn.cc as cc,
       zn.bcc as bcc,
       text_to_bool(zn.automatic::text) as send_automatic, -- 1 or null
       zn.email as to,
       zn.betrokkene_role as person_involved_role,
       legacy_subject_id_as_json(zn.behandelaar) as subject
       from zaaktype_notificatie zn join bibliotheek_notificaties bn on zn.bibliotheek_notificaties_id = bn.id
       where zn.zaak_status_id = zaak_statusid
       order by zn.id) messages;
END;
$$;


CREATE OR REPLACE FUNCTION get_phases_as_json(zaaktype_nodeid int)
RETURNS table(j json)
IMMUTABLE
LANGUAGE plpgsql
AS $$
declare
begin
    return QUERY select json_agg(phases) from (select zts.status as phase_id,
        zts.naam as milestone_name,
        zts.fase as phase_name,
        zts.created,
        zts.last_modified,
        zts.termijn as term_in_days,
        case when g.uuid is null or r.uuid is null then null else
        json_build_object(
            'enabled', case when zts.status = 1 then true else text_to_bool(zts.role_set::text) end,
            'department_uuid', g.uuid,
            'department_name', g.name,
            'role_uuid', r.uuid,
            'role_name', r.name
        ) end as assignment,
        get_kenmerken_for_phase_as_json(zts.id) as custom_fields,
        get_results_as_json(zts.id) as results,
        get_tasks_as_json(zts.id) as tasks,
        get_templates_as_json(zts.id) as templates,
        get_messages_as_json(zts.id) as email_templates
        from zaaktype_status zts
        left join roles r on r.id = zts.role_id
        left join groups g on g.id = zts.ou_id
        where zts.zaaktype_node_id = zaaktype_nodeid
        order by zts.status) phases;
END;
$$;

COMMIT;
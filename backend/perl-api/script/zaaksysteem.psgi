#!/usr/bin/env perl
use strict;
use warnings;
use Zaaksysteem;

my $app = Zaaksysteem->apply_default_middlewares(Zaaksysteem->psgi_app(@_));

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) xxllnc Zaakgericht and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


#!/bin/sh

set -e

ROOT_DIR=$(dirname $0)'/../'

if [ -z "$1" ]; then
    echo "This script needs a DSN. Please supply it as the first argument."
    echo "Username and password are also accepted, as further arguments."
    exit 1
fi

echo "    Using database '${DB_NAME}'"

${ROOT_DIR}/script/zaaksysteem_create.pl model DB DBIC::Schema Zaaksysteem::Schema \
    create=static \
    result_base_class='Zaaksysteem::Result' \
    overwrite_modifications=1 \
    skip_load_external=1 \
    datetime_timezone='UTC' \
    "$@"

rm ${ROOT_DIR}/lib/Zaaksysteem/Model/DB.pm.new
rm ${ROOT_DIR}/t/model_DB.t

package TestFor::General::ZAPI::Response::Iterator;
use warnings;
use strict;
use base 'Test::Class';

use TestSetup;

use URI;
use Zaaksysteem::ZAPI::Response;
use Zaaksysteem::Object::Iterator;
use Zaaksysteem::Object::Model;

sub setup : Test(startup) {
    my $self = shift;
    $self->{model} = Zaaksysteem::Object::Model->new(schema => $zs->schema);
}

sub test_zapi_response_iterator : Tests {
    my $self = shift;

    $zs->txn_ok(sub {
        my $new_obj = $self->{model}->inflate_from_json('{ "object_class": "object" }');
        my $object1 = $self->{model}->save(object => $new_obj);
        my $object2 = $self->{model}->save(object => $new_obj);
        my $rs = $self->{model}->rs;

        my $iter = Zaaksysteem::Object::Iterator->new(
            rs    => $rs,
            model => $self->{model},
        );

        my $resp = Zaaksysteem::ZAPI::Response->new(
            unknown => $iter,
            uri_prefix => URI->new('http://localhost/'),
        );

        ok($resp->_is_iterator($iter), "_is_iterator knows Zaaksysteem::Object::Iterator is an iterator");
        ok(!$resp->_is_iterator({}), "_is_iterator knows HASH is not an iterator");
    }, "ZAPI::Response::Iterator");
}

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

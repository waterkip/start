package TestFor::General::SAML2;

# ./zs_prove -v t/lib/TestFor/General/SAML2.pm 
use base qw(ZSTest);

=head1 NAME

TestFor::General::SAML2 - Tests for ZS::SAML2 and friends

=head1 SYNOPSIS

    ./zs_prove -v t/lib/TestFor/General/SAML2.pm

=cut

use TestSetup;
use File::Spec::Functions qw(catfile);
use File::Slurp;
use Encode;

use Net::SAML2::IdP;
use Zaaksysteem::SAML2;

=head1 TESTS

=head2 implementation_ssl_verify_xml

Implementation tests for ssl fingerprint verification

=cut

sub implementation_ssl_verify_xml : Tests {
    my $self        = shift;

    my $mockself    = Test::MockObject->new;
    $mockself->mock('_save_content_to_fh', \&Zaaksysteem::SAML2::_save_content_to_fh);
    $mockself->mock('idp', sub {
        my $iself = shift;

        return $iself->{idp} if $iself->{idp};

        my $idp = Test::MockObject->new;
        return $iself->{idp} = $idp->mock(
            'certs',
            sub {
                my $mself = shift;

                return $mself->{_certs} if $mself->{_certs};

                return $mself->{_certs} = {
                    signing => $zs->slurp_file(catfile(qw/t inc testcert testcrt.crt/))
                };
            }
        )
    });

    my $xml             = $zs->slurp_file(catfile(qw/t inc testcert signed-xml.xml/));

    ok(
        Zaaksysteem::SAML2::_ssl_verify_xml(
            $mockself,
            $xml
        ),
        'Correctly validated xml, signature correct'
    );

    my $incorrect_xml   = $xml;
    $incorrect_xml      =~ s/Bruce/bruce/;

    throws_ok(
        sub {
            Zaaksysteem::SAML2::_ssl_verify_xml(
                $mockself,
                $incorrect_xml
            )
        },
        qr/Response from IDP invalid, signature validation failed/,
        'Exception: Incorrectly validated incorrect_xml, signature incorrect, validation fails'
    );

    $mockself->idp->certs->{signing} = '';

    throws_ok(
        sub {
            Zaaksysteem::SAML2::_ssl_verify_xml(
                $mockself,
                $incorrect_xml
            )
        },
        qr/Could not find signing certificate of IDP in metadata/,
        'Exception: no signing certificate found'
    );
}

=head2 zs_saml2_parse_metadata_xml

Parse and validate metadata XML's.

=cut

sub zs_saml2_parse_metadata_xml : Tests {
    my $self = shift;
    if (!$ENV{ZS_DEVELOPER_SAML2}) {
        $self->builder->skip(
            "We don't have dummy metadata XML's for this scenario"
        );
        return;
    }

    $zs->txn_ok(
        sub {

            my $xml = decode_utf8(read_file(catfile(qw(t data federationmetadata.xml))));
            my $netsaml = Net::SAML2::IdP->new_from_xml(
                xml    => $xml,
                cacert => 'foo',
            );

            isa_ok($netsaml, "Net::SAML2::IdP");
            like($netsaml->certs->{signing}, qr#-----BEGIN CERTIFICATE-----#, "Found a certificate");

        }
    );
}

=head2 zs_saml2_test_spoof_mode

Test the spoof mode of ZS::SAML2

=cut

sub zs_saml2_test_spoof_mode : Tests {
    $zs->txn_ok(
        sub {

            my $idp = $zs->create_interface_ok(interface_config => { saml_type => 'spoof' });
            my $sp  = $zs->create_interface_ok();

            my $saml = Zaaksysteem::SAML2->new_from_interfaces(
                idp  => $idp,
                sp   => $sp,
            );

            isa_ok($saml, "Zaaksysteem::SAML2");
            isa_ok($saml->idp, "Zaaksysteem::SAML2::Spoof");
            isa_ok($saml->sp,  "Zaaksysteem::SAML2::Spoof");

        }
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

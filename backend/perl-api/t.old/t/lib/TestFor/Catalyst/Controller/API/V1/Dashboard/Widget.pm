package TestFor::Catalyst::Controller::API::V1::Dashboard::Widget;
use base qw(ZSTest::Catalyst);

use Moose;
use TestSetup;

=head1 NAME

TestFor::Catalyst::Controller::API::V1::Dashboard::Widget - Widget API for Dashboard

=head1 SYNOPSIS

    See USAGE tests

    Quick test of this single file from within vagrant
    ZS_DISABLE_STUF_PRELOAD=1 ./zs_prove -v t/lib/TestFor/Catalyst/Controller/API/V1/Dashboard/Widget.pm

=head1 DESCRIPTION

These tests prove the interactions between the outside servicebus and our zaaksysteem. This test
uses the version 1 API of zaaksysteem, and proves the C<api/v1/dashboard/widget> namespace.

=head1 USAGE

Usage tests, use these if you would like to know how to interact with the API, it's also useful
for extended documentation of the StUF API.

=head2 Create

=head3 create widget

Url: /api/v1/dashboard/widget/create

B<Request>

=begin javascript

{
   "column" : 2,
   "data" : "{ json_data: \"valid\" }",
   "row" : 1,
   "size_x" : 300,
   "size_y" : 150,
   "widget" : "searchquery"
}

=end javascript

B<Response>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-ea80c8-ae79bf",
   "result" : {
      "instance" : {
         "pager" : {
            "next" : null,
            "page" : 1,
            "pages" : 1,
            "prev" : null,
            "rows" : 1,
            "total_rows" : 1
         },
         "rows" : [
            {
               "instance" : {
                  "column" : "2",
                  "data" : "{ json_data: \"valid\" }",
                  "id" : "051e88a1-1180-4210-b04e-a07347e096fb",
                  "row" : "1",
                  "size_x" : "300",
                  "size_y" : "150",
                  "widget" : "searchquery"
               },
               "reference" : "051e88a1-1180-4210-b04e-a07347e096fb",
               "type" : "widget"
            }
         ]
      },
      "reference" : null,
      "type" : "set"
   },
   "status_code" : 200
}

=end javascript

=cut

sub cat_api_v1_dashboard_widget_create : Tests {
    my $self    = shift;

    $zs->zs_transaction_ok(sub {
        my $mech     = $zs->mech;
        $mech->zs_login;

        $mech->post_json(
            $mech->zs_url_base . '/api/v1/dashboard/widget/create',
            {
                widget          => 'searchquery',
                row             => 1,
                column          => 2,
                data            => { more_data => { one => 'two'}},
                size_x          => 300,
                size_y          => 150,
            }
        );

        my $json     = $mech->content;
        my $o_data   = $zs->get_json_as_perl($json);

        is($o_data->{result}->{type}, 'set', 'Got a set of widgets');
        is($o_data->{result}->{instance}->{rows}->[0]->{type}, 'widget', 'Got a result of type: widget');
    }, 'api/v1/dashboard/widget/create: create a simple widget');

    $zs->zs_transaction_ok(sub {
        my $mech     = $zs->mech;

        my $o_data   = $self->_create_widget($mech, { widget => "" });

        is($o_data->{result}->{type}, 'validationexception', 'Got valid validation exception on missing params');
    }, 'api/v1/dashboard/widget/create: create a simple widget with bogus input');
}

=head3 list widgets

Url: /api/v1/dashboard/widget

B<Response>

=begin javascript

{
  'api_version' => 1,
  'request_id' => 'mintlab-56b7f3-503c15',
  'result' => {
    'instance' => {
      'pager' => {
        'next' => undef,
        'page' => 1,
        'pages' => 1,
        'prev' => undef,
        'rows' => 3,
        'total_rows' => 3
      },
      'rows' => [
        {
          'instance' => {
            'column' => '2',
            'data' => '{ json_data: "valid" }',
            'id' => '132658d9-9eea-4ed1-bfc7-512525882533',
            'row' => '1',
            'size_x' => '300',
            'size_y' => '150',
            'widget' => 'third'
          },
          'reference' => '132658d9-9eea-4ed1-bfc7-512525882533',
          'type' => 'widget'
        },
        {
          'instance' => {
            'column' => '2',
            'data' => '{ json_data: "valid" }',
            'id' => '807c09e7-4027-4988-89ee-96f05233ce6c',
            'row' => '1',
            'size_x' => '300',
            'size_y' => '150',
            'widget' => 'second'
          },
          'reference' => '807c09e7-4027-4988-89ee-96f05233ce6c',
          'type' => 'widget'
        },
        {
          'instance' => {
            'column' => '2',
            'data' => '{ json_data: "valid" }',
            'id' => 'b334e0aa-db0e-4d94-a739-5c36ec69dd70',
            'row' => '1',
            'size_x' => '300',
            'size_y' => '150',
            'widget' => 'first'
          },
          'reference' => 'b334e0aa-db0e-4d94-a739-5c36ec69dd70',
          'type' => 'widget'
        }
      ]
    },
    'reference' => undef,
    'type' => 'set'
  },
  'status_code' => 200
}

=end javascript

=cut

sub cat_api_v1_dashboard_widget_list : Tests {
    my $self    = shift;

    $zs->zs_transaction_ok(sub {
        my $mech     = $zs->mech;

        my @widgets  = qw/first second third/;

        $self->_create_widget($mech, { widget => $_}) for @widgets;

        $mech->get(
            $mech->zs_url_base . '/api/v1/dashboard/widget',
        );

        my $json     = $mech->content;
        my $o_data   = $zs->get_json_as_perl($json);

        is($o_data->{result}->{type}, 'set', 'Got a set of records');
        is(@{ $o_data->{result}->{instance}->{rows} }, scalar @widgets, 'Got number of records: ' . scalar(@widgets));

        for my $row (@{ $o_data->{result}->{instance}->{rows} }) {
            ok($row->{instance}->{widget}, 'Got a widget in instance: ' . $row->{instance}->{widget});
        }
    }, 'api/v1/dashboard/widget: list the widgets');
}

=head3 update widget

Url: /api/v1/dashboard/widget/b66be381-282c-4fbb-a603-2d683518670d/update

B<Request>

=begin javascript

{
   "column" : 2,
   "data" : "{ json_data: \"valid\" }",
   "row" : 1,
   "size_x" : 300,
   "size_y" : 150,
   "widget" : "searchquery"
}

=end javascript

B<Response>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-ea80c8-0f7467",
   "result" : {
      "instance" : {
         "pager" : {
            "next" : null,
            "page" : 1,
            "pages" : 1,
            "prev" : null,
            "rows" : 1,
            "total_rows" : 1
         },
         "rows" : [
            {
               "instance" : {
                  "column" : "3",
                  "data" : "{ json_data: \"morevalid\" }",
                  "id" : "70cae962-1128-4775-86d6-ef1140ddc20a",
                  "row" : "44",
                  "size_x" : "340",
                  "size_y" : "120",
                  "widget" : "searchquery2"
               },
               "reference" : "70cae962-1128-4775-86d6-ef1140ddc20a",
               "type" : "widget"
            }
         ]
      },
      "reference" : null,
      "type" : "set"
   },
   "status_code" : 200
}

=end javascript

=cut

sub cat_api_v1_dashboard_widget_update : Tests {
    my $self    = shift;

    $zs->zs_transaction_ok(sub {
        my $mech     = $zs->mech;
        my $data     = $self->_create_widget($mech);

        my $params   = {
            widget          => 'searchquery2',
            row             => 44,
            column          => 3,
            data            => { more_data => { one => 'two'}},
            size_x          => 340,
            size_y          => 120,
        },

        my $uuid     = $data->{result}->{instance}->{rows}->[0]->{reference};

        $mech->post_json(
            $mech->zs_url_base . '/api/v1/dashboard/widget/' . $uuid . '/update',
            $params
        );

        my $json     = $mech->content;
        my $o_data   = $zs->get_json_as_perl($json);

        is($o_data->{result}->{instance}->{rows}->[0]->{type}, 'widget', 'Got valid type');

        my $instance = $o_data->{result}->{instance}->{rows}->[0]->{instance};

        for my $param (keys %$params) {
            if (ref $params->{$param}) {
                is_deeply($instance->{$param}, $params->{$param}, "Param $param correctly updated");
                next;
            }
            is($instance->{$param}, $params->{$param}, "Param $param correctly updated");
        }

    }, 'api/v1/dashboard/widget/UUID/update: update a widget');

}

=head3 bulk update widget

Url: /api/v1/dashboard/widget/bulk_update

B<Request>

=begin javascript

{
   "updates" : [
      {
         "column" : 3,
         "data" : "{ json_data: \"morevalid\" }",
         "row" : 44,
         "size_x" : 340,
         "size_y" : 120,
         "uuid" : "222d0176-a4ce-4179-94b6-481953e4655d",
         "widget" : "searchquery2"
      },
      {
         "column" : 4,
         "data" : "{ json_data: \"evenmorevalid\" }",
         "row" : 55,
         "size_x" : 350,
         "size_y" : 160,
         "uuid" : "f2c90b5c-3abf-464a-9acd-4c00b15a6e0c",
         "widget" : "searchquery2"
      }
   ]
}


=end javascript

B<Response>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-0af018-02310b",
   "result" : {
      "instance" : {
         "pager" : {
            "next" : null,
            "page" : 1,
            "pages" : 1,
            "prev" : null,
            "rows" : 2,
            "total_rows" : 2
         },
         "rows" : [
            {
               "instance" : {
                  "column" : "4",
                  "data" : "{ json_data: \"evenmorevalid\" }",
                  "id" : "0730fc3f-e4df-448a-90fb-fcdd851a9f46",
                  "row" : "55",
                  "size_x" : "350",
                  "size_y" : "160",
                  "widget" : "searchquery2"
               },
               "reference" : "0730fc3f-e4df-448a-90fb-fcdd851a9f46",
               "type" : "widget"
            },
            {
               "instance" : {
                  "column" : "3",
                  "data" : "{ json_data: \"morevalid\" }",
                  "id" : "102eb145-7fcd-44ab-a34d-fa044acc91a7",
                  "row" : "44",
                  "size_x" : "340",
                  "size_y" : "120",
                  "widget" : "searchquery2"
               },
               "reference" : "102eb145-7fcd-44ab-a34d-fa044acc91a7",
               "type" : "widget"
            }
         ]
      },
      "reference" : null,
      "type" : "set"
   },
   "status_code" : 200
}

=end javascript

=cut

sub cat_api_v1_dashboard_widget_bulk_update : Tests {
    my $self    = shift;

    $zs->zs_transaction_ok(sub {
        my $mech     = $zs->mech;

        my $data     = $self->_create_widget($mech);
        my $data2    = $self->_create_widget($mech);

        my $uuid1     = $data2->{result}->{instance}->{rows}->[0]->{reference};
        my $uuid2     = $data2->{result}->{instance}->{rows}->[1]->{reference};

        my $params   = {
            updates => [
                {
                    uuid            => $uuid1,
                    widget          => 'searchquery2',
                    row             => 44,
                    column          => 3,
                    data            => { more_data => { one => 'two'}},
                    size_x          => 340,
                    size_y          => 120,
                },
                {
                    uuid            => $uuid2,
                    widget          => 'searchquery2',
                    row             => 55,
                    column          => 4,
                    data            => { more_data => { one => 'three'}},
                    size_x          => 350,
                    size_y          => 160,
                }
            ]
        };

        $mech->post_json(
            $mech->zs_url_base . '/api/v1/dashboard/widget/bulk_update',
            $params
        );

        my $json     = $mech->content;
        my $o_data   = $zs->get_json_as_perl($json);

        is($o_data->{result}->{instance}->{rows}->[0]->{type}, 'widget', 'Got valid type');

        for my $row (@{ $o_data->{result}->{instance}->{rows} }) {
            my $compare;
            if ($row->{reference} eq $uuid1) {
                $compare = $params->{updates}->[0];
            } elsif ($row->{reference} eq $uuid2) {
                $compare = $params->{updates}->[1];
            }

            my $instance = $row->{instance};

            for my $param (keys %$compare) {
                next if $param eq 'uuid';

                if (ref $compare->{$param}) {
                    is_deeply($instance->{$param}, $compare->{$param}, "Param $param correctly updated");
                    next;
                }
                is($instance->{$param}, $compare->{$param}, "Param $param correctly updated");
            }
        }

    }, 'api/v1/dashboard/widget/bulk_update: bulk update a widget');

}

=head3 delete widget

Url: /api/v1/dashboard/widget/b66be381-282c-4fbb-a603-2d683518670d/delete

B<Request>

=begin javascript

{}

=end javascript

B<Response>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-ea80c8-d8d303",
   "result" : {
      "instance" : {
         "pager" : {
            "next" : null,
            "page" : 1,
            "pages" : 1,
            "prev" : null,
            "rows" : 0,
            "total_rows" : 0
         },
         "rows" : []
      },
      "reference" : null,
      "type" : "set"
   },
   "status_code" : 200
}

=end javascript

=cut

sub cat_api_v1_dashboard_widget_delete : Tests {
    my $self    = shift;

    $zs->zs_transaction_ok(sub {
        my $mech     = $zs->mech;
        my $data     = $self->_create_widget($mech);

        my $uuid     = $data->{result}->{instance}->{rows}->[0]->{reference};

        ok($schema->resultset('ObjectData')->find($uuid), 'Found object in database');

        $mech->post_json(
            $mech->zs_url_base . '/api/v1/dashboard/widget/' . $uuid . '/delete',
            {}
        );

        my $json     = $mech->content;
        my $o_data   = $zs->get_json_as_perl($json);

        is(@{ $o_data->{result}->{instance}->{rows} }, 0, 'Got zero rows');

        ok(!$schema->resultset('ObjectData')->find($uuid), 'Succesfully deleted object from database');

    }, 'api/v1/dashboard/widget/delete: delete a widget');
}

sub _create_widget {
    my ($self, $mech, $data) = @_;

    $mech->zs_login;

    $mech->post_json(
        $mech->zs_url_base . '/api/v1/dashboard/widget/create',
        {
            widget          => 'searchquery',
            row             => 1,
            column          => 2,
            data            => { more_data => { one => 'two'}},
            size_x          => 300,
            size_y          => 150,
            $data ? %$data : (),
        },
    );

    my $json     = $mech->content;
    my $o_data   = $zs->get_json_as_perl($json);

    return $o_data;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

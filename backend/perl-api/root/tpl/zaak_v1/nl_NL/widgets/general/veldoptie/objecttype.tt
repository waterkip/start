[% USE JSON %]

        <div class="veldoptie-object-type" data-zs-case-webform-object-type-field data-zs-case-webform-object-type-field-object="object" data-zs-case-webform-object-type-field-can-change="[% view_mode ? 'false' : 'true' %]" data-ng-class="{'veldoptie-object-type-disabled': caseWebformObjectTypeField.canChange() }" data-object-rights="metadata">
            
            <script type="text/zs-scope-data">
                {
                    "metadata": [% JSON.encode(kenmerk.object_metadata) %],
                    "object": [% JSON.encode(kenmerk.object_id) %]
                }
            </script>
            
            <div class="veldoptie-object-type-header kenmerk-titel">
                <i class="mdi mdi-hexagon-outline icon-light-blue icon-lebensraum"></i> <[caseWebformObjectTypeField.getObjectTypeName()]>
            </div>

            
            <ul class="veldoptie-object-type-list">
                <li class="veldoptie-object-type-item veldoptie-object-type-item-type-<[mutation.type]>" data-ng-repeat="mutation in caseWebformObjectTypeField.mutations" data-mutation="mutation" data-zs-case-webform-object-type-mutation 
                    data-ng-class="{
                        'veldoptie-object-type-item-disabled': !caseWebformObjectTypeMutation.canChange(), 
                        'veldoptie-object-type-item-loading': caseWebformObjectTypeMutation.isLoading(),
                        'veldoptie-object-type-item-error': caseWebformObjectTypeMutation.hasError(),
                        'veldoptie-object-type-item-invalid': caseWebformObjectTypeMutation.isInvalid(),
                        'veldoptie-object-type-item-open': caseWebformObjectTypeMutation.isOpen()
                    }">
                    
                    <div class="veldoptie-object-type-item-header">

                        <span class="veldoptie-object-type-item-label">
                            <[caseWebformObjectTypeMutation.getLabel()]>
                        </span>
                        
                        <div class="veldoptie-object-type-item-type item-label" data-zs-title="<[caseWebformObjectTypeMutation.getStateMessage()]>">
                            <[caseWebformObjectTypeMutation.getTypeLabel()]>
                        </div>
                        
                        <div class="veldoptie-object-type-list-item-options">

                            <a class="veldoptie-object-type-list-item-option" data-ng-href="/object/<[mutation.object_uuid]>" target="_blank" rel="noopener" data-zs-title="Open objectdossier" data-ng-show="!!mutation.object_uuid">
                                <i class="icon-font-awesome icon-external-link"></i>
                            </a>
                            
                            <button class="veldoptie-object-type-list-item-option" type="button" data-ng-click="caseWebformObjectTypeMutation.toggleForm()" data-zs-title="Bewerk mutatie" data-ng-show="caseWebformObjectTypeMutation.isEditable()">
                                <i class="icon-font-awesome icon-pencil"></i>
                            </button>
                            
                            <button class="veldoptie-object-type-list-item-option" type="button" data-ng-click="caseWebformObjectTypeMutation.cancel()" data-zs-title="Verwijder mutatie" data-ng-show="caseWebformObjectTypeMutation.canChange()">
                                <i class="icon-font-awesome icon-remove"></i>
                            </button>
                            
                        </div>
                        
                        <div class="zs-spinner-small" data-zs-spinner="caseWebformObjectTypeMutation.isLoading()">
                        </div>
                        
                    </div>
                    
                    <div class="veldoptie-object-type-item-header-form">
                    
                        <div class="veldoptie-object-type-item-form" data-zs-case-webform-object-type-form data-zs-case-webform-object-type-form-values="mutation.values" data-object-type-id="caseWebformObjectTypeField.getObjectTypeId()" data-zs-case-webform-object-type-form-edit data-ng-show="caseWebformObjectTypeMutation.isOpen()&&!caseWebformObjectTypeForm.isLoading()" data-ng-class="{'veldoptie-object-type-item-form-open': caseWebformObjectTypeMutation.isOpen(), 'veldoptie-object-type-item-form-loading': caseWebformObjectTypeForm.isLoading() }">
                        </div>
                        
                        <div class="zs-spinner-small" zs-spinner="caseWebformObjectTypeForm.isLoading()">
                        </div>
                        
                        <div class="form-fieldset-actions" data-ng-show="caseWebformObjectTypeMutation.isOpen()">
                        
                            <button type="button" class="form-action form-action-submit btn btn-flat" data-ng-click="caseWebformObjectTypeMutation.closeForm()">
                                Annuleren
                            </button>
                            <button type="submit" class="form-action form-action-submit btn btn-primary" data-ng-click="caseWebformObjectTypeMutation.confirm($event)" data-ng-disabled="!caseWebformObjectTypeMutation.isValid()">
                                Opslaan
                            </button>
                            
                        </button>
                        
                    </div>
                    
                    
                    
                </li>
            </ul>

            <div class="veldoptie-object-type-add" data-ng-include="'/widgets/general/veldoptie/objecttype/add.html'" data-ng-if="caseWebformObjectTypeField.canChange()">
                
            </div>
            
            <div class="zs-spinner-small" data-zs-spinner="caseWebformObjectTypeField.isLoading()">
            </div>

        </div>
        
        <script type="text/ng-template" id="/widgets/general/veldoptie/objecttype/add.html">
            
            <div class="form-fieldset" data-zs-case-webform-object-type-mutation-add="caseWebformObjectTypeField.addMutation($mutation)" data-object-type-prefix="caseWebformObjectTypeField.getObjectTypePrefix()">
            
                <div class="form-fieldset-children">
                    <div class="form-field veldoptie-object-type-add-buttons">
                        <div class="form-field_label">
                            Wat wilt u doen?
                        </div>
                        
                        <div class="form-field-input button-group" data-ng-show="caseWebformObjectTypeMutationAdd.getVisibleOptions(caseWebformObjectTypeField.hasPermission).length">
                            <button type="button" class="mutation-add-type-button button-secondary button button-small" data-ng-repeat="option in caseWebformObjectTypeMutationAdd.getVisibleOptions(caseWebformObjectTypeField.hasPermission)" data-ng-class="{'button-active': option.value === caseWebformObjectTypeMutationAdd.mutationType }" data-ng-click="caseWebformObjectTypeMutationAdd.handleOptionClick(option)">
                                <[option.label]>
                            </button>
                        </div>
                        <div class="form-field-input" data-ng-show="!caseWebformObjectTypeMutationAdd.getVisibleOptions(caseWebformObjectTypeField.hasPermission).length">
                            Geen mogelijkheden beschikbaar
                        </div>
                    </div>

                    <div class="veldoptie-object-type-form-wrap" data-ng-class="{ 'veldoptie-object-type-form-wrap-show': caseWebformObjectTypeMutationAdd.isFormVisible() || caseWebformObjectTypeMutationAdd.isObjectVisible() }">

                        <div class="form-fieldset veldoptie-object-type-search" data-ng-show="caseWebformObjectTypeMutationAdd.isObjectVisible()">

                            <div class="form-fieldset-meta">
                                <div class="form-fieldset-header clearfix">
                                <span class="form-fieldset-title">
                                    <[caseWebformObjectTypeMutationAdd.getSelectedOptionLabel()]>
                                </span>
                                </div>
                            </div>

                            <div class="form-fieldset-children">

                                <div class="form-field">
                                    <div class="form-field_label">
                                        Zoek een object
                                    </div>
                                    <div class="form-field-input">
                                        
                                        <div data-ng-show="caseWebformObjectTypeMutationAdd.mutation.object_uuid" class="veldoptie-object-type-title">
                                            <[caseWebformObjectTypeMutationAdd.mutation.label]>
                                        </div>
                                        
                                        <div class="veldoptie-object-type-search spot-enlighter-wrapper" data-ng-show="caseWebformObjectTypeMutationAdd.isSpotEnlighterVisible()">
                                            <input type="text" data-zs-spot-enlighter data-ng-model="caseWebformObjectTypeMutationAdd.newObject" data-zs-spot-enlighter-restrict="'objects'" data-zs-spot-enlighter-params="caseWebformObjectTypeMutationAdd.getSpotEnlighterParams()" data-zs-placeholder="'Zoek een bestaand object'" data-zs-spot-enlighter-select="caseWebformObjectTypeMutationAdd.handleSpotEnlighterSelect($object)"/>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                            
                        <div data-ng-show="caseWebformObjectTypeMutationAdd.isFormVisible()">
                        
                             <div data-zs-case-webform-object-type-form data-zs-case-webform-object-type-form-values="caseWebformObjectTypeMutationAdd.getMutationValues()" data-object-type-id="caseWebformObjectTypeField.getObjectTypeId()" data-zs-case-webform-object-type-form-add data-zs-case-webform-object-type-form-open="caseWebformObjectTypeMutationAdd.isFormVisible()">
                            </div>
                            
                        </div>
                        
                        <div class="form-fieldset-actions" data-ng-show="caseWebformObjectTypeMutationAdd.isActionsVisible()">
                            <button type="button" class="form-action form-action-submit btn btn-flat" data-ng-click="caseWebformObjectTypeMutationAdd.cancel()">
                                Annuleren
                            </button>
                            <button type="button" class="form-action form-action-submit btn btn-primary" data-zs-confirm="caseWebformObjectTypeMutationAdd.confirm($event)" data-ng-disabled="!caseWebformObjectTypeMutationAdd.isValid()" data-zs-confirm-verb="Toevoegen" data-zs-confirm-label="Weet u zeker dat u dit object wil markeren voor verwijdering?" data-zs-confirm-enabled="caseWebformObjectTypeMutationAdd.mutationType==='delete'">
                                Toevoegen
                            </button>
                        </div>
                        

                </div>

            </div>
            
        </div>
        </script>
        
        <script type="text/ng-template" id="/widgets/general/veldoptie/objecttype/form.html">
            <div class="veldoptie-object-type-form" data-zs-form-template-parser="<[caseWebformObjectTypeForm.getForm()]>">
            </div>
        </script>

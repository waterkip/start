[% USE Dumper %]

[%

    bag = undef;

# Opties
# veldoptie_type    => type
# veldoptie_name    => type
# veldoptie_value    => type
# veldoptie_opties    => type
    RETURN UNLESS (veldoptie_type && veldoptie_name);
%]

[% USE JSON %]

[% BLOCK veldoptie_value_row %]
    <li>
        [% INCLUDE veldoptie_single veldoptie_value=vokey %]
        <div class="row-actions">
            <a href="#" class="del" role="button">
                <i class="icon-font-awesome icon-remove focus-outline"></i>
                <span class="visuallyhidden">Verwijder deze regel</span>
            </a>
            <span class="del-inactive">
                <i class="icon-font-awesome icon-remove icon-remove-disabled"></i>
            </span>
        </div>
    </li>
[% END %]

[% BLOCK get_kenmerk;
  IF kenmerk != 'current';
    date_kenmerk = c.model('DB::ZaaktypeKenmerken').find({id => kenmerk});
    IF date_kenmerk ;
      date_kenmerk = "kenmerk_id_" _ date_kenmerk.get_column('bibliotheek_kenmerken_id');
    END;
  ELSE;
    date_kenmerk = kenmerk;
  END;
END; %]

[% BLOCK veldoptie_multiple %]
<div class="veldoptie_multiple">
    <ul class="veldoptie_multiple_list">
        [% multiple_shown = 0 %]
        [% FOREACH vokey IN veldoptie_value %]
            [% NEXT UNLESS vokey.length %]
            [% multiple_shown = 1 %]
            [% INCLUDE veldoptie_value_row %]
        [% END %]

        [% UNLESS multiple_shown %]
            [% vokey = veldoptie_value %]
            [% INCLUDE veldoptie_value_row %]
        [% END %]
    </ul>
    [% IF veldoptie_label_multiple %]
        [% title = veldoptie_label_multiple | html_entity %]
    [% ELSE %]
        [% title = "Veld toevoegen" %]
    [% END %]
    <a href="#" class="add-veldoptie add button button-secondary
    button-small"><i class="icon icon-font-awesome icon-plus"></i> [% title %]</a>
</div>
[% END %]

[% BLOCK veldoptie_single %]


    [%
      # Force all values to be a single value, but not for all, because
      # they are special
      IF veldoptie_type != 'select' && veldoptie_type != 'checkbox' && veldoptie_type != 'option' && veldoptie_type != 'bag_adressen' && veldoptie_type != 'bag_straat_adressen' && veldoptie_type != 'bag_openbareruimtes' && veldoptie_type != 'file';
        veldoptie_value = veldoptie_value.list.0;
      END;
    %]

    [% IF veldoptie_id %]
        [% my_veldoptie_id = ' id="' _ veldoptie_id _ '"' %]
    [% ELSE %]
        [% my_veldoptie_id = '' # freaking TT globals %]
    [% END %]

    [% IF veldoptie_hidden %]
        <input type="hidden" name="[% veldoptie_name %]" value="[% veldoptie_value %]" data-magic-string="attribute.[% veldoptie_bibliotheek.magic_string %]"/>
    [% ELSIF veldoptie_type == 'textarea' %]
        <textarea
            name="[% veldoptie_name %]"
            cols="60"
            rows="4"
            [% my_veldoptie_id %]
            class="veldoptie_textarea"
        >[% veldoptie_value | html_entity %]</textarea>
    [% ELSIF
        veldoptie_type == 'text' ||
        veldoptie_type == 'image_from_url' ||
        veldoptie_type == 'text_uc'
    %]
        [% IF veldoptie_type == 'image_from_url' && veldoptie_value %]
            <img src="[% veldoptie_value | html_entity %]" /><br />
        [% END %]
        <input
            type="text"
            [% my_veldoptie_id %]
            name="[% veldoptie_name %]"
            value="[% veldoptie_value | html_entity %]"
            class="veldoptie_[% veldoptie_type %]"
            />
    [% ELSIF veldoptie_type == 'numeric' %]
        [% PROCESS widgets/general/veldoptie/numeric.tt %]


    [% ELSIF veldoptie_type == 'richtext' %]
        <input
            name="[% veldoptie_name %]" 
            type="hidden"
            class="zs-rich-text-editor"
            features="bold italic underline bullet ordered link"
            value="[% veldoptie_value | html_entity %]"
        />

    [% ELSIF veldoptie_type == 'url' %]

        <div class="ezra_url_input_type veldoptie_webadres">
            <input
                type="text"
                [% my_veldoptie_id %]
                name="[% veldoptie_name %]"
                value="[% veldoptie_value | html_entity %]"
                class="veldoptie_[% veldoptie_type %]"
                />

            <button class="ezra_url_field_goto button button-secondary button-small [% UNLESS veldoptie_value %]disabled="disabled"[% END %]" rel="[% veldoptie_value | html_entity %]" title="Ga naar de url">
                <i class="icon-font-awesome icon icon-external-link"></i>
                <span class="visuallyhidden">Ga naar de ingevulde URL</span>
            </button>
        </div>

    [% ELSIF veldoptie_type == 'bankaccount' %]
        [% PROCESS widgets/general/veldoptie/bankaccount.tt %]
    [% ELSIF veldoptie_type == 'email' %]
        [% PROCESS widgets/general/veldoptie/email.tt %]
    [% ELSIF veldoptie_type == 'geojson' %]
      <div data-zs-case-webform-geojson-field>
        <input
          type="hidden"
          name="[% veldoptie_name %]" 
          value='[% veldoptie_value %]'
        />
        <div class="geojson-placeholder"></div>
      </div>
    [% ELSIF veldoptie_type == 'address_v2' %]
      <div data-zs-case-webform-address2-field>
        <input
          type="hidden"
          name="[% veldoptie_name %]" 
          value="[% veldoptie_value %]"
        />
        <input
          type="hidden"
          name="map_wms_layer_id" 
          value="[% veldoptie_properties.map_wms_layer_id %]"
        />
        <input
          type="hidden"
          name="map_wms_feature_attribute_id" 
          value="[% veldoptie_properties.map_wms_feature_attribute_id %]"
        />
        <div class="address-placeholder" veldoptie-id="[% veldoptie_id %]"></div>
      </div>
    [% ELSIF veldoptie_type == 'date' %]

        [% IF veldoptie_zaaktype_kenmerk.properties.date_limit;
            start = veldoptie_zaaktype_kenmerk.properties.date_limit.start;
            end   = veldoptie_zaaktype_kenmerk.properties.date_limit.end;

            IF start.active;
                PROCESS get_kenmerk kenmerk = start.reference;
                start_kenmerk = date_kenmerk;
            END;

            IF end.active;
                PROCESS get_kenmerk kenmerk = end.reference;
                end_kenmerk = date_kenmerk;
            END;

        END -%]
        <div class="veldoptie_datepicker_wrapper">
            <span class="veldoptie_datepicker_suggestion">Datumformaat: dd-mm-jjjj</span>
            <input
                type="text"
                placeholder="Vul een datum in"
                name="[% veldoptie_name %]"
                autocomplete="off"
                value="[% veldoptie_value.0 || veldoptie_value | html_entity %]"
                [% IF start.active || end.active %]
                class="veldoptie_datepicker_datelimit normal100"
                    datepicker_start_active="[% start.active || 0 %]";
                    [% IF start.active -%]
                    datepicker_start_during="[% start.during %]";
                    datepicker_start_num="[% start.num %]";
                    datepicker_start_term="[% start.term %]";
                    datepicker_start_reference="[% start_kenmerk %]";
                    [% END -%]

                    datepicker_end_active="[% end.active || 0 %]";
                    [% IF end.active -%]
                    datepicker_end_during="[% end.during %]";
                    datepicker_end_num="[% end.num %]";
                    datepicker_end_term="[% end.term %]";
                    datepicker_end_reference="[% end_kenmerk %]";
                    [% END -%]
                [% ELSE %]
                class="veldoptie_datepicker normal100"
                [% END %]
                [% IF my_veldoptie_id %]
                    [% my_veldoptie_id %]
                [% ELSE %]
                    id="dp_[% veldoptie_name %]"
                [% END %]
            />
        </div>
    [% ELSIF veldoptie_type.match('^bag_') %]
        [% multiple_addresses = 
            veldoptie_type == 'bag_adressen' ||
            veldoptie_type == 'bag_straat_adressen' ||
            veldoptie_type == 'bag_openbareruimtes'
        %]
        [% select_street = veldoptie_type.match('openbareruimte') %]
        <div 
        class="veldoptie-bag veldoptie_bag_angular_wrapper[% IF multiple_addresses %] multiple-addresses[% END %][% IF select_street %] select-street[% END %]" 
        data-zs-case-webform-object-field 
        data-zs-case-webform-object-field-limit="[% IF !multiple_addresses %]1[% END %]" 
        data-zs-case-webform-object-field-value="value" 
        data-zs-case-webform-object-field-data-converter>
            <script type="text/zs-scope-data" data-zs-scope-data-load="caseWebformObjectFieldDataConverter.setObjects($data.value)">
                [% temp = [] -%]
                [% FOREACH key IN veldoptie_value -%]
                    [% 
                       bag_value = key.split('-');
                       bag_entry = bag_model.get(bag_value.0, bag_value.1);
                       temp.push(bag_entry.to_attribute_value(bag_value.0)) IF bag_entry;
                    %]
                [% END -%]
                [% JSON.encode({
                    value => temp
                }) %]
            </script>
            <div class="bag-selected-addresses ng-cloak">
                <label data-ng-repeat="address in caseWebformObjectField.objects">
                    <input class="ezra-disable_fieldUpdate ezra-ezra_disable_webform_update" type="checkbox" checked="checked" name="[% veldoptie_name %]" value="<[address.bag_id]>" data-ng-click="caseWebformObjectField.removeObject(address)"/>
                    <[address.human_identifier]>
                </label>
            </div>
            <div class="spot-enlighter-wrapper" data-ng-show="caseWebformObjectField.limit>caseWebformObjectField.objects.length">
                <input 
                    [% my_veldoptie_id %]
                    class="ezra-disable_fieldUpdate ezra-ezra_disable_webform_update" 
                    type="text" 
                    name="bogus_[% veldoptie_name %]" 
                    data-ng-model="newObject" 
                    data-zs-spot-enlighter-select="caseWebformObjectField.handleObjectSelect($object)" 
                    data-zs-placeholder="'[% IF !select_street %]Vul een adres in[% ELSE %]Vul een straatnaam in[% END %]'" 
                    data-zs-spot-enlighter 
                    data-zs-spot-enlighter-restrict="'[% select_street ? 'bag-street' : 'bag' %]'" 
                    data-zs-spot-enlighter-label="human_identifier" 
                    data-zs-object-required="false"
                >
            </div>
            <div class="bag-address-decoy" data-ng-switch="caseWebformObjectField.objects.length">
                <div data-ng-switch-when="0">
                    <input type="hidden" name="[% veldoptie_name %]"/>
                </div>
            </div>
        </div>
    [% ELSIF veldoptie_type == 'valuta' %]
        [%
            USE ValutaFormattingFilter;

            veldoptie_value = veldoptie_value.0 || veldoptie_value;
            IF veldoptie_value;
              veldoptie_value = veldoptie_value | $ValutaFormattingFilter;
              veldoptie_value = veldoptie_value | format('%01.2f');
            END;
            public_form = ( c.session.pip || c.session.form ) ? 1 : 0;
        %]
        <input
            class="veldoptie_[% veldoptie_type %]_simpel [% IF !public_form %]veldoptie_valuta_field[% END %]"
            type="text"
            [% my_veldoptie_id %]
            name="[% veldoptie_name %]"
            value="[% veldoptie_value.replace('\.', ',') | html_entity %]"
         /> <span class="veld-uitleg">Voorbeeld: 500,00</span>
    [% ELSIF veldoptie_type.match('valutain') || veldoptie_type.match('valutaex') %]
    [% veldoptie_value = veldoptie_value.0 || veldoptie_value %]
    [% valutasplitted = veldoptie_value.split('\.'); %]
        <div class="veldoptie_valuta">
            <input
                type="hidden"
                name="[% veldoptie_name %]"
                value="[% veldoptie_value | html_entity %]"
                class="[% veldoptie_type %] eur"
             />
            [% c.loc('&euro;') %] <input
                size="10"
                type="text"
                [% my_veldoptie_id %]
                name="eur_[% veldoptie_name %]"
                value="[% valutasplitted.0 %]"
                class="[% veldoptie_type %] eur ezra-disable_fieldUpdate"
             />
            <input
                size="2"
                type="text"
                name="cnt_[% veldoptie_name %]"
                value="[% valutasplitted.1 %]"
                class="[% veldoptie_type %] cnt ezra-disable_fieldUpdate"
             />
             <span class="veldoptie_valuta_calculation nojshide veld-uitleg">
                [% (veldoptie_type.match('valutain') ? 'excl.' : 'incl.') %]
                [% c.loc('BTW &euro;') %]
                <span class="value [% (
                    veldoptie_type.match('valutain') ? 'inclbtw' : 'exclbtw')
                %] btw[% (
                    veldoptie_type.match('valuta[inex]+(\d+)')
                        ? veldoptie_type.match('valuta[inex]+(\d+)').0
                        : '19'
                ) %]"></span>
            </span>
        </div>
    [% ELSIF veldoptie_type == 'select' || veldoptie_type == 'checkbox' || veldoptie_type == 'option' %]
        [% PROCESS widgets/general/veldoptie/multioption.tt %]
    [% ELSIF veldoptie_type == 'file' %]
        [% PROCESS widgets/general/veldoptie/file.tt %]

    [% ELSIF veldoptie_type == 'calendar' %]
        [% PROCESS widgets/general/veldoptie/qmatic.tt
            qmatic_can_edit = 1
        %]
    [% ELSIF veldoptie_type == 'calendar_supersaas' %]
        [% PROCESS widgets/general/veldoptie/supersaas.tt
            supersaas_can_edit = 1
        %]
    [% ELSIF veldoptie_type == 'appointment' %]
        [% PROCESS widgets/general/veldoptie/appointment.tt
            appointment_can_edit = 1
        %]
    [% ELSIF veldoptie_type == 'appointment_v2' %]
        [% PROCESS widgets/general/veldoptie/appointment_v2.tt
            appointment_can_edit = 1
        %]
    [% ELSIF veldoptie_type == 'googlemaps' || veldoptie_type == 'geolatlon' %]
        [% PROCESS widgets/general/map.tt
            MAP_INPUT       = veldoptie_name
            MAP_TEMPLATE    = 'address'
            MAP_CSSPROFILE  = 'smallfixed'
            MAP_FIXEDWIDTH  = 1
            MAP_LOCATION    = veldoptie_value.0 || veldoptie_value
            MAP_CENTER      = c.get_customer_info.latitude _ ' ' _ c.get_customer_info.longitude
            MAP_FORM        = 1
            MAP_LATLONONLY  = (veldoptie_type == 'geolatlon' ? 1 : 0)
            MAP_WMS_LAYER_ID= veldoptie_properties.map_wms_layer_id
            MAP_WMS_FEATURE_ATTRIBUTE_ID = veldoptie_properties.map_wms_feature_attribute_id
            MAP_WMS_FEATURE_ATTRIBUTE_LABEL = veldoptie_properties.map_wms_feature_attribute_label
            my_veldoptie_id = my_veldoptie_id
        %]
    [% ELSIF veldoptie_type == 'objecttype' %]
        [% PROCESS widgets/general/veldoptie/objecttype.tt %]
    [% END %]

[% END %]


[% IF veldoptie_multiple %]
    [% PROCESS veldoptie_multiple %]
[% ELSE %]
    [% PROCESS veldoptie_single %]
[% END %]


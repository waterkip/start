{{- define "egress_policy.to_olo_sftp" -}}
- toFQDNs:
    {{- range .Values.global.olo.hostnames }}
    - matchName: {{ . | quote }}
    {{- end }}
{{- end }}
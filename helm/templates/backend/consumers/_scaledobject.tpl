{{/*
ScaledObject template
*/}}
{{- define "consumer.scaled_object" -}}
{{- $ := index . 0 }}
{{- $app := index . 1 }}
{{- $scaling := $app.scaling | default $.Values.global.scaling -}}
{{- if (and $app.enabled $.Values.global.keda.enabled) -}}
apiVersion: keda.sh/v1alpha1
kind: ScaledObject
metadata:
  name: {{ include "fullname" (list $ $app.name) }}
  namespace: {{ $.Release.Namespace }}
spec:
  scaleTargetRef:
    name: {{ include "fullname" (list $ $app.name) }}

  {{- $scaling.parameters | toYaml | nindent 2 }}
  triggers:
    {{- /*
    queue_name_notifications is an exception for consumer_logging, which processes
    two queues.
    */}}
    {{- range (list $app.queue_name $app.queue_name_notifications) }}
    {{- if . }}
    - type: rabbitmq
      metadata:
        protocol: amqp
        queueName: {{ . }}
        mode: QueueLength
        value: {{ $scaling.queue_length | quote }}
      authenticationRef:
        name: keda-trigger-auth-rabbitmq-conn
    {{- end }}
    {{- end }}
{{- end }}
{{- end }}
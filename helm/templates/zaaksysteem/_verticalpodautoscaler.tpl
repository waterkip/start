{{/*
Vertical Pod Autoscaler template
*/}}
{{- define "default.verticalpodautoscaler" -}}
{{- $ := index . 0 }}
{{- $app := index . 1 }}
{{- if $app.enabled -}}
apiVersion: autoscaling.k8s.io/v1
kind: VerticalPodAutoscaler
metadata:
  name: {{ include "fullname" (list $ $app.name) }}
  namespace: {{ $.Release.Namespace }}
spec:
  targetRef:
    apiVersion: "apps/v1"
    {{- if (and $app.verticalAutoscaler $app.verticalAutoscaler.kind) }}
    kind: {{ $app.verticalAutoscaler.kind }}
    {{- else }}
    kind: "Deployment"
    {{- end }}
    name: {{ include "fullname" (list $ $app.name) }}
  updatePolicy:
    updateMode: "Auto"
  {{- if (and $app.verticalAutoscaler $app.verticalAutoscaler.resourcePolicy) }}
  resourcePolicy:
    {{- $app.verticalAutoscaler.resourcePolicy | toYaml | nindent 4 }}
  {{- end }}
{{- end }}
{{- end }}

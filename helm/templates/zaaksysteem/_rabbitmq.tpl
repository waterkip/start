{{- define "zaaksysteem.rabbitmq_url" -}}
amqps://{{ .Values.global.rabbitmq.username }}:{{ .Values.global.rabbitmq.password }}@{{ .Values.global.rabbitmq.host }}:{{ .Values.global.rabbitmq.port }}/{{ .Values.global.rabbitmq.virtualhost }}
{{- end }}
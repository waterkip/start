{{/*
Pod distribution budget template
*/}}
{{- define "default.poddisruptionbudget" -}}
{{- $ := index . 0 }}
{{- $app := index . 1 }}
{{- if $app.enabled -}}
apiVersion: policy/v1
kind: PodDisruptionBudget
metadata:
  name: {{ include "fullname" (list $ $app.name) }}
spec:
  minAvailable: 50%
  selector:
    matchLabels:
      {{- include "selectorLabels" (list $ $app.name) | nindent 6 }}
{{- end }}
{{- end }}

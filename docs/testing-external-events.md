# Testing external events

For the testing of external events there are x steps needed described below

## Setting up the case type

In the case type the external event needs to be activated on the "acties" page
by selecting the event to be send under "Externe notificaties".

## Create a koppelling

In the beheer part of the zaaksysteem a new koppeling of the external
notification type needs to be added. One setting needs to be filled here:
Trigger URL with the url of the server within the network(not the exposed port)
If the example docker-compose is used the url should be:
http://http-listener:8888

## Create the server

TThe final part is the creation of the server that will receive the notifications
This can be done by using a existing image that echoes and logs the incoming
messages. This image needs to be added to the same network as the perl-api
by providing the same value for the networks variable.

Example docker-compose-override:
```yaml
    http-listener:
        image: mendhak/http-https-echo:29
        environment:
            - HTTP_PORT=8888
            - HTTPS_PORT=9999
        ports:
            - "8080:8888"
            - "8443:9999"
        networks:
            - "default"
```
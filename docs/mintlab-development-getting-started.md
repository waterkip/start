# Getting Started with the development process / Gitlab / Database Changes / tips / etc

Ensure you have the following in your `gitconfig` file. Gitconfigs can be
local (`.git/config`), global (`~/.gitconfig` or `~/.config/git/config`) and/or
in your worktrees.

example:

```Shell
$ cat ~/.gitconfig 
[...]
[merge]
   ff = only
```

This avoids unnessecary problems when using git pull/push, you can override
with `--no-ff`, but ask yourself: "do I really need to?"

## I - When starting new development

We use our `start` repository when coding. Depending on the type of bug or
feature, start from the correct branch. For bugfixes on production, make
sure you branch of  `preprod`, but in any other situation
you better branch of `master`.

We start by checking out the `master` branch, so your local working directory
is updated to match the selected branch.

example:

```bash
git checkout master
git pull
```

Create a new local branch and swith to the new local branch
(`git checkout -b [new branch name]`). The new branch name uses the format
`[gitlabusername]/MINTY-[NR]-description`, where:

- `[gitlabusername]` is the author's name (the gitlab username)
- `MINTY-[NR]` is the corresponding (unique) issue tracker ID: the MINTY JIRA
  ticket number
- `description` being the summary with special characters removed, whitespaces
  collapsed and replaced with an underscore, and the whole set to lowercase

`git checkout -b [gitlabusername]/MINTY-[NR]-description`

example:

`git checkout -b michielootjers/MINTY-1234-feat-add_document_to_a_case`

## II - When continuing development in feature branch

1. Start of day to ensure up to date code:

in feature branch:

```Shell
git fetch origin
git rebase -i origin master
```

See [tidy up your merge request](https://about.gitlab.com/blog/2019/02/07/start-using-git/)
for cleaning up commit messages.

2. [code... code... code...]

3. Make sure that **isort**, **black** and **ruff** have run on your code
   especially when your editor does not do this automatically!
   isort, black and ruff should be in your virtualenv when using start.git

	- isort (`alias is='isort'`)
	- black (`alias bl='black -l 79 --exclude \\.eggs\|\\.env\|\\.git .'`)

It is advisable to add ad git pre-commit hook for these command so you never accidentally send un-isorted, un-blacked and/or unflake'ed code into branches.

The CI scripts ensures that branches with un-sorted, un-formatted, un-linted code does not pass.

4. Add to git

```Shell
git add .
git commit -m "Good commit message"
git push
```

5. Check pipeline on gitlab for possible errors.

## III - Testing

 In the development process you've also created tests (right?).
 To make sure your tests run without failure and your code did not
 mess up existing code:

1)	Make sure you are in the correct virtual environment for your code.

2a)	Run all the tests with verbose output:

	```
	pytest -svvx
	```
	
	(-vv extra verbose , -s shows print("output") in tests,-x stop on first test failure. )

2b)	When all available tests are correct and you only want to run
	tests regarding your own code/tests you can give a path for
	test collection, eg:

	```
	pytest -vv ./tests/document/test_document_commands.py 
	```

3)	Make sure you have covered all possible code and code paths.

	```
	pytest -vv -cov --cov-report=html
	```

The output will be in: ~/start/zsnl-whatever/htmlcov/index.html

Running with coverage report takes significantly longer especially
for large codebases like zsnl_domains. So when all code is covered
and only existing tests fail you maybe don't want to use this every time
you run tests.

## IV - Merging to development

When in feature branch:

```Shell
git checkout development
```

When there are still any changes to be commited in your feature branch
take a look at them. There shouldn't be any after following the above steps for
developing. So either throw them out (git checkout changed-file), stash them
(git stash) for possible later use or reference, or commit them following
steps 3 to 6 in section II

```Shell
git pull --ff-only
```

(ff = only should already be in your ~/.gitconfig)

If there are any errors:

```Shell
git reset --hard origin/development
```

*note:* this throws away any local changes in the development branch, there
shouldn't usually be any anyway.

```Shell
git merge --no-ff <feature branch>
git push origin development
```

Check pipeline on CI/CD gitlab, should be ok since your work in the feature
branch was up to date when you followed steps in section II

## V - Creating a merge request

In Gitlab create Merge Request.

When creating a merge request make sure you have a correct MR title, eg:

```
MINTY-3411 As an asignee i can save e-mail to Documents as PDF
```

As long as the tests are not done or you are still developing on the new feature ensure that you have "WIP:" or "Draft:" in the name so it can't be merged  to master accidentally.

```
WIP: MINTY-3411: As an assignee i can save e-mail to Documents as PDF
```

2) "Delete branch after merge = true"

Then once tester gives an OK for new features:

3) Remove the `WIP:` or `Draft:` from the merge request title.

4) Ask review on Slack @development / @python / or any other means

5)
	a. Review comments given by the reviewer, and adjust code accordingly if needed.

	b. Commit any possible changes (see above)

6) Make sure you get an approval/thumb up from reviewer(s).

7) Merge to master on Gitlab.

## VI - If there are merge conflicts according to Gitlab on merge to master

When Gitlab complains that there are merge conflicts regarding merge to master,
you can look at the merge conflicts on the Gitlab page, but fix them locally:

In feature branch:

```Shell
git fetch
git rebase -i origin/master
```

This makes the changes in master become the new basis of your work in your feature branch. 

`-i` (interactive) gives you the ability to clean up your commit messages in your feature branch. See [Start Using Git on gitlab.com](https://about.gitlab.com/blog/2019/02/07/start-using-git/) for more information about cleaning up commit your messages

Follow the git cli guidelines about resolving, adding and continuing the rebase until done, this will result in a message from git:

```Shell
Successfully rebased and updated refs/heads/<YOUR-FEATURE-BRANCH>
```

After that you can force push your feature branch:

```Shell
git push -f origin <feature-branch>. 
```

## VII - Resetting development to master

In master:

```Shell
git fetch
git checkout development
git reset --hard origin/development
```

When not only local (this would mostly be in the beginning of the sprint)

```Shell
git push -f origin development
```

## IIX - Making database changes

- Make sure you have latest master

- Create feature branch of master

- in **/backend/perl-api/db/upgrade/NEXT/** create a file: **[pre/rel/post]-xxxx-description.sql **

  - use **pre** in case the database can be executed before installing the release. For example adding a new nullable column with a default value which will nog break the code
    Use **rel** in case the database change must be performed during a release.

    Use **post** in case the database change should be executed when installing the new version of the application.

  - **xxxx** is a followup number start with 1000 if nothing present and work up from 
    there, does not need to be +1 which could interfere with possible other db-change 
    commits in the sprint, also named xxxx + 1,  but at least a different higher number. 

- Fill the [your_filename].sql file with the database changes. Make sure to use transactions eg:

  ```
  BEGIN;
      ALTER TABLE x ADD COLUMN y .... ;
  COMMIT;
  ```

- Run the script `./backend/perl-api/dev-bin/update_database.sh backend/perl-api/db/upgrade/NEXT/[your filename].sql`. Execute this script from the repository root folder.

  In case the script fails, you may have to add this to your `docker-compose.override.yml` file:
  ```
    perl-api:
    build:
      target: development
  ```

  It will create a new database in the running postgres container, setup the database and applies the changes in [your_filename].sql in it. After that a new `template.sql` and `test-template.sql` file is generated. (be aware: this is not the zaaksysteem schema that the application does use (see step below how to update that database)). See `./backend/perl-api/db/README.md` for more details about the `template.sql` and `test-template.sql` files.

- If everything went right, the `update-database` script already `git add`ed them

  - *[your_filename].sql*
  - *template.sql*
  - *test-template.sql*
  - **.pm files (optional) . This are generated ORM perl classes that are generated based on the database structure.*

  in the feature branch and create a MR.

- Also make sure to execute the changes in your local zaaksysteem db. You can do this with the following command:

  First login into the database container and start psql in the zaaksyssteem database

  `docker-compose exec database psql -U zaaksysteem`

  in this container excecute the script that need to be applied:

  `\i /opt/zaaksysteem/db/upgrade/NEXT/[your_filename].sql`

- Make the correct changes in /backend/domains/database the schema files and maybe others depeding on the changes you made  (follow the usual development process for changes in zsnl_domains, feature-branch, MR, etc)

- Make sure that the database changes in the .sql file are correct, and make sure all stakeholders in this change/interrested parties are up-to-date about the database change. 

- Run the db changes using the script in `/backend/database`


### Rollout on environment

After changes are merged to a branch (master/preprod/production), these need to be applied "semi-manual".
To do this, you can make use of the script in backend/database/main.py. First make sure you create an virtual environment

- in a terminal, go to the path backend/database
- create the venv with this command; `python3 -m venv .venv`
- activate the venv; `source .venv/bin/activate`
- install the required libraries `pip install -r requirements.txt`
- run the script to apply db changes

usage: main.py [-h] [-f FILE] [--directory DIRECTORY] [-d DATABASE] [-c CREDENTIALS] -n {zaaksysteem-development,zaaksysteem-preprod,zaaksysteem-gov,zaaksysteem-com,gov,gov1,com,prd} [-w WORKERS]
               [-l {DEBUG,INFO,WARNING,ERROR,CRITICAL}]

### Modifications to your own changes

If by any chance a database change you made alters again within the sprint, but after a while (when things have been rolled out already), always add an extra alteration .sql file with an increased version number and a database alteration on your changes. For example a rename of a fields you created. So that database upgrades can always be done in sequence. 

By executing `zs zaaksysteem db` database changes are applied, which respects the agreed upon order: 1000-xxx.sql, pre-1000-xxxx.sql, rel-1000-xxx.sql and post-1000-xxxx.sql.)

Also because all 'interested parties'/stakeholders as mentioned above can in the meantime be a lot of different (non-)teammembers  above could have people end up locally with different database, depending wether or not they have applied your initial changes already on their local datbase. 

And _if_ unneeded .sql files are left over also git-rm them from development, since people may have pulled from development in the meantime, and are left with unneeded .sql files in their repo, which makes things less clear.

## IX - Changing version tags for zsnl modules

When MR's are ok and merged into master it is probable/possible that other modules need to depend on a newer 
version of said module

- Merge MR to master following above process (V and/or in case of database changes IIX)

	```
	git checkout master
	git pull --ff-only
	
	# Most likely "patch"
	# Version numbers use semantic versioning, eg: major.minor.patch notation
	bumpversion patch
	```
	

	```
	git push origin master v<new.version.number>
	```

After this the new version can be added in your requirements.txt files for other modules.

Note I: when pushing to development and you get merge conflicts regarding `requirements.txt`, take into account that `requirements.txt` in the development branch can always depend on development versions of other modules. 

Note II: `requirements.txt` in the master branch should, of course, never depend on development versions.


## X - Some usefull aliases, functions, terminal/CLI enhancements etc

- To show your current git branch in terminal and other utils:
	https://github.com/magicmonty/bash-git-prompt.git

- In Linux terminal set the terminal title, put in your ~/.bashrc:

	```
	# set title of current terminal 
	setTerminalTitle(){
	       echo -ne "\033]0;${1}\007"
	}
	```

- git diff with human readable line numbers. Put in your ~/.bashrc

	```
	gdiff() {
		git diff --color=always $1 | \
		gawk '{bare=$0;gsub("\033[[][0-9]*m","",bare)};\
	    	match(bare,"^@@ -([0-9]+),[0-9]+ [+]([0-9]+),[0-9]+ @@",a){left=a[1];right=a[2];next};\
		bare ~ /^(---|\+\+\+|[^-+ ])/{print;next};\
		{line=gensub("^(\033[[][0-9]*m)?(.)","\\2\\1",1,$0)};\
		bare~/^-/{print "-"left++ ":" line;next};\
		bare~/^[+]/{print "+"right++ ":" line;next};\
		{print "("left++","right++"):"line;next}'
	}
	```

- In your ~/.bash_aliases pick and choose:

	```
	alias is='isort -sg .eggs -sg .env -m 3 -tc -rc -ds -y'
	alias bl='black -l 79 --exclude \\.eggs\|\\.env\|\\.git .'
	alias ga='git add '
	alias gc='git commit -m '
	alias gd='git diff'
	alias grep='grep --color=auto'
	alias gs='git status'
	alias dc='docker-compose'
	alias zsdb='COMPOSE_FILE=~/your_start.git_base_dir/docker_compose.yml docker-compose exec database psql -U zaaksysteem'
	```

/* eslint-disable playwright/expect-expect */
import { BrowserContext, expect, Page, test } from '@playwright/test';
import {
  navigateAdministration,
  checkOrAddCategoryOrFolder,
  whatsChangedAndPublish,
} from './administration';
import {
  BSN2,
  generateApplicantName,
  generateApplicantNameToClick,
} from '../../testfiles/personData';
import { authPathBeheerder, testSettings } from '../../playwright.config';
import { testrondeData } from '../zs-employee/testrondeData';
import path from 'path';

const { options, baseUrl, typeDelay, longTimeout, shortTimeout } = testSettings;
const person = generateApplicantName(BSN2);
const personSearchName = generateApplicantNameToClick({
  BSN: BSN2,
});
const casetypes = [
  'Auto TESTRONDE Deelzaak V2',
  'Auto TESTRONDE Gerelateerde zaak V2',
  'Auto TESTRONDE Vervolgzaak V2',
  'Auto TESTRONDE V2',
];
const casetypeFileDir = 'casetypes';
const categoryOrFolder = 'Automatische E2E TESTRONDE';

async function setImportSettings(
  page: Page,
  toggleText: string,
  attributeType: string
) {
  await page
    .frameLocator('internal:attr=[title="Beheer formulier"i]')
    .getByText(`${toggleText}`)
    .first()
    .click();
  const locator = page
    .frameLocator('internal:attr=[title="Beheer formulier"i]')
    .locator(`//select/option[contains(.//text(),"- ${categoryOrFolder}")]`)
    .first();
  // eslint-disable-next-line playwright/no-conditional-in-test
  const optionNumber = (await locator.getAttribute('value')) || -1;
  await page
    .frameLocator('internal:attr=[title="Beheer formulier"i]')
    .locator('select[name="bibliotheek_categorie_id"]')
    .selectOption(optionNumber.toString());
  await page
    .frameLocator('iframe[title="Beheer formulier"]')
    .getByLabel(
      `Geselecteerde categorie voor alle nieuwe ${attributeType} gebruiken`
    )
    .check();
  await page
    .frameLocator('iframe[title="Beheer formulier"]')
    .locator('button[name="action"]')
    .click();

  await expect(
    page
      .frameLocator('iframe[title="Beheer formulier"]')
      .locator('#add')
      .getByText(`${attributeType} toe`)
  ).toBeHidden({ timeout: longTimeout });
}

Object.values(casetypes).forEach(casetype => {
  const casetypeFile = `${casetype.replace(/\s/g, '_')}.ztb`;
  test.describe.serial(`Import ${casetype} @db2`, () => {
    let page: Page;
    let context: BrowserContext;
    test.use({ storageState: authPathBeheerder });

    test.beforeAll(async ({ browser }) => {
      context = await browser.newContext(options);
      page = await context.newPage();

      await page.goto(baseUrl);
      await page.waitForLoadState('domcontentloaded');
    });

    test(`Start the import of casetype ${casetype}`, async ({}) => {
      await test.step(`Open admin section for import of ${casetype}`, async () => {
        await navigateAdministration({ page, menuItem: 'Catalogus' });
      });

      await test.step(`Check and if not present make category ${categoryOrFolder} for ${casetype}`, async () => {
        await checkOrAddCategoryOrFolder({
          page,
          categoryOrFolder: `${categoryOrFolder}`,
        });
      });

      await test.step(`Click import for ${casetype}`, async () => {
        await page.getByTestId('AddIcon').nth(1).click();
        await page.getByRole('button', { name: 'Importeren' }).click();
        await page.waitForLoadState('domcontentloaded');
      });

      await test.step(`Select Zaaktypen for ${casetype}`, async () => {
        await page
          .frameLocator('internal:attr=[title="Beheer formulier"i]')
          .getByLabel('Zaaktypen')
          .check();
      });

      await test.step(`Select Zaaksysteem.nl ${casetype}`, async () => {
        await page
          .frameLocator('internal:attr=[title="Beheer formulier"i]')
          .getByLabel('Zaaksysteem.nl')
          .check();
      });

      await test.step(`Add file ${casetype}`, async () => {
        const fileChooserPromise = page.waitForEvent('filechooser');
        await page
          .frameLocator('internal:attr=[title="Beheer formulier"i]')
          .getByRole('button', { name: 'Kies bestand' })
          .click();
        const fileChooser = await fileChooserPromise;
        await fileChooser.setFiles(
          path.join(
            __dirname,
            casetypeFileDir,
            `${casetype.replace(/ /g, '_')}.ztb`
          )
        );
      });

      await test.step(`Wait for upload then click volgende ${casetype}`, async () => {
        await expect(
          page
            .frameLocator('internal:attr=[title="Beheer formulier"i]')
            .getByText(`${casetypeFile}`)
        ).toBeVisible();
        await page
          .frameLocator('internal:attr=[title="Beheer formulier"i]')
          .getByRole('button', { name: 'Volgende' })
          .click();
      });

      await test.step(`Expect heading Zaaktypen to be visible for ${casetype}`, async () => {
        await expect(
          page
            .frameLocator('internal:attr=[title="Beheer formulier"i]')
            .getByRole('heading', { name: 'Zaaktypen' })
        ).toBeVisible();
      });
    });

    if (casetype.endsWith('TESTRONDE V2')) {
      test(`Set main settings for ${casetype}`, async () => {
        await test.step(`Select folder for casetype for ${casetype}`, async () => {
          const casetypeIsNew = await page
            .frameLocator('internal:attr=[title="Beheer formulier"i]')
            .getByText('Voeg meegeleverd zaaktype toe')
            .isVisible();
          // eslint-disable-next-line playwright/no-conditional-in-test
          casetypeIsNew
            ? await setImportSettings(page, `${casetype} (MAIN)`, 'zaaktype')
            : '';
        });

        await test.step(`Select folder for templates for ${casetype}`, async () => {
          await setImportSettings(
            page,
            'Auto alle kenmerken als DOCX',
            'sjabloon'
          );
        });

        await test.step(`Select folder for notifications for ${casetype}`, async () => {
          await setImportSettings(
            page,
            'Auto Testronde registratie betrokkene',
            'bericht'
          );
        });

        await test.step(`Select betrokkene for ${casetype}`, async () => {
          await page
            .frameLocator('internal:attr=[title="Beheer formulier"i]')
            .getByText('Natuurlijk persoon')
            .click();
          await page
            .frameLocator('iframe[title="Beheer formulier"]')
            .getByPlaceholder('Typ hier uw zoekterm')
            .click();
          await page
            .frameLocator('iframe[title="Beheer formulier"]')
            .getByPlaceholder('Typ hier uw zoekterm')
            .pressSequentially(person);
          try {
            await expect(
              page
                .frameLocator('iframe[title="Beheer formulier"]')
                .getByText(personSearchName)
            ).toBeVisible({ timeout: shortTimeout });
          } catch {
            await page
              .frameLocator('iframe[title="Beheer formulier"]')
              .getByPlaceholder('Typ hier uw zoekterm')
              .pressSequentially(person);
            await expect(
              page
                .frameLocator('iframe[title="Beheer formulier"]')
                .getByText(personSearchName)
            ).toBeVisible();
          }
          await page
            .frameLocator('iframe[title="Beheer formulier"]')
            .getByPlaceholder('Typ hier uw zoekterm')
            .press('ArrowDown');
          await page
            .frameLocator('iframe[title="Beheer formulier"]')
            .getByPlaceholder('Typ hier uw zoekterm')
            .press('Enter');
          await page
            .frameLocator('iframe[title="Beheer formulier"]')
            .locator('button[name="action"]')
            .click();

          await expect(
            page
              .frameLocator('iframe[title="Beheer formulier"]')
              .locator('#add')
              .getByText('Voeg meegeleverde betrokkene rol toe')
          ).toBeHidden({ timeout: longTimeout });
        });
      });
    }

    test(`Import casetype and check import for ${casetype}`, async ({}) => {
      await test.step(`Import casetype ${casetype}`, async () => {
        await page
          .frameLocator('internal:attr=[title="Beheer formulier"i]')
          .getByRole('button', { name: 'Importeren' })
          .click();
        await page.waitForLoadState('domcontentloaded');
      });

      await test.step(`Check adding of ${casetype}`, async () => {
        await page.getByRole('link', { name: 'Catalogus Catalogus' }).click();
        await page.getByPlaceholder('Zoeken in de catalogus').click();
        await page.getByPlaceholder('Zoeken in de catalogus').fill(casetype);
        await page
          .getByPlaceholder('Zoeken in de catalogus')
          .press('Enter', { delay: typeDelay });
        await page.waitForLoadState('domcontentloaded');
        const indexMax = 5;
        for (let index = 0; index < indexMax; index++) {
          try {
            await expect(
              page.getByRole('link', { name: `${casetype}` })
            ).toBeVisible({ timeout: shortTimeout });
            index = indexMax;
          } catch {
            await page.reload({ waitUntil: 'domcontentloaded' });
          }
        }
      });
    });

    if (!casetype.endsWith('TESTRONDE V2')) {
      test(`Set object relation attribute for imported ${casetype}`, async ({}) => {
        await test.step(`Open ${casetype}`, async () => {
          await page.getByText(casetype, { exact: true }).click();
        });

        await test.step(`Goto phase 2 to add v2 relation object attribute for ${casetype}`, async () => {
          await page
            .frameLocator('iframe[title="Beheer formulier"]')
            .getByRole('link', { name: '2', exact: true })
            .click();
        });

        await test.step(`Add v2 relation object attribute for ${casetype}`, async () => {
          await page
            .frameLocator('iframe[title="Beheer formulier"]')
            .getByRole('link', { name: ' Kenmerk' })
            .scrollIntoViewIfNeeded();
          await page
            .frameLocator('iframe[title="Beheer formulier"]')
            .getByRole('link', { name: ' Kenmerk' })
            .click();
          await page
            .frameLocator('iframe[title="Beheer formulier"]')
            .getByRole('textbox')
            .click();
          await page
            .frameLocator('iframe[title="Beheer formulier"]')
            .getByRole('textbox')
            .fill('auto_relatie_kenmerk_testronde_v2_object');
          await page
            .frameLocator('iframe[title="Beheer formulier"]')
            .getByRole('button', { name: 'Zoeken' })
            .click();
          await expect(
            page
              .frameLocator('iframe[title="Beheer formulier"]')
              .getByRole('textbox')
          ).toBeHidden();
          await page
            .frameLocator('iframe[title="Beheer formulier"]')
            .getByRole('tabpanel', { name: 'Resultaten' })
            .getByRole('cell', {
              name: 'auto_relatie_kenmerk_testronde_v2_object',
            })
            .click();
        });

        await test.step(`Remove imported v2 relation object attribute for ${casetype}`, async () => {
          await page
            .frameLocator('iframe[title="Beheer formulier"]')
            .getByRole('cell', {
              name: ' Naam: auto_relatie_kenmerk_testronde_v2_object Titel: TESTRONDE V2 object Type: Relatie Magic string: [[auto_relatie_kenmerk_testronde_v2_object]] Gepubliceerde versie: 1 Actuele versie: 1  ',
            })
            .getByRole('link', { name: '' })
            .first()
            .click();
          await page.waitForLoadState('domcontentloaded');
        });

        await test.step(`Open to edit v2 relation object attribute for ${casetype}`, async () => {
          await page
            .frameLocator('iframe[title="Beheer formulier"]')
            .getByRole('cell', {
              name:
                ' Naam: auto_relatie_kenmerk_testronde_v2_object Titel: ' +
                '- Type: Relatie Magic string: [[auto_relatie_kenmerk_testronde_v2_object]] ' +
                'Gepubliceerde versie: niet beschikbaar Actuele versie: 1  ',
            })
            .getByRole('link', { name: '' })
            .click();
        });

        await test.step(`Set mandatory for v2 relation object attribute for ${casetype}`, async () => {
          await page
            .frameLocator('iframe[title="Beheer formulier"]')
            .locator('input[name="kenmerken_value_mandatory"]')
            .check();
        });

        await test.step(`Set title for v2 relation object attribute for ${casetype}`, async () => {
          await page
            .frameLocator('iframe[title="Beheer formulier"]')
            .locator('input[name="kenmerken_label"]')
            .click();
          await page
            .frameLocator('iframe[title="Beheer formulier"]')
            .locator('input[name="kenmerken_label"]')
            .fill(
              testrondeData.auto_relatie_kenmerk_testronde_v2_object.title !==
                undefined
                ? testrondeData.auto_relatie_kenmerk_testronde_v2_object.title
                : ''
            );
        });

        await test.step(`Check show and edit on PIP for v2 relation object attribute for ${casetype}`, async () => {
          await page
            .frameLocator('iframe[title="Beheer formulier"]')
            .locator('input[name="kenmerken_pip"]')
            .check();
          await page
            .frameLocator('iframe[title="Beheer formulier"]')
            .locator('input[name="kenmerken_pip_can_change"]')
            .check();
        });

        await test.step(`Set external help for v2 relation object attribute for ${casetype}`, async () => {
          await page
            .frameLocator('iframe[title="Beheer formulier"]')
            .locator('#quill_kenmerken_help_extern')
            .getByRole('paragraph')
            .click();
          await page
            .frameLocator('iframe[title="Beheer formulier"]')
            .locator('#quill_kenmerken_help_extern div')
            .first()
            .fill(
              testrondeData.auto_relatie_kenmerk_testronde_v2_object
                .explanationExternal !== undefined
                ? testrondeData.auto_relatie_kenmerk_testronde_v2_object
                    .explanationExternal
                : ''
            );
        });

        await test.step(`Check button make object for v2 relation object attribute for ${casetype}`, async () => {
          await page
            .frameLocator('iframe[title="Beheer formulier"]')
            .locator('input[name="kenmerken_custom_object_create"]')
            .check();
        });

        await test.step(`Set setting button make object title for v2 relation object attribute for ${casetype}`, async () => {
          await page
            .frameLocator('iframe[title="Beheer formulier"]')
            .getByPlaceholder('Aanmaken Auto TESTRONDE Objecttype V2')
            .click();
          await page
            .frameLocator('iframe[title="Beheer formulier"]')
            .getByPlaceholder('Aanmaken Auto TESTRONDE Objecttype V2')
            .fill('Object V2 aanmaken via relatiekenmerk');
        });
      });

      Object.values(testrondeData)
        .filter(attribute => attribute.inObject)
        .forEach(attribute => {
          test(`Add prefill of attribute ${attribute.nameAttribute} in object relation attribute for ${casetype}`, async ({}) => {
            await test.step(`Add prefill of attribute ${attribute.nameAttribute} in object relation attribute for ${casetype}`, async () => {
              await page
                .frameLocator('iframe[title="Beheer formulier"]')
                .locator(
                  `input[name="kenmerken_custom_object_attr_${attribute.magicString}"]`
                )
                .click();
              // eslint-disable-next-line playwright/no-conditional-in-test
              attribute.magicString === 'auto_tst_o0001_object_naam'
                ? await page
                    .frameLocator('iframe[title="Beheer formulier"]')
                    .locator(
                      `input[name="kenmerken_custom_object_attr_${attribute.magicString}"]`
                    )
                    .fill('Geef hier een naam die de objecttitel uniek maakt')
                : await page
                    .frameLocator('iframe[title="Beheer formulier"]')
                    .locator(
                      `input[name="kenmerken_custom_object_attr_${attribute.magicString}"]`
                    )
                    .fill(`[[${attribute.magicString}]]`);
            });
          });
        });

      test(`Finish and save for ${casetype}`, async ({}) => {
        await test.step(`Save the V2 relation object settings for ${casetype}`, async () => {
          await page
            .frameLocator('iframe[title="Beheer formulier"]')
            .getByRole('button', { name: 'Opslaan' })
            .click();
        });

        await test.step(`Fill changed and save for ${casetype}`, async () => {
          await whatsChangedAndPublish({
            page,
            whatsChangedArray: ['Kenmerken'],
            publishText: `V2 object added in ${casetype}`,
            testScriptName: casetype,
          });
        });
      });
    }
  });
});

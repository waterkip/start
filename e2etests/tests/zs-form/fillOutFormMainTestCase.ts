/* eslint-disable playwright/no-conditional-in-test */
/* eslint-disable playwright/no-networkidle */
import { Page, expect, test } from '@playwright/test';
import { executeSteps } from '../../utils/executeSteps';
import {
  expectAttributeValueOnForm,
  expectAttributeValueOnOverview,
  fillAttribute,
} from './formFillingAndCaseRegistration';
import { testrondeData } from '../zs-employee/testrondeData';
import { testSettings } from '../../playwright.config';

const { typeDelay } = testSettings;

const addressLines =
  '1114AD 1051JLH.J.E. Wenckebachweg 90Donker Curtiusstraat 7';
const firstPageAttributes = Object.values(testrondeData).filter(
  attribute => attribute.onPages?.toString().includes('form1')
);
const secondPageAttributes = Object.values(testrondeData).filter(
  attribute => attribute.onPages?.toString().includes('form2')
);
const overviewPageAttributes = Object.values(testrondeData).filter(
  attribute => attribute.onPages?.toString().includes('overview')
);
const firstPageDefaultedAttributes = firstPageAttributes.filter(
  attribute => attribute.valueDefault !== undefined
);
const secondPageDefaultedAttributes = secondPageAttributes.filter(
  attribute => attribute.valueDefault !== undefined
);
const firstPageNonFixedAttributes = firstPageAttributes.filter(attribute => {
  return attribute.afterChoiceValueFixed !== true;
});
const overviewPageCheckableAttributes = overviewPageAttributes.filter(
  attribute => {
    return (
      attribute.inputType !== 'Geolocatie' &&
      attribute.inputType !== 'Locatie met kaart'
    );
  }
);

async function clickNext(formPage: Page) {
  const button = formPage.getByRole('button', { name: 'Volgende' }).first();
  await button.scrollIntoViewIfNeeded();
  await button.press('Enter');
  await formPage.waitForLoadState('domcontentloaded');
}

interface PropsMainTestCase {
  formPage: Page;
  formName: string;
}

export const fillOutFormMainTestCaseWithoutRules = async ({
  formPage,
  formName,
}: PropsMainTestCase) => {
  await test.step(`Check if test is on first page form ${formName} for choice Nee`, async () => {
    const formHeader = formPage.url().includes('intern')
      ? formPage
          .locator('form[name="case-register-group-form"]')
          .getByText('Datum van de test + release tag')
      : formPage.locator('h2');

    await formPage.waitForLoadState('domcontentloaded');
    await expect(formHeader).toHaveText('Datum van de test + release tag');
  });

  await executeSteps(
    attribute =>
      `Ensure default value of ${attribute.nameAttribute} is present`,
    firstPageDefaultedAttributes,
    expectAttributeValueOnForm(formPage)
  );

  await executeSteps(
    attribute =>
      `Fill value for ${attribute.nameAttribute} on ${formName} for choice Nee`,
    firstPageNonFixedAttributes,
    fillAttribute(formPage)
  );

  await test.step(`Go to next page on ${formName} for choice Nee`, async () => {
    await clickNext(formPage);
    await expect(formPage.getByText('Vul ze allemaal in!')).toBeVisible();
  });

  await test.step(`Ensure address is visible before rules execute on ${formName} for choice Nee`, async () => {
    // eslint-disable-next-line playwright/no-conditional-in-test
    if (await formPage.getByText('3311BV Meer Veld toevoegen').isVisible()) {
      await formPage.getByText('3311BV Meer Veld toevoegen').click();
    }
    await expect(formPage.getByText(addressLines)).toBeVisible();
  });

  await test.step(`TEMP fix for default value amount not being filled for choice Nee`, async () => {
    if (formPage.url().includes('webform')) {
      await formPage.getByLabel('auto_tst_k1023_valuta').click();
      await formPage
        .getByLabel('auto_tst_k1023_valuta')
        .fill(testrondeData.auto_tst_k1023_valuta?.valueDefault ?? '');
      await formPage
        .getByLabel('auto_tst_k1023_valuta')
        .press('Tab', { delay: typeDelay });
    }
  });

  await executeSteps(
    attribute =>
      `Check default value of ${attribute.nameAttribute} on ${formName} for choice Nee`,
    secondPageDefaultedAttributes,
    expectAttributeValueOnForm(formPage)
  );

  await test.step("Set 'Nee' to K1016 field, so that rules don't trigger", async () => {
    await formPage
      .getByText('auto_tst_k1016_enkelvoudige_keuze')
      .scrollIntoViewIfNeeded();
    await formPage.getByLabel('Nee').click();
  });

  await test.step(`Ensure address is visible after rules were not triggered`, async () => {
    // eslint-disable-next-line playwright/no-conditional-in-test
    if (await formPage.getByText('3311BV Meer Veld toevoegen').isVisible()) {
      await formPage.getByText('3311BV Meer Veld toevoegen').click();
    }
    await expect(formPage.getByText(addressLines)).toBeVisible();
  });

  await executeSteps(
    attribute =>
      `Ensure no change of default value of ${attribute.nameAttribute} on ${formName} for choice Nee`,
    secondPageDefaultedAttributes,
    expectAttributeValueOnForm(formPage)
  );

  await executeSteps(
    attribute =>
      `Fill value for ${attribute.nameAttribute} on ${formName} for choice Nee`,
    secondPageAttributes,
    fillAttribute(formPage)
  );

  await test.step('Go to overview page on ${formName} for choice Nee', async () => {
    await clickNext(formPage);
    formPage.url().includes('intern')
      ? await formPage.waitForSelector('.allocation-picker-option-button')
      : await expect(formPage.getByText('auto_tst_k1023_valuta')).toBeVisible();
  });

  await executeSteps(
    attribute =>
      `Check value of ${attribute.nameAttribute} on the overview page on ${formName} for choice Nee`,
    overviewPageCheckableAttributes,
    expectAttributeValueOnOverview(formPage)
  );
};

export const fillOutFormMainTestCaseWithRules = async ({
  formPage,
  formName,
}: PropsMainTestCase) => {
  await executeSteps(
    attribute =>
      `Ensure default value of ${attribute.nameAttribute} is present in ${formName} for choice Ja`,
    firstPageDefaultedAttributes,
    expectAttributeValueOnForm(formPage)
  );
  await test.step(`Check if test is on first page form ${formName} for choice Ja`, async () => {
    const formHeader = formPage.url().includes('intern')
      ? formPage
          .locator('form[name="case-register-group-form"]')
          .getByText('Datum van de test + release tag')
      : formPage.locator('h2');

    await expect(formHeader).toHaveText('Datum van de test + release tag');
  });

  await executeSteps(
    attribute =>
      `Fill value for ${attribute.nameAttribute} in ${formName} for choice Ja`,
    firstPageNonFixedAttributes,
    fillAttribute(formPage)
  );

  await test.step(`Go to next page on ${formName} for choice Ja`, async () => {
    await clickNext(formPage);
    await formPage.waitForLoadState('domcontentloaded');
    await expect(formPage.getByText('Vul ze allemaal in!')).toBeVisible();
  });

  await test.step(`Ensure address is visibile before rules execute`, async () => {
    // eslint-disable-next-line playwright/no-conditional-in-test
    if (await formPage.getByText('3311BV Meer Veld toevoegen').isVisible()) {
      await formPage.getByText('3311BV Meer Veld toevoegen').click();
    }
    await expect(formPage.getByText(addressLines)).toBeVisible();
  });

  await test.step(`TEMP fix for default value amount not being filled for choice Ja`, async () => {
    if (formPage.url().includes('webform')) {
      await formPage.getByLabel('auto_tst_k1023_valuta').click();
      await formPage
        .getByLabel('auto_tst_k1023_valuta')
        .fill(testrondeData.auto_tst_k1023_valuta?.valueDefault ?? '');
      await formPage
        .getByLabel('auto_tst_k1023_valuta')
        .press('Tab', { delay: typeDelay });
    }
  });

  await executeSteps(
    attribute =>
      `Check default value of ${attribute.nameAttribute} on ${formName} for choice Ja`,
    secondPageDefaultedAttributes,
    expectAttributeValueOnForm(formPage)
  );

  await test.step("Set 'Ja' to K1016 field, so that rules do trigger", async () => {
    await formPage
      .getByText('auto_tst_k1016_enkelvoudige_keuze')
      .scrollIntoViewIfNeeded();
    await formPage.getByLabel('Ja', { exact: true }).click();
    await formPage.getByLabel('Ja', { exact: true }).press('Enter');
  });

  await test.step('Ensure address is not visible after rules were triggered', async () => {
    // eslint-disable-next-line playwright/no-conditional-in-test
    if (await formPage.getByText('3311BV Meer Veld toevoegen').isVisible()) {
      await formPage.getByText('3311BV Meer Veld toevoegen').click();
    }
    await expect(formPage.getByText(addressLines)).toBeHidden();
  });

  await executeSteps(
    attribute =>
      `Check rules value of ${attribute.nameAttribute} on ${formName} for choice Ja`,
    secondPageDefaultedAttributes,
    expectAttributeValueOnForm(formPage, true)
  );

  await executeSteps(
    attribute =>
      `Fill value for ${attribute.nameAttribute} on ${formName} for choice Ja`,
    secondPageAttributes,
    fillAttribute(formPage)
  );

  await test.step(`Go to overview page on ${formName} for choice Ja`, async () => {
    await clickNext(formPage);
    formPage.url().includes('intern')
      ? await formPage.waitForSelector('.allocation-picker-option-button')
      : await expect(formPage.getByText('auto_tst_k1023_valuta')).toBeVisible();
  });

  await executeSteps(
    attribute =>
      `Check value of ${attribute.nameAttribute} on the overview page on ${formName} for choice Ja`,
    overviewPageCheckableAttributes,
    expectAttributeValueOnOverview(formPage, true)
  );
};

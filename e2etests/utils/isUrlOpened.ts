import { expect, Page } from '@playwright/test';

export const isUrlOpened = (page: Page, urlTitle: string) =>
  expect(page).toHaveTitle(urlTitle);

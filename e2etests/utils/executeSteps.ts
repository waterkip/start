import { test } from '@playwright/test';

export async function executeSteps<T>(
  titleFn: (arg: T) => string,
  xs: T[],
  fn: (arg: T) => Promise<void>
) {
  for (const el of xs) {
    // eslint-disable-next-line playwright/valid-title
    await test.step(titleFn(el), async () => {
      await fn(el);
    });
  }
}

/* eslint-disable prettier/prettier */
import { expect, Page } from '@playwright/test';
import { locators } from './locators';
import { BSN1, generateApplicantName, generateApplicantNameToClick } from '../testfiles/personData';

export const createCase = async ({
  page,
  caseType = 'Basiszaaktype dun',
  applicantType = 'Persoon',
  BSN = BSN1,
  channel = 'email',
}: {
  page: Page;
  caseType?: string;
  applicantType?: string;
  BSN?: string;
  channel?: string;
}) => {
  const applicantName = generateApplicantName(BSN);
  const applicantNameToClick = generateApplicantNameToClick({ BSN });
  await page.locator('[aria-label="Groene plus knop menu openen"]').click();
  try {
    await expect(
      page.getByRole('menuitem', { name: 'Zaak aanmaken' })
    ).toBeVisible();
  } catch {
    await page.locator('[aria-label="Groene plus knop menu openen"]').click();
  }
  await page.getByRole('menuitem', { name: 'Zaak aanmaken' }).click();
  await page.getByLabel('Zaaktype*').fill(caseType);
  await page.locator(locators.resultFound(caseType)).click();
  await page.getByLabel(applicantType, { exact: true }).click();
  await page.getByLabel(`Aanvrager (${applicantType.toLowerCase()})*`).click();
  await page
    .getByLabel(`Aanvrager (${applicantType.toLowerCase()})* `)
    .pressSequentially(applicantName);
  await page.waitForResponse(res => res.status() === 200);
  await expect(page.getByRole('button', { name: `${applicantNameToClick}` })).toBeVisible();
  await page.getByRole('button', { name: `${applicantNameToClick}` }).click();
  await expect(page.getByText(`${applicantNameToClick}`).first()).toBeVisible();
  await page
    .getByRole('combobox', { name: 'ContactkanaalDit veld is verplicht' })
    .selectOption(`string:${channel}`);
  await page.getByText('Volgende').click();
  await page.waitForLoadState('domcontentloaded');
  // eslint-disable-next-line playwright/no-networkidle
  await page.waitForLoadState('networkidle');
};

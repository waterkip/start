import { defineConfig, devices } from '@playwright/test';
import path from 'path';
import { Settings, TestSettings } from './types';

export const authPathBeheerder = path.join(
  __dirname,
  'playwright/.auth/beheerder.json'
);
export const authPathPerson = path.join(
  __dirname,
  'playwright/.auth/person.json'
);

const noSettingsWarning = () => {
  console.warn('using default settings!');
  console.warn(
    'create a e2etests/settings.json file to override default settings'
  );
};

export const defaultSettings: Required<TestSettings> = {
  environment: 'default',
  pageTitle: 'Development :: Zaaksysteem.nl',
  baseDomain: 'development.zaaksysteem.nl',
  baseUrl: 'https://development.zaaksysteem.nl/',
  adminUser: 'admin',
  adminPassword: 'admin',
  consumerUser: 'gebruiker',
  consumerPassword: 'gebruiker',
  beheerderUser: 'beheerder',
  beheerderPassword: 'beheerder',
  personAuthentication: {
    bsn: '024515061',
    loginForm: 'Betaling internetkassa Ingenico automatische test',
  },
  clickDelay: 200,
  clickDelaySlow: 1500,
  shortTimeout: process.env.CI ? 3000 : 1500,
  superShortTimeout: 800,
  longTimeout: 10000,
  extraLongTimeout: 15000,
  superLongTimeout: 30000,
  softErrorMessage: 'This could be a timing error!!!',
  typeDelaySlow: 300,
  typeDelay: 50,
  options: {
    locale: 'nl-NL',
    viewport: {
      width: 1200,
      height: 600,
    },
    deviceScaleFactor: 0,
  },
};

let customSettings: TestSettings | null = null;
// Get the branchname from the Gitlab pipeline if it is available
// Or locally set like export CI_COMMIT_REF_SLUG=development
const branchName = process.env?.CI_COMMIT_REF_SLUG ?? 'default';

// Try to load the settings.json. This file is used to override the defaultSettings
try {
  const settings = require('./settings.json') as Settings;
  console.log(`get settings for branch ${branchName}`);
  customSettings =
    settings?.branch?.[branchName] ?? settings?.branch?.default ?? null;

  if (!customSettings) noSettingsWarning();
} catch (error) {
  noSettingsWarning();
}

// Combine the defaultSettings and the customSettings.
// The customSettings override the defaultSettings
export const testSettings: Required<TestSettings> = {
  ...defaultSettings,
  ...customSettings,
  environment: branchName,
};

/**
 * See https://playwright.dev/docs/test-configuration.
 */
export default defineConfig({
  testDir: './tests',
  timeout: 120 * 1000,
  globalTimeout: process.env.CI ? 2000 * 1000 : 900 * 1000,
  expect: {
    timeout: 10000,
  },
  fullyParallel: true,
  forbidOnly: !!process.env.CI,
  retries: 0,
  workers: process.env.CI ? 1 : undefined,
  reporter: [
    ['list'],
    ['line'],
    ['html', { open: 'never', outputFolder: './tests/test-reports/' }],
    ['./utils/MyReporter.ts'],
  ],
  use: {
    baseURL: testSettings.baseUrl,
    locale: 'nl-NL',
    video: 'on',
    // video: process.env.CI ? 'retain-on-failure' : 'on',
    screenshot: 'only-on-failure',
    actionTimeout: 0,
    trace: process.env.CI ? 'retain-on-failure' : 'on',
    ignoreHTTPSErrors: true,
  },
  outputDir: './tests/test-screenshots/',
  projects: [
    {
      name: 'setup',
      testMatch: '**/*.setup.ts',
    },
    {
      name: 'e2e tests on chromium',
      dependencies: ['setup'],
      use: { ...devices['Desktop Chrome'] },
    },
    {
      name: 'logout beheerder',
      testMatch: '**/auth.beheerder.logout.ts',
      dependencies: ['e2e tests on chromium'],
      use: {
        storageState: authPathBeheerder,
        ...devices['Desktop Chrome'],
      },
    },
  ],
});

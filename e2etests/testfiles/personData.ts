import { testSettings } from '../playwright.config';

const { environment } = testSettings;
export interface Person {
  BSN: string;
  firstNames: string;
  insertions: string;
  familyName: string;
  nobleTitle: string;
  birthDate: string;
  gender: string;
  residenceCountry: string;
  residenceStreet: string;
  residenceZipcode: string;
  residenceHouseNumber: string;
  residenceHouseLetter: string;
  residenceHouseNumberSuffix: string;
  residenceCity: string;
  foreignAddress1: string;
  foreignAddress2: string;
  foreignAddress3: string;
  addressInCommunity: string;
  hasCorrespondenceAddress: string;
  correspondenceCountry: string;
  correspondenceStreet: string;
  correspondenceZipcode: string;
  correspondenceHouseNumber: string;
  correspondenceHouseLetter: string;
  correspondenceHouseNumberSuffix: string;
  correspondenceCity: string;
  phoneNumber: string;
  mobileNumber: string;
  email: string;
  preferenceChannel: string;
  internalNote: string;
}
interface PersonData {
  [environment: string]: {
    [BSN: string]: Person;
  };
}

export const BSN1 = '010082426';
export const BSN2 = '547608238';
export const BSN3 = '132915947';
export const BSN4 = '999990032';

const personData: PersonData = {
  development: {
    '010082426': {
      BSN: '010082426',
      firstNames: 'Gerard',
      insertions: '',
      familyName: 'Kuipers',
      nobleTitle: 'Bardon',
      birthDate: '14-05-1956',
      gender: 'Man',
      residenceCountry: 'Nederland',
      residenceStreet: 'Vaewribzxuaoqevti',
      residenceZipcode: '1721KB',
      residenceHouseNumber: '88',
      residenceHouseLetter: 'Y',
      residenceHouseNumberSuffix: 'IB',
      residenceCity: 'Testgemeente',
      foreignAddress1: '',
      foreignAddress2: '',
      foreignAddress3: '',
      addressInCommunity: 'true',
      hasCorrespondenceAddress: 'false',
      correspondenceCountry: '',
      correspondenceStreet: '',
      correspondenceZipcode: '',
      correspondenceHouseNumber: '',
      correspondenceHouseLetter: '',
      correspondenceHouseNumberSuffix: '',
      correspondenceCity: '',
      phoneNumber: '0512345667',
      mobileNumber: '0699887700',
      email: 'burgerdiakriet@xxllnc.nl',
      preferenceChannel: 'email',
      internalNote:
        'DO NOT USE OR CHANGE!!! This person is used in the automated e2e test.',
    },
    '547608238': {
      BSN: '547608238',
      firstNames: 'Jan',
      insertions: '',
      familyName: 'Janssen',
      nobleTitle: 'Bardon',
      birthDate: '10-05-1954',
      gender: 'Man',
      residenceCountry: 'België',
      residenceStreet: '',
      residenceZipcode: '',
      residenceHouseNumber: '',
      residenceHouseLetter: '',
      residenceHouseNumberSuffix: '',
      residenceCity: '',
      foreignAddress1: 'Rue de Gozée 706',
      foreignAddress2: '6110 Montigny-le-Tilleul',
      foreignAddress3: 'België',
      addressInCommunity: 'true',
      hasCorrespondenceAddress: 'true',
      correspondenceCountry: 'Nederland',
      correspondenceStreet: 'Ozyweyvbydptzskit',
      correspondenceZipcode: '',
      correspondenceHouseNumber: '19',
      correspondenceHouseLetter: 'S',
      correspondenceHouseNumberSuffix: 'PC',
      correspondenceCity: 'Testgemeente',
      phoneNumber: '0301234567',
      mobileNumber: '0687654321',
      email: 'burgerdiakriet@xxllnc.nl',
      preferenceChannel: 'webformulier',
      internalNote:
        'DO NOT USE OR CHANGE!!! This person is used in the automated e2e test.',
    },
    '132915947': {
      BSN: '132915947',
      firstNames: 'Chris',
      insertions: '',
      familyName: 'Brouwer',
      nobleTitle: 'Bardon',
      birthDate: '12-05-1922',
      gender: 'Man',
      residenceCountry: 'Nederland',
      residenceStreet: 'Cgukahfjlxclhlkck',
      residenceZipcode: '1020JZ',
      residenceHouseNumber: '40',
      residenceHouseLetter: 'S',
      residenceHouseNumberSuffix: 'FI',
      residenceCity: 'Testgemeente',
      foreignAddress1: '',
      foreignAddress2: '',
      foreignAddress3: '',
      addressInCommunity: 'true',
      hasCorrespondenceAddress: 'false',
      correspondenceCountry: '',
      correspondenceStreet: '',
      correspondenceZipcode: '',
      correspondenceHouseNumber: '',
      correspondenceHouseLetter: '',
      correspondenceHouseNumberSuffix: '',
      correspondenceCity: '',
      phoneNumber: '06123456789',
      mobileNumber: '',
      email: 'test@xxllnc.nl',
      preferenceChannel: 'telefoon',
      internalNote:
        'DO NOT USE OR CHANGE!!! This person is used in the automated e2e test.',
    },
    '999990032': {
      BSN: '999990032',
      firstNames: 'Mihailović',
      insertions: '',
      familyName: 'Bilgiç',
      nobleTitle: 'Sultan',
      birthDate: '09-10-1992',
      gender: 'Man',
      residenceCountry: 'Nederland',
      residenceStreet: 'Lavendelweg',
      residenceZipcode: '9731HM',
      residenceHouseNumber: '60',
      residenceHouseLetter: '',
      residenceHouseNumberSuffix: '',
      residenceCity: 'Groningen',
      foreignAddress1: '',
      foreignAddress2: '',
      foreignAddress3: '',
      addressInCommunity: 'true',
      hasCorrespondenceAddress: 'true',
      correspondenceCountry: 'Nederland',
      correspondenceStreet: 'Indischestraat',
      correspondenceZipcode: '2022VN',
      correspondenceHouseNumber: '1',
      correspondenceHouseLetter: 'A',
      correspondenceHouseNumberSuffix: 'ZW',
      correspondenceCity: 'Haarlem',
      phoneNumber: '0687654321',
      mobileNumber: '',
      email: 'burgerdiakriet@xxllnc.nl',
      preferenceChannel: 'webformulier',
      internalNote:
        'DO NOT USE OR CHANGE!!! This person is to be removed in the automated e2e test.',
    },
  },
  preprod: {
    '010082426': {
      BSN: '010082426',
      firstNames: 'Delajah',
      insertions: '',
      familyName: 'Vos',
      nobleTitle: 'Bardon',
      birthDate: '10-05-1972',
      gender: 'Man',
      residenceCountry: 'Nederland',
      residenceStreet: 'Tvpprcjdcaisglgoo',
      residenceZipcode: '1476WT',
      residenceHouseNumber: '93',
      residenceHouseLetter: 'G',
      residenceHouseNumberSuffix: 'KG',
      residenceCity: 'Sint-Michielsgestel',
      foreignAddress1: '',
      foreignAddress2: '',
      foreignAddress3: '',
      addressInCommunity: 'true',
      hasCorrespondenceAddress: 'false',
      correspondenceCountry: '',
      correspondenceStreet: '',
      correspondenceZipcode: '',
      correspondenceHouseNumber: '',
      correspondenceHouseLetter: '',
      correspondenceHouseNumberSuffix: '',
      correspondenceCity: '',
      phoneNumber: '0512345667',
      mobileNumber: '0699887700',
      email: 'burgerdiakriet@xxllnc.nl',
      preferenceChannel: 'email',
      internalNote:
        'DO NOT USE OR CHANGE!!! This person is used in the automated e2e test.',
    },
    '547608238': {
      BSN: '547608238',
      firstNames: 'Bert',
      insertions: '',
      familyName: 'Schouten',
      nobleTitle: 'Bardon',
      birthDate: '16-05-1908',
      gender: 'Man',
      residenceCountry: 'Nederland',
      residenceStreet: 'Ozyweyvbydptzskit',
      residenceZipcode: '3311BV',
      residenceHouseNumber: '19',
      residenceHouseLetter: '',
      residenceHouseNumberSuffix: 'PC',
      residenceCity: 'Testgemeente',
      foreignAddress1: '',
      foreignAddress2: '',
      foreignAddress3: '',
      addressInCommunity: 'true',
      hasCorrespondenceAddress: 'true',
      correspondenceCountry: 'Nederland',
      correspondenceStreet: 'Vhpzqwpznggbmpchh',
      correspondenceZipcode: '',
      correspondenceHouseNumber: '56',
      correspondenceHouseLetter: 'Y',
      correspondenceHouseNumberSuffix: 'ZY',
      correspondenceCity: 'Sint-Michielsgestel',
      phoneNumber: '0301234567',
      mobileNumber: '0687654321',
      email: 'burgerdiakriet@xxllnc.nl',
      preferenceChannel: 'webformulier',
      internalNote:
        'DO NOT USE OR CHANGE!!! This person is used in the automated e2e test.',
    },
    '132915947': {
      BSN: '132915947',
      firstNames: 'Sven',
      insertions: '',
      familyName: 'Springintveld',
      nobleTitle: 'Bardon',
      birthDate: '18-05-1999',
      gender: 'Man',
      residenceCountry: '',
      residenceStreet: '',
      residenceZipcode: '',
      residenceHouseNumber: '',
      residenceHouseLetter: '',
      residenceHouseNumberSuffix: '',
      residenceCity: '',
      foreignAddress1: '',
      foreignAddress2: '',
      foreignAddress3: '',
      addressInCommunity: 'true',
      hasCorrespondenceAddress: 'true',
      correspondenceCountry: 'Nederland',
      correspondenceStreet: 'Lhmyvwjffrhhtcdvr',
      correspondenceZipcode: '',
      correspondenceHouseNumber: '9',
      correspondenceHouseLetter: 'F',
      correspondenceHouseNumberSuffix: 'MW',
      correspondenceCity: 'Sint-Michielsgestel',
      phoneNumber: '06123456789',
      mobileNumber: '',
      email: 'test@xxllnc.nl',
      preferenceChannel: 'telefoon',
      internalNote:
        'DO NOT USE OR CHANGE!!! This person is used in the automated e2e test.',
    },
    '999990032': {
      BSN: '999990032',
      firstNames: 'Mihailović',
      insertions: '',
      familyName: 'Bilgiç',
      nobleTitle: 'Sultan',
      birthDate: '09-10-1992',
      gender: 'Man',
      residenceCountry: 'Nederland',
      residenceStreet: 'Lavendelweg',
      residenceZipcode: '9731HM',
      residenceHouseNumber: '60',
      residenceHouseLetter: '',
      residenceHouseNumberSuffix: '',
      residenceCity: 'Groningen',
      foreignAddress1: '',
      foreignAddress2: '',
      foreignAddress3: '',
      addressInCommunity: 'true',
      hasCorrespondenceAddress: 'true',
      correspondenceCountry: 'Nederland',
      correspondenceStreet: 'Indischestraat',
      correspondenceZipcode: '2022VN',
      correspondenceHouseNumber: '1',
      correspondenceHouseLetter: 'A',
      correspondenceHouseNumberSuffix: 'ZW',
      correspondenceCity: 'Haarlem',

      phoneNumber: '0687654321',
      mobileNumber: '',
      email: 'burgerdiakriet@xxllnc.nl',
      preferenceChannel: 'webformulier',
      internalNote:
        'DO NOT USE OR CHANGE!!! This person is to be removed in the automated e2e test.',
    },
  },
  master: {
    '010082426': {
      BSN: '010082426',
      firstNames: 'Delajah',
      insertions: '',
      familyName: 'Vos',
      nobleTitle: 'Bardon',
      birthDate: '10-05-1972',
      gender: 'Man',
      residenceCountry: 'Nederland',
      residenceStreet: 'Tvpprcjdcaisglgoo',
      residenceZipcode: '1476WT',
      residenceHouseNumber: '93',
      residenceHouseLetter: 'G',
      residenceHouseNumberSuffix: 'KG',
      residenceCity: 'Sint-Michielsgestel',
      foreignAddress1: '',
      foreignAddress2: '',
      foreignAddress3: '',
      addressInCommunity: 'true',
      hasCorrespondenceAddress: 'false',
      correspondenceCountry: '',
      correspondenceStreet: '',
      correspondenceZipcode: '',
      correspondenceHouseNumber: '',
      correspondenceHouseLetter: '',
      correspondenceHouseNumberSuffix: '',
      correspondenceCity: '',
      phoneNumber: '0512345667',
      mobileNumber: '0699887700',
      email: 'burgerdiakriet@xxllnc.nl',
      preferenceChannel: 'email',
      internalNote:
        'DO NOT USE OR CHANGE!!! This person is used in the automated e2e test.',
    },
    '547608238': {
      BSN: '547608238',
      firstNames: 'Chris',
      insertions: '',
      familyName: 'Springintveld',
      nobleTitle: 'Bardon',
      birthDate: '14-05-1991',
      gender: 'Man',
      residenceCountry: 'Nederland',
      residenceStreet: '',
      residenceZipcode: '',
      residenceHouseNumber: '',
      residenceHouseLetter: '',
      residenceHouseNumberSuffix: 'PC',
      residenceCity: 'Testgemeente',
      foreignAddress1: '',
      foreignAddress2: '',
      foreignAddress3: '',
      addressInCommunity: 'true',
      hasCorrespondenceAddress: 'true',
      correspondenceCountry: 'Nederland',
      correspondenceStreet: 'Tncpthdokciviwmhi',
      correspondenceZipcode: '',
      correspondenceHouseNumber: '74',
      correspondenceHouseLetter: 'S',
      correspondenceHouseNumberSuffix: 'PQ',
      correspondenceCity: 'Sint-Michielsgestel',
      phoneNumber: '0301234567',
      mobileNumber: '0687654321',
      email: 'burgerdiakriet@xxllnc.nl',
      preferenceChannel: 'webformulier',
      internalNote:
        'DO NOT USE OR CHANGE!!! This person is used in the automated e2e test.',
    },
    '132915947': {
      BSN: '132915947',
      firstNames: 'Theo',
      insertions: '',
      familyName: 'Brouwer',
      nobleTitle: 'Bardon',
      birthDate: '11-05-1951',
      gender: 'Man',
      residenceCountry: 'Nederland',
      residenceStreet: 'Bgiwdcziviaizynnc',
      residenceZipcode: '1697GN',
      residenceHouseNumber: '97',
      residenceHouseLetter: 'Y',
      residenceHouseNumberSuffix: 'ZJ',
      residenceCity: 'Sint-Michielsgestel',
      foreignAddress1: '',
      foreignAddress2: '',
      foreignAddress3: '',
      addressInCommunity: 'true',
      hasCorrespondenceAddress: 'false',
      correspondenceCountry: '',
      correspondenceStreet: '',
      correspondenceZipcode: '',
      correspondenceHouseNumber: '',
      correspondenceHouseLetter: '',
      correspondenceHouseNumberSuffix: '',
      correspondenceCity: '',
      phoneNumber: '06123456789',
      mobileNumber: '',
      email: 'burgerdiakriet@xxllnc.nl',
      preferenceChannel: 'telefoon',
      internalNote:
        'DO NOT USE OR CHANGE!!! This person is used in the automated e2e test.',
    },
    '999990032': {
      BSN: '999990032',
      firstNames: 'Mihailović',
      insertions: '',
      familyName: 'Bilgiç',
      nobleTitle: 'Sultan',
      birthDate: '09-10-1992',
      gender: 'Man',
      residenceCountry: 'Nederland',
      residenceStreet: 'Lavendelweg',
      residenceZipcode: '9731HM',
      residenceHouseNumber: '60',
      residenceHouseLetter: '',
      residenceHouseNumberSuffix: '',
      residenceCity: 'Groningen',
      foreignAddress1: '',
      foreignAddress2: '',
      foreignAddress3: '',
      addressInCommunity: 'true',
      hasCorrespondenceAddress: 'true',
      correspondenceCountry: 'Nederland',
      correspondenceStreet: 'Indischestraat',
      correspondenceZipcode: '2022VN',
      correspondenceHouseNumber: '1',
      correspondenceHouseLetter: 'A',
      correspondenceHouseNumberSuffix: 'ZW',
      correspondenceCity: 'Haarlem',
      phoneNumber: '0687654321',
      mobileNumber: '',
      email: 'burgerdiakriet@xxllnc.nl',
      preferenceChannel: 'webformulier',
      internalNote:
        'DO NOT USE OR CHANGE!!! This person is to be removed in the automated e2e test.',
    },
  },
  dev: {
    '010082426': {
      BSN: '010082426',
      firstNames: 'Erika Automatische Regressietest',
      insertions: 'de',
      familyName: 'Goede',
      nobleTitle: 'Gravin',
      birthDate: '01-04-1987',
      gender: 'Anders',
      residenceCountry: 'Nederland',
      residenceStreet: 'Grotemarkt',
      residenceZipcode: '3011PA',
      residenceHouseNumber: '22',
      residenceHouseLetter: '',
      residenceHouseNumberSuffix: '',
      residenceCity: 'Rotterdam',
      foreignAddress1: '',
      foreignAddress2: '',
      foreignAddress3: '',
      addressInCommunity: 'true',
      hasCorrespondenceAddress: 'false',
      correspondenceCountry: '',
      correspondenceStreet: '',
      correspondenceZipcode: '',
      correspondenceHouseNumber: '',
      correspondenceHouseLetter: '',
      correspondenceHouseNumberSuffix: '',
      correspondenceCity: '',
      phoneNumber: '0512345667',
      mobileNumber: '0699887700',
      email: 'burgerdiakriet@xxllnc.nl',
      preferenceChannel: 'email',
      internalNote:
        'DO NOT USE OR CHANGE!!! This person is used in the automated e2e test.',
    },
    '132915947': {
      BSN: '132915947',
      firstNames: 'Ŗî Ãō Øū Ŋÿ Ği ŢžŰŲ ŜŞőĠĪ Ŷŵ Ĉŷ',
      insertions: "over 't",
      familyName: "T.Śar ŃĆ ĹāÑ ŤÙmön ĊéŴÀŅŇĩ Ļl'ÁÚŘŠĎÉ Pomme- d' Or ĽÒÓĢÛŨ",
      nobleTitle: 'Bardon',
      birthDate: '01-01-2010',
      gender: 'Man',
      residenceCountry: 'Nederland',
      residenceStreet: 'Yhkqlkrfgmborzyuk',
      residenceZipcode: '1234AA',
      residenceHouseNumber: '49',
      residenceHouseLetter: '',
      residenceHouseNumberSuffix: 'BU',
      residenceCity: 'Testgemeente',
      foreignAddress1: '',
      foreignAddress2: '',
      foreignAddress3: '',
      addressInCommunity: 'false',
      hasCorrespondenceAddress: 'false',
      correspondenceCountry: '',
      correspondenceStreet: '',
      correspondenceZipcode: '',
      correspondenceHouseNumber: '',
      correspondenceHouseLetter: '',
      correspondenceHouseNumberSuffix: '',
      correspondenceCity: '',
      phoneNumber: '0105123455',
      mobileNumber: '0623456789',
      email: 'burgerdiakriet@xxllnc.nl',
      preferenceChannel: 'persoonlijke Internetpagina',
      internalNote:
        'DO NOT USE OR CHANGE!!! This person is used in the automated e2e test.',
    },
    '547608238': {
      BSN: '547608238',
      firstNames: 'Françoise Automatische Regressietest',
      insertions: "sûr 't",
      familyName: 'Brăiloiu',
      nobleTitle: '',
      birthDate: '18-06-1980',
      gender: 'Vrouw',
      residenceCountry: 'België',
      residenceStreet: '',
      residenceZipcode: '',
      residenceHouseNumber: '',
      residenceHouseLetter: '',
      residenceHouseNumberSuffix: '',
      residenceCity: '',
      foreignAddress1: 'Rue de Gozée 706',
      foreignAddress2: '6110 Montigny-le-Tilleul',
      foreignAddress3: 'België',
      addressInCommunity: 'false',
      hasCorrespondenceAddress: 'false',
      correspondenceCountry: '',
      correspondenceStreet: '',
      correspondenceZipcode: '',
      correspondenceHouseNumber: '',
      correspondenceHouseLetter: '',
      correspondenceHouseNumberSuffix: '',
      correspondenceCity: '',
      phoneNumber: '0301234567',
      mobileNumber: '0687654321',
      email: 'burgerdiakriet@xxllnc.nl',
      preferenceChannel: 'E-mail',
      internalNote:
        'DO NOT USE OR CHANGE!!! This person is used in the automated e2e test.',
    },
    '999990032': {
      BSN: '999990032',
      firstNames: 'Mihailović',
      insertions: '',
      familyName: 'Bilgiç',
      nobleTitle: 'Sultan',
      birthDate: '09-10-1992',
      gender: 'Man',
      residenceCountry: 'Nederland',
      residenceStreet: 'Lavendelweg',
      residenceZipcode: '9731HM',
      residenceHouseNumber: '60',
      residenceHouseLetter: '',
      residenceHouseNumberSuffix: '',
      residenceCity: 'Groningen',
      foreignAddress1: '',
      foreignAddress2: '',
      foreignAddress3: '',
      addressInCommunity: 'true',
      hasCorrespondenceAddress: 'true',
      correspondenceCountry: 'Nederland',
      correspondenceStreet: 'Indischestraat',
      correspondenceZipcode: '2022VN',
      correspondenceHouseNumber: '1',
      correspondenceHouseLetter: 'A',
      correspondenceHouseNumberSuffix: 'ZW',
      correspondenceCity: 'Haarlem',
      phoneNumber: '0687654321',
      mobileNumber: '',
      email: 'burgerdiakriet@xxllnc.nl',
      preferenceChannel: 'webformulier',
      internalNote:
        'DO NOT USE OR CHANGE!!! This person is to be removed in the automated e2e test.',
    },
  },
};

export const personDataEnvironment =
  environment === 'preprod'
    ? personData.preprod
    : environment === 'master'
      ? personData.master
      : environment === 'development'
        ? personData.development
        : personData.dev;

export const generateApplicantName = (BSN: string) => {
  const suffix =
    personDataEnvironment[BSN].insertions !== ''
      ? ` ${personDataEnvironment[BSN].insertions} `
      : ' ';
  const birthdateNotOnDev =
    environment !== 'dev' && environment !== 'default'
      ? `  ${personDataEnvironment[BSN].birthDate}`
      : '';
  return (
    personDataEnvironment[BSN].firstNames.split(' ')[0] +
    suffix +
    personDataEnvironment[BSN].familyName +
    birthdateNotOnDev
  );
};

interface ApplicantNameToClick {
  BSN: string;
  withBirthdate?: boolean;
}

export const generateApplicantNameToClick = ({
  BSN,
  withBirthdate = true,
}: ApplicantNameToClick) => {
  const suffix =
    personDataEnvironment[BSN].insertions !== ''
      ? ` ${personDataEnvironment[BSN].insertions} `
      : ' ';
  const birthdateNotOnDev =
    environment !== 'dev' && environment !== 'default' && withBirthdate
      ? ` (${personDataEnvironment[BSN].birthDate}`
      : '';
  const firstNameArray = personDataEnvironment[BSN].firstNames.split(' ');
  let initials = '';
  firstNameArray.forEach(firstName => {
    initials = `${initials}${firstName.charAt(0).toUpperCase()}.`;
  });
  return (
    initials +
    suffix +
    personDataEnvironment[BSN].familyName +
    birthdateNotOnDev
  );
};

import { BrowserContextOptions } from '@playwright/test';

export interface LoginUser {
  name: string;
  password: string;
}

export interface TestSettings {
  environment: string;
  pageTitle: string;
  baseDomain: string;
  baseUrl: string;
  beheerderUser: string;
  beheerderPassword: string;
  adminUser?: string;
  adminPassword?: string;
  consumerUser?: string;
  consumerPassword?: string;
  personAuthentication: {
    bsn: string;
    loginForm: string;
  };
  clickDelay?: number;
  clickDelaySlow?: number;
  shortTimeout?: number;
  superShortTimeout?: number;
  longTimeout?: number;
  extraLongTimeout?: number;
  superLongTimeout?: number;
  softErrorMessage?: string;
  typeDelaySlow?: number;
  typeDelay?: number;
  options?: BrowserContextOptions;
}

export interface Settings {
  description: [string];
  branch: {
    default: TestSettings;
    [branchName: string]: TestSettings;
  };
}

stages:

  - Start
  - Before_Prepare
  - Prepare
  - Tag
  - QA
  - Build
  - BuildSecondPhase
  - Verify
  - PrepareDeploy
  - Release
  - Deploy
  - E2etests
  - Cleanup

wait-for-other-pipelines-to-finish:       # This job runs in the build stage, which runs first.
  stage: Start
  tags: [ "xxllnc-shared", "arch:x86_64" ]
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule" && $RUN_E2E_TESTS
    - if: $CI_COMMIT_BRANCH =~ /^(master|production|preprod|development)$/
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
  image:
    name: registry.gitlab.com/python-gitlab/python-gitlab:latest
    entrypoint: [""]
  variables:
    PYTHONUNBUFFERED: 1
  script:
    - echo "Check if other pipelines are running and wait until they finish"
    - python util/wait_for_pipeline.py

# Get a new version number and add it as a tag to the commit.
# The version is saved in a .env file so it can be used 
# as an artifact in the downstream project.
set-version-number-and-tag-commit:
  stage: Prepare
  tags: [ "xxllnc-shared", "arch:x86_64" ]
  image: ${CI_REGISTRY_IMAGE}/util/semantic_release:latest
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule" || $CI_COMMIT_TAG'
      when: never
    - if: $CI_COMMIT_BRANCH =~ /^(master|production|preprod|development)$/
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
  script:
    - |-
      if [[ "${CI_COMMIT_BRANCH}" =~ ^(master|preprod|production)$ ]]; 
      then 
        echo "On release branch, using semantic version to create version number"

        echo "Fallback version, this is used when no new version could be determined"
        export CURRENT_VERSION=$(git describe --tags --abbrev=0)
        echo ${CURRENT_VERSION%-*}-${CI_PIPELINE_IID} > version.txt

        npx semantic-release -t \${version}-${CI_COMMIT_REF_SLUG}
        echo "VERSION=$(cat version.txt)-${CI_COMMIT_REF_SLUG}" > version_${CI_PIPELINE_IID}.env
      else
        echo "Not on a release branch so using CI_PIPELINE_IDD to create a version number"
        echo "VERSION=0.0.${CI_PIPELINE_IID}-${CI_COMMIT_REF_SLUG}" > version_${CI_PIPELINE_IID}.env
      fi;
    - cat version_${CI_PIPELINE_IID}.env
  artifacts:
    reports:
      dotenv: version_${CI_PIPELINE_IID}.env

include:
  - "/template.gitlab-ci.yml"
  - "/backend/background-tasks/background-tasks.gitlab-ci.yml"
  - "/backend/bi-dumper/bi-dumper.gitlab-ci.yml"
  - "/backend/consumer-cm/cm.gitlab-ci.yml"
  - "/backend/consumer-communication/communication.gitlab-ci.yml"
  - "/backend/consumer-document/document.gitlab-ci.yml"
  - "/backend/consumer-document_processing/document_processing.gitlab-ci.yml"
  - "/backend/consumer-geo/geo.gitlab-ci.yml"
  - "/backend/consumer-jobs/jobs.gitlab-ci.yml"
  - "/backend/consumer-logging/logging.gitlab-ci.yml"
  - "/backend/consumer-notification/notification.gitlab-ci.yml"
  - "/backend/consumer-queue_runner/queue_runner.gitlab-ci.yml"
  - "/backend/database-cleaner/database-cleaner.gitlab-ci.yml"
  - "/backend/domains/domains.gitlab-ci.yml"
  - "/backend/http-admin/admin.gitlab-ci.yml"
  - "/backend/http-auth/auth.gitlab-ci.yml"
  - "/backend/http-cm/cm.gitlab-ci.yml"
  - "/backend/http-communication/communication.gitlab-ci.yml"
  - "/backend/http-document/document.gitlab-ci.yml"
  - "/backend/http-documentconverter/documentconverter.gitlab-ci.yml" # partly using template
  - "/backend/http-geo/geo.gitlab-ci.yml"
  - "/backend/http-jobs/jobs.gitlab-ci.yml"
  - "/backend/http-style/style.gitlab-ci.yml"
  - "/backend/http-virusscanner/virusscanner.gitlab-ci.yml" # partly using template
  - "/backend/migration_libraries/migration_libraries.gitlab-ci.yml"
  - "/backend/olo-syncer/olo-syncer.gitlab-ci.yml"
  - "/backend/p5-bttw-tools/p5-bttw-tools.gitlab-ci.yml"
  - "/backend/p5-service-http-library/p5-service-http-library.gitlab-ci.yml"
  - "/backend/perl-api/perl-api-api2csv.gitlab-ci.yml"
  - "/backend/perl-api/perl-api-frontend.gitlab-ci.yml" # partly using template
  - "/backend/perl-api/perl-api.gitlab-ci.yml" # partly using template
  - "/backend/zsnl-pyramid/pyramid.gitlab-ci.yml"
  - "/backend/minty-amqp/minty-amqp.gitlab-ci.yml"
  - "/backend/minty-infra-amqp/infra-amqp.gitlab-ci.yml"
  - "/backend/minty-infra-email/infra-email.gitlab-ci.yml"
  - "/backend/minty-infra-misc/infra-misc.gitlab-ci.yml"
  - "/backend/minty-infra-sqlalchemy/infra-sqlalchemy.gitlab-ci.yml"
  - "/backend/minty-infra-storage/infra-storage.gitlab-ci.yml"
  - "/backend/minty/minty.gitlab-ci.yml"
  - "/backend/minty-pyramid/minty-pyramid.gitlab-ci.yml"
  - "/backend/timed-events/timed-events.gitlab-ci.yml"
  - "/frontend-mono/frontend.gitlab-ci.yml" # partly using template
  - "/helm/helm.gitlab-ci.yml"
  - "/util/cleanup_gitlab_containers/cleanup_gitlab_container.gitlab-ci.yml"
  - "/util/semantic_release/semantic-release.gitlab-ci.yml"

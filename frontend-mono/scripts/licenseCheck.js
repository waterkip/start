// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { exec } from 'child_process';
import { init } from 'license-checker';

exec('git diff master -- yarn.lock', (err, stdout) => {
  console.log(stdout);
  if (stdout) {
    console.log('Dependencies change detected, checking licences...');
    init({ start: process.cwd() }, function (err, packages) {
      if (err) {
        throw 'Cannot access dependency licenses';
      } else {
        const agpl = Object.values(packages).find(pckg =>
          pckg.licenses.includes('agpl')
        );

        if (agpl) {
          throw 'AGPL license detected';
        }
      }
    });
  }
});

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { resolve, dirname } from 'path';
import { fileURLToPath } from 'url';
import tpl from '../../webpack.config.tpl.js';

const thisFileDirectory = dirname(fileURLToPath(import.meta.url));

export default tpl({
  name: 'external-components',
  publicUrlFrag: '',
  srcPath: resolve(thisFileDirectory, 'src'),
  indexPath: resolve(thisFileDirectory, 'src', 'index.tsx'),
  htmlPath: resolve(thisFileDirectory, 'public', 'index.html'),
  rootPath: thisFileDirectory,
  tsConfigPath: resolve('tsconfig.json'),
});

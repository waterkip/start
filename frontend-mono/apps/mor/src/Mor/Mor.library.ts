// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useParams } from 'react-router-dom';
import {
  CaseTypeSettingsType,
  FormatAttributeType,
  FormatCaseType,
  FormatCaseTypeSettingType,
  FormatIntegrationType,
  GetCasesType,
  ParamsType,
  UseUrlType,
  getIntegrationType,
} from './Mor.types';
import { fetchCases, fetchIntegration } from './Mor.requests';

const DAY = 1000 * 60 * 60 * 24;

export const useUrl: UseUrlType = () => {
  const { view, caseNumber } = useParams() as ParamsType;

  return { view, caseNumber };
};

const formatAttribute: FormatAttributeType = ({
  object: { column_name, value_type },
}) => ({
  magic_string: column_name.replace('attribute.', ''),
  type: value_type,
});

const formatCaseTypeSetting: FormatCaseTypeSettingType = ({
  casetype: { id, object_id },
  casetype_subject_attribute,
  casetype_case_attributes,
}) => ({
  uuid: id,
  id: object_id,
  subjectAttr: formatAttribute(casetype_subject_attribute),
  specialAttrs: casetype_case_attributes.map(formatAttribute),
});

export const formatIntegration: FormatIntegrationType = ({
  result: {
    instance: {
      interface_config: {
        title,
        casetypes,
        header_bgcolor: colorMain,
        accent_color: colorAccent,
        header_bgimage: headerImage,
        app_help_uri: helpUrl,
      },
    },
  },
}) => ({
  title,
  caseTypeSettings: casetypes
    .filter(({ casetype }) => Boolean(casetype))
    .map(formatCaseTypeSetting),
  headerImage,
  colorMain,
  colorAccent,
  helpUrl,
});

export const getIntegration: getIntegrationType = async () => {
  const response = await fetchIntegration();
  const integration = formatIntegration(response);

  return integration;
};

export const formatCase: FormatCaseType = (
  {
    instance: {
      id: uuid,
      number: caseNumber,
      subject: caseSubject,
      casetype: {
        reference: caseTypeUuid,
        instance: { name: caseTypeName, version: caseTypeVersion },
      },
      case_location: location,
      status,
      date_target,
      date_of_completion,
      attributes,
      result_id: resultId,
    },
  },
  now,
  integration
) => {
  const number = `${caseNumber}`;
  const targetDate = new Date(date_target).valueOf();
  const completionDate = new Date(date_of_completion).valueOf();
  const time =
    status === 'resolved' ? targetDate - completionDate : targetDate - now;
  const urgency = Math.floor(time / DAY);

  const { caseTypeSettings } = integration;
  const caseTypeSetting = caseTypeSettings.find(
    ({ uuid }) => uuid === caseTypeUuid
  ) as CaseTypeSettingsType;

  const {
    subjectAttr: { magic_string },
  } = caseTypeSetting;
  const attrSubject = attributes[magic_string][0];
  const subject = attrSubject || caseSubject || caseTypeName;

  const filterText = [number, subject, location].join(' ').toLowerCase();

  return {
    // overview
    number,
    subject,
    location,
    urgency,
    filterText,
    // caseview
    uuid,
    caseTypeSetting,
    caseTypeVersion,
    status,
    values: attributes,
    resultId,
  };
};

export const getCases: GetCasesType = async (integration, zql) => {
  const response = await fetchCases(zql);

  const {
    result: {
      instance: {
        pager: { total_rows },
        rows,
      },
    },
  } = response;

  const now = new Date().valueOf();

  return {
    count: total_rows,
    cases: rows.map((row: any) => formatCase(row, now, integration)),
  };
};

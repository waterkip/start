// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useCaseViewStyles = makeStyles(
  ({ palette: { common }, transitions }: Theme) => ({
    wrapper: {
      position: 'absolute',
      height: '100%',
      right: 0,
      zIndex: 1,
      backgroundColor: common.white,
      width: '100%',
      overflow: 'hidden',
      display: 'flex',
      flexDirection: 'column',
    },
    open: {
      width: '100%',
      transition: transitions.create(
        // @ts-ignore
        ['width'],
        {
          easing: transitions.easing.sharp,
          duration: transitions.duration.shortest,
        }
      ),
    },
    closed: {
      width: 0,
      transition: transitions.create(
        // @ts-ignore
        ['width'],
        {
          easing: transitions.easing.sharp,
          duration: transitions.duration.shortest,
        }
      ),
    },
    content: {
      flexGrow: 1,
    },
  })
);

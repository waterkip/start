// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { Typography } from '@mui/material';
import { AttributeType } from '../CaseView.types';
import { useContentStyles } from './Content.styles';

type AttributePropsType = {
  attr: AttributeType;
};

const Attribute: React.ComponentType<AttributePropsType> = ({
  attr: { label, value, resultLabel },
}) => {
  const classes = useContentStyles();

  return (
    <div className={classes.attribute}>
      <Typography variant="h6">{label}</Typography>
      {Array.isArray(value) ? (
        <ul style={{ margin: 0, paddingLeft: 20 }}>
          {value.map(val => (
            <li key={val}>{val}</li>
          ))}
        </ul>
      ) : (
        <span>{resultLabel || value}</span>
      )}
    </div>
  );
};

export default Attribute;

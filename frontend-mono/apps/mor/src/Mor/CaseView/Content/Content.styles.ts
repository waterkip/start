// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

const buttonHeight = 50;

export const useContentStyles = makeStyles(
  ({ palette: { elephant }, mintlab: { shadows }, transitions }: Theme) => ({
    wrapper: {
      width: '100%',
      overflowY: 'auto',
      display: 'flex',
      flexDirection: 'column',
      gap: 10,
      padding: 20,
      flexGrow: 1,
    },
    button: {
      width: '100%',
      '&>*': {
        width: '100%',
        height: buttonHeight,
      },
    },
    form: {
      display: 'flex',
      flexDirection: 'column',
      padding: 20,
      backgroundColor: elephant.lighter,
      boxShadow: shadows.insetTop,
      borderRadius: 8,
    },
    formOpen: {
      maxHeight: 3000,
      overflow: 'unset',
      paddingTop: 20,
      paddingBottom: 20,
      transition: transitions.create(
        // @ts-ignore
        ['max-height', 'padding-top', 'padding-bottom'],
        {
          easing: transitions.easing.sharp,
          duration: transitions.duration.enteringScreen,
        }
      ),
    },
    formClosed: {
      maxHeight: 0,
      paddingTop: 0,
      paddingBottom: 0,
      overflow: 'hidden',
      transition: transitions.create(
        // @ts-ignore
        ['max-height', 'padding-top', 'padding-bottom'],
        {
          easing: transitions.easing.sharp,
          duration: transitions.duration.enteringScreen,
        }
      ),
    },
    specialAttributes: {
      display: 'flex',
      flexDirection: 'column',
      gap: 20,
      padding: 20,
      backgroundColor: elephant.lighter,
      boxShadow: shadows.insetTop,
      borderRadius: 8,
      '&>:first-child': {
        display: 'flex',
        flexDirection: 'row',
        gap: 10,
      },
    },
    attributes: {
      display: 'flex',
      flexDirection: 'column',
      gap: 20,
    },
    attribute: {
      display: 'flex',
      flexDirection: 'column',
      '&>:first-child': {
        color: elephant.dark,
      },
    },
    document: {
      display: 'flex',
      flexDirection: 'row',
      gap: 10,
      marginTop: 5,
      alignItems: 'center',
    },
  })
);

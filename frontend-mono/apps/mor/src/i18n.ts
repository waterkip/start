// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

//@ts-ignore
import { initI18n } from '@zaaksysteem/common/src/i18n';
import { initReactI18next } from 'react-i18next';
import locale from './mor.locale';

export async function setupI18n() {
  return initI18n(
    {
      lng: 'nl',
      keySeparator: '.',
      defaultNS: 'mor',
      fallbackNS: 'mor',
      resources: locale,
      interpolation: {
        escapeValue: false,
      },
    },
    [initReactI18next]
  );
}

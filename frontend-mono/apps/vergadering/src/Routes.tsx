// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
//@ts-ignore
import ErrorBoundary from '@zaaksysteem/common/src/components/ErrorBoundary/ErrorBoundary';
import { useAppStyle } from './App.style';
import MeetingsModule from './Meetings';

export interface RoutesPropsType {
  prefix: string;
}

const MeetingsRoutes: React.ComponentType<RoutesPropsType> = ({ prefix }) => {
  const classes = useAppStyle();

  return (
    <div className={classes.app}>
      <ErrorBoundary>
        <Router>
          <Routes>
            <Route path={`${prefix}/*`} element={<MeetingsModule />} />
          </Routes>
        </Router>
      </ErrorBoundary>
    </div>
  );
};

export default MeetingsRoutes;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

type RawViewType = 'per_meeting' | 'not_per_meeting';
export type ViewType = 'open' | 'resolved';
export type GroupingType = 'grouped' | 'ungrouped';
export type SetGroupingType = (grouping: GroupingType) => void;

export type ParamsType = {
  shortName: string;
  view: ViewType;
  caseNumber: string;
};

export type UseUrlType = () => ParamsType;

type RawAttributeMapType = {
  checked: 0 | 1;
  internal_name: {
    searchable_object_label_public: string;
    searchable_object_id: string;
  };
  external_name: string;
};

type AttributeMapType = {
  checked?: boolean;
  magicString: string;
  label: string;
  isSubject?: boolean;
  isDocument?: boolean;
};

type RawAttributeType = {
  object: {
    column_name: string;
  };
};

type LinkType = {
  label: string;
  link: string;
};

export type GetMagicStringType = (attribute: RawAttributeType) => string;

type RawIntegrationType = {
  instance: {
    interface_config: {
      access: 'ro' | 'rw';
      defaultview: RawViewType;

      accent_color: string;
      header_bgcolor: string;
      header_loader_color: string;
      header_bgimage: string;
      links: LinkType[];

      title_normal: string;
      title_small: string;
      short_name: string;

      casetype_meeting: number[];
      meeting_publication_filter_attribute: RawAttributeType;
      meeting_publication_filter_attribute_value: string;

      casetype_voorstel_item: number[];
      proposal_publication_filter_attribute: RawAttributeType;
      proposal_publication_filter_attribute_value: string;
      magic_strings_accept: RawAttributeType[];
      magic_strings_comment: RawAttributeType[];

      attribute_mapping: RawAttributeMapType[];
    };
  };
  reference: string;
};

export type IntegrationsResponseBodyType = {
  result: {
    instance: {
      rows: RawIntegrationType[];
    };
  };
};

export type IntegrationType = {
  uuid: string;
  canEdit: boolean;
  defaultGrouping: GroupingType;

  colorAccent: string;
  colorHeader: string;
  colorLoader: string;
  image: string;
  links: LinkType[];

  title: string;
  titleSmall: string;
  shortName: string;

  meetingCaseTypeId: number;
  meetingFilterAttribute: string;
  meetingFilterKeyword: string;

  proposalCaseTypeIds: number[];
  proposalFilterAttribute: string;
  proposalFilterKeyword: string;
  proposalVoteAttributes: string[];
  proposalCommentAttributes: string[];

  meetingMapping: AttributeMapType[];
  proposalMapping: AttributeMapType[];
  documentMapping: AttributeMapType[];
};

export type MapMappingType = (map: RawAttributeMapType) => AttributeMapType;

export type FormatIntegrationType = (
  response: RawIntegrationType
) => IntegrationType;
export type GetIntegrationsType = () => Promise<IntegrationType[]>;

type RawCaseRelation = {
  reference: string;
};

type RawCaseType = {
  reference: string;
  instance: {
    number: number;
    attributes: {
      [key: string]: string[];
    };
    case_relationships: {
      plain: {
        instance: {
          rows: RawCaseRelation[];
        };
      };
    };
    casetype: {
      preview: string;
      reference: string;
      instance: {
        version: number;
      };
    };
    confidentiality: { mapped: string };
    requestor: {
      preview: string;
    };
    result: string | null;

    status: string;
  };
};

export type CaseResponseBodyType = {
  result: {
    instance: {
      rows: RawCaseType[];
    };
  };
};

export type MeetingType = {
  uuid: string;
  id: number;
  subject?: string;
  date?: string;
  time?: string;
  location?: string;
  chairperson?: string;
  relationUuids: string[];
};

export type FormatMeetingType = (
  integration: IntegrationType,
  meeting: RawCaseType
) => MeetingType;
export type GetMeetingsType = (
  view: ViewType,
  integration: IntegrationType
) => Promise<MeetingType[]>;

export type FilterType = {
  label: string;
  value: string;
};

export type SetFiltersType = (filters: FilterType[]) => void;

type ChoiceType = {
  label: string;
  value: string;
};

export type FieldType = {
  type: string;
  label: string;
  magicString: string;
  permissions: {
    group: number;
    role: number;
  }[];
  choices: ChoiceType[];
};

type PermissionTypeRaw = {
  group: {
    instance: { id: number };
  };
  role: {
    instance: { id: number };
  };
};

type RawCaseTypeType = {
  phases: {
    fields: {
      type: string;
      label: string;
      is_group: boolean;
      magic_string: string;
      permissions: PermissionTypeRaw[];
      values: {
        active: number;
        sort_order: number;
        value: string;
      }[];
    }[];
  }[];
};

type CaseTypeType = {
  fields: FieldType[];
};

export type CaseTypeResponseBodyType = {
  result: {
    instance: RawCaseTypeType;
  };
};

export type FormatCaseTypeType = (rawCaseType: RawCaseTypeType) => CaseTypeType;

export type GetCaseTypeType = (rawCase: RawCaseType) => Promise<CaseTypeType>;

export type AttributeType = AttributeMapType & {
  value?: string | null;
  type?: string;
};

export type PermissionType = { group: number; role: number };

export type CommentAttributeType = AttributeType & {
  permissions: PermissionType[];
};
export type VotingAttributeType = CommentAttributeType & {
  choices: ChoiceType[];
};

export type VotingAttributesType = {
  vote: VotingAttributeType;
  comment: CommentAttributeType;
};

export type ProposalType = {
  uuid: string;
  id: number;
  attributes: AttributeType[];
  votingAttributesList: VotingAttributesType[];
  documentUuids: string[];
  counter: number;
  filterText: string;

  status: string;
};

export type FormatAttributesType = (
  rawProposal: RawCaseType,
  proposalMapping: AttributeMapType[],
  caseType?: CaseTypeType
) => AttributeType[];

export type FormatVotingAttributesType = (
  rawProposal: RawCaseType,
  voteAttributes: string[],
  commentAttributes: string[],
  caseType: CaseTypeType
) => VotingAttributesType[];

export type FormatProposalType = (
  integration: IntegrationType,
  proposal: RawCaseType,
  caseType?: CaseTypeType
) => ProposalType;

export type GetTimeType = (time?: string) => number;
export type GetDateTimeType = (meeting: MeetingType) => number;

export type SortMeetingsType = (
  integration: IntegrationType,
  view: ViewType,
  meetings: MeetingType[]
) => MeetingType[];

export type GetContentType = (
  integration: IntegrationType,
  view: ViewType,
  grouping: GroupingType
) => Promise<any[]>;

export type GetProposalsType = (
  integration: IntegrationType,
  meeting: MeetingType
) => Promise<ProposalType[]>;

export type FilterProposalsType = (
  proposals: ProposalType[],
  filters: FilterType[]
) => ProposalType[];

export type GetCaseObjType = (
  integration: IntegrationType,
  caseNumber: string
) => Promise<CaseObjType>;

type RawDocumentType = {
  instance: {
    id: string;
    name: string;
    number: string;
  };
};

export type DocumentResponseBodyType = {
  result: { instance: { rows: RawDocumentType[] } };
};

export type DocumentType = {
  uuid: string;
  label: string;
  number: string;
};

export type FormatDocumentType = (document: RawDocumentType) => DocumentType;

export type GetDocumentsType = (
  proposal: ProposalType
) => Promise<DocumentType[]>;

type RawNoteType = {
  reference: string;
  instance: {
    content: string;
  };
};

export type NoteResponseBodyType = {
  result: { instance: { rows: RawNoteType[] } };
};

export type NoteType = {
  uuid: string;
  content: string;
};

export type FormatNoteType = (note: RawNoteType) => NoteType;

export type GetNotesType = (proposal: ProposalType) => Promise<NoteType[]>;

export type CaseObjType = ProposalType & {
  documents: DocumentType[];
  notes: NoteType[];
};

export type CreateNoteType = (
  caseUuid: string,
  content: string
) => Promise<undefined>;

export type UpdateNoteType = (
  caseUuid: string,
  note: NoteType
) => Promise<undefined>;

export type DeleteNoteType = (
  caseUuid: string,
  noteUuid: string
) => Promise<undefined>;

export type PerformSaveType = (
  caseObj: CaseObjType,
  votingAttributes: VotingAttributesType,
  decision: string,
  comment: string
) => Promise<void>;

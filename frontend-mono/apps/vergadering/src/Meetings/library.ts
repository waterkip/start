// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useParams } from 'react-router-dom';
import { UseUrlType, ParamsType } from './types';
import { prefix } from './constants';

export const dateFormatText = 'DD MMMM YYYY';

export const useUrl: UseUrlType = () => {
  const { shortName, view, caseNumber } = useParams() as ParamsType;

  return { shortName, view, caseNumber };
};

export const logout = (shortName?: string) => {
  const logoutUrl = '/auth/logout';
  const loginUrl = shortName ? `${prefix}/${shortName}` : prefix;

  fetch(logoutUrl).then(() => {
    window.location.href = loginUrl;
  });
};

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useQuery } from '@tanstack/react-query';
import classNames from 'classnames';
import ServerErrorDialog from '@zaaksysteem/common/src/components/ServerErrorDialog/ServerErrorDialog';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import { useUrl } from '../library';
import { IntegrationType, CaseObjType } from '../types';
import { QUERY_CASE } from '../constants';
import { getCaseObj } from '../requests';
import { useCaseViewStyles } from './CaseView.styles';
import Header from './Header/Header';
import Content from './Content/Content';

type CaseViewPropsType = {
  integration: IntegrationType;
};

/* eslint complexity: [2, 7] */
const CaseView: React.ComponentType<CaseViewPropsType> = ({ integration }) => {
  const classes = useCaseViewStyles();
  const { caseNumber } = useUrl();

  const {
    data: caseObj,
    error,
    isFetching,
  } = useQuery<CaseObjType | null, V2ServerErrorsType>(
    [QUERY_CASE, caseNumber],
    () => (caseNumber ? getCaseObj(integration, caseNumber) : null)
  );

  if (caseObj) {
    document.title = `${integration.title} - ${caseObj.id}`;
  }

  const subject = caseObj?.attributes.find(attr => attr.isSubject)?.value;
  const title = subject ? `${caseNumber}: ${subject}` : caseNumber;

  return (
    <div
      className={classNames(
        classes.wrapper,
        caseNumber ? classes.open : classes.closed
      )}
    >
      <Header integration={integration} title={title} />
      {caseObj && !isFetching ? (
        <Content integration={integration} caseObj={caseObj} title={title} />
      ) : (
        <Loader />
      )}
      {error && <ServerErrorDialog />}
    </div>
  );
};

export default CaseView;

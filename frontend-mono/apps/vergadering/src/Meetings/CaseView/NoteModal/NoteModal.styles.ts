// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';
import { zIndex } from '../../constants';

export const useNodeModalStyles = makeStyles(
  ({ palette: { sahara }, transitions }: Theme) => ({
    wrapper: {
      position: 'absolute',
      height: '100%',
      right: 0,
      zIndex: zIndex.note,
      backgroundColor: sahara.lightest,
      width: '100%',
      overflow: 'hidden',
      display: 'flex',
      flexDirection: 'column',
      '&>*.MuiFormControl-root': {
        height: '100%',
        overflowY: 'auto',
      },
    },
    open: {
      width: '100%',
      transition: transitions.create(
        // @ts-ignore
        ['width'],
        {
          easing: transitions.easing.sharp,
          duration: transitions.duration.shortest,
        }
      ),
    },
    closed: {
      width: 0,
      transition: transitions.create(
        // @ts-ignore
        ['width'],
        {
          easing: transitions.easing.sharp,
          duration: transitions.duration.shortest,
        }
      ),
    },
  })
);

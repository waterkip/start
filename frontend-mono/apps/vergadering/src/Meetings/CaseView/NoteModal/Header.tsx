// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { zIndex } from '../../constants';
import { useHeaderStyles } from './../Header/Header.styles';

type HeaderPropsType = {
  title: string;
  close: () => void;
  canEdit: boolean;
  creating: boolean;
  createAction: () => void;
  editAction: () => void;
  deleteAction: () => void;
};

const Header: React.ComponentType<HeaderPropsType> = ({
  title,
  close,
  canEdit,
  creating,
  createAction,
  editAction,
  deleteAction,
}) => {
  const classes = useHeaderStyles();

  const saveAction = creating ? createAction : editAction;

  return (
    <div className={classes.wrapper} style={{ zIndex: zIndex.noteHeader }}>
      <IconButton onClick={close}>
        <Icon size="small">{iconNames.arrow_back}</Icon>
      </IconButton>
      <Typography classes={{ root: classes.title }} variant="h3">
        {title}
      </Typography>
      {!creating && canEdit && (
        <IconButton onClick={deleteAction}>
          <Icon>{iconNames.delete}</Icon>
        </IconButton>
      )}
      {canEdit && (
        <IconButton onClick={saveAction}>
          <Icon>{iconNames.save}</Icon>
        </IconButton>
      )}
    </div>
  );
};

export default Header;

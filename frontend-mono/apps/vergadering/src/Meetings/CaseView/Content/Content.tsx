// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useWidth } from '@mintlab/ui/App/library/useWidth';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { Box, IconButton, Typography } from '@mui/material';
import { CaseObjType, IntegrationType, NoteType } from '../../types';
import NoteModal from '../NoteModal/NoteModal';
import { createButtonTop, useContentStyles } from './Content.styles';
import Attribute from './Attribute';
import Note from './Note';
import SideBar from './SideBar';
import VotingAttributes from './VotingAttributes';

type ContentPropsType = {
  integration: IntegrationType;
  caseObj: CaseObjType;
  title: string;
};

/* eslint complexity: [2, 8] */
const Content: React.ComponentType<ContentPropsType> = ({
  integration,
  caseObj,
  title,
}) => {
  const [t] = useTranslation('meetings');
  const classes = useContentStyles();

  const [noteToEdit, setNoteToEdit] = useState<NoteType | null>(null);

  const width = useWidth();
  const isCompact = ['xs', 'sm'].includes(width);

  const { canEdit } = integration;
  const { status, notes, votingAttributesList, attributes } = caseObj;

  const resolved = status === 'resolved';
  const hasNotes = Boolean(notes.length);

  return (
    <>
      <div className={classes.wrapper}>
        <div className={classes.content}>
          {(resolved || hasNotes) && (
            <div className={classes.notes}>
              {resolved && (
                <div className={classes.resolvedWarning}>
                  <Icon size="small">{iconNames.warning}</Icon>
                  {t('case.resolvedWarning')}
                </div>
              )}
              {notes.map(note => (
                <Note
                  key={note.uuid}
                  note={note}
                  canEdit={canEdit}
                  edit={() => setNoteToEdit(note)}
                />
              ))}
            </div>
          )}

          <div className={classes.attributes}>
            <Typography variant="h3">{t('case.attributes.title')}</Typography>
            {canEdit && (
              <Box
                className={classes.noteCreateWrapper}
                sx={{ top: hasNotes ? createButtonTop : 0 }}
              >
                <IconButton
                  classes={{ root: classes.noteCreate }}
                  onClick={() => setNoteToEdit({ uuid: '', content: '' })}
                >
                  <Icon>{iconNames.sticky_note}</Icon>
                </IconButton>
              </Box>
            )}
            {attributes
              .filter(attr => Boolean(attr.value))
              .map((attr, index) => (
                <Attribute key={`${attr.magicString}-${index}`} attr={attr} />
              ))}
          </div>

          <div className={classes.attributes}>
            <Typography variant="h3">
              {t('case.votingAttributes.title')}
            </Typography>
            {votingAttributesList.map((votingAttributes, index) => (
              <VotingAttributes
                key={`vote-${index}`}
                caseObj={caseObj}
                votingAttributes={votingAttributes}
                index={index}
              />
            ))}
          </div>

          {isCompact && <SideBar caseObj={caseObj} isCompact={isCompact} />}
        </div>
        {!isCompact && <SideBar caseObj={caseObj} />}
      </div>

      {/* modal is one level higher to cover the top menu */}
      <NoteModal
        caseObj={caseObj}
        title={title}
        canEdit={canEdit}
        noteToEdit={noteToEdit as NoteType}
        setNoteToEdit={setNoteToEdit}
      />
    </>
  );
};

export default Content;

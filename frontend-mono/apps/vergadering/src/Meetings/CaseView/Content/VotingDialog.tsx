// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import FormDialog from '@zaaksysteem/common/src/components/dialogs/FormDialog/FormDialog';
import { CaseObjType, VotingAttributesType } from '../../types';
import { performSave } from '../../requests';
import { QUERY_CASE } from '../../constants';
import { queryClient } from '../../../App';

type VotingDialogPropsType = {
  caseObj: CaseObjType;
  votingAttributes: VotingAttributesType;
  index: number;
  close: () => void;
};

const VotingDialog: React.ComponentType<VotingDialogPropsType> = ({
  caseObj,
  votingAttributes,
  index,
  close,
}) => {
  const [t] = useTranslation('meetings');

  const [saving, setSaving] = useState<boolean>(false);

  const { vote, comment } = votingAttributes;

  const formDefinition = [
    {
      name: 'vote',
      type: fieldTypes.RADIO_GROUP,
      value: vote.value,
      label: vote.label,
      choices: vote.choices,
    },
    {
      name: 'comment',
      type: fieldTypes.TEXTAREA,
      value: comment.value,
      label: comment.label,
    },
  ];

  const save = async (values: any) => {
    setSaving(true);

    await performSave(caseObj, votingAttributes, values.vote, values.comment);

    const votingAttributesList = caseObj.votingAttributesList.map(
      (attributes, ind) => {
        if (index !== ind) return attributes;

        return {
          vote: { ...attributes.vote, value: values.vote },
          comment: { ...attributes.comment, value: values.comment },
        };
      }
    );
    const newCaseObj = { ...caseObj, votingAttributesList };

    queryClient.setQueriesData([QUERY_CASE], newCaseObj);
    setSaving(false);
    close();
  };

  return (
    <FormDialog
      icon="edit"
      title={t('case.vote.title')}
      formDefinitionT={t as any}
      onClose={close}
      formDefinition={formDefinition as any}
      onSubmit={save}
      saving={saving}
      open={true}
      saveLabel={t('common:verbs.save')}
      fullScreen={true}
    />
  );
};

export default VotingDialog;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTheme } from '@mui/material';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import Button from '@mintlab/ui/App/Material/Button';
import { NoteType } from '../../types';
import { useContentStyles } from './Content.styles';

type NotePropsType = {
  note: NoteType;
  canEdit: boolean;
  edit: () => void;
};

const Note: React.ComponentType<NotePropsType> = ({ note, canEdit, edit }) => {
  const classes = useContentStyles();
  const {
    palette: { common, sahara },
    mintlab: { shadows },
  } = useTheme<Theme>();

  const sx = {
    display: 'flex',
    flexDirection: 'row',
    gap: 1.5,
    padding: 2,
    fontSize: '1rem',
    alignItems: 'center',
    backgroundColor: sahara.lightest,
    color: common.black,
    boxShadow: shadows.sharp,
    borderRadius: 1,
    cursor: 'pointer',
    '&:hover': {
      backgroundColor: sahara.lighter,
    },
  };

  return (
    <Button sx={sx} name="edit" onClick={edit}>
      <Icon size="medium">{iconNames.sticky_note}</Icon>
      <div className={classes.noteLabel}>{note.content}</div>
      <Icon size="small">{canEdit ? iconNames.edit : iconNames.eye}</Icon>
    </Button>
  );
};

export default Note;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useTheme } from '@mui/material/styles';
import { useNavigate } from 'react-router-dom';
import Typography from '@mui/material/Typography';
import ButtonBar from '@mintlab/ui/App/Zaaksysteem/ButtonBar';
import { useWidth } from '@mintlab/ui/App/library/useWidth';
import { iconNames } from '@mintlab/ui/App/Material/Icon';
import Button from '@mintlab/ui/App/Material/Button';
import { CreatableSelect } from '@mintlab/ui/App/Zaaksysteem/Select';
import { logout } from '../../library';
import {
  FilterType,
  IntegrationType,
  SetFiltersType,
  ViewType,
  GroupingType,
  SetGroupingType,
} from '../../types';
import { useUrl } from '../../library';
import { groupings, views } from '../../constants';
import { useHeaderStyles } from './Header.styles';

type HeaderPropsType = {
  integration: IntegrationType;
  filters: FilterType[];
  setFilters: SetFiltersType;
  grouping: GroupingType;
  setGrouping: SetGroupingType;
};

const Header: React.ComponentType<HeaderPropsType> = ({
  integration,
  filters,
  setFilters,
  grouping,
  setGrouping,
}) => {
  const [t] = useTranslation('meeting');
  const classes = useHeaderStyles();
  const width = useWidth();
  const navigate = useNavigate();
  const { view } = useUrl();
  const {
    // @ts-ignore
    palette: { common, elephant, primary },
  } = useTheme();

  const [filtering, setFiltering] = useState<boolean>(false);
  const { title, image, colorHeader } = integration;

  const getNavigationButtonStyles = (navView: ViewType) => ({
    backgroundColor: 'unset',
    boxShadow: 'unset',
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    width: 270,
    minWidth: 150,
    height: 50,
    marginLeft: width === 'xs' ? 0 : 4,
    flexGrow: width === 'xs' ? 1 : 0,
    borderBottom:
      navView === view
        ? `3px solid ${common.white}`
        : `3px solid ${colorHeader}`,
    color: navView === view ? common.white : elephant.light,
    fontSize: 15,
    '&:hover': {
      backgroundColor: 'unset',
      boxShadow: 'unset',
    },
  });

  const actions = [
    ...(filtering
      ? [
          {
            label: t('buttonBar.close'),
            name: 'filter',
            icon: iconNames.close,
            action: () => {
              setFilters([]);
              setFiltering(false);
            },
          },
        ]
      : [
          {
            label: t('buttonBar.filter'),
            name: 'filter',
            icon: iconNames.search,
            action: () => {
              setFiltering(true);
            },
            sx: { color: common.white },
          },
        ]),
    ...(grouping === groupings[0]
      ? [
          {
            label: t('buttonBar.ungroup'),
            name: 'filter',
            icon: iconNames.view_agenda,
            action: () => {
              setGrouping(groupings[1]);
            },
          },
        ]
      : [
          {
            label: t('buttonBar.group'),
            name: 'filter',
            icon: iconNames.format_align_justify,
            action: () => {
              setGrouping(groupings[0]);
            },
          },
        ]),
  ];

  const advancedActions = integration.links.map(({ label, link }) => ({
    label,
    name: label,
    action: () => window.open(link, '_blank'),
  }));

  const permanentActions = [
    {
      icon: iconNames.power_settings_new,
      action: () => logout(integration.shortName),
      label: t('buttonBar.logout'),
      name: 'buttonBarInfo',
    },
  ];

  return (
    <div
      className={classes.wrapper}
      style={{
        backgroundColor: colorHeader || primary.main,
      }}
    >
      <div
        className={classes.image}
        style={{
          backgroundImage: `url(${image})`,
          backgroundColor: colorHeader,
        }}
      />
      <div className={classes.headerTop}>
        {filtering ? (
          <CreatableSelect
            // eslint-disable-next-line jsx-a11y/no-autofocus
            autoFocus={true}
            isMulti={true}
            value={filters}
            onChange={event => {
              const value = event?.target.value;

              if (value.length <= 3) {
                setFilters(value);
              }
            }}
            placeholder={t('filter.placeholder')}
          />
        ) : (
          <Typography
            variant="h3"
            sx={{
              color: common.white,
              overflow: 'hidden',
              textOverflow: 'ellipsis',
              whiteSpace: 'nowrap',
              margin: 1,
            }}
          >
            {title}
          </Typography>
        )}
        <div>
          <ButtonBar
            actions={actions}
            advancedActions={advancedActions}
            permanentActions={permanentActions}
            negative
          />
        </div>
      </div>
      <div className={classes.navigation}>
        {views.map(view => {
          const action = () => navigate(`../${view}`);
          const sx = getNavigationButtonStyles(view);
          const name = `nav-${view}`;
          const text = t(`navigation.${view}`).toUpperCase();

          return (
            <Button key={view} name={name} action={action} sx={sx}>
              {text}
            </Button>
          );
        })}
      </div>
    </div>
  );
};

export default Header;

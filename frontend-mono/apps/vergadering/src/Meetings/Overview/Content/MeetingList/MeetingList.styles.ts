// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useMeetingListStyles = makeStyles(
  ({ palette: { elephant }, typography }: Theme) => ({
    wrapper: {
      display: 'flex',
      flexDirection: 'column',
    },
    placeholder: {
      display: 'flex',
      justifyContent: 'center',
      textAlign: 'center',
      width: '100%',
      padding: 40,
    },
    summary: {
      display: 'flex',
      flexDirection: 'row',
      gap: 10,
      backgroundColor: elephant.lightest,
      borderBottom: `1px solid ${elephant.light}`,
      padding: 20,
      cursor: 'pointer',
      '&:hover': {
        backgroundColor: elephant.light,
      },
    },
    title: {
      flexGrow: 1,
      fontWeight: typography.fontWeightMedium,
      fontSize: 16,
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      whiteSpace: 'nowrap',
      minWidth: 150,
    },
    props: {
      display: 'flex',
      flexDirection: 'row',
      gap: 20,
      color: elephant.darker,
    },
    prop: {
      display: 'flex',
      flexDirection: 'row',
      gap: 5,
      textWrap: 'nowrap',
    },
  })
);

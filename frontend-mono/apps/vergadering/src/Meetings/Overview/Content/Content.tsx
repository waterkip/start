// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import { Swiper } from '@mintlab/ui/App/Zaaksysteem/Swiper/Swiper';
import { useQuery } from '@tanstack/react-query';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import ServerErrorDialog from '@zaaksysteem/common/src/components/ServerErrorDialog/ServerErrorDialog';
import { SHEET_WIDTH } from '../../styles';
import {
  QUERY_MEETINGS_OPEN,
  QUERY_MEETINGS_RESOLVED,
  views,
} from '../../constants';
import { useUrl } from '../../library';
import { FilterType, GroupingType, IntegrationType } from '../../types';
import { getContent } from '../../requests';
import { queryClient } from '../../../App';
import MeetingList from './MeetingList/MeetingList';
import ProposalWrapper from './ProposalList/ProposalWrapper';

type ContentPropsType = {
  integration: IntegrationType;
  grouping: GroupingType;
  filters: FilterType[];
};

/* eslint complexity: [2, 7] */
const Content: React.ComponentType<ContentPropsType> = ({
  integration,
  grouping,
  filters,
}) => {
  const { view } = useUrl();

  const onLoadLeft = () => {
    queryClient.invalidateQueries([QUERY_MEETINGS_OPEN]);
  };
  const onLoadRight = () => {
    queryClient.invalidateQueries([QUERY_MEETINGS_RESOLVED]);
  };

  const { data: contentOpen, error: errorLeft } = useQuery<
    any[],
    V2ServerErrorsType
  >([QUERY_MEETINGS_OPEN, grouping], () =>
    getContent(integration, views[0], grouping)
  );

  const { data: contentResolved, error: errorRight } = useQuery<
    any[],
    V2ServerErrorsType
  >([QUERY_MEETINGS_RESOLVED, grouping], () =>
    getContent(integration, views[1], grouping)
  );

  return (
    <>
      {(errorLeft || errorRight) && <ServerErrorDialog />}

      {!contentOpen || !contentResolved ? (
        <div>
          <Loader />
        </div>
      ) : (
        <Swiper
          view={view}
          views={views}
          minWidth={SHEET_WIDTH}
          onLoadLeft={onLoadLeft}
          onLoadRight={onLoadRight}
          ContentLeft={
            grouping === 'grouped' ? (
              <MeetingList
                key={view}
                integration={integration}
                filters={filters}
                meetings={contentOpen}
              />
            ) : (
              <ProposalWrapper
                integration={integration}
                filters={filters}
                proposals={contentOpen}
              />
            )
          }
          ContentRight={
            grouping === 'grouped' ? (
              <MeetingList
                key={view}
                integration={integration}
                filters={filters}
                meetings={contentResolved}
              />
            ) : (
              <ProposalWrapper
                integration={integration}
                filters={filters}
                proposals={contentResolved}
              />
            )
          }
        />
      )}
    </>
  );
};

export default Content;

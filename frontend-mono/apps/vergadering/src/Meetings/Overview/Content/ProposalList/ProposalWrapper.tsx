// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { Box } from '@mui/material';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import { FilterType, IntegrationType, ProposalType } from '../../../types';
import { useProposalListStyles } from './ProposalList.styles';
import ProposalList from './ProposalList';

type ProposalWrapperPropsType = {
  integration: IntegrationType;
  filters: FilterType[];
  proposals: ProposalType[];
};

const ProposalWrapper: React.ComponentType<ProposalWrapperPropsType> = ({
  integration,
  filters,
  proposals,
}) => {
  const [t] = useTranslation('meetings');
  const classes = useProposalListStyles();

  const { colorAccent } = integration;

  return (
    <div>
      <Box className={classes.header} sx={{ color: colorAccent }}>
        {t('proposals.header')}
      </Box>
      {proposals ? (
        <ProposalList
          integration={integration}
          filters={filters}
          proposals={proposals}
        />
      ) : (
        <Loader />
      )}
    </div>
  );
};

export default ProposalWrapper;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export interface IZaaksysteemProps {
  description: string;
  zaaksysteemUrl: string;
  zaaksysteemId: string;
  teamId: string | undefined;
  channelId: string | undefined;
  isDarkTheme: boolean;
  environmentMessage: string;
  hasTeamsContext: boolean;
  userDisplayName: string;
  relativeUrl: string | undefined;
  domainUrl: string | undefined;
}

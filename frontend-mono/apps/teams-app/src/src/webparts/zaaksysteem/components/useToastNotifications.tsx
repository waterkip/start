// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as React from 'react';
import {
  useId,
  useToastController,
  Toast,
  ToastTitle,
  ToastBody,
} from '@fluentui/react-components';
import { ProgressIndicator } from '@fluentui/react/lib/ProgressIndicator';

type NotificationFunctions = {
  notify: (filename: string) => void;
  notifyDownload: (filename: string) => void;
  notifyDownloadFailed: (filename: string, foutmelding: string) => void;
  toasterId: string;
};

export const useToastNotifications = (): NotificationFunctions => {
  const toasterId = useId('toaster');
  const { dispatchToast } = useToastController(toasterId);

  const notify = (filename: string): void => {
    dispatchToast(
      <Toast>
        <ToastTitle>{filename}</ToastTitle>
        <ToastBody>Kopie geslaagd</ToastBody>
      </Toast>,
      { intent: 'success' }
    );
  };

  const notifyDownload = (filename: string): void => {
    dispatchToast(
      <Toast>
        <ToastTitle>{filename}</ToastTitle>
        <ToastBody>
          <ProgressIndicator
            label="Voortgang"
            description="Aan het kopieren..."
          />
        </ToastBody>
      </Toast>,
      { intent: 'info' }
    );
  };

  const notifyDownloadFailed = (
    filename: string,
    foutmelding: string
  ): void => {
    dispatchToast(
      <Toast>
        <ToastTitle>{filename}</ToastTitle>
        <ToastBody>{foutmelding}</ToastBody>
      </Toast>,
      { intent: 'error' }
    );
  };

  return { notify, notifyDownload, notifyDownloadFailed, toasterId };
};

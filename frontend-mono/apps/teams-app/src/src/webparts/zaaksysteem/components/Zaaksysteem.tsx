// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as React from 'react';
import { escape } from '@microsoft/sp-lodash-subset';
// import { initializeIcons } from '@fluentui/react/lib/Icons';
import { initializeIcons } from '@fluentui/font-icons-mdl2';
import { DefaultButton } from '@fluentui/react/lib/Button';
import { TransferFilesToZsDialog } from './TransferFilesToZsDialog';
import styles from './Zaaksysteem.module.scss';
import ZaaksysteemTable from './ZaaksysteemTable';
import { getTeamFiles } from './getTeamFiles';

initializeIcons();

// Defining the properties for the ZaakSysteem web part
export interface ZaaksysteemWebPartProps {
  description: string;
  zaaksysteemUrl: string;
  zaaksysteemId: string;
  relativeUrl?: string;
  domainUrl?: string;
  hasTeamsContext?: boolean;
  channelId?: string;
  teamId?: string;
}

export default ({
  relativeUrl,
  zaaksysteemUrl,
  zaaksysteemId,
  domainUrl,
  teamId,
  channelId,
  hasTeamsContext,
}: ZaaksysteemWebPartProps) => {
  const [filesTransfer, setFilesTransfer] = React.useState({
    opened: false,
    files: [] as any[],
  });

  const zsUrl = escape(
    zaaksysteemUrl[zaaksysteemUrl.length - 1] === '/'
      ? zaaksysteemUrl
      : zaaksysteemUrl + '/'
  );

  const externalUrl = zsUrl + 'intern/zaak/' + escape(zaaksysteemId);

  // Returning the JSX for the component
  return (
    <section
      className={`${styles.zaakSysteem} ${hasTeamsContext ? styles.teams : ''}`}
    >
      <div className={styles.header}>
        <h1 className={styles.zaaknummer}>
          Zaaknummer: {escape(zaaksysteemId)}
        </h1>
        <DefaultButton
          className={styles.zaakbutton}
          href={externalUrl}
          target="_blank"
          title="Naar mijn zaak"
        >
          Naar mijn zaak
        </DefaultButton>
        <DefaultButton
          className={styles.zaakbutton}
          onClick={async () => {
            const files = await getTeamFiles(relativeUrl || '');
            setFilesTransfer({ opened: true, files });
          }}
          title="Uploaden naar zaak"
        >
          Uploaden naar zaak
        </DefaultButton>
      </div>
      <ZaaksysteemTable
        zaaksysteemUrl={zsUrl}
        zaaksysteemId={zaaksysteemId}
        relativeUrl={relativeUrl}
        teamId={teamId}
        channelId={channelId}
        domainUrl={domainUrl}
      />
      <TransferFilesToZsDialog
        opened={filesTransfer.opened}
        close={() => setFilesTransfer({ opened: false, files: [] })}
        files={filesTransfer.files}
        relativeUrl={relativeUrl || ''}
        zaaksysteemId={zaaksysteemId}
        zaaksysteemUrl={zsUrl}
      />
    </section>
  );
};

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useAttributeFormStyle = makeStyles(
  ({ typography, palette: { primary } }: Theme) => ({
    mainContainer: { padding: '0 5px' },
    headerContainer: {
      display: 'flex',
      justifyContent: 'space-between',
      paddingBottom: 15,
    },
    actionsContainer: {
      display: 'flex',
      '&>.MuiIconButton-root': {
        padding: '6px',
      },
    },
    attributeName: {
      ...typography.h5,
      margin: 0,
      display: 'flex',
      alignItems: 'center',
    },
  })
);

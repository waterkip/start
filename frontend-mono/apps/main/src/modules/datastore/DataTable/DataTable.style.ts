// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useDataTableStyles = makeStyles(() => ({
  wrapper: {
    flexGrow: 1,
    overflow: 'hidden',
    display: 'flex',
    flexDirection: 'column',
  },
  loader: {
    position: 'absolute',
    bottom: 1,
    width: '100%',
  },
  tableWrapper: {
    flexGrow: 1,
    width: '100%',
    overflow: 'auto',
  },
  pagination: {
    width: `100%`,
    position: 'relative',
  },
}));

export const useTableStyles = makeStyles(
  ({ mintlab: { greyscale }, palette: { primary } }: Theme) => ({
    tableRow: {
      borderBottom: `1px solid ${greyscale.dark}`,
      alignItems: 'center',
      '&:hover': {
        backgroundColor: primary.lightest,
      },
    },
  })
);

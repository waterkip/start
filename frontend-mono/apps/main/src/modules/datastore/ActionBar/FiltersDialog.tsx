// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useRef } from 'react';
import { useTranslation } from 'react-i18next';
import DialogContent from '@mui/material/DialogContent';
import Divider from '@mui/material/Divider';
import DialogActions from '@mui/material/DialogActions';
import { Dialog, DialogTitle } from '@mintlab/ui/App/Material/Dialog';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import createDialogActions from '@zaaksysteem/common/src/components/dialogs/library/createDialogActions';
import { cloneWithout } from '@mintlab/kitchen-sink/source';
import { defaultFilters } from '../DataStore.library';
import { useFiltersDialogStyles } from './FiltersDialog.style';
import {
  DataTypeType,
  FiltersType,
  SetFiltersType,
} from './../DataStore.types';
import {
  getFiltersDialogFormDefinition,
  getFiltersDialogRules,
} from './FiltersDialog.formdefinition';

type FiltersDialogPropsType = {
  dataType: DataTypeType;
  filters: FiltersType;
  setFilters: SetFiltersType;
  onClose: () => void;
  open: boolean;
};

const FiltersDialog: React.ComponentType<FiltersDialogPropsType> = ({
  dataType,
  filters,
  setFilters,
  onClose,
  open,
}) => {
  const [t] = useTranslation('dataStore');
  const classes = useFiltersDialogStyles();
  const dialogEl = useRef();

  const title = t('filters.dialog.title');
  const formDefinition = getFiltersDialogFormDefinition(
    t as any,
    dataType,
    filters
  );
  const rules = getFiltersDialogRules(dataType);

  let {
    fields,
    formik: { values },
  } = useForm({
    formDefinition,
    rules,
  });

  return (
    <Dialog
      disableBackdropClick={true}
      open={open}
      onClose={onClose}
      scope={'dataStore-filters-dialog'}
      ref={dialogEl}
    >
      <DialogTitle
        elevated={true}
        icon="settings"
        title={title}
        onCloseClick={onClose}
      />
      <DialogContent>
        <div className={classes.formWrapper}>
          {fields.map(
            ({ FieldComponent, key, type, suppressLabel, ...rest }) => {
              const props = cloneWithout(rest, 'mode');

              return (
                <FormControlWrapper
                  {...props}
                  label={suppressLabel ? false : props.label}
                  compact={true}
                  key={`${props.name}-formcontrol-wrapper`}
                >
                  <FieldComponent
                    {...props}
                    t={t}
                    containerRef={dialogEl.current}
                  />
                </FormControlWrapper>
              );
            }
          )}
        </div>
      </DialogContent>
      <>
        <Divider />
        <DialogActions>
          {createDialogActions(
            [
              {
                text: t('filters.dialog.set'),
                onClick: () => {
                  setFilters({
                    ...values,
                    freeform_filter: filters.freeform_filter,
                    zapi_page: 0,
                    zapi_num_rows: filters.zapi_num_rows,
                  });
                  onClose();
                },
              },
              {
                text: t('filters.dialog.reset'),
                onClick: () => {
                  setFilters({
                    ...defaultFilters,
                    freeform_filter: filters.freeform_filter,
                  });
                  onClose();
                },
              },
              {
                text: t('common:dialog.cancel'),
                onClick: onClose,
              },
            ],
            'datastore-filters-dialog'
          )}
        </DialogActions>
      </>
    </Dialog>
  );
};

export default FiltersDialog;

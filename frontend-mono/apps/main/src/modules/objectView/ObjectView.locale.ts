// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export default {
  nl: {
    attributes: {
      title: 'Kenmerken',
      actions: {
        cancel: 'Annuleren',
        edit: 'Bewerken',
        save: 'Opslaan',
        activate: 'Inschakelen',
        delete: 'Verwijderen',
        create: 'Zaak aanmaken',
      },
      customAttributes: {
        title: 'Specifieke kenmerken',
        description: 'De eigen kenmerken van dit object.',
      },
      systemAttributes: {
        title: 'Algemene kenmerken',
        description: 'De systeemkenmerken van dit object.',
        uuid: 'UUID',
        status: 'Status',
        registrationDate: 'Registratiedatum',
        lastModified: 'Gewijzigd op',
      },
    },
    create: {
      fields: {
        caseType: 'Zaaktype',
      },
    },
    delete: {
      description: 'Let op: Deze actie kan niet ongedaan gemaakt worden.',
      fields: {
        reason: 'Reden',
      },
    },
    sideMenu: {
      attributes: 'Kenmerken',
      timeline: 'Tijdlijn',
      map: 'Kaart',
      relationships: 'Relaties',
    },
    status: {
      active: 'Actief',
      inactive: 'Inactief',
      draft: 'Concept',
    },
    relations: {
      title: 'Relaties',
    },
    timeline: {
      title: 'Tijdlijn',
    },
    serverErrors: {
      'case_management/custom_object/object/not_found':
        'Het object kon niet gevonden worden. U heeft onvoldoende rechten of het object is verwijderd. Raadpleeg uw beheerder voor meer informatie.',
      'case_management/custom_object/object_type/not_found':
        'Het objecttype kon niet gevonden worden. U heeft onvoldoende rechten of het objecttype is verwijderd. Raadpleeg uw beheerder voor meer informatie.',
      'case_management/custom_object/object_create/unauthorised':
        'Object kan niet worden gemaakt vanwege onvoldoende rechten.',
    },
  },
};

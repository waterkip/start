// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { v4 } from 'uuid';
import {
  fetchObject,
  updateObject,
  fetchObjectType,
  deleteObject,
} from './ObjectView.requests';
import {
  FetchDataType,
  GetObjectType,
  GetObjectTypeType,
} from './ObjectView.types';

const getObject: GetObjectType = async uuid => {
  const object = await fetchObject(uuid);

  const {
    id,
    attributes: {
      custom_fields,
      date_created,
      last_modified,
      status,
      title,
      version_independent_uuid,
    },
    relationships,
    meta,
  } = object;

  const relatedCasesUuids = relationships?.cases
    ?.map((caseRelation: any) => caseRelation.data.id || '')
    .filter(Boolean);

  return {
    uuid: id,
    customFieldsValues: custom_fields || {},
    lastModified: last_modified,
    registrationDate: date_created,
    title: title || '',
    objectTypeVersionUuid:
      object.relationships?.custom_object_type?.data.id || '',
    versionIndependentUuid: version_independent_uuid || '',
    status,
    relatedCasesUuids,
    authorizations: meta?.authorizations || [],
  };
};

const getObjectType: GetObjectTypeType = async object => {
  const objectType = await fetchObjectType(object.objectTypeVersionUuid);

  return {
    ...objectType.data.attributes,
    customFieldsDefinition:
      objectType.data.attributes.custom_field_definition?.custom_fields || [],
    authorizations: objectType.data.meta?.authorizations || [],
  };
};

export const fetchData: FetchDataType = async (uuid: string) => {
  const object = await getObject(uuid);
  const objectType = await getObjectType(object);

  return { object, objectType };
};

export const updateObjectAction = async (
  relatedCasesUuidsExcludingAutomatic: string[],
  objectUuid: string,
  values: { [key: string]: any },
  status?: 'active' | 'inactive'
) => {
  const data: any /* APICaseManagement.UpdateCustomObjectRequestBody */ = {
    uuid: v4(),
    existing_uuid: objectUuid,
    custom_fields: values,
    // this is mock data, but it's required by the API
    archive_metadata: {
      status: 'to preserve',
      ground: '',
      retention: 0,
    },
    related_to: {
      cases: relatedCasesUuidsExcludingAutomatic,
    },
    status: status || 'active',
  };

  return await updateObject(data);
};

export const activateObject = async (
  relatedCasesUuids: string[],
  objectUuid: string,
  values: { [key: string]: any }
) => {
  return await updateObjectAction(
    relatedCasesUuids,
    objectUuid,
    values,
    'active'
  );
};

export const deleteObjectAction = async (uuid: string, reason: string) => {
  await deleteObject({ uuid, reason });
};

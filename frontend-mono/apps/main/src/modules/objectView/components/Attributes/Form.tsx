// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
//@ts-ignore
import { cloneWithout } from '@mintlab/kitchen-sink/source';
import { SubHeader } from '@zaaksysteem/common/src/components/SubHeader/SubHeader';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import { AnyFormDefinitionField } from '@zaaksysteem/common/src/components/form/types';
import { useAttributesStyles } from './Attributes.style';

type FormPropsType = {
  type: 'custom' | 'system';
  fields: AnyFormDefinitionField[];
};

/* eslint complexity: [2, 10] */
const Form: React.FunctionComponent<FormPropsType> = ({ type, fields }) => {
  const [t] = useTranslation('objectView');
  const classes = useAttributesStyles();

  const title = t(`attributes.${type}Attributes.title`);
  const subTitle = t(`attributes.${type}Attributes.description`);

  return (
    <>
      <SubHeader title={title} description={subTitle} />
      <div>
        {fields.map(
          ({ FieldComponent, name, error, touched, value, ...rest }) => {
            const restValues = cloneWithout(rest, 'type', 'classes');

            return (
              <FormControlWrapper
                {...restValues}
                error={error}
                touched={touched}
                key={name}
                contentColumnClassName={classes.colContent}
              >
                <FieldComponent
                  name={name}
                  value={value}
                  key={name}
                  {...restValues}
                />
              </FormControlWrapper>
            );
          }
        )}
      </div>
    </>
  );
};

export default Form;

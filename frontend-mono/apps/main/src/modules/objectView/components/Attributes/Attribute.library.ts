// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import generateCustomFieldFormDefinition from '@zaaksysteem/common/src/components/form/library/generateCustomFieldFormDefinition';
import * as i18next from 'i18next';
import { ObjectType, ObjectTypeType } from '../../ObjectView.types';

export const getCustomFormDefinition = (
  t: i18next.TFunction,
  object: ObjectType,
  objectType: ObjectTypeType,
  editing: boolean,
  isAllowedSystemAttributes: boolean
) => {
  const customAttributesFormDefinition = generateCustomFieldFormDefinition({
    customFieldsDefinition: objectType.customFieldsDefinition,
    customFieldsValues: object.customFieldsValues,
    readOnly: !editing,
    t: t as any,
    config: {
      context: {
        type: 'ObjectView' as const,
        data: {
          magic_string: '',
          objectType,
          object,
        },
      },
    },
  });

  customAttributesFormDefinition.forEach(field => {
    field.config = {
      ...field.config,
      context: {
        type: 'ObjectView' as const,
        data: {
          ...field.config.context.data,
          magic_string: field.name,
        },
      },
    };
    if (isAllowedSystemAttributes) {
      field.hidden = false;
    }
  });

  return customAttributesFormDefinition;
};

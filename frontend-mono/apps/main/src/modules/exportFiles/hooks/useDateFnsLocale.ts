// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import { useEffect } from 'react';
import { useTranslation } from 'react-i18next';

type useDateFnsLocalePropsType = {
  onLoad: (mod: Locale) => void;
};

export const useDateFnsLocale = ({ onLoad }: useDateFnsLocalePropsType) => {
  const [, i18n] = useTranslation();
  useEffect(() => {
    (async () => {
      const module = await import(
        /* webpackMode: "lazy", webpackChunkName: "df-[index]", webpackExclude: /_lib/ */
        `date-fns/locale/${i18n.language}/index.js`
      );
      if (onLoad) onLoad(module);
    })();
  }, []);
};

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export default {
  nl: {
    title: 'Nieuwe zoekopdracht',
    fields: {
      basedOn: {
        error: 'Selecteer minimaal 1, en maximaal {{maxRows}} resultaten.',
        label: 'Gebaseerd op',
      },
      abort:
        'Er zijn geen resultaten gevonden voor deze Job. Het is niet mogelijk om op basis van deze Job een nieuwe zoekopdracht aan te maken.',
      intro:
        'Middels dit scherm kan een nieuwe zoekopdracht worden aangemaakt, op basis van één of meerdere resultaten van de Job. De resultaten van de aangevinkte types zullen worden ingesteld als "Zaaknummer" filter in de nieuwe zoekopdracht. Verder kunnen een naam en eventuele labels opgegeven worden, zodat deze zoekopdracht makkelijker terug te vinden is.',
      name: {
        label: 'Naam',
        placeholder: 'Naam zoekopdracht',
      },
      labels: {
        label: 'Labels',
        default: 'Jobs',
      },
      open: 'Open zoekopdracht in nieuwe tab',
    },
    combinedResults: {
      successes: 'Successen',
      warnings: 'Waarschuwingen',
      errors: 'Fouten',
    },
    snacks: {
      success: 'Nieuwe zoekopdracht is opgeslagen.',
    },
  },
};

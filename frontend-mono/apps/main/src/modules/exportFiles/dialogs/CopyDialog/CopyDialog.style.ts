// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles(() => ({
  contentWrapper: {
    height: '100%',
    minHeight: 650,
    minWidth: 500,
    display: 'flex',
  },
  summaryOverview: {
    width: '100%',
    padding: 0,
    margin: 0,
  },
  summaryRow: {
    display: 'flex',
  },
}));

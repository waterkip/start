// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export type DialogType = {
  type: 'viewer' | 'newSavedSearch' | 'copyJob' | 'deleteJob' | 'cancelJob';
  parameters: {
    [key: string]: any;
  };
};

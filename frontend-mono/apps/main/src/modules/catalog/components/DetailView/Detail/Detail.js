// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { withStyles } from '@mui/styles';
import Icon from '@mintlab/ui/App/Material/Icon';
import classNames from 'classnames';
import { detailStyleSheet } from './Detail.style';

const Detail = ({
  classes,
  children,
  title = '',
  icon,
  offset = true,
  truncate = true,
}) => (
  <div className={classes.wrapper}>
    <div className={classes.icon}>
      <Icon>{icon}</Icon>
    </div>

    <div
      className={classNames(classes.contentWrapper, {
        [classes.offset]: offset,
      })}
    >
      {title ? <div className={classes.title}>{title}</div> : null}

      <div className={classes.content}>
        <div
          className={classNames(classes.values, {
            [classes.truncate]: truncate,
          })}
        >
          {children}
        </div>
      </div>
    </div>
  </div>
);

export default withStyles(detailStyleSheet)(Detail);

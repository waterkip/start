// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
//@ts-ignore
import { asArray } from '@mintlab/kitchen-sink/source/array';

const Default = ({ value }: { value: any }) => (
  <span>{asArray(value).join(', ')}</span>
);

export default Default;

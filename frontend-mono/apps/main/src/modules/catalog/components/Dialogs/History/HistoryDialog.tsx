// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { v4 } from 'uuid';
import { Dialog, DialogTitle } from '@mintlab/ui/App/Material/Dialog';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import Box from '@mui/material/Box';
import FormDialog from '@zaaksysteem/common/src/components/dialogs/FormDialog/FormDialog';
import { capitalize } from '@mintlab/kitchen-sink/source';
import { TEXT } from '../../../../../components/Form/Constants/fieldTypes';
import CaseTypeVersion from './CaseTypeVersion';
import {
  useGetHistory,
  useHistoryReactivationMutation,
} from './HistoryDialog.library';

/* eslint complexity: [2, 7] */
export const useHistoryDialog = () => {
  const [openedData, setOpenedData] = React.useState('');
  const [reactivated, setReactivated] = React.useState(['', '']);
  const [t] = useTranslation('catalog');

  const { data, isLoading } = useGetHistory(openedData);
  const { mutateAsync, isLoading: saving } = useHistoryReactivationMutation(
    () => {
      setOpenedData('');
      setReactivated(['', '']);
    }
  );

  const dialog = data && openedData && (
    <>
      <Dialog
        disableBackdropClick={true}
        open={true}
        onClose={() => setOpenedData('')}
        scope="catalog-case-type-versions-dialog"
      >
        <DialogTitle
          elevated={true}
          icon="history"
          id={v4()}
          title={t('history.dialog.title')}
          onCloseClick={() => setOpenedData('')}
        />

        {isLoading ? (
          <Loader />
        ) : (
          <Box
            sx={{
              height: '600px',
              width: '500px',
              overflow: 'scroll',
              overflowX: 'hidden',
              padding: '0',
            }}
          >
            {data?.data
              ?.map((version: any, index: number) => (
                <CaseTypeVersion
                  key={index}
                  reactivateVersion={() =>
                    setReactivated([
                      version.attributes.id,
                      version.attributes.version,
                    ])
                  }
                  version={version.attributes.version}
                  active={!version.attributes.active}
                  details={[
                    {
                      title: t('history.details.display_name'),
                      value: version.attributes.display_name || '-',
                    },
                    {
                      title: t('history.details.modified_components'),
                      value: (version.attributes.modified_components || [])
                        .map((component: string) => {
                          return capitalize(component);
                        })
                        .join(', '),
                    },
                    {
                      title: t('history.details.change_note'),
                      value: version.attributes.change_note,
                    },
                  ]}
                  created={version.attributes.created}
                />
              ))
              .reverse()}
          </Box>
        )}
      </Dialog>
      {reactivated[0] && (
        <FormDialog
          saving={saving}
          icon="extension"
          formDefinition={[
            {
              name: 'reason',
              type: TEXT,
              value: '',
              required: true,
              label: t('history.fields.reason.label'),
              placeholder: t('history.fields.reason.label'),
            },
          ]}
          onClose={() => setReactivated(['', ''])}
          title={t('history.dialog.titleActivate', { version: reactivated[1] })}
          onSubmit={values =>
            mutateAsync({
              reason: values.reason,
              case_type_id: openedData,
              version_id: reactivated[0],
            })
          }
        />
      )}
    </>
  );

  return { openHistoryDialog: setOpenedData, historyDialog: dialog };
};

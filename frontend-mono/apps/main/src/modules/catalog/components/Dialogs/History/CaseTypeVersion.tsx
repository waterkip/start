// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import fecha from 'fecha';
import { Box } from '@mui/material';
import Avatar from '@mui/material/Avatar';
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import Button from '@mintlab/ui/App/Material/Button';
//@ts-ignore
import Card from '@mintlab/ui/App/Material/Card';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { useCaseTypeVersionStyle } from './CaseTypeVersion.style';

type CaseTypeVersionPropsType = {
  active: boolean;
  details: { title: string; value: string }[];
  created: string;
  version: number;
  reactivateVersion: () => void;
};

const CaseTypeVersion = ({
  reactivateVersion,
  active,
  details,
  created,
  version,
}: CaseTypeVersionPropsType) => {
  const style = useCaseTypeVersionStyle();
  const [t] = useTranslation('catalog');
  const dateTimeCreated = new Date(created);
  const date = fecha.format(dateTimeCreated, t('common:dates:dateFormatText'));
  const time = fecha.format(dateTimeCreated, t('common:dates:timeFormat'));

  return (
    <Accordion sx={style.panel}>
      <AccordionSummary expandIcon={<Icon>{iconNames.expand_more}</Icon>}>
        <Box sx={style.summaryWrapper}>
          <Avatar sx={style.avatar}>{version}</Avatar>
          <Box sx={style.dateTime}>
            {created ? (
              <>
                <Box sx={style.date}>{date}</Box>
                <Box sx={style.time}>{`${time} ${t('common:dates.hour')}`}</Box>
              </>
            ) : null}
          </Box>

          {active ? (
            <Button
              action={event => {
                event.stopPropagation();
                reactivateVersion();
              }}
              sx={style.activateButton}
              name="caseTypeActivate"
            >
              {t('history.dialog.restore')}
            </Button>
          ) : null}
        </Box>
      </AccordionSummary>
      <AccordionDetails sx={active ? style.detailsActive : {}}>
        {/* @ts-ignore */}
        <Card sx={style.detailsCard}>
          {details.map((detail, index) => {
            return (
              <Box key={index} sx={style.detailWrapper}>
                <Box sx={style.detailTitle}>{`${detail.title}:`}</Box>
                <Box sx={style.detailValue}>{detail.value || '-'}</Box>
              </Box>
            );
          })}
        </Card>
      </AccordionDetails>
    </Accordion>
  );
};

export default CaseTypeVersion;

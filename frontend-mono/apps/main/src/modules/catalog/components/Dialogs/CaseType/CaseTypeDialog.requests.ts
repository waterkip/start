// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { APIAdmin } from '@zaaksysteem/generated/types/APIAdmin.types';
import { request } from '@zaaksysteem/common/src/library/request/request';

type RequestBody = APIAdmin.CreateVersionedCasetypeRequestBody;
type ResponseBody = APIAdmin.CreateVersionedCasetypeResponseBody;

type CreateCaseTypeType = (data: RequestBody) => Promise<ResponseBody>;

export const createCaseType: CreateCaseTypeType = async data => {
  const url = '/api/v2/admin/catalog/create_versioned_casetype';

  return await request<ResponseBody>('POST', url, data);
};

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import { useMutation, useQueryClient } from '@tanstack/react-query';
import { useNavigate } from 'react-router';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import { openServerError } from '@zaaksysteem/common/src/signals';
import { cloneWithout } from '@mintlab/kitchen-sink/source';
import { invalidateAfterChange } from '../../../Catalog.symbols';
import { fetchCaseType } from '../../../../caseTypeManagement/CaseTypeManagement.requests';
import { leadTimeDefault } from '../../../../caseTypeManagement/conversions/form.conversions';
import { defaultPhases } from '../../../../caseTypeManagement/conversions/phases.conversions';
import { createCaseType } from './CaseTypeDialog.requests';

export const useSaveCaseTypeMutation = (
  t: i18next.TFunction,
  onSuccess: () => void,
  duplicating: boolean
) => {
  const navigate = useNavigate();
  const client = useQueryClient();

  return useMutation<any, V2ServerErrorsType, any>(
    ['create_case_type'],
    async ({
      name,
      status,
      update_description,
      folderId,
      uuid: casetype_uuid,
      caseTypeId,
    }) => {
      const active = status === 'online';

      const common = t('caseTypeDialog.changedComponents.common');
      const update_components = [common];
      const change_log = { update_description, update_components };

      if (duplicating) {
        const caseType = await fetchCaseType(caseTypeId);

        let caseTypeAttributes = cloneWithout(
          caseType.data.attributes,
          'uuid',
          'active',
          'catalog_folder'
        );

        caseTypeAttributes.general_attributes.name = name;

        const catalog_folder_uuid =
          caseType.data.attributes.catalog_folder.uuid;

        return createCaseType({
          ...caseTypeAttributes,
          casetype_uuid,
          active,
          catalog_folder_uuid,
          change_log,
        }).then(() => {
          if (catalog_folder_uuid !== folderId) {
            navigate(catalog_folder_uuid);
          }
        });
      } else {
        return createCaseType({
          casetype_uuid,
          catalog_folder_uuid: folderId || undefined,
          change_log,
          active,
          general_attributes: {
            name,
            ...leadTimeDefault,
          },
          relations: {
            trigger: 'intern',
            allowed_requestor_types: ['medewerker'],
          },
          phases: defaultPhases,
          results: [],
        });
      }
    },
    {
      onError: openServerError,
      onSuccess: () => {
        invalidateAfterChange(client);
        onSuccess();
      },
    }
  );
};

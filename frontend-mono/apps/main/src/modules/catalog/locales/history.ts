// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const history = {
  history: {
    dialog: {
      title: 'Versiebeheer',
      titleActivate: 'Versie {{version}} herstellen',
      activate: 'Activeren',
      restore: 'Herstellen',
    },
    fields: {
      reason: {
        label: 'Reden',
      },
    },
    details: {
      display_name: 'Gebruiker',
      modified_components: 'Aangepast',
      change_note: 'Opmerkingen',
    },
  },
};

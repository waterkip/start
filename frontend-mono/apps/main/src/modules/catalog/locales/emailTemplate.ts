// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const emailTemplate = {
  emailTemplate: {
    defaultCreate: 'Nieuw aangemaakt',
    defaultEdit: 'Wijziging',
    dialog: {
      title: 'Emailsjabloon {{action}}',
    },
    fields: {
      label: {
        label: 'Label',
      },
      sender: {
        label: 'Afzender',
        placeholder: 'Bijv: Afdeling Burgerzaken',
      },
      sender_address: {
        label: 'Antwoordadres',
        placeholder: 'Bijv: burgerzaken@gemeente.nl',
      },
      subject: {
        label: 'Onderwerp',
      },
      message: {
        label: 'Bericht',
      },
      attachments: {
        label: 'Bijlagen',
      },
      commit_message: {
        label: 'Opmerking',
        help: 'Korte omschrijving van de reden van aanmaak / wijziging.',
      },
    },
  },
};

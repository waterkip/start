// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const folder = {
  folder: {
    dialog: {
      title: 'Map {{action}}',
    },
    fields: {
      name: {
        label: 'Mapnaam',
      },
    },
  },
};

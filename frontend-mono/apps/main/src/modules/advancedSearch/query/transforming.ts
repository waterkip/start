// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { APICaseManagement } from '@zaaksysteem/generated/types/APICaseManagement.types';
import { v4 } from 'uuid';
//@ts-ignore
import { isPopulatedArray } from '@mintlab/kitchen-sink/source/array';
import {
  SavedSearchType,
  PermissionType,
  EditFormStateType,
  KindType,
  ColumnType,
} from '../AdvancedSearch.types';
import {
  getTypeOrSubType,
  isCustomAttribute,
  replaceKeysInJSON,
} from '../library/library';
import {
  filtersKeyNamesReplacements,
  FIELD_TYPES_AS_SINGLE_VALUE,
  FIELD_TYPES_YES_NO_BOOLEAN,
  FIELD_TYPES_AS_OPTIONS_OBJECT,
} from '../library/config';
import { hasValue } from '../../../library/validation/value';

type ResultType = APICaseManagement.ListSavedSearchResponseBody['data'];

// Converts from API responses ---> Frontend ready Saved Searches objects
/* eslint complexity: [2, 14] */
export const mapAPISavedSearch =
  (kind: KindType) =>
  (result: ResultType[0]): EditFormStateType => {
    const parseFilters = (
      filters: ResultType[0]['attributes']['filters']
    ): SavedSearchType['filters'] => ({
      ...filters,
      operator: filters?.operator || 'and',
      filters: filters.filters.map((filter: any) => {
        const customAttribute = isCustomAttribute(filter);
        const compareType = getTypeOrSubType(filter);
        const getParsedValue = () => {
          if (kind === 'custom_object' && customAttribute) {
            return filter.values;
          } else if (
            !customAttribute &&
            hasValue(filter.values) &&
            FIELD_TYPES_AS_SINGLE_VALUE.includes(compareType)
          ) {
            return filter.values[0];
          } else if (
            customAttribute &&
            hasValue(filter.values.values) &&
            FIELD_TYPES_AS_SINGLE_VALUE.includes(compareType)
          ) {
            //@ts-ignore
            return filter.values.values[0];
          } else if (FIELD_TYPES_YES_NO_BOOLEAN.includes(filter.type)) {
            return filter.values === true ? 'yes' : 'no';
          } else if (
            filter.type === 'attributes.value' &&
            FIELD_TYPES_AS_OPTIONS_OBJECT.includes(compareType)
          ) {
            return {
              ...filter.values,
              values: (filter.values.values || []).map((val: any) => val.value),
            };
          } else {
            return filter.values;
          }
        };
        return {
          ...filter,
          uuid: v4(),
          values: getParsedValue(),
        };
      }),
    });

    const mapPermissions = (
      permissions: ResultType[0]['attributes']['permissions']
    ) =>
      permissions.map(
        ({ group_id, role_id, permission }): PermissionType => ({
          groupID: group_id,
          roleID: role_id,
          writePermission: permission.includes('write'),
          saved: true,
        })
      );
    const mapColumns = (columns: Partial<ColumnType>[]) =>
      columns.map((column: any) => ({ ...column, uuid: v4() }));

    const {
      attributes: {
        filters,
        name,
        permissions,
        columns,
        sort_column,
        sort_order,
        labels,
        date_created,
        date_updated,
        updated_by,
        template,
      },
      id,
      meta,
      relationships,
    } = result;

    const returnObj: EditFormStateType = {
      uuid: id,
      name,
      template: template || 'standard',
      type: 'user',
      filters: parseFilters(filters),
      permissions: mapPermissions(permissions),
      authorizations: meta?.authorizations || [],
      columns: mapColumns(columns),
      sortColumn: sort_column,
      sortOrder: sort_order,
      labels: isPopulatedArray(labels)
        ? labels.map((label: any) => label.attributes.name)
        : [],
      metaData: {
        createdByUuid: relationships?.owner?.data.id || '',
        createdByName: relationships?.owner?.meta?.summary || '',
        createdByDate: date_created || '',
        lastEditedByUuid: updated_by?.uuid || '',
        lastEditedByName: updated_by?.summary || '',
        lastEditedByDate: date_updated || '',
      },
      selectedFilter: null,
      selectedObjectType: null,
    };

    return replaceKeysInJSON(
      returnObj,
      filtersKeyNamesReplacements.map((element: string[]) =>
        [...element].reverse()
      )
    );
  };

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent } from 'react';
import { FormikProps } from 'formik';
import * as i18next from 'i18next';
import { QueryClient } from '@tanstack/react-query';
import { hasAccess } from '../../library/library';
import {
  AuthorizationsType,
  EditFormStateType,
  IdentifierType,
  KindType,
  ModeType,
} from '../../AdvancedSearch.types';
import Filters from './Filters/Filters';
import Permissions from './Permissions/Permissions';
import Columns from './Columns/Columns';
import EditFormTabs from './Tabs';

type EditFormContentPropsType = {
  classes: any;
  handleTabChange: any;
  authorizations?: AuthorizationsType[];
  t: i18next.TFunction;
  mode: ModeType;
  kind: KindType;
  formik: FormikProps<EditFormStateType>;
  client: QueryClient;
  tabValue: number;
  identifier?: IdentifierType | null;
};

const EditFormContent: FunctionComponent<EditFormContentPropsType> = ({
  classes,
  handleTabChange,
  authorizations,
  t,
  mode,
  kind,
  formik,
  client,
  tabValue,
  identifier,
}) => {
  return (
    <>
      <EditFormTabs
        classes={classes}
        tabValue={tabValue}
        handleTabChange={handleTabChange}
        authorizations={authorizations}
        mode={mode}
        t={t}
      />
      <div className={classes.editFormTabsWrapper}>
        <div className={classes.tabsContent}>
          <Filters
            kind={kind}
            formik={formik}
            classes={classes}
            show={tabValue === 0}
            t={t}
            client={client}
          />
          {(mode === 'new' ||
            (mode === 'edit' &&
              authorizations &&
              hasAccess(authorizations, 'admin'))) && (
            <Permissions
              classes={classes}
              formik={formik}
              show={tabValue === 1}
              mode={mode}
              t={t}
            />
          )}
          <Columns
            classes={classes}
            show={tabValue === 2}
            t={t}
            kind={kind}
            mode={mode}
            identifier={identifier}
          />
        </div>
      </div>
    </>
  );
};

export default EditFormContent;

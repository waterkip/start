// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useRef, FunctionComponent, useState, useEffect } from 'react';
import { Field } from 'formik';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import Button from '@mintlab/ui/App/Material/Button';
import {
  CaseTypeFinder,
  CaseTypeValueType,
} from '@zaaksysteem/common/src/components/form/fields/CaseTypeFinder/CaseTypeFinder';
//@ts-ignore
import { isPopulatedArray } from '@mintlab/kitchen-sink/source/array';
import { ErrorType } from '../../../../AdvancedSearch.types';
import { CaseTypeVersionValueType } from '../../../../AdvancedSearch.types.filters';
import { FilterCommonPropsType } from '../Filters.types';
import { validateForm } from '../../EditForm.library';
import { hasNoValue } from '../library/Filters.library';
import { useStyles } from './CaseTypeVersion.style';
import CaseTypeVersionEntries from './CaseTypeVersionEntries';
import { getVersions, getVersionChoices } from './CaseTypeVersion.library';

export const TRANSLATION_BASE =
  'editForm.fields.filters.fields.casetypeVersion';

const CaseTypeVersion: FunctionComponent<FilterCommonPropsType> = ({
  identifier,
  t,
  name,
  index,
}) => {
  const classes = useStyles();
  const helpersRef = useRef<HTMLInputElement | null>(null);
  const [selectedCaseType, setSelectedCaseType] =
    useState<CaseTypeValueType | null>(null);
  const [versions, setVersions] = useState<CaseTypeVersionValueType[]>([]);
  const [selectedVersion, setSelectedVersion] = useState<string | null>(null);
  const validAdd = selectedCaseType && selectedVersion;

  const handleAdd = () => {
    if (!helpersRef) return;
    const insertVersion = versions.find(
      ver => ver.casetype_version_uuid === selectedVersion
    );
    if (!insertVersion) return;

    //@ts-ignore-next-line
    helpersRef.current.insert(0, insertVersion);
  };

  useEffect(() => {
    if (!selectedCaseType) return;

    (async () => {
      const versions = await getVersions(selectedCaseType.value);
      setSelectedVersion(null);
      setVersions(versions);
    })();
  }, [selectedCaseType]);

  return (
    <div className={classes.wrapper}>
      <div className={classes.addWrapper}>
        <CaseTypeFinder
          name={`casetypeFinder-${index}`}
          placeholder={t(`${TRANSLATION_BASE}.placeholderCasetype`)}
          multiValue={false}
          value={selectedCaseType}
          //@ts-ignore
          onChange={(event: React.ChangeEvent<any>) => {
            setSelectedCaseType(event.target.value as CaseTypeValueType);
          }}
          isClearable={false}
        />

        <Select
          choices={getVersionChoices(versions, t)}
          value={selectedVersion}
          isClearable={false}
          onChange={(event: React.ChangeEvent<any>) =>
            setSelectedVersion(event.target.value.value)
          }
          placeholder={t(`${TRANSLATION_BASE}.placeholderVersion`)}
          disabled={!selectedCaseType}
          loading={true}
        />
        <Button
          name={`addButton-${index}`}
          onClick={handleAdd}
          sx={{ minWidth: 32, height: 32 }}
          disabled={!validAdd}
        >
          +
        </Button>
      </div>
      <Field
        component={CaseTypeVersionEntries}
        name={name}
        validate={(value: any): ErrorType | undefined => {
          const isUnpopulatedArray = !isPopulatedArray(value);
          return hasNoValue(value) || isUnpopulatedArray
            ? {
                value: t(`${TRANSLATION_BASE}.errors.minEntries`),
                uuid: identifier,
              }
            : undefined;
        }}
        classes={classes}
        t={t}
        validateForm={validateForm}
        index={index}
        helpersRef={helpersRef}
        setHelpersRef={(ref: HTMLInputElement | null) => {
          if (ref) helpersRef.current = ref;
        }}
      />
    </div>
  );
};

export default CaseTypeVersion;

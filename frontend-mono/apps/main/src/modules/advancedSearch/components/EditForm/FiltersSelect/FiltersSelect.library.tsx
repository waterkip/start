// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import { v4 } from 'uuid';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
//@ts-ignore
import { isPopulatedArray } from '@mintlab/kitchen-sink/source/array';
import { FormikProps, FieldArrayRenderProps, FieldInputProps } from 'formik';
import { getFieldOperatorsConfig } from '../../../library/config';
import { validateForm } from '../EditForm.library';
import { EditFormStateType, KindType } from '../../../AdvancedSearch.types';
import {
  FilterType,
  CustomAttributeTypes,
} from '../../../AdvancedSearch.types.filters';
import { isCustomAttribute } from '../../../library/library';

// eslint-disable-next-line complexity
export const getDefaultValues = (
  type: FilterType['type'] | CustomAttributeTypes['type']
) => {
  switch (type) {
    case 'attributes.last_modified':
    case 'attributes.registration_date':
    case 'attributes.destruction_date':
    case 'attributes.target_date':
    case 'attributes.stalled_until':
    case 'attributes.stalled_since':
    case 'attributes.completion_date':
      return null;
    case 'attributes.channel_of_contact':
    case 'attributes.department_role':
    case 'attributes.result':
    case 'attributes.case_location':
    case 'attributes.confidentiality':
    case 'relationship.assignee.id':
    case 'relationship.requestor.id':
    case 'relationship.coordinator.id':
    case 'attributes.payment_status':
    case 'keyword':
    case 'attributes.case_phase':
    case 'attributes.has_unaccepted_changes':
    case 'attributes.retention_period_source_date':
    case 'attributes.parent_number':
    case 'attributes.custom_number':
    case 'attributes.case_number':
    case 'attributes.subject':
    case 'relationship.requestor.zipcode':
    case 'relationship.requestor.coc_number':
    case 'attributes.case_price':
    case 'attributes.correspondence_address':
    case 'relationship.requestor.coc_location_number':
    case 'relationship.requestor.noble_title':
    case 'relationship.requestor.trade_name':
    case 'bag_adres':
    case 'bag_adressen':
    case 'bag_straat_adres':
    case 'bag_straat_adressen':
    case 'bag_openbareruimte':
    case 'bag_openbareruimtes':
    case 'address_v2':
    case 'relationship':
      return [];
    case 'attributes.archival_state':
      return 'vernietigen';
    case 'attributes.urgency':
      return 'normal';
    case 'attributes.period_of_preservation_active':
      return 'active';
    case 'relationship.requestor.investigation':
    case 'relationship.requestor.is_secret':
      return 'yes';
    default:
      return '';
  }
};

/* eslint complexity: [2, 12] */
export const handleOnChange =
  (
    arrayHelpers: FieldArrayRenderProps | null,
    form: FormikProps<EditFormStateType>,
    field: FieldInputProps<any>,
    kind: KindType
  ) =>
  (event: React.ChangeEvent<any>) => {
    const { target } = event;
    let filterObj: FilterType | {};
    const isCustomAttribute = target?.value?.data?.type;
    const filterType = isCustomAttribute
      ? target.value.data.type
      : target.value.value;
    const uuid = v4();
    if (target?.value?.data?.type) {
      const { name, attributeUuid, magicString, type } = target.value.data;
      filterObj = {
        type: 'attributes.value',
        uuid,
        values: {
          label: name,
          attributeUuid,
          type,
          values: getDefaultValues(type) as any,
          magicString,
        },
      };
    } else {
      const defaultValues = getDefaultValues(filterType);
      filterObj = {
        type: filterType,
        uuid,
        values: defaultValues,
      };
    }

    const config = getFieldOperatorsConfig({
      filter: filterObj as any,
      kind,
    });

    if (config && config.location && isPopulatedArray(config.operators)) {
      if (isCustomAttribute) {
        //@ts-ignore
        filterObj.values.operator = config?.operators[0] as any;
      } else {
        //@ts-ignore
        filterObj.operator = config?.operators[0] as any;
      }
    }

    field.onChange(event);
    form.setFieldValue(field.name, null);
    arrayHelpers?.insert(0, filterObj);
    validateForm(form);
  };

export const filterOptionFunc =
  (values: any) => (option: ValueType<string, any>, input?: string) => {
    const hasFilters = isPopulatedArray(values.filters?.filters);
    const isPresentAsFilter = () => {
      if (!hasFilters) return true;
      return values.filters?.filters.some((filter: FilterType) =>
        option?.data?.type
          ? //@ts-ignore
            isCustomAttribute(filter) &&
            filter.values.magicString === option.data.magicString
          : filter.type === option.value
      );
    };

    const inputMatches = () => {
      if (!input || input.length < 3) return true;
      return Boolean(
        option.label &&
          (option.label as string).toLowerCase().indexOf(input?.toLowerCase()) >
            -1
      );
    };

    const inputMatch = inputMatches();
    const presenceCheck = hasFilters ? !isPresentAsFilter() : true;
    return input ? presenceCheck && inputMatch : presenceCheck;
  };

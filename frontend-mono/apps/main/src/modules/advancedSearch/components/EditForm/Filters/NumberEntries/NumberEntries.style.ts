// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles(() => ({
  wrapper: {},
  addWrapper: {
    display: 'flex',
    gap: 16,
    alignItems: 'center',
    '&>:nth-child(1)': {
      width: '50%',
    },
    '&>:nth-child(2)': {
      flex: 1,
    },
  },
  entry: {},
  entryWrapper: {
    marginTop: 20,
    display: 'flex',
    gap: 12,
    alignItems: 'center',
    '&>:nth-child(1)': {
      width: 'auto',
    },
  },
}));

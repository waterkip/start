// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles(({ mintlab: { greyscale } }: Theme) => ({
  departmentRoleWrapper: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  addButton: {
    width: 'max-content',
    justifySelf: 'flex-end',
    alignSelf: 'flex-end',
  },
  entriesWrapper: {
    marginTop: 10,
  },
  entryWrapper: {
    border: `2px solid ${greyscale.light}`,
    marginBottom: 8,
    padding: '20px 8px 8px 20px',
    display: 'flex',
    width: '100%',
  },
  entryField: {
    flex: 1,
  },
  entryDelete: {
    width: 50,
    alignItems: 'baseline',
    display: 'flex',
    justifyContent: 'center',
  },
  entryFieldRow: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 12,
    '&>:nth-child(1)': {
      width: 100,
    },
    '&>:nth-child(2)': {
      flex: 1,
    },
  },
}));

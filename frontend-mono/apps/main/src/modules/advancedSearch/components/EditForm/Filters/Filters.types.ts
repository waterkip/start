// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import { FormikProps } from 'formik';
import { EditFormStateType } from '../../../AdvancedSearch.types';
import { FilterType } from '../../../AdvancedSearch.types.filters';

export type FilterCommonPropsType = {
  name: string;
  identifier: string;
  t: i18next.TFunction;
  formik: FormikProps<EditFormStateType>;
  index: number;
  filter: FilterType;
};

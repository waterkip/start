

  # Advanded Search - Edit Form


  ## General

  The Edit Form uses Formik, in conjunction with its `<Field>` and `<FieldArray>` components to render and maintain the form's fields.

  The tabs are not mounted and unmounted when switching between them, but rather hidden and shown via CSS classes. When Formik triggers the fields validation functions, it will only account for fields that are mounted. Thus, errors in fields that are currently in an error state but not mounted, such as the `columns` field when there are no columns defined, are lost. The form then appears to have no errors and would be considered valid. Always having all fields rendered solves this problem.

  ## Columns

  
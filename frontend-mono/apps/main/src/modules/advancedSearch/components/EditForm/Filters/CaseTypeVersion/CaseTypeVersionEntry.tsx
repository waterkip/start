// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, { FunctionComponent } from 'react';
import * as i18next from 'i18next';
import fecha from 'fecha';
import { FormikProps, FieldInputProps } from 'formik';
import { get } from '@mintlab/kitchen-sink/source';
import { EditFormStateType } from '../../../../AdvancedSearch.types';
import { useStyles } from './CaseTypeVersion.style';
import { TRANSLATION_BASE } from './CaseTypeVersion';

type NumberEntryPropsType = {
  form: FormikProps<EditFormStateType>;
  field: FieldInputProps<any>;
  t: i18next.TFunction;
  index: number;
  classes: any;
};

const Entry: FunctionComponent<NumberEntryPropsType> = ({ form, field, t }) => {
  const { name } = field;
  const entry = get(form.values, name) as any;
  const classes = useStyles();

  return (
    <div className={classes.entry}>{`${entry.casetype_name}, ${t(
      `${TRANSLATION_BASE}.versionLong`
    )} ${entry.casetype_version_number}, ${fecha.format(
      new Date(entry.casetype_version_date),
      t('common:dates.dateAndTimeFormat')
    )}`}</div>
  );
};

export default Entry;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent } from 'react';
//@ts-ignore
import { isPopulatedArray } from '@mintlab/kitchen-sink/source/array';
import { useTheme } from '@mui/material';
import * as i18next from 'i18next';
import { Field, FieldInputProps } from 'formik';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { ColumnType, KindType } from '../../../AdvancedSearch.types';
import { useAttributeChoicesQuery } from '../components/AttributeFinder/AttributeFinder.library';
import { editFormSx } from '../../../styles/editForm';
import {
  mapColumnToChoice,
  getAttributesChoices,
  handleOnChange,
  filterOption,
} from './Columns.library';

type ColumnSelectorPropsType = {
  kind: KindType;
  name: string;
  columns: ColumnType[];
  handleSelectOnChange: (event: any) => void;
  classes: any;
  field: FieldInputProps<any>;
  t: i18next.TFunction;
  allColumns: ColumnType[];
};

/* eslint complexity: [2, 12] */
const ColumnSelectorCmp: FunctionComponent<ColumnSelectorPropsType> = ({
  name,
  handleSelectOnChange,
  classes,
  t,
  kind,
  field,
  columns,
  allColumns,
}) => {
  let choices = [];
  const selectProps = useAttributeChoicesQuery({
    config: {
      enabled: kind === 'case',
    },
  });

  const theme = useTheme<Theme>();
  const { loading, onInputChange } = selectProps;
  const sortFunc = (sortA: any, sortB: any) =>
    sortA.label.localeCompare(sortB.label);

  const defaultChoices = getAttributesChoices(t).sort(sortFunc);

  if (kind == 'case' && isPopulatedArray(defaultChoices)) {
    choices.push(...defaultChoices);
  }
  if (kind === 'case' && isPopulatedArray(selectProps.choices)) {
    choices.push(
      ...selectProps.choices
        .map(choice => ({
          ...choice,
          categoryLabel: t('editForm.attributeCategories.customAttributes'),
        }))
        .sort(sortFunc)
    );
  }
  if (kind === 'custom_object' && isPopulatedArray(allColumns)) {
    choices.push(...allColumns.map(mapColumnToChoice).sort(sortFunc));
  }

  return (
    <div className={classes.columnsSystemAttributeSelectors}>
      <Select
        {...field}
        loading={loading}
        onInputChange={onInputChange}
        variant="generic"
        name={`${name}-add-select`}
        value={null}
        choices={choices}
        filterOption={filterOption(columns)}
        onChange={handleOnChange(handleSelectOnChange)}
        placeholder={t(`editForm.fields.columns.selectColumn_${kind}`)}
        freeSolo={true}
        isClearable={false}
        sx={editFormSx().lightSelect}
        groupBy={option => option.categoryLabel}
        startAdornment={
          <Icon size="small" sx={{ color: theme.palette.elephant.light }}>
            {iconNames.search}
          </Icon>
        }
      />
    </div>
  );
};

export const ColumnSelector: FunctionComponent<
  Pick<
    ColumnSelectorPropsType,
    | 't'
    | 'classes'
    | 'kind'
    | 'columns'
    | 'handleSelectOnChange'
    | 'allColumns'
    | 'name'
  >
> = ({ t, classes, kind, columns, handleSelectOnChange, allColumns, name }) => (
  <Field
    name={name}
    component={ColumnSelectorCmp}
    classes={classes}
    handleSelectOnChange={handleSelectOnChange}
    t={t}
    kind={kind}
    columns={columns}
    allColumns={allColumns}
  />
);

export default ColumnSelector;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent } from 'react';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import {
  AttributeFinderPropsType,
  SearchResultAPIType,
} from './AttributeFinder.types';

const AttributeFinderCustomObject: FunctionComponent<
  AttributeFinderPropsType
> = ({
  handleSelectOnChange,
  name,
  t,
  selectKey,
  customFields,
  filterOption,
}) => {
  const choices = customFields?.length
    ? customFields.map(customField => ({
        label: customField.name,
        value: customField.uuid,
        data: customField,
      }))
    : [];

  return (
    <>
      <Select
        choices={choices}
        variant="generic"
        name={name}
        key={selectKey}
        isClearable={false}
        onChange={(event: React.ChangeEvent<any>) => {
          const data = event.target.value.data as SearchResultAPIType['data'];
          handleSelectOnChange({
            target: {
              ...event.target,
              value: {
                ...event.target.value,
                data,
              },
            },
          });
        }}
        placeholder={t('editForm.attributeFinder.placeholder')}
        {...(filterOption && { filterOption })}
        freeSolo={true}
      />
    </>
  );
};

export default AttributeFinderCustomObject;

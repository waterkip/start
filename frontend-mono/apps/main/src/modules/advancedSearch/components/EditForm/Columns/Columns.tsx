// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent, useEffect, useState, useRef } from 'react';
import * as i18next from 'i18next';
import { get } from '@mintlab/kitchen-sink/source';
import { FormikProps, FieldInputProps, Field } from 'formik';
import classNames from 'classnames';
import {
  DragDropContext,
  Draggable,
  Droppable,
  DropResult,
} from 'react-beautiful-dnd';
import {
  ColumnType,
  EditFormStateType,
  ClassesType,
  KindType,
  ModeType,
  IdentifierType,
} from '../../../AdvancedSearch.types';
import { useCustomFields } from '../../../query/useCustomFields';
import { getColumns } from '../../EditForm/EditForm.library';
import ColumnSelector from './ColumnSelector';
import ColumnEntry from './ColumnEntry';

const FIELD_NAME = 'columns';

type ColumnsPropsType = {
  classes: ClassesType;
  show: Boolean;
  t: i18next.TFunction;
  kind: KindType;
  mode: ModeType;
  identifier?: IdentifierType | null;
};

/* eslint complexity: [2, 12] */
const validateFunc = (t: i18next.TFunction) => (values: ColumnType[]) => {
  return values && Array.isArray(values) && values.length > 0
    ? undefined
    : t('editForm.fields.columns.errorMessage');
};

const Columns: FunctionComponent<
  ColumnsPropsType & {
    form: FormikProps<EditFormStateType>;
    field: FieldInputProps<any>;
  }
> = ({ form, field, classes, show, t, kind, mode, identifier }) => {
  const { name } = field;
  let selectedObjectType: string | null = null;
  const {
    setFieldValue,
    values: { columns, sortColumn, sortOrder },
  } = form;

  if (kind === 'custom_object') {
    selectedObjectType = get(form, 'values.selectedObjectType.value');
  }
  const [allColumns, setAllColumns] = useState<ColumnType[]>([]);
  const customFields = useCustomFields({
    uuid: selectedObjectType,
    config: {
      enabled: kind === 'custom_object' && Boolean(selectedObjectType),
      staleTime: 1000,
    },
  });

  const columnsRef = useRef(null as ColumnType[] | null);
  useEffect(() => {
    columnsRef.current = columns;
  }, [columns]);

  const onDragEnd = (result: DropResult) => {
    if (!result.destination) return;
    const newItems = reorder(result.source.index, result.destination.index);
    setFieldValue(name, newItems);
  };

  const reorder = (startIndex: number, endIndex: number) => {
    const result = Array.from(columns);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);
    return result;
  };

  const onDelete = (index: number) => {
    //@ts-ignore
    const newItems = columnsRef.current.filter(
      (thisItem, thisIndex) => thisIndex !== index
    );
    setFieldValue(name, newItems);
  };

  const handleSelectOnChange = ({ target: value }: any) => {
    const data = value.value.data;
    setFieldValue(name, [...[data], ...columns]);
  };

  const init = () => {
    const allColumns = getColumns(kind, customFields.data || null, t);
    if (kind === 'custom_object') {
      setAllColumns(allColumns);
    }
  };

  useEffect(() => {
    if (kind === 'custom_object' && customFields.data) init();
  }, [customFields.data]);

  useEffect(() => {
    if (kind === 'case') {
      setTimeout(() => init(), 0);
    }
  }, [mode, identifier]);

  return (
    <>
      <div
        className={classNames(classes.columnsWrapper, {
          [classes.show]: show,
          [classes.hide]: !show,
        })}
      >
        <ColumnSelector
          kind={kind}
          name={name}
          columns={columns}
          handleSelectOnChange={handleSelectOnChange}
          classes={classes}
          t={t}
          allColumns={allColumns}
        />
        <div>
          {/* @ts-ignore */}
          <DragDropContext onDragEnd={onDragEnd}>
            {/* @ts-ignore */}
            <Droppable droppableId={`${name}-droppable`}>
              {provided => (
                <div {...provided.droppableProps} ref={provided.innerRef}>
                  {(columns || []).map((item, index) => {
                    return (
                      //@ts-ignore
                      <Draggable
                        key={item.uuid}
                        draggableId={item.uuid}
                        index={index}
                      >
                        {(draggableProvided, draggableSnapshot) => (
                          <ColumnEntry
                            name={name}
                            item={item}
                            index={index}
                            provided={draggableProvided}
                            //@ts-ignore
                            snapshot={draggableSnapshot}
                            classes={classes}
                            onDelete={onDelete}
                            setFieldValue={setFieldValue}
                            sortColumn={sortColumn}
                            sortOrder={sortOrder}
                            t={t}
                          />
                        )}
                      </Draggable>
                    );
                  })}
                  {provided.placeholder as any}
                </div>
              )}
            </Droppable>
          </DragDropContext>
        </div>
      </div>
    </>
  );
};

export default (props: ColumnsPropsType) => (
  <Field
    {...props}
    name={FIELD_NAME}
    component={Columns}
    validate={validateFunc(props.t)}
    key={FIELD_NAME}
  />
);

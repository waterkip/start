// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, {
  FunctionComponent,
  useState,
  useEffect,
  useCallback,
} from 'react';
import set from 'date-fns/set';
import * as i18next from 'i18next';
import Button from '@mintlab/ui/App/Material/Button';
import DialogContent from '@mui/material/DialogContent';
import Divider from '@mui/material/Divider';
import {
  Dialog as UIDialog,
  DialogTitle,
} from '@mintlab/ui/App/Material/Dialog';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import {
  DateTimePicker,
  StaticDatePicker,
} from '@mintlab/ui/App/Material/DatePicker';
import { useStyles } from '../DateEntries.style';
import { getOperatorChoices, TRANSLATION_BASE } from '../DateEntries.library';
import {
  OperatorType,
  DateModeType,
} from '../../../../../AdvancedSearch.types';
import { DateEntryTypeAbsolute } from '../../../../../AdvancedSearch.types.filters';

type AbsoluteDialogPropsType = {
  onSubmit: (entry: DateEntryTypeAbsolute) => void;
  onClose: () => void;
  entry?: DateEntryTypeAbsolute | null;
  open: boolean;
  t: i18next.TFunction;
  mode: DateModeType;
};

const AbsoluteDialog: FunctionComponent<AbsoluteDialogPropsType> = ({
  onSubmit,
  entry,
  open,
  onClose,
  t,
  mode,
}) => {
  const [absoluteValue, setAbsoluteValue] = useState<string | null>(null);
  const [operator, setOperator] = useState<OperatorType | null>(null);
  const [dialogRef, setDialogRef] = useState<HTMLInputElement | null>(null);
  const classes = useStyles();

  useEffect(() => {
    setAbsoluteValue(entry?.value || new Date().toISOString());
    setOperator(entry?.operator || null);
  }, [open]);

  const isValid = () => {
    if (!absoluteValue || !operator) return false;
    return true;
  };

  const measuredRef = useCallback((node: any) => {
    if (node !== null) setDialogRef(node);
  }, []);

  const pickerProps = {
    ...(absoluteValue && { value: absoluteValue }),
    name: 'absolute-dialog-dateTimePicker',
    staticVariant: true,
    onChange: ({ target: { value } }: any) => {
      const parsedValue =
        mode === 'date'
          ? set(new Date(value), {
              hours: 0,
              minutes: 0,
              seconds: 0,
              milliseconds: 0,
            })
          : value;
      setAbsoluteValue(parsedValue.toISOString());
    },
    outputFormat: 'ISO',
  };

  const PickerComponent = mode === 'date' ? StaticDatePicker : DateTimePicker;

  return (
    <React.Fragment>
      <UIDialog onClose={onClose} open={open} ref={measuredRef}>
        <DialogTitle
          title={t(`${TRANSLATION_BASE}titles.absolute`)}
          icon="today"
        />
        <Divider />
        <DialogContent>
          <div className={classes.dialogWrapper}>
            <div className={classes.operator}>
              {dialogRef ? (
                <Select
                  variant="generic"
                  key={`absolute-dialog-select-operator`}
                  name={`absolute-dialog-select-operator`}
                  choices={getOperatorChoices(t, mode === 'date')}
                  value={operator || null}
                  isClearable={false}
                  onChange={(event: React.ChangeEvent<any>) =>
                    setOperator(event.target.value.value)
                  }
                  placeholder={t(`${TRANSLATION_BASE}operator.placeholder`)}
                  inputProps={{ readOnly: true }}
                />
              ) : null}
            </div>
            <Divider />
            <PickerComponent {...pickerProps} />
          </div>
        </DialogContent>
        <Divider />
        <div className={classes.actionButtons}>
          <Button
            name="closeAbsoluteDateEntry"
            action={() => {
              onClose();
            }}
          >
            {t(`verbs.cancel`)}
          </Button>
          <Button
            name="submitAbsoluteDateEntry"
            disabled={!isValid()}
            action={() => {
              onSubmit({
                type: 'absolute',
                operator,
                value: absoluteValue,
              });
              onClose();
            }}
          >
            {t(`verbs.confirm`)}
          </Button>
        </div>
      </UIDialog>
    </React.Fragment>
  );
};

export default AbsoluteDialog;

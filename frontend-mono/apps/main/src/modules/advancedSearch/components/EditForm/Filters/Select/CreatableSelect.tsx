// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent } from 'react';
import { Field } from 'formik';
import { CreatableSelect, ValueType } from '@mintlab/ui/App/Zaaksysteem/Select';
import { FilterCommonPropsType } from '../Filters.types';
import { ValidateFuncType } from '../../../../AdvancedSearch.types';
import { useStyles } from './Select.styles';

interface FilterSelectPropsType extends FilterCommonPropsType {
  validate?: ValidateFuncType<ValueType<string>[]> | undefined;
  choices?: any[];
  placeholder?: string;
  valuesTransformFunction?: (values: any) => any;
}

const PassThrough = (props: any) => {
  const { field } = props;
  let newProps = JSON.parse(JSON.stringify(props));

  if (props.valuesTransformFunction) {
    newProps.onChange = (event: React.ChangeEvent<any>) => {
      event.target.value = props.valuesTransformFunction(event.target.value);
      return field.onChange(event);
    };
  }

  return <CreatableSelect {...field} {...newProps} />;
};

const CreatableSelectCmp: FunctionComponent<FilterSelectPropsType> = ({
  name,
  validate = null,
  t,
  choices,
  valuesTransformFunction,
  ...rest
}) => {
  const classes = useStyles();

  return (
    <div className={classes.wrapper}>
      <Field
        name={name}
        validate={validate}
        component={PassThrough}
        choices={choices}
        valuesTransformFunction={valuesTransformFunction}
        {...rest}
      />
    </div>
  );
};

export default CreatableSelectCmp;

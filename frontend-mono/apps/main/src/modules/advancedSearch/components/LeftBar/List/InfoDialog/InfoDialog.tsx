// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { ReactNode, FunctionComponent } from 'react';
import fecha from 'fecha';
import * as i18next from 'i18next';
//@ts-ignore
import { isPopulatedArray } from '@mintlab/kitchen-sink/source/array';
import DialogContent from '@mui/material/DialogContent';
import Divider from '@mui/material/Divider';
import Button from '@mintlab/ui/App/Material/Button';
import DialogActions from '@mui/material/DialogActions';
import {
  Dialog as UIDialog,
  DialogTitle,
} from '@mintlab/ui/App/Material/Dialog';
import { renderTags } from '../../LeftBar.library';
import {
  AuthorizationsType,
  SavedSearchType,
} from '../../../../AdvancedSearch.types';

type InfoDialogPropsType = {
  savedSearch: SavedSearchType;
  open: boolean;
  onClose: () => void;
  classes: any;
  t: i18next.TFunction;
};

const getAuthorizationsInfo = (
  authorizations: AuthorizationsType[],
  t: i18next.TFunction
) => {
  if (authorizations.includes('admin')) {
    return t('moreInfo.authorizationLevels.admin');
  } else if (authorizations.includes('readwrite')) {
    return t('moreInfo.authorizationLevels.readwrite');
  } else if (authorizations.includes('read')) {
    return t('moreInfo.authorizationLevels.read');
  }
  return '';
};

/* eslint complexity: [2, 10] */
const InfoDialog: FunctionComponent<InfoDialogPropsType> = ({
  savedSearch,
  open,
  onClose,
  classes,
  t,
}) => {
  const DATE_FORMAT = 'D MMMM YYYY HH:mm';
  const { labels, authorizations } = savedSearch;
  const showSharedBy = !authorizations.includes('admin');
  const hasLabels = isPopulatedArray(labels);

  if (!savedSearch.metaData) return null;

  const {
    createdByName,
    createdByDate,
    lastEditedByName,
    lastEditedByDate,
    lastEditedByUuid,
  } = savedSearch.metaData;
  return (
    <React.Fragment>
      <UIDialog onClose={onClose} open={open}>
        <DialogTitle title={savedSearch.name} />
        <Divider />
        <DialogContent>
          <div className={classes.infoDialogWrapper}>
            <div className={classes.infoDialogRow}>
              <span>{t('moreInfo.created') as ReactNode}</span>
              <span>
                {`${
                  createdByDate
                    ? fecha.format(new Date(createdByDate), DATE_FORMAT)
                    : ''
                } ${t('moreInfo.by')} ${createdByName}`}
              </span>
            </div>
            {Boolean(lastEditedByDate) && Boolean(lastEditedByUuid) && (
              <div className={classes.infoDialogRow}>
                <span>{t('moreInfo.edited') as ReactNode}</span>
                <span>{`${fecha.format(
                  new Date(lastEditedByDate),
                  DATE_FORMAT
                )} ${t('moreInfo.by')} ${lastEditedByName}`}</span>
              </div>
            )}
            <div className={classes.infoDialogRow}>
              <span>{t('moreInfo.labels') as ReactNode}</span>
              <span className={classes.infoDialogLabels}>
                {hasLabels
                  ? renderTags({ editing: false, classes })(labels)
                  : t('moreInfo.none')}
              </span>
            </div>
            <div className={classes.infoDialogRow}>
              <span>{t('moreInfo.authorizations') as ReactNode}</span>
              <span>{getAuthorizationsInfo(authorizations, t)}</span>
            </div>
            {showSharedBy && (
              <div className={classes.infoDialogRow}>
                <span>{t('moreInfo.sharedBy') as ReactNode}</span>
                <span>{`${createdByName}`}</span>
              </div>
            )}
          </div>
        </DialogContent>
        <Divider />
        <DialogActions>
          <Button name="closeInfoDialogButton" action={() => onClose()}>
            {t('verbs.close')}
          </Button>
        </DialogActions>
      </UIDialog>
    </React.Fragment>
  );
};

export default InfoDialog;

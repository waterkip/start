// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent } from 'react';
import * as i18next from 'i18next';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { useNavigate } from 'react-router-dom';
import { QueryClient } from '@tanstack/react-query';
import FormDialog from '@zaaksysteem/common/src/components/dialogs/FormDialog/FormDialog';
import {
  openServerError,
  openSnackbar,
  snackbarMargin,
} from '@zaaksysteem/common/src/signals';
import { invalidateAllQueries } from '../../../../query/library';
import { KindType, SavedSearchType } from '../../../../AdvancedSearch.types';
import { useSingleSaveMutation } from '../../../../query/useSingle';
import { getURLMatches } from '../../../../library/library';

const TRANSLATION_BASE = 'dialogs.copy';

type CopyDialogPropsType = {
  savedSearch: SavedSearchType;
  open: boolean;
  onClose: () => void;
  t: i18next.TFunction;
  client: QueryClient;
  kind: KindType;
};

/* eslint complexity: [2, 10] */
const CopyDialog: FunctionComponent<CopyDialogPropsType> = ({
  savedSearch,
  open,
  onClose,
  t,
  client,
  kind,
}) => {
  const { authorizations } = savedSearch;
  const navigate = useNavigate();
  const urlMatches = getURLMatches();
  const saveMutation = useSingleSaveMutation({
    kind,
    identifier: null,
    mode: 'new',
    authorizations,
  });

  const formDefinition = [
    {
      name: 'newName',
      type: fieldTypes.TEXT,
      value: `${savedSearch.name} (${t(`${TRANSLATION_BASE}.copy`)})`,
      label: t(`${TRANSLATION_BASE}.label`),
      required: true,
      placeholder: t(`${TRANSLATION_BASE}.placeholder`),
    },
  ];

  const doCopy = (values: any) => {
    const newSavedSearch = {
      ...savedSearch,
      uuid: null,
      name: values.newName,
      type: 'user',
    };
    saveMutation.mutate(newSavedSearch, {
      onSuccess: data => {
        invalidateAllQueries(client);
        openSnackbar({ message: t('snacks.copySaved'), sx: snackbarMargin });
        onClose();
        navigate(
          `/${urlMatches?.params.prefix}/${urlMatches?.params.module}/${kind}/view/${data}`
        );
      },
      onError: openServerError,
    });
  };

  return (
    <FormDialog
      saving={false}
      onSubmit={doCopy}
      icon="file_copy"
      title={`${t(`${TRANSLATION_BASE}.title`)} ${savedSearch.name}`}
      formDefinitionT={t as any}
      onClose={onClose}
      formDefinition={formDefinition}
      open={open}
      saveLabel={t(`${TRANSLATION_BASE}.copyBtn`)}
    />
  );
};

export default CopyDialog;

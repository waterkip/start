// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, {
  useEffect,
  FunctionComponent,
  Dispatch,
  SetStateAction,
  MutableRefObject,
} from 'react';
import * as i18next from 'i18next';
import { QueryClient } from '@tanstack/react-query';
import TablePagination from '@mui/material/TablePagination';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import IconButton from '@mui/material/IconButton';
import { useStyles } from '../Pagination.styles';
import {
  UseResultsQueryQueryParamsType,
  prefetchPages,
} from '../../../../query/useResults';
import { SKIP_PAGES } from '../../../../library/config';
import {
  CursorCollectionType,
  SkipDirectionType,
} from '../../../../AdvancedSearch.types';

type ResultPaginationPropsType = {
  totalResults: number;
  page: number;
  resultsPerPage: number;
  setResultsPerPage: Dispatch<SetStateAction<number>>;
  setPage: Dispatch<SetStateAction<number>>;
  queryClient: QueryClient;
  queryParams: UseResultsQueryQueryParamsType;
  prevPageRef: MutableRefObject<number | null>;
  cursorCollectionRef: MutableRefObject<CursorCollectionType | null>;
  cursorCollection: CursorCollectionType | null;
  isLoading: boolean;
  t: i18next.TFunction;
  busy: boolean;
  setBusy: Dispatch<SetStateAction<boolean>>;
};

/* eslint complexity: [2, 20] */
const ResultsPagination: FunctionComponent<ResultPaginationPropsType> = ({
  totalResults = 0,
  page,
  resultsPerPage,
  setResultsPerPage,
  setPage,
  queryClient,
  queryParams,
  prevPageRef,
  cursorCollectionRef,
  cursorCollection,
  isLoading,
  t,
  busy = false,
  setBusy,
}) => {
  const classes = useStyles();

  const color = 'rgba(0, 0, 0, 0.87)';
  const colorDisabled = 'rgba(0, 0, 0, 0.26)';
  const totalPages = Math.ceil(totalResults / resultsPerPage);
  const forceDisable = busy || isLoading;
  const disableSkipPrev = forceDisable || page < SKIP_PAGES + 1;
  const disableSkipNext = forceDisable || totalPages - page < SKIP_PAGES;
  const disableNext = forceDisable || page === totalPages;
  const disablePrev = forceDisable || page === 1;

  useEffect(() => setBusy(false), [page, resultsPerPage]);

  const handleSingleNavigation = (newPage: number) => {
    setBusy(true);
    prevPageRef.current = page;
    cursorCollectionRef.current = cursorCollection;
    setPage(newPage);
  };

  const handleSkipNavigation = (direction: SkipDirectionType) => {
    setBusy(true);
    prefetchPages({
      page,
      pages: SKIP_PAGES,
      direction,
      setPage,
      queryClient,
      queryParams,
      prevPageRef,
      cursorCollection,
      cursorCollectionRef,
    });
  };

  const getPage = () => {
    if (isLoading) return 0;
    return resultsPerPage >= totalResults ? 0 : page - 1;
  };

  return (
    <div className={classes.resultsTablePaging}>
      {totalPages > 0 ? (
        <TablePagination
          disabled={forceDisable}
          component="div"
          count={totalResults}
          page={getPage()}
          onPageChange={() => {}}
          rowsPerPage={resultsPerPage}
          rowsPerPageOptions={[5, 10, 25, 50]}
          onRowsPerPageChange={(event: any) => {
            setResultsPerPage(event?.target?.value);
          }}
          classes={{
            actions: classes.actions,
          }}
          labelRowsPerPage={t('pagination.rowsPerPage') as string}
          ActionsComponent={paginationProps => {
            return (
              <div className={classes.actionButtons}>
                <div className={classes.pageIndicator}>
                  {
                    t('pagination.pageLocation', {
                      page,
                      totalPages,
                    }) as string
                  }
                </div>
                <IconButton
                  onClick={() => handleSkipNavigation('back')}
                  size="small"
                  disabled={disableSkipPrev}
                >
                  <Tooltip
                    enterDelay={200}
                    title={t('pagination.skipPrevious', {
                      skip: SKIP_PAGES,
                    })}
                  >
                    <Icon
                      size="small"
                      //@ts-ignore
                      color={disableSkipPrev ? colorDisabled : color}
                    >
                      {iconNames.keyboard_double_arrow_left}
                    </Icon>
                  </Tooltip>
                </IconButton>
                <IconButton
                  onClick={() => handleSingleNavigation(page - 1)}
                  size="small"
                  disabled={disablePrev}
                >
                  <Tooltip enterDelay={200} title={t('pagination.previous')}>
                    <Icon
                      size="small"
                      //@ts-ignore
                      color={disablePrev ? colorDisabled : color}
                    >
                      {iconNames.keyboard_arrow_left}
                    </Icon>
                  </Tooltip>
                </IconButton>
                <IconButton
                  onClick={() => handleSingleNavigation(page + 1)}
                  size="small"
                  disabled={disableNext}
                >
                  <Tooltip enterDelay={200} title={t('pagination.next')}>
                    <Icon
                      size="small"
                      //@ts-ignore
                      color={disableNext ? colorDisabled : color}
                    >
                      {iconNames.keyboard_arrow_right}
                    </Icon>
                  </Tooltip>
                </IconButton>
                <IconButton
                  onClick={() => handleSkipNavigation('forward')}
                  size="small"
                  disabled={disableSkipNext}
                >
                  <Tooltip
                    enterDelay={200}
                    title={t('pagination.skipNext', {
                      skip: SKIP_PAGES,
                    })}
                  >
                    <Icon
                      size="small"
                      //@ts-ignore
                      color={disableSkipNext ? colorDisabled : color}
                    >
                      {iconNames.keyboard_double_arrow_right}
                    </Icon>
                  </Tooltip>
                </IconButton>
              </div>
            );
          }}
        />
      ) : null}
    </div>
  );
};

export default ResultsPagination;

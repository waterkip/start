// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles(({ palette: { danger } }: Theme) => ({
  caseStatusWrapper: {
    width: 42,
    position: 'relative',
  },
  caseStatusNr: {
    position: 'absolute',
    width: 18,
    height: 18,
    top: -8,
    left: 14,
    color: 'white',
    borderRadius: '50%',
    border: '2px solid white',
    backgroundColor: danger.dark,
    textAlign: 'center',
    fontSize: 11,
  },
}));

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles(() => {
  return {
    exportDialogWrapper: {
      width: 500,
      minHeight: 300,
      display: 'flex',
      flexDirection: 'column',
      padding: 10,
    },
    exportDialogSection: {
      marginTop: 10,
      marginBottom: 10,
    },
    exportDialogOptionsRow: {
      display: 'flex',
      marginBottom: 10,
      alignItems: 'center',
      width: '80%',

      '&>:nth-child(1)': {
        width: '40%',
        fontWeight: 600,
      },
      '&>:nth-child(2)': {
        flex: 1,
      },
      '&>:nth-child(3)': {
        marginLeft: 14,
        width: 50,
      },
    },
    exportDialogCommands: {
      display: 'flex',
      alignItems: 'center',
      '&>:nth-child(1)': {
        flex: 1,
        marginRight: 20,
      },
      '&>:nth-child(2)': {
        width: 120,
      },
    },
  };
});

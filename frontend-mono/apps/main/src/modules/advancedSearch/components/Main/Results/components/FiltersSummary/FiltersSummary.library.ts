// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
//@ts-ignore
import { isPopulatedArray } from '@mintlab/kitchen-sink/source/array';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import * as i18next from 'i18next';
import { asArray } from '@mintlab/kitchen-sink/source';
import { formatCurrency } from '@zaaksysteem/common/src/library/currency';
import fecha from 'fecha';
import { KindType } from '../../../../../AdvancedSearch.types';
import {
  FilterType,
  FiltersType,
} from '../../../../../AdvancedSearch.types.filters';
import { getReadableValue } from '../../../../EditForm/Filters/DateEntries/DateEntries.library';
import {
  getDateMode,
  filterTranslationKeys,
  FIELD_TYPES_YES_NO_BOOLEAN,
  FIELD_TYPES_AS_OPTIONS_OBJECT,
} from '../../../../../library/config';
import {
  isCustomAttribute,
  getTypeOrSubType,
} from '../../../../../library/library';

export const parseFilters = (filters: FiltersType | null) => {
  if (!filters || !isPopulatedArray(filters.filters)) return null;
  return {
    ...filters,
    filters: filters.filters.filter(
      thisF => thisF.type !== 'relationship.custom_object_type'
    ),
  };
};

/* eslint complexity: [2, 50] */
export const getFiltersSummary = (
  filters: FiltersType,
  t: i18next.TFunction,
  kind: KindType
) => {
  const getTitle = (filter: FilterType) => {
    const translationKey =
      filterTranslationKeys[
        filter.type as keyof typeof filterTranslationKeys
      ] || filter.type;
    return isCustomAttribute(filter)
      ? filter.values.label
      : t(`editForm.fields.filters.fields.${translationKey}.label`);
  };

  const getType = (filter: FilterType) =>
    isCustomAttribute(filter) ? 'customAttribute' : 'systemAttribute';

  const getTranslatedOperator = (param: FilterType | FiltersType) => {
    return t(
      `editForm.fields.filters.operators.${
        //@ts-ignore
        param.operator || param.values.operator || 'or'
      }`
    );
  };

  if (!filters || !isPopulatedArray(filters.filters)) return null;

  const parsedFilters = filters.filters
    .flatMap(filter => {
      let values = null;
      if (
        filter.values &&
        //@ts-ignore
        isPopulatedArray(filter.values) &&
        [
          'relationship.case_type.id',
          'keyword',
          'relationship.requestor.id',
          'relationship.assignee.id',
          'relationship.coordinator.id',
          'attributes.case_location',
          'attributes.correspondence_address',
          'attributes.parent_number',
          'attributes.custom_number',
          'attributes.external_reference',
          'attributes.case_phase',
          'attributes.subject',
          'relationship.requestor.zipcode',
          'relationship.requestor.coc_number',
          'relationship.case_type.identification',
          'relationship.requestor.coc_location_number',
          'relationship.requestor.noble_title',
          'relationship.requestor.trade_name',
          'relationship.requestor.department',
          'relationship.requestor.correspondence_zipcode',
          'relationship.requestor.correspondence_street',
          'relationship.requestor.correspondence_city',
          'relationship.requestor.street',
          'relationship.requestor.city',
          'relationship.custom_object.id',
        ].includes(filter.type)
      ) {
        //@ts-ignore
        values = filter.values.map(value => value.label);
      } else if (filter.type === 'attributes.case_number') {
        if (filter?.options?.truncate === true) {
          const kindTranslation = t(
            `editForm.fields.filters.fields.caseNumber.caseNr`,
            {
              count: filter?.values?.length,
            }
          );
          values = [`${filter?.values?.length} ${kindTranslation}`];
        } else {
          values = (filter.values || []).map(value => value.label);
        }
      } else if (
        [
          'attributes.channel_of_contact',
          'attributes.payment_status',
          'attributes.result',
          'attributes.confidentiality',
          'attributes.has_unaccepted_changes',
          'attributes.retention_period_source_date',
          'relationship.case_type.confidentiality',
          'relationship.requestor.gender',
        ].includes(filter.type)
      ) {
        //@ts-ignore
        values = filter.values.map(value => {
          const translationKey =
            filterTranslationKeys[
              filter.type as keyof typeof filterTranslationKeys
            ] || filter.type;
          return t(
            `editForm.fields.filters.fields.${translationKey}.choices.${value}`
          );
        });
      } else if (
        [
          'attributes.registration_date',
          'attributes.destruction_date',
          'attributes.target_date',
          'attributes.stalled_until',
          'attributes.stalled_since',
          'attributes.completion_date',
          'attributes.last_modified',
          'relationship.requestor.date_of_death',
          'relationship.requestor.date_of_birth',
          'relationship.requestor.date_of_marriage',
        ].includes(filter.type)
      ) {
        //@ts-ignore
        values = filter.values.map(value =>
          getReadableValue(value, t, getDateMode(filter.type))
        );
      } else if (
        [
          'attributes.archive_status',
          'attributes.archival_state',
          'attributes.urgency',
          'attributes.period_of_preservation_active',
        ].includes(filter.type) ||
        (kind === 'custom_object' &&
          ['attributes.status'].includes(filter.type))
      ) {
        const translationKey =
          filterTranslationKeys[
            filter.type as keyof typeof filterTranslationKeys
          ] || filter.type;
        values = [
          t(
            `editForm.fields.filters.fields.${translationKey}.choices.${filter.values}`
          ),
        ];
      } else if (filter.type === 'attributes.department_role') {
        values = filter.values.map(
          value =>
            `${value.department?.label} ${
              value.role ? `/ ${value.role?.label}` : ''
            } `
        );
      } else if (['attributes.case_price'].includes(filter.type)) {
        //@ts-ignore
        values = filter.values.map((value: any) => {
          const translations = t(
            `editForm.fields.filters.types.numberEntries.operators`,
            {
              returnObjects: true,
            }
          );
          return `${translations[value[0]]} ${value[1]}`;
        });
      } else if (
        kind === 'case' &&
        ['attributes.status'].includes(filter.type)
      ) {
        //@ts-ignore
        values = filter?.values?.map(value => {
          return t(`case.status.${value}`) as string;
        });
      } else if (filter.type === 'relationship.case_type.version') {
        values = filter?.values?.map(
          //@ts-ignore
          value =>
            `${value.casetype_name} ${t(
              `editForm.fields.filters.fields.casetypeVersion.versionShort`
            )}${value.casetype_version_number}, ${fecha.format(
              new Date(value.casetype_version_date),
              t('common:dates.dateAndTimeFormat')
            )} `
        );
      } else if (isCustomAttribute(filter)) {
        const subType = getTypeOrSubType(filter);
        if (
          kind === 'custom_object' ||
          (kind === 'case' &&
            [
              'bag_adres',
              'bag_adressen',
              'bag_straat_adres',
              'bag_straat_adressen',
              'bag_openbareruimte',
              'bag_openbareruimtes',
              'address_v2',
              'email',
              'bankaccount',
              'numeric',
              'text',
              'textarea',
              'richtext',
            ].includes(subType))
        ) {
          //@ts-ignore
          values = filter.values.values.map(
            (val: ValueType<any, any>) => val.label
          );
        } else if (FIELD_TYPES_AS_OPTIONS_OBJECT.includes(subType)) {
          values = filter.values.values;
        } else if (subType.indexOf('valuta') > -1) {
          //@ts-ignore
          values = filter.values.values.map((value: any) => {
            const translations = t(
              `editForm.fields.filters.types.numberEntries.operators`,
              {
                returnObjects: true,
              }
            );
            // @ts-ignore
            return `${translations[value[0]]} ${formatCurrency(t, value[1])}`;
          });
        } else if (subType === 'relationship') {
          const additionalData = filter.values.additionalData;
          switch (additionalData.type) {
            case 'custom_object':
              values = [`${additionalData.name}`];
              break;
            case 'subject':
              values = (
                (filter.values?.values as ValueType<string>[]) || []
              ).map(val => val.label);
              break;
          }
        } else if (subType === 'date') {
          values = (filter.values?.values || ([] as any)).map((value: any) =>
            getReadableValue(value, t, getDateMode(filter.values.type))
          );
        } else if (subType === 'file') {
          values = [t(`editForm.fields.filters.types.file.summary`)];
        }
      } else if (FIELD_TYPES_YES_NO_BOOLEAN.includes(filter.type)) {
        const translations = t(
          `editForm.fields.filters.fields.generic.yesNo.choices`,
          {
            returnObjects: true,
          }
        );

        values = asArray(translations[filter?.values as any]);
      } else {
        values = null;
      }

      return values && values?.length
        ? {
            title: getTitle(filter),
            filter: {
              operator: getTranslatedOperator(filter).toLowerCase(),
              values,
            },
            type: getType(filter),
          }
        : [];
    })
    .filter(thisFilter => Boolean(thisFilter));

  return {
    operator: getTranslatedOperator(filters).toLowerCase(),
    filters: parsedFilters,
  };
};

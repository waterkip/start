// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const permissionsStyles = ({ greyscale, elephant }: any) => {
  return {
    permissionsWrapper: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      gap: 16,
    },
    permissionsEntries: {
      width: '100%',
    },
    newPermission: {
      display: 'flex',
      width: '100%',
      justifyContent: 'flex-end',
      alignItems: 'center',
      '&>:nth-child(1)': {
        marginRight: 10,
      },
    },
    permissionsNoPermissions: {
      whiteSpace: 'normal',
      width: '100%',
      overflow: 'hidden',
    },
    permissionsEntryWrapper: {
      border: `2px solid ${elephant.lightest}`,
      borderRadius: 10,
      padding: '10px 10px 0px 10px',
      background: 'white',
    },
    permissionsEntryDelete: {
      display: 'flex',
      justifyContent: 'flex-end',
      marginBottom: 6,
    },
    permissionsEntryRow: {
      display: 'flex',
      flexDirection: 'row',
      '&>:nth-child(1)': {
        width: 100,
        display: 'flex',
        alignItems: 'center',
      },
      '&>:nth-child(2)': {
        flex: 1,
      },
      marginBottom: 12,
    },
    permissionsEntryRowSelect: {
      '&>:nth-child(2)': {
        flex: 1,
        backgroundColor: greyscale.light,
      },
    },
    permissionsAddButton: {
      width: 120,
      marginLeft: 380,
    },
    permissionsInfo: {
      marginBottom: 20,
      display: 'flex',
      alignItems: 'center',
      alignSelf: 'flex-end',
      justifyContent: 'flex-end',
      color: elephant.dark,
    },
    permissionsText: {
      fontWeight: 400,
      textAlign: 'left' as const,
      '& li': {
        marginBottom: 8,
      },
      '& ul': {
        marginLeft: 16,
        padding: 0,
      },
    },
  };
};

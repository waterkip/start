// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { Routes, Route, Navigate } from 'react-router-dom';
import { ReactQueryDevtools } from '@tanstack/react-query-devtools';
import attributesLocale from '@zaaksysteem/common/src/locale/attributes.locale';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import { TopbarTitle } from '@mintlab/ui/App/Zaaksysteem/Layout/Topbar/TopbarTitle/TopbarTitle';
import locale from './locale/AdvancedSearch.locale';
import AdvancedSearch from './AdvancedSearch';
import LocalStateProvider from './context/LocalStateProvider';
import WidgetDummy from './WidgetDummy';

const AdvancedSearchModule: React.FunctionComponent = () => {
  const [t] = useTranslation('search');
  return (
    <>
      <Routes>
        <Route
          path="widget/:identifier"
          element={<WidgetDummy kind={'case'} />}
        />
        <Route path="" element={<Navigate to="case" replace={true} />} />
        <Route
          path=":kind/*"
          element={
            <LocalStateProvider>
              <AdvancedSearch />
            </LocalStateProvider>
          }
        />
      </Routes>
      <TopbarTitle title={t('title')} />
    </>
  );
};

export default () => (
  <I18nResourceBundle resource={locale} namespace="search">
    <I18nResourceBundle resource={attributesLocale} namespace="attributes">
      <ReactQueryDevtools />
      <AdvancedSearchModule />
    </I18nResourceBundle>
  </I18nResourceBundle>
);

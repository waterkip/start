// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useQuery } from '@tanstack/react-query';
import { openServerError } from '@zaaksysteem/common/src/signals';
import { FiltersType, GetDateType, InitiateExportType } from './Log.types';
import { fetchData, fetchUsers, startExport } from './Log.requests';

export const defaultFilters: FiltersType = {
  keyword: '',
  caseId: '',
  user: null,
  page: 0,
  rowsPerPage: 50,
};

const formatRow = ({
  instance: {
    event_id,
    case_id,
    date_created,
    created_by,
    component,
    event_data,
  },
}: any) => ({
  name: event_id,
  uuid: event_id,
  caseId: case_id,
  dateCreated: date_created,
  event: event_data?.human_readable,
  component,
  user: created_by?.instance?.display_name,
});

const formatRows = (rows: any) => rows.map((row: any) => formatRow(row));

const convertFilters = (filters: FiltersType) => {
  const { page, rowsPerPage, caseId, keyword, user } = filters;

  return {
    page: page + 1,
    rows_per_page: rowsPerPage,
    'query:match:case_id': caseId || undefined,
    'query:match:keyword': keyword || undefined,
    'query:match:subject': user?.value || undefined,
  };
};

export const getData: GetDateType = async filters => {
  const params = convertFilters(filters);
  const data = await fetchData(params);
  const rows = formatRows(data.rows);
  const count = data.pager.total_rows;

  return {
    rows,
    count,
  };
};

const formatUser = ({ meta: { summary }, id }: any) => ({
  label: summary,
  value: id,
});

const formatUsers = (users: any) => users.map((user: any) => formatUser(user));

export const useUsersQuery = () => {
  const [input, setInput] = React.useState<string | undefined>(undefined);
  const enabled = Boolean(input);

  const data = useQuery(
    ['users', input],
    async ({ queryKey: [__, keyword] }) => {
      return fetchUsers(keyword || '');
    },
    { enabled, onError: openServerError }
  );

  return {
    onInputChange: (ev: any, val: any, reason: any) =>
      ['input', 'clear'].includes(reason) && setInput(val),
    loading: data.status === 'loading' && enabled,
    choices: formatUsers(data.data?.data || []),
  };
};

export const initiateExport: InitiateExportType = async filters => {
  const params = convertFilters(filters);

  return await startExport(params);
};

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import classNames from 'classnames';
import { useTranslation } from 'react-i18next';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import Pagination from '@mintlab/ui/App/Material/Pagination';
import SortableTable, {
  useSortableTableStyles,
} from '@mintlab/ui/App/Zaaksysteem/SortableTable';
import { DataType, FiltersType, SetFiltersType } from '../Log.types';
import { useDataTableStyles, useTableStyles } from './DataTable.style';
import { getColumns } from './DataTable.library';

type DataTablePropsType = {
  data: DataType;
  loading: boolean;
  filters: FiltersType;
  setFilters: SetFiltersType;
};

const DataTable: React.ComponentType<DataTablePropsType> = ({
  data,
  loading,
  filters,
  setFilters,
}) => {
  const classes = useDataTableStyles();
  const tableStyles = useTableStyles();
  const sortableTableStyles = useSortableTableStyles();
  const [t] = useTranslation('log');

  const columns = getColumns(t as any, classes);
  const rows = data.rows;

  return (
    <div className={classes.wrapper}>
      <div
        className={classNames(classes.table, {
          [classes.tableLoading]: loading,
        })}
      >
        <div
          style={{
            flex: '1 1 auto',
            minWidth: columns.reduce(
              (acc, { minWidth }) => acc + minWidth + 10,
              60
            ),
            height: `calc(${rows.length} * 42px + 50px)`,
          }}
        >
          <SortableTable
            styles={{ ...sortableTableStyles, ...tableStyles }}
            rows={rows}
            //@ts-ignore
            columns={columns}
            rowHeight={42}
            loading={false}
            noRowsMessage={t('table.noResults')}
            sortDirectionDefault="DESC"
            sortInternal={false}
            sorting="column"
          />
        </div>
        {loading && (
          <div className={classes.loader}>
            <Loader />
          </div>
        )}
      </div>
      <div className={classes.pagination}>
        <Pagination
          scope="log-pagination"
          component={'div'}
          count={data.count}
          labelRowsPerPage={`${t('table.labelRowsPerPage')}:`}
          rowsPerPageOptions={[5, 10, 20, 50]}
          rowsPerPage={filters.rowsPerPage}
          changeRowsPerPage={(event: any) =>
            setFilters({
              ...filters,
              page: 0,
              rowsPerPage: event.target.value,
            })
          }
          page={filters.page}
          getNewPage={(page: number) => setFilters({ ...filters, page })}
          labelDisplayedRows={({ from, to, count }: any) =>
            `${from}-${to} ${t('common:of')} ${count}`
          }
        />
      </div>
    </div>
  );
};

export default DataTable;

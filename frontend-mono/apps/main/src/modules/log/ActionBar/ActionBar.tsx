// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useDebouncedCallback } from 'use-debounce';
import {
  snackbarMargin,
  openServerError,
  openSnackbar,
} from '@zaaksysteem/common/src/signals';
import ActionBar from '@mintlab/ui/App/Zaaksysteem/ActionBar';
import { FiltersType, SetFiltersType } from './../Log.types';
import { initiateExport, useUsersQuery } from './../Log.library';

type ActionBarPropsType = {
  filters: FiltersType;
  setFilters: SetFiltersType;
};

const ActionBarWrapper: React.ComponentType<ActionBarPropsType> = ({
  filters,
  setFilters,
}) => {
  const [t] = useTranslation('log');
  const [loading, setLoading] = useState<boolean>(false);
  const [keyword, setKeyword] = useState<string>('');
  const [caseId, setCaseId] = useState<string>('');
  const setFilter = (filter: any) => {
    setFilters({ ...filters, ...filter, page: 0 });
  };
  const [debouncedCallback] = useDebouncedCallback((type, value) => {
    if (!value.length || value.length >= 3) setFilter({ [type]: value });
  }, 500);

  const selectProps = useUsersQuery();

  return (
    <ActionBar
      filters={[
        {
          type: 'text',
          value: keyword,
          placeholder: t('filters.keyword'),
          onChange: (event: any) => {
            const value = event.target.value;
            setKeyword(value);
            debouncedCallback('keyword', event.target.value);
          },
          closeAction: () => {
            setKeyword('');
            setFilter({ keyword: '' });
          },
        },
        {
          type: 'text',
          value: caseId,
          placeholder: t('filters.caseId'),
          onChange: (event: any) => {
            const value = event.target.value;
            setCaseId(value);
            debouncedCallback('caseId', event.target.value);
          },
          closeAction: () => {
            setCaseId('');
            setFilter({ caseId: '' });
          },
        },
        {
          type: 'select',
          value: filters.user,
          placeholder: t('filters.user'),
          onChange: (event: any) => {
            setFilter({
              user: event.target.value,
            });
          },
          filterOption: () => true,
          ...selectProps,
        },
      ]}
      advancedActions={[
        {
          label: t('export.button'),
          disabled: loading,
          name: 'export',
          action: () => {
            setLoading(true);
            initiateExport(filters)
              .then(() => {
                openSnackbar({
                  message: t('export.snack'),
                  sx: snackbarMargin,
                });
              })
              .catch(openServerError)
              .finally(() => setLoading(false));
          },
        },
      ]}
    />
  );
};

export default ActionBarWrapper;

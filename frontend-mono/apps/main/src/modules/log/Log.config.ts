// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const defaultOptions = {
  queries: {
    staleTime: Infinity,
    refetchOnWindowFocus: false,
    retry: 0,
  },
};

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { IconButton, Typography } from '@mui/material';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import {
  STYLES_VERSION,
  cssProperties,
  loadTemplate,
  saveTemplate,
  uploadTemplate,
} from './customStylingTemplates.library';

/* eslint complexity: [2, 8] */
const Field = ({
  type = 'text',
  value = '',
  label = '',
  name = '',
  accept = '',
  tooltip = '',
}) => {
  const labelEl = (
    <span
      style={{
        ...(type === 'file' ? { width: '150px' } : {}),
      }}
    >
      {label || name}
    </span>
  );
  return (
    <label
      style={{
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        fontSize: '15px',
        marginBottom: '10px',
      }}
    >
      {tooltip ? <Tooltip title={tooltip}>{labelEl}</Tooltip> : labelEl}
      <input
        style={{
          fontSize: '15px',
          height: '30px',
          minWidth: '200px',
          ...(type === 'file' ? { width: '200px' } : {}),
        }}
        type={type}
        name={name}
        id={name}
        accept={accept}
        defaultValue={value}
      />
      {type === 'file' && (
        <IconButton
          onClick={() => {
            const url = URL.createObjectURL(
              //@ts-ignore
              document.getElementById(name).files[0]
            );

            window.open(url, '_blank');
          }}
        >
          <Icon>{iconNames.eye}</Icon>
        </IconButton>
      )}
    </label>
  );
};

export default () => {
  return (
    <>
      <input
        id="archiveUpload"
        style={{ width: 0, height: 0, overflow: 'hidden' }}
        type="file"
        onChange={loadTemplate}
        accept=".zss"
      />
      <form
        style={{
          padding: '0 30px 50px 30px',
          maxWidth: '600px',
          display: 'flex',
          flexDirection: 'column',
        }}
      >
        <div
          style={{
            margin: '20px 0 0 0',
            display: 'flex',
            justifyContent: 'space-between',
          }}
        >
          <div style={{ opacity: 0.2 }}>
            <div>Versie: {STYLES_VERSION}</div>
            <a
              target="_blank"
              rel="noreferrer"
              href="https://gitlab.com/xxllnc/zaakgericht/zaken/start/-/tree/development/frontend-mono/apps/main/src/modules/customStyling"
            >
              Changelog
            </a>
          </div>
          <div style={{ display: 'flex' }}>
            <Tooltip title="Upload en bewerk van lokaal bestand">
              <IconButton
                sx={{ marginRight: '10px' }}
                // @ts-ignore
                onClick={() => document.querySelector('#archiveUpload').click()}
              >
                <Icon>{iconNames.edit}</Icon>
              </IconButton>
            </Tooltip>
            <Tooltip title="Reset to default">
              <IconButton
                sx={{ marginRight: '10px' }}
                onClick={() => {
                  document.querySelector('form')?.reset();

                  loadTemplate();
                }}
              >
                <Icon>{iconNames.cancel}</Icon>
              </IconButton>
            </Tooltip>
            <Tooltip title="Opslaan (downloaden)">
              <IconButton
                sx={{ marginRight: '10px' }}
                onClick={() => {
                  saveTemplate(
                    new FormData(document.querySelector('form') || undefined)
                  );
                }}
              >
                <Icon>{iconNames.save}</Icon>
              </IconButton>
            </Tooltip>
            <Tooltip title="Toevoegen aan development">
              <IconButton
                onClick={() => {
                  uploadTemplate(
                    new FormData(document.querySelector('form') || undefined)
                  );
                }}
              >
                <Icon>{iconNames.eye}</Icon>
              </IconButton>
            </Tooltip>
          </div>
        </div>
        <Typography style={{ margin: '20px 0 30px 0' }} variant="h2">
          Algemeen
        </Typography>
        <Field name="name" label="Template naam" />
        <Field name="siteImprovId" label="SiteImpov ID" />
        <Field name="documentTitle" label="Document Titel" />
        <Field name="mainAddress" label="Link" />
        <Typography style={{ margin: '40px 0 30px 0' }} variant="h2">
          Afbeeldingen
        </Typography>
        <Field
          type="file"
          name="favicon"
          label="Favicon"
          accept=".ico"
          tooltip="Als u niet zeker weet hoe u een favicon uit de website moet halen of hoe u deze van de klant kunt krijgen, vraag dan naar de ontwikkeling"
        />
        <Field
          type="file"
          name="logo-login"
          label="Logo login"
          accept=".png,.jpeg,.jpg,.svg"
          tooltip="Logo dat wordt weergegeven wanneer u probeert in te loggen Zaaksysteem as behereer"
        />
        <Field
          type="file"
          name="logo-pip"
          label="Logo PIP"
          accept=".png,.jpeg,.jpg,.svg"
          tooltip="Logo weergegeven bovenaan de pagina voor PIP en webform"
        />
        <Typography style={{ margin: '40px 0 30px 0' }} variant="h2">
          CSS
        </Typography>
        {cssProperties.map(field => (
          <Field key={field.name} {...field} />
        ))}
      </form>
    </>
  );
};

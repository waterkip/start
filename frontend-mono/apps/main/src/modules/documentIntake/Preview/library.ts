// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

const SCALE_FACTOR = 0.9;

export const getSize = (
  image: HTMLImageElement,
  ref?: { current: Element | null }
) => {
  if (ref && ref.current) {
    const rect = ref.current.getBoundingClientRect();
    const maxHeight = window.innerHeight - rect.y;
    const ratio = image.height / image.width;

    if (image.height > maxHeight) {
      const newHeight = maxHeight * SCALE_FACTOR;
      return {
        width: newHeight / ratio,
        height: newHeight,
      };
    }
  }

  return {
    width: image.width,
    height: image.height,
  };
};

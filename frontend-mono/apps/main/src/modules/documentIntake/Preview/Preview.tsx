// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useEffect, useState, useRef } from 'react';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import { usePreviewStyles } from './Preview.styles';
import { getSize } from './library';

type PreviewPropsType = {
  url: string;
};

const LOAD_DELAY = 1500;

const Preview: React.ComponentType<PreviewPropsType> = ({ url }) => {
  const [image, setImage] = useState(null as any);
  const classes = usePreviewStyles();
  const ref = useRef(null);
  let timeoutId: NodeJS.Timeout | null = null;
  useEffect(() => {
    timeoutId = setTimeout(async () => {
      const img = new Image();
      img.onload = () => setImage(img);
      img.src = url;
    }, LOAD_DELAY);

    return function () {
      if (timeoutId) {
        clearTimeout(timeoutId);
      }
    };
  }, []);

  return (
    <div className={classes.wrapper} ref={ref}>
      {image && ref ? (
        <div
          style={{
            backgroundImage: `url(${image.src})`,
            backgroundSize: 'cover',
            backgroundPosition: '50% 50%',
            width: getSize(image, ref).width,
            height: getSize(image, ref).height,
          }}
        />
      ) : (
        <div className={classes.loaderWrapper}>
          <Loader className={classes.loader} />
        </div>
      )}
    </div>
  );
};

export default Preview;

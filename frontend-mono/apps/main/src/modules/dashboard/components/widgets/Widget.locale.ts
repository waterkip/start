// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

const locale = {
  nl: {
    search: {
      placeholder: 'Filter de getoonde resultaten',
    },
    filter: 'Filter',
    removeFilters: 'Wis alle filters',
  },
};

export default locale;

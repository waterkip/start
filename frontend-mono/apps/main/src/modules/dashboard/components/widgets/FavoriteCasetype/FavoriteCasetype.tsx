// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { CaseTypeFinder } from '@zaaksysteem/common/src/components/form/fields/CaseTypeFinder/CaseTypeFinder';
import Button from '@mintlab/ui/App/Material/Button';
import CaseCreateDialog from '@mintlab/ui/App/Zaaksysteem/Layout/Topbar/Dialogs/CaseCreate/CaseCreateDialog';
import WidgetHeader from '../components/WidgetHeader';
import Widget from '../Widget';
import {
  addCasetypeToFavorites,
  FavCaseTypeType,
  fetchFavoriteCasetypes,
  removeCasetypeFromFavorites,
} from './FavoriteCasetype.library';

const FavoriteCasetype = ({
  onClose,
  height,
}: {
  onClose: () => {};
  height: number;
}) => {
  const [favoriteCasetypes, setFavoriteCasetypes] = React.useState<
    FavCaseTypeType[]
  >([]);
  const [activeCasetype, setActiveCasetype] = React.useState('');
  const [t] = useTranslation();

  React.useEffect(() => {
    fetchFavoriteCasetypes().then(setFavoriteCasetypes);
  }, []);

  return (
    <Widget
      header={
        <WidgetHeader
          title={t('dashboard:widget.favoriteCasetype')}
          onClose={onClose}
        />
      }
    >
      <div>
        <CaseCreateDialog
          open={Boolean(activeCasetype)}
          onClose={() => setActiveCasetype('')}
          savedCaseTypeUuid={activeCasetype}
        />
        <ul
          style={{
            padding: 15,
            marginTop: 0,
            height: height === 2 ? 190 : 350,
            overflow: 'scroll',
          }}
        >
          {favoriteCasetypes.map(casetype => (
            <li
              style={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'space-between',
                padding: '5px 0',
                borderBottom: '1px solid hsla(0,0%,63%,.2)',
              }}
              key={casetype.id}
            >
              <Button
                name="setActiveCaseType"
                onClick={() => setActiveCasetype(casetype.reference)}
              >
                {casetype.name}
              </Button>
              <Button
                onClick={() => {
                  removeCasetypeFromFavorites(casetype);
                  setFavoriteCasetypes(
                    favoriteCasetypes.filter(ins => casetype.id !== ins.id)
                  );
                }}
                icon="close"
                iconSize="small"
                name="removeCasetypeFromFavorites"
              />
            </li>
          ))}
        </ul>
        <CaseTypeFinder
          value={null}
          onChange={evt => {
            const val = evt.target.value?.data;

            if (
              val &&
              !favoriteCasetypes.find(casetype => casetype.reference === val.id)
            ) {
              addCasetypeToFavorites(val).then(() => {
                fetchFavoriteCasetypes().then(setFavoriteCasetypes);
              });
            }
          }}
          {...({} as any)}
        />
      </div>
    </Widget>
  );
};

export default FavoriteCasetype;

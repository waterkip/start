// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

const attributes = {
  title: 'Kenmerken',
  mode: {
    toggle: {
      showMagicStrings: 'Toon magicstrings',
      showTitles: 'Toon kenmerktitels',
    },
  },
  placeholder: 'Maak een groep aan om de fase in te richten.',
  group: {
    title: 'Groep',
    add: 'Groep toevoegen',
    edit: 'Groep bewerken',
  },
  attribute: {
    title: 'Kenmerk',
    add: 'Kenmerk toevoegen…',
    edit: 'Kenmerk bewerken',
    auths: {
      add: 'Afdeling en rol toevoegen',
      department: {
        placeholder: 'Kies een afdeling…',
      },
      role: {
        placeholder: 'Kies een rol…',
      },
    },
    dateLimit: {
      start: 'Start',
      end: 'Einde',
      pre: 'Voor',
      post: 'Na',
      currentDate: 'Huidige datum',
    },
    objectMapping: {
      attributeOfObject: 'Kenmerk van object',
      prefilledValue: 'Vooringevulde waarde',
    },
  },
  textblock: {
    title: 'Tekstblok',
    add: 'Tekstblok toevoegen',
    edit: 'Tekstblok bewerken',
  },
  labels: {
    permanentHidden: 'Permanent verborgen',
    referential: 'Referentieel',
    title: 'Titel',
    description: 'Toelichting (intern en extern)',
    descriptionInternal: 'Toelichting (intern)',
    mandatory: 'Verplicht',
    publishOnPip: 'Zichtbaar op PIP',
    editOnPip: 'Wijzigbaar op PIP',
    descriptionExternal: 'Toelichting (extern)',
    skipQueue: 'Wachtrij overslaan',
    caseAddress: 'Zaakadres',
    labelMultiple: 'Label voor de opplusknop',
    showOnMap: 'Zichtbaar op kaart',
    mapLayer: 'Kaartlaag',
    mapLayerAttribute: 'Kaartlaag kenmerk',
    dateLimits: 'Datumbeperking',
    authorizations: 'Specifieke behandelrechten',
    role: 'Rol',
    createObjectEnabled: 'Object aanmaken',
    createObjectLabel: 'Object aanmaken label',
    createObjectMapping: 'Object kenmerk mapping',
  },
  help: {
    permanentHidden:
      'Permanent verborgen kenmerken zijn niet zichtbaar in de zaak en niet zichtbaar op de formulieren. Deze kenmerken kunnen niet zichtbaar gemaakt worden door middel van regels.',
    referential:
      'Referentiële kenmerken tonen de waarde van hetzelfde kenmerk uit de hoofdzaak. Deze waarde is niet werkelijk aanwezig in de zaak en kan dus niet ingezet worden voor bijvoorbeeld sjablonen of regels. Documentkenmerken worden niet ondersteund.',
    title:
      'De kenmerktitel die wordt getoond op het registratie- en webformulier, in het zaakdossier, en op de PIP. Wanneer er geen titel ingesteld is wordt daar de kenmerknaam getoond.',
    description:
      'De toelichting die wordt getoond op het registratie- en webformulier, in het zaakdossier, en op de PIP.',
    descriptionInternal:
      'De toelichting die wordt getoond op het registratieformulier en in het zaakdossier.',
    mandatory:
      'Verplichte kenmerken blokkeren de voortgang van formulieren en faseovergangen van zaakdossiers wanneer ze geen waarde hebben.',
    publishOnPip:
      'Een {{elementType}} is enkel zichtbaar op de PIP wanneer deze gepubliceerd is.',
    editOnPip:
      'Kenmerken die gewijzigd kunnen worden op de PIP staan toe dat de aanvrager een wijzigingsvoorstel doen voor de kenmerkwaarde.',
    descriptionExternal:
      'De toelichting die wordt getoond op het webformulier en in de PIP.',
    skipQueue:
      "Wanneer in het zaaktype ingesteld is dat kenmerk- en documentwijzigingen van collega's in de wachtrij geplaatst worden kan met deze instellingen een uitzondering voor het kenmerk gemaakt worden.",
    labelMultiple: 'Label voor opplusknop',
    caseAddress:
      'Het adres dat in dit kenmerk wordt ingevoerd zal worden ingesteld als adres van de zaak. Gebruik deze instelling voor slechts één (zichtbaar) kenmerk per zaaktype.',
    role: 'De rol voor het geselecteerde contact.',
    showOnMap: '',
    mapLayer:
      'De geselecteerde kaartlaag zal standaard zichtbaar zijn op de kaart.',
    mapLayerAttribute:
      "Het kenmerk dat hier ingesteld is zal ingevuld worden met de waarde van de geselecteerde kaartlaag. Ondersteunde kenmerktypen zijn: 'Enkelvoudige keuze', 'Keuzelijst'.",
    dateLimits:
      'De beschikbare data kunnen beperkt worden door een begindatum en/of een einddatum te selecteren, relatief aan de huidige datum of een ander datumkenmerk dat zich eerder in de fase bevindt.',
    authorizations:
      'Het kenmerk kan enkel bewerkt worden door gebruikers in de opgegeven afdelingen/rollen.',
    createObjectEnabled:
      'Maakt het mogelijk om objecten aan te maken vanuit het zaakdossier.',
    createObjectLabel:
      'Het label dat gebruikt wordt voor de knop waarmee het aanmaken van objecten gestart kan worden.',
    createObjectMapping:
      'Standaard worden kenmerken van het object vooringevuld door waarden uit de zaak uit dezelfde kenmerken. Door een magicstring op te geven kan hiervoor verwezen worden naar een ander kenmerk (van hetzelfde kenmerktype). Ook kan een kenmerk vooringevuld worden met een standaard waarde door een waarde op te geven die niet een magicstring is.',
  },
  dialogs: {
    group: {
      remove: {
        title: 'Groep verwijderen',
        label: 'Wilt u de groep met of zonder de kenmerken verwijderen?',
        with: 'Inclusief de kenmerken',
        without: 'Exclusief de kenmerken',
      },
    },
  },
};

const results = {
  title: 'Resultaten',
  placeholder: 'Maak een resultaat aan.',
  add: 'Resultaat toevoegen',
  edit: 'Resultaat bewerken',
  labels: {
    name: 'Naam (omschrijving)',
    resultType: 'Resultaattype-generiek',
    isDefault: 'Standaard keuze',

    selectionList: 'Selectielijst',
    selectionListSourceDate: 'Selectielijst brondatum',
    selectionListEndDate: 'Selectielijst einddatum',
    selectionListNumber: 'Selectielijst-nummer',

    archivalTrigger: 'Activeer bewaartermijn',
    archivalNomination: 'Archiefnominatie (waardering)',
    retentionPeriod: 'Bewaartermijn',
    archivalNominationValuation: 'Brondatum archiefprocedure',

    comments: 'Toelichting',
    processtypeNumber: 'Procestype-nummer',
    processtypeName: 'Procestype-naam',
    processtypeDescription: 'Procestype-omschrijving',
    processtypeExplanation: 'Procestype-toelichting',
    processtypeObject: 'Procestype-object',
    processtypeGeneric: 'Procestype-generiek',
    origin: 'Herkomst',
    processPeriod: 'Procestermijn',
  },
};

const rules = {
  title: 'Regels',
};

const actions = {};

const tasks = {
  placeholder: {
    new: 'Voeg een nieuwe taak toe…',
    existing: 'Deze taak zal verwijderd worden',
  },
};

export default {
  phases: {
    attributes,
    results,
    rules,
    actions,
    tasks,
    toggleView: 'Weergave omschakelen',
    config: {
      configure: 'Fasen configureren',
      dialog: {
        title: 'Fasen configureren',
        description:
          'Maak nieuwe fasen aan, wijzig of verwijder bestaande fasen, of sleep fasen om deze te herordenen.',
        warning:
          'Wanneer fasen worden toegevoegd of versleept worden de volgende instellingen automatisch bijgewerkt:',
        warnings: {
          0: 'Resultaatmogelijkheden blijven altijd onderdeel van de laatste fase.',
          1: 'In de tussenfasen wordt de toewijzing gelijk gezet aan die van de voorliggende fase, tenzij al ingesteld.',
          2: "In de tussenfasen worden de zaaktypeacties van type 'Vervolgzaak' gewijzigd naar type 'Gerelateerde zaak'.",
          3: "In de laatste fase worden de naam en mijlpaal gewijzigd naar 'Afhandelen' en 'Afgehandeld'.",
          4: 'In de laatste fase wordt de toewijzing verwijderd.',
          5: "In de laatste fase worden de zaaktypeacties van type 'Deelzaak' gewijzigd naar 'Gerelateerde zaak'.",
        },
      },
      formDialog: {
        title: 'Fase {{verb}}',
        labels: {
          name: 'Naam',
          milestone: 'Naam mijlpaal',
          term: 'Termijn mijlpaal',
        },
      },
    },
    sidebar: {
      actions: {
        title: 'Acties',
      },
      tasks: {
        title: 'Taken',
      },
    },
  },
};

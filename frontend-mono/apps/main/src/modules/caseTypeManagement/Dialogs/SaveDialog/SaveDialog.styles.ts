// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useSaveDialogStyles = makeStyles(() => ({
  content: {
    display: 'flex',
    flexDirection: 'column',
    gap: 20,
  },
  changedTabs: {
    display: 'flex',
    flexDirection: 'column',
    gap: 10,
  },
  changedTab: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    gap: 5,
  },
}));

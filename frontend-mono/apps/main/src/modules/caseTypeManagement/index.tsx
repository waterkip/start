// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { Routes, Route, Navigate } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import Sheet from '@mintlab/ui/App/Zaaksysteem/Sheet/Sheet';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import TopbarTitle from '@mintlab/ui/App/Zaaksysteem/Layout/Topbar/TopbarTitle/TopbarTitle';
import { useAdminBanner } from '../../library/auth';
import CaseTypeManagement from './CaseTypeManagement';
import locale from './locale/caseTypeManagement.locale';
import { getBreadcrumbs } from './CaseTypeManagement.library';
import { useCaseTypeManagementStyles } from './CaseTypeManagement.styles';
import {
  useCaseTypeQuery,
  useHtmlEmailTemplatesQuery,
  useMapLayersQuery,
  useSubjectRolesQuery,
} from './CaseTypeManagement.requests';
import {
  CaseTypeManagementContext,
  useContext,
} from './CaseTypeManagement.context';

const CaseTypeManagementModule: React.ComponentType = () => {
  const [t] = useTranslation('caseTypeManagement');

  const { data: caseType } = useCaseTypeQuery();
  const { isLoading: loadingHtmlEmailTemplates } = useHtmlEmailTemplatesQuery();
  const { isLoading: loadingSubjectRoles } = useSubjectRolesQuery();
  const { isLoading: loadingMapLayers } = useMapLayersQuery();

  const context = useContext();

  const loading =
    !caseType ||
    loadingHtmlEmailTemplates ||
    loadingSubjectRoles ||
    loadingMapLayers;

  return loading ? (
    <Loader />
  ) : (
    <CaseTypeManagementContext.Provider value={context}>
      <CaseTypeManagement caseType={caseType} />
      <TopbarTitle breadcrumbs={getBreadcrumbs(t as any, caseType)} />
    </CaseTypeManagementContext.Provider>
  );
};

export default () => {
  const classes = useCaseTypeManagementStyles();
  const banner = useAdminBanner.catalog();

  return (
    <I18nResourceBundle resource={locale} namespace="caseTypeManagement">
      {banner || (
        <Sheet classes={{ sheet: classes.sheet }}>
          <Routes>
            <Route path=":uuid/*">
              <Route
                path=""
                element={<Navigate to="common" replace={true} />}
              />
              <Route path=":tab/*" element={<CaseTypeManagementModule />} />
            </Route>
          </Routes>
        </Sheet>
      )}
    </I18nResourceBundle>
  );
};

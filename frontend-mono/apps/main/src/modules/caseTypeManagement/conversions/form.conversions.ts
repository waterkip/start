// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { allowedRequestorTypes } from '../Content/FormTab/FormTab.choices';
import {
  ConvertAllowedRequestorTypesType,
  ConvertCheckboxListType,
  ConvertDefaultDocumentFoldersType,
  ConvertLeadTimeType,
  ConvertPresetAssigneeType,
  ConvertPresetRequestorType,
  ListAsArrayType,
  RevertCheckboxListType,
  RevertDefaultDocumentFoldersType,
  RevertLeadTimeType,
  RevertPresetAssigneeType,
  RevertPresetRequestorType,
  RevertRequestorTypesType,
} from '../Content/FormTab/FormTab.types';

export const leadTimeDefault = {
  legal_period: {
    type: 'kalenderdagen' as 'kalenderdagen',
    value: '1',
  },
  service_period: {
    type: 'kalenderdagen' as 'kalenderdagen',
    value: '1',
  },
};

/* eslint complexity: [2, 8] */
export const convertLeadTime: ConvertLeadTimeType = (legal, service) =>
  legal?.type === 'einddatum'
    ? {
        leadTimeType: 'set',
        leadTimeLegal: { amount: '', type: 'kalenderdagen' },
        leadTimeService: { amount: '', type: 'kalenderdagen' },
        leadTimeLegalDate: legal?.value || '',
        leadTimeServiceDate: service?.value || '',
      }
    : {
        leadTimeType: 'term',
        leadTimeLegal: {
          amount: legal?.value || leadTimeDefault.legal_period.value,
          type: legal?.type || leadTimeDefault.legal_period.type,
        },
        leadTimeService: {
          amount: service?.value || leadTimeDefault.service_period.value,
          type: service?.type || leadTimeDefault.service_period.type,
        },
        leadTimeLegalDate: '',
        leadTimeServiceDate: '',
      };

export const revertLeadTime: RevertLeadTimeType = ({
  common: {
    leadTimeType,
    leadTimeLegal,
    leadTimeService,
    leadTimeLegalDate,
    leadTimeServiceDate,
  },
}) =>
  leadTimeType === 'term'
    ? {
        legal_period: {
          type: leadTimeLegal.type,
          value: leadTimeLegal.amount,
        },
        service_period: {
          type: leadTimeService.type,
          value: leadTimeService.amount,
        },
      }
    : {
        legal_period: {
          type: 'einddatum',
          value: leadTimeLegalDate,
        },
        service_period: {
          type: 'einddatum',
          value: leadTimeServiceDate,
        },
      };

// filter out unused 'preset_client' value and
// sort into the right order
// to ensure a change and undone change is not marked as dirty
export const convertAllowedRequestorTypes: ConvertAllowedRequestorTypesType =
  requestorTypes =>
    allowedRequestorTypes.filter(requestorType =>
      (requestorTypes || []).includes(requestorType)
    );

export const revertRequestorTypes: RevertRequestorTypesType = ({
  relations: { allowedRequestorTypes, presetRequestor },
}) => [...allowedRequestorTypes, ...(presetRequestor ? ['preset_client'] : [])];

export const convertPresetRequestor: ConvertPresetRequestorType = contact => {
  if (!contact) return null;

  const { uuid: value, type, name: label } = contact;

  return { value, type, label };
};

export const convertPresetAssignee: ConvertPresetAssigneeType = contact => {
  if (!contact) return null;

  const { uuid: value, type, name: label } = contact;

  return { value, type, label };
};

export const revertPresetRequestor: RevertPresetRequestorType = contact => {
  if (!contact) return;

  const { value: uuid, type, label: name } = contact;

  return { uuid, type, name };
};

export const revertPresetAssignee: RevertPresetAssigneeType = contact => {
  if (!contact) return;

  const { value: uuid, type, label: name } = contact;

  return { uuid, type, name };
};

export const convertCheckboxList: ConvertCheckboxListType = listAsObject => {
  if (!listAsObject) return [];

  return Object.entries(listAsObject).reduce(
    (acc, [key, value]) => (value ? [...acc, key] : acc),
    [] as ListAsArrayType
  );
};

export const revertCheckboxList: RevertCheckboxListType = (
  possibleValues,
  listAsArray
) => {
  return possibleValues.reduce(
    (acc: any, item: string) => ({
      ...acc,
      [item]: listAsArray.includes(item),
    }),
    {}
  );
};

export const convertDefaultDocumentFolders: ConvertDefaultDocumentFoldersType =
  defaultDocumentFolders => {
    if (!defaultDocumentFolders) return [];

    return defaultDocumentFolders.map(value => ({ value, label: value }));
  };

export const revertDefaultDocumentFolders: RevertDefaultDocumentFoldersType =
  defaultDocumentFolders => defaultDocumentFolders.map(({ value }) => value);

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useEffect, useState } from 'react';
import { openServerError } from '@zaaksysteem/common/src/signals';
import {
  getCaseType,
  saveCaseType,
} from '../../../CaseTypeManagement.requests';
import { ChildType } from '../ChildrenTab.types';
import { revertCaseTypeToApi } from '../../../conversions/caseTypeManagement.conversions';
import {
  GetValuesToSaveType,
  PublishChildrenType,
  UseVerifyChildrenType,
  VerifyChildrenType,
} from './SaveDialog.types';

const verifyChildren: VerifyChildrenType = async (
  verifiedChildren,
  setVerifiedChildren
) => {
  let newVerifiedChildren = verifiedChildren;

  for (const childToPublish of verifiedChildren) {
    const biggestPhaseToCopy = Object.values(childToPublish.phaseSections)
      .map(section => Number(section))
      .filter(section => !isNaN(section))
      .reverse()[0];

    const childUuid = childToPublish.caseType?.value as string;
    const childCaseType = await getCaseType(childUuid).catch(openServerError);

    if (!childCaseType) return;

    const childPhases = childCaseType.phases;
    const biggestMiddlePhase = childPhases.length - 1;

    const publishable =
      !biggestPhaseToCopy || biggestPhaseToCopy <= biggestMiddlePhase;

    newVerifiedChildren = newVerifiedChildren.map(child =>
      child.uuid === childToPublish.uuid
        ? {
            ...child,
            verified: true,
            publishable,
            childCaseType,
          }
        : child
    );

    setVerifiedChildren(newVerifiedChildren);
  }
};

const createUnverifiedChild = (child: ChildType) => ({
  ...child,
  verified: false,
  publishable: false,
});

export const useVerifyChildren: UseVerifyChildrenType = childrenToPublish => {
  const unverifiedChildren = childrenToPublish.map(createUnverifiedChild);
  const [verifiedChildren, setVerifiedChildren] = useState(unverifiedChildren);

  useEffect(() => verifyChildren(unverifiedChildren, setVerifiedChildren), []);

  return verifiedChildren;
};

const getValuesToSave: GetValuesToSaveType = (
  baseSections,
  phaseSections,
  caseType,
  childCaseType
) => {
  const motherCaseType = baseSections.reduce(
    (acc, section) => ({ ...acc, [section]: caseType[section] }),
    {}
  );

  const newChild = {
    ...childCaseType,
    ...motherCaseType,
  };

  return revertCaseTypeToApi(newChild);
};

export const publishChildren: PublishChildrenType = async (
  t,
  children,
  caseType
) => {
  const promises = children
    .map(async child => {
      const { childCaseType, baseSections, phaseSections } = child;

      if (!childCaseType) return null;

      const data = getValuesToSave(
        baseSections,
        phaseSections,
        caseType,
        childCaseType
      );

      if (!data) return null;

      await saveCaseType({
        ...data,
        change_log: {
          update_components: baseSections.map(section => t(`tabs.${section}`)),
          update_description: t('children.save.inherited', {
            motherName: caseType.common.name,
          }),
        },
      });
    })
    .filter(Boolean);

  await Promise.all(promises);
};

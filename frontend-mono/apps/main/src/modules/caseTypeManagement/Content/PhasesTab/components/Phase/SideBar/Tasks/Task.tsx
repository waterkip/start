// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTheme } from '@mui/material';
import { useTranslation } from 'react-i18next';
import { useDebouncedCallback } from 'use-debounce';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import IconButton from '@mui/material/IconButton';
import { TextField } from '@mintlab/ui/App/Material/TextField';
import { TaskType } from '../../../../PhasesTab.types';
import { useTasksStyles } from './Tasks.styles';

type TaskPropsType = {
  task: TaskType;
  onEdit: (task: TaskType) => void;
  onDelete: (task: TaskType) => void;
  provided: any;
};

const Task: React.ComponentType<TaskPropsType> = ({
  task,
  onEdit,
  onDelete,
  provided,
}) => {
  const [t] = useTranslation('caseTypeManagement');
  const classes = useTasksStyles();

  const [setValueDebounced] = useDebouncedCallback(onEdit, 300);
  const [value, setValue] = useState(task.value);

  const { mintlab } = useTheme<Theme>();
  const sxButton = {
    backgroundColor: mintlab.greyscale.dark,
    '&:hover': { backgroundColor: mintlab.greyscale.darker },
  };
  const sxTextField = {
    '& .MuiInputBase-input': {
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      padding: '10px 15px 8px 15px',
    },
  };

  return (
    <div
      id={task.id}
      className={classes.task}
      ref={provided.innerRef}
      {...provided.draggableProps}
    >
      <div {...provided.dragHandleProps} className={classes.handle}>
        <Icon>{iconNames.drag_indicator}</Icon>
      </div>
      <div className={classes.text}>
        <TextField
          value={value}
          placeholder={t('phases.tasks.placeholder.existing')}
          onChange={(event: any) => {
            setValue(event.target.value);
            setValueDebounced({ ...task, value: event.target.value });
          }}
          sx={sxTextField}
        />
      </div>
      <div className={classes.remove}>
        <IconButton
          color="inherit"
          onClick={() => onDelete(task)}
          sx={sxButton}
        >
          <Icon size="extraSmall">{iconNames.close}</Icon>
        </IconButton>
      </div>
    </div>
  );
};

export default Task;

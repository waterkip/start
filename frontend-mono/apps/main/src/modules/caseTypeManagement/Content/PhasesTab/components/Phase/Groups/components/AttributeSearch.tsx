// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import { APICaseManagement } from '@zaaksysteem/generated';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { useTranslation } from 'react-i18next';
import {
  PhaseType,
  GroupType,
  AttributeTypeType,
  AttributeType,
} from '../../../../PhasesTab.types';

type RequestParamsType = APICaseManagement.SearchAttributeRequestParams;
type ResponseBodyType = APICaseManagement.SearchAttributeResponseBody;

const fetchChoices = async (keyword: string) => {
  const url = '/api/v2/cm/attribute/search';
  const params = { 'filter[keyword]': keyword, page: 1, page_size: 100 };
  const fullUrl = buildUrl<RequestParamsType>(url, params);

  return await request<ResponseBodyType>('GET', fullUrl);
};

const formatChoices = (rawChoices: ResponseBodyType['data']) =>
  rawChoices.map(({ id, attributes: { type, label, magic_string } }) => ({
    label,
    value: magic_string,
    uuid: id,
    type: type as AttributeTypeType,
  }));

export type AttributeChoiceType = {
  label: string;
  value: string;
  uuid: string;
  type: AttributeTypeType;
};

type AttributeSearchPropsType = {
  phase: PhaseType;
  group: GroupType;
  addAttribute: (
    group: GroupType,
    attributeChoice: AttributeChoiceType
  ) => void;
};

const AttributeSearch: React.ComponentType<AttributeSearchPropsType> = ({
  phase,
  group,
  addAttribute,
}) => {
  const [t] = useTranslation('caseTypeManagement');
  const [choices, setChoices] = useState<AttributeChoiceType[]>([]);

  const magicStrings = phase.groups.reduce((acc, group) => {
    const attributes = group.fields.filter(
      field => field.type !== 'textblock'
    ) as AttributeType[];
    const magicStrings = attributes.map(field => field.magicString);

    return [...acc, ...magicStrings];
  }, [] as string[]);

  return (
    <Select
      placeholder={t('phases.attributes.attribute.add')}
      choices={choices}
      freeSolo={true}
      isClearable={false}
      onChange={event => {
        const fieldChoice = event.target.value as AttributeChoiceType;

        addAttribute(group, fieldChoice);
        setChoices(choices.filter(choice => choice.uuid !== fieldChoice.uuid));
      }}
      onInputChange={async event => {
        // @ts-ignore
        const keyword = event.target.value;

        if (!keyword) return;

        const response = await fetchChoices(keyword);
        const choices = formatChoices(response.data);
        const filteredChoices = choices.filter(
          choice => !magicStrings.includes(choice.value)
        );

        setChoices(filteredChoices);
      }}
      sx={{ '&& .MuiInputBase-input': { padding: 0.3 } }}
    />
  );
};

export default AttributeSearch;

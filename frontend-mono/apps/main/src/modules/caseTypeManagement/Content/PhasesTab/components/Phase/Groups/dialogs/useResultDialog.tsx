// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import FormDialog from '@zaaksysteem/common/src/components/dialogs/FormDialog/FormDialog';
import { ResultType, UpdateResultsType } from '../../../../PhasesTab.types';
import { addResult, editResult } from '../Groups.library';
import { getResultFormDefinition } from './definitions';

type StateType = {
  type: 'add' | 'edit';
  result?: ResultType;
} | null;

export type OpenResultDialogType = (state: StateType) => void;

/* eslint complexity: [2, 9] */
export const useResultDialog = (
  results: ResultType[],
  updateResults: UpdateResultsType
) => {
  const [t] = useTranslation('caseTypeManagement');

  const [resultState, setResultState] = useState<StateType>(null);

  const onClose = () => setResultState(null);

  if (!resultState)
    return {
      resultDialog: null,
      openResultDialog: setResultState,
    };

  const { type, result } = resultState;
  const formValues = type === 'edit' ? result : undefined;

  const formDefinition = getResultFormDefinition(t as any, formValues);
  const onSubmit = (values: any) => {
    if (type === 'edit' && result) {
      editResult(values, results, updateResults, result);
    } else {
      addResult(values, results, updateResults);
    }
    onClose();
  };

  const resultDialog = (
    <FormDialog
      title={t(`phases.results.${type}`)}
      icon={type}
      open={true}
      onClose={onClose}
      formDefinition={formDefinition}
      onSubmit={onSubmit}
      compact={false}
    />
  );

  return {
    resultDialog,
    openResultDialog: setResultState,
  };
};

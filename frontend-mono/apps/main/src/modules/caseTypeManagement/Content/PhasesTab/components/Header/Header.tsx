// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useContext, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { Box, IconButton } from '@mui/material';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { useTranslation } from 'react-i18next';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import { TopMenu } from '@mintlab/ui/App/Zaaksysteem/TopMenu';
import { PhaseType, UpdatePhasesType } from '../../PhasesTab.types';
import { rotate } from '../../../../CaseTypeManagement.styles';
import { CaseTypeManagementContext } from '../../../../CaseTypeManagement.context';
import PhaseConfigDialog from './PhaseConfigDialog/PhaseConfigDialog';
import { useHeaderStyles } from './Header.styles';

type HeaderPropsType = {
  phases: PhaseType[];
  updatePhases: UpdatePhasesType;
  phaseNumber: string;
};

const Header: React.ComponentType<HeaderPropsType> = ({
  phases,
  updatePhases,
  phaseNumber,
}) => {
  const classes = useHeaderStyles();
  const [t] = useTranslation('caseTypeManagement');

  const navigate = useNavigate();

  const {
    isCompact,
    allowSideBySide,
    phasesStackedMode,
    setPhasesStackedMode,
    setDefaultPhase,
  } = useContext(CaseTypeManagementContext);

  const [configuring, setConfiguring] = useState<boolean>(false);

  const isMenuCompact = phases.length > 2 && isCompact;
  const navigationItems = phases.map((phase, index) => ({
    label: isMenuCompact ? `${index + 1}` : phase.name,
    selected: Number(phaseNumber) - 1 === index,
    onClick: () => {
      const phaseNumber = `${phase.number}`;

      setDefaultPhase(phaseNumber);
      navigate(`${phaseNumber}`);
    },
  }));

  return (
    <div className={classes.wrapper}>
      <TopMenu items={navigationItems} />
      <div className={classes.button}>
        <Tooltip title={t('phases.config.configure')}>
          <IconButton onClick={() => setConfiguring(true)}>
            <Icon size="small">{iconNames.settings}</Icon>
          </IconButton>
        </Tooltip>
      </div>
      {allowSideBySide && (
        <div className={classes.button}>
          <Tooltip title={t('phases.toggleView')}>
            <IconButton
              onClick={() => setPhasesStackedMode(!phasesStackedMode)}
            >
              <Box sx={phasesStackedMode ? rotate : {}}>
                <Icon size="small">{iconNames.view_agenda}</Icon>
              </Box>
            </IconButton>
          </Tooltip>
        </div>
      )}

      {configuring && (
        <PhaseConfigDialog
          phaseNumber={phaseNumber}
          phases={phases}
          updatePhases={updatePhases}
          open={configuring}
          onClose={() => setConfiguring(false)}
        />
      )}
    </div>
  );
};

export default Header;

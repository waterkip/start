// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { IconButton } from '@mui/material';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { OpenGroupDialogType } from '../dialogs/useGroupDialog';
import { useGroupsStyles } from '../Groups.styles';
import { OpenResultDialogType } from '../dialogs/useResultDialog';

type PlaceholderPropsType = {
  type: 'group' | 'result';
  openDialog: OpenGroupDialogType | OpenResultDialogType;
};

const Placeholder: React.ComponentType<PlaceholderPropsType> = ({
  type,
  openDialog,
}) => {
  const [t] = useTranslation('caseTypeManagement');
  const classes = useGroupsStyles();

  const title =
    type === 'group'
      ? t('phases.attributes.placeholder')
      : t('phases.results.placeholder');

  const tooltip =
    type === 'group'
      ? t('phases.attributes.group.add')
      : t('phases.results.add');

  const icon = type === 'group' ? iconNames.add_card : iconNames.add_task;

  return (
    <div className={classes.placeholder}>
      <div>{title}</div>
      <div className={classes.addButtons}>
        <IconButton
          onClick={() => openDialog({ type: 'add' })}
          sx={{ padding: 0.95 }}
        >
          <Tooltip title={tooltip}>
            <Icon size="extraSmall" color="inherit">
              {icon}
            </Icon>
          </Tooltip>
        </IconButton>
      </div>
    </div>
  );
};

export default Placeholder;

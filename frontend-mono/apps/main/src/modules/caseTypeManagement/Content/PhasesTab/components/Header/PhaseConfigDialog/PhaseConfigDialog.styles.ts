// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const usePhaseConfigDialogStyles = makeStyles(
  ({ mintlab: { greyscale } }: Theme) => ({
    wrapper: {
      display: 'flex',
      flexDirection: 'column',
      gap: 20,
      width: 550,
    },
    droppable: {
      display: 'flex',
      flexDirection: 'column',
    },
    description: {
      display: 'flex',
    },
    warningList: {
      margin: 0,
      marginLeft: 10,
      padding: 10,
    },
    add: {
      width: 100,
      color: greyscale.darker,
    },
    icon: {
      color: greyscale.darkest,
    },
  })
);

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { IconButton } from '@mui/material';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { useGroupsStyles } from '../Groups.styles';

type ActionButtonsPropsType = {
  edit: () => void;
  remove?: () => void;
};

/* eslint complexity: [2, 8] */
const ActionButtons: React.ComponentType<ActionButtonsPropsType> = ({
  edit,
  remove,
}) => {
  const classes = useGroupsStyles();

  return (
    <div className={classes.buttons}>
      <IconButton onClick={edit} sx={{ padding: 0.95 }}>
        <Icon size="extraSmall" color="inherit">
          {iconNames.edit}
        </Icon>
      </IconButton>
      {remove && (
        <IconButton onClick={remove} sx={{ padding: 0.95 }}>
          <Icon size="extraSmall" color="inherit">
            {iconNames.close}
          </Icon>
        </IconButton>
      )}
    </div>
  );
};

export default ActionButtons;

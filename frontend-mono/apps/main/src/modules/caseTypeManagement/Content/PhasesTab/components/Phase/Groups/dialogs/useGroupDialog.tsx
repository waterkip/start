// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import FormDialog from '@zaaksysteem/common/src/components/dialogs/FormDialog/FormDialog';
import {
  GroupType,
  PhaseType,
  UpdatePhaseType,
} from '../../../../PhasesTab.types';
import { addGroup, editGroup } from '../Groups.library';
import { getGroupFormDefinition } from './definitions';

type StateType = {
  type: 'add' | 'edit';
  group?: GroupType;
} | null;

export type OpenGroupDialogType = (state: StateType) => void;

/* eslint complexity: [2, 9] */
export const useGroupDialog = (
  phase: PhaseType,
  updatePhase: UpdatePhaseType
) => {
  const [t] = useTranslation('caseTypeManagement');

  const [groupState, setGroupState] = useState<StateType>(null);

  const onClose = () => setGroupState(null);

  if (!groupState)
    return {
      groupDialog: null,
      openGroupDialog: setGroupState,
    };

  const { type, group } = groupState;
  const formValues = type === 'edit' ? group : undefined;

  const formDefinition = getGroupFormDefinition(t as any, formValues);
  const onSubmit = (values: any) => {
    if (type === 'edit' && group) {
      editGroup(values, phase, updatePhase, group);
    } else {
      addGroup(values, phase, updatePhase, group);
    }
    onClose();
  };

  const groupDialog = (
    <FormDialog
      title={t(`phases.attributes.group.${type}`)}
      icon={type}
      open={true}
      onClose={onClose}
      formDefinition={formDefinition}
      onSubmit={onSubmit}
    />
  );

  return {
    groupDialog,
    openGroupDialog: setGroupState,
  };
};

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import {
  CHECKBOX,
  NUMERIC,
  SELECT,
} from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { AnyFormDefinitionField } from '@zaaksysteem/common/src/components/form/types';

const translations = 'phases.attributes.attribute.dateLimit';
const terms = ['days', 'weeks', 'months', 'years'];
const durings = ['pre', 'post'];
const defaultAttributeChoice = 'currentDate';

type GetFormDefinitionType = (
  t: i18next.TFunction,
  namespace: string,
  dateLimit: any,
  dateAttributeChoices: { label: string; value: string }[]
) => AnyFormDefinitionField[];

export const getFormDefinition: GetFormDefinitionType = (
  t,
  namespace,
  dateLimit,
  dateAttributeChoices
) => {
  const termChoices = terms.map(value => ({
    label: t(`common:terms.${value}`),
    value,
  }));
  const duringChoices = durings.map(value => ({
    label: t(`${translations}.${value}`),
    value,
  }));
  const attributeChoices = [
    {
      label: t(`${translations}.currentDate`),
      value: defaultAttributeChoice,
    },
    ...dateAttributeChoices,
  ];

  return [
    {
      name: 'active',
      type: CHECKBOX,
      label: t(`${translations}.${namespace}`),
      value: dateLimit?.active || false,
    },
    {
      name: 'value',
      type: NUMERIC,
      value: dateLimit?.value || '1',
    },
    {
      name: 'term',
      type: SELECT,
      choices: termChoices,
      value: dateLimit?.term || terms[0],
      nestedValue: true,
      isClearable: false,
    },
    {
      name: 'during',
      type: SELECT,
      choices: duringChoices,
      value: dateLimit?.during || durings[0],
      nestedValue: true,
      isClearable: false,
    },
    {
      name: 'reference',
      type: SELECT,
      choices: attributeChoices,
      value: dateLimit?.reference || attributeChoices[0].value,
      nestedValue: true,
      isClearable: false,
    },
  ];
};

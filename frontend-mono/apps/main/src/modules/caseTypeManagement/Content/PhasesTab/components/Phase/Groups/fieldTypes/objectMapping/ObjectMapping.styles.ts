// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useObjectMappingStyles = makeStyles(({ typography }: Theme) => ({
  wrapper: {
    display: 'flex',
    flexDirection: 'column',
    gap: 10,
  },
  header: {
    display: 'flex',
    flexDirection: 'row',
    gap: 10,
    paddingTop: 12,
  },
  headerItem: {
    flexGrow: 1,
    padding: '0 10px',
    fontWeight: typography.fontWeightBold,
  },
  mapItem: {
    display: 'flex',
    alignItems: 'center',
    gap: 10,
  },
  fields: {
    display: 'flex',
    flexDirection: 'row',
    gap: 10,
    flex: 1,
    height: 42,
  },
}));

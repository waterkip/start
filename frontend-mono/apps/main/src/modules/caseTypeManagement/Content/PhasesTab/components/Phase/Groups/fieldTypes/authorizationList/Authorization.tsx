// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import Button from '@mintlab/ui/App/Material/Button';
import { useSubForm } from '@zaaksysteem/common/src/components/form/hooks/useSubForm';
import { FormFieldPropsType } from '@zaaksysteem/common/src/components/form/types/fieldComponent.types';
import { AttributeAuthorizationsType as AuthType } from '../../../../../PhasesTab.types';
import { getFormDefinition, getRules } from './getFormDefinition';
import { useAuthorizationListStyles } from './AuthorizationList.styles';

const AuthorizationForm: React.ComponentType<
  FormFieldPropsType<any, any, AuthType> & {
    remove: () => void;
  }
> = ({ name, remove, value, ...props }) => {
  const [t] = useTranslation('caseTypeManagement');
  const classes = useAuthorizationListStyles();

  const formDefinition = getFormDefinition(t as any);
  const rules = getRules();

  const { fields } = useSubForm({
    namespace: name,
    formDefinition,
    rules,
    ...props,
  });

  return (
    <div className={classes.authItem}>
      <div className={classes.fields}>
        {fields.map(({ FieldComponent, key, mode, ...fieldProps }) => (
          <FormControlWrapper {...fieldProps} key={key}>
            <FieldComponent {...fieldProps} disabled={fieldProps.disabled} />
          </FormControlWrapper>
        ))}
      </div>
      <Button
        icon="close"
        iconSize="small"
        action={remove}
        name="removeAuthorization"
        disabled={props.disabled}
      />
    </div>
  );
};

export default AuthorizationForm;

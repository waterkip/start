// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { GetAnyFormDefinitionType } from '../FormTab.types';

export const getRegistrationFormFormDefinition: GetAnyFormDefinitionType =
  t => {
    return [
      {
        name: 'registrationForm',
        type: fieldTypes.TEXT,
        isLabel: true,
      },
      {
        name: 'allowAssigningToSelf',
        type: fieldTypes.CHECKBOX,
      },
      {
        name: 'allowAssigning',
        type: fieldTypes.CHECKBOX,
      },
      {
        name: 'showConfidentiality',
        type: fieldTypes.CHECKBOX,
      },
      {
        name: 'showContactDetails',
        type: fieldTypes.CHECKBOX,
      },
      {
        name: 'allowAddRelations',
        type: fieldTypes.CHECKBOX,
      },
    ];
  };

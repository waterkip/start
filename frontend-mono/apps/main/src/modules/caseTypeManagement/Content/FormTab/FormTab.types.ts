// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { AnyFormDefinitionField } from '@zaaksysteem/common/src/components/form/types';
import * as i18next from 'i18next';
import {
  ChoiceType,
  HtmlEmailTemplateType,
  FormTabType,
  CaseTypeResponseBodyType,
  CaseTypeType,
  RawPresetAssigneeType,
  PresetAssigneeType,
  RawPresetRequestorType,
  PresetRequestorType,
} from '../../CaseTypeManagement.types';

type AttributesType = CaseTypeResponseBodyType['data']['attributes'];
type CommonType = AttributesType['general_attributes'];
type DocumentationType = AttributesType['documentation'];
type RelationsType = AttributesType['relations'];
type CaseDossierType = AttributesType['case_dossier'];

export type CreateChoiceType = (
  value: string,
  t?: i18next.TFunction,
  fieldName?: string
) => ChoiceType;

export type CreateChoicesType = (
  values: string[],
  t?: i18next.TFunction,
  fieldName?: string
) => ChoiceType[];

export type FormDefinitionMapType = {
  [key in FormTabType]: GetFormDefinitionType;
};

export type RulesMapType = {
  [key in FormTabType]: any;
};

export type FormDefinitionsMapType = {
  [key in FormTabType]: GetAnyFormDefinitionType;
};

export type GetAnyFormDefinitionType = (
  t: i18next.TFunction,
  htmlEmailTemplates: HtmlEmailTemplateType[]
) => any[];

export type GetFormDefinitionType = (
  tab: FormTabType,
  t: i18next.TFunction,
  htmlEmailTemplates: HtmlEmailTemplateType[],
  caseType: CaseTypeType
) => AnyFormDefinitionField[];

export type GetRulesType = (
  tab: FormTabType,
  values: CaseTypeType,
  formDefinition: AnyFormDefinitionField[]
) => any;

export type GenericGetRulesType = (
  values: CaseTypeType,
  formDefinition: AnyFormDefinitionField[]
) => any;

export type GetChoicesType = (t: i18next.TFunction) => ChoiceType[];

export type ProcessingTypeValuesType = NonNullable<
  NonNullable<DocumentationType['gdpr']>['processing_type']
>[];

export type ProcessingLegalValuesType = NonNullable<
  NonNullable<DocumentationType['gdpr']>['processing_legal']
>[];

export type InitiatorTypeValuesType = NonNullable<
  DocumentationType['initiator_type']
>[];

export type DesignationOfConfidentialityValuesType = NonNullable<
  DocumentationType['designation_of_confidentiality']
>[];

export type LeadTimeTypeValueType = 'term' | 'set';
export type LeadTimeValueType = NonNullable<
  NonNullable<CommonType['legal_period']>['type']
>;
export type LeadTimeType = { amount: string; type: LeadTimeValueType };

export type ConvertLeadTimeType = (
  legal: CommonType['legal_period'],
  service: CommonType['service_period']
) => Pick<
  CaseTypeType['common'],
  | 'leadTimeType'
  | 'leadTimeLegal'
  | 'leadTimeService'
  | 'leadTimeLegalDate'
  | 'leadTimeServiceDate'
>;

export type RevertLeadTimeType = (caseType: CaseTypeType) => {
  legal_period: CommonType['legal_period'];
  service_period: CommonType['service_period'];
};

export type AllowedRequestorTypesType =
  | 'natuurlijk_persoon_na'
  | 'natuurlijk_persoon'
  | 'niet_natuurlijk_persoon'
  | 'medewerker';

type RawAllowedRequestorTypesType = RelationsType['allowed_requestor_types'];

export type ConvertAllowedRequestorTypesType = (
  requestorTypes: RawAllowedRequestorTypesType
) => AllowedRequestorTypesType[];

export type RevertRequestorTypesType = (
  caseType: CaseTypeType
) => NonNullable<RawAllowedRequestorTypesType>;

type ListAsObjectType = {
  [key: string]: boolean;
};

export type ListAsArrayType = string[];

export type ConvertCheckboxListType = (
  listAsObject?: ListAsObjectType
) => ListAsArrayType;

export type RevertCheckboxListType = (
  possibleValues: string[],
  listAsArray: ListAsArrayType
) => ListAsObjectType;

export type ConvertDefaultDocumentFoldersType = (
  defaultDocumentFolders: CaseDossierType['default_document_folders']
) => ChoiceType[];

export type RevertDefaultDocumentFoldersType = (
  defaultDocumentFolders: ChoiceType[]
) => NonNullable<CaseDossierType['default_document_folders']>;

export type ConvertPresetAssigneeType = (
  contact: RawPresetAssigneeType
) => PresetAssigneeType;

export type RevertPresetAssigneeType = (
  contact: PresetAssigneeType
) => RawPresetAssigneeType;

export type ConvertPresetRequestorType = (
  contact: RawPresetRequestorType
) => PresetRequestorType;

export type RevertPresetRequestorType = (
  contact: PresetRequestorType
) => RawPresetRequestorType;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { getApiFormDefinition } from './definitions/api';
import { getCaseFormDefinition } from './definitions/case';
import { getCommonFormDefinition, getCommonRules } from './definitions/common';
import { getDocumentationFormDefinition } from './definitions/documentation';
import {
  getWebformFormDefinition,
  getWebformRules,
} from './definitions/webform';
import { getRegistrationFormFormDefinition } from './definitions/registrationform';
import { getRelationsFormDefinition } from './definitions/relations';
import {
  GetFormDefinitionType,
  GetRulesType,
  RulesMapType,
  FormDefinitionsMapType,
} from './FormTab.types';

export const formDefinitionsMap: FormDefinitionsMapType = {
  common: getCommonFormDefinition,
  documentation: getDocumentationFormDefinition,
  relations: getRelationsFormDefinition,
  webform: getWebformFormDefinition,
  registrationform: getRegistrationFormFormDefinition,
  case_dossier: getCaseFormDefinition,
  api: getApiFormDefinition,
};

export const getDefinition: GetFormDefinitionType = (
  tab,
  t,
  htmlEmailTemplates,
  caseType
) => {
  const formDefinitionGetter = formDefinitionsMap[tab];

  return formDefinitionGetter(t, htmlEmailTemplates).map(field => {
    if (field.isLabel)
      return {
        ...field,
        title: t(`form.headers.${field.name}.title`),
        description: t(`form.headers.${field.name}.description`),
      };

    const translations = t('form.fields', { returnObjects: true });
    const translation = translations[field.name];
    // @ts-ignore
    const { label, tooltip, placeholder } = translation;

    // @ts-ignore
    const value = caseType[tab][field.name];

    return {
      ...field,
      label,
      help: tooltip,
      placeholder,
      value,
    };
  });
};

const rulesMap: RulesMapType = {
  common: getCommonRules,
  documentation: () => [],
  relations: () => [],
  webform: getWebformRules,
  registrationform: () => [],
  case_dossier: () => [],
  api: () => [],
};

export const getRules: GetRulesType = (tab, caseType, formDefinition) => {
  return rulesMap[tab](caseType, formDefinition);
};

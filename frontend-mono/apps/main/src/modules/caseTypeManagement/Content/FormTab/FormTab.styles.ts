// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useFormTabStyles = makeStyles(
  ({ mintlab: { greyscale } }: Theme) => ({
    wrapper: {
      display: 'flex',
      flexDirection: 'column',
      overflowY: 'auto',
      padding: 20,
      paddingBottom: 100,
    },
    subHeader: {
      marginBottom: 20,
    },
    subHeaderTop: {
      marginTop: 40,
    },
    fieldWrapper: {
      maxWidth: 1000,
    },
    richText: {
      backgroundColor: greyscale.light,
    },
    noHelp: {
      width: 'calc(100% - 50px)',
    },
  })
);

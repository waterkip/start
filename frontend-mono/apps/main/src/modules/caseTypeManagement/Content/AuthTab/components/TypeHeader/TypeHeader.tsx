// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { Typography } from '@mui/material';
import DropdownMenu, {
  DropdownMenuList,
} from '@mintlab/ui/App/Zaaksysteem/DropdownMenu';
import Button from '@mintlab/ui/App/Material/Button';
import {
  AuthCopyType,
  AuthSetType,
  AuthTypeType,
  SetSetsType,
  SetCopyType,
} from '../../AuthTab.types';
import {
  addAuths,
  copyAuths,
  overwriteAuths,
  removeAuths,
} from '../../AuthTab.library';
import { useTypeHeaderStyles } from './TypeHeader.styles';

type TypeHeaderPropsType = {
  sets: AuthSetType;
  setSets: SetSetsType;
  copy: AuthCopyType;
  setCopy: SetCopyType;
  type: AuthTypeType;
};

const TypeHeader: React.ComponentType<TypeHeaderPropsType> = ({
  sets,
  setSets,
  copy,
  setCopy,
  type,
}) => {
  const [t] = useTranslation('caseTypeManagement');
  const classes = useTypeHeaderStyles();

  const actionList = [
    {
      action: () => copyAuths(sets, setCopy, type),
      label: t('auth.action.copy'),
    },
    {
      action: () => addAuths(sets, setSets, copy, type),
      label: t('auth.action.paste'),
      condition: Boolean(copy),
    },
    {
      action: () => overwriteAuths(sets, setSets, copy, type),
      label: t('auth.action.overwrite'),
      condition: Boolean(copy),
    },
    {
      action: () => removeAuths(sets, setSets, type),
      label: t('auth.action.removeAll'),
    },
  ];

  return (
    <div className={classes.wrapper}>
      <div />
      <Typography variant="h2">{t(`auth.type.${type}`)}</Typography>
      <DropdownMenu
        trigger={<Button icon="more_vert" name="advancedActions" />}
      >
        <DropdownMenuList items={actionList} />
      </DropdownMenu>
    </div>
  );
};

export default TypeHeader;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import { APIAdmin } from '@zaaksysteem/generated';
import { BreadcrumbItemType } from '@mintlab/ui/App/Material/Breadcrumbs';
import { AuthType } from './Content/AuthTab/AuthTab.types';
import { ChildType } from './Content/ChildrenTab/ChildrenTab.types';
import { PhaseType, SideBarType } from './Content/PhasesTab/PhasesTab.types';
import {
  AllowedRequestorTypesType,
  DesignationOfConfidentialityValuesType,
  InitiatorTypeValuesType,
  LeadTimeTypeValueType,
  LeadTimeValueType,
  ProcessingLegalValuesType,
  ProcessingTypeValuesType,
} from './Content/FormTab/FormTab.types';

export type ChoiceType = { label: string; value: string };

type RawRelationsType =
  CaseTypeResponseBodyType['data']['attributes']['relations'];

export type RawPresetRequestorType = RawRelationsType['preset_requestor'];
export type PresetRequestorType = {
  label: string;
  value: string;
  type: NonNullable<RawPresetRequestorType>['type'];
} | null;

export type RawPresetAssigneeType = RawRelationsType['api_preset_assignee'];
export type PresetAssigneeType = {
  label: string;
  value: string;
  type: NonNullable<RawPresetAssigneeType>['type'];
} | null;

export type FormTabType =
  | 'common'
  | 'documentation'
  | 'relations'
  | 'webform'
  | 'registrationform'
  | 'case_dossier'
  | 'api';

export type CustomTabType = 'phases' | 'authorization' | 'children';

export type TabType = FormTabType | CustomTabType;

// CASE TYPE

export type ParamsType = {
  uuid: string;
  tab: TabType;
  phaseNumber: string;
  sideBarType: SideBarType;
};

type LeadTimeType = { amount: string; type: LeadTimeValueType };

export type CaseTypeType = {
  uuid: string;
  name: string;
  active: boolean;
  catalogFolder: CaseTypeResponseBodyType['data']['attributes']['catalog_folder'];
  common: {
    name: string;
    identification: string;
    tags: string;
    description: string;
    summaryInternal: string;
    summaryExternal: string;
    leadTimeType: LeadTimeTypeValueType;
    leadTimeLegal: LeadTimeType;
    leadTimeService: LeadTimeType;
    leadTimeLegalDate: string;
    leadTimeServiceDate: string;
  };
  documentation: {
    processDescription: string;
    initiatorType: InitiatorTypeValuesType[0];
    motivation: string;
    purpose: string;
    archiveClassificationCode: string;
    designationOfConfidentiality:
      | DesignationOfConfidentialityValuesType[0]
      | null;
    responsibleSubject: string;
    responsibleRelationship: string;
    possibilityForObjectionAndAppeal: boolean;
    publication: boolean;
    publicationText: string;
    bag: boolean;
    lexSilencioPositivo: boolean;
    mayPostpone: boolean;
    mayExtend: boolean;
    extensionPeriod: string;
    adjournPeriod: string;
    penaltyLaw: boolean;
    wkpbApplies: boolean;
    eWebform: string;
    legalBasis: string;
    localBasis: string;

    gdprEnabled: boolean;
    kind: string[];
    source: string[];
    processingType: ProcessingTypeValuesType[0];
    processForeignCountry: boolean;
    processForeignCountryReason: string;
    processingLegal: ProcessingLegalValuesType[0];
    processingLegalReason: string;
  };
  relations: {
    allowedRequestorTypes: AllowedRequestorTypesType[];
    presetRequestor: PresetRequestorType;
    presetAssignee: PresetAssigneeType;
    requestorAddressCase: boolean;
    requestorAddressCorrespondence: boolean;
    requestorAddressMap: boolean;
  };
  registrationform: {
    allowAssigningToSelf: boolean;
    allowAssigning: boolean;
    showConfidentiality: boolean;
    showContactDetails: boolean;
    allowAddRelations: boolean;
  };
  webform: {
    publicConfirmationTitle: string;
    publicConfirmationMessage: string;
    caseLocationMessage: string;
    pipViewMessage: string;
    enableWebform: boolean;
    createDelayed: boolean;
    addressCheck: boolean;
    reUseCaseData: boolean;
    enableOnlinePayment: boolean;
    enableManualPayment: boolean;
    emailRequired: boolean;
    phoneRequired: boolean;
    mobileRequired: boolean;
    disableCaptcha: boolean;
    generatePdfEndWebform: boolean;

    priceWebform: string;
    priceFrontdesk: string;
    pricePhone: string;
    priceEmail: string;
    priceAssignee: string;
    pricePost: string;
  };
  case_dossier: {
    disablePipForRequestor: boolean;
    lockRegistrationPhase: boolean;
    queueCoworkerChanges: boolean;
    allowExternalTaskAssignement: boolean;
    defaultDocumentFolders: ChoiceType[];
    htmlEmailTemplate: string | null;
  };
  api: {
    apiCanTransition: boolean;
    notifyOnNewCase: boolean;
    notifyOnNewDocument: boolean;
    notifyOnNewMessage: boolean;
    notifyOnExceedTerm: boolean;
    notifyOnAllocate: boolean;
    notifyOnPhaseTransition: boolean;
    notifyOnTaskChange: boolean;
    notifyOnLabelChange: boolean;
    notifyOnSubjectChange: boolean;
  };
  phases: PhaseType[];
  authorization: AuthType[];
  children: ChildType[];
};

export type GetBreadcrumbsType = (
  t: i18next.TFunction,
  caseType: CaseTypeType
) => BreadcrumbItemType[];

export type CaseTypeRequestParamsType =
  APIAdmin.GetVersionedCasetypeRequestParams;
export type CaseTypeResponseBodyType =
  APIAdmin.GetVersionedCasetypeResponseBody;

export type ConvertCaseTypeFromApiType = (
  response: CaseTypeResponseBodyType
) => CaseTypeType;

export type FetchCaseTypeType = (
  uuid: string
) => Promise<CaseTypeResponseBodyType>;

export type GetCaseTypetype = (uuid: string) => Promise<CaseTypeType>;

// SAVE CASETYPE

export type CaseTypeRequestBodyType =
  APIAdmin.UpdateVersionedCasetypeRequestBody;

export type RevertCaseTypeToApiType = (
  caseType: CaseTypeType
) => CaseTypeRequestBodyType;

export type SaveType =
  (data: {}) => APIAdmin.UpdateVersionedCasetypeResponseBody;

// HTML EMAIL TEMPLATE

export type HtmlEmailTemplateType = ChoiceType;

type RawHtmlEmailTemplateType = {
  image: string | null;
  label: string;
  template: string;
};

type EmailIntegrationType = {
  instance: {
    interface_config: {
      rich_email: 1 | 0 | null;
      rich_email_templates: RawHtmlEmailTemplateType[] | null;
    };
  };
};

export type EmailIntegrationsResponseBodyType = {
  result: {
    instance: {
      rows: EmailIntegrationType[];
    };
  };
};

type RawSubjectRoleType = {
  instance: {
    label: string;
  };
};

export type SubjectRolesResponseBodyType = {
  result: {
    instance: {
      rows: RawSubjectRoleType[];
    };
  };
};

export type SubjectRoleType = ChoiceType;

type WmsLayerType = {
  instance: {
    active: boolean;
    label: string;
    layer_name: string;
  };
};

export type MapIntegrationResponseBodyType = {
  result: {
    instance: {
      wms_layers: WmsLayerType[];
    };
  };
};

export type MapLayerType = ChoiceType;

type ListAsObjectType = {
  [key: string]: boolean;
};

type ListAsArrayType = string[];

export type ConvertCheckboxListType = (
  listAsObject?: ListAsObjectType
) => ListAsArrayType;

export type RevertCheckboxListType = (
  possibleValues: string[],
  listAsArray: ListAsArrayType
) => ListAsObjectType;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { queryClient } from '../../queryClient';

export const GET_JOBS = 'GET_JOBS';
export const GET_CASE_OBJECT = 'GET_CASE_OBJECT';

export const invalidateCaseJobsAndCaseObj = () => {
  queryClient.invalidateQueries([GET_JOBS, GET_CASE_OBJECT]);
};

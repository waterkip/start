// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useParams } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import {
  SideMenu,
  SideMenuItemType,
} from '@mintlab/ui/App/Zaaksysteem/SideMenu';
import { CaseObjType } from '../../Case.types';

const getSelectedItem = (params: any): string => {
  const view = params['*'].split('/')[0];

  return view || 'phases';
};

const MenuItemLink = React.forwardRef<any, SideMenuItemType>(
  ({ href, ...restProps }, ref) => <Link to={href || ''} {...restProps} />
);

const Navigation: React.ComponentType<{
  caseObj: CaseObjType;
  folded: boolean;
}> = ({ caseObj, folded }) => {
  const [t] = useTranslation('case');
  const params = useParams();
  const selectedItem = getSelectedItem(params);

  const menuItems: SideMenuItemType[] = [
    {
      id: 'phases',
      icon: iconNames.done_all,
      count: caseObj.numUnacceptedUpdates,
    },
    {
      id: 'documents',
      icon: iconNames.insert_drive_file,
      count: caseObj.numUnacceptedFiles,
    },
    { id: 'timeline', icon: iconNames.schedule },
    {
      id: 'communication',
      icon: iconNames.chat_bubble,
      count: caseObj.numUnreadCommunication,
    },
    ...(caseObj.location ? [{ id: 'location', icon: iconNames.map }] : []),
    { id: 'relations', icon: iconNames.link },
  ].map(({ id, icon, count }) => {
    const label = t(`views.${id}`);

    return {
      id,
      icon: <Icon size="small">{icon}</Icon>,
      count,
      label,
      href: id,
      selected: selectedItem === id,
      component: MenuItemLink,
    };
  });

  return <SideMenu items={menuItems} folded={folded} />;
};

export default Navigation;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { AnyFormDefinitionField } from '@zaaksysteem/common/src/components/form/types';
import {
  Rule,
  valueOneOf,
  valueEquals,
  hideFields,
  showFields,
  setFieldValue,
  transferDataAsConfig,
} from '@zaaksysteem/common/src/components/form/rules';
import { CaseObjType, CaseTypeType } from '../../Case.types';
import {
  getCurrentResult,
  getDestructionTermOptions,
  getResults,
} from './Caseactions.library';

type GetFormDefinitionType = ({
  t,
  caseObj,
  caseType,
}: {
  t: i18next.TFunction;
  caseObj: CaseObjType;
  caseType: CaseTypeType;
}) => AnyFormDefinitionField[];

const getAssignFormDefinition: GetFormDefinitionType = ({ t, caseObj }) => [
  {
    name: 'allocationType',
    type: fieldTypes.RADIO_GROUP,
    value: caseObj.status,
    label: t('caseActions.assign.allocationType'),
    required: true,
    choices: [
      {
        label: t('caseActions.assign.choices.departmentRole'),
        value: 'departmentRole',
      },
      {
        label: t('caseActions.assign.choices.employee'),
        value: 'employee',
      },
      {
        label: t('caseActions.assign.choices.self'),
        value: 'self',
      },
    ],
  },
  {
    name: 'department',
    type: fieldTypes.DEPARTMENT_FINDER,
    value: null,
    label: t('caseActions.assign.department'),
    required: true,
    placeholder: t('caseActions.assign.placeholders.department'),
  },
  {
    name: 'role',
    type: fieldTypes.ROLE_FINDER,
    value: null,
    required: true,
    label: t('caseActions.assign.role'),
    placeholder: t('caseActions.assign.placeholders.role'),
  },
  {
    name: 'employee',
    type: fieldTypes.CONTACT_FINDER,
    value: null,
    required: true,
    label: t('caseActions.assign.employee'),
    placeholder: t('caseActions.assign.placeholders.employee'),
    config: {
      subjectTypes: ['employee'],
    },
  },
  {
    name: 'addAsAssignee',
    type: fieldTypes.CHECKBOX,
    value: false,
    label: t('caseActions.assign.addAsAssignee'),
    suppressLabel: true,
  },
  {
    name: 'changeDepartment',
    type: fieldTypes.CHECKBOX,
    value: true,
    label: t('caseActions.assign.changeDepartment'),
    suppressLabel: true,
  },
  {
    name: 'notify',
    type: fieldTypes.CHECKBOX,
    value: true,
    label: t('caseActions.assign.notify'),
    suppressLabel: true,
  },
  {
    name: 'comment',
    type: fieldTypes.TEXTAREA,
    value: '',
    label: t('caseActions.assign.comment'),
  },
];

const getAssignRules = () => [
  new Rule()
    .when('allocationType', valueEquals('departmentRole'))
    .then(showFields(['department', 'role']))
    .else(hideFields(['department', 'role'])),
  new Rule()
    .when('allocationType', valueEquals('employee'))
    .then(showFields(['employee', 'addAsAssignee', 'notify']))
    .else(hideFields(['employee', 'addAsAssignee', 'notify'])),
  new Rule()
    .when('allocationType', valueOneOf(['employee', 'self']))
    .then(showFields(['changeDepartment']))
    .else(hideFields(['changeDepartment'])),
  new Rule()
    .when(() => true)
    .then(transferDataAsConfig('department', 'role', 'parentRoleUuid')),
];

const getStallFormDefinition: GetFormDefinitionType = ({ t }) => [
  {
    name: 'reason',
    type: fieldTypes.TEXT,
    value: null,
    label: t('caseActions.stall.reason'),
    required: true,
  },
  {
    name: 'suspensionType',
    type: fieldTypes.RADIO_GROUP,
    value: 'definite',
    required: true,
    choices: [
      {
        label: t('caseActions.stall.choices.suspensionType.indefinite'),
        value: 'indefinite',
      },
      {
        label: t('caseActions.stall.choices.suspensionType.definite'),
        value: 'definite',
      },
    ],
  },
  {
    name: 'termType',
    type: fieldTypes.SELECT,
    value: {
      label: t(`caseActions.stall.choices.termType.calendar_days`),
      value: 'calendar_days',
    },
    required: true,
    label: t('caseActions.stall.termType'),
    choices: ['calendar_days', 'work_days', 'weeks', 'fixed_date'].map(
      termType => ({
        label: t(`caseActions.stall.choices.termType.${termType}`),
        value: termType,
      })
    ),
  },
  {
    name: 'termLength',
    type: fieldTypes.NUMERIC,
    value: 0,
    label: t('caseActions.stall.termLength'),
    required: true,
  },
  {
    name: 'fixedDate',
    type: fieldTypes.DATEPICKER,
    value: null,
    variant: 'inline',
    required: true,
    label: t('caseActions.stall.fixedDate'),
  },
];

const getStallRules = () => [
  new Rule()
    .when('suspensionType', valueEquals('definite'))
    .then(showFields(['termType']))
    .else(hideFields(['termType'])),
  new Rule()
    .when('suspensionType', valueEquals('indefinite'))
    .then(setFieldValue('termType', null)),
  new Rule()
    .when('termType', valueOneOf(['calendar_days', 'work_days', 'weeks']))
    .then(showFields(['termLength']))
    .else(hideFields(['termLength'])),
  new Rule()
    .when('termType', valueEquals('fixed_date'))
    .then(showFields(['fixedDate']))
    .else(hideFields(['fixedDate'])),
];

const getResumeFormDefinition: GetFormDefinitionType = ({ t, caseObj }) => [
  {
    name: 'reason',
    type: fieldTypes.TEXT,
    value: null,
    label: t('caseActions.resume.reason'),
    required: true,
  },
  {
    name: 'since',
    type: fieldTypes.DATEPICKER,
    value: caseObj.stalledSinceDate,
    variant: 'inline',
    required: true,
    label: t('caseActions.resume.since'),
  },
  {
    name: 'until',
    type: fieldTypes.DATEPICKER,
    value: caseObj.stalledUntilDate,
    variant: 'inline',
    required: true,
    label: t('caseActions.resume.until'),
  },
];

const getResolvePrematurelyFormDefinition: GetFormDefinitionType = ({
  t,
  caseObj,
  caseType,
}) => {
  const currentResult = getCurrentResult(caseObj, caseType);
  const resultValue = currentResult
    ? currentResult.resultaat_id.toString()
    : null;

  return [
    {
      name: 'reason',
      type: fieldTypes.TEXT,
      value: null,
      label: t('caseActions.resolvePrematurely.reason'),
      required: true,
    },
    {
      name: 'result',
      type: fieldTypes.RADIO_GROUP,
      value: resultValue,
      label: t('caseActions.resolvePrematurely.result'),
      required: true,
      choices: getResults(caseType),
    },
  ];
};

const getProlongFormDefinition: GetFormDefinitionType = ({ t }) => [
  {
    name: 'reason',
    type: fieldTypes.TEXT,
    value: null,
    label: t('caseActions.prolong.reason'),
    required: true,
  },
  {
    name: 'prolongType',
    type: fieldTypes.RADIO_GROUP,
    value: null,
    required: true,
    choices: [
      {
        label: t('caseActions.prolong.choices.prolongType.fixedDate'),
        value: 'fixedDate',
      },
      {
        label: t('caseActions.prolong.choices.prolongType.changeTerm'),
        value: 'changeTerm',
      },
    ],
  },
  {
    name: 'termType',
    type: fieldTypes.RADIO_GROUP,
    value: 'kalenderdagen',
    required: true,
    choices: ['kalenderdagen', 'werkdagen', 'weken'].map(termType => ({
      label: t(`caseActions.prolong.choices.termType.${termType}`),
      value: termType,
    })),
  },
  {
    name: 'amount',
    type: fieldTypes.NUMERIC,
    value: null,
    label: t('caseActions.prolong.termLength'),
    required: true,
  },
  {
    name: 'fixedDate',
    type: fieldTypes.DATEPICKER,
    value: null,
    variant: 'inline',
    required: true,
    label: t('caseActions.prolong.fixedDate'),
  },
];

const getProlongRules = () => [
  new Rule()
    .when('prolongType', valueEquals('fixedDate'))
    .then(showFields(['fixedDate']))
    .else(hideFields(['fixedDate'])),
  new Rule()
    .when('prolongType', valueEquals('changeTerm'))
    .then(showFields(['termType', 'amount']))
    .else(hideFields(['termType', 'amount'])),
];

const getCopyFormDefinition: GetFormDefinitionType = ({ t }) => [
  {
    name: 'copyText',
    type: fieldTypes.TEXT,
    value: t('caseActions.copy.text'),
    readOnly: true,
  },
];

const getSetRequestorFormDefinition: GetFormDefinitionType = ({
  t,
  caseObj,
}) => [
  {
    name: 'requestor',
    type: fieldTypes.CONTACT_FINDER,
    value: null,
    required: true,
    label: t('caseActions.setRequestor.requestor'),
    placeholder: t('caseActions.setRequestor.placeholder', {
      requestorType: t(
        `common:entityType.${caseObj.requestor?.type}`
      ).toLowerCase(),
    }),
    suppressLabel: true,
    config: {
      subjectTypes: [caseObj.requestor?.type],
    },
  },
];

const getSetAssigneeFormDefinition: GetFormDefinitionType = ({ t }) => [
  {
    name: 'assignee',
    type: fieldTypes.CONTACT_FINDER,
    value: null,
    required: true,
    label: t('caseActions.setAssignee.requestor'),
    placeholder: t('caseActions.setAssignee.placeholder'),
    suppressLabel: true,
    config: {
      subjectTypes: ['employee'],
    },
  },
];

const getSetCoordinatorFormDefinition: GetFormDefinitionType = ({ t }) => [
  {
    name: 'coordinator',
    type: fieldTypes.CONTACT_FINDER,
    value: null,
    required: true,
    label: t('caseActions.setCoordinator.coordinator'),
    placeholder: t('caseActions.setCoordinator.placeholder'),
    suppressLabel: true,
    config: {
      subjectTypes: ['employee'],
    },
  },
];

const getSetDepartmentAndRoleFormDefinition: GetFormDefinitionType = ({
  t,
}) => [
  {
    name: 'department',
    type: fieldTypes.DEPARTMENT_FINDER,
    value: null,
    label: t('caseActions.setDepartmentAndRole.department'),
    required: true,
    placeholder: t('caseActions.setDepartmentAndRole.placeholders.department'),
  },
  {
    name: 'role',
    type: fieldTypes.ROLE_FINDER,
    value: null,
    required: true,
    label: t('caseActions.setDepartmentAndRole.role'),
    placeholder: t('caseActions.setDepartmentAndRole.placeholders.role'),
  },
];

const getSetDepartmentAndRoleRules = () => [
  new Rule()
    .when(() => true)
    .then(transferDataAsConfig('department', 'role', 'parentRoleUuid')),
];

const getSetRegistrationDateFormDefinition: GetFormDefinitionType = ({
  t,
  caseObj,
}) => [
  {
    name: 'registrationDate',
    type: fieldTypes.DATEPICKER,
    value: caseObj.registrationDate,
    variant: 'inline',
    required: true,
    suppressLabel: true,
    label: t('caseActions.setRegistrationDate.registrationDate'),
    placeholder: t('caseActions.setRegistrationDate.placeholder'),
  },
];

const getSetTargetCompletionDateFormDefinition: GetFormDefinitionType = ({
  t,
}) => [
  {
    name: 'targetCompletionDate',
    type: fieldTypes.DATEPICKER,
    value: null,
    variant: 'inline',
    required: true,
    suppressLabel: true,
    label: t('caseActions.setTargetCompletionDate.targetCompletionDate'),
    placeholder: t('caseActions.setTargetCompletionDate.placeholder'),
  },
];

const getSetCompletionDateFormDefinition: GetFormDefinitionType = ({ t }) => [
  {
    name: 'completionDate',
    type: fieldTypes.DATEPICKER,
    value: null,
    variant: 'inline',
    required: true,
    suppressLabel: true,
    label: t('caseActions.setCompletionDate.completionDate'),
    placeholder: t('caseActions.setCompletionDate.placeholder'),
  },
];

const getSetDestructionDateFormDefinition: GetFormDefinitionType = ({ t }) => [
  {
    name: 'reason',
    type: fieldTypes.SELECT,
    value: null,
    required: true,
    label: t('caseActions.setDestructionDate.reason'),
    placeholder: t('caseActions.setDestructionDate.placeholders.reason'),
    choices: [
      'In belang van de aanvrager',
      'Uniek of bijzonder karakter voor de organisatie',
      'Bijzondere tijdsomstandigheid of gebeurtenis',
      'Beeldbepalend karakter',
      'Samenvatting van gegevens',
      'Betrokkene(n) is van bijzondere betekenis geweest',
      'Vervanging van stukken bij calamiteit',
      'Aanleiding van algemene regelgeving',
      'Verstoring van logische samenhang',
    ].map((reason: string) => ({ label: reason, value: reason })),
  },
  {
    name: 'termType',
    type: fieldTypes.RADIO_GROUP,
    value: null,
    required: true,
    choices: [
      {
        label: t('caseActions.setDestructionDate.choices.termType.fixedDate'),
        value: 'fixedDate',
      },
      {
        label: t('caseActions.setDestructionDate.choices.termType.calculate'),
        value: 'calculate',
      },
    ],
  },
  {
    name: 'fixedDate',
    type: fieldTypes.DATEPICKER,
    value: null,
    variant: 'inline',
    required: true,
    label: t('caseActions.setDestructionDate.fixedDate'),
    suppressLabel: true,
    placeholder: t('caseActions.setDestructionDate.placeholders.fixedDate'),
  },
  {
    name: 'term',
    type: fieldTypes.SELECT,
    value: null,
    required: true,
    label: t('caseActions.setDestructionDate.term'),
    placeholder: t('caseActions.setDestructionDate.placeholders.term'),
    choices: getDestructionTermOptions(t),
  },
];

const getSetDestructionDateRules = () => [
  new Rule()
    .when('termType', valueEquals('fixedDate'))
    .then(showFields(['fixedDate']))
    .else(hideFields(['fixedDate'])),
  new Rule()
    .when('termType', valueEquals('calculate'))
    .then(showFields(['term']))
    .else(hideFields(['term'])),
];

const getSetPhaseFormDefinition: GetFormDefinitionType = ({
  caseObj,
  caseType,
}) => {
  const phases = caseType.phases;
  const currentPhase = caseObj.phase;

  return [
    {
      name: 'milestone',
      type: fieldTypes.RADIO_GROUP,
      value: currentPhase.toString(),
      choices: phases.map((phase: any) => ({
        label: phase.phase,
        value: phase.milestone.toString(),
        // can't set to the registration phase, or to a future phase
        disabled: phase.milestone === 1 || phase.milestone > currentPhase,
      })),
    },
  ];
};

const getSetStatusFormDefinition: GetFormDefinitionType = ({ t, caseObj }) => [
  {
    name: 'status',
    type: fieldTypes.RADIO_GROUP,
    value: caseObj.status,
    choices: ['new', 'open', 'stalled', 'resolved'].map(status => ({
      label: t(`status.${status}`),
      value: status,
    })),
  },
];

const getSetPaymentStatusFormDefinition: GetFormDefinitionType = ({
  t,
  caseObj,
}) => [
  {
    name: 'paymentStatus',
    type: fieldTypes.RADIO_GROUP,
    value: caseObj.payment?.status,
    choices: ['success', 'pending', 'failed', 'offline'].map(status => ({
      label: t(`paymentStatus.${status}`),
      value: status,
    })),
  },
];

const getCustomiseAuthorizationsFormDefinition: GetFormDefinitionType = ({
  t,
}) => [
  {
    format: 'object',
    name: 'authorizationsDisplay',
    type: 'AuthorizationField',
    value: [
      // MINTY-8819
      // {
      //   department: {
      //     label: 'a',
      //     value: 'a',
      //   },
      //   role: {
      //     label: 'b',
      //     value: 'b',
      //   },
      //   capabilities: 'read',
      // },
    ],
    required: true,
    readOnly: true,
  },
  {
    format: 'object',
    name: 'authorizations',
    type: 'AuthorizationField',
    value: [],
    required: true,
  },
  {
    name: 'cascadeTo',
    type: fieldTypes.CHECKBOX_GROUP,
    value: [],
    choices: ['continuation', 'partial', 'related'].map(value => ({
      label: t(
        `caseActions.customiseAuthorizations.cascadeTo.choices.${value}`
      ),
      value: value,
    })),
    label: `${t('caseActions.customiseAuthorizations.cascadeTo.label')}:`,
  },
];

const getSetResultFormDefinition: GetFormDefinitionType = ({
  caseObj,
  caseType,
}) => {
  const currentResult = getCurrentResult(caseObj, caseType);
  const resultValue = currentResult.resultaat_id.toString();

  return [
    {
      name: 'result',
      type: fieldTypes.RADIO_GROUP,
      value: resultValue,
      required: true,
      choices: getResults(caseType),
    },
  ];
};

const getUpdateFieldValueFormDefinition: GetFormDefinitionType = () => [];

const getChangeCaseTypeFormDefinition: GetFormDefinitionType = ({ t }) => [
  {
    name: 'casetype',
    type: fieldTypes.CASE_TYPE_FINDER,
    value: null,
    label: t('caseActions.changeCaseType.caseType'),
    suppressLabel: true,
    placeholder: t('caseActions.changeCaseType.placeholder'),
    required: true,
  },
];

export const getFormDefinition = {
  assign: getAssignFormDefinition,
  stall: getStallFormDefinition,
  resume: getResumeFormDefinition,
  resolvePrematurely: getResolvePrematurelyFormDefinition,
  prolong: getProlongFormDefinition,
  copy: getCopyFormDefinition,
  setRequestor: getSetRequestorFormDefinition,
  setAssignee: getSetAssigneeFormDefinition,
  setCoordinator: getSetCoordinatorFormDefinition,
  setDepartmentAndRole: getSetDepartmentAndRoleFormDefinition,
  setRegistrationDate: getSetRegistrationDateFormDefinition,
  setTargetCompletionDate: getSetTargetCompletionDateFormDefinition,
  setCompletionDate: getSetCompletionDateFormDefinition,
  setDestructionDate: getSetDestructionDateFormDefinition,
  setPhase: getSetPhaseFormDefinition,
  setStatus: getSetStatusFormDefinition,
  setPaymentStatus: getSetPaymentStatusFormDefinition,
  customiseAuthorizations: getCustomiseAuthorizationsFormDefinition,
  setResult: getSetResultFormDefinition,
  updateFieldValue: getUpdateFieldValueFormDefinition,
  changeCaseType: getChangeCaseTypeFormDefinition,
};

export const getRules = {
  assign: getAssignRules,
  stall: getStallRules,
  resume: () => [],
  resolvePrematurely: () => [],
  prolong: getProlongRules,
  copy: () => [],
  setRequestor: () => [],
  setAssignee: () => [],
  setCoordinator: () => [],
  setDepartmentAndRole: getSetDepartmentAndRoleRules,
  setRegistrationDate: () => [],
  setTargetCompletionDate: () => [],
  setCompletionDate: () => [],
  setDestructionDate: getSetDestructionDateRules,
  setPhase: () => [],
  setStatus: () => [],
  setPaymentStatus: () => [],
  customiseAuthorizations: () => [],
  setResult: () => [],
  updateFieldValue: () => [],
  changeCaseType: () => [],
};

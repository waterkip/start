// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
//@ts-ignore
import ZsIcon from '@mintlab/ui/App/Zaaksysteem/ZsIcon';
import { ActionType } from '../Actions.types';

type IconPropsType = {
  action: ActionType;
};

export const Allocation: React.ComponentType<IconPropsType> = () => (
  <Icon size="small" color="coral">
    {iconNames.people}
  </Icon>
);

export const Case: React.ComponentType<IconPropsType> = () => (
  <ZsIcon size="small">{'entityType.case'}</ZsIcon>
);

export const Email: React.ComponentType<IconPropsType> = () => (
  <ZsIcon size="small">{'entityType.email_template'}</ZsIcon>
);

export const Object_action: React.ComponentType<IconPropsType> = () => (
  <ZsIcon size="small">{'entityType.object_type'}</ZsIcon>
);

export const Subject: React.ComponentType<IconPropsType> = ({ action }) => (
  <ZsIcon size="small">
    {action.data.betrokkene_type === 'natuurlijk_persoon'
      ? 'entityType.person'
      : 'entityType.organization'}
  </ZsIcon>
);

export const Template: React.ComponentType<IconPropsType> = () => (
  <ZsIcon size="small">{'entityType.document_template'}</ZsIcon>
);

const Icons: {
  allocation: React.ComponentType<IconPropsType>;
  case: React.ComponentType<IconPropsType>;
  email: React.ComponentType<IconPropsType>;
  object_action: React.ComponentType<IconPropsType>;
  subject: React.ComponentType<IconPropsType>;
  template: React.ComponentType<IconPropsType>;
} = {
  allocation: Allocation,
  case: Case,
  email: Email,
  object_action: Object_action,
  subject: Subject,
  template: Template,
};

export default Icons;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useParams } from 'react-router-dom';
import { PanelLayout, Panel } from '@mintlab/ui/App/Zaaksysteem/PanelLayout';
import { TopMenu } from '@mintlab/ui/App/Zaaksysteem/TopMenu';
import { CaseObjType, CaseTypeType } from '../../Case.types';
import { useCasePhasesStyles } from './Phases.styles';
import Form from './Form/Form';
import SideBar from './SideBar';
import { getNavigationItems } from './Phases.library';

export type PhasesPropsType = {
  caseObj: CaseObjType;
  caseType: CaseTypeType;
};

type PhasesParamsType = {
  phaseNumber: string;
};

const Phases: React.FunctionComponent<PhasesPropsType> = ({
  caseObj,
  caseType,
}) => {
  const { phaseNumber } = useParams() as PhasesParamsType;
  const classes = useCasePhasesStyles();
  const [openTasksCount, setOpenTasksCount] = useState<number>();
  const [checkedActionsCount, setCheckedActionsCount] = useState<number>();

  const navigationItems = getNavigationItems(caseObj, caseType, phaseNumber);

  return (
    <div className={classes.wrapper}>
      <div className={classes.header}>
        <TopMenu items={navigationItems} />
      </div>
      <div className={classes.content}>
        <PanelLayout>
          <Panel className={classes.form}>
            <Form
              caseObj={caseObj}
              caseType={caseType}
              phaseNumber={phaseNumber}
            />
          </Panel>
          <Panel type="side" className={classes.sideBar}>
            <SideBar
              caseObj={caseObj}
              caseType={caseType}
              phaseNumber={Number(phaseNumber)}
              openTasksCount={openTasksCount}
              checkedActionsCount={checkedActionsCount}
              setOpenTasksCount={setOpenTasksCount}
              setCheckedActionsCount={setCheckedActionsCount}
            />
          </Panel>
        </PanelLayout>
      </div>
    </div>
  );
};

export default Phases;

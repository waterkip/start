// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { SubjectTypeType } from '@zaaksysteem/common/src/types/SubjectTypes';
import { APICaseManagement } from '@zaaksysteem/generated';

type Attributes = Pick<
  APICaseManagement.TaskEntity['attributes'],
  'completed' | 'description' | 'title'
>;

type Meta = Pick<
  APICaseManagement.TaskEntity['meta'],
  'is_editable' | 'can_set_completion'
>;

export type RelatedContactType = null | {
  label: string;
  value: string;
  type: SubjectTypeType;
};

export type TaskType = {
  title: string;
  task_uuid: string;
  dso_action_request?: boolean;
  assignee: RelatedContactType;
  supply_chain_partner: RelatedContactType;
  product_code: string | null;
  due_date: string | null;
} & Attributes &
  Meta;

export type TasksType = TaskType[];

export type HandleChangeType = (
  action: any,
  tasks: TasksType,
  newTasks: TasksType
) => void;
export type AddTaskType = (title: string) => void;
export type UpdateTaskType = (task_uuid: string, values: TaskType) => void;
export type DeleteTaskType = (task_uuid: string) => void;
export type SetTaskCompletionType = (
  task_uuid: string,
  completed: boolean
) => void;
export type EnterEditModeType = (task: TaskType) => void;
export type ExitEditModeType = () => void;

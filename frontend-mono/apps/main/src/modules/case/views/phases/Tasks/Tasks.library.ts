// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { v4 } from 'uuid';
import {
  fetchTasks,
  createTaskRequest,
  setTaskCompletionRequest,
  updateTaskRequest,
  deleteTaskRequest,
  fetchRelatedContacts,
} from './Tasks.requests';
import {
  HandleChangeType,
  RelatedContactType,
  TasksType,
  TaskType,
} from './Tasks.types';

export const sortTasks = (tasks: TasksType): TasksType => {
  const completed = tasks.filter(task => task.completed).reverse();
  const nonCompleted = tasks.filter(task => !task.completed).reverse();
  const sortedTasks = [...nonCompleted, ...completed];

  return sortedTasks;
};

export const countOpenTasks = (tasks: TasksType): number =>
  tasks.filter(task => !task.completed).length;

export const getTasks = async (
  caseUuid: string,
  phaseNumber: string,
  setTasks: (tasks: TasksType) => void,
  setOpenTasksCount: (openTasksCount: number) => void
) => {
  const response = await fetchTasks(caseUuid, phaseNumber);

  /* eslint complexity: [2, 7] */
  const tasks = response.data?.map(
    ({
      id,
      attributes: { completed, description, due_date, title },
      product_code,
      dso_action_request,
      meta: { is_editable, can_set_completion },
      relationships: { assignee },
    }: any) => {
      const contact =
        assignee && assignee.data
          ? {
              label: assignee.data.meta.display_name,
              type: assignee.data.type,
              value: assignee.data.id,
            }
          : null;

      return {
        task_uuid: id,
        title,
        completed,
        description,
        dso_action_request,
        assignee: !dso_action_request ? contact : null,
        supply_chain_partner: dso_action_request ? contact : null,
        product_code,
        due_date,
        is_editable,
        can_set_completion,
      };
    }
  );

  // because data is optional in the apidocs
  if (!tasks) return;

  setTasks(tasks);

  const openTasksCount = countOpenTasks(tasks);

  setOpenTasksCount(openTasksCount);
};

export const addTaskAction = async (
  caseUuid: string,
  phaseNumber: string,
  title: string,
  tasks: TasksType,
  handleChange: HandleChangeType
) => {
  const task_uuid = v4();
  const newTask: TaskType = {
    title,
    task_uuid,
    description: '',
    completed: false,
    dso_action_request: false,
    product_code: '',
    due_date: null,
    assignee: null,
    supply_chain_partner: null,
    is_editable: true,
    can_set_completion: true,
  };
  const request = () =>
    createTaskRequest(caseUuid, task_uuid, phaseNumber, title);

  handleChange(request, tasks, [...tasks, newTask]);
};

export const updateTaskAction = async (
  task_uuid: string,
  values: TaskType,
  tasks: TasksType,
  handleChange: HandleChangeType
) => {
  const contact = values.dso_action_request
    ? values.supply_chain_partner
    : values.assignee;
  const assignee = contact ? { type: contact.type, id: contact.value } : null;

  const request = () => updateTaskRequest({ ...values, task_uuid, assignee });

  const newTasks = tasks.reduce((acc: TasksType, task) => {
    if (task.task_uuid !== task_uuid) return [...acc, task];

    const newTask = { ...task, ...values };

    return [...acc, newTask];
  }, []);

  handleChange(request, tasks, newTasks);
};

export const deleteTaskAction = async (
  task_uuid: string,
  tasks: TasksType,
  handleChange: HandleChangeType
) => {
  const request = () => deleteTaskRequest(task_uuid);

  const newTasks = tasks.filter(task => task.task_uuid !== task_uuid);

  handleChange(request, tasks, newTasks);
};

export const setTaskCompletionAction = async (
  task_uuid: string,
  completed: boolean,
  tasks: TasksType,
  handleChange: HandleChangeType
) => {
  const request = () => setTaskCompletionRequest(task_uuid, completed);

  const newTasks = tasks.reduce((acc: TasksType, task) => {
    if (task.task_uuid !== task_uuid) return [...acc, task];

    const newTask = { ...task, completed };

    return [...acc, newTask];
  }, []);

  handleChange(request, tasks, newTasks);
};

export const getSupplyChainPartnersAction = async (
  caseUuid: string,
  setSupplyChainPartners: (contacts: RelatedContactType[]) => void
) => {
  const response = await fetchRelatedContacts(caseUuid);
  const contacts = response.data
    .map(
      ({
        attributes: { magic_string_prefix },
        relationships: {
          subject: {
            data: {
              id,
              meta: { display_name },
              type,
            },
          },
        },
      }) => ({
        value: id,
        type,
        label: display_name,
        magicString: magic_string_prefix,
      })
    )
    .filter(contact => contact.magicString.includes('ketenpartner'));

  // @ts-ignore
  setSupplyChainPartners(contacts);
};

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useTranslation } from 'react-i18next';
import React from 'react';
import FormDialog from '@zaaksysteem/common/src/components/dialogs/FormDialog/FormDialog';
import {
  CASE_FINDER,
  SELECT,
} from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import DropdownMenu from '@mintlab/ui/App/Zaaksysteem/DropdownMenu';
import MenuItem from '@mui/material/MenuItem/MenuItem';
import { useCaseObjectQuery } from '../../../Case.library';
import { ChipButton } from '../ChipButton';
import { relocatingView } from '../Documents.signals';
import {
  useCopyToCaseMutation,
  useCopyToPdfMutation,
  useRelatedCasesQuery,
} from '../Documents.requests';
import { SelectionsHook } from '../Documents.types';
import { UNSUPPORTED_COPY_TO_PDF_EXTENSIONS } from '../Documents.library';

/* eslint complexity: [2, 7] */
export const CopyItems = (props: { useSelectedRows: SelectionsHook }) => {
  const [t] = useTranslation('case');
  const [selectedRows, selections, selectedRowIds] = props.useSelectedRows();
  const caseObject = useCaseObjectQuery();
  const { mutate: copyToPdf } = useCopyToPdfMutation();
  const [dialog, setDialog] = React.useState({
    opened: false,
    toRelated: false,
  });

  return (
    <>
      <DropdownMenu
        transformOrigin={{
          vertical: -10,
          horizontal: 'center',
        }}
        trigger={
          <ChipButton
            icon="file_copy"
            title={t('documentTableActions.copy')}
            enabled={
              selections.exist &&
              !relocatingView.value &&
              !selectedRows.some(row => row.hasLockedFiles)
            }
          />
        }
      >
        {[
          {
            id: 'pdf',
            show:
              selections.single &&
              caseObject.data?.canEdit &&
              !UNSUPPORTED_COPY_TO_PDF_EXTENSIONS.includes(
                selectedRows[0].extension || ''
              ),
            onClick: () => {
              copyToPdf({
                caseId: caseObject?.data?.number || 0,
                fileId: selectedRowIds.value[0],
              });
            },
          },
          {
            id: 'related',
            onClick: () => {
              setDialog({ opened: true, toRelated: true });
            },
            show: true,
          },
          {
            id: 'other',
            onClick: () => {
              setDialog({ opened: true, toRelated: false });
            },
            show: true,
          },
        ].map(item => {
          const text = t('documentTableActions.copyTypes.' + item.id);
          return item.show ? (
            <MenuItem key={item.id} onClick={item.onClick}>
              {text}
            </MenuItem>
          ) : null;
        })}
      </DropdownMenu>
      {dialog.opened && (
        <CopyDialog
          useSelectedRows={props.useSelectedRows}
          toRelated={dialog.toRelated}
          onClose={() => {
            setDialog({ opened: false, toRelated: false });
          }}
        />
      )}
    </>
  );
};

const CopyDialog = ({
  toRelated,
  onClose,
  useSelectedRows,
}: {
  onClose: () => void;
  toRelated: boolean;
  useSelectedRows: SelectionsHook;
}) => {
  const [t] = useTranslation('case');
  const caseObject = useCaseObjectQuery();
  const [selectedRows] = useSelectedRows();

  const { data, isLoading: initializing } = useRelatedCasesQuery(
    toRelated,
    caseObject.data?.uuid || ''
  );
  const { mutateAsync: copy, isLoading: saving } = useCopyToCaseMutation(
    caseObject.data?.number || 0
  );

  return (
    <FormDialog
      initializing={initializing && toRelated}
      minHeight={320}
      saving={saving}
      onSubmit={values => copy({ ...values, rows: selectedRows }).then(onClose)}
      icon="file_copy"
      title={t('documentTabDialogs.copy.title')}
      formDefinitionT={t}
      onClose={onClose}
      formDefinition={
        toRelated
          ? [
              {
                type: SELECT,
                value: data ? data[0] : null,
                choices: data,
                label: t('documentTabDialogs.copy.copyTo'),
                format: 'text',
                name: 'copyTo',
              },
            ]
          : [
              {
                type: CASE_FINDER,
                label: t('documentTabDialogs.copy.copyTo'),
                value: null,
                format: 'text',
                config: { flatId: true },
                name: 'copyTo',
              },
            ]
      }
    />
  );
};

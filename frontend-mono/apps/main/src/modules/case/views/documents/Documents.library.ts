// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
/* eslint-disable complexity */

import { useDocumentsQuery } from './Documents.requests';
import {
  selectedAcceptedRowIds,
  selectedPendingRowIds,
} from './Documents.signals';
import {
  ActiveFiltering,
  DocumentsNodeType,
  DragSource,
  DocumentsTreeType,
  SelectionsHook,
  DocumentUploadOptions,
} from './Documents.types';

export const UNSUPPORTED_COPY_TO_PDF_EXTENSIONS = [
  'csv',
  'xls',
  'xlsx',
  'msg',
  'eml',
  'pdf',
];

export const getIdsOfLockedFiles = (data: DocumentsNodeType[]) =>
  data.filter(row => row.locked).map(row => row.id);

export const useSelectedAcceptedRows: SelectionsHook = () => {
  const { data } = useDocumentsQuery();
  const rows =
    data?.filter(row => selectedAcceptedRowIds.value.includes(row.id)) || [];

  return [
    rows,
    {
      exist: Boolean(rows.length),
      single: rows.length === 1,
      multiple: rows.length > 1,
      hasFolders: rows.some(row => row.type === 'folder'),
      hasFiles: rows.some(row => row.type === 'file'),
      onlyFiles: rows.every(row => row?.type === 'file'),
      onlyFolders: rows.every(row => row?.type === 'folder'),
      hasNonEmptyFolders: rows.some(
        row => row.type === 'folder' && Boolean(row.children?.length)
      ),
    },
    selectedAcceptedRowIds,
  ];
};

export const useSelectedPendingRows: SelectionsHook = () => {
  const { data } = useDocumentsQuery();
  const rows =
    data?.filter(row => selectedPendingRowIds.value.includes(row.id)) || [];

  return [
    rows,
    {
      exist: Boolean(rows.length),
      single: rows.length === 1,
      multiple: rows.length > 1,
      hasFolders: rows.some(row => row.type === 'folder'),
      hasFiles: rows.some(row => row.type === 'file'),
      onlyFiles: rows.every(row => row?.type === 'file'),
      onlyFolders: rows.every(row => row?.type === 'folder'),
      hasNonEmptyFolders: rows.some(
        row => row.type === 'folder' && Boolean(row.children?.length)
      ),
    },
    selectedPendingRowIds,
  ];
};

export const getRowsInbetween = (
  rows: DocumentsNodeType[],
  row1: DocumentsNodeType,
  row2: DocumentsNodeType
) => {
  let row1Reached = false;
  let row2Reached = false;
  let adding = false;
  const res = [] as DocumentsNodeType[];

  /* eslint complexity: [2, 10] */
  rows.forEach(row => {
    if (row.id === row1.id && row2Reached) {
      row1Reached = true;
      adding = false;
    } else if (row.id === row2.id && row1Reached) {
      row2Reached = true;
      adding = false;
    } else if (row.id === row1.id && !row2Reached) {
      row1Reached = true;
      adding = true;
    } else if (row.id === row2.id && !row1Reached) {
      row2Reached = true;
      adding = true;
    }

    adding && res.push(row);
  });

  return res;
};

export const findAndMark = (ids: string[]) => {
  ids.forEach(id => {
    let repeats = 0;
    //@ts-ignore
    const interval = setInterval(() => {
      repeats++;
      const el = document.querySelector(`div[data-id="${id}"]`);
      el?.querySelector('input')?.click();
      (repeats > 30 || el) && clearInterval(interval);
    }, 100);
  });
};

let count = 0;
const REFRESH_RATE = 4;

export const adjustGridStyling = () => {
  if (count > REFRESH_RATE) {
    const datagrid =
      document.querySelector<HTMLDivElement>('.datagrid-accepted');
    const pending = document.querySelector<HTMLDivElement>('.datagrid-pending');
    const labelsContainer =
      document.querySelector<HTMLDivElement>('.labelsContainer');
    const body = datagrid?.children[1] as HTMLDivElement;
    const header = datagrid?.children[0] as HTMLDivElement;
    const root = document.querySelector(':root') as HTMLBaseElement;

    if (body && header && labelsContainer) {
      const labelsContainerHeight =
        labelsContainer.getBoundingClientRect().height;
      const headerHeight = header.getBoundingClientRect().height;
      const pendingGridHeight = pending
        ? pending.getBoundingClientRect().height
        : 0;
      const paddingHeight = 150;

      const leftoverVerticalSpace =
        window.innerHeight -
        paddingHeight -
        pendingGridHeight -
        headerHeight -
        labelsContainerHeight;
      const fallbackHeight =
        window.innerHeight - 43 - labelsContainerHeight - headerHeight;

      body.style.maxHeight =
        (leftoverVerticalSpace < 260 ? fallbackHeight : leftoverVerticalSpace) +
        'px';
    }

    if (datagrid) {
      const leftoverSpace =
        datagrid.clientWidth -
        Math.max(
          ...Array.from(datagrid.querySelectorAll('.rowTitleText')).map(
            el => el.clientWidth
          )
        ) -
        88;

      if (leftoverSpace > 200) {
        root.style.setProperty('--createdDisplay', 'flex');
      } else {
        root.style.setProperty('--createdDisplay', 'none');
      }

      if (leftoverSpace > 200) {
        root.style.setProperty('--actionsDisplay', 'flex');
      } else {
        root.style.setProperty('--actionsDisplay', 'none');
      }

      if (leftoverSpace > 300) {
        root.style.setProperty('--originDisplay', 'flex');
      } else {
        root.style.setProperty('--originDisplay', 'none');
      }

      if (leftoverSpace > 385) {
        root.style.setProperty('--extensionDisplay', 'flex');
      } else {
        root.style.setProperty('--extensionDisplay', 'none');
      }

      if (leftoverSpace > 475) {
        root.style.setProperty('--authorDisplay', 'flex');
      } else {
        root.style.setProperty('--authorDisplay', 'none');
      }
    }

    count = 0;
  } else {
    count++;
  }

  requestAnimationFrame(adjustGridStyling);
};

export const rowSorter = (
  filteredEntries: DocumentsNodeType[],
  filtering: ActiveFiltering
) => {
  return filteredEntries.sort((rowA, rowB) => {
    const sorterOnSameNesting = (
      rowA: DocumentsNodeType,
      rowB: DocumentsNodeType
    ) => {
      const order = filtering.desc ? 1 : -1;
      let directionFile =
        rowA.title.toLowerCase() < rowB.title.toLowerCase() ? -1 : 1;
      let directionFolder = 0;

      if (filtering.target === 'title') {
        directionFile =
          rowA.title.toLowerCase() < rowB.title.toLowerCase()
            ? order * -1
            : order * 1;
      } else if (filtering.target === 'createdAt') {
        // We want sorting on this to leave order of folders intact
        directionFolder = directionFile;
        directionFile =
          rowA.createdAt === rowB.createdAt
            ? directionFile
            : (rowA._createdAt || Infinity) > (rowB._createdAt || Infinity)
              ? order * 1
              : order * -1;
      } else if (filtering.target === 'origin') {
        directionFolder = directionFile;
        directionFile =
          rowA.origin === rowB.origin
            ? directionFile
            : (rowA.origin || 'x') > (rowB.origin || 'x')
              ? order * 1
              : order * -1;
      } else if (filtering.target === 'originDate') {
        directionFolder = directionFile;
        directionFile =
          rowA.originDate === rowB.originDate
            ? directionFile
            : (rowA._originDate || Infinity) > (rowB._originDate || Infinity)
              ? order * 1
              : order * -1;
      } else if (filtering.target === 'extension') {
        directionFolder = directionFile;
        directionFile =
          rowA.extension?.toLowerCase() === rowB.extension?.toLowerCase()
            ? directionFile
            : (rowA.extension?.toLowerCase() || Infinity) >
                (rowB.extension?.toLowerCase() || Infinity)
              ? order * 1
              : order * -1;
      } else if (filtering.target === 'author') {
        directionFolder = directionFile;
        directionFile =
          rowA.createdBy === rowB.createdBy
            ? directionFile
            : (rowA.createdBy || Infinity) > (rowB.createdBy || Infinity)
              ? order * 1
              : order * -1;
      }

      return rowA.type === 'folder' && rowB.type === 'folder'
        ? directionFolder || directionFile
        : rowA.type === 'folder'
          ? -1
          : rowB.type === 'folder'
            ? 1
            : directionFile;
    };

    if (rowA.hierarchy.find(parent => parent.id === rowB.id)) {
      return 1;
    } else if (rowB.hierarchy.find(parent => parent.id === rowA.id)) {
      return -1;
    }

    const commonParent = rowA.hierarchy.find(par1 =>
      rowB.hierarchy.find(par2 => par2.id === par1.id)
    );

    const rowAParentIx = rowA.hierarchy.findIndex(
      par => par.id === commonParent?.id
    );

    const rowBParentIx = rowA.hierarchy.findIndex(
      par => par.id === commonParent?.id
    );

    const rowARepresentative =
      filteredEntries.find(
        row => row.id === rowA.hierarchy[rowAParentIx + 1].id
      ) || rowA;

    const rowBRepresentative =
      filteredEntries.find(
        row => row.id === rowB.hierarchy[rowBParentIx + 1].id
      ) || rowB;

    return sorterOnSameNesting(rowARepresentative, rowBRepresentative);
  });
};

/* eslint complexity: [2, 12] */
export const toggleRowSelection =
  (
    selectedRows: DocumentsNodeType[],
    listedRows: DocumentsNodeType[],
    lockedIds: string[],
    bucket: typeof selectedAcceptedRowIds
  ) =>
  (target: DocumentsNodeType) => {
    const change = selectedRows.find(
      selectedRow => selectedRow.id === target.id
    )
      ? selectedRows.filter(selectedRow => selectedRow.id !== target.id)
      : [...selectedRows, target];
    const changeIds = change.map(ch => ch.id);
    const rowSelectionModel = selectedRows.map(row => row.id);
    const addingSelections = changeIds.length > rowSelectionModel.length;
    const result = new Set(changeIds);
    if (changeIds.length === 0 || changeIds.length === listedRows.length) {
      // skip
    } else if (rowSelectionModel.length - changeIds.length > 1) {
      result.clear();
    } else if (addingSelections) {
      const addedId = changeIds.filter(
        el => !rowSelectionModel.includes(el)
      )[0];
      const addedEl = listedRows.find(row => row.id === addedId);
      if (
        addedEl?.type === 'folder' &&
        addedEl?.children &&
        addedEl?.children.some(child => changeIds.includes(child))
      ) {
        addedEl.children.forEach(child => result.add(child));
      } else {
        changeIds.forEach(id => {
          const row = listedRows.find(row => row.id === id);
          if (row?.type === 'folder' && row.children?.length) {
            row.children.forEach(child => result.add(child));
          }
        });
      }
    } else {
      const removedId = rowSelectionModel.filter(
        el => !changeIds.includes(el)
      )[0];
      const removedEl = listedRows.find(row => row.id === removedId);
      if (removedEl?.type === 'folder' && removedEl.children) {
        removedEl.children.forEach(child => result.delete(child));
      }
    }

    const fixTotalSelection = () => {
      listedRows.forEach(row => {
        if (
          row.type === 'folder' &&
          row.children?.length &&
          row.children.some(
            child => !result.has(child) && !lockedIds.includes(child)
          ) &&
          listedRows.length > 1
        ) {
          result.delete(row.id);
        }
      });
    };
    let len = result.size;
    fixTotalSelection();
    while (len !== result.size) {
      len = result.size;
      fixTotalSelection();
    }

    bucket.value = listedRows
      .filter(row => !row.locked)
      .filter(row => result.has(row.id))
      .map(row => row.id);
  };

export const toggleRangeSelection =
  (
    selectedRows: DocumentsNodeType[],
    listedRows: DocumentsNodeType[],
    bucket: typeof selectedAcceptedRowIds
  ) =>
  (target: DocumentsNodeType) => {
    const anchorRow = selectedRows[0];

    const result = [] as string[];
    let insideRange = false;
    listedRows
      .filter(row => !row.locked)
      /* eslint complexity: [2, 15] */
      .forEach(row => {
        let tempInsideRange = false;
        if (row.id === anchorRow.id || row.id === target.id) {
          tempInsideRange = true;
        }

        if (
          ((insideRange || tempInsideRange) && row.type === 'file') ||
          bucket.value.includes(row.id)
        ) {
          result.push(row.id);
        }

        if (row.id === target.id && insideRange) {
          insideRange = false;
        } else if (row.id === anchorRow.id && insideRange) {
          insideRange = false;
        } else if (row.id === anchorRow.id || row.id === target.id) {
          insideRange = true;
        }
      });

    bucket.value = result;
  };

export const attachGlobalDropzone = (
  uploadFiles: (upload: DocumentUploadOptions) => void,
  dragSource: DragSource,
  text: string,
  subText: string,
  folderUuid: string
) => {
  const textContainerEl = document.createElement('div');
  const textEl = document.createElement('span');
  const subTextEl = document.createElement('span');
  const popoverEl = document.createElement('div');
  const explorer = document.querySelector(
    '.documentExplorer'
  ) as HTMLDivElement;

  popoverEl.popover = 'auto';
  popoverEl.style.background = 'transparent';
  popoverEl.style.width = '100%';
  popoverEl.style.height = '100%';
  popoverEl.style.border = '6px dashed gray';
  popoverEl.style.opacity = '0';
  popoverEl.style.transition = 'opacity 0.4s allow-discrete';

  textContainerEl.style.display = 'flex';
  textContainerEl.style.justifyContent = 'center';
  textContainerEl.style.alignItems = 'center';
  textContainerEl.style.height = '86%';
  textContainerEl.style.flexDirection = 'column';

  textEl.innerText = text;
  textEl.style.fontSize = '52px';
  textEl.style.color = '#0000008f';

  subTextEl.innerText = subText;
  subTextEl.style.color = '#0000008f';
  subTextEl.style.marginTop = '20px';

  const showPopover = (ev: DragEvent) => {
    ev.preventDefault();
    if (dragSource[0] !== 'tableItem' && ev.dataTransfer?.items.length) {
      explorer.style.opacity = '0.25';
      popoverEl.showPopover();
      popoverEl.style.opacity = '1';
    }
  };

  const upload = (ev: DragEvent) => {
    if (dragSource[0] !== 'tableItem' && ev.dataTransfer?.items.length) {
      ev.preventDefault();
      popoverEl.style.opacity = '0';
      explorer.style.opacity = '1';
      popoverEl.hidePopover();
      uploadFiles({
        items: Array.from(ev.dataTransfer.items),
        targetUuid: folderUuid,
      });
    }
  };

  const hidePopover = (ev: DragEvent) => {
    if (
      dragSource[0] !== 'tableItem' &&
      ev.dataTransfer?.items.length &&
      (ev.clientX < 0 || ev.clientY < 0)
    ) {
      explorer.style.opacity = '1';
      popoverEl.style.opacity = '0';
      popoverEl.hidePopover();
    }
  };

  document.body.addEventListener('dragover', showPopover);
  document.body.addEventListener('dragleave', hidePopover);
  document.body.addEventListener('drop', upload);

  popoverEl.appendChild(textContainerEl);
  textContainerEl.appendChild(textEl);
  textContainerEl.appendChild(subTextEl);
  document.body.appendChild(popoverEl);

  return () => {
    document.body.removeEventListener('dragover', showPopover);
    document.body.removeEventListener('dragleave', hidePopover);
    document.body.removeEventListener('drop', upload);
    popoverEl.remove();
  };
};

export const getRowIdsMatchingFilter = (
  data: DocumentsTreeType | undefined,
  filterString: string
) => {
  let filteredIds: string[] = [];
  const diacriticsRegex = /[\u0300-\u036f]/g;

  data?.forEach(row => {
    const normalizedTitle = row.title
      .normalize('NFD')
      .replace(diacriticsRegex, '')
      .toLowerCase();

    const filterMatch = filterString
      ? filterString
          .normalize('NFD')
          .replace(diacriticsRegex, '')
          .toLowerCase()
          .split(' ')
          .every(word => normalizedTitle.includes(word))
      : true;

    if (filterMatch) {
      filteredIds.push(row.id);
      row.hierarchy.slice(0, row.hierarchy.length - 1).forEach(par => {
        filteredIds.push(par.id);
      });
    }
  });

  return filteredIds;
};

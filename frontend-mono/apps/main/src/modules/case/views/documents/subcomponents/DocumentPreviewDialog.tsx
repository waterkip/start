// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { DocumentPreviewModal } from '@zaaksysteem/common/src/components/DocumentPreview';
import { previewDialog } from '../Documents.signals';

export const DocumentPreviewDialog = () => {
  return (
    <DocumentPreviewModal
      {...previewDialog.value}
      open={previewDialog.value.opened}
      onClose={() => {
        previewDialog.value = { opened: false };
        window.top?.postMessage({ type: 'exitFullscreen' });
      }}
    />
  );
};

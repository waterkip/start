// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as React from 'react';
import { useTranslation } from 'react-i18next';
import { Box, Typography } from '@mui/material';
import FolderOpen from '@mui/icons-material/Folder';
import ChevronRight from '@mui/icons-material/ChevronRight';
import { useCaseObjectQuery } from '../../Case.library';
import { DocumentVersionsDialog } from './subcomponents/DocumentVersionsDialog';
import { DocumentFileIcon } from './subcomponents/DocumentFileIcon';
import {
  CheckboxRow,
  ExtensionRow,
  TitleRow,
  VersionRow,
} from './subcomponents/Rows';
import { HiddenFileUpload } from './subcomponents/HiddenFileUpload';
import { UploadButton } from './subcomponents/UploadButton';
import { CreateNewFolderButton } from './subcomponents/CreateNewFolderButton';
import { SearchDocuments } from './subcomponents/SearchDocuments';
import { TrashbinButton } from './subcomponents/TrashbinButton';
import { GoToV1Button } from './subcomponents/GoToV1Button';
import { AcceptDocument, BulkAccept } from './actions/AcceptDocuments';
import { RejectDocument, RejectDocumentDialog } from './actions/RejectDocument';
import { RenameFolder } from './actions/RenameFolder';
import { Anonymize } from './actions/Anonymize';
import { SendMessage } from './actions/SendMessage';
import { ValidsignAction } from './actions/ValidsignAction';
import { MergeAction } from './actions/MergeAction';
import { EditDocument } from './actions/EditDocument';
import { ReplaceDocument } from './actions/ReplaceDocument';
import { EditDocumentProperties } from './actions/EditDocumentProperties';
import { DocumentsDownload } from './actions/DocumentsDownload';
import { DocumentsRestore } from './actions/DocumentsRestore';
import { PermanentDelete } from './actions/PermanentDelete';
import { CopyItems } from './actions/CopyItems';
import { SoftDeleteItems } from './actions/SoftDeleteItems';
import { DocumentPreviewDialog } from './subcomponents/DocumentPreviewDialog';
import {
  useApplyLabelsMutation,
  useDocumentsQuery,
  useRelocationMutation,
  useUploadFilesMutation,
} from './Documents.requests';
import {
  currentMode,
  documentContext,
  documentFilter,
  selectedAcceptedRowIds,
  selectedPendingRowIds,
  trashBinView,
} from './Documents.signals';
import { ActiveFiltering, DragSource } from './Documents.types';
import './style.css';
import { dragGhost } from './subcomponents/dragGhost';
import { LabelsContainer } from './subcomponents/LabelsContainer';
import {
  adjustGridStyling,
  attachGlobalDropzone,
  getIdsOfLockedFiles,
  getRowIdsMatchingFilter,
  rowSorter,
  toggleRangeSelection,
  toggleRowSelection,
  useSelectedAcceptedRows,
  useSelectedPendingRows,
} from './Documents.library';
import { DocumentsTableHeader } from './subcomponents/DocumentsTableHeader';
import { DocumentContextMenu } from './subcomponents/DocumentContextMenu';
import { RegularSignAction } from './actions/RegularSignAction';

adjustGridStyling();

/* eslint-disable complexity */
/* eslint-disable react/jsx-key */
const CaseDocuments = () => {
  const [t] = useTranslation('case');
  const { data } = useDocumentsQuery();
  const caseObject = useCaseObjectQuery();
  const [expandedIds, setExpandedRowsIds] = React.useState<string[]>([]);
  const [draggedOver, setDraggedOver] = React.useState('');
  const [dragSource, setDragSource] = React.useState(['', ''] as DragSource);

  const { mutateAsync: relocate } = useRelocationMutation();
  const { mutate: applyLabels } = useApplyLabelsMutation();
  const { mutateAsync: uploadFiles } = useUploadFilesMutation();

  const canEdit =
    Boolean(caseObject.data?.canEdit && caseObject.data?.assignee) ||
    caseObject.data?.canManage;

  React.useEffect(() => {
    //@ts-ignore
    document.querySelector('#root').style.height = 'initial';
  }, []);

  const lockedIds = data ? getIdsOfLockedFiles(data) : [];
  const expandedRows = data?.filter(row => expandedIds.includes(row.id)) || [];
  const filteredIds = getRowIdsMatchingFilter(data, documentFilter.value);

  const [acceptedFiltering, setAcceptedFiltering] = React.useState({
    target: 'title',
    desc: true,
    untouched: true,
  } as ActiveFiltering);

  const [pendingFiltering, setPendingFiltering] = React.useState({
    target: 'title',
    desc: true,
    untouched: true,
  } as ActiveFiltering);

  const listedRows =
    data?.filter(
      row =>
        filteredIds.includes(row.id) &&
        (trashBinView.value ? row.inTrash === true : !row.inTrash)
    ) || [];

  const listedPendingRows = rowSorter(
    listedRows.filter(row => !row.accepted),
    pendingFiltering
  );
  const listedPendingInteractableRows = listedPendingRows.filter(
    row => !row.locked
  );
  const listedAcceptedRows = rowSorter(
    listedRows.filter(row => row.accepted),
    acceptedFiltering
  );
  const listedAcceptedInteractableRows = listedAcceptedRows.filter(
    row => !row.locked
  );
  const displayedAcceptedRows = listedAcceptedRows.filter(row => {
    const inRootFolder = row.hierarchy.length < 2;
    const underExpandedFolder = expandedRows.find(
      er => er.title === row.hierarchy.at(-2)?.title
    );

    return inRootFolder || underExpandedFolder;
  });

  const [selectedAcceptedRows, selections] = useSelectedAcceptedRows();
  const [selectedPendingRows, pendingSelections] = useSelectedPendingRows();

  const toggleRangeSelectionAcceptedFn = toggleRangeSelection(
    selectedAcceptedRows,
    listedRows,
    selectedAcceptedRowIds
  );
  const toggleRowSelectionAcceptedFn = toggleRowSelection(
    selectedAcceptedRows,
    listedRows,
    lockedIds,
    selectedAcceptedRowIds
  );

  const toggleRangeSelectionPendingFn = toggleRangeSelection(
    selectedPendingRows,
    listedRows,
    selectedPendingRowIds
  );
  const toggleRowSelectionPendingFn = toggleRowSelection(
    selectedPendingRows,
    listedRows,
    lockedIds,
    selectedPendingRowIds
  );

  React.useEffect(() => {
    //@ts-ignore
    document.querySelector('#root').style.height = 'initial';
  }, []);

  const firstSelectedFolder = selectedAcceptedRows.filter(
    row => row.type === 'folder'
  )[0];

  React.useEffect(
    () =>
      canEdit
        ? attachGlobalDropzone(
            uploadFiles,
            dragSource,
            t('documentTableActions.uploadFile'),
            firstSelectedFolder
              ? t('documentTableActions.uploadFileToFolder', {
                  folderTitle: firstSelectedFolder.title,
                })
              : t('documentTableActions.uploadFileExt'),
            firstSelectedFolder?.uuid
          )
        : () => {},
    [dragSource[0], uploadFiles, canEdit, firstSelectedFolder?.id]
  );

  return (
    <div
      onMouseEnter={ev =>
        ev.buttons === 0 && dragSource[0] !== '' && setDragSource(['', ''])
      }
      className="documentExplorer"
    >
      <DocumentContextMenu />
      <Box
        sx={{
          display: 'flex',
          justifyContent: 'space-between',
          paddingRight: '8px',
          margin: '10px 0 15px 0',
        }}
      >
        <HiddenFileUpload />
        <Box sx={{ display: 'inline-flex', alignItems: 'center' }}>
          <Typography
            sx={{ color: '#086797', marginRight: '2px', fontSize: '1.8rem' }}
            variant="h2"
          >
            {t('documentTableHeaders.moduleName')}
          </Typography>
          {canEdit && <UploadButton />}
          {canEdit && <CreateNewFolderButton />}
        </Box>
        <Box sx={{ display: 'inline-flex', alignItems: 'center' }}>
          <SearchDocuments />
          <TrashbinButton />
          <GoToV1Button />
        </Box>
      </Box>
      {!trashBinView.value && listedPendingRows.length ? (
        <table className="datagrid datagrid-pending">
          <DocumentsTableHeader
            showActionsColumn={canEdit}
            dragSource={['', '']}
            setFiltering={setPendingFiltering}
            activeFiltering={pendingFiltering}
            allRows={listedPendingInteractableRows}
            useSelectedRows={useSelectedPendingRows}
            rowIsPartOfTheTable={row => Boolean(row.accepted)}
            actions={
              <>
                {!trashBinView.value && (
                  <EditDocumentProperties
                    useSelectedRows={useSelectedPendingRows}
                  />
                )}
                <DocumentsDownload useSelectedRows={useSelectedPendingRows} />
                {!trashBinView.value && (
                  <CopyItems useSelectedRows={useSelectedPendingRows} />
                )}
                {!trashBinView.value && canEdit && pendingSelections.single && (
                  <RejectDocument />
                )}
                {!trashBinView.value && canEdit && <BulkAccept />}
              </>
            }
          />
          <tbody className="datagrid-body">
            {listedPendingRows.map(row => {
              return (
                <tr
                  className={[
                    'row',
                    'row-pending',
                    selectedPendingRows.find(sr => sr.id === row.id) &&
                      'row-selected',
                    row.archiveable === false && 'row-nonarchiveable',
                  ]
                    .filter(Boolean)
                    .join(' ')}
                  key={row.id}
                  onClick={ev => {
                    if (
                      Array.from(document.querySelectorAll('button')).every(
                        buttonEl => !buttonEl.contains(ev.target as any)
                      ) &&
                      document
                        .querySelector('.datagrid-pending')
                        ?.contains(ev.target as any)
                    ) {
                      (ev.shiftKey &&
                        selectedPendingRowIds.value.length &&
                        !selectedPendingRowIds.value.includes(row.id)
                        ? toggleRangeSelectionPendingFn
                        : toggleRowSelectionPendingFn)(row);
                    }
                  }}
                  onContextMenu={ev => {
                    ev.preventDefault();
                    documentContext.value = {
                      row,
                      posX: ev.clientX,
                      posY: ev.clientY,
                      opened: true,
                    };
                  }}
                >
                  <CheckboxRow
                    row={row}
                    selectedRows={selectedPendingRows}
                    disabled={currentMode.value === 'relocating'}
                  />
                  <td style={{ display: 'flex', width: '100%' }}>
                    <Box sx={{ ml: '13px' }} className="row-icon">
                      <DocumentFileIcon
                        uuid={row.uuid}
                        extension={row.extension || ''}
                      />
                    </Box>
                    <TitleRow row={row} />
                    <ExtensionRow row={row} />
                    <td className="row-origin">{row.origin}</td>
                    <td className="row-origindate">{row.originDate}</td>
                    <td className="row-version" />
                    <td className="row-created">{row.createdAt}</td>
                    <td className="row-author light">
                      <span className="ellipsis">{row.createdBy}</span>
                    </td>
                    {canEdit && (
                      <td className="row-actions">
                        <AcceptDocument uuid={row.uuid} title={row.title} />
                        <RejectDocument file={row} />
                        <EditDocumentProperties
                          useSelectedRows={useSelectedPendingRows}
                          row={row}
                        />
                      </td>
                    )}
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      ) : null}
      {!trashBinView.value ? (
        <LabelsContainer setDragSource={setDragSource} />
      ) : null}
      <table
        className="datagrid datagrid-accepted"
        onDragEnter={ev => {
          if (!dragSource[0]) {
            ev.preventDefault();
            setDragSource(['system']);
          }
        }}
        onDragEnd={ev => {
          ev.preventDefault();
          setDragSource(['', '']);
        }}
      >
        <DocumentsTableHeader
          activeFiltering={acceptedFiltering}
          setFiltering={setAcceptedFiltering}
          dragSource={dragSource}
          allRows={listedAcceptedInteractableRows}
          useSelectedRows={useSelectedAcceptedRows}
          rowIsPartOfTheTable={row => Boolean(row.accepted)}
          actions={
            <>
              {!trashBinView.value && (
                <EditDocumentProperties
                  useSelectedRows={useSelectedAcceptedRows}
                />
              )}
              {!trashBinView.value && canEdit && <ReplaceDocument />}
              {!trashBinView.value && canEdit && <EditDocument />}
              {!trashBinView.value && canEdit && <SoftDeleteItems />}
              {!trashBinView.value && (
                <CopyItems useSelectedRows={useSelectedAcceptedRows} />
              )}
              {trashBinView.value && canEdit && caseObject.data?.canManage && (
                <PermanentDelete />
              )}
              {trashBinView.value && canEdit && <DocumentsRestore />}
              <DocumentsDownload useSelectedRows={useSelectedAcceptedRows} />
              {!trashBinView.value && <SendMessage />}
              {!trashBinView.value && canEdit && <Anonymize />}
              {!trashBinView.value && canEdit && <RegularSignAction />}
              {!trashBinView.value && <ValidsignAction />}
              {!trashBinView.value && canEdit && selections.multiple && (
                <MergeAction />
              )}
              {!trashBinView.value && canEdit && selections.hasFolders && (
                <RenameFolder />
              )}
            </>
          }
        />

        <tbody className="datagrid-body">
          {displayedAcceptedRows.map(row => {
            const folderWithContents =
              row.type === 'folder' &&
              (row.children?.filter(rowId => {
                const child = data?.find(row => row.id === rowId);
                return child?.accepted && !child.inTrash;
              }).length || 0) > 0;
            const expand = () => {
              row.type === 'folder' &&
                row.children?.length &&
                setExpandedRowsIds(
                  (expandedRows.find(er => er.id === row.id)
                    ? expandedRows.filter(
                        er =>
                          er.id !== row.id &&
                          !er.hierarchy.find(
                            parent => parent.title === row.title
                          )
                      )
                    : [...expandedRows, row]
                  ).map(row => row.id)
                );
            };

            return (
              <tr
                draggable={!trashBinView.value && !row.locked}
                onDragStart={ev => {
                  setDragSource(['tableItem']);
                  ev.dataTransfer.dropEffect = 'move';
                  if (!selectedAcceptedRowIds.value.includes(row.id)) {
                    toggleRowSelectionAcceptedFn(row);
                  }
                  const folders = [] as string[];
                  const topLevelItemIds = [] as string[];

                  selectedAcceptedRowIds.value.forEach(rowId => {
                    const target = listedRows.find(row => row.id === rowId);

                    if (target) {
                      !folders.some(folderName =>
                        Boolean(
                          target.hierarchy.find(
                            parent => parent.title === folderName
                          )
                        )
                      ) && topLevelItemIds.push(target.id);
                      target.type === 'folder' && folders.push(target.title);
                    }
                  });
                  topLevelItemIds.forEach((id, ix) => {
                    const orig = document.getElementById(
                      'draggingContainer-' + id
                    );
                    if (orig) {
                      const transferRow = orig.cloneNode() as HTMLElement;
                      // We only want to drag shadow of file icon and name
                      transferRow.appendChild(orig.children[0].cloneNode(true));
                      transferRow.appendChild(orig.children[1].cloneNode());
                      transferRow.children[1].appendChild(
                        orig.children[1].children[0].cloneNode(true)
                      );
                      ix === 0 &&
                        (dragGhost.style.width = orig.clientWidth + 'px');
                      transferRow.style.position = 'absolute';
                      transferRow.style.top = ix * 12 + 'px';
                      transferRow.style.left = ix * 12 + 'px';
                      const xyz = document.createElement('div');
                      xyz.style.width = '29px';
                      transferRow.prepend(xyz);
                      dragGhost.appendChild(transferRow);
                    }
                  });

                  ev.dataTransfer.setDragImage(dragGhost, 0, 0);
                }}
                onDragEnd={(ev: any) => {
                  dragGhost.innerHTML = '';
                  setDraggedOver('');
                  setDragSource(['', '']);
                }}
                onDragOver={(ev: any) => {
                  if (
                    (!selectedAcceptedRowIds.value.includes(row.id) &&
                      dragSource[0] === 'tableItem' &&
                      row.type === 'folder') ||
                    (dragSource[0] === 'label' && row.type === 'file')
                  ) {
                    ev.preventDefault();
                    setDraggedOver(row.id);
                  }
                }}
                onDragLeave={(ev: any) => {
                  ev.preventDefault();
                  setDraggedOver('');
                }}
                onDrop={ev => {
                  if (dragSource[0] === 'tableItem') {
                    row.type === 'folder' &&
                      selectedAcceptedRows.every(ar => ar.uuid !== row.uuid) &&
                      relocate({
                        destination: row.uuid,
                        rows: selectedAcceptedRows,
                      }).then(() => {
                        setExpandedRowsIds([...expandedIds, row.id]);
                      });
                  } else if (dragSource[0] === 'label' && row.type === 'file') {
                    applyLabels({
                      document_label_uuids: [dragSource[1]],
                      document_uuids: [row.uuid],
                    });
                  }

                  ev.preventDefault();
                  setDraggedOver('');
                  setDragSource(['', '']);
                }}
                onClick={ev => {
                  ev.shiftKey &&
                  selectedAcceptedRowIds.value.length &&
                  !selectedAcceptedRowIds.value.includes(row.id)
                    ? toggleRangeSelectionAcceptedFn(row)
                    : row.type === 'folder' &&
                        (ev.target as any)?.type !== 'checkbox'
                      ? expand()
                      : toggleRowSelectionAcceptedFn(row);
                }}
                onContextMenu={ev => {
                  ev.preventDefault();
                  documentContext.value = {
                    row,
                    posX: ev.clientX,
                    posY: ev.clientY,
                    opened: true,
                  };
                }}
                className={[
                  'row',
                  selectedAcceptedRows.find(sr => sr.id === row.id) &&
                    'row-selected',
                  row.archiveable === false && 'row-nonarchiveable',
                  draggedOver === row.id && 'row-dragover',
                ]
                  .filter(Boolean)
                  .join(' ')}
                key={row.id}
              >
                <CheckboxRow
                  row={row}
                  selectedRows={selectedAcceptedRows}
                  disabled={currentMode.value === 'relocating'}
                />
                <Box
                  sx={{
                    ml: (row.hierarchy.length - 1) * 4,
                  }}
                  className="row-expander"
                  onClick={expand}
                >
                  {folderWithContents && (
                    <button
                      style={{
                        margin: 0,
                        padding: 0,
                        border: 0,
                        background: 0,
                        display: 'flex',
                      }}
                      name="expander"
                      type="button"
                    >
                      <ChevronRight
                        sx={{
                          marginLeft: '-5px',
                          opacity: 0.5,
                          transform: expandedRows.find(er => er.id === row.id)
                            ? 'rotate(90deg)'
                            : 'none',
                          ':hover': {
                            opacity: 1,
                          },
                        }}
                        fontSize="small"
                      />
                    </button>
                  )}
                </Box>
                <Box
                  id={'draggingContainer-' + row.id}
                  sx={{ display: 'flex', width: '100%' }}
                >
                  <Box onClick={expand} className="row-icon">
                    {row.type === 'folder' ? (
                      <FolderOpen sx={{ color: '#cdcd06' }} />
                    ) : (
                      <DocumentFileIcon
                        uuid={row.uuid}
                        extension={row.extension || ''}
                      />
                    )}
                  </Box>
                  <TitleRow row={row} onTopClick={expand} />

                  {row.type === 'file' && (
                    <>
                      <ExtensionRow row={row} />
                      <td className="row-origin">{row.origin}</td>
                      <td className="row-origindate">{row.originDate}</td>
                    </>
                  )}
                  {row.type === 'file' && (
                    <td className="row-version">
                      <VersionRow row={row} />
                    </td>
                  )}
                  {row.type === 'file' && (
                    <>
                      <td className="row-created">{row.createdAt}</td>
                      <td className="row-author light">
                        <span className="ellipsis">{row.createdBy}</span>
                      </td>
                    </>
                  )}
                </Box>
              </tr>
            );
          })}
        </tbody>
      </table>
      <DocumentVersionsDialog canRestore={canEdit || false} />
      <DocumentPreviewDialog />
      <RejectDocumentDialog />
    </div>
  );
};

export default CaseDocuments;

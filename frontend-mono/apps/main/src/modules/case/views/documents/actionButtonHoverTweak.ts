// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const actionButtonHoverTweak = {
  '&:hover': { background: 'rgb(0 0 0 / 8%)' },
};

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import useSession from '@zaaksysteem/common/src/hooks/useSession';
import { Dialog, DialogTitle } from '@mintlab/ui/App/Material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import { ChipButton } from '../ChipButton';
import { useSignerModuleQuery } from '../Documents.requests';
import { useSelectedAcceptedRows } from '../Documents.library';

/* eslint complexity: [2, 11] */
export const ValidsignAction = () => {
  const session = useSession();
  const [dialogOpened, setDialogOpened] = React.useState(false);
  const [t] = useTranslation('case');
  const [files, selections] = useSelectedAcceptedRows();
  const integrationActive =
    session.active_interfaces.includes('koppelapp_signer');
  const { data: signerModuleData } = useSignerModuleQuery(integrationActive);

  const qs = new URLSearchParams({
    documents: JSON.stringify(
      files.map(file => ({
        uuid: file.uuid,
        id: file.id,
      }))
    ),
    name: session.logged_in_user.given_name || '',
    lastName: session.logged_in_user.surname || '',
    email: session.logged_in_user.email || '',
  });

  return selections.single && selections.onlyFiles && integrationActive ? (
    <>
      {selections.onlyFiles ? (
        <ChipButton
          onClick={() => setDialogOpened(true)}
          icon="signature"
          title={signerModuleData?.label || ''}
        />
      ) : null}
      {dialogOpened && (
        <Dialog
          PaperProps={{ sx: { overflowY: 'visible' } }}
          onClose={() => setDialogOpened(false)}
          open={true}
          disableBackdropClick={true}
        >
          <DialogTitle
            icon="signature"
            onCloseClick={() => setDialogOpened(false)}
            elevated={true}
            title={(files.length > 1
              ? [files.length, t('documentTabDialogs.sign.titleMulti')]
              : [files[0].title, t('documentTabDialogs.sign.titleSingle')]
            ).join(' ')}
          />
          <DialogContent>
            <iframe
              title="validsign-dialog"
              style={{ width: '550px', height: '580px' }}
              src={[signerModuleData?.endpoint, qs].join('?')}
              frameBorder="0"
            />
          </DialogContent>
        </Dialog>
      )}
    </>
  ) : null;
};

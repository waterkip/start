// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import * as i18next from 'i18next';
import Button from '@mintlab/ui/App/Material/Button';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import { CustomObjectType } from '../../Relations.types';

export const getColumns = (
  classes: any,
  t: i18next.TFunction,
  saving: boolean,
  unrelate: (uuid: string) => void
) => {
  return [
    {
      name: 'type',
      width: 1,
      flexGrow: 1,
      // eslint-disable-next-line
      cellRenderer: ({ rowData }: { rowData: CustomObjectType }) => (
        <Tooltip title={rowData.type} noWrap={true}>
          <span>{rowData.type}</span>
        </Tooltip>
      ),
    },
    {
      name: 'name',
      width: 1,
      flexGrow: 1,
      // eslint-disable-next-line
      cellRenderer: ({ rowData }: { rowData: CustomObjectType }) => (
        <Tooltip title={rowData.name} noWrap={true}>
          <a target="_parent" href={`/main/object/${rowData.uuid}`}>
            {rowData.name}
          </a>
        </Tooltip>
      ),
    },
    {
      name: 'customFieldNames',
      width: 1,
      flexGrow: 1,
      // eslint-disable-next-line
      cellRenderer: ({
        rowData: { uuid, customFieldNames },
      }: {
        rowData: CustomObjectType;
      }) => (
        <div className={classes.customObjectsAttributes}>
          {customFieldNames.map(fieldName => {
            const fallback = t('customObjects.noAttributeRelation');

            return (
              <div key={fieldName} className={classes.customObjectsAttribute}>
                {!fieldName ? (
                  <>
                    <Tooltip title={fallback} noWrap={true}>
                      <span>{fallback}</span>
                    </Tooltip>

                    <Button
                      icon="close"
                      iconSize="extraSmall"
                      name="unrelateCustomObject"
                      disabled={saving}
                      action={() => unrelate(uuid)}
                    />
                  </>
                ) : (
                  <Tooltip title={fieldName} noWrap={true}>
                    <span>{fieldName}</span>
                  </Tooltip>
                )}
              </div>
            );
          })}
        </div>
      ),
    },
  ].map(row => ({
    ...row,
    label: t(`customObjects.columns.${row.name}`),
  }));
};

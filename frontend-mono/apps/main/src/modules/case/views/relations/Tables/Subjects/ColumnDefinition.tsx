// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import * as i18next from 'i18next';
import Button from '@mintlab/ui/App/Material/Button';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import { SubjectType } from '../../Relations.types';
import { canUnrelateContact } from './library';

type GetColumnsType = (
  t: i18next.TFunction,
  caseOpen: boolean,
  saving: boolean,
  startEdit: (subject: SubjectType) => void,
  unrelate: (relationUuid: string) => void
) => any;

type RowDataType = {
  rowData: SubjectType;
};

export const getColumns: GetColumnsType = (
  t,
  caseOpen,
  saving,
  startEdit,
  unrelate
) =>
  [
    {
      name: 'type',
      width: 1,
      flexGrow: 1,
      // eslint-disable-next-line
      cellRenderer: ({ rowData }: RowDataType) => {
        const type = t(`subjects.cells.type.${rowData.subject.type}`);
        return (
          <Tooltip title={type} noWrap={true}>
            <span>{type}</span>
          </Tooltip>
        );
      },
    },
    {
      name: 'name',
      width: 1,
      flexGrow: 1,
      // eslint-disable-next-line
      cellRenderer: ({ rowData }: RowDataType) => (
        <Tooltip title={rowData.subject.name} noWrap={true}>
          <a
            target="_parent"
            href={`/main/contact-view/${rowData.subject.type}/${rowData.subject.uuid}`}
          >
            {rowData.subject.name}
          </a>
        </Tooltip>
      ),
    },
    {
      name: 'role',
      width: 1,
      flexGrow: 1,
      // eslint-disable-next-line
      cellRenderer: ({ rowData }: RowDataType) => (
        <Tooltip title={rowData.role} noWrap={true}>
          <span>{rowData.role}</span>
        </Tooltip>
      ),
    },
    {
      name: 'authorized',
      width: 1,
      flexGrow: 1,
      // eslint-disable-next-line
      cellRenderer: ({ rowData }: RowDataType) =>
        rowData.authorized && <Icon>{iconNames.done}</Icon>,
    },
    ...(caseOpen
      ? [
          {
            name: 'edit',
            width: 70,
            minWidth: 70,
            // eslint-disable-next-line
            cellRenderer: ({ rowData }: RowDataType) =>
              canUnrelateContact(
                rowData.role,
                rowData.source_custom_field_magic_string
              ) && (
                <Button
                  name="editSubject"
                  disabled={saving}
                  action={() => startEdit(rowData)}
                  icon="edit"
                  iconSize="extraSmall"
                />
              ),
          },
          {
            name: 'delete',
            width: 70,
            minWidth: 70,
            // eslint-disable-next-line
            cellRenderer: ({ rowData }: RowDataType) =>
              canUnrelateContact(
                rowData.role,
                rowData.source_custom_field_magic_string
              ) && (
                <Button
                  name="subjectRemove"
                  icon="close"
                  iconSize="extraSmall"
                  disabled={saving}
                  action={() => unrelate(rowData.uuid)}
                />
              ),
          },
        ]
      : []),
  ].map(row => ({
    ...row,
    label: t(`subjects.columns.${row.name}`),
  }));

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import FormDialog from '@zaaksysteem/common/src/components/dialogs/FormDialog/FormDialog';
import { iconNames } from '@mintlab/ui/App/Material/Icon';
import { PlannedCaseType } from '../../Relations.types';
import { getFormDefinition, getRules } from './formDefinition';

type DialogPropsType = {
  isOpen: boolean;
  close: () => void;
  add: (formValues: any) => Promise<void>;
  edit: (formValues: any, plannedCase?: PlannedCaseType) => Promise<void>;
  plannedCase?: PlannedCaseType;
};

const Dialog: React.ComponentType<DialogPropsType> = ({
  isOpen,
  close,
  add,
  edit,
  plannedCase,
}) => {
  const [t] = useTranslation('caseRelations');
  const editing = Boolean(plannedCase);
  const mode = editing ? 'edit' : 'add';

  return (
    <FormDialog
      formDefinition={getFormDefinition(t as any, plannedCase) as any}
      title={t(`plannedCases.dialog.${mode}.title`)}
      icon={iconNames.work}
      onClose={close}
      scope="add-planned-cases"
      open={isOpen}
      onSubmit={editing ? formValues => edit(formValues, plannedCase) : add}
      saveLabel={t(`plannedCases.dialog.${mode}.submit`)}
      rules={getRules()}
    />
  );
};

export default Dialog;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles(({ typography }: Theme) => ({
  view: {
    padding: 20,
  },
  searchPanel: {
    maxWidth: 500,
    minWidth: 500,
  },
  searchWrapper: {
    display: 'flex',
    flexDirection: 'column',
    gap: 20,
    padding: 20,
  },
  resultsWrapper: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    gap: 20,
    overflow: 'hidden',
  },
  resultsAria: {
    opacity: 0,
    position: 'absolute',
  },
  placeholder: {
    padding: 20,
  },
  separator: {
    marginBottom: 20,
    fontWeight: typography.fontWeightBold,
  },
}));

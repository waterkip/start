// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { AnyFormDefinitionField } from '@zaaksysteem/common/src/components/form/types';
import { ContactType } from '../ContactSearch.types';

type GetFormDefinitionType = ({
  t,
  supportsSedulaNumber,
}: {
  t: i18next.TFunction;
  supportsSedulaNumber?: boolean;
}) => AnyFormDefinitionField[];

const getPersonFormDefinition: GetFormDefinitionType = ({
  t,
  supportsSedulaNumber,
}) => [
  {
    name: 'excludeAuthenticated',
    type: fieldTypes.CHECKBOX,
    label: t('search.person.excludeAuthenticated'),
    value: false,
  },
  {
    name: 'bsn',
    type: fieldTypes.TEXT,
    label: t('search.person.bsn'),
    value: '',
    manualRequired: true,
  },
  ...(supportsSedulaNumber
    ? [
        {
          name: 'or',
          type: fieldTypes.TEXT,
          value: '',
          isLabel: true,
        },
        {
          name: 'sedulaNumber',
          type: fieldTypes.TEXT,
          label: t('search.person.sedulaNumber'),
          value: '',
          manualRequired: true,
        },
      ]
    : []),
  {
    name: 'or',
    type: fieldTypes.TEXT,
    value: '',
    isLabel: true,
  },
  {
    name: 'zipcode',
    type: fieldTypes.TEXT,
    label: t('search.person.zipcode'),
    value: '',
    manualRequired: true,
  },
  {
    name: 'houseNumber',
    type: fieldTypes.NUMERIC,
    label: t('search.person.houseNumber'),
    value: '',
    manualRequired: true,
  },
  {
    name: 'or',
    type: fieldTypes.TEXT,
    value: '',
    isLabel: true,
  },
  {
    name: 'dateOfBirth',
    type: fieldTypes.DATEPICKER,
    label: t('search.person.dateOfBirth'),
    value: null,
    manualRequired: true,
  },
  {
    name: 'prefix',
    type: fieldTypes.TEXT,
    label: t('search.person.prefix'),
    value: '',
  },
  {
    name: 'familyName',
    type: fieldTypes.TEXT,
    label: t('search.person.familyName'),
    value: '',
    manualRequired: true,
  },
];

const getOrganizationFormDefinition: GetFormDefinitionType = ({ t }) => [
  {
    name: 'rsin',
    type: fieldTypes.TEXT,
    label: t('search.organization.rsin'),
    value: '',
  },
  {
    name: 'coc',
    type: fieldTypes.TEXT,
    label: t('search.organization.coc'),
    value: '',
  },
  {
    name: 'name',
    type: fieldTypes.TEXT,
    label: t('search.organization.name'),
    value: '',
  },
  {
    name: 'street',
    type: fieldTypes.TEXT,
    label: t('search.organization.street'),
    value: '',
  },
  {
    name: 'houseNumber',
    type: fieldTypes.NUMERIC,
    label: t('search.organization.houseNumber'),
    value: '',
  },
  {
    name: 'houseNumberLetter',
    type: fieldTypes.TEXT,
    label: t('search.organization.houseNumberLetter'),
    value: '',
  },
  {
    name: 'houseNumberSuffix',
    type: fieldTypes.TEXT,
    label: t('search.organization.houseNumberSuffix'),
    value: '',
  },
  {
    name: 'zipcode',
    type: fieldTypes.TEXT,
    label: t('search.organization.zipcode'),
    value: '',
  },
  {
    name: 'city',
    type: fieldTypes.TEXT,
    label: t('search.organization.city'),
    value: '',
  },
];

const getEmployeeFormDefinition: GetFormDefinitionType = ({ t }) => [
  {
    name: 'query',
    type: fieldTypes.TEXT,
    label: t('search.employee.query'),
    value: '',
    required: true,
  },
  {
    name: 'inactive',
    type: fieldTypes.CHECKBOX,
    label: t('search.employee.inactive'),
    value: false,
    suppressLabel: true,
  },
];

export const getFormDefinition = {
  person: getPersonFormDefinition,
  organization: getOrganizationFormDefinition,
  employee: getEmployeeFormDefinition,
};

type GetCanSubmitType = (
  type: ContactType,
  isValid: boolean,
  values: any
) => boolean;

/* eslint complexity: [2, 14] */
export const getCanSubmit: GetCanSubmitType = (type, isValid, values) => {
  if (type !== 'person') {
    return isValid;
  }

  const { bsn, sedulaNumber, zipcode, houseNumber, dateOfBirth, familyName } =
    values;

  const hasBsnValue = bsn.length === 8 || bsn.length === 9;
  const hasSedulaNumber = sedulaNumber?.length === 10;
  const hasZipcode = Boolean(zipcode);
  const hasHouseNumber = Boolean(houseNumber);
  const hasDateOfBirth = Boolean(dateOfBirth);
  const hasFamilyName = Boolean(familyName);

  if (values.excludeAuthenticated) {
    return (
      hasBsnValue ||
      hasSedulaNumber ||
      hasZipcode ||
      hasHouseNumber ||
      hasDateOfBirth ||
      hasFamilyName
    );
  }

  const hasAddressValue = hasZipcode && hasHouseNumber;
  // it should require both dateOfBirth and familyName
  // but not until we have fixed an eidas bug
  // that prevents importing persons with date-of-births
  // const hasOtherValue = hasDateOfBirth && hasFamilyName;
  const hasOtherValue = hasDateOfBirth || hasFamilyName;

  return hasBsnValue || hasSedulaNumber || hasAddressValue || hasOtherValue;
};

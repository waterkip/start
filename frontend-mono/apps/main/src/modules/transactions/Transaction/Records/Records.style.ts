// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useDataTableStyles = makeStyles(
  ({ typography, palette: { elephant } }: Theme) => ({
    wrapper: {
      flexGrow: 1,
      overflow: 'hidden',
      display: 'flex',
      flexDirection: 'column',
    },
    tableWrapper: {
      fontFamily: typography.fontFamily,
      display: 'block',
      margin: `20px 20px 0 20px`,
      overflowY: 'scroll',
      width: '100%',
    },
    loader: {
      position: 'absolute',
      bottom: 1,
      width: '100%',
    },
    record: {
      display: 'flex',
      flexDirection: 'row',
      gap: 5,
    },
    recordPreview: {
      textDecoration: 'none',
      color: elephant.main,
    },
  })
);

export const useTableStyles = makeStyles(
  ({ mintlab: { greyscale }, palette: { primary } }: Theme) => ({
    tableRow: {
      borderBottom: `1px solid ${greyscale.dark}`,
      alignItems: 'center',
      '&:hover': {
        backgroundColor: primary.lightest,
      },
    },
  })
);

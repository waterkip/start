// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { Routes, Route } from 'react-router-dom';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import { openServerError } from '@zaaksysteem/common/src/signals';
import { useQuery } from '@tanstack/react-query';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import { useTranslation } from 'react-i18next';
import { useAdminBanner } from '../../library/auth';
import Transaction from './Transaction/Transaction';
import locale from './Transactions.locale';
import { IntegrationType } from './Transactions.types';
import { getIntegrations } from './Transactions/Transactions.library';
import Transactions from './Transactions/Transactions';

type TransactionsModuleType = {};

const TransactionsModule: React.ComponentType<TransactionsModuleType> = () => {
  const [t] = useTranslation('transactions');
  const banner = useAdminBanner.system();

  const { data: integrationsData } = useQuery(
    ['integrations'],
    () => getIntegrations(),
    { onError: openServerError }
  );

  const everyIntegration: IntegrationType = {
    uuid: '',
    id: 0,
    value: '0',
    manualTypes: ['unsupported'],
    label: t('integration.all'),
  };
  const integrations = [everyIntegration, ...(integrationsData || [])];

  if (!integrationsData) return <Loader />;

  return (
    banner || (
      <>
        <Routes>
          <Route
            path={'/'}
            element={<Transactions integrations={integrations} />}
          />
          <Route
            path={':transactionUuid'}
            element={<Transaction integrations={integrations} />}
          />
        </Routes>
      </>
    )
  );
};

export default () => (
  <I18nResourceBundle resource={locale} namespace="transactions">
    <TransactionsModule />
  </I18nResourceBundle>
);

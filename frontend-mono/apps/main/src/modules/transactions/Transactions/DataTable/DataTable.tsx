// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { SortDirectionType } from 'react-virtualized';
import { useSelectionBehaviourReturnType } from '@zaaksysteem/common/src/hooks/useSelectionBehaviour';
import Pagination from '@mintlab/ui/App/Material/Pagination';
import SortableTable, {
  useSortableTableStyles,
} from '@mintlab/ui/App/Zaaksysteem/SortableTable';
import {
  TransactionsDataType,
  FiltersType,
  SetFiltersType,
  SelectedRowsType,
} from '../../Transactions.types';
import { useDataTableStyles, useTableStyles } from './DataTable.style';
import { getColumns } from './DataTable.library';

type DataTablePropsType = {
  data?: TransactionsDataType;
  filters: FiltersType;
  setFilters: SetFiltersType;
  selectedRows: SelectedRowsType;
  selectionProps: useSelectionBehaviourReturnType;
};

const DataTable: React.ComponentType<DataTablePropsType> = ({
  data,
  filters,
  setFilters,
  selectedRows,
  selectionProps,
}) => {
  const classes = useDataTableStyles();
  const tableStyles = useTableStyles();
  const sortableTableStyles = useSortableTableStyles();
  const [t] = useTranslation('transactions');

  const columns = getColumns(t as any, classes);
  const rows =
    data?.rows.map(row => ({
      ...row,
      selected: selectedRows.includes(row.id),
    })) || [];

  return (
    <div className={classes.wrapper}>
      <div className={classes.tableWrapper}>
        <div
          style={{
            flex: '1 1 auto',
            minWidth: columns.reduce(
              (acc, { minWidth }) => acc + minWidth + 10,
              60
            ),
            height: `calc(${rows.length} * 42px + 50px)`,
          }}
        >
          <SortableTable
            styles={{ ...sortableTableStyles, ...tableStyles }}
            rows={rows}
            //@ts-ignore
            columns={columns}
            rowHeight={42}
            loading={false}
            noRowsMessage={t('table.noResults')}
            sortDirectionDefault="DESC"
            sortInternal={false}
            sorting="column"
            onSort={(sortBy: string, sortDirection: SortDirectionType) => {
              setFilters({
                ...filters,
                page: 0,
                sortBy,
                sortDirection,
              });
            }}
            {...selectionProps}
          />
        </div>
      </div>

      <div className={classes.pagination}>
        <Pagination
          scope="transactions-filters-pagination"
          component={'div'}
          count={data?.count || 0}
          labelRowsPerPage={`${t('table.labelRowsPerPage')}:`}
          rowsPerPageOptions={[5, 10, 20, 50]}
          rowsPerPage={filters.numRows}
          changeRowsPerPage={(event: any) =>
            setFilters({
              ...filters,
              page: 0,
              numRows: event.target.value,
            })
          }
          page={filters.page}
          getNewPage={(page: number) => setFilters({ ...filters, page })}
          labelDisplayedRows={({ from, to, count }: any) =>
            `${from}-${to} ${t('common:of')} ${count}`
          }
        />
      </div>
    </div>
  );
};

export default DataTable;

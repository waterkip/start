// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import {
  GetIntegrationType,
  TransactionType,
  GetTransactionsType,
  PerformActionActionType,
  PerformManualTransactionType,
  TransactionResponseBodyType,
  IntegrationType,
} from '../Transactions.types';
import {
  fetchIntegrations,
  fetchTransactions,
  performAction,
  postManualTransaction,
} from './Transactions.requests';

export const getIntegrations: GetIntegrationType = async () => {
  const response = await fetchIntegrations();

  const integrations = response.data.map(
    ({ id, attributes: { name, manual_type, legacy_id } }) => ({
      uuid: id,
      id: legacy_id,
      value: id,
      manualTypes: manual_type.length
        ? manual_type
        : ['unsupported' as 'unsupported'],
      label: name,
    })
  );

  return integrations;
};

export const sortSupportedColumnDict = {
  created: 'me.date_created',
};

export const formatTransaction = (
  {
    id: uuid,
    attributes: {
      id,
      direction,
      external_id,
      created,
      next_retry,
      record_count,
      processed,
      error_fatal,
      error_count,
      error_message,
    },
    meta,
    relationships,
  }: TransactionResponseBodyType['data'],
  integrations?: IntegrationType[]
): TransactionType => {
  // logic copied over from perl
  const status =
    processed && !error_count && !error_fatal
      ? 'success'
      : next_retry && !processed && !error_fatal
        ? 'pending'
        : 'error';

  const integrationId = relationships.integration?.data?.id;
  const integration =
    integrations?.find(integration => integration.value === integrationId)
      ?.label || '';

  return {
    uuid,
    name: uuid,
    id,
    status,
    interface: integration,
    record: { preview: meta?.summary },
    direction,
    externalId: external_id,
    nextAttempt: next_retry,
    created,
    records: record_count,
    errors: error_count,
    errorMessage: error_message,
  };
};

export const getTransactions: GetTransactionsType = async (
  { keyword, integration, withError, numRows, page, sortDirection },
  integrations
) => {
  const params = {
    'filters[keyword]': keyword || undefined,
    'filters[relationship.integration.id]':
      integration.value === '0' ? undefined : integration.value,
    'filters[has_errors]': withError
      ? ('has_errors' as 'has_errors')
      : undefined,
    page_size: numRows,
    page: page + 1,
    sort:
      sortDirection === 'ASC'
        ? ('created' as 'created')
        : ('-created' as '-created'),
  };

  const response = await fetchTransactions(params);
  const count = response.meta?.total_results || 0;

  return {
    count,
    rows: response.data.map(transaction =>
      formatTransaction(transaction, integrations)
    ),
  };
};

export const performActionAction: PerformActionActionType = async (
  action,
  filters,
  selected,
  everythingSelected
) => {
  const data = {
    freeform_filter: filters.keyword,
    interface_id: Number(filters.integration.value) || undefined,
    'records.is_error': filters.withError ? 1 : '',
    selection_id: selected.map(Number),
    selection_type: everythingSelected ? 'all' : 'subset',
  };

  return await performAction(action, data);
};

/* eslint complexity: [2, 9] */
export const performManualTransaction: PerformManualTransactionType = async (
  values: any
) => {
  const {
    integration: { id },
    text,
    file: files,
    references,
  } = values;

  let data = {
    interface: id,
    input_data: text ? text : undefined,
    input_filestore_uuid: files?.map((file: { value: string }) => file.value),
    input_references: references?.map((reference: { value: string }) => ({
      reference: reference.value,
    })),
  };

  return await postManualTransaction(data);
};

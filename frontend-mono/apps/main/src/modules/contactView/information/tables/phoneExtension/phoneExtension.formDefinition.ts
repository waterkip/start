// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { AnyFormDefinitionField } from '@zaaksysteem/common/src/components/form/types';
import { SubjectType } from './../../../ContactView.types';

export const getFormDefinition = (
  t: i18next.TFunction,
  subject: SubjectType
): AnyFormDefinitionField[] => {
  return [
    {
      name: 'phoneExtension',
      type: fieldTypes.NUMERIC,
    },
  ].map(field => ({
    label: t(`phoneExtension.${field.name}`),
    value: subject[field.name],
    ...field,
  }));
};

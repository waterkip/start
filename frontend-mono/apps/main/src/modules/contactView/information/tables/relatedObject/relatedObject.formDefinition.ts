// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import generateCustomFieldFormDefinition from '@zaaksysteem/common/src/components/form/library/generateCustomFieldFormDefinition';
import { AnyFormDefinitionField } from '@zaaksysteem/common/src/components/form/types';
import { ObjectType, ObjectTypeType } from '../../Information.types';

export const getFormDefinition = (
  t: i18next.TFunction,
  object: ObjectType,
  objectType: ObjectTypeType
): AnyFormDefinitionField[] =>
  generateCustomFieldFormDefinition({
    customFieldsDefinition:
      objectType.attributes.custom_field_definition.custom_fields,
    customFieldsValues: object.attributes.custom_fields,
    readOnly: true,
    t: t as any,
  });

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import Table from '@mui/material/Table/Table';
import TableRow from '@mui/material/TableRow/TableRow';
import TableCell from '@mui/material/TableCell/TableCell';
import TableHead from '@mui/material/TableHead/TableHead';
import TableBody from '@mui/material/TableBody/TableBody';
import { useTranslation } from 'react-i18next';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { IconButton } from '@mui/material';
import { HostType } from '../ControlPanel.types';
import { useStyles } from '../ControlPanel.styles';

type HostsPropsType = {
  hosts: HostType[];
  setHostToEdit: (host: HostType) => void;
};

const Hosts: React.ComponentType<HostsPropsType> = ({
  hosts,
  setHostToEdit,
}) => {
  const classes = useStyles();
  const [t] = useTranslation('controlPanel');

  if (!hosts.length) {
    return <div className={classes.placeholder}>{t('hosts.placeholder')}</div>;
  }

  const sortedHosts = hosts.sort((hostA, hostB) =>
    hostA.fqdn < hostB.fqdn ? -1 : 1
  );

  return (
    <Table sx={{ tableLayout: 'fixed' }}>
      <TableHead>
        <TableRow>
          <TableCell>{t('hosts.fields.fqdn')}</TableCell>
          <TableCell>{t('hosts.fields.environment')}</TableCell>
          <TableCell className={classes.tableCellMedium}>
            {t('hosts.fields.template')}
          </TableCell>
          <TableCell className={classes.tableCellSmall}>
            {t('hosts.fields.sslCert')}
          </TableCell>
          <TableCell className={classes.tableCellSmall}>
            {t('hosts.fields.sslKey')}
          </TableCell>
          <TableCell className={classes.tableCellIcon} />
          <TableCell className={classes.tableCellIconWithPadding} />
        </TableRow>
      </TableHead>
      <TableBody>
        {sortedHosts.map((host, index) => {
          return (
            <TableRow key={`host-${index}`} className={classes.tableRow}>
              <TableCell>
                <Tooltip title={host.fqdn} noWrap={true}>
                  <span>{host.fqdn}</span>
                </Tooltip>
              </TableCell>
              <TableCell>
                <Tooltip title={host.environment?.label || ''} noWrap={true}>
                  <span>{host.environment?.label || ' - '}</span>
                </Tooltip>
              </TableCell>
              <TableCell className={classes.tableCellMedium}>
                <Tooltip title={host.template} noWrap={true}>
                  <span>{host.template || ' - '}</span>
                </Tooltip>
              </TableCell>
              <TableCell className={classes.tableCellSmall}>
                {host.sslCert ? (
                  <Icon size="small">{iconNames.check}</Icon>
                ) : null}
              </TableCell>
              <TableCell className={classes.tableCellSmall}>
                {host.sslKey ? (
                  <Icon size="small">{iconNames.check}</Icon>
                ) : null}
              </TableCell>
              <TableCell className={classes.tableCellIcon} />
              <TableCell className={classes.tableCellIconWithPadding}>
                <Tooltip title={t('common:verbs.edit')}>
                  <IconButton onClick={() => setHostToEdit(host)} size="small">
                    <Icon size="small">{iconNames.edit}</Icon>
                  </IconButton>
                </Tooltip>
              </TableCell>
            </TableRow>
          );
        })}
      </TableBody>
    </Table>
  );
};

export default Hosts;

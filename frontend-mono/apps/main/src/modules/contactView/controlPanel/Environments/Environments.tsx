// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import Table from '@mui/material/Table/Table';
import TableRow from '@mui/material/TableRow/TableRow';
import TableCell from '@mui/material/TableCell/TableCell';
import TableHead from '@mui/material/TableHead/TableHead';
import TableBody from '@mui/material/TableBody/TableBody';
import { useTranslation } from 'react-i18next';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { IconButton } from '@mui/material';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import Button from '@mintlab/ui/App/Material/Button';
import DropdownMenu, {
  DropdownMenuList,
} from '@mintlab/ui/App/Zaaksysteem/DropdownMenu';
import classNames from 'classnames';
import { openSnackbar } from '@zaaksysteem/common/src/signals';
import { ControlPanelType, EnvironmentType } from '../ControlPanel.types';
import { useStyles } from '../ControlPanel.styles';
import { CONTROL_PANEL_DATA, otapTypes } from '../ControlPanel.constants';
import EnvironmentDetailsDialog from '../Dialogs/EnvironmentDetails';
import { saveEnvironment } from '../ControlPanel.requests';
import { queryClient } from '../../../../queryClient';

type EnvironmentsPropsType = {
  controlPanel: ControlPanelType;
  environments: EnvironmentType[];
  setEnvironmentToEdit: (host: EnvironmentType) => void;
};

const Environments: React.ComponentType<EnvironmentsPropsType> = ({
  controlPanel,
  environments,
  setEnvironmentToEdit,
}) => {
  const classes = useStyles();
  const [t] = useTranslation('controlPanel');

  const [environmentToView, setEnvironmentToView] =
    useState<EnvironmentType | null>(null);

  if (!environments.length) {
    return (
      <div className={classes.placeholder}>{t('environments.placeholder')}</div>
    );
  }

  const sortedEnvironments = environments.sort((envA, envB) =>
    otapTypes.indexOf(envA.otap) < otapTypes.indexOf(envB.otap) ? -1 : 1
  );

  const save = async (
    type: 'disabled' | 'protected',
    value: boolean,
    environment: EnvironmentType
  ) => {
    await saveEnvironment(controlPanel, { [type]: value }, environment);

    queryClient.invalidateQueries([CONTROL_PANEL_DATA]);

    const activated = value ? 'activated' : 'deactivated';
    const verb = t(`common:verbs.${activated}`).toLowerCase();
    openSnackbar(t(`environments.snacks.${type}`, { verb }));
  };

  return (
    <Table sx={{ tableLayout: 'fixed' }}>
      <TableHead>
        <TableRow>
          <TableCell className={classes.tableCellSmall}>
            {t('environments.fields.otap')}
          </TableCell>
          <TableCell>{t('environments.fields.label')}</TableCell>
          <TableCell>{t('environments.fields.fqdn')}</TableCell>
          <TableCell>{t('environments.fields.hosts')}</TableCell>
          <TableCell className={classes.tableCellMedium}>
            {t(`environments.fields.template`)}
          </TableCell>
          <TableCell className={classes.tableCellIcon} />
          <TableCell className={classes.tableCellIconWithPadding} />
        </TableRow>
      </TableHead>
      <TableBody>
        {sortedEnvironments.map((environment, index) => {
          return (
            <TableRow
              key={`environment-${index}`}
              className={classNames(classes.tableRow, {
                [classes.inactive]: environment.status === 'disabled',
              })}
            >
              <TableCell>
                <Tooltip
                  title={t(`environments.otapTypes.${environment.otap}`)}
                >
                  <span>{t(`environments.otapTypes.${environment.otap}`)}</span>
                </Tooltip>
              </TableCell>
              <TableCell>
                <Tooltip title={environment.label} noWrap={true}>
                  <span>{environment.label}</span>
                </Tooltip>
              </TableCell>
              <TableCell>
                <Tooltip title={environment.fqdn} noWrap={true}>
                  <a
                    href={`/auth/token/remote_login?instance_id=${environment.uuid}`}
                    target="_blank"
                    rel="noreferrer"
                  >
                    {environment.fqdn}
                  </a>
                </Tooltip>
              </TableCell>
              <TableCell>
                {environment.hosts.map((host, index) => (
                  <Tooltip
                    key={`environment-host-${index}`}
                    title={host.fqdn}
                    noWrap={true}
                  >
                    <a
                      href={`/auth/token/remote_login?instance_id=${environment.uuid}&host_id=${host.uuid}`}
                      target="_blank"
                      rel="noreferrer"
                    >
                      {host.fqdn}
                    </a>
                  </Tooltip>
                ))}
              </TableCell>
              <TableCell className={classes.tableCellMedium}>
                <Tooltip title={environment.template} noWrap={true}>
                  <span>{environment.template}</span>
                </Tooltip>
              </TableCell>
              <TableCell className={classes.tableCellIcon}>
                <DropdownMenu
                  trigger={
                    <Button
                      icon="more_vert"
                      name="advancedActions"
                      iconSize="small"
                    />
                  }
                >
                  <DropdownMenuList
                    items={[
                      {
                        action: () => save('disabled', false, environment),
                        label: t('common:verbs.activate'),
                        hidden: environment.status !== 'disabled',
                      },
                      {
                        action: () => save('disabled', true, environment),
                        label: t('common:verbs.deactivate'),
                        hidden: environment.status !== 'active',
                      },
                      {
                        action: () => save('protected', true, environment),
                        label: t('environments.actions.protect'),
                        hidden: Boolean(environment.protected),
                      },
                      {
                        action: () => save('protected', false, environment),
                        label: t('environments.actions.unprotect'),
                        hidden: !environment.protected,
                      },
                      {
                        action: () => setEnvironmentToView(environment),
                        label: t('environments.actions.details'),
                      },
                    ]}
                  />
                </DropdownMenu>
              </TableCell>
              <TableCell className={classes.tableCellIconWithPadding}>
                <Tooltip title={t('common:verbs.edit')}>
                  <IconButton onClick={() => setEnvironmentToEdit(environment)}>
                    <Icon size="small">{iconNames.edit}</Icon>
                  </IconButton>
                </Tooltip>
              </TableCell>
            </TableRow>
          );
        })}
      </TableBody>
      {environmentToView && (
        <EnvironmentDetailsDialog
          environment={environmentToView}
          onClose={() => setEnvironmentToView(null)}
          open={Boolean(environmentToView)}
        />
      )}
    </Table>
  );
};

export default Environments;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export default {
  nl: {
    setup: {
      intro:
        'Om omgevingen voor deze organisatie te beheren, moet er eerst een keuze gemaakt worden wat voor type omgevingen bij deze organisatie horen.',
    },
    controlPanel: {
      actions: {
        details: 'Details bekijken',
      },
      details: {
        template: 'Template',
        shortname: 'Short name',
        customerType: 'Type',
        readOnly: 'Read only',
        diskspace: 'Diskspace',
      },
    },
    environment: 'Omgeving',
    environments: {
      title: 'Omgevingen',
      placeholder: 'Er zijn geen omgevingen aanwezig.',
      fields: {
        uuid: 'UUID',
        label: 'Titel',
        fqdn: 'Webadres',
        hosts: 'Hosts',
        template: 'Template',
        otap: 'Doel',
        customerType: 'Type cloud',
        softwareVersion: 'Softwareversie',
        fallbackUrl: 'Maintenance URL',
        provisionedOn: 'Datum provisioning',
        servicesDomain: '"Client Side Cert"-domein',
        apiDomain: 'API domein',
        database: 'Database',
        databaseHost: 'Database host',
        filestore: 'Storage Bucket',
        freeformReference: 'Vrije referentie',
        diskspace: 'Used diskspace (GB)',
        password: 'Wachtwoord',
        oidcOrganizationId: 'OIDC organisatie id',
      },
      placeholders: {
        template: 'Selecteer een template…',
      },
      customerTypes: {
        government: 'Overheidscloud',
        commercial: 'Commerciële cloud',
        lab: 'Lab cloud',
        development: 'Development cloud',
        staging: 'Staging cloud',
        acceptance: 'Acceptatie cloud',
        testing: 'Test cloud',
        preprod: 'Preproduction cloud',
        production: 'Production cloud',
      },
      otapTypes: {
        production: 'Productie',
        accept: 'Acceptatie',
        development: 'Ontwikkeling',
        testing: 'Test',
      },
      softwareVersions: {
        master: 'Productie',
      },
      actions: {
        details: 'Details bekijken',
        protect: 'Bescherming activeren',
        unprotect: 'Bescherming deactiveren',
      },
      snacks: {
        create: 'De omgeving is aangemaakt.',
        update: 'De omgeving is bewerkt.',
        disabled: 'De omgeving is {{verb}}',
        protected: 'De bescherming van de omgeving is {{verb}}',
      },
    },
    host: 'Host',
    hosts: {
      title: 'Hosts',
      placeholder: 'Er zijn geen hosts aanwezig.',
      create: 'Aanmaken',
      snacks: {
        create: 'De host is aangemaakt.',
        update: 'De host is bewerkt.',
      },
      fields: {
        label: 'Titel',
        fqdn: 'Host',
        sslCert: 'Certificaat',
        sslKey: 'Private Key',
        template: 'Template',
        environment: 'Omgeving',
      },
      placeholders: {
        fqdn: 'voorbeeld.domein.nl',
        template: 'Selecteer een template…',
        environment: 'Selecteer een omgeving…',
      },
    },
  },
};

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { buildUrl } from '@mintlab/kitchen-sink/source';
import { request } from '@zaaksysteem/common/src/library/request/request';
import {
  FindEnvironmentType,
  FormatEnvironmentType,
  FormatHostType,
  GetControlPanelType,
  GetDataType,
  SetCustomerTypeType,
  SaveEnvironmentType,
  SaveHostType,
} from './ControlPanel.types';

export const getControlPanel: GetControlPanelType = async uuid => {
  const idUrl = `/api/v1/subject/${uuid}`;
  const idResponse = await request('GET', idUrl);
  const id = idResponse.result.instance.old_subject_identifier;

  const params = {
    zql: `SELECT {} FROM controlpanel WHERE owner = "${id}"`,
  };
  const url = buildUrl(`/api/v1/controlpanel`, params);
  const response = await request('GET', url);
  const controlPanels = response.result.instance.rows;

  if (!controlPanels.length) return { id };

  const controlPanel = controlPanels[0].instance;

  return {
    id,
    controlPanel: {
      uuid: controlPanel.id,
      template: controlPanel.template,
      shortname: controlPanel.shortname,
      readOnly: controlPanel.read_only,
      customerType: controlPanel.customer_type,
      allowedDiskspace: controlPanel.allowed_diskspace,
      allowedEnvironments: controlPanel.allowed_instances,
      availableTemplates: controlPanel.available_templates,
    },
  };
};

export const setCustomerType: SetCustomerTypeType = async (
  id,
  customerType
) => {
  const data = { owner: id, customer_type: customerType };
  const url = '/api/v1/controlpanel/create';

  await request('POST', url, data);
};

const formatEnvironment: FormatEnvironmentType = ({ instance }) => ({
  name: instance.id,
  uuid: instance.id,
  fqdn: instance.fqdn,
  label: instance.label,
  diskspace: Number(instance.stat_diskspace),
  hosts: instance.hosts.rows.map((host: any) => ({
    fqdn: host.instance.fqdn,
    uuid: host.reference,
  })),
  status: instance.status,
  otap: instance.otap,
  template: instance.template,
  customerType: instance.customer_type,
  softwareVersion: instance.software_version,
  fallbackUrl: instance.fallback_url,
  provisionedOn: instance.provisioned_on,
  servicesDomain: instance.services_domain,
  apiDomain: instance.api_domain,
  database: instance.database,
  databaseHost: instance.database_host,
  filestore: instance.filestore,
  freeformReference: instance.freeform_reference,
  protected: instance.protected,
  disabled: instance.disabled,
  oidcOrganizationId: instance.oidc_organization_id,
});

const findEnvironment: FindEnvironmentType = (hostUuid, environments) =>
  environments.find(environment =>
    environment.hosts.map(host => host.uuid).includes(hostUuid)
  );

const formatHost: FormatHostType = (
  { reference, instance: { fqdn, label, ssl_cert, ssl_key, template } },
  environments
) => ({
  uuid: reference,
  fqdn,
  label,
  sslCert: ssl_cert,
  sslKey: ssl_key,
  template,
  environment: findEnvironment(reference, environments),
});

export const getData: GetDataType = async uuid => {
  const url = `/api/v1/controlpanel/${uuid}/instance`;
  const response = await request('GET', url);
  const rows = response.result.instance.rows;
  const environments = rows.map(formatEnvironment);

  const params = { rows_per_page: 100 };
  const hostsUrl = buildUrl(`/api/v1/controlpanel/${uuid}/host`, params);
  const hostsResponse = await request('GET', hostsUrl);
  const hostsRows = hostsResponse.result.instance.rows;
  const hosts = hostsRows.map((host: any) => formatHost(host, environments));

  return { environments, hosts };
};

export const saveEnvironment: SaveEnvironmentType = async (
  controlPanel,
  values,
  environment
) => {
  const data = {
    label: values.label,
    fqdn: values.fqdn,
    customer_type: values.customerType || undefined,
    otap: values.otap,
    software_version: values.softwareVersion || undefined,
    template: values.template,
    password: values.password || undefined,

    protected: values.protected,
    disabled: values.disabled,
    oidc_organization_id: values.oidcOrganizationId || undefined,
  };
  const baseUrl = `/api/v1/controlpanel/${controlPanel.uuid}/instance`;
  const suffix = environment ? `${environment.uuid}/update` : 'create';
  const url = `${baseUrl}/${suffix}`;

  await request('POST', url, data);
};

/* eslint complexity: [2, 9] */
export const saveHost: SaveHostType = async (controlPanel, values, host) => {
  const data = {
    label: values.label,
    fqdn: values.fqdn,
    ssl_cert: values.sslCert || null,
    ssl_key: values.sslKey || null,
    template: values.template || null,
    instance: values.environment || null,
  };
  const baseUrl = `/api/v1/controlpanel/${controlPanel.uuid}/host`;
  const suffix = host ? `${host.uuid}/update` : 'create';
  const url = `${baseUrl}/${suffix}`;

  await request('POST', url, data);
};

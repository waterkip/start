// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useParams } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import useSession from '@zaaksysteem/common/src/hooks/useSession';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import {
  SideMenu,
  SideMenuItemType,
} from '@mintlab/ui/App/Zaaksysteem/SideMenu';
import { ContactViewParamsType } from '.';

const MenuItemLink = React.forwardRef<any, SideMenuItemType>(
  ({ href, ...restProps }, ref) => <Link to={href || ''} {...restProps} />
);
MenuItemLink.displayName = 'MenuItemLink';

export const ContactViewSideMenu: React.ComponentType = () => {
  const session = useSession();
  const [t] = useTranslation('contactView');
  const params = useParams<
    keyof ContactViewParamsType
  >() as ContactViewParamsType;

  const selectedItem = params['*'].split('/')[0];

  const controlpanelActive = session.active_interfaces.includes('controlpanel');
  const hasEnvironments = params.type === 'organization' && controlpanelActive;
  const environmentsTab = { id: 'environments', icon: iconNames.storage };

  const menuItems: SideMenuItemType[] = [
    { id: 'data', icon: iconNames.person },
    { id: 'communication', icon: iconNames.alternate_email },
    { id: 'cases', icon: iconNames.list },
    { id: 'map', icon: iconNames.map },
    { id: 'relationships', icon: iconNames.link },
    { id: 'timeline', icon: iconNames.schedule },
    ...(hasEnvironments ? [environmentsTab] : []),
  ].map<SideMenuItemType>(({ id, icon }) => ({
    id,
    href: id,
    label: t(`sideMenu.${id}`),
    selected: selectedItem === id,
    component: MenuItemLink,
    icon: <Icon size="small">{icon}</Icon>,
  }));

  return <SideMenu items={menuItems} />;
};

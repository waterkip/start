// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as yup from 'yup';
import { REGEXP_EMAIL_OR_MAGIC_STRING } from '@zaaksysteem/common/src/constants/regexes';

/**
 * Validation rules for basic component types.
 * Note that rules are responsible for implementing the
 * `required` check, as the order of checks can matter.
 */

/*
 * Matches emailaddresses and magic strings, for example:
 * - test@zaaksysteem.nl
 * - [[ behandelaar_email ]]
 */

export const emailRule = ({ t, field }) => {
  const base = emailOrMagicStringRule({ t, field });
  const { required } = field;

  return required ? base.required() : base;
};

export const emailOrMagicStringRule = ({ t, field }) => {
  return field.allowMagicString
    ? yup
        .string()
        .nullable()
        .matches(REGEXP_EMAIL_OR_MAGIC_STRING, {
          message: t('validations:email.invalidEmailAddressOrMagicString'),
          excludeEmptyString: true,
        })
    : yup.string().nullable().email(t('validations:email.invalidEmailAddress'));
};

export const selectRule = ({ field }) =>
  field.required ? yup.mixed().required() : yup.mixed();

export const textRule = ({ field }) =>
  field.required ? yup.string().required() : yup.string();

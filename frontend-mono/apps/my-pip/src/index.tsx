// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

/// <reference path='../../../packages/ui/types/Theme.d.ts'/>

import React from 'react';
import ReactDOM from 'react-dom';
import { setupI18n } from './i18n';
import App from './App';
import * as serviceWorker from './serviceWorker';
import './index.css';

setupI18n().then(() => {
  ReactDOM.render(<App />, document.getElementById('root'));
});

const oldFetch = window.fetch;

window.fetch = (resource, options) => {
  const jwtToken = localStorage.getItem('jwt_token');
  const headers = jwtToken
    ? { Authorization: 'Bearer ' + jwtToken }
    : ({} as {});

  if (
    resource.toString().startsWith(window.location.origin) ||
    resource.toString().startsWith('/')
  ) {
    if (options) {
      return oldFetch(resource, {
        ...options,
        headers: {
          ...headers,
          ...(options.headers || {}),
        },
      });
    }

    return oldFetch(resource, { headers });
  }

  return oldFetch(resource, options);
};

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

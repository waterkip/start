// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

/**
 * Iterate a `Map` of key/value functions until the first *key*
 * that produces a truthy result when called and return the outcome
 * of calling the associated *value*.
 *
 * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map
 *
 * @param {Object} options
 * @param {Map} options.map
 * @param {Array} [options.keyArguments=[]]
 * @param {Array} [options.valueArguments=[]]
 * @param {*} [options.fallback]
 * @return {*}
 */
export function reduceMap({
  map,
  keyArguments = [],
  valueArguments = [],
  fallback,
}) {
  for (const [mapKey, mapValue] of map) {
    if (mapKey(...keyArguments)) {
      return mapValue(...valueArguments);
    }
  }

  if (typeof fallback === 'function') {
    return fallback(...valueArguments);
  }

  return fallback;
}

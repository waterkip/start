// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

const { dictionary } = require('./dictionary');

/**
 * Create a dictionary that is not enumerable.
 *
 * @param {Object} object
 * @return {Object}
 */
export const expose = object =>
  dictionary(object, {
    enumerable: false,
  });

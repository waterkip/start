// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import ReactErrorBoundary from 'react-error-boundary';
import { useTranslation } from 'react-i18next';
import Typography from '@mui/material/Typography';
import Icon from '@mintlab/ui/App/Material/Icon';
import { withStyles } from '@mui/styles';
import { errorBoundaryStylesheet } from './ErrorBoundary.style';

const FallbackComponent = ({ classes, componentStack }) => {
  const [t] = useTranslation('common');
  return (
    <div className={classes.wrapper} title={componentStack}>
      <div className={classes.titleWrapper}>
        <span className={classes.icon}>
          <Icon size={48}>error_outline</Icon>
        </span>
        <Typography variant="body1">{t('clientErrors.render')}</Typography>
      </div>

      {process.env.NODE_ENV === 'development' ? (
        <Typography variant="body2" classes={{ root: classes.stacktrace }}>
          {componentStack}
        </Typography>
      ) : null}
    </div>
  );
};

const FallbackComponentWithStyles = withStyles(errorBoundaryStylesheet)(
  FallbackComponent
);

const ErrorBoundary = ({ children, ...props }) => (
  <ReactErrorBoundary
    FallbackComponent={FallbackComponentWithStyles}
    {...props}
  >
    {children}
  </ReactErrorBoundary>
);

export default ErrorBoundary;

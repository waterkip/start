// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import classNames from 'classnames';
import { v4 as unique } from 'uuid';
import Typography from '@mui/material/Typography';
import { useSubHeaderStyles } from './Subheader.style';

export type SubHeaderPropsType = {
  title: string;
  titleSuffix?: string;
  titleSuffixClass?: string;
  description?: string;
  styles?: ReturnType<typeof useSubHeaderStyles>;
};

export const SubHeader: React.FunctionComponent<SubHeaderPropsType> = ({
  title,
  description,
  titleSuffix,
  titleSuffixClass,
  styles,
}) => {
  const SubHeaderStyles = useSubHeaderStyles();
  const classes = styles || SubHeaderStyles;
  const descriptionId = unique();

  return (
    <div className={classes.wrapper}>
      <div className={classes.titleWrapper}>
        <Typography variant="h4" classes={{ root: classes.title }}>
          {title}
        </Typography>
        <Typography
          variant="h4"
          classes={{ root: classNames(classes.title, titleSuffixClass) }}
        >
          {titleSuffix}
        </Typography>
      </div>
      {description && (
        <Typography
          variant="body1"
          classes={{ root: classes.description }}
          id={descriptionId}
        >
          {description}
        </Typography>
      )}
    </div>
  );
};

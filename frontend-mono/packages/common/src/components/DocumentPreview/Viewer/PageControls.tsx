// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import classNames from 'classnames';
import Button from '@mintlab/ui/App/Material/Button';
import Typography from '@mui/material/Typography';

import { usePageControlStyles } from './PageControls.style';

interface PageControlsPropsType {
  page: number;
  numPages: number;
  className?: string;

  onPageChange: (page: number) => void;
}

export const PageControls: React.ComponentType<PageControlsPropsType> = ({
  numPages,
  onPageChange,
  className,
  page,
}) => {
  const [t] = useTranslation('common');
  const classes = usePageControlStyles();
  const changePage = (newPage: number) => {
    if (newPage > 0 && newPage <= numPages) {
      onPageChange(newPage);
    }
  };
  const nextPage = () => changePage(page + 1);
  const previousPage = () => changePage(page - 1);

  return (
    <div className={classNames(classes.wrapper, className)}>
      <Button
        sx={{ color: 'white' }}
        name="pageBack"
        aria-label={t('filePreview.previousPage')}
        iconSize="small"
        icon="navigate_before"
        action={previousPage}
      />
      <Typography variant="body1">
        {page} / {numPages}
      </Typography>
      <Button
        sx={{ color: 'white' }}
        name="pageForward"
        aria-label={t('filePreview.nextPage')}
        icon="navigate_next"
        iconSize="small"
        action={nextPage}
      />
    </div>
  );
};

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import * as i18next from 'i18next';

const resourceUseCount: { [key: string]: number } = {};

const getResourceCount = (namespace: string) =>
  resourceUseCount[namespace] || 0;

const incrementResourceCount = (namespace: string) => {
  resourceUseCount[namespace] = getResourceCount(namespace) + 1;
};

const decrementResourceCount = (namespace: string) => {
  resourceUseCount[namespace] = getResourceCount(namespace) - 1;
};

export const addResourceBundle = (
  i18n: i18next.i18n,
  namespace: string,
  locale: any
) => {
  incrementResourceCount(namespace);
  Object.entries(locale).forEach(([lng, translations]) => {
    i18n.addResourceBundle(lng, namespace, translations);
  });
};

export const removeResourceBundle = (
  i18n: i18next.i18n,
  namespace: string,
  locale: any
) => {
  decrementResourceCount(namespace);

  if (resourceUseCount[namespace] <= 0) {
    Object.entries(locale).forEach(([lng]) =>
      i18n.removeResourceBundle(lng, namespace)
    );
  }
};

export type I18nResourceBundleProps = {
  namespace: string;
  resource: any;
  children: React.ReactNode;
};

const I18nResourceBundle: React.ComponentType<I18nResourceBundleProps> = ({
  resource,
  namespace,
  children,
}) => {
  const [, i18n] = useTranslation();
  const [added, setAdded] = useState(false);

  useEffect(() => {
    addResourceBundle(i18n, namespace, resource);
    setAdded(true);

    return () => {
      removeResourceBundle(i18n, namespace, resource);
    };
  }, [resource, namespace]);

  return <React.Fragment>{added ? children : null} </React.Fragment>;
};

export default I18nResourceBundle;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

const locale = {
  nl: {
    types: {
      job: 'Job',
      legacy_download: 'Exportbestand (V1)',
    },
    jobSimulation: {
      yes: 'Simulatie',
      no: 'Uitgevoerde Job',
    },
    jobTypes: {
      delete_case: 'Verwijder zaken',
      simulate_delete_case: 'Verwijder zaken (simulatie)',
      run_archive_export: 'SIP (Archive) export',
      simulate_run_archive_export: 'SIP (Archive) export (simulatie)',
    },
    serverErrors: {
      'jobs/user_too_many_jobs':
        'U heeft te veel actieve Jobs, en kunt geen nieuwe meer aanmaken. Wacht tot er één is afgerond, of annuleer een actieve Job.',
    },
  },
};

export default locale;

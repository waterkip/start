// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { v4 as unique } from 'uuid';
import DialogContent from '@mui/material/DialogContent';
import Divider from '@mui/material/Divider';
import { withStyles } from '@mui/styles';
import { Dialog, DialogTitle } from '@mintlab/ui/App/Material/Dialog';
//@ts-ignore
import ZsIcon from '@mintlab/ui/App/Zaaksysteem/ZsIcon';
import Typography from '@mui/material/Typography';
import { addScopeAttribute } from '@mintlab/ui/App/library/addScope';
import classNames from 'classnames';
import { addElementStylesheet } from './AddElement.style';
import {
  AddElementPropsType,
  SectionType,
  ClassesType,
} from './AddElement.types';

const Section = ({
  actions,
  classes,
}: {
  actions: SectionType[];
  classes: ClassesType;
}) => {
  return (
    <div className={classes.elementWrapper}>
      {actions.map(({ action, type, title, icon, disabled }: SectionType) => (
        <button
          className={classNames(classes.element, {
            [classes.elementHover]: !disabled,
            [classes.elementDisabled]: disabled,
          })}
          key={type}
          type="button"
          onClick={action}
          disabled={disabled}
          {...addScopeAttribute('catalog', 'add-element', 'button', type)}
        >
          <div className={classes.elementIcon}>
            {icon ? (
              typeof icon === 'string' ? (
                <ZsIcon style={{ fontSize: 30 }}>{icon}</ZsIcon>
              ) : (
                icon
              )
            ) : null}
          </div>
          <div className={classes.elementTitle}>
            <Typography variant="body2">{title}</Typography>
          </div>
        </button>
      ))}
    </div>
  );
};

const AddElement: React.FunctionComponent<AddElementPropsType> = ({
  t,
  classes,
  hide,
  sections,
  title,
  titleIcon = 'add',
}) => {
  const handleClose = () => hide();
  const labelId = unique();
  const scope = 'catalog-add-element-dialog';

  return (
    <Dialog
      //@ts-ignore
      id={scope}
      open={true}
      onClose={handleClose}
      scope={scope}
    >
      <DialogTitle
        elevated={true}
        icon={titleIcon}
        id={labelId}
        title={title}
        onCloseClick={handleClose}
        scope={scope}
      />
      <DialogContent>
        {sections.map((section, index) => (
          <React.Fragment key={index}>
            {index > 0 && (
              <div className={classes.dividerWrapper}>
                <Divider />
              </div>
            )}
            <Section actions={section} classes={classes} />
          </React.Fragment>
        ))}
      </DialogContent>
    </Dialog>
  );
};

export default withStyles(addElementStylesheet)(AddElement);

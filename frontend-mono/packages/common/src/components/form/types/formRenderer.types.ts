// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import { ComponentType } from 'react';
import { FormikProps, FormikTouched, useFormik } from 'formik';
import { Rule } from '../rules';
import { ValidationRule } from '../validation/createValidationRule';
import {
  FormValuesType,
  RegisterValidationForNamespaceType,
} from './generic.types';
import { FormDefinition, StepsDefinition } from './formDefinition.types';

type UseFormBaseType<Values> = {
  formDefinition: FormDefinition<Values>;
  rules?: Rule[];
  /**
   * @deprecated Add components directly with component property of field
   */
  fieldComponents?: {
    [key: string]: ComponentType<any>;
  };
  validationMap?: {
    [key: string]: ValidationRule;
  };
};

export type UseFormType<Values> = UseFormBaseType<Values> & {
  isInitialValid?: boolean;
  enableReinitialize?: boolean;
  t?: i18next.TFunction;
  onChange?: (values: FormValuesType<Values>) => void;
  onSubmit?: (values: Values, formikHelpers: any) => void | Promise<any>;
  initialValues?: Values;
  initialTouched?: ReturnType<typeof useFormik>['initialTouched'];
  onTouched?: (
    touched: FormikTouched<Values>,
    values: FormValuesType<Values>
  ) => void;
};

export type UseSteppedFormType<Values> = UseFormType<Values> & {
  stepsDefinition: StepsDefinition;
};

export type UseSubFormType<Values> = UseFormBaseType<Values> & {
  namespace: string;
  validateForm: () => void;
  formik: FormikProps<FormValuesType<Values>>;
  registerValidation: RegisterValidationForNamespaceType;
};

export type UseRuleEngineType<FormShape> = {
  formDefinition: FormDefinition<FormShape>;
  values: FormShape | Partial<FormShape>;
  rules: Rule[];
  setValues: (values: FormShape) => void;
  setFieldValue: (fieldName: string, value: any) => void;
  setFormDefinition: (formDefinition: FormDefinition<FormShape>) => void;
  namespace?: string;
};

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import {
  hideFields,
  showFields,
  setFieldValue,
  setOptional,
  setRequired,
} from './actions';

describe('Rule engine', () => {
  describe('actions', () => {
    const fields = [
      {
        name: 'test-field',
        hidden: false,
        value: '',
        required: true,
      },
      {
        name: 'test-field2',
        hidden: true,
        value: '',
        required: false,
      },
      {
        name: 'test-field3',
        hidden: false,
        value: '',
        required: true,
      },
    ];

    describe('hideFields', () => {
      test('Should hide field if name matches', () => {
        const action = hideFields(['test-field']);
        const result = action([...fields]);

        expect(result[0].hidden).toEqual(true);
        expect(result[2].hidden).toEqual(false);
      });
    });

    describe('showFields', () => {
      test('Should show field if name matches', () => {
        const action = showFields(['test-field2']);
        const result = action([...fields]);

        expect(result[0].hidden).toEqual(false);
        expect(result[1].hidden).toEqual(false);
      });
    });

    describe('setFieldValue', () => {
      test('Should set field value', () => {
        const action = setFieldValue('test-field', 'test');
        const result = action([...fields]);

        expect(result[0].value).toEqual('test');
        expect(result[1].value).toEqual('');
      });
    });

    describe('setOptional', () => {
      test('Should set field as optional', () => {
        const action = setOptional(['test-field']);
        const result = action([...fields]);

        expect(result[0].required).toEqual(false);
        expect(result[2].required).toEqual(true);
      });
    });

    describe('setRequired', () => {
      test('Should set field as required', () => {
        const action = setRequired(['test-field2']);
        const result = action([...fields]);

        expect(result[0].required).toEqual(true);
        expect(result[2].required).toEqual(true);
      });
    });
  });
});

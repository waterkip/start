// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import TextField from '@mintlab/ui/App/Material/TextField/index';
import { formatCurrency } from '@zaaksysteem/common/src/library/currency';
import { ReadonlyValuesContainer } from '../library/ReadonlyValuesContainer';
import { FormFieldComponentType } from '../types/Form2.types';

export const CurrencyField: FormFieldComponentType<string> = props => {
  const [t] = useTranslation();

  const { value, readOnly } = props;

  const formatType = t('common:currencyCode');
  const readOnlyValue = value ? formatCurrency(t, Number(value)) : value;

  return readOnly ? (
    <ReadonlyValuesContainer value={readOnlyValue} />
  ) : (
    <TextField {...props} formatType={formatType} value={value} />
  );
};

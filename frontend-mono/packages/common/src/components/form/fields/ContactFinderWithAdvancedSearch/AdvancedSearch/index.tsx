// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState, useEffect } from 'react';
import useSession from '@zaaksysteem/common/src/hooks/useSession';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import { SubjectTypeType } from '@zaaksysteem/common/src/types/SubjectTypes';
import AdvancedSearch, { AdvancedSearchPropsType } from './AdvancedSearch';
import {
  contactTypes,
  getStufConfigIntegrations,
} from './AdvancedSearch.library';
import { StufConfigIntegrationType } from './AdvancedSearch.types';

type AdvancedSearchModulePropsType = Pick<
  AdvancedSearchPropsType,
  'onSelectRow' | 'onClose'
> & {
  subjectTypes: SubjectTypeType[];
};

const AdvancedSearchModule: React.ComponentType<
  AdvancedSearchModulePropsType
> = props => {
  const session = useSession();
  const [stufConfigIntegrations, setStufConfigIntegrations] =
    useState<StufConfigIntegrationType[]>();

  useEffect(() => {
    getStufConfigIntegrations(session).then(setStufConfigIntegrations);
  }, []);

  if (!stufConfigIntegrations) return <Loader />;

  const supportedContactTypes = contactTypes.filter(contactType =>
    props.subjectTypes.includes(contactType)
  );

  return (
    <AdvancedSearch
      stufConfigIntegrations={stufConfigIntegrations}
      contactTypes={supportedContactTypes}
      {...props}
    />
  );
};

export default AdvancedSearchModule;

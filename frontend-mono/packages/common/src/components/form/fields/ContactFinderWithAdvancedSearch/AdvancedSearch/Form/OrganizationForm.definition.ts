// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import {
  Rule,
  setRequired,
  setOptional,
  showFields,
  hideFields,
} from '@zaaksysteem/common/src/components/form/rules';
import { AnyFormDefinitionField } from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import { findField } from '../AdvancedSearch.library';
import {
  OrganizationFields,
  GetOrganizationFormDefinitionType,
  GetOrganizationRulesType,
} from './../AdvancedSearch.types';
import { getRequiredOrganizationFields } from './Form.library';

export const getFormDefinition: GetOrganizationFormDefinitionType = t => {
  return [
    {
      name: 'searchIn',
      label: `${t('fields.searchIn')}:`,
      type: fieldTypes.FLATVALUE_SELECT,
      value: 'intern',
      choices: [
        { label: t('integration.searchIn.intern'), value: 'intern' },
        { label: t('integration.searchIn.extern'), value: 'extern' },
      ],
      required: false,
      isClearable: false,
    },
    {
      name: 'rsin',
      label: t('fields.rsin'),
      placeholder: t('fields.rsin'),
      type: fieldTypes.TEXT,
      value: '',
      required: false,
    },
    {
      name: 'cocNumber',
      label: t('fields.cocNumber'),
      placeholder: t('fields.cocNumber'),
      type: fieldTypes.TEXT,
      value: '',
      required: false,
    },
    {
      name: 'cocLocationNumber',
      label: t('fields.cocLocationNumber'),
      placeholder: t('fields.cocLocationNumber'),
      type: fieldTypes.TEXT,
      value: '',
      required: false,
    },
    {
      name: 'tradeName',
      label: t('fields.tradeName'),
      placeholder: t('fields.tradeName'),
      type: fieldTypes.TEXT,
      value: '',
      required: false,
    },
    {
      name: 'street',
      label: t('fields.street'),
      placeholder: t('fields.street'),
      type: fieldTypes.TEXT,
      value: '',
      required: false,
    },
    {
      name: 'zipCode',
      label: t('fields.zipCode'),
      placeholder: t('fields.zipCode'),
      type: fieldTypes.TEXT,
      value: '',
      required: false,
    },
    {
      name: 'streetNumber',
      label: t('fields.streetNumber'),
      placeholder: t('fields.streetNumber'),
      type: fieldTypes.TEXT,
      value: '',
      required: false,
    },
    {
      name: 'streetNumberLetter',
      label: t('fields.streetNumberLetter'),
      placeholder: t('fields.streetNumberLetter'),
      type: fieldTypes.TEXT,
      value: '',
      required: false,
    },
    {
      name: 'suffix',
      label: t('fields.streetNumberSuffix'),
      placeholder: t('fields.streetNumberSuffix'),
      type: fieldTypes.TEXT,
      value: '',
      required: false,
    },
  ];
};

/* eslint complexity: [2, 99] */
const organizationFormValidations = (fields: AnyFormDefinitionField[]) => {
  const allRequiredFields = getRequiredOrganizationFields().flat();
  const rsin = findField(fields, 'rsin');
  const cocNumber = findField(fields, 'cocNumber');
  const cocLocationNumber = findField(fields, 'cocLocationNumber');
  const tradeName = findField(fields, 'tradeName');
  const zipCode = findField(fields, 'zipCode');
  const streetNumber = findField(fields, 'streetNumber');

  const allRequiredFieldsAreEmpty =
    !rsin.value &&
    !cocNumber.value &&
    !cocLocationNumber.value &&
    !tradeName.value &&
    !zipCode.value &&
    !streetNumber.value;

  if (allRequiredFieldsAreEmpty) {
    fields = setRequired(allRequiredFields)(fields);
  } else {
    fields = setOptional(allRequiredFields)(fields);
  }

  const formIsValid =
    rsin.value ||
    cocNumber.value ||
    cocLocationNumber.value ||
    tradeName.value ||
    (zipCode.value && streetNumber.value);

  if (rsin.value) {
    fields = setRequired(['rsin'])(fields);
  }

  if (cocNumber.value) {
    fields = setRequired(['cocNumber'])(fields);
  }

  if (cocLocationNumber.value) {
    fields = setRequired(['cocLocationNumber'])(fields);
  }

  if (tradeName.value) {
    fields = setRequired(['tradeName'])(fields);
  }

  if (
    (zipCode.value && streetNumber.value) ||
    (!formIsValid && (zipCode.value || streetNumber.value))
  ) {
    fields = setRequired(['zipCode', 'streetNumber'])(fields);
  }

  return fields;
};

export const getRules: GetOrganizationRulesType = hasKvKApi => [
  new Rule<OrganizationFields>()
    .when(() => true)
    .then(organizationFormValidations),
  new Rule<OrganizationFields>()
    .when(() => hasKvKApi)
    .then(showFields(['searchIn']))
    .else(hideFields(['searchIn'])),
];

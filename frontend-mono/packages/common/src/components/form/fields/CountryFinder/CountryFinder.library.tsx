// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useQuery } from '@tanstack/react-query';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { openServerError } from '../../../../signals';

type ChoiceType = {
  type: string;
  id: any;
  attributes?: {
    name: string;
    code: number;
  };
};

export const useCountryChoicesQuery = () => {
  const data = useQuery(
    ['countries'],
    async ({ queryKey: [__, keyword] }) => {
      const body = await request<any>(
        'GET',
        '/api/v2/cm/contact/get_countries'
      ).catch(openServerError);

      let unknown: any;

      const countries = body
        ? ((body.data as Array<ChoiceType>) || [])
            .map(choice => {
              return {
                label: choice?.attributes?.name || '',
                value: choice?.attributes?.code,
              };
            })
            .filter(choice => {
              if (choice?.value === 0) unknown = choice;
              return choice?.value !== 0;
            })
            .sort((aChoice, bChoice) =>
              aChoice.label > bChoice.label ? 1 : -1
            )
        : [];

      return !countries || !countries?.length
        ? []
        : [...[unknown], ...countries];
    },
    { enabled: true }
  );

  const selectProps = {
    loading: data.status === 'loading',
    choices: data.data || [],
  };

  return [
    selectProps,
    data.status === 'success' && selectProps.choices.length === 0,
  ] as const;
};

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { TextField } from '@mintlab/ui/App/Material/TextField';
import { FormFieldComponentType } from '../types/Form2.types';

export const Textarea: FormFieldComponentType<string> = ({
  readOnly,
  ...restProps
}) => {
  return readOnly ? (
    <textarea
      value={restProps.value || '-'}
      readOnly={true}
      style={{
        padding: '8px 0px 8px 12px',
        minHeight: restProps.minHeight || '200px',
        fontFamily: 'inherit',
        fontSize: 'inherit',
        width: 'calc(100% - 24px)',
        border: 'none',
        overflow: 'auto',
        outline: 'none',
        boxShadow: 'none',
        resize: 'none',
      }}
    />
  ) : (
    <TextField
      isMultiline={true}
      rows={7}
      {...restProps}
      sx={{
        '& .MuiInputBase-input': {
          padding: '5px 0px 0px',
          borderRadius: '8px',
        },
        '& .MuiInputBase-root': {
          height: '100%',
          padding: '10px 5px 0px 15px',
          backgroundColor: 'transparent',
          '&:hover': {
            backgroundColor: 'transparent',
          },
          overflowY: 'auto',
        },
      }}
    />
  );
};

export default Textarea;

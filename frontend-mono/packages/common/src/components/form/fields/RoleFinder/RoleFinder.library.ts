// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useQuery } from '@tanstack/react-query';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { openServerError } from '@zaaksysteem/common/src/signals';
import { APICaseManagement } from '@zaaksysteem/generated';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { GetSummaryType } from './RolesFinder.types';

const getSummary: GetSummaryType = role => role.meta.summary.toLowerCase();

export const useRoleChoicesQuery = (parentUuid: string | undefined) => {
  const data = useQuery(
    ['roles', parentUuid],
    async ({ queryKey: [__, parent_uuid] }) => {
      const body = await request<APICaseManagement.GetRolesResponseBody>(
        'GET',
        buildUrl<APICaseManagement.GetRolesRequestParams>(
          '/api/v2/cm/authorization/get_roles',
          {
            filter: {
              'relationships.parent.id': parent_uuid || '',
            },
          }
        )
      );

      const roles = body ? body.data || [] : [];

      const sortedRoles = roles.sort((roleA, roleB) =>
        getSummary(roleA) < getSummary(roleB) ? -1 : 1
      );

      const systemRoles = sortedRoles.filter(role =>
        Boolean(role.attributes?.description)
      );

      const departmentRoles = sortedRoles.filter(
        role => !role.attributes?.description
      );

      return [...departmentRoles, ...systemRoles].map(role => ({
        label: role.attributes?.name || '',
        value: role.id,
      }));
    },
    { onError: openServerError }
  );

  const selectProps = {
    loading: data.status === 'loading',
    choices: data.data || [],
  };

  return [
    selectProps,
    data.status === 'success' && selectProps.choices.length === 0,
  ] as const;
};

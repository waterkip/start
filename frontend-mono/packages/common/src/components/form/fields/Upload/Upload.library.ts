// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as uuid from 'uuid';
import { APIDocument } from '@zaaksysteem/generated';
import { request } from '@zaaksysteem/common/src/library/request/request';

export type ResponseBodyType = APIDocument.CreateFileResponseBody;

export const sendAsFile = (file: File) => {
  let formData = new FormData();

  formData.append('file', file);

  return request('POST', '/api/v2/file/create_file', formData, {
    type: 'formdata',
    cached: false,
  })
    .then((response: ResponseBodyType) => response)
    .catch((response: ResponseBodyType) => Promise.reject(response));
};

export const sendAsDocument = (file: File) => {
  let formData = new FormData();
  const document_uuid = uuid.v4();

  formData.append('document_file', file);
  formData.append('skip_intake', '1');
  formData.append('document_uuid', document_uuid);

  return request('POST', '/api/v2/document/create_document', formData, {
    type: 'formdata',
    cached: false,
  })
    .then(
      (response: ResponseBodyType) =>
        ({
          ...response,
          data: { ...response.data, id: document_uuid },
        }) as ResponseBodyType
    )
    .catch((response: ResponseBodyType) => Promise.reject(response));
};

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
type DataType = {
  textLabel?: string;
};
export type DepartmentFinderOptionType = ValueType<string, DataType>;
export type RoleFinderOptionType = ValueType<string, DataType>;

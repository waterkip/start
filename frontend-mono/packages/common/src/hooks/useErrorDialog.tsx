// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { v4 } from 'uuid';
import Alert from '@zaaksysteem/common/src/components/dialogs/Alert/Alert';

type OpenDialogPropsType = {
  message: React.ReactNode;
  title?: string;
};

export function useErrorDialog(): [
  React.ReactNode,
  ({ message, title }: OpenDialogPropsType) => any,
] {
  const [isOpen, setOpen] = React.useState(false);
  const [message, setMessage] = React.useState<React.ReactNode>();
  const [title, setTitle] = React.useState('');
  const [t] = useTranslation('common');

  const doOpen = ({ message, title }: OpenDialogPropsType) => {
    setMessage(message);
    setTitle(title || t('common:dialog.error.title'));
    setOpen(true);
  };
  const doClose = () => setOpen(false);

  const alert = React.useMemo(
    () => (
      <Alert
        primaryButton={{ text: t('common:dialog.ok'), action: doClose }}
        key={typeof message === 'string' ? message : v4()}
        onClose={doClose}
        title={title}
        open={isOpen}
      >
        {message}
      </Alert>
    ),
    [isOpen]
  );

  return [alert, doOpen];
}

export default useErrorDialog;

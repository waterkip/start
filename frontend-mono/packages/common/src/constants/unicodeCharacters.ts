// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const nonBreakingSpace = '\u00A0';
export const nonBreakingDash = '\u2011';

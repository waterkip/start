// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const REGEXP_URL =
  //eslint-disable-next-line
  /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)/;
export const REGEXP_NUMERIC = /^(0|[1-9][0-9]*)$/;
export const REGEXP_EMAIL_OR_MAGIC_STRING =
  /^\[\[(\s+)?([a-z0-9_]+)(\s+)?\]\]$|^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
export const REGEXP_TEXT = /^(?!\s*$).+/;
export const REGEXP_DATE_DAY = /^\d{4}-\d{2}-\d{2}/;
export const REGEXP_ZIPCODE = /^[1-9][0-9]{3} ?(?!sa|sd|ss)[a-z]{2}$/;
export const REGEXP_COCNR = /^[0-9]{1,8}$/;
export const REGEXP_TAGS = /<(.|\n)*?>/g;
export const REGEXP_UUID =
  /^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { SubjectRelationType } from './SubjectRelations';

export type CommunicationContextCapabilitiesType = {
  allowSplitScreen: boolean;
  canAddAttachmentToCase: boolean;
  canAddSourceFileToCase: boolean;
  canAddThreadToCase: boolean;
  canCreateContactMoment: boolean;
  canCreateNote: boolean;
  canCreatePipMessage: boolean;
  canCreateEmail: boolean;
  canMarkUnread: boolean;
  canDeleteMessage: boolean;
  canSelectContact: boolean;
  canSelectCase: boolean;
  canImportMessage: boolean;
  canFilter: boolean;
  canOpenPDFPreview: boolean;
  canCreatePostex: boolean;
};

export type CommunicationContextContextType =
  | 'pip'
  | 'case'
  | 'contact'
  | 'inbox';

export type RecipientTypeType =
  | 'requestor'
  | 'colleague'
  | 'coordinator'
  | 'authorized'
  | 'role'
  | 'other';

type EmailType = {
  value: string;
  label: string;
};

type AttachmentType = {
  value: string;
  label: string;
};

type AssigneeType = {
  value: string;
  label: string;
};

export type EmailTemplateType = {
  id: number;
  uuid: string;
  label: string;
  senderName: string;
  senderEmail: string;
  recipientType: RecipientTypeType;
  behandelaar?: string;
  assignee?: AssigneeType[];
  role?: string;
  to: EmailType[];
  cc: EmailType[];
  bcc: EmailType[];
  subject: string;
  body: string;
  attachments: AttachmentType[];
  attributeAttachmentIds: number[];
};

export interface CommunicationContextType {
  capabilities: CommunicationContextCapabilitiesType;
  context: CommunicationContextContextType;
  setAddThreadToCaseDialogOpened: (threadId: string | undefined) => void;
  setImportMessageDialogOpened: (opened: boolean) => void;
  caseUuid?: string;
  caseNumber?: number;
  contactUuid?: string;
  contactName?: string;
  contactType?: string;
  htmlEmailTemplateName?: string;
  emailTemplates?: EmailTemplateType[];
  subjectRelations?: SubjectRelationType[];
}

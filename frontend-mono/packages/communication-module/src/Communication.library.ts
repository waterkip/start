// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { fetchCaseRelations } from './Communication.requests';
import {
  FormatSubjectRelationType,
  GetSubjectRelationsType,
} from './types/SubjectRelations';

const formatSubjectRelation: FormatSubjectRelationType = (
  {
    attributes: { is_preset_client, magic_string_prefix, authorized },
    relationships: {
      subject: {
        data: { id, meta },
      },
    },
  },
  subjects
) => {
  const subject = subjects?.find(subj => subj.id === id);
  const email = subject?.attributes.contact_information.email;

  return {
    uuid: id as string,
    label: meta.display_name,
    value: email || '',
    role: magic_string_prefix,
    isPresetClient: is_preset_client,
    authorized,
  };
};

export const getSubjectRelations: GetSubjectRelationsType = async caseUuid => {
  const response = await fetchCaseRelations(caseUuid);

  const subjectRelations = response.data.map(subject =>
    formatSubjectRelation(subject, response.included)
  );

  return subjectRelations;
};

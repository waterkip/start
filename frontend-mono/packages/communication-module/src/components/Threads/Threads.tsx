// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useParams } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import Typography from '@mui/material/Typography';
//@ts-ignore
import ErrorBoundary from '@zaaksysteem/common/src/components/ErrorBoundary/ErrorBoundary';
import { CommunicationContext } from '../../Communication.context';
import ThreadList from './ThreadList/ThreadList';
import { useThreadsStyle } from './Threads.style';
import { FilterType, useGetThreadListQuery } from './Threads.library';
import ThreadsHeader from './ThreadsHeader/ThreadsHeader';

/* eslint complexity: [2, 11] */
const Threads = () => {
  const context = React.useContext(CommunicationContext);
  const [searchTerm, setSearchTerm] = React.useState('');
  const [filterValue, setFilter] = React.useState<FilterType>();
  const classes = useThreadsStyle();
  const [t] = useTranslation(['communication']);
  const { identifier } = useParams();

  const { data, isLoading, fetchNextPage, isFetchingNextPage } =
    useGetThreadListQuery(context, searchTerm, filterValue);

  const lastPage = data && data.pages.at(-1);
  const hasMorePages = Boolean(lastPage && lastPage.data.length === 100);

  const threadList = data ? data.pages.map(page => page.data).flat() : [];

  return (
    <div className={classes.wrapper}>
      {data && (
        <ThreadsHeader
          filter={filterValue}
          onFilterChange={setFilter}
          searchTerm={searchTerm}
          onSearchTermChange={setSearchTerm}
        />
      )}
      {isLoading && <Loader delay={200} />}
      <ErrorBoundary>
        {threadList.length > 0 ? (
          <div className={classes.listWrapper}>
            <ThreadList
              loadMoreRows={fetchNextPage}
              isFetchingNextPage={isFetchingNextPage}
              hasMorePages={hasMorePages}
              threads={threadList}
              context={context.context}
              showLinkToCase={context.capabilities.canSelectCase}
              selectedThreadId={identifier}
            />
          </div>
        ) : isLoading ? null : (
          <div className={classes.noContentWrapper}>
            <Typography
              variant="body1"
              classes={{ root: classes.noContentText }}
            >
              {t('thread.noContent')}
            </Typography>
          </div>
        )}
      </ErrorBoundary>
    </div>
  );
};

export default Threads;

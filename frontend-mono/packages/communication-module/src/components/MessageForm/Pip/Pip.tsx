// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import { useMutation, useQueryClient } from '@tanstack/react-query';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import { openServerError } from '@zaaksysteem/common/src/signals';
import mapValuesToFormDefinition from '@zaaksysteem/common/src/components/form/library/mapValuesToFormDefinition';
import { GET_MESSAGES, GET_THREADS } from '../../../Communication.constants';
import GenericForm from '../GenericForm/GenericForm';
import { CommunicationContext } from '../../../Communication.context';
import { ComponentPropsType } from '../MessageForm.types';
import { getPreview, savePipAction } from '../MessageForm.library';
import { getPipFormDefinition, SavePipValuesType } from './Pip.formDefinition';

/* eslint complexity: [2, 9] */
const Pip: React.ComponentType<ComponentPropsType> = ({
  cancel,
  caseUuid,
  threadUuid,
  firstMessage,
}) => {
  const navigate = useNavigate();
  const queryClient = useQueryClient();

  const commContext = useContext(CommunicationContext);
  const { context, contactUuid, subjectRelations } = commContext;

  const resolvedCaseUuid = (commContext.caseUuid || caseUuid) as string;

  const { mutate, isLoading } = useMutation<any, V2ServerErrorsType>({
    mutationKey: ['saveContactMoment'],
    mutationFn: async (values: any) => {
      const resolvedValues = await getPreview(values, values.case_uuid);

      return savePipAction(resolvedValues, threadUuid);
    },
    onError: openServerError,
    onSuccess: () => {
      queryClient.refetchQueries(GET_THREADS);
      queryClient.invalidateQueries(GET_MESSAGES);

      if (context === 'case') {
        cancel();
      } else if (context === 'pip') {
        navigate('..');
      }
    },
  });

  const values = firstMessage
    ? {
        subject: firstMessage.subject ? `RE: ${firstMessage.subject}` : '',
      }
    : {};

  const formDefinition = getPipFormDefinition(
    context,
    resolvedCaseUuid,
    contactUuid,
    subjectRelations
  );
  const formDefinitionWithValues = values
    ? mapValuesToFormDefinition<SavePipValuesType>(values, formDefinition)
    : formDefinition;

  return (
    <GenericForm
      cancel={cancel}
      busy={isLoading}
      save={mutate}
      formDefinition={formDefinitionWithValues}
      formName="contact-moment"
      primaryButtonLabelKey="forms.send"
      enablePreview={context !== 'pip'}
    />
  );
};

export default Pip;

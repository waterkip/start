// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import {
  hideFields,
  showFields,
  valueEquals,
  valueOneOf,
} from '@zaaksysteem/common/src/components/form/rules';
import Rule from '@zaaksysteem/common/src/components/form/rules/library/Rule';
import { capitalize } from '@mintlab/kitchen-sink/source';
import { CommunicationContextType } from '../../../types/Context.types';

/* eslint complexity: [2, 8] */
export const getPostexRules = (commContext: CommunicationContextType) => [
  new Rule()
    .when('recipient_type', valueEquals('betrokkene'))
    .then(showFields(['recipient_role']))
    .else(hideFields(['recipient_role'])),
  new Rule()
    .when('recipient_type', valueOneOf(['authorized', 'betrokkene']))
    .then(showFields(['role_name']))
    .else(hideFields(['role_name'])),
  new Rule()
    .when(() => true)
    .then(fields => {
      return fields.map(field => {
        if (field.name === 'recipient_role') {
          const names = commContext.subjectRelations
            ?.filter(
              rel =>
                !['coordinator', 'behandelaar', 'aanvrager'].includes(rel.role)
            )
            .map(rel => capitalize(rel.role.replace('1', '')));

          const choices = Array.from(new Set(names)).map(value => ({
            value,
            label: value,
          }));

          return {
            ...field,
            choices,
          };
        } else if (field.name === 'role_name') {
          const recipientTypeFieldValue = (
            fields.find(field1 => field1.name === 'recipient_type')
              ?.value as any
          )?.value;

          let value = '';

          if (recipientTypeFieldValue === 'authorized') {
            value =
              commContext.subjectRelations
                ?.filter(rel => rel.authorized)
                .map(rel => rel.label)
                .join(', ') || '';
          } else if (recipientTypeFieldValue === 'betrokkene') {
            const selectedRole = (
              fields.find(field2 => field2.name === 'recipient_role')
                ?.value as any
            )?.value;

            if (selectedRole) {
              value =
                commContext.subjectRelations
                  ?.filter(rel =>
                    rel.role.startsWith(selectedRole.toLowerCase())
                  )
                  .map(rel => rel.label)
                  .join(', ') || '';
            }
          }

          return { ...field, value };
        } else {
          return field;
        }
      });
    }),
];

export const getPostexFormDefinition = (
  caseUuid: string,
  contactType: string,
  hasAuthorizedSubject: boolean,
  hasExternalSubject: boolean
) => {
  const defaultDocumentsParams =
    window.top?.location.href.split('documents=')[1];
  const defaultDocuments = defaultDocumentsParams
    ? defaultDocumentsParams.split(',')
    : undefined;

  const recipientChoices = [
    ...(contactType === 'employee'
      ? []
      : [
          {
            label: 'addFields.recipientTypes.requestor',
            value: 'requestor',
          },
        ]),
    ...(hasExternalSubject
      ? [
          {
            label: 'addFields.recipientTypes.role',
            value: 'betrokkene',
          },
        ]
      : []),
    ...(hasAuthorizedSubject
      ? [
          {
            label: 'addFields.recipientTypes.authorized',
            value: 'authorized',
          },
        ]
      : []),
    {
      label: 'addFields.recipientTypes.ocr',
      value: 'ocr',
    },
  ];

  return [
    {
      name: 'recipient_type',
      type: fieldTypes.SELECT,
      value: recipientChoices[0],
      isClearable: false,
      showLabel: true,
      hideBorder: true,
      label: 'addFields.recipientType',
      choices: recipientChoices,
      applyBackgroundColor: true,
    },
    {
      name: 'recipient_role',
      type: fieldTypes.SELECT,
      value: null,
      isClearable: false,
      showLabel: true,
      hideBorder: true,
      label: 'addFields.roleType',
      // set through rules
      choices: [],
      applyBackgroundColor: true,
    },
    {
      name: 'role_name',
      type: fieldTypes.TEXT,
      showLabel: true,
      readOnly: true,
      // set through rules
      value: '',
      label: 'addFields.roleName',
    },
    {
      format: 'file' as const,
      accept: ['pdf'],
      name: 'attachments',
      type: fieldTypes.CASE_DOCUMENT_FINDER,
      value: null,
      required: true,
      uploadDialog: false,
      multiValue: true,
      label: 'addFields.attachments',
      placeholder: 'addFields.attachmentsPlaceholder',
      applyBackgroundColor: true,
      config: { caseUuid, defaultDocuments, accept: ['.pdf'] },
    },
  ];
};

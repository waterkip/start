// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useNavigate } from 'react-router';
import { useMutation, useQueryClient } from '@tanstack/react-query';
import { openServerError } from '@zaaksysteem/common/src/signals';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import { CommunicationContext } from '../../../Communication.context';
import { GET_THREADS } from '../../../Communication.constants';
import GenericForm from '../GenericForm/GenericForm';
import { ComponentPropsType } from '../MessageForm.types';
import { saveContactMomentAction } from '../MessageForm.library';
import { getContactMomentFormDefinition } from './ContactMoment.formDefinition';

const ContactMoment: React.ComponentType<ComponentPropsType> = ({ cancel }) => {
  const context = React.useContext(CommunicationContext);
  const navigate = useNavigate();
  const queryClient = useQueryClient();

  const { mutate, isLoading } = useMutation<any, V2ServerErrorsType>({
    mutationKey: ['saveContactMoment'],
    mutationFn: (values: any) => saveContactMomentAction(values),
    onError: openServerError,
    onSuccess: () => {
      queryClient.refetchQueries(GET_THREADS);
      navigate('..');
    },
  });

  const formDefinition = getContactMomentFormDefinition(context);

  return (
    <GenericForm
      cancel={cancel}
      busy={isLoading}
      formDefinition={formDefinition}
      formName="contact-moment"
      save={mutate}
    />
  );
};

export default ContactMoment;

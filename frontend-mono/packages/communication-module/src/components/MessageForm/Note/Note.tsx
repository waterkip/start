// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useNavigate } from 'react-router-dom';
import { useMutation, useQueryClient } from '@tanstack/react-query';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import { openServerError } from '@zaaksysteem/common/src/signals';
import { GET_THREADS } from '../../../Communication.constants';
import { CommunicationContext } from '../../../Communication.context';
import { ComponentPropsType } from '../MessageForm.types';
import GenericForm from '../GenericForm/GenericForm';
import { saveNoteAction } from '../MessageForm.library';
import { noteFormDefinition } from './Note.formDefinition';

const Note: React.ComponentType<ComponentPropsType> = ({ cancel }) => {
  const context = React.useContext(CommunicationContext);
  const navigate = useNavigate();
  const queryClient = useQueryClient();

  const { mutate, isLoading } = useMutation<any, V2ServerErrorsType>({
    mutationKey: ['saveContactMoment'],
    mutationFn: (values: any) =>
      saveNoteAction(values, context.caseUuid, context.contactUuid),
    onError: openServerError,
    onSuccess: () => {
      queryClient.refetchQueries(GET_THREADS);
      navigate('..');
    },
  });

  return (
    <GenericForm
      cancel={cancel}
      busy={isLoading}
      save={mutate}
      formDefinition={noteFormDefinition}
      formName="contact-moment"
    />
  );
};

export default Note;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useQuery } from '@tanstack/react-query';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { APICommunication } from '@zaaksysteem/generated';
import { openServerError } from '@zaaksysteem/common/src/signals';

export const useCaseChoicesForContactQuery = (
  contactUuid: string | undefined
) => {
  const data = useQuery(
    ['contactCases', contactUuid],
    async ({ queryKey: [__, uuid] }) => {
      const body =
        await request<APICommunication.GetCaseListForContactResponseBody>(
          'GET',
          buildUrl<APICommunication.GetCaseListForContactRequestParams>(
            `/api/v2/communication/get_case_list_for_contact`,
            {
              contact_uuid: uuid || '',
            }
          )
        ).catch(openServerError);

      return body
        ? (body.data || [])
            .filter(caseItem => caseItem.attributes.status !== 'resolved')
            .map(caseItem => ({
              value: caseItem.id,
              label: `${caseItem.attributes.display_id}: ${caseItem.attributes.case_type_name}`,
              subLabel: caseItem.attributes.description,
            }))
        : [];
    }
  );

  return {
    loading: data.status === 'loading',
    choices: data.data || [],
  };
};

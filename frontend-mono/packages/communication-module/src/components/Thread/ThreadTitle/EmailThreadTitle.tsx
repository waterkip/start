// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { ExternalMessageType } from '../../../types/Message.types';
import ThreadTitle, { ThreadTitlePropsType } from './ThreadTitle';

export type EmailThreadTitlePropsType = {
  message: ExternalMessageType;
  showAddThreadToCaseDialog: () => void;
  canAddThreadToCase: boolean;
} & Pick<ThreadTitlePropsType, 'context' | 'showLinkToCase'>;

const EmailThreadTitle: React.FunctionComponent<EmailThreadTitlePropsType> = ({
  message,
  showAddThreadToCaseDialog,
  canAddThreadToCase,
  ...rest
}) => (
  <ThreadTitle
    {...rest}
    title={message.subject}
    openThreadToCaseDialog={
      canAddThreadToCase ? showAddThreadToCaseDialog : undefined
    }
  />
);

export default EmailThreadTitle;

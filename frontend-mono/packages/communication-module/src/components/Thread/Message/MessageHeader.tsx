// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { ReactElement } from 'react';
import fecha from 'fecha';
import { useTranslation } from 'react-i18next';
import { useTheme } from '@mui/material';

import { useWidth } from '@mintlab/ui/App/library/useWidth';
import Typography from '@mui/material/Typography';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { MessageType } from '../../../types/Message.types';
import { ThreadType } from '../Thread.types';
import { messageHeaderStyle } from './MessageHeader.style';
import { MessageActions } from './MessageActions/MessageActions';

const formatDate = (format: string, zulu: string) => {
  const newDate = new Date(zulu);
  return fecha.format(newDate, format);
};

export interface MessageHeaderPropsType {
  message: MessageType;
  thread: ThreadType;
  title: string;
  subTitle?: string;
  info?: string;
  icon: ReactElement;
}

const MessageHeader: React.FunctionComponent<MessageHeaderPropsType> = ({
  message,
  thread,
  title,
  subTitle,
  info,
  icon,
}) => {
  const { createdDate, isUnread, failureReason } = message;
  const width = useWidth();
  const theme = useTheme<Theme>();
  const classes = messageHeaderStyle(theme, Boolean(isUnread));
  const [t] = useTranslation('communication');
  const dateFormat =
    width === 'xs' ? t('thread.dateFormat') : t('thread.dateFormatLong');

  return (
    <div style={classes.header as any}>
      <div style={{ display: 'flex' }}>
        {width !== 'xs' && <div style={classes.icon}>{icon}</div>}
        <div style={classes.meta}>
          <div style={classes.title}>
            <Typography variant="body1" sx={classes.titleMain}>
              {title}
            </Typography>
            <Typography variant="body1" sx={classes.titleSub}>
              {subTitle}
            </Typography>
          </div>
          {Boolean(info) && (
            <Typography variant="body2" sx={classes.info}>
              {info}
            </Typography>
          )}
        </div>
      </div>
      <div style={classes.dateAndMenu}>
        {message.hasAttachements && (
          <Icon style={{ color: '#1860C3' }} size="small">
            {iconNames.attachment}
          </Icon>
        )}
        {failureReason && (
          <Tooltip style={{ width: 'auto' }} title={failureReason}>
            <Icon style={{ color: '#ff0000' }} size="small">
              {iconNames.warning}
            </Icon>
          </Tooltip>
        )}
        <Typography variant="caption" sx={classes.date}>
          {formatDate(dateFormat, createdDate)}
        </Typography>
        <MessageActions message={message} thread={thread} />
      </div>
    </div>
  );
};

export default MessageHeader;

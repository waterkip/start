// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Accordion, AccordionSummary, AccordionDetails } from '@mui/material';
import { ExternalMessageType } from '../../../types/Message.types';
import { ThreadType } from '../Thread.types';
import MessageHeader from './MessageHeader';
import MessageContent from './MessageContent';
import MessageSenderIcon from './MessageSenderIcon/MessageSenderIcon';
import CreateExternalMessageTitle from './library/CreateExternalMessageTitle';

type PostexMessagePropsType = {
  message: ExternalMessageType;
  thread: ThreadType;
  caseNumber?: string;
  expanded?: boolean;
  isUnread: boolean;
};

/* eslint complexity: [2, 7] */
const PostexMessage: React.FunctionComponent<PostexMessagePropsType> = ({
  message,
  expanded = false,
  thread,
}) => {
  const { content, sender, type, attachments, participants } = message;
  const icon = <MessageSenderIcon type={sender ? sender.type : 'person'} />;
  const title = CreateExternalMessageTitle(message);
  const [t] = useTranslation('communication');
  const [isExpanded, setExpanded] = useState(message.isUnread || expanded);

  const formattedParticipants = participants?.to.map(pt => pt.name).join(', ');
  const headerName = 'thread.postexMessageHeader.recipient';
  const participantTextLines = formattedParticipants
    ? [`${t(headerName)} : ${formattedParticipants}`]
    : [];

  const info = participantTextLines.reduce<string>(
    (acc, line) =>
      line
        ? `${acc}
    ${line}`
        : acc,
    ''
  );

  return (
    <Accordion
      sx={{
        padding: 0,
        boxShadow: 'none',
        '&:before': {
          height: 0,
        },
      }}
      defaultExpanded={isExpanded}
    >
      <AccordionSummary
        onClick={() => setExpanded(!isExpanded)}
        sx={{
          height: '100px',
          padding: 0,
          boxShadow: 'none',
          '& .MuiExpansionPanelSummary-content': {
            margin: 0,
            display: 'inline',
          },
        }}
      >
        <MessageHeader
          thread={thread}
          message={message}
          title={title}
          icon={icon}
          info={info}
        />
      </AccordionSummary>
      <AccordionDetails sx={{ padding: 0, display: 'inline' }}>
        <MessageContent
          content={content}
          attachments={attachments}
          type={type}
        />
      </AccordionDetails>
    </Accordion>
  );
};

export default PostexMessage;

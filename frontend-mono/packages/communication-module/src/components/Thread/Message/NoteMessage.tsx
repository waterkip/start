// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
//@ts-ignore
import ZsIcon from '@mintlab/ui/App/Zaaksysteem/ZsIcon';
import { NoteMessageType } from '../../../types/Message.types';
import { ThreadType } from '../Thread.types';
import MessageHeader from './MessageHeader';
import MessageContent from './MessageContent';

type NoteMessagePropsType = {
  thread: ThreadType;
  message: NoteMessageType;
};

const NoteMessage: React.FunctionComponent<NoteMessagePropsType> = ({
  thread,
  message,
}) => {
  const [t] = useTranslation('communication');
  const title = t('thread.note.messageTitle', {
    user: message.sender.name,
  });
  const icon = <ZsIcon size="small">{'entityType.inverted.note'}</ZsIcon>;

  return (
    <div>
      <MessageHeader
        thread={thread}
        message={message}
        title={title}
        icon={icon}
      />
      <MessageContent content={message.content} type={message.type} />
    </div>
  );
};

export default NoteMessage;

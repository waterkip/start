// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
/* eslint-disable no-console */

const fs = require('fs');
const { promisify } = require('util');
const prettier = require('prettier');

const writeFileAsync = promisify(fs.writeFile);

async function writeTypes(types, branch) {
  types.forEach(async ([domain, api_docs, entities]) => {
    const timezone = { timeZone: 'Europe/Amsterdam' };
    const dateTime = new Date().toLocaleString('nl', timezone);

    const banner = `
      // Generated on: ${dateTime} (Amsterdam time)
      // Branch used: ${branch}
      //
      // This file was automatically generated. DO NOT MODIFY IT BY HAND.
      // Instead, rerun generation of ${domain} domain.
      /* eslint-disable */
    `;

    const allEntities = entities.reduce((acc, ent) => [...acc, ...ent], []);
    const allTypes = [...api_docs, ...allEntities];
    const output = allTypes.join('\n');
    const namespacedOutput = `${banner} export namespace API${domain} {${output}}`;

    const path = `./types/API${domain}.types.ts`;
    const prettierOptions = {
      ...(await prettier.resolveConfig(__filename)),
      parser: 'typescript',
    };
    const formattedOutput = await prettier.format(
      namespacedOutput,
      prettierOptions
    );
    writeFileAsync(path, formattedOutput);

    console.log(`- written ${allTypes.length} types in ${path}`);
  });
}

module.exports = writeTypes;

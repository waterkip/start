// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
// eslint-disable-next-line import/no-unresolved
import { shallow } from 'enzyme';
import Clone from '.';

/**
 * @test {Clone}
 */
describe('The `Clone` component', () => {
  test('passes its props into its child component', () => {
    const Test = props => {
      expect(props).toEqual({
        foo: 'foo',
        bar: 'bar',
      });

      return null;
    };

    shallow(
      <Clone foo="foo" bar="bar">
        <Test />
      </Clone>
    ).render();
  });
});

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { default as Clone } from './Clone';
export * from './Clone';
export default Clone;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

// declare module '@material-ui/icons/utils/createSvgIcon' {
//   import SvgIcon from '@material-ui/core/SvgIcon';

//   declare function createSvgIcon(
//     path: React.ComponentType,
//     displayName: string
//   ): typeof SvgIcon;

//   export default createSvgIcon;
// }

declare module '@material-ui/icons/utils/createSvgIcon' {
  import SvgIcon from '@material-ui/core/SvgIcon';

  declare function createSvgIcon(
    path: React.ReactNode,
    displayName: string
  ): typeof SvgIcon;

  export default createSvgIcon;
}

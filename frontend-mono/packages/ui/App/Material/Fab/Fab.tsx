// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import MuiFab from '@mui/material/Fab';
import Icon, { IconNameType } from '../Icon';

type MuiFabPropsType = React.ComponentProps<typeof MuiFab>;

type FabPropsType = {
  color?: MuiFabPropsType['color'];
  size?: MuiFabPropsType['size'];
  sx?: MuiFabPropsType['sx'];
  iconName: IconNameType;
  action: () => void;
  ariaLabel: string;
};

export const Fab = ({
  action,
  iconName,
  color,
  ariaLabel,
  size,
  sx,
}: FabPropsType) => {
  return (
    <MuiFab
      data-scope="fab-button"
      color={color}
      aria-label={ariaLabel}
      onClick={action}
      size={size}
      sx={sx}
    >
      <Icon style={{ display: 'flex' }}>{iconName}</Icon>
    </MuiFab>
  );
};

export default Fab;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export type DatePickerPropsType = {
  value?: string | null;
  onChange?: (ev: { target: { name: string; value: string | null } }) => void;
  name?: string;
  outputFormat?: 'ISO' | 'DATE' | string;
  displayFormat?: string;
  clearable?: boolean;
  disabled?: boolean;
  placeholder?: string;
  minDate?: string | Date | null;
  maxDate?: string | Date | null;
  sx?: any;
};

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import MuiPagination from '@mui/material/TablePagination';
import PaginationActions from './PaginationActions';
import { usePaginationStyle } from './Pagination.style';

// ZS-TODO: check MUI documentation
// `onChangePage` is never called but required
// (we pass it to `ActionsComponent`)
const noop = () => {};

type MuiPaginationPropsType = React.ComponentProps<typeof MuiPagination>;

type PaginationPropsType = {
  changeRowsPerPage: MuiPaginationPropsType['onRowsPerPageChange'];
  component: React.ElementType<any>;
  count: number;
  getNewPage: (newPage: number) => void;
  labelDisplayedRows: MuiPaginationPropsType['labelDisplayedRows'];
  labelRowsPerPage: MuiPaginationPropsType['labelRowsPerPage'];
  page: number;
  rowsPerPage: number;
  rowsPerPageOptions: MuiPaginationPropsType['rowsPerPageOptions'];
  scope: string;
};

export const Pagination = ({
  changeRowsPerPage,
  component,
  count,
  labelRowsPerPage,
  labelDisplayedRows,
  page,
  rowsPerPageOptions,
  rowsPerPage,
  getNewPage,
  scope,
}: PaginationPropsType) => {
  const pageCount = Math.ceil(count / rowsPerPage);
  const sx = usePaginationStyle();

  // note: SelectProps and ActionsComponent are UpperCamelCase
  return (
    <MuiPagination
      sx={sx}
      component={component}
      count={count}
      labelDisplayedRows={labelDisplayedRows}
      labelRowsPerPage={labelRowsPerPage}
      onRowsPerPageChange={changeRowsPerPage}
      onPageChange={noop}
      page={page}
      rowsPerPage={rowsPerPage}
      rowsPerPageOptions={rowsPerPageOptions}
      data-scope={scope}
      ActionsComponent={() => (
        <PaginationActions
          onChangePage={getNewPage}
          page={page}
          pageCount={pageCount}
        />
      )}
    />
  );
};

export default Pagination;

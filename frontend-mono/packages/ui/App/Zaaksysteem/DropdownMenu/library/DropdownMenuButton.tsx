// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { Fragment } from 'react';
import MuiButton from '@mui/material/Button';
import Icon, { IconNameType } from '../../../Material/Icon';
import { addScopeAttribute } from '../../../library/addScope';
import { useDropdownMenuButtonStyle } from '../DropdownMenu.style';

export type DropdownMenuButtonProps = {
  label: string;
  action: (event: any) => void;
  icon?: IconNameType;
  scope?: string;
  disabled?: boolean;
  hidden?: boolean;
};

const DropdownMenuButton = React.forwardRef(
  (
    {
      action,
      icon,
      label,
      scope,
      disabled = false,
      hidden = false,
    }: DropdownMenuButtonProps,
    ref
  ) => {
    const style = useDropdownMenuButtonStyle();
    const labelComponent = icon ? (
      <Fragment>
        <Icon size="small">{icon}</Icon>
        {label}
      </Fragment>
    ) : (
      label
    );

    if (hidden) return null;

    return (
      <MuiButton
        ref={ref as any}
        onClick={action}
        fullWidth={false}
        disabled={disabled}
        {...addScopeAttribute(scope, 'button')}
        sx={style}
      >
        {labelComponent}
      </MuiButton>
    );
  }
);

DropdownMenuButton.displayName = 'DropdownMenuButton';

export default DropdownMenuButton;

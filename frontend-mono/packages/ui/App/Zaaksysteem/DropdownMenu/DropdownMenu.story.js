// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { React, stories, action } from '../../story';
import DropdownMenu, { DropdownMenuList } from '.';

const triggerButton = (
  <button type="button" data-scope="story-dropdown-trigger">
    Open
  </button>
);

stories(module, __dirname, {
  Default() {
    return (
      <DropdownMenu trigger={triggerButton} scope="story">
        <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div>
      </DropdownMenu>
    );
  },
  'Simple list without icons': function SimpleList() {
    const data = [
      {
        action: action('Foo Action'),
        label: 'First menu item',
      },
      {
        action: action('Bar Action'),
        label: 'Second menu item',
      },
    ];

    return (
      <DropdownMenu trigger={triggerButton} scope="story">
        <DropdownMenuList items={data} scope="story-dropdown-menu" />
      </DropdownMenu>
    );
  },
  'Multiple lists with icons': function MultipleLists() {
    const data = [
      [
        {
          action: action('Foo Action'),
          label: 'Undo recent actions',
        },
      ],
      [
        {
          action: action('Foo Action'),
          icon: 'menu',
          label: 'First menu item with icon',
        },
        {
          action: action('Bar Action'),
          icon: 'home',
          label: 'Second menu item with icon',
        },
        {
          action: action('Foo Action'),
          icon: 'menu',
          label: 'Third menu item with icon',
        },
      ],
      [
        {
          action: action('Foo Action'),
          icon: 'menu',
          label: 'Fourth menu item with icon',
        },
        {
          action: action('Bar Action'),
          icon: 'home',
          label: 'Fifth menu item with icon',
        },
        {
          action: action('Foo Action'),
          icon: 'menu',
          label: 'Sixth menu item with icon',
        },
      ],
    ];

    return (
      <DropdownMenu trigger={triggerButton}>
        <DropdownMenuList items={data} />
      </DropdownMenu>
    );
  },
});

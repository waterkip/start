// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { helpMode, setHelpMode } from '@zaaksysteem/common/src/signals';
import { Dialog, DialogTitle } from '@mintlab/ui/App/Material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import { iconNames } from '@mintlab/ui/App/Material/Icon';
import {
  closeHelpDialog,
  helpDialogData,
} from '@zaaksysteem/common/src/signals';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import { getHelpData } from './HelpDialog.library';
import locale from './HelpDialog.locale';

const KEY_CODE = 'F2';

const HelpDialog = () => {
  const [t] = useTranslation('helpDialog');
  const { title, content } = getHelpData(t, helpDialogData.value);
  const open = Boolean(title);

  return (
    <Dialog
      open={open}
      scope={'help-dialog'}
      fullWidth={true}
      maxWidth={'sm'}
      onClose={closeHelpDialog}
    >
      <DialogTitle
        elevated={true}
        icon={iconNames.info}
        title={title}
        onCloseClick={closeHelpDialog}
      />
      <DialogContent>
        {typeof content === 'string' ? <span>{content}</span> : content}
      </DialogContent>
    </Dialog>
  );
};

const HelpDialogWrapper = () => {
  useEffect(() => {
    const toggleHelpMode = (event: any) => {
      const { id, title } = helpDialogData.value;

      if (
        (event.code === KEY_CODE ||
          (event.type === 'click' && helpMode.value)) &&
        !id &&
        !title
      ) {
        setHelpMode(!helpMode.value);
      }
    };

    window.addEventListener('keydown', toggleHelpMode);
    window.addEventListener('click', toggleHelpMode);

    return () => {
      window.removeEventListener('keydown', toggleHelpMode);
      window.addEventListener('click', toggleHelpMode);
    };
  }, []);

  return (
    <I18nResourceBundle resource={locale} namespace="helpDialog">
      <HelpDialog />
    </I18nResourceBundle>
  );
};

export default HelpDialogWrapper;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useTopMenuStyles = makeStyles(
  ({ palette: { primary, elephant, common } }: Theme) => ({
    counter: {
      backgroundColor: primary.main,
      color: common.white,
    },
    counterInactive: {
      backgroundColor: elephant.dark,
      color: common.white,
    },
    textWrapper: {
      textOverflow: 'ellipsis',
      overflow: 'hidden',
    },
    text: {
      whiteSpace: 'nowrap',
    },
  })
);

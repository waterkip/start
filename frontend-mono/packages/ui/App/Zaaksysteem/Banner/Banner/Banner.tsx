// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import ButtonBase from '@mui/material/ButtonBase';
import { Box } from '@mui/system';
import Typography from '@mui/material/Typography';
import Icon, { iconNames } from '../../../Material/Icon/Icon';
import { addScopeAttribute, addScopeProp } from '../../../library/addScope';
import DropdownMenu, {
  DropdownMenuList,
} from '../../DropdownMenu/DropdownMenu';
import BannerButton from '../BannerButton/BannerButton';
import { useBannnerStyle } from './Banner.style';

const { isArray } = Array;

export const Banner = ({
  variant = 'secondary',
  label,
  primary,
  secondary,
  scope,
}: {
  variant: 'primary' | 'secondary' | 'danger';
  label: string;
  action: any;
  scope: string;
  primary: any;
  secondary: any;
}) => {
  const styles = useBannnerStyle(variant);

  return (
    <Box sx={styles.outerContainer} {...addScopeAttribute(scope, 'banner')}>
      <Box sx={styles.innerContainer}>
        <Typography variant="body2" sx={styles.blackFont}>
          {label}
        </Typography>

        {getPrimary({
          variant,
          primary,
          scope,
        })}
        {getSecondary({
          secondary,
          scope,
        })}
      </Box>
    </Box>
  );
};

const getPrimary = ({ variant, primary, scope }: any) => {
  if (!primary) return null;

  return (
    <BannerButton
      variant={variant}
      action={primary.action}
      {...addScopeProp(scope, 'banner-primary')}
    >
      {primary.label}
    </BannerButton>
  );
};

const getSecondary = ({ secondary, scope }: any) => {
  if (!secondary) return null;

  if (isArray(secondary)) {
    const triggerButton = (
      <ButtonBase {...addScopeProp(scope, 'banner-secondary-list-button')}>
        <Icon size="small">{iconNames.more_vert}</Icon>
      </ButtonBase>
    );

    return (
      <DropdownMenu trigger={triggerButton}>
        <DropdownMenuList items={secondary} scope="banner" />
      </DropdownMenu>
    );
  }

  return (
    <ButtonBase
      onClick={secondary.action}
      {...addScopeProp(scope, 'banner-secondary-button')}
    >
      <Icon size="small">{secondary.icon}</Icon>
    </ButtonBase>
  );
};

export default Banner;

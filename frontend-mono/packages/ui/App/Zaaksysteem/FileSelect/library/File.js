// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import Card from '@mui/material/Card';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import LinearProgress from '@mui/material/LinearProgress';
import { withStyles, withTheme } from '@mui/styles';
import ZsIcon from '@mintlab/ui/App/Zaaksysteem/ZsIcon';
import classNames from 'classnames';
import Button from '../../../Material/Button/Button';
import { addScopeAttribute, addScopeProp } from '../../../library/addScope';
import { Loader } from '../../Loader/Loader';
import { fileStylesheet } from './File.style';

const getLoader = ({ status, uploadProgress }) => {
  switch (status) {
    case 'pending':
      return uploadProgress > 0 ? (
        <LinearProgress variant="determinate" value={uploadProgress * 100} />
      ) : (
        <LinearProgress />
      );

    default:
      return null;
  }
};

const getIcon = ({ status, theme }) => {
  switch (status) {
    case 'pending':
      return (
        <Loader
          style={{ height: '19px', width: '19px', margin: '0 auto' }}
          color={theme.mintlab.greyscale.darker}
        />
      );
    case 'failed':
      return <ZsIcon size="small">file.error</ZsIcon>;
    default:
      return <ZsIcon size="small">file.default</ZsIcon>;
  }
};

const wrapTooltip = (message, component) => {
  return message ? <Tooltip title={message}>{component}</Tooltip> : component;
};

const File = ({
  name,
  className,
  classes,
  onDeleteClick,
  status = '',
  uploadProgress = 0,
  theme,
  scope,
  onLinkClick,
  onDownloadClick,
  iconHoverMessage,
  ...rest
}) => (
  <Card
    className={classNames(classes.card, className)}
    {...addScopeAttribute(scope, 'file')}
    {...rest}
  >
    <div className={classes.uploadProgress}>
      {getLoader({ status, uploadProgress })}
    </div>
    <div className={classes.flexContainer}>
      <div className={classes.iconContainer}>
        {wrapTooltip(iconHoverMessage, getIcon({ status, theme, classes }))}
      </div>

      <p className={classes.name}>{name}</p>
      {onDeleteClick && (
        <Button
          name="removeFile"
          action={onDeleteClick}
          icon="close"
          iconSize="small"
          {...addScopeProp(scope, 'file', 'delete')}
        />
      )}
      {onLinkClick && (
        <Button
          name="selectFile"
          action={onLinkClick}
          iconSize="extraSmall"
          icon="open_in_new"
          {...addScopeProp(scope, 'file', 'link')}
        />
      )}
      {onDownloadClick && (
        <Button
          name="downloadFile"
          action={onDownloadClick}
          iconSize="extraSmall"
          icon="save_alt"
          {...addScopeProp(scope, 'file', 'link')}
        />
      )}
    </div>
  </Card>
);

export default withTheme(withStyles(fileStylesheet)(File));

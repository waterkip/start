// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { SubjectTypeType } from '@zaaksysteem/common/src/types/SubjectTypes';
import CaseCreateDialog from '@mintlab/ui/App/Zaaksysteem/Layout/Topbar/Dialogs/CaseCreate/CaseCreateDialog';
import ContactMomentCreateDialog from '@mintlab/ui/App/Zaaksysteem/Layout/Topbar/Dialogs/ContactMomentCreate/ContactMomentCreateDialog';
import { useKccStyles } from './Kcc.styles';
import { PhoneCallType } from './Kcc.types';
import PhoneCall from './PhoneCall/PhoneCall';

type KccPropsType = {
  phoneCalls: PhoneCallType[];
  clearPhoneCall: (id: number) => void;
  clearActivePhoneCalls: () => void;
};

type DialogType = {
  type: 'case' | 'contactMoment';
  contact: {
    type: SubjectTypeType;
    uuid: string;
  };
};

const Kcc: React.ComponentType<KccPropsType> = ({
  phoneCalls,
  clearPhoneCall,
  clearActivePhoneCalls,
}) => {
  const classes = useKccStyles();
  const [dialog, setDialog] = useState<DialogType | null>(null);
  const closeDialog = () => setDialog(null);

  return (
    <div className={classes.wrapper}>
      {phoneCalls.map(phoneCall => (
        <PhoneCall
          key={phoneCall.id}
          phoneCall={phoneCall}
          clearPhoneCall={clearPhoneCall}
          clearActivePhoneCalls={clearActivePhoneCalls}
          setDialog={setDialog}
        />
      ))}

      {dialog && (
        <CaseCreateDialog
          open={dialog.type === 'case'}
          onClose={closeDialog}
          contact={dialog.contact}
        />
      )}
      {dialog && (
        <ContactMomentCreateDialog
          open={dialog.type === 'contactMoment'}
          onClose={closeDialog}
          contact={dialog.contact}
        />
      )}
    </div>
  );
};

export default Kcc;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useKccStyles = makeStyles(() => ({
  wrapper: {
    position: 'absolute',
    bottom: 0,
    left: 100,
    right: 0,
    margin: 'auto',
    display: 'flex',
    justifyContent: 'center',
    gap: 20,
    alignItems: 'flex-end',
  },
}));

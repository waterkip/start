// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import classNames from 'classnames';
import { useTranslation } from 'react-i18next';
import Typography from '@mui/material/Typography';
import useSession from '@zaaksysteem/common/src/hooks/useSession';
import Button from '@mintlab/ui/App/Material/Button';
import { useNavigationBarStyles } from './NavigationBar.styles';
import ReleaseNotesDialog from './Dialogs/ReleaseNotesDialog';
import AboutDialog from './Dialogs/AboutDialog';
import { getButtons } from './NavigationBar.library';
import ListWithNavigate from './ListComponents/ListWithNavigate';
import { DialogType } from './NavigationBar.types';
import HelpDialog from './Dialogs/HelpDialog';

/* eslint-disable complexity */
export const NavigationBar: React.ComponentType = () => {
  const classes = useNavigationBarStyles();
  const [t] = useTranslation('layout');
  const [open, setOpen] = useState(false);
  const [dialogOpen, setDialogOpen] = useState<DialogType>(null);
  const close = () => setDialogOpen(null);

  const session = useSession();
  const buttonLists = getButtons(t, session, setDialogOpen);

  /* 
    The future is to use useNavigate() to navigate between views,
    but this can only be done within a BrowserRouter,
    and only to views that are in the parent BrowserRouter.

    In stages:

    1.  Current: Utilizing useNavigate to go Main -> Main and Admin -> Admin

    2.  When Main and Admin are consolidated, then useNavigate 
          between react views.

        Remove the check (2) that limits useNavigate
          from Main to Main or Admin to Admin.

    3.  When all views are refactored to react, then useNavigate
          to all views.

        Note: There are still links to external apps.
          Don't remove the href prop.
  */

  return (
    <nav
      className={classNames(
        classes.wrapper,
        open ? classes.open : classes.close
      )}
    >
      <div className={classes.menuButtonWrapper}>
        <div className={classes.environmentWrapper}>
          <Typography
            variant="h3"
            color="inherit"
            classes={{ root: classes.environment }}
          >
            {session.account.instance.company}
          </Typography>
        </div>
        <Button
          action={() => setOpen(!open)}
          name="navigationBarMenu"
          icon={open ? 'close' : 'menu'}
          aria-label={`${t('menu')} ${t(`common:verbs.${open ? 'close' : 'open'}`).toLowerCase()}`}
          sx={{
            color: 'inherit',
            paddingRight: open ? 2 : 1,
          }}
        />
      </div>

      <div className={classes.listsWrapper}>
        {buttonLists.map((buttonList, index) => (
          <div key={index}>
            {index ? <div className={classes.separator} /> : null}

            <ListWithNavigate open={open} buttons={buttonList} />
          </div>
        ))}
      </div>

      {dialogOpen === 'help' && <HelpDialog onClose={close} />}
      {dialogOpen === 'about' && <AboutDialog onClose={close} />}
      {dialogOpen === 'releaseNotes' && <ReleaseNotesDialog onClose={close} />}
    </nav>
  );
};

export default NavigationBar;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useHelpDialogStyles = makeStyles(
  ({ palette: { elephant } }: Theme) => ({
    wrapper: {
      display: 'flex',
      flexDirection: 'column',
      gap: 50,
    },
    section: {
      display: 'flex',
      flexDirection: 'column',
      gap: 10,
      alignItems: 'center',
    },
    description: {
      textAlign: 'center',
      margin: 0,
    },
    key: {
      backgroundColor: elephant.lightest,
      padding: 5,
      borderRadius: 4,
      margin: '0px 5px',
    },
  })
);

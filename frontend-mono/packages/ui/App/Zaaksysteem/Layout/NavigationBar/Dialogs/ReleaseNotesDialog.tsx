// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useRef } from 'react';
import { useTranslation } from 'react-i18next';
import DialogContent from '@mui/material/DialogContent';
import { Dialog, DialogTitle } from '@mintlab/ui/App/Material/Dialog';
import { useReleaseNotesDialogStyles } from './ReleaseNotesDialog.styles';

type ReleaseNotesDialogPropsType = {
  onClose: () => void;
};

const ReleaseNotesDialog: React.ComponentType<ReleaseNotesDialogPropsType> = ({
  onClose,
}) => {
  const [t] = useTranslation('layout');
  const classes = useReleaseNotesDialogStyles();
  const dialogEl = useRef();

  const title = t('dialog.releaseNotes.title');

  return (
    <>
      <Dialog
        disableBackdropClick={true}
        open={true}
        onClose={onClose}
        scope={'release-notes-dialog'}
        ref={dialogEl}
      >
        <DialogTitle
          elevated={true}
          icon="lightbulb"
          title={title}
          onCloseClick={onClose}
        />
        <DialogContent>
          <div className={classes.wrapper}>
            {t('dialog.releaseNotes.text')}:{' '}
            <a
              rel="noreferrer"
              href={
                'https://exxellence.atlassian.net/wiki/spaces/KRN/pages/966729859115/Release+notes+Zaaksysteem'
              }
              target="_blank"
            >
              {title}
            </a>
          </div>
        </DialogContent>
      </Dialog>
    </>
  );
};

export default ReleaseNotesDialog;

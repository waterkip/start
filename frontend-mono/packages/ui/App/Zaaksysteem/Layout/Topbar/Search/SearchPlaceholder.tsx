// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import { useSearchStyles } from './Search.styles';

type PlaceholderPropsType = {
  setActive: () => void;
};

const SearchPlaceholder: React.ComponentType<PlaceholderPropsType> = ({
  setActive,
}) => {
  const classes = useSearchStyles();
  const [t] = useTranslation('search');
  const placeholder = t('searchIn', { systemName: t('common:systemName') });

  const isMac = navigator.appVersion.indexOf('Mac') != -1;

  React.useEffect(() => {
    const keyDownListener = ({ ctrlKey, metaKey, key }: any) => {
      const ctrl = isMac ? metaKey : ctrlKey;

      if (ctrl && key === '/') {
        setActive();
      }
    };

    window.addEventListener('keydown', keyDownListener);

    return () => window.removeEventListener('keydown', keyDownListener);
  }, []);

  return (
    <div
      className={classes.placeholder}
      tabIndex={0}
      role="button"
      onClick={setActive}
      onKeyDown={event => {
        if (event.key === 'Enter') {
          setActive();
        }
      }}
      aria-label={placeholder}
    >
      <Select
        variant="generic"
        isClearable={false}
        isMulti={false}
        placeholder={placeholder}
        freeSolo={true}
        startAdornment={
          <Icon style={{ marginRight: '10px' }} size="small">
            {iconNames.search}
          </Icon>
        }
        inputProps={{ tabIndex: -1 }}
      />
    </div>
  );
};

export default SearchPlaceholder;

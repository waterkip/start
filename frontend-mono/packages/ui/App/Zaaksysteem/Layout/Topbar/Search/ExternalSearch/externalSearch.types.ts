// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import { ForceKeywordType, ResultType } from '../Search.types';
import { Next2KnowType } from './next2know/next2know.types';

export type OverwriteType = any;

type SupportedExternalSearches = 'next2know';

export type ExternalSearchOverwritesType = {
  [key in SupportedExternalSearches]: OverwriteType;
};
export type SetExternalSearchOverwriteType = (
  name: SupportedExternalSearches,
  overwrites: OverwriteType
) => void;

export type PerformSearchType = (
  t: i18next.TFunction,
  keyword: string,
  integration: any,
  forceKeyword: ForceKeywordType,
  setExternalSearchOverwrite: SetExternalSearchOverwriteType
) => Promise<ResultType[]>;

export type ExternalSearchType = {
  integration: Next2KnowType;
  performSearch: PerformSearchType;
};

export type PerformExternalSearchesType = (
  t: i18next.TFunction,
  keyword: string,
  externalSearches: ExternalSearchType[],
  forceKeyword: ForceKeywordType,
  setExternalSearchOverwrite: SetExternalSearchOverwriteType
) => Promise<ResultType[]>;

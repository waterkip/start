// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { APICaseManagement } from '@zaaksysteem/generated';

import * as i18next from 'i18next';
import {
  ExternalSearchType,
  SetExternalSearchOverwriteType,
} from './ExternalSearch/externalSearch.types';

export type FilterType =
  | 'all'
  | 'case'
  | 'organization'
  | 'employee'
  | 'document'
  | 'saved_search'
  | 'custom_object'
  | 'object_v1'
  | 'case_type'
  | 'custom_object_type';

export type PreferenceType = FilterType | null;

export type FocusOnSearchType = () => void;
export type GetFilterPreferenceType = () => PreferenceType;
export type SetFilterPreferenceType = (filter: PreferenceType) => void;

type FilterChoiceType = {
  label: string;
  value: FilterType;
};

export type GetFilterChoicesType = (
  t: i18next.TFunction,
  hasCatalogPrivileges: boolean
) => FilterChoiceType[];

export type SearchRequestParamsType = APICaseManagement.SearchRequestParams;
export type SearchResponseBodyType = APICaseManagement.SearchResponseBody;

// remove Exclude 'person' after it's removed as an option from the API
export type ParamFilterTypeType = Exclude<
  SearchRequestParamsType['type'][0],
  'person'
>;
export type ResultTypeType = ParamFilterTypeType | 'external';

export type ResultType = {
  uuid: string;
  type: ResultTypeType;
  title: string;
  description?: string;
  url?: string;
  action?: () => void;
  caseUuid?: string;
};

export type GroupedResultsType = {
  case?: ResultType[];
  organization?: ResultType[];
  employee?: ResultType[];
  document?: ResultType[];
  saved_search?: ResultType[];
  custom_object?: ResultType[];
  object_v1?: ResultType[];
  case_type?: ResultType[];
  custom_object_type?: ResultType[];
  external?: ResultType[];
};

export type GetUrlType = (
  uuid: string,
  type: ResultTypeType,
  caseUuid?: string
) => string;

export type ConvertFilterToTypeType = (
  filter: FilterType,
  hasCatalogPrivileges: boolean
) => ParamFilterTypeType[];

export type FormatResultType = (
  row: SearchResponseBodyType['data'][0]
) => ResultType;

export type GetBuiltInSavedSearchesType = (
  t: i18next.TFunction,
  filter: FilterType,
  keyword: string
) => ResultType[];

export type GroupResultsType = (results: ResultType[]) => GroupedResultsType;

export type ForceKeywordType = (keyword: string) => void;

export type useSearchType = (
  t: i18next.TFunction,
  keyword: string,
  filter: FilterType,
  hasCatalogPrivileges: boolean,
  externalSearches: ExternalSearchType[],
  forceKeyword: ForceKeywordType,
  setExternalSearchOverwrite: SetExternalSearchOverwriteType
) => { results: ResultType[]; loading: boolean };

export type SearchType = (
  params: SearchRequestParamsType
) => Promise<SearchResponseBodyType>;

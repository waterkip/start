// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { PerformExternalSearchesType } from './externalSearch.types';

export const performExternalSearches: PerformExternalSearchesType = async (
  t,
  keyword,
  externalSearches,
  forceKeyword,
  setExternalSearchOverwrite
) => {
  const resultSets = await Promise.all(
    externalSearches.map(
      async ({ integration, performSearch }) =>
        await performSearch(
          t,
          keyword,
          integration,
          forceKeyword,
          setExternalSearchOverwrite
        )
    )
  );

  const results = resultSets.reduce((acc, results) => [...acc, ...results], []);

  return results;
};

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { useGetContactQuery } from '../Profile.requests';
import { useInfoStyles } from './Info.styles';

export type InfoPropsType = { open: number };

export const Info: React.ComponentType<InfoPropsType> = ({ open }) => {
  const [t] = useTranslation('layout');
  const classes = useInfoStyles();

  const { data: contact } = useGetContactQuery(open);

  if (!contact) return null;

  const { name, email, phoneNumber, department, roles } = contact;

  return (
    <div className={classes.info}>
      <div className={classes.infoItem}>
        <div className={classes.infoLabel}>{t('profile.info.name')}</div>
        <div className={classes.infoValue}>{name}</div>
      </div>
      <div className={classes.infoItem}>
        <div className={classes.infoLabel}>{t('profile.info.phonenumber')}</div>
        <div className={classes.infoValue}>{phoneNumber}</div>
      </div>
      <div className={classes.infoItem}>
        <div className={classes.infoLabel}>{t('profile.info.email')}</div>
        <div className={classes.infoValue}>{email}</div>
      </div>
      <div className={classes.infoItem}>
        <div className={classes.infoLabel}>{t('profile.info.department')}</div>
        <div className={classes.infoValue}>{department}</div>
      </div>
      <div className={classes.infoItem}>
        <div className={classes.infoLabel}>{t('profile.info.roles')}</div>
        <div className={classes.infoValue}>
          <ul className={classes.roleList}>
            {roles.map((role: string) => (
              <li key={role}>{role}</li>
            ))}
          </ul>
        </div>
      </div>
    </div>
  );
};

export default Info;

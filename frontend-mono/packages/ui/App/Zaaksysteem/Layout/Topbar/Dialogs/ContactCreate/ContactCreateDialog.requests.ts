// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { buildUrl } from '@mintlab/kitchen-sink/source';
import { request } from '@zaaksysteem/common/src/library/request/request';
import {
  FetchCountryCodesType,
  CreateContactType,
  CountyCodeResponseBodyType,
  FetchLegalEntityTypesType,
  LegalEntityResponseBodyType,
  CreateContactResponseBodyType,
} from './ContactCreateDialog.types';

export const fetchCountryCodes: FetchCountryCodesType = async () => {
  const url = buildUrl('/api/v1/general/country_codes', { rows_per_page: 400 });

  const response = await request<CountyCodeResponseBodyType>('GET', url);

  return response;
};

export const fetchLegalEntityTypes: FetchLegalEntityTypesType = async () => {
  const url = buildUrl('/api/v1/general/legal_entity_types', {
    rows_per_page: 100,
  });

  const response = await request<LegalEntityResponseBodyType>('GET', url);

  return response;
};

export const createContact: CreateContactType = async params => {
  const url = buildUrl('/betrokkene/0/bewerken', params);

  const response = request<CreateContactResponseBodyType>('POST', url);

  return response;
};

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { v4 } from 'uuid';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { ContactType } from '@zaaksysteem/common/src/components/form/fields/ContactFinder/ContactFinder.types';
import { APICaseManagement } from '@zaaksysteem/generated';
import { APICommunication } from '@zaaksysteem/generated';
import { ContactMomentCreateDialogPropsType } from './ContactMomentCreateDialog.types';
import { ContactMomentCreateFormValuesType } from './ContactMomentCreateDialog.formDefinition';

// Get full details of the contact and return a Select Option object
export const getContactSelectOption = async (
  contact: ContactMomentCreateDialogPropsType['contact']
): Promise<ContactType | null> => {
  if (!contact) return null;

  const url = buildUrl<APICaseManagement.GetContactRequestParams>(
    '/api/v2/cm/contact/get_contact',
    contact
  );
  const result = await request('GET', url);

  return result
    ? {
        value: contact.uuid,
        type: contact.type,
        label: result.data.attributes.name,
      }
    : null;
};

export const createContactMoment = async (
  values: ContactMomentCreateFormValuesType
): Promise<boolean> => {
  const data = {
    thread_uuid: v4(),
    contact_moment_uuid: v4(),
    case_uuid: values.case?.value || values.case || null,
    contact_uuid: values.contact.value,
    channel: values.contactChannel.value,
    content: values.content,
    direction: values.direction,
  };
  const url = '/api/v2/communication/create_contact_moment';
  const result =
    await request<APICommunication.CreateContactMomentResponseBody>(
      'POST',
      url,
      data
    );

  return result.data.success;
};

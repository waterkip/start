// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { SubjectTypeType } from '@zaaksysteem/common/src/types/SubjectTypes';
import { ContactType } from '@zaaksysteem/common/src/components/form/fields/ContactFinder/ContactFinder.types';

type ContactChannelType =
  | 'assignee'
  | 'frontdesk'
  | 'phone'
  | 'mail'
  | 'email'
  | 'webform'
  | 'social_media';

export type ContactMomentCreateDialogPropsType = {
  selectedDocumentUuid?: string;
  contact?: {
    uuid: string;
    type: SubjectTypeType;
  };
  open: boolean;
  onClose: () => void;
  container?: React.ReactInstance | null;
  contactChannel?: ContactChannelType;
};

export type ContactMomentCreateDialogInnerPropsType = Pick<
  ContactMomentCreateDialogPropsType,
  'open' | 'onClose' | 'container' | 'contactChannel'
> & {
  contactOption?: ContactType | null;
};

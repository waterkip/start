// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import ButtonBar from './ButtonBar';

export type { ButtonBarPropsType } from './ButtonBar';

export default ButtonBar;

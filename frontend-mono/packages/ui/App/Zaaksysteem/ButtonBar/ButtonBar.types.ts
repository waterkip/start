// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { IconNameType } from '@mintlab/ui/App/Material/Icon';

export type ButtonType = {
  label: string;
  name: string;
  type?: 'text' | 'icon';
  icon?: IconNameType;
  disabled?: boolean;
  action: (event: any) => void;
  [key: string]: any;
};

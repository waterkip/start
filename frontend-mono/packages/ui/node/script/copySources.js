const { promisify } = require('util');
const copy = promisify(require('copy'));
const rimraf = promisify(require('rimraf'));

function cleanDist() {
  console.log('Cleaning lib...');
  return rimraf('./lib');
}

function copySourceFiles() {
  console.log('Copying source files to lib folder...');

  return copy(
    [
      './App/**/*.{js,css,svg,json,woff,woff2}',
      '!./App/**/*.story.js',
      '!./App/**/*.test.js',
    ],
    './lib'
  );
}

cleanDist().then(copySourceFiles).catch(console.error);

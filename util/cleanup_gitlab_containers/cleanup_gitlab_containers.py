#!/usr/bin/env python3
#
# Cleanup GitLab container registry
#
import argparse
import os
import shlex
import subprocess
import sys
from joblib import Parallel, delayed
from loguru import logger
from multiprocessing import cpu_count
from packaging import version
from typing import Final, List

log_level: Final = os.getenv("LOG_LEVEL", "INFO")
dry_run: Final = True
no_of_tags_to_keep: Final[int] = 5
ci_project_path = os.environ.get("CI_PROJECT_PATH")

# This function is used in the sort it selects the
# version number string and converts it to a version
# Than this version can be used in the sorting function


def _sort_key(tag: dict):
    return version.parse(tag["version_number"])


def is_valid_tag(tag: str) -> bool:
    try:
        version.parse(tag["version_number"])
        return True
    except version.InvalidVersion:
        return False


# Determine wicht tags to keep
# Returns a list of all the tags to keep
def determine_wich_tags_to_remove(repo: str, tags: List[str]) -> dict:
    logger.info(f"found {len(tags)} tags for repo {repo}")
    logger.debug(f"The following tags are found (in repo: {repo}): {tags}")

    # Make a dict to store the different variants of tags to keep
    # How many tags to keep is set in `no_of_tags_to_keep`
    tags_per_branch = {
        "-master": [],
        "-development": [],
        "-preprod": [],
        "-production": [],
        "master-ci-": [],
        "development-ci-": [],
        "preprod-ci-": [],
        "production-ci-": [],
        "release/v": [],
        "-release": [],
    }

    #  Make al list of all the tags to remove and one with all the tags to keep
    tags_to_remove: List[str] = []
    tags_to_keep: List[str] = []

    for tag in tags:
        if tag.endswith("-master"):
            tags_per_branch["-master"].append(
                {"label": tag, "version_number": tag.split("-master")[0]}
            )
        elif tag.endswith("-development"):
            tags_per_branch["-development"].append(
                {"label": tag, "version_number": tag.split("-development")[0]}
            )
        elif tag.endswith("-preprod"):
            tags_per_branch["-preprod"].append(
                {"label": tag, "version_number": tag.split("-preprod")[0]}
            )
        elif tag.endswith("-production"):
            tags_per_branch["-production"].append(
                {"label": tag, "version_number": tag.split("-production")[0]}
            )

        elif tag.startswith("master-ci-"):
            tags_per_branch["master-ci-"].append(
                {"label": tag, "version_number": tag.split("master-ci-")[1]}
            )
        elif tag.startswith("development-ci-"):
            tags_per_branch["development-ci-"].append(
                {"label": tag, "version_number": tag.split("development-ci-")[1]}
            )
        elif tag.startswith("preprod-ci-"):
            tags_per_branch["preprod-ci-"].append(
                {"label": tag, "version_number": tag.split("preprod-ci-")[1]}
            )
        elif tag.startswith("production-ci-"):
            tags_per_branch["production-ci-"].append(
                {"label": tag, "version_number": tag.split("production-ci-")[1]}
            )

        elif tag.startswith("release/v"):
            tags_per_branch["release/v"].append(
                {"label": tag, "version_number": tag.split("release/v")[1]}
            )
        elif tag.endswith("-release"):
            tags_per_branch["-release"].append(
                {"label": tag, "version_number": tag.split("-release")[0]}
            )
        elif tag.endswith("latest"):
            tags_to_keep.append({"label": tag, "version_number": tag})
        else:
            tags_to_remove.append({"label": tag, "version_number": tag})

    for values in tags_per_branch.values():
        # splitup invalid and valid tags
        valid_tags = list(filter(is_valid_tag, values))
        invalid_tags = [tag for tag in valid_tags if tag not in valid_tags]

        # Sort the list so the old versions can be removed
        sorted_list = sorted(valid_tags, key=_sort_key, reverse=True)

        tags_to_remove.extend(list(reversed(sorted_list[no_of_tags_to_keep:])))
        tags_to_remove.extend(invalid_tags)  # invalid tags will be deleted
        tags_to_keep.extend(sorted_list[:no_of_tags_to_keep])

    logger.debug(f"keeping tags: {tags_to_keep}")
    logger.debug(f"removing tags: {tags_to_remove}")
    logger.info(f"removing {len(tags_to_remove)} tags and keeping {len(tags_to_keep)}")

    return {"tags_to_remove": tags_to_remove, "tags_to_keep": tags_to_keep}


# Get all the tags for the repo and delete all tags that need to be removed
def cleanup_repo(repo: str, delete_tags: bool):
    logger.info(f"Start cleaning up containers for repo {repo}")

    logger.info(f"Get all the tags for repo {repo}")
    tags = (
        subprocess.run(["./regctl", "tag", "ls", repo], stdout=subprocess.PIPE)
        .stdout.decode()
        .splitlines()
    )

    tags_to_remove = determine_wich_tags_to_remove(repo, tags)["tags_to_remove"]

    total_no_of_tags = len(tags_to_remove)

    for index, tag in enumerate(tags_to_remove):
        try:
            delete_tag = ["./regctl", "tag", "delete", f"{repo}:{tag['label']}"]
            logger.info(f"Removing {index}/{total_no_of_tags}: {shlex.join(delete_tag)}")

            if delete_tags:
                subprocess.check_call(delete_tag)

        except subprocess.CalledProcessError as e:
            logger.warning(f"Failed to remove tag {tag['label']} for repo {repo}: {e}")


# Main
if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="cleanup-gitlab-containers",
        description="Cleanup containers in the gitlab container registry",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument("--delete-tags", action="store_true")

    args = parser.parse_args()

    logger.remove()
    log_format = "<level>{level: <8}</level> {message}"
    logger.add(sys.stdout, format=log_format, level=log_level)

    if args.delete_tags:
        logger.info("Option delete-tags is set. Tags will be deleted")
    else:
        logger.info("This is a dry run, no tags will be deleted.")
        logger.info("Add the option --delete-tags to delete the tags")

    logger.info("Create/Update regctl config")
    subprocess.check_call("./regctl registry set --hostname ${CI_REGISTRY}", shell=True)
    subprocess.check_call(
        "./regctl registry login -u ${CI_REGISTRY_USER} -p ${CI_REGISTRY_PASSWORD}", shell=True
    )

    with open("repositories_to_clean.txt") as f:
        repositories = [line.strip() for line in f.readlines() if not line.startswith("#")]

    logger.info(f"Found {len(repositories)} repositories to clean-up")

    Parallel(n_jobs=cpu_count(), backend="multiprocessing")(
        delayed(cleanup_repo)(f"{ci_project_path}/{repo}", args.delete_tags)
        for repo in repositories
    )
